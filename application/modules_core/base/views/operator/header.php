<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.1.3
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?php echo $this->session->userdata('page_title'); ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
    <link href="<?php echo base_url();?>metronic/assets/font-awesome/fontcss.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-slider/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-slider/css/full-slider.css" rel="stylesheet" type="text/css"/>
     <!--  <link href="<?php echo base_url();?>metronic/assets/global/css/components.css" rel="stylesheet" type="text/css"/>
    -->  
<!--  END THEME STYLES -->
    <link href="<?php echo base_url();?>metronic/assets/css/bootstrap.css" rel="stylesheet">

    <link href="<?php echo base_url();?>metronic/assets/css/style1.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>metronic/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>metronic/assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    

    <link href="<?php echo base_url();?>metronic/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>metronic/assets/css/style-responsive.css" rel="stylesheet">
  
    <link href="<?php echo base_url();?>metronic/assets/css/style-responsive.css" rel="stylesheet">

    <link href="<?php echo base_url();?>metronic/assets/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="<?php echo base_url();?>metronic/assets/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-datetimepicker/css/datetimepicker.css" rel="stylesheet">


    <link href="<?php echo base_url();?>metronic/assets/css/datepicker.css" rel="stylesheet">
    <link href="<?php echo base_url();?>metronic/assets/css/qunit.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>metronic/assets/notif/css/ns-default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>metronic/assets/notif/css/ns-style-attached.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>metronic/assets/notif/css/ns-style-growl.css" />

    <script src="<?php echo base_url();?>metronic/assets/js/jquery-2.1.3.js"></script>
    <script src="<?php echo base_url();?>metronic/assets/js/chart-master/Chart.js"></script>

 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-full-width">
<!-- BEGIN HEADER -->

<!--header start-->
      <header class="header black-bg" style="background-color:#FFF;">
            <!--logo start-->

            <a href="<?php echo base_url();?>dashboard/operator" class="logo rw-words rw-words-1" style="font-size:20px;margin-top:25px;">
                <span>RSUD</span>
                <span>Rumah Sakit Umum Daerah</span>
                <span>Bhayangkara</span>
                <span>RSUD</span>
                <span>Rumah Sakit Umum Daerah</span>
                <span>Bhayangkara</span>
            </a><!--logo end-->
            <div class="top-menu">
            <!--disini buat naruh menu -->
                <ul class="nav pull-right top-menu">
                    <li><div class="name"> <?php echo $user['user_name'] ?> </a>
                    </li>
                    <li><a class="logout" href="<?php echo base_url();?>login/operator/logout">Logout</a></li>
                </ul>
            </div>
        </header>

    <!-- END HEADER INNER -->
<!-- END HEADER -->
<div class="clearfix">
</div>

<div class="men-separator"></div>

<?php echo $menu_view ?>


<div class="clearfix">
</div>

<div class="page-container" style="background: #EFEFEF">

    <div class="page-content-wrapper">
        <div class="page-content">

    <div id="dialogoverlay"></div>
        <div id="dialogbox">
          <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
          </div>
        </div>