		</div>
	</div>
	<!-- END CONTENT -->

<!-- END CONTAINER -->
</div>
<!-- BEGIN FOOTER -->
<div style="padding-bottom:40px;height:20px;background:#0379ae">
	<div class="page-footer" >
		<div class="page-footer-inner" >
			<label style="margin-top:5px;">
			 2014 &copy; Medical apps by Mahatala.
			</label>
			<div class="go-top">
			<i class="fa fa-angle-up"></i>
			</div>
			
		</div>
	</div>
</div>
<!-- END FOOTER -->

<script src="<?php echo base_url();?>metronic/assets/bootstrap3-editable/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/js/jquery-2.1.3.js"></script> 
<script src="<?php echo base_url();?>metronic/assets/js/bootstrap-datepicker.min.js"></script>
    
<script src="<?php echo base_url();?>metronic/assets/js/chart-master/Chart.js"></script>
<script src="<?php echo base_url();?>metronic/assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS 
<script src="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-editable/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<script src="<?php echo base_url();?>metronic/assets/global/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/global/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/typeahead/bootstrap3-typeahead.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/typeahead/bootstrap3-typeahead.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo base_url() ?>metronic/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>metronic/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>metronic/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>metronic/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>metronic/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>metronic/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/admin/pages/scripts/form-validation.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>metronic/assets/admin/pages/scripts/table-advanced.js"></script>

<script src="<?php echo base_url();?>metronic/assets/js/modernizr.js"></script>
<script src="<?php echo base_url();?>metronic/assets/js/icon/modernizr.custom.js"></script>
<script src="<?php echo base_url();?>metronic/assets/js/icon/modernizr.custom.72111.js"></script>
<script src="<?php echo base_url();?>metronic/assets/notif/js/modernizr.custom.js"></script>

<script src="<?php echo base_url();?>metronic/assets/notif/js/classie.js"></script>

<script src="<?php echo base_url();?>metronic/assets/notif/js/notificationFx.js"></script>
<script src="<?php echo base_url();?>metronic/assets/modal2/js/cssParser.js"></script>
<script src="<?php echo base_url();?>metronic/assets/modal2/js/modalEffects.js"></script>
<!-- END PAGE LEVEL SCRIPTS <script src="<?php echo base_url();?>metronic/assets/modal2/js/css-filters-polyfill.js"></script>
-->
<script>
	var polyfilter_scriptpath = 'metronic/assets/modal2/js/';
</script>



<script>
	setTimeout(function() {

				var x = getAllElementsWithAttribute('data-provide');
				setElements(x);

				$("#tblrwp1").hide();
				$("#rwp1").click(function(){
					$("#tblrwp1").slideToggle();
				});

				$("#tblrwp2").hide();
				$("#rwp2").click(function(){
					$("#tblrwp2").slideToggle();
				});

				$("#tblrwp3").hide();
				$("#rwp3").click(function(){
					$("#tblrwp3").slideToggle();
				});

				$("#retur").editable();
				$(".app").editable();
				$(".appBar").editable();
				$(".tmbhnBy").editable();
				$("#ubahObat").hide();
				$("#ubahDetObat").hide();
				
				$(".informasi").show();

				$("#tabelcare").hide();
				$("#tabelhis").hide();
				$("#infoAdaan").hide();
				$("#infoRiwaAda").hide();
				$("#infoTerimaObat").hide();
				$("#infoRiwTerimaObat").hide();
				$(".tutupBiru").hide();
				$("#infoMintaObat").hide();
				$("#infoRiwMintaObat").hide();
									
				$("#infoRetDepartemen").hide();
				$("#infoRiwRetDepartemen").hide();
				$("#infoRetDistributor").hide();
				$("#infoRiwRetDistributor").hide();
				$("#infoMasObat").hide();
				$("#infoDetObat1").hide();
				$("#btnBatalObat").hide();
				$("#btnBatalDetObat").hide();
				$("#btnBatalObatBrg").hide();
				
				$("#btnBatalDetBarang").hide();
				
				$("#penunjangLab").hide();
				$("#asalRujukan").hide();
				$("#tujuanRujuk").hide();

				$("#tndknOverview").on('mouseover', function() {
			      $(this).animate({ backgroundColor: "#A7FFAE", color : "black" }, 500)
						  }).on('mouseleave', function() {
						      $(this).stop(true, true);
						      $(this).animate({ backgroundColor: "#50BFF9", color : "white" }, 500);
				 }); 

	

				$("#tambahOver2").on('mouseover', function() {
			      //jQuery UI doesn't support the hotpink keyword 
			      $(this).animate({ color : "black" }, 500)
						  }).on('mouseleave', function() {
						      $(this).stop(true, true);
						      $(this).animate({ color : "white" }, 500);
				 }); 

				$("#tmbhOverview").on('mouseover', function() {
			      //jQuery UI doesn't support the hotpink keyword 
			      $(this).animate({ backgroundColor: "#A7FFAE", color : "black" }, 500)
						  }).on('mouseleave', function() {
						      $(this).stop(true, true);
						      $(this).animate({ backgroundColor: "#50BFF9", color : "white" }, 500);
				 }); 
				$("#hstoryOverview").on('mouseover', function() {
			      //jQuery UI doesn't support the hotpink keyword 
			      $(this).animate({ backgroundColor: "#A7FFAE", color : "black" }, 500)
						  }).on('mouseleave', function() {
						      $(this).stop(true, true);
						      $(this).animate({ backgroundColor: "#50BFF9", color : "white" }, 500);
				 }); 

				$("#statusRujukan").change(function(){
					if (document.getElementById('statusRujukan').value=="Ya") {
										
						$("#asalRujukan").show();
						$("#tujuanRujuk").show();
					}
					else if (document.getElementById('statusRujukan').value=="Tidak"){			
						$("#asalRujukan").hide();
						$("#tujuanRujuk").hide();
					}
				});

				$(".paginate-button").click(function(){
					$(".paginate-button").removeClass("active");
					$(this).addClass("active");					
				});

				//btn

				$("button[class=cekSubmit]").click(function(){
					alert('Sudah berhasil tersubmit');				
				});

				$("#edMasObat").click(function(e){
					e.preventDefault();
					$("#nmObat").focus();
					$("#btnBatalObat").show();
					$("#smpanObat").hide();
					$("#ubahObat").show();					
				});

				$("#btnBatalObatBrg").hide();
				$("#ubahBarang").hide();
				
				$("#edMasBarang").click(function(e){
					e.preventDefault();
					$("#btnBatalObatBrg").show();
					$("#nmBarang").focus();
					$("#smpanBarang").hide();
					$("#smpanBarang1").hide();
					$("#ubahBarang").show();	
										
				});
				$("#btnBatalObatBrg").click(function(e){
					e.preventDefault();
					$("#btnBatalObatBrg").hide();
					$("#smpanBarang").show();
					$("#smpanBarang1").show();
					$("#ubahBarang").hide();
																
				});
				$("#ubahBarang").click(function(e){
					e.preventDefault();
					$("#btnBatalObatBrg").hide();
					$("#smpanBarang").show();
					$("#smpanBarang1").show();
					$("#ubahBarang").hide();
																
				});

				$("#ubahDetBarang").hide();
				$("#ubahObat").hide();
				$("#edDetLogBarang").click(function(e){
					e.preventDefault();
					$("#btnBatalDetBarang").show();
					$("#pedBarangDet").prop('disabled', true);
					$("#ubahDetBarang").show()
					$("#resetBarang").hide();
					$("#smpanDetBarang").hide();
				});
				$("#ubahDetBarang").click(function(e){
					e.preventDefault();
					$("#btnBatalDetBarang").hide();
					$("#resetBarang").show();
					$("#pedBarangDet").prop('disabled', false);
					$("#ubahDetBarang").hide()
					$("#smpanDetBarang").show();
				});

				$("#btnBatalObat").click(function(e){
					e.preventDefault();
					$("#btnBatalObat").hide();
					$("#smpanObat").show();
					$("#btnBatalObat").hide();
					$("#ubahObat").hide();
																
				});
				$("#ubahObat").click(function(e){
					e.preventDefault();
					$("#btnBatalObat").hide();
					$("#resetObat").show();
					$("#smpanObat1").show();
					$("#ubahObat").hide();
																
				});
				$("#edMasObat").click(function(e){
					e.preventDefault();
					$("#btnBatalObat").show();
					$("#resetObat").hide();
					$("#smpanObat1").hide();
					$("#ubahObat").show();
																
				});

				$("#ubahDetObat").click(function(e){
					e.preventDefault();

					$("#noBatchDetObat").prop('disabled', false);

					$("#jmlDetObat").prop('disabled', false);

					$("#pedObatDet").prop('disabled', false);

					$("#selectSumDanaObat").prop('disabled', false);
					
					$("#btnBatalDetObat").hide();

					$("#ubahDetObat").hide();
					$("#resetObat1").show();


					$("#smpanDetObat").show();
										
				});
				$("#smpanObatBrg").click(function(e){
					e.preventDefault();
					$("#btnBatalObatBrg").hide();
										
				});
				$("#smpanDetBarang").click(function(e){
					e.preventDefault();

					$("#pedBarangDet").prop('disabled', false);
				
					
					$("#btnBatalDetBarang").hide();					
				});

				$("#edDetObat").click(function(e){
					e.preventDefault();
					$("#noBatchDetObat").prop('disabled', true);

					$("#jmlDetObat").prop('disabled', true);

					$("#pedObatDet").prop('disabled', true);

					$("#selectSumDanaObat").prop('disabled', true);
					
					$("#btnBatalDetObat").show();
					$("#resetObat1").hide();

					$("#exdate").focus();
										
					$("#ubahDetObat").show();

					$("#smpanDetObat").hide();
				});
				$("#editInvenBut").hide();
			
				$("#editInvenBut").click(function(e){
					e.preventDefault();
					$(".editInven").removeClass("editableform editable-click");
					$(".editInven").removeClass("editable");
					$(".editInven").css("color","black");
					$(".editInven").css("cursor","default");
					$("#editInvenBut").hide();
					$(".edIven").show();

										
				});

				$("#tblInven1").on('click','tr td a.edIven',function(e){
					e.preventDefault();
					 var inven = $(this).closest('td').prevAll('td:has(a.editInven)')
                 .children('td a.editInven');
					inven.addClass("editableform editable-click");
					inven.editable();
					inven.css("color","blue");
					inven.css("cursor","pointer");
					$("#editInvenBut").show();
					$(".edIven").hide();

				});

				//logistik
				$("#editInvenButBar").hide();
				

				$("#editInvenButBar").click(function(e){
					e.preventDefault();
					$(".editInvenBar").removeClass("editableform editable-click");
					$(".editInvenBar").removeClass("editable");
					$(".editInvenBar").css("color","black");
					$(".editInvenBar").css("cursor","default");
					$("#editInvenButBar").hide();
					document.getElementById("status").innerHTML = "Edit";
										
				});


				
				$("#tblInven2").on('click','tr td a.edIvenBar',function(e){
					e.preventDefault();
					 var inven = $(this).closest('td').prevAll('td:has(a.editInvenBar)')
                 .children('td a.editInvenBar');
					inven.addClass("editableform editable-click");
					inven.editable();
					inven.css("color","blue");
					inven.css("cursor","pointer");
					document.getElementById("status").innerHTML = "Batal";
					$("#editInvenButBar").show();

				});

				$("#adaanTambah").click(function(event){
					event.preventDefault();
					 
				});


				$("#penunjangEKG").show();
				$("#penunjangusg").hide();
				$("#penunjangFis").hide();
				$("#penunjangRad").hide();
				$("#penunjangMri").hide();
				$("#penunjangCt").hide();
				$("#penunjangend").hide();

				$("#depTujuan").change(function(){
					if (document.getElementById('depTujuan').value=="Laboratorium") {
								
						$("#penunjangLab").show();
						$("#penunjangEKG").hide();
						$("#penunjangusg").hide();
						$("#penunjangFis").hide();
						$("#penunjangRad").hide();
						$("#penunjangMri").hide();
						$("#penunjangCt").hide();
						$("#penunjangend").hide();
					}
					else if (document.getElementById('depTujuan').value=="EKG") {

						$("#penunjangLab").hide();
						$("#penunjangEKG").show();
						$("#penunjangusg").hide();
						$("#penunjangFis").hide();
						$("#penunjangRad").hide();
						$("#penunjangMri").hide();
						$("#penunjangCt").hide();
						$("#penunjangend").hide();
					}else if (document.getElementById('depTujuan').value=="USG") {
						
						$("#penunjangLab").hide();
						$("#penunjangEKG").hide();
						$("#penunjangusg").show();
						$("#penunjangFis").hide();
						$("#penunjangRad").hide();
						$("#penunjangMri").hide();
						$("#penunjangCt").hide();
						$("#penunjangend").hide();
					}else if (document.getElementById('depTujuan').value=="CT Scan") {
						
						$("#penunjangLab").hide();
						$("#penunjangEKG").hide();
						$("#penunjangusg").hide();
						$("#penunjangFis").hide();
						$("#penunjangRad").hide();
						$("#penunjangMri").hide();
						$("#penunjangCt").show();
						$("#penunjangend").hide();
					}else if (document.getElementById('depTujuan').value=="MRI") {
						
						$("#penunjangLab").hide();
						$("#penunjangEKG").hide();
						$("#penunjangusg").hide();
						$("#penunjangFis").hide();
						$("#penunjangRad").hide();
						$("#penunjangMri").show();
						$("#penunjangCt").hide();
						$("#penunjangend").hide();
					}else if (document.getElementById('depTujuan').value=="Endoscopy") {
						
						$("#penunjangLab").hide();
						$("#penunjangEKG").hide();
						$("#penunjangusg").hide();
						$("#penunjangFis").hide();
						$("#penunjangRad").hide();
						$("#penunjangMri").hide();
						$("#penunjangCt").hide();
						$("#penunjangend").show();
					}else if (document.getElementById('depTujuan').value=="Radiologi") {
						
						$("#penunjangLab").hide();
						$("#penunjangEKG").hide();
						$("#penunjangusg").hide();
						$("#penunjangFis").hide();
						$("#penunjangRad").show();
						$("#penunjangMri").hide();
						$("#penunjangCt").hide();
						$("#penunjangend").hide();
					}else if (document.getElementById('depTujuan').value=="Fisioterapi") {
						
						$("#penunjangLab").hide();
						$("#penunjangEKG").hide();
						$("#penunjangusg").hide();
						$("#penunjangFis").show();
						$("#penunjangRad").hide();
						$("#penunjangMri").hide();
						$("#penunjangCt").hide();
						$("#penunjangend").hide();
					}else{

						$("#penunjangLab").hide();
						$("#penunjangEKG").hide();
						$("#penunjangusg").hide();
						$("#penunjangFis").hide();
						$("#penunjangRad").hide();
						$("#penunjangMri").hide();
						$("#penunjangCt").hide();
						$("#penunjangend").hide();
					}
				});


				$("#alasanPindah").hide();
				$("#semuaPoli").hide();
				$("#alasanPlg").show();
				$("#isiRujuk").hide();
				$("#ketMati").hide();

				$("#formRekUnit").hide();
				$("#formRekDokter").hide();
				$("#pasienMeninggal").hide();
				$("#detPasienMeninggal").hide();
				$("#formMati").hide();
				$("#wktMati").hide();

				$("#formLahir").show();
				$(".hovermenu").hide();

				$("#list1 img").click(function(){
					$("#hovermenu1").slideToggle();
				});

				$("#navigation ul li > a").on('mouseover', function() {
			      //jQuery UI doesn't support the hotpink keyword 
			      $(this).animate({ backgroundColor: "white", color : "#179D3C" }, 500)
						  }).on('mouseleave', function() {
						      $(this).stop(true, true);
						      $(this).animate({ backgroundColor: "#31DB75", color : "black" }, 500);
				 }); 

				$("#list2 img").click(function(){
					$("#hovermenu2").slideToggle();
				});	

				$("#list3 img").click(function(){
					$("#hovermenu3").slideToggle();
				});	

				$("#list4 img").click(function(){
					$("#hovermenu4").slideToggle();
				});	
				
				$("#list5 img").click(function(){
					$("#hovermenu5").slideToggle();
				});	


				$('#invObat').datetimepicker();
				
				//   $('#detpick').datepicker({
	   			$("#statusLahir").change(function(){
					if (document.getElementById('statusLahir').value=="Hidup") {

						$("#formMati").hide();
						$("#formLahir").show();
					}
					else if (document.getElementById('statusLahir').value=="Mati") {
						
						$("#formMati").show();
						$("#formLahir").hide();
					}else{
								
						$("#formMati").hide();
						$("#formLahir").hide();
					}
				});
				//     type: 'text',
				//     pk: 1,
				//     url: '/post',
				//     title: 'Enter username'
				// });
				$("#alasanKeluarPasien").change(function(){
					if (document.getElementById('alasanKeluarPasien').value=="Rujuk Spesialis") {
						$("#ketMati").hide();
						$("#wktMati").hide();
						
						$("#detPasienMeninggal").hide();
						$("#alasanPindah").hide();
						$("#semuaPoli").show();
						$("#alasanPlg").hide();
						$("#isiRujuk").hide();
						$("#formRekUnit").hide();
						$("#formRekDokter").hide();
						$("#pasienMeninggal").hide();
					}
					else if (document.getElementById('alasanKeluarPasien').value=="Pasien Dipulangkan") {
						$("#semuaPoli").hide();
						$("#alasanPlg").show();
						$("#isiRujuk").hide();
						$("#formRekUnit").hide();
						$("#formRekDokter").hide();
						$("#pasienMeninggal").hide();
						$("#ketMati").hide();
						$("#wktMati").hide();
						
						$("#detPasienMeninggal").hide();
						$("#alasanPindah").hide();
					}else if (document.getElementById('alasanKeluarPasien').value=="Pasien Dipindahkan") {
						$("#semuaPoli").hide();
						$("#alasanPlg").hide();
						$("#ketMati").hide();
						$("#wktMati").hide();
						
						$("#detPasienMeninggal").hide();
						$("#alasanPindah").show();
						$("#isiRujuk").hide();
						$("#formRekUnit").hide();
						$("#formRekDokter").hide();
						$("#pasienMeninggal").hide();
					}
					
					else if (document.getElementById('alasanKeluarPasien').value=="Atas Permintaan Sendiri") {
						$("#semuaPoli").hide();
						$("#alasanPlg").show();
						$("#isiRujuk").hide();
						$("#formRekUnit").hide();
						$("#formRekDokter").hide();
						$("#pasienMeninggal").hide();
						$("#ketMati").hide();
						$("#wktMati").hide();
						
						$("#detPasienMeninggal").hide();
						$("#alasanPindah").hide();
					}else if (document.getElementById('alasanKeluarPasien').value=="Rujuk Rumah Sakit Lain") {
						$("#semuaPoli").hide();
						$("#alasanPlg").hide();
						$("#isiRujuk").show();
						$("#formRekUnit").hide();
						$("#formRekDokter").hide();
						$("#ketMati").hide();
						$("#wktMati").hide();
						
						$("#detPasienMeninggal").hide();
						$("#alasanPindah").hide();
						$("#pasienMeninggal").hide();
					}
					else if (document.getElementById('alasanKeluarPasien').value=="Rujuk Rawat Inap(IGD)") {
						$("#semuaPoli").hide();
						$("#alasanPlg").hide();
						$("#isiRujuk").hide();
						$("#formRekUnit").show();
						$("#ketMati").hide();
						$("#wktMati").hide();
						
						$("#detPasienMeninggal").hide();
						$("#alasanPindah").hide();
						$("#formRekDokter").show();
						$("#pasienMeninggal").hide();
					}
					else if (document.getElementById('alasanKeluarPasien').value=="Rujuk Rawat Inap(Admisi)") {
						$("#semuaPoli").hide();
						$("#alasanPlg").hide();
						$("#isiRujuk").hide();
						$("#alasanPindah").hide();
						$("#formRekUnit").show();
						$("#formRekDokter").show();
						$("#pasienMeninggal").hide();
						$("#ketMati").hide();
						$("#wktMati").hide();

					}
					else if (document.getElementById('alasanKeluarPasien').value=="Pasien Meninggal") {
						$("#semuaPoli").hide();
						$("#alasanPlg").hide();
						$("#isiRujuk").hide();
						$("#alasanPindah").hide();
						$("#formRekUnit").hide();
						$("#formRekDokter").hide();
						$("#detPasienMeninggal").show();
						$("#pasienMeninggal").show();
						$("#ketMati").show();
						$("#wktMati").show();

					}
					else{
								
						$("#semuaPoli").hide();
						$("#alasanPlg").hide();
						$("#isiRujuk").hide();
						$("#alasanPindah").hide();
						$("#detPasienMeninggal").hide();
						$("#pasienMeninggal").hide();
						$("#ketMati").hide();
						$("#wktMati").hide();


						$("#formRekUnit").hide();
						$("#formRekDokter").hide();

					}
				});

				$("#tabelKonsultasi").hide();
				$("#tabelKamar").hide();
				$("#asuransi").hide();
				$("#kontrak").hide();
				$("#kelas").hide();
				$("#noasuransi").hide();
				$("#ivbpjs").hide();
				$("#carabayar").change(function(){
					if (document.getElementById('carabayar').value=="BPJS") {
						$("#asuransi").hide();
						$("#kontrak").hide();
						$("#kelas").show();
						$("#noasuransi").show();
						$("#ivbpjs").show();
						$("#ivnonbpjs").hide();
					}
					else if (document.getElementById('carabayar').value=="Asuransi") {
						$("#kontrak").hide();
						$("#kelas").hide();
						$("#asuransi").show();
						$("#noasuransi").show();
						$("#ivbpjs").hide();
						$("#ivnonbpjs").show();
					}
					else if (document.getElementById('carabayar').value=="Kontrak") {
						$("#asuransi").hide();
						$("#kelas").hide();
						$("#kontrak").show();
						$("#noasuransi").show();
						$("#ivbpjs").hide();
						$("#ivnonbpjs").show();
					}
					else if (document.getElementById('carabayar').value=="Jamkesmas") {
						$("#asuransi").hide();
						$("#kelas").hide();
						$("#kontrak").hide();
						$("#noasuransi").show();
						$("#ivbpjs").hide();
						$("#ivnonbpjs").show();
					}
					else if (document.getElementById('carabayar').value=="Gratis") {
						$("#asuransi").hide();
						$("#kelas").hide();
						$("#kontrak").hide();
						$("#noasuransi").show();
						$("#ivbpjs").hide();
						$("#ivnonbpjs").show();
					}
					else{
						$("#noasuransi").hide();	
						$("#asuransi").hide();
						$("#kontrak").hide();
						$("#kelas").hide();
						$("#ivbpjs").hide();
						$("#ivnonbpjs").show();

					}
				});

				$("#tblRiwayat").hide();
				$("#tblResep").hide();
				$("#asuransiruj").hide();
				$("#kontrakruj").hide();
				$("#kelasruj").hide();
				$("#noasuransiruj").hide();
				$("#carabayarruj").change(function(){
					if (document.getElementById('carabayarruj').value=="BPJS") {
						$("#asuransiruj").hide();
						$("#kontrakruj").hide();
						$("#kelasruj").show();
						$("#noasuransiruj").show();
					}
					else if (document.getElementById('carabayarruj').value=="Asuransi") {
						$("#kontrakruj").hide();
						$("#kelasruj").hide();
						$("#asuransiruj").show();
						$("#noasuransiruj").show();
					}
					else if (document.getElementById('carabayarruj').value=="Kontrak") {
						$("#asuransiruj").hide();
						$("#kelasruj").hide();
						$("#kontrakruj").show();
						$("#noasuransiruj").show();
					}
					else if (document.getElementById('carabayarruj').value=="Jamkesmas") {
						$("#asuransiruj").hide();
						$("#kelasruj").hide();
						$("#kontrakruj").hide();
						$("#noasuransiruj").show();
					}
					else if (document.getElementById('carabayarruj').value=="Gratis") {
						$("#asuransiruj").hide();
						$("#kelasruj").hide();
						$("#kontrakruj").hide();
						$("#noasuransiruj").show();
					}
					else{
						$("#noasuransiruj").hide();	
						$("#asuransiruj").hide();
						$("#kontrakruj").hide();
						$("#kelasruj").hide();
					}
				});

				$(".go-top").hide();
				var offset = 0;
			    var duration = 300;
			    jQuery(window).scroll(function() {
			        if (jQuery(this).scrollTop() > offset) {
			            jQuery('.go-top').fadeIn(duration);
			        } else {
			            jQuery('.go-top').fadeOut(duration);
			        }
			    });
			    jQuery('.go-top').click(function(event) {
			        event.preventDefault();
			        jQuery('html, body').animate({scrollTop: 0}, duration);
			        return false;
			    })

				$("#btnBawah").click(function(){
					$("#info1").slideToggle();
				});

				
				$("#ovih").click(function(){
					$("#inovih").slideToggle();
				});

				$("#tpfi").click(function(){
					$("#intpfi").slideToggle();
				});

				$("#rovih").click(function(){
					$("#inrovih").slideToggle();
				});

				$("#btnBawahTabelRiwayat").click(function(){
					$("#tblRiwayat").slideToggle();
				});

				$("#btnBawahRiwRetDepartemen").click(function(){
					$("#infoRiwRetDepartemen").slideToggle();
				});

				$("#btnBawahRiwRetDistributor").click(function(){
					$("#infoRiwRetDistributor").slideToggle();
				});

				$("#btnBawahMintaBarang").click(function(){
					$("#infoMintaBarang").slideToggle();
				});
				$("#btnBawahStokOpnameBarang").click(function(){
					$("#infoStokOpnameBarang").slideToggle();
				});
				$("#btnBawahAdaanGudang").click(function(){
					$("#infoAdaanGudang").slideToggle();
				});
				$("#btnBawahTerimaBarang").click(function(){
					$("#infoTerimaBarang").slideToggle();
				});
				$("#btnBawahRiwTerimaBarang").click(function(){
					$("#infoRiwTerimaBarang").slideToggle();
				});
				$("#btnBawahRiwaAdaGudang").click(function(){
					$("#infoRiwaAdaGudang").slideToggle();
				});
				
				$("#btnBawahDataPasienObat").click(function(){
					$("#infoDataPasienObat").slideToggle();
				});
				$("#btnBawahDetBarang1").click(function(){
					$("#infoDetBarang1").slideToggle();
				});
				$("#btnBawahInventoriGudangBarang").click(function(){
					$("#infoInventoriGudangBarang").slideToggle();
				});
				$("#btnBawahMasBarang").click(function(){
					$("#infoMasBarang").slideToggle();
				});

				$("#btnBawahDataKaryawan").click(function(){
					$("#infoDataKaryawan").slideToggle();
				});

				$("#btnBawahInventoriGudang").click(function(){
					$("#infoInventoriGudang").slideToggle();
				});
				
				$("#btnBawahTerimaObat").click(function(){
					$("#infoTerimaObat").slideToggle();
				});

				$("#btnBawahRiwTerimaObat").click(function(){
					$("#infoRiwTerimaObat").slideToggle();
				});
				$("#btnBawahMasObat").click(function(){
					$("#infoMasObat").slideToggle();
				});
				$("#btnBawahMintaObat").click(function(){
					$("#infoMintaObat").slideToggle();
				});
				$("#btnBawahRiwMintaObat").click(function(){
					$("#infoRiwMintaObat").slideToggle();
				});
				$("#btnBawahRetDistributor").click(function(){
					$("#infoRetDistributor").slideToggle();
				});
				$("#btnBawahRetDepartemen").click(function(){
					$("#infoRetDepartemen").slideToggle();
				});
				$("#btnBawahStokOpname").click(function(){
					$("#infoStokOpname").slideToggle();
				});
				$("#infoObatRacikan").hide();
				$("#btnBawahObatRacikan").click(function(){
					$("#infoObatRacikan").slideToggle();
				});
				$("#btnBawahObatNonRacikan").click(function(){
					$("#infoObatNonRacikan").slideToggle();
				});
				$("#btnBawahDataObat").click(function(){
					$("#infoDataObat").slideToggle();
				});


				$("#btnBawahAdaan").click(function(){
					$("#infoAdaan").slideToggle();
				});
				$("#btnBawahRiwaAda").click(function(){
					$("#infoRiwaAda").slideToggle();
				});


				$("#btnBawah2").click(function(){
					$("#info2").slideToggle();
				});

				$("#btnBawahBersalin").click(function(){
					$("#infoBersalin").slideToggle();
				});
				$("#btnBawahDetObat").click(function(){
					$("#infoDetObat").slideToggle();
				});
				$("#btnBawahDetObat1").click(function(){
					$("#infoDetObat1").slideToggle();
				});

				$("#btnBawahPenunjang").click(function(){
					$("#infoPenunjang").slideToggle();
				});

				$("#btnBawahWali").click(function(){
					$("#infoWali").slideToggle();
				});

				$("#btnBawahRiwayat").click(function(){
					$("#infoRiwayat").slideToggle();
				});


				$("#btnBawahOrder").click(function(){
					$("#infoKamar").slideToggle();
				});

				$("#btnBawahInventori").click(function(){
					$("#infoInventori").slideToggle();
				});

				$("#btnBawahPermintaan").click(function(){
					$("#infoPermintaan").slideToggle();
				});

				$("#btnBawahInventoriBarang").click(function(){
					$("#infoInventoriBarang").slideToggle();
				});

				$("#btnBawahPermintaanBarang").click(function(){
					$("#infoPermintaanBarang").slideToggle();
				});

				$("#btnBawahRetur").click(function(){
					$("#infoRetur").slideToggle();
				});

				$("#btnBawahTambahCare").click(function(){
					$("#tbhCare").slideToggle();
				});
				$("#btnBawahTambahResep").click(function(){
					$("#tambahResep").slideToggle();
				});
				$("#btnBawahTabelResep").click(function(){
					$("#tblResep").slideToggle();
				});


				$("#btnBawahCare").click(function(){
					$("#tabelcare").slideToggle();
				});

				$("#btnBawahHis").click(function(){
					$("#tabelhis").slideToggle();
				});

				$("#btnBawahhasil").click(function(){
					$("#infohasillab").slideToggle();
				});

				$("#btnBawahPeriksa").click(function(){
					$("#infohasilperiksa").slideToggle();
				});

				$("#btnBawahOrderMakan").click(function(){
					$("#infoMakan").slideToggle();
				});

				$("#btnBawahPindah").click(function(){
					$("#infoPindah").slideToggle();
				});

				$("#btnBawahOrderKonsul").click(function(){
					$("#infoKonsul").slideToggle();
				});
				$("#btnBawahResumePulang").click(function(){
					$("#infoResumePulang").slideToggle();
				});


				$("#btnBawahResumeKamar").click(function(){
					$("#infoResume").slideToggle();
				});

				$("#btnTabelKonsultasi").click(function(){
					$("#tabelKonsultasi").slideToggle();
				});

				

				$("#btnTableKamarOperasi").click(function(){
					$("#tabelKamar").slideToggle();
				});

				$("#btnDaftarmakan").click(function(){
					$("#tabelDaftarMakan").slideToggle();
				});

				$("#btnPindah").click(function(){
					$("#tabelPindahPasien").slideToggle();
				});

				$('#sandbox-container .input-daterange').datepicker({

				});
				$('#detpick').datepicker({
	                     format: "dd/mm/yyyy"
	            });

				$("#filter").change(function(){
					if(document.getElementById('filter').value=="Dokter"){
						$("#textFilter").attr('placeholder',"Nama Dokter");
					}
					else if(document.getElementById('filter').value=="Cara Pembayaran"){
						$("#textFilter").attr('placeholder',"Cara Bayar");
					}
					else if(document.getElementById('filter').value=="Pasien Baru"){
						$("#textFilter").attr('placeholder',"Pasien Baru");
					}
					else {
						$("#textFilter").attr('placeholder',"Pasien Lama");
					}
				});

				$("#filterInv").change(function(){
					if(document.getElementById('filterInv').value=="Nama Obat"){
						$("#filterby").attr('placeholder',"Nama Obat");
					}
					else if(document.getElementById('filterInv').value=="Jenis Obat"){
						$("#filterby").attr('placeholder',"Jenis Obat");
					}
					else if(document.getElementById('filterInv').value=="Merek"){
						$("#filterby").attr('placeholder',"Merek");
					}
					else if(document.getElementById('filterInv').value=="Sumber Dana"){
						$("#filterby").attr('placeholder',"Sumber Dana");
					}
					else {
						$("#filterby").attr('placeholder',"Penyedia");
					}
				});



				$( ".dropdown" ).click(function() {  
				    if (  $( ".glyphicon-chevron-up" ).css( "transform" ) == 'none' ){
				        $(".glyphicon-chevron-up").css("transform","rotate(180deg)");
				    } else {
				        $(".glyphicon-chevron-up").css("transform","" );
				    }
				});

				$(function () {
				  $('[data-toggle="tooltip"]').tooltip()
				});
				
				
				// $("#simpanOver").hide();	
				// $("#bcancel").hide();				

				// $("#tambahOver").click(function(){
				// 	$(".isian").attr("disabled", false);
				// 	$(".calder").attr("readonly", true);
				// 	$("#simpanOver").show();
				// 	$("#bcancel").show();
				// });

				// $("#bcancel").click(function(){
				// 	$(".isian").attr("disabled", true);
				// 	$(".calder").attr("readonly", false);
				// 	$("#simpanOver").hide();
				// 	$("#bcancel").hide();
				// });

				$("#simpanbtn").hide();
				$("#batalbtn").hide();
				
				$("#editbtn").click(function(){
					$(".isian").attr("disabled", false);
					$("#simpanbtn").show();
					$("#editbtn").hide();
					$("#batalbtn").show();
				});

				$("#simpanbtn").click(function(){
					$(".isian").attr("disabled", true);
					$("#simpanbtn").hide();
					$("#editbtn").show();
					$("#batalbtn").hide();
				});

				$("#clsbtn").click(function(){
					$(".isian").attr("disabled", true);
					$("#simpanbtn").hide();
					$("#editbtn").show();
					$("#batalbtn").hide();
				});

				$("#batalbtn").click(function(){
					$(".isian").attr("disabled", true);
					$("#simpanbtn").hide();
					$("#editbtn").show();
					$("#batalbtn").hide();
				});
				
				var addDivA = $('#addInputKom');
				var i1 = $('#addInputKom').size();
				
				$('.addNewKomposisi').on('click', function() {
					
				$('<tr><td>A</td><td width="20%"><input type="text" class="form-control" name="tmbhnKom"></td><td width="20%"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDivA);
				i1++;
				// $(".tmbhnKom").editable(); 
				
				return false;
				
				});
				
				var addDivB = $('#addInputKom2');
				var i1 = $('#addInputKom2').size();
				
				$('.addNewKomposisiNon').on('click', function() {
					
				$('<tr><td>A</td><td width="20%"><input type="text" class="form-control" name="tmbhnKom2"></td><td width="20%"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDivB);
				 i1++;
				// $(".tmbhnKom2").editable(); 
				
				return false;
				
				});

				var addDivRac = $('#addTabObat');
				var i2 = $('#addTabObat').size();
				
				$('#tmbhObatRacikan').on('click', function() {
					
				$('<tr><td>Obat Racik 1</td><td style="text-align:center">A,B,C</td><td style="text-align:center">20</td><td style="text-align:center">KG</td><td style="text-align:center">10000</td><td width="20%"><input type="text" class="form-control" name="embalase"></td><td width="20%"><input type="text" class="form-control" name="jf"></td><td width="20%"><input type="text" class="form-control" name="biayaTambahan"></td><td style="text-align:center">30000</td><td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDivRac);
				i2++;
				// $(".tmbhnBy").editable(); 
				
				return false;
				
				});

				var addDivNonRac = $('#addTabObat');
				var i3 = $('#addTabObat').size();
				
				$('#tmbhObatNonRacikan').on('click', function() {
					
				$('<tr><td>Obat Racik 1</td><td style="text-align:center">A,B,C</td><td style="text-align:center">20</td><td style="text-align:center">KG</td><td style="text-align:center">10000</td><td width="20%"><input type="text" class="form-control" name="embalase"></td><td width="20%"><input type="text" class="form-control" name="jf"></td><td width="20%"><input type="text" class="form-control" name="biayaTambahan"></td><td style="text-align:center">30000</td><td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDivRac);
				i3++;
				// $(".tmbhnBy").editable(); 
				
				return false;
				
				});

				var addDivTerima = $('#addinputterima');
				var i = $('#addinputterima').size();
				
				$('.addNewTerima').on('click', function() {
					
					$('<tr><td>Baru</td><td>Baru</td><td><input type="text" class="form-control" name="batch"></td><td><input type="text" class="form-control" name="quantity"></td><td><input type="text" class="form-control" name="diskon"></td><td>Harga</td><td>Total</td><td style="text-align:center;"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDivTerima);
					i++;
					$(".adaanQtyTer").editable(); 
					
					return false;
				
				});

				var addLog = $('#tblby');
				var e = $('#tblby').size();
				
				$('.addNewips').on('click', function() {
				$('<tr><td>'+e+'</td><td>Nama Barang</td><td>200</td><td><input type="text" class="form-control" name="qty"></td><td>Buah<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addLog);
				e++;
				$(".adaanQty").editable(); 
				
				return false;
				
				});

				var addDiv = $('#addinput');
				var i = $('#addinput').size();
				
				$('.addNew').on('click', function() {
				$('<tr><td>Nama Obat</td><td>Penyedia</td><td><input type="text" class="form-control" name="qty"></td><td>Satuan</td><td>HPS</td><td>TOtal</td><td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDiv);
				i++;
				$(".adaanQty").editable(); 
				
				return false;
				
				});

				var addDivBar = $('#addinputAdaBarang');
				var b = $('#addinputAdaBarang').size();
				
				$('.addNewAdaBarang').on('click', function() {
					
				$('<tr><td>Baru</td><td>Baru</td><td><a href="#" class="adaanQtyBar editableform editable-click" data-type="text" data-pk="1" data-original-title="Edit Quantity" id="adaanQtyBarid'+i+'">1</a></td><td>Baru</td><td>Baru</td><td>Baru|&nbsp;<a href="#" class="removeRow" style="float:right;"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDivBar);
				i++;
				$(".adaanQtyBar").editable(); 
				
				return false;
				
				});

				var addDiv1 = $('#addinputReturDis');
				var z = $('#addinputReturDis').size();
				
				$('.addNew1').on('click', function() {
					
				$('<tr><td>Nama Obat</td><td><input type="text" class="form-control" name="qty" id="'+z+'"></td><td>Satuan</td><td>Merek</td><td style="text-align:right;">100</td><td style="text-align:center">20 Desember 2012</td><td style="text-align:center;"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDiv1);
				z++;
				$(".returQty").editable(); 
				
				return false;
				
				});				

				
				//Apotik
				var addDivApo = $('#addinputMintaApoUm');
				var apoUm = $('#addinputMintaApoUm').size();
				
				//data-kosong apo umum
				var addDivKos = $('.addKosong');
				var countKos = $(".addKosong tr").length;
				if (countKos==0) {
				$('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>').appendTo(addDivKos);						
				};

				//data-kosong apo retur
				var addDivApo1 = $('.returObat');
				var countApo1 = $(".returObat tr").length;
				if (countApo1==0) {
				$('<tr><td colspan="7" style="text-align:center" class="dataKosong1">DATA KOSONG</td></tr>').appendTo(addDivApo1);						
				};								
				
				$('.addNewApoUm').on('click', function() {
				var count1 = $(".apo tr").length;
				if (count1==1) {
				$('.dataKosong').closest('tr').remove();
				};				
				$('<tr><td>1</td><td>Baru</td><td>Baru</td><td>Baru</td><td>Baru</td><td width="20%"><input type="text" class="form-control"></td><td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDivApo);
				z++;
				$(".returApoUm").editable(); 
				
				return false;
				
				});


				var addDivApoRet = $('#addinputRetApoUm');
				var apoUmRet = $('#addinputRetApoUm').size();
				
				$('.addNewRetApoUm').on('click', function() {
				var count2 = $(".returObat tr").length;
				if (count2==1) {
				$('.dataKosong1').closest('tr').remove();
				};				
				$('<tr><td>1</td><td>Baru</td><td>Baru</td><td>Baru</td><td>Baru</td><td width="20%"><input type="text" class="form-control"></td><td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDivApoRet);
				z++;
				$(".returRetApoUm").editable(); 
				
				return false;
				
				});

				//logistim retur

				var addDivBarRet = $('#addinputReturDisBar');
				var barID = $('#addinputReturDisBar').size();
				
				$('.addNewBar').on('click', function() {
					
				$('<tr><td>Baru</td><td>Baru</td><td><a href="#" class="returBarQty editableform editable-click" data-type="text" data-pk="1" data-original-title="Edit Quantity" id="adaanQtyid'+barID+'">1</a></td><td>Baru</td><td>Baru</td><td>auwp</td><td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(addDivBarRet);
				barID++;
				$(".returBarQty").editable(); 
				
				return false;
				
				});

				//for scrolling fixed
				$(window).scroll(function(){
					$("#rowfix").css("top",Math.max(100,110-$(this).scrollTop()));
				    var x = $("#rowfix").css("top");
					if( x == "100px"){
						$("#rowfix").animate({ width: "100%", backgroundColor : "#50BFF9", color:"white", marginLeft: "0px" }, 50);
						$("#rowfix a").animate({ color:"white" }, 50);						
						
					}else if(x > "109px"){
						$("#rowfix").animate({ width: "98.55%", backgroundColor : "transparent", color:"black", marginLeft: "10px"}, 100);
						$("#rowfix a").animate({ color:"#428BCA" }, 100);
						

					}

				    
				});


				// var addDiv = $('#addinput');
				// var i = $('#addinput p').size() + 1;


				//  $('#addNew').on('click', function() {
				// $('<p><input type="text" id="p_new" size="40" name="p_new_' + i +'" value="" placeholder="I am New" /><a href="#" id="remNew">Remove</a> </p>').appendTo(addDiv);
				// i++;
				 
				// return false;
				// });
				 
				// $('#remNew').on('click', function() {
				// if( i > 2 ) {
	
				// $(this).parents('p').remove();
				// i--;
				// }
				// return false;
				// });
				

				// $('#remNew').live('click', function() {
				// if( i > 2 ) {
				// $(this).parents('tr').remove();
				// i--;
				// }
				// return false;
				// });
				$(".round-button-tes").click(function(e){
					e.preventDefault();
					$('.round-button-active').removeClass("round-button-active");
					$(this).addClass("round-button-active");
				});

				$("#btnsavejp").hide();
				$("#btneditjp").click(function(e){
					e.preventDefault();
					$('#btnsavejp').show();
					$('#btneditjp').hide();
					$('.jp').attr("readonly", false);
				})
				
				$("#btnsavejp").click(function(e){
					e.preventDefault();
					$('#btneditjp').show();
					$('#btnsavejp').hide();
					$('.jp').attr("readonly", true);
				})	

				$("#btnsaveko").hide();
				$("#btneditko").click(function(e){
					e.preventDefault();
					$('#btnsaveko').show();
					$('#btneditko').hide();
					$('.jasa').attr("readonly", false);
				})
				
				$("#btnsaveko").click(function(e){
					e.preventDefault();
					$('#btneditko').show();
					$('#btnsaveko').hide();
					$('.jasa').attr("readonly", true);
				})	


				$('#tabApo').on('click', 'a[class="removeRow"]', function(e) {
					e.preventDefault();
					$(this).closest('tr').remove();
					var count = $(".apo tr").length;
					var addDivTab = $('.apo');
					if (count==0) {
					$('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>').appendTo(addDivTab);						
					};
				});

				$('#tabRetur').on('click', 'a[class="removeRow"]', function(e) {
					e.preventDefault();
					$(this).closest('tr').remove();
					var count3 = $(".returObat tr").length;
					var addDivTabRet = $('.returObat');
					if (count3==0) {
					$('<tr><td colspan="7" style="text-align:center" class="dataKosong1">DATA KOSONG</td></tr>').appendTo(addDivTabRet);						
					};
				});

				//general table
				
				$('table').on('click', 'a[class="removeRow"]', function(e) {
					e.preventDefault();
					$(this).closest('tr').remove();
					
				});

				$('.tableDT').dataTable({
					"bLengthChange":false,
					"bInfo":false,
					"pageLength" : 20

				});
				$('.tableDTUtama').dataTable({
				    "bFilter": false,
			        "bLengthChange": false,
					"bSortable":true,
					"bInfo":false,
					"pageLength" : 20
				});

				//** Function : create new input box for assistant name
				$('#tambah-asisten').on('click',function(){
					// console.log('syalala');
					$(this).before(
						'<div class="input-group" style="margin-top:10px">'+
						'<input type="text" class="form-control nama-asisten" placeholder="Nama Asisten">'+
						'<span class="input-group-addon"><i class="fa fa-times delete-asisten"></i></span>'+
						'</div>');
					return false;
				});

				//** Function : delete assistant input box
				$('#group-asisten').on('click','i.delete-asisten',function(){
					$(this).closest('div').remove();
					return false;
				});
				//END OF ASISTEN

				$('.tableDTUtamaScroll').dataTable({
				    "bFilter": false,
			        "bLengthChange": false,
					"bSortable":true,
					// "scrollX": true,
					"bInfo":false,
					"orderCellsTop": true,
					"pageLength" : 20
				});

				jQuery('.dataTable').wrap('<div class="dataTables_scroll" />');

				$('.tableDTScroll').dataTable({
				    "bFilter": true,
			        "bLengthChange": false,
					"bSortable":true,
					// "scrollX": true,
					"orderCellsTop": true
				});


				$("#jnsPeng").hide();
				$("#jnsPen").hide();

				$("#ktg").change(function(){
					if (document.getElementById('ktg').value=="Penerimaan") {
										
						$("#jnsPen").show();
						$("#jnsPeng").hide();
						$("#jnsPenge").hide();
					}
					else if (document.getElementById('ktg').value=="Pengeluaran"){			
						
						$("#jnsPeng").show();
						$("#jnsPen").hide();
						$("#jnsPenge").hide();

					}else if(document.getElementById('ktg').value=="Pilih"){


						$("#jnsPeng").hide();
						$("#jnsPen").hide();
						$("#jnsPenge").show();
					}
				});
				$("#kwali").hide();
				$("#jbtan").change(function(){
					if (document.getElementById('jbtan').value=="Dokter") {

						$("#kwali").show();

					}else{
						$("#kwali").hide();
					}
				});
				$("#statu").hide();
				$("#statusMen").change(function(){
					if (document.getElementById('statusMen').value=="Meninggal") {

						$("#statu").show();

					}else{
						$("#statu").hide();
					}
				});

				$("#username").prop('disabled', true);
				$("#password").prop('disabled', true);
				$("#akses").prop('disabled', true);
				$("#akun").change(function(){
					if (document.getElementById('akun').value=="ya") {

						$("#username").prop('disabled', false);
						$("#password").prop('disabled', false);
						$("#akses").prop('disabled', false);
					
					}else if (document.getElementById('akun').value=="tidak"){

						$("#username").prop('disabled', true);
						$("#password").prop('disabled', true);
						$("#akses").prop('disabled', true);
					}else{
						$("#username").prop('disabled', true);
						$("#password").prop('disabled', true);
						$("#akses").prop('disabled', true);
					}

				});

				$('.removeTR').on('click', function(e) {
					e.preventDefault();
					//var $foo = $( foo );
					var a = $(this);
					Confirm.render('Delete Post?','delete_post',a);
					
				});


				$("#show1").hide();
				$("#show2").hide();
				$("#show3").hide();
				$("#show4").hide();
				$("#show5").hide();
				$("#show6").hide();
				$("#show7").hide();
				$("#show8").hide();
				$("#show9").hide();
				$("#show10").hide();

				$("#list1").on('mouseover', function() {
			    	$("#show1").fadeIn(300);
				 }); 
				$("#kotak1").on('mouseleave', function() {  	
					$("#show1").fadeOut(300);
				 }); 

				$("#list2").on('mouseover', function() {
					$("#show2").fadeIn(300);
				 }); 
				$("#kotak2").on('mouseleave', function() {
					$("#show2").fadeOut(300);
				 }); 

				$("#list3").on('mouseover', function() {
			    	$("#show3").fadeIn(300);
				 }); 
				$("#kotak3").on('mouseleave', function() {  	
					$("#show3").fadeOut(300);
				 }); 

				$("#list4").on('mouseover', function() {
					$("#show4").fadeIn(300);
				 }); 
				$("#kotak4").on('mouseleave', function() {
					$("#show4").fadeOut(300);
				 }); 

				$("#list5").on('mouseover', function() {
			    	$("#show5").fadeIn(300);
				 }); 
				$("#kotak5").on('mouseleave', function() {  	
					$("#show5").fadeOut(300);
				 }); 

				$("#list5").on('mouseover', function() {
					$("#show5").fadeIn(300);
				 }); 
				$("#kotak5").on('mouseleave', function() {
					$("#show5").fadeOut(300);
				 }); 

				$("#list6").on('mouseover', function() {
			    	$("#show6").fadeIn(300);
				 }); 
				$("#kotak6").on('mouseleave', function() {  	
					$("#show6").fadeOut(300);
				 }); 

				$("#list7").on('mouseover', function() {
					$("#show7").fadeIn(300);
				 }); 
				$("#kotak7").on('mouseleave', function() {
					$("#show7").fadeOut(300);
				 }); 

				$("#list8").on('mouseover', function() {
			    	$("#show8").fadeIn(300);
				 }); 
				$("#kotak8").on('mouseleave', function() {  	
					$("#show8").fadeOut(300);
				 }); 

				$("#list9").on('mouseover', function() {
					$("#show9").fadeIn(300);
				 }); 
				$("#kotak9").on('mouseleave', function() {
					$("#show9").fadeOut(300);
				 }); 

				$("#list10").on('mouseover', function() {
					$("#show10").fadeIn(300);
				 }); 
				$("#kotak10").on('mouseleave', function() {
					$("#show10").fadeOut(300);
				 }); 
				
				$('.khusus ul').addClass('khususheader');
				//setElementsSpanClass($("div span"));
				setElementsLabel($(".customSearch"));
				setElementsClass($(".dataTables_filter label"));		
				//setElementsSpan($(".customSpan"));
				//$(".taudeh").removeClass("customSpan");
    }, 0);
</script>
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
   TableAdvanced.init();
   // FormValidation.init();
});
</script>
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
</script>

<script type="text/javascript">
	$(document).ready(function(){
		var $input = $('.typeahead');
			$input.typeahead({source:["Arya","Jems","Anet","Febe","Jemly","Erika"], 
			            autoSelect: true}); 
			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

	});  
</script>


<script type="text/javascript">
	function getAllElementsWithAttribute(attribute)
					{
					  var matchingElements = [];
					  var allElements = document.getElementsByTagName('*');
					  for (var i = 0, n = allElements.length; i < n; i++)
					  {
					    if (allElements[i].getAttribute(attribute) !== null)
					    {
					      // Element exists with attribute. Add to array.
					      matchingElements.push(allElements[i]);
					    }
					  }
					  return matchingElements;
					}

	function setElements(vektor){
		for (var i = 0, n = vektor.length; i < n; i++)
	  	{
			vektor[i].setAttribute('data-date-autoclose','true');
		}
	}
	function setElementsSpan(vektor){
		for (var i = 0, n = vektor.length; i < n; i++)
	  	{
			vektor[i].setAttribute('style','background:#EFEFEF;padding:0px 10px 0px 10px;');
		}

	}
	function setElementsLabel(vektor){
		for (var i = 0, n = vektor.length; i < n; i++)
	  	{
			vektor[i].setAttribute('placeholder','Cari');
		}
	}

	function setElementsClass(vektor){
		for (var i = 0, n = vektor.length; i < n; i++)
	  	{
			vektor[i].setAttribute('class','duhdek');
		}
	}
	function setElementsSpanClass(vektor){
		for (var i = 0, n = vektor.length; i < n; i++)
	  	{
			vektor[i].setAttribute('class','customSpan');
		}
	}



</script>



<script type="text/javascript">

	function tambahPengadaanRBA(a, b){
			var x = a;

			$('<tr>'+
				'<td style="text-align:center">1</td>'+
				'<td style="text-align:right">No Rek</td>'+
				'<td>Test</td>'+
				'<td style="text-align:right;">0</td>'+
				'<td>Sat</td>'+
				'<td style="text-align:right;">0</td>'+
				'<td style="text-align:right;">0</td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahPermintaanFarmasi(a, b){
			var x = a;

			$('<tr>'+
				'<td style="text-align:center">1</td>'+
				'<td>nama obat</td>'+
				'<td>satuan</td>'+
				'<td>merek</td>'+
				'<td style="text-align:right;">0</td>'+
				'<td><input type="text" class="input-sm form-control" name="data1" placeholder="0" style="text-align:right;width:190px"></td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahReturFarmasi(a, b){
			var x = a;

			$('<tr>'+
				'<td style="text-align:center">1</td>'+
				'<td>Nama Obat</td>'+
				'<td>satuan</td>'+
				'<td>merek</td>'+
				'<td style="text-align:right;">0</td>'+
				'<td><input type="text" class="input-sm form-control" name="data1" placeholder="0" style="text-align:right;width:190px"></td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahPermintaanLogistik(a, b){
			var x = a;

			$('<tr>'+
				'<td style="text-align:center">1</td>'+
				'<td>Nama Barang</td>'+
				'<td>satuan</td>'+
				'<td>merek</td>'+
				'<td style="text-align:right;">0</td>'+
				'<td><input type="text" class="input-sm form-control" name="data1" placeholder="0" style="text-align:right;width:190px"></td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahTagihanTindakanOperasi(a, b){
			var x = a;

			$('<tr>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td style="text-align:center;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahTagihanKamarBpjs(a, b){
			var x = a;

			$('<tr>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahTagihanAkomodasiBpjs(a, b){
			var x = a;

			$('<tr>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahTagihanTidakanPerawatanBpjs(a, b){
			var x = a;

			$('<tr>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td style="text-align:center;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahTagihanTindakanPenunjangBpjs(a, b){
			var x = a;

			$('<tr>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td style="text-align:center;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahTagihanTindakanOperasiBpjs(a, b){
			var x = a;

			$('<tr>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td>a</td>'+
				'<td style="text-align:center;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:right;">a</td>'+
				'<td style="text-align:center">'+
					'<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
				'</td>'+
			'</tr>').appendTo(x);
			return false;
	};

	function tambahTabelRetur(a, b){
			var x = a;

			$('<tr><td>1.</td><td>Paramex</td><td><input type="text" class="form-control"></td><td>Pil</td><td style="text-alignright">200000</td><td style="text-alignright">1200000</td><td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(x);
			return false;
	};
	
	function tambahTabel(a, b){
			var x = a;

			$('<tr><td></td><td></td><td></td><td><input type="text" class="form-control"> </td><td></td><td></td><td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(x);
			return false;
	};

	function tambahTabelKasir(a, b){
			var x = a;

			$('<tr><td></td><td></td><td></td><td></td><td></td><td></td><td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(x);
			return false;
	};

	function tambahTabelKasirBPJS(a, b){
			var x = a;

			$('<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(x);
			return false;
	};

	function tambahMenu(a, b){
			var x = a;

			$('<tr><td></td><td><input type="text" class="form-control" id="namaMenu" name="namaMenu"></td><td><select class="form-control"><option selected>Pilih</option><option value="Makan">Makan</option><option value="Snack">Snack</option></select></td><td><input type="text" class="form-control" id="hrgMenu" name="hrgMenu"></td><td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(x);
			return false;
	};
	function tambahTindakan(a, b, c, d){
			var x = a;
			var y = c;
			var z = d;
			$('<div class="form-group"><label class="control-label col-md-3"></label><div class="col-md-3"><input type="text" class="typeahead form-control" autocomplete="off" spellcheck="false" placeholder="Cari Tindakan"></div><div class="col-md-1" style="margin-left:-20px"><a href="#" class="removeBar"><i class="glyphicon glyphicon-remove"></i></a></div></div>').appendTo(x);
			$('<div class="form-group"><label class="control-label col-md-3"></label><div class="col-md-3"><input type="text" class="form-control" id="tarifMediko" name="tarifMediko" placeholder="Tarif" readonly ></div><div class="col-md-1" style="margin-left:-20px"><a href="#" class="removeBar"><i class="glyphicon glyphicon-remove"></i></a></div></div>').appendTo(y);
			$('<div class="form-group"><label class="control-label col-md-3"></label><div class="col-md-3"><input type="text" class="form-control" id="onfakturMediko" name="onfakturMediko" placeholder="On Faktur" ></div><div class="col-md-1" style="margin-left:-20px"><a href="#" class="removeBar"><i class="glyphicon glyphicon-remove"></i></a></div></div>').appendTo(z);
			return false;
	};

	function tambahTransaksi(a,b){
			var x = a;

			$('<tr><td></td><td></td><td></td><td></td><td></td><td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a><a href="#detailTransaksi" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail Transaksi"></i></a></td></tr>').appendTo(x);
			return false;	
	}

	function tambahBed(a,b,d){
			var x = a;	
			$('<tr><td><input type="text" class="form-control" id="bed'+d+'" name="bed'+d+'" placeholder="Bed '+d+'"></td><td><input type="text" class="form-control" id="desk'+d+'" name="desk'+d+'" placeholder="Deskripsi"></td><td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>').appendTo(x);
			return false;	
	}

</script>

<script>
			// (function() {
			// 	var bttn = document.getElementById('trigger');
				


			// 	// make sure..
			// 	bttn.disabled = false;

			// 	bttn.addEventListener( 'click', function() {
			// 		// simulate loading (for demo purposes only)
			// 		classie.add( bttn, 'active' );
			// 		// setTimeout( function() {

			// 			classie.remove( bttn, 'active' );
						
			// 			// create the notification
			// 			var notification = new NotificationFx({
			// 				message : '<p>DATA BERHASIL DITAMBAHKAN</p>',
			// 				layout : 'growl',
			// 				effect : 'genie',
			// 				type : 'notice', // notice, warning or error
			// 				onClose : function() {
			// 					bttn.disabled = false;
			// 				}
			// 			});

			// 			// show the notification
			// 			notification.show();

			// 		// }, 1200 );
					
			// 		// disable the button (for demo purposes only)
			// 		this.disabled = true;
			// 	} );
			// })();
</script>

<script src="<?php echo base_url();?>metronic/assets/notif/js/classie.js"></script>
<script src="<?php echo base_url();?>metronic/assets/notif/js/notificationFx.js"></script>


<script type="text/javascript">
	var maxHeight = 500;

	$(function(){

	    $("#nav > li.khusus").hover(function() {
	    
	         var $container = $(this),
	             $list = $container.find("ul"),
	             //$anchor = $container.find("a"),
	             height = $list.height() * 1.1,       // make sure there is enough room at the bottom
	             multiplier = height / maxHeight;     // needs to move faster if list is taller
	        
	        // need to save height here so it can revert on mouseout            
	        $container.data("origHeight", $container.height());
	        
	        // so it can retain it's rollover color all the while the dropdown is open
	        //$anchor.addClass("hover");
	        
	        // make sure dropdown appears directly below parent list item    
	        $list
	            .show()
	            .css({
	                paddingTop: $container.data("origHeight")
	            });
	        
	        // don't do any animation if list shorter than max
	        if (multiplier > 1) {
	            $container
	                .css({
	                    height: maxHeight,
	                    overflow: "hidden",
	                    "z-index":"900"
	                })
	                .mousemove(function(e) {
	                	$list.css("z-index","-50");
	                    var offset = $container.offset();
	                    var relativeY = ((e.pageY - offset.top) * multiplier) - ($container.data("origHeight") * multiplier);
	                    if (relativeY > $container.data("origHeight")) {
	                        $list.css("top", -relativeY + $container.data("origHeight"));
	                    };
	                });
	        }
	        
	    }, 

	    function() {
	    
	        var $el = $(this);
	        
	        // put things back to normal
	        $el
	            .height($(this).data("origHeight"))
	            .find("ul")
	            .css({ top: 0 })
	            .hide()
	            .end()
	            .find("a")
	            .removeClass("hover");
	    
	    });
	    
	    
	});
</script>

<script type="text/javascript">
	function CustomAlert(){
		this.render = function(dialog){
			var winW = window.innerWidth;
		    var winH = window.innerHeight;
			var dialogoverlay = document.getElementById('dialogoverlay');
		    var dialogbox = document.getElementById('dialogbox');
			dialogoverlay.style.display = "block";
		    dialogoverlay.style.height = winH+"px";
			dialogbox.style.left = (winW/2) - (550 * .5)+"px";
		    dialogbox.style.top = "100px";
		    dialogbox.style.display = "block";
			document.getElementById('dialogboxhead').innerHTML = "Acknowledge This Message";
		    document.getElementById('dialogboxbody').innerHTML = dialog;
			document.getElementById('dialogboxfoot').innerHTML = '<button onclick="Alert.ok()">OK</button>';
		}
		this.ok = function(){
			document.getElementById('dialogbox').style.display = "none";
			document.getElementById('dialogoverlay').style.display = "none";
		}
	}
	var Alert = new CustomAlert();
	function deletePost(ddd){
		//var db_id = id.replace("post_", "");
		//var x = document.getElementById(id);
		// Run Ajax request here to delete post from database
		//document.body.removeChild(document.getElementById(id));

		//$('.post_1').closest('tr').remove();
		//var $x = $ddd;
		//$x.remove();						
							
	}
	/*function myAlert(){
		var notification = new NotificationFx({
							message : '<p>DATA BERHASIL DITAMBAHKAN</p>',
							layout : 'growl',
							effect : 'genie',
							type : 'notice', // notice, warning or error
							onClose : function() {
								bttn.disabled = false;
							}
						});

						// show the notification
						notification.show();
	}*/
	function CustomConfirm(){
		this.render = function(dialog,op,a){

			var winW = window.innerWidth;
		    var winH = window.innerHeight;
			var dialogoverlay = document.getElementById('dialogoverlay');
		    var dialogbox = document.getElementById('dialogbox');
			dialogoverlay.style.display = "block";
		    dialogoverlay.style.height = winH+"px";
			dialogbox.style.left = (winW/2) - (550 * .5)+"px";
		    dialogbox.style.top = "100px";
		    dialogbox.style.display = "block";
			//a.closest('tr').remove();
			//var ab = $(a);
			document.getElementById('dialogboxhead').innerHTML = "Confirm that action";
		    document.getElementById('dialogboxbody').innerHTML = dialog;
			document.getElementById('dialogboxfoot').innerHTML = '<a href="#" class="yes" style="color:white"><button class="btn btn-success">Yes</button></a> <button class="btn btn-danger" onclick="Confirm.no()">No</button>';
			$('.yes').on('click', function(e) {
				e.preventDefault();
				//var $foo = $( foo );
				var u = a;
				Confirm.yes('delete_post',u);		
			});

		}
		this.no = function(){
			document.getElementById('dialogbox').style.display = "none";
			document.getElementById('dialogoverlay').style.display = "none";
		}
		this.yes = function(op,a){
			if(op == "delete_post"){
				a.closest('tr').remove();
			}
			document.getElementById('dialogbox').style.display = "none";
			document.getElementById('dialogoverlay').style.display = "none";
		}
	}
	var Confirm = new CustomConfirm();
	// Set the Options for "Bloodhound" Engine
        //         var my_Suggestion_class = new Bloodhound({
        //             limit: 10,
        //             datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        //             queryTokenizer: Bloodhound.tokenizers.whitespace,
        //             local: $.map(country_list, function(item) {
        //                 //console.log(item);
        //                 return {value: item};
        //             })
        //         });

        // //Initialize the Suggestion Engine
        //         my_Suggestion_class.initialize();

        //         var typeahead_elem = $('.typeahead');
        //         typeahead_elem.typeahead({
        //             hint: true,
        //             highlight: true,
        //             minLength: 1
        //         },
        //         {
        //             name: 'value',
        //             displayKey: 'value',
        //             source: my_Suggestion_class.ttAdapter(),
        //             templates: {
        //                 empty: [
        //                     '<div class="noitems">',
        //                     'No Items Found',
        //                     '</div>'
        //                 ].join('\n')
        //             }
        //         });
        //         //Get the Typeahead Value on Following Events
        //         $('input').on([
        //             'typeahead:initialized',
        //             'typeahead:initialized:err',
        //             'typeahead:selected',
        //             'typeahead:autocompleted',
        //             'typeahead:cursorchanged',
        //             'typeahead:opened',
        //             'typeahead:closed'
        //         ].join(' '), function(x) {
        //             //console.log(this.value); 
        //         });
        //         $('input').on([
        //             'typeahead:initialized',
        //             'typeahead:initialized:err',
        //             'typeahead:selected',
        //             'typeahead:autocompleted',
        //             'typeahead:cursorchanged',
        //             'typeahead:opened',
        //             'typeahead:closed'
        //         ].join(' '), function(x) {
        //             console.log(this.value); 
        //         })
	</script>


<script src="<?php echo base_url();?>metronic/assets/js/main.js"></script>
<script type="text/javascript">
	(function () {

    var scrollHandle = 0,
        scrollStep = 5,
        //parent = $(window);
        parent = $('#pannerku');

    //Start the scrolling process
    $(".panner").on("mouseenter", function () {
        var data = $(this).data('scrollModifier'),
            direction = parseInt(data, 10);        

        //$(this).addClass('active');

        startScrolling(direction, scrollStep);
    });

    //Kill the scrolling
    $(".panner").on("mouseleave", function () {
        stopScrolling();
        //$(this).removeClass('active');
    });

    //Actual handling of the scrolling
    function startScrolling(modifier, step) {
        if (scrollHandle === 0) {
            scrollHandle = setInterval(function () {
                var newOffset = parent.scrollLeft() + (scrollStep * modifier);

                parent.scrollLeft(newOffset);
            }, 10);
        }
    }

    function stopScrolling() {
        clearInterval(scrollHandle);
        scrollHandle = 0;
    }

	}());
</script>

<script type="text/javascript">
	function myAlert(text) {
		// simulate loading (for demo purposes only)
		// setTimeout( function() {
		var m = '<p>'+text+'</p>';
		// create the notification
		var notification = new NotificationFx({
			message : m,
			layout : 'growl',
			effect : 'genie',
			type : 'notice', // notice, warning or error
		});

		// show the notification
		notification.show();

		// }, 1200 );
		// disable the button (for demo purposes only)
	}
</script>
<!-- END JAVASCRIPTS -->