<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Laporan_Inventori'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>INVENTORI BARANG</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong><?php echo strtoupper($nama_dept) ?> RS DATU SANGGUL RANTAU</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Laporan per </strong> 
	<?php echo $awal; ?> <strong>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th width="20">No.</th>
					<th colspan="2"> Nama Barang </th>
					<th colspan="2"> Merek </th>
					<th colspan="2"> Harga </th>
					<th colspan="2"> Stok </th>
					<th colspan="2"> Satuan </th>
					<th colspan="2"> Tahun Pengadaan</th>
					<th colspan="2"> Sumber Dana</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					if (isset($inventoribarang)) {
						if (!empty($inventoribarang)) {
							$i = 1;
							foreach ($inventoribarang as $value) {
								echo '<tr>
										<td>'.($i++).'</td>
										<td colspan="2">'.$value['nama'].'</td>
										<td colspan="2">'.$value['nama_merk'].'</td>
										<td colspan="2">'.$value['harga'].'</td>
										<td colspan="2">'.$value['stok'].'</td>
										<td colspan="2">'.$value['satuan'].'</td>
										<td colspan="2">'.$value['tahun_pengadaan'].'</td>
										<td colspan="2">'.$value['sumber_dana'].'</td>
									</tr>';
							}
						}
					}
				?>
			</tbody>
		</table>
	</div>

</body>
</html>
