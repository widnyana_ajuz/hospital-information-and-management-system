<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Invoicenonbpjs extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_invoicenonbpjs");
		$data['page_title'] = "Invoice Non BPJS";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		redirect('laboratorium/homelab');
	}

	public function invoice($no_invoice){
		$data['content'] = 'tagihan/invoicenonbpjs';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$invoice = $this->m_invoicenonbpjs->get_visit_id($no_invoice);
		$visit_id = $invoice['visit_id'];
		$sub_visit = $invoice['sub_visit'];
		$data['visit_id'] = $visit_id;
		$data['sub_visit'] = $invoice['sub_visit'];
		$data['no_invoice'] = $no_invoice;
		$data['invoice'] = $invoice;
		$data['dept_id'] = $this->m_invoicenonbpjs->get_deptid('LABORATORIUM');

		$pasien = $this->m_invoicenonbpjs->get_data_pasien($visit_id);
		$data['pasien'] = $pasien;

		$data['tagihantunjang'] = $this->m_invoicenonbpjs->get_tagihantunjang($sub_visit);
		$data['tagihanadmisi'] = $this->m_invoicenonbpjs->get_tagihanadmisi($no_invoice);
		$data['tindakan'] = $this->m_invoicenonbpjs->get_tindakan($no_invoice);
		
		$this->load->view('base/operator/template', $data);
	}

	public function save_tagihan(){
		foreach ($_POST as $data) {
			$insert = $data;
		}

		$check = $this->m_invoicenonbpjs->check_tagihan($insert['penunjang_detail_id']);
		if(count($check)==0){
			$insert['petugas_input'] = $this->session->userdata('session_operator')['petugas_id'];
			$ins = $this->m_invoicenonbpjs->save_tagihan($insert);
		}

		$update['on_faktur'] = $insert['on_faktur'];
		$up = $this->m_invoicenonbpjs->update_penunjang_detail($insert['penunjang_detail_id'],$update);
		$up = $this->m_invoicenonbpjs->update_tagihan_penunjang($insert['penunjang_detail_id'],$update);

		header("Content-Type: application/json");
		echo json_encode($insert);
	}
}
