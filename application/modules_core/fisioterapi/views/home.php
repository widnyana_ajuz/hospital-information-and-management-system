<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>fisioterapi/homelab">FISIOTERAPI</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">List Pasien APS</a>
	</li>
</div>

<input type="hidden" id="dept_id" value="<?php echo $dept_id; ?>">
<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	    <li class="active"><a href="#list" class="cl" data-toggle="tab">List Pasien APS</a></li>
	    <li><a href="#permintaan" class="cl" data-toggle="tab">List Pasien Rujukan</a></li>
	    <li><a href="#hasil" class="cl" data-toggle="tab">Hasil Pemeriksaan Lab</a></li>
	    <li><a href="#tagihan" class="cl" data-toggle="tab">Tagihan</a></li>
	   	<li><a href="#farmasi" class="cl" data-toggle="tab">Farmasi</a></li>
	    <li><a href="#logistik" class="cl" data-toggle="tab">Logistik</a></li>
	    <li><a href="#laporan" class="cl" data-toggle="tab">Laporan</a></li>
	    <li><a href="#master" class="cl" data-toggle="tab">Master</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="list">
	        <form class="form-horizontal" method="POST" id="search_aps">
	           	<div class="search">
					<label class="control-label col-md-3">Nama Pasien / Rekam Medis <span class="required" style="color : red">* </span>
					</label>
					<div class="col-md-4" style="width:420px;">		
						<input type="text" id="input_aps" class="form-control" placeholder="Masukkan Nama atau Nomor Rekam Medis Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>	
			</form>
			<hr class="garis">

			<div class="portlet box red">
				<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<input type="hidden" id="jmlaps" value="<?php echo count($listaps); ?>">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tableAPS">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal</th>
								<th>#Rekam Medis</th>
								<th> Nama Pasien</th>
								<th>Jenis Kelamin</th>
								<th>Jenis Pemeriksaan</th>
								<th width="80">Action</th>
							</tr>
						</thead>	
						<tbody>
							<?php
								$i = 0;

								foreach($listaps as $data){
									$tgl = strtotime($data['waktu']);
									$hasil = date('d F Y', $tgl); 
									echo '
										<tr>
											<td align="center">'.++$i.'</td>
											<td style="text-align:center">'.$hasil.'</td>
											<td style="text-align:right">'.$data['rm_id'].'</td>
											<td>'.$data['nama'].'</td>										
											<td>'.$data['jenis_kelamin'].'</td>
											<td>'.$data['jenis_periksa'].'</td>
											<td style="text-align:center"><a href="#tambahPeri" onclick="pemeriksaan(&quot;'.$data['penunjang_id'].'&quot;)" class="tambahlab" data-toggle="modal" ><i class="glyphicon glyphicon-check"></i></a></td>										
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="tambahHasil" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-600px">
			<div class="modal-dialog">
        		<div class="modal-content" style="width:1200px">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Tambah Hasil Pemeriksaan</h3>
        			</div>	
        	    
				    <div class="dropdown">
			            <div id="titleInformasi">Identitas Pasien</div>
			            <div class="btnBawah" id="btnBawahhasil"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			        </div>
			        <form class="form-horizontal" role="form">
			        	<br>
				        <div class="informasi" id="infohasillab">
				        	<div class="form-group">
								<label class="control-label col-md-3">No. Rekam Medis :</label>
								<label type="text" class="control-label col-md-3"  name="noRm" id="phasil_rm" placeholder="No Rekam Medis" >00000000</label>
								<label class="control-label col-md-2">Jenis Kelamin :</label>
								<label type="text"  class="control-label col-md-2" name="jk" d="phasil_kelamin" placeholder="Jenis Kelamin" >Pria </label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Nama Pasien :</label>
								<label type="text"  class="control-label col-md-3" name="nama" id="phasil_nama" placeholder="Nama Pasien" >Putu </label>
								<label class="control-label col-md-2">Tanggal Lahir :</label>
								<label type="text"  class="control-label col-md-2" name="ttl" id="phasil_tlahir" placeholder="Tanggal Lahir" >30 Mei 1977</label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Alamat :</label>
								<label type="text" class="control-label col-md-3" name="alamat" id="phasil_alamat" placeholder="Alamat" >Bali</label>
								<label class="control-label col-md-2">Umur :</label>
								<label type="text"  class="control-label col-md-2" name="umur" id="phasil_umur" placeholder="Umur" >50 tahun</label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Pengirim :</label>
								<label type="text" class="control-label col-md-2" name="pengirim" id="phasil_pengirim" placeholder="Pengirim" >Jems</label>
								
							</div>
							<br>						
			            </div>
			        </form>

			        <div class="dropdown">
			            <div id="titleInformasi">Hasil Periksa</div>
			            <div class="btnBawah" id="btnBawahPeriksa"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			        </div>

			        <form class="form-horizontal" role="form" id="submit_periksa">
			        	<div class="informasi" id="hahaha" >
							<div class="form-group">
								<label class="control-label col-md-3">Penunjang</label>
								<div class="col-md-2">
									<input type="hidden" id="phasil_penunjang_id">
									<input type="text" class="form-control" id="phasil_penunjang" name="penunjang" placeholder="Penunjang" readonly>
								</div>
							</div>
				
							<div class="form-group">
								<label class="control-label col-md-3">Pemeriksa</label>
								<div class="col-md-3">	
									<input type="hidden" id="paramedis_id">
									<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" required>
								</div>
		        			</div>

							<div class="form-group">
								<label class="control-label col-md-3">Status</label>
								<div class="col-md-3">
									<select class="form-control select" name="pilihpemeriksa" id="phasil_status" required>
										<option value="" selected>Pilih Status</option>
										<option value="Selesai Sebagian">Selesai Sebagian</option>
										<option value="Selesai">Selesai</option>
									</select>
								</div>
							</div>
						</div>	

						<hr class="garis" style="margin-left:30px; margin-right:30px;">		
														
						<div class="tabelinformasi">	
							<div class="portlet-body" style="margin: 20px 30px 10px 10px">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="info">
											<th width="10px">No.</th>
											<th>Jenis Pemeriksaan</th>
											<th>Hasil</th>
											<th>Nilai Normal</th>
											<th>On Faktur</th>
											<th>Keterangan</th>
										</tr>
									</thead>
									<tbody id="tbody_periksa">
										
									</tbody>
								</table>
							</div>
						</div>

						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:980px">
							<span style="background-color:white	; padding:0px 10px 0px 10px;">
								<button type="reset"  data-dismiss="modal" class="btn btn-danger">Batal</button> &nbsp;
								<button  type="submit" class="btn btn-success">Simpan</button> 
							</span>
						</div>
						<br>	
				    </form>
			   	</div>
			</div>
		</div>

		<div class="modal fade" id="view" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-600px">
			<div class="modal-dialog">
        		<div class="modal-content" style="width:1200px">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Lihat Hasil Pemeriksaan</h3>
        			</div>	
        	    
				    <div class="dropdown">
			            <div id="titleInformasi">Identitas Pasien</div>
			            <div class="btnBawah" id="btnBawahhasil"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			        </div>
			        <form class="form-horizontal" role="form" >
			        	<br>
			        	  <div class="informasi" id="infohasillab">
				        	<div class="form-group">
								<label class="control-label col-md-3">No. Rekam Medis :</label>
								<label type="text" class="control-label col-md-3"  name="noRm" placeholder="No Rekam Medis" id="lhasil_rm"></label>
								<label class="control-label col-md-2">Jenis Kelamin :</label>
								<label type="text"  class="control-label col-md-2" name="jk" placeholder="Jenis Kelamin" id="lhasil_jenis" ></label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Nama Pasien :</label>
								<label type="text"  class="control-label col-md-3" name="nama" id="lhasil_nama" placeholder="Nama Pasien" > </label>
								<label class="control-label col-md-2">Tanggal Lahir :</label>
								<label type="text"  class="control-label col-md-2" name="ttl" id="lhasil_tlahir" placeholder="Tanggal Lahir" >30 Mei 1977</label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Alamat :</label>
								<label type="text" class="control-label col-md-3" name="alamat" id="lhasil_alamat" placeholder="Alamat" >Bali</label>
								<label class="control-label col-md-2">Umur :</label>
								<label type="text"  class="control-label col-md-2" name="umur" id="lhasil_umur" placeholder="Umur" >50 tahun</label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Pengirim :</label>
								<label type="text" class="control-label col-md-2" name="pengirim" id="lhasil_pengirim" placeholder="Pengirim" >Jems</label>
								
							</div>
							<br>						
			            </div>
				        
			        </form>

			        <div class="dropdown">
			            <div id="titleInformasi">Hasil Periksa</div>
			            <div class="btnBawah" id="btnBawahPeriksa"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			        </div>
			        <form class="form-horizontal" role="form">
			        	<div class="informasi" id="infohasilperiksa" >
			        		
							<div class="form-group">
								<label class="control-label col-md-3">Penunjang</label>
								<div class="col-md-2">
									<input type="text" class="form-control" name="penunjang" id="lhasil_penunjang" placeholder="Penunjang" readonly>
									
								</div>
							</div>
				
							<div class="form-group">
								<label class="control-label col-md-3">Pemeriksa</label>
								<div class="col-md-3">
									<input type="text" class="form-control" readonly name="pilihpemeriksa" id="lhasil_pemeriksa">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Status</label>
								<div class="col-md-3">
									<input type="text" class="form-control" readonly name="pilihpemeriksa" id="lhasil_status">
								</div>
							</div>
						</div>	
						<hr class="garis" style="margin-left:30px; margin-right:30px;">										
						<div class="tabelinformasi">	
							<div class="portlet-body" style="margin: 20px 30px 10px 10px">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="info">
											<th width="10px">No.</th>
											<th>Jenis Pemeriksaan</th>
											<th>Hasil</th>
											<th>Nilai Normal</th>
											<th>Keterangan</th>
										</tr>
									</thead>
									<tbody id="tbody_lihathasil">
										
									</tbody>
								</table>
							</div>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span style="background-color:white; padding:0px 10px 0px 10px;" id="inAction">
								<!-- <button type="reset" class="btn btn-warning" data-dismiss="modal">Kembali</button>
								
								<button type="reset" class="btn btn-info" data-dismiss="modal">Cetak</button> -->
							</span>
						</div>
						<br><br>	
				    </form>
			   	</div>
			</div>
		</div>

		<div class="modal fade" id="tambahPeri" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-600px">
        	<div class="modal-dialog">
        		<div class="modal-content" style="width:1200px">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Tambah Jenis Pemeriksaan</h3>
        			</div>	
        			<div class="modal-body">
        			<form class="form-horizontal" method="POST" id="submit_pemeriksaan">
        				<div class="form-group" style="padding-left:10px;">
        					<input type="hidden" id="penunjang_id">
							<label class="control-label col-md-2">Kelas Pemeriksaan</label>
							<div class="col-md-3">
								<select class="form-control select" name="pilihpemeriksa" id="kelas_periksa" required>
									<option value="" selected>Pilih Kelas</option>
									<option value="III">III</option>
									<option value="II">II</option>
									<option value="I">I</option>
									<option value="Utama">Utama</option>
									<option value="VIP">VIP</option>
								</select>
							</div>
						</div>

        				<div class="navigation" style="margin-left: 10px" >
						 	<ul id="tabs" class="nav tablab nav-tabs" data-tabs="tabs">
						 		<?php
						 			foreach ($jenisperiksa as $key => $data) {
						 				if($key==0)
						 					echo'
						 						<li class="active"><a href="#'.$data['kategori'].'" class="cl" data-toggle="tab">'.$data['kategori'].'</a></li>			
						 					';	
						 				else
							 				echo'
							 					<li><a href="#'.$data['kategori'].'" class="cl" data-toggle="tab">'.$data['kategori'].'</a></li>			
							 				';
						 			}
						 		?>
							</ul>
							
							<div id="my-tab-content" class="tab-content" >
							<?php
								foreach ($jenisperiksa as $data) {
									echo '
										
								        <div class="tab-pane" id="'.$data['kategori'].'" >
								        	<table class="table table-striped table-bordered table-hover">
										';
										foreach ($tindakan as $key => $value) {
											if(intval($value['nomor'])==0)
												echo '<tr>';

											if(intval($value['nomor'])%4==0 && intval($value['nomor'])!=0)
												echo '</tr><tr>';

											if($value['kategori']==$data['kategori']){
												echo'
												<td>
													<label class="checkbox-inline"><input type="checkbox" name="jenisperiksa" value="'.$value['nama_tindakan'].'">'.$value['nama_tindakan'].'</label>
												</td>';
											}
										}

									echo'
											</tr>
											</table>
									    </div>
									';
								}
							?>
							</div>
        				</div>

        			</div>	
      				<div class="modal-footer">
 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
 			       		<button type="submit" class="btn btn-success">Tambah</button>
			      	</div>

			      	</form>
        		</div>        	
        	</div>
        </div>

        <div class="tab-pane" id="hasil">
        	<form class="form-horizontal" id="search_hasil">
	           	<div class="search">
					<label class="control-label col-md-3">Nama Pasien / Rekam Medis <span class="required" style="color : red">* </span>
					</label>
					<div class="col-md-4" style="width:420px;">		
						<input type="text" id="input_hasil" class="form-control" placeholder="Masukkan Nama atau Nomor Rekam Medis Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>	
			</form>
			<hr class="garis">
			
			<div class="portlet-body" style="margin: 10px 10px 0px 10px">
				<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tableHasil">
					<thead>
						<tr class="info">
							<th width="20">No.</th>
							<th>Tanggal</th>
							<th>#Rekam Medis</th>
							<th>Nama</th>
							<th>Pengirim</th>
							<th>Unit Pengirim</th>
							<th>Status</th>
							<th width="100">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no = 0;
							foreach ($listperiksa as $data) {
								$tgl = strtotime($data['waktu']);
								$date = date('d F Y', $tgl);
								echo '
									<tr>
										<td align="center">'.++$no.'</td>
										<td>'.$date.'</td>
										<td>'.$data['rm_id'].'</td>
										<td>'.$data['nama'].'</td>';

								if(is_null($data['nama_petugas'])){
									echo '<td>'.$data['pengirim'].'</td>';
								}else{
									echo '<td>'.$data['nama_petugas'].'</td>';
								}

								echo'<td>'.$data['nama_dept'].'</td>';
										
								if($data['status']=="PROSES") echo'<td>Belum</td>';
								else echo'<td>'.$data['status'].'</td>';

								echo'<td style="text-align:center">';
								if($data['status']=="Selesai"){
									echo '<a href="#view" data-toggle="modal" onclick="LihatHasil(&quot;'.$data['penunjang_id'].'&quot;)"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Lihat Detail"></i></a>';
								}else{
									echo '
										<a href="#tambahHasil" data-toggle="modal" onclick="Periksa(&quot;'.$data['penunjang_id'].'&quot;)"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Pemeriksaan"></i></a>
											<a href="#view" data-toggle="modal" onclick="LihatHasil(&quot;'.$data['penunjang_id'].'&quot;)"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Lihat Detail"></i></a>
									';
								}
								echo '</td>										
									</tr>
								';
							}
						?>
						
					</tbody>
				</table>
			</div>
		</div>

		<div class="tab-pane" id="permintaan"> 
			<form class="form-horizontal" id="search_rujuk">
				<div class="search">
					<label class="control-label col-md-3">Nama Pasien / Rekam Medis <span class="required" style="color : red">* </span>
					</label>
					<div class="col-md-4" style="width:420px;">		
						<input type="text" class="form-control" id="input_searchRujuk" placeholder="Masukkan Nama atau Nomor Rekam Medis Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>	
			</form>
			<hr class="garis">
            <div>
            	<div class="portlet-body" style="margin: 10px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_rujuk">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Unit</th>
								<th>Tanggal </th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Dokter Pengirim</th>
								<th>Jenis Pemeriksaan</th>
								<th width="80">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 0;
								foreach ($listrujuk as $data) {
									$tgl = strtotime($data['waktu']);
									$hasil = date('d F Y', $tgl);
									echo '
										<tr>
											<td align="center">'.++$no.'</td>
											<td>'.$data['nama_dept'].'</td>
											<td style="text-align:center">'.$hasil.'</td>
											<td style="rext-align:right">'.$data['rm_id'].'</td>
											<td>'.$data['nama'].'</td>
											<td>'.$data['nama_petugas'].'</td>
											<td>'.$data['jenis_periksa'].'</td>
											<td style="text-align:center; cursor:pointer;">
												<a href="#tambahPeri" onclick="pemeriksaan(&quot;'.$data['penunjang_id'].'&quot;)" class="tambahlab" data-toggle="modal"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Tambah JenisPemeriksaan"></i></a>
											</td>
										</tr>
									';
								}
							?>
						</tbody>
					</table>												
				</div>
            </div>
		</div>
        
        <div class="tab-pane" id="farmasi">
        	<div class="dropdown" id="btnBawahInventori" >
	            <div id="titleInformasi">Inventori</div>
	            <div id="btnBawahInventori" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="infoInventori">
				<div class="form-group">
	            	<form class="form-horizontal informasi" role="form" method="post" id="submitfilterfarmasiunit">
		            	<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
						<div class="col-md-2" style="width:200px">
							<select class="form-control select" name="filterInv" id="filterInv">
								<option value="" selected>Pilih</option>
								<option value="jenis">Jenis Obat</option>
								<option value="merek">Merek</option>
								<option value="nama">Nama Obat</option>							
							</select>	
						</div>
						<div class="col-md-2" style="margin-left:-15px; width:200px;" >
							<input type="text" class="form-control" id="filterby" name="valfilter" placeholder="Value"/>
						</div>
						<div class="col-md-1" >
							<button type="submit" class="btn btn-warning">FILTER</button> 
						</div>
					</form>
					<div class="col-md-1" >
						<button class="btn btn-danger" id="expired">EXPIRED</button> 
					</div>
					<div class="col-md-1" >
						<button class="btn btn-warning" id="expired3">EX 3 BULAN</button>
					</div>
					<div class="col-md-1" style="margin-left: 20px;">
						<button class="btn btn-warning" id="expired6">EX 6 BULAN</button>
					</div>
				</div>
				<br><br>
				<div class="form-group" >
					<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabelinventoriunit">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th> Nama Obat </th>
									<th> No Batch </th>
									<th> Harga Jual </th>
									<th> Merek </th>
									<th> Stok</th>
									<th> Satuan </th>
									<th width="200"> Tanggal Kadaluarsa </th>
									<th width="100"> Action </th>								
								</tr>
							</thead>
							<tbody id="tbodyinventoriunit">
								<?php  
									if (isset($obatunit)) {
										$i = 1;
										foreach ($obatunit as $value) {
											$tgl = DateTime::createFromFormat('Y-m-d', $value['tgl_kadaluarsa']);
											echo '<tr>'.
												'<td>'.($i++).'</td>'.
												'<td>'.$value['nama'].'</td>'.
												'<td>'.$value['no_batch'].'</td>'.
												'<td>'.$value['harga_jual'].'</td>'.
												'<td>'.$value['nama_merk'].'</td>'.
												'<td>'.$value['total_stok'].'</td>'.
												'<td>'.$value['satuan'].'</td>'.								
												'<td>'.$tgl->format('d F Y').'</td>'.
												'<td><a href="#" class="inoutobat" data-toggle="modal" data-target="#inout"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'.
												'<a href="#edInvenBer" data-toggle="modal" class="printobat"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>'.
												'<input type="hidden" class="barangmerk_id" value="'.$value['merk_id'].'">'.
												'<input type="hidden" class="barangjenis_obat_id" value="'.$value['jenis_obat_id'].'">'.
												'<input type="hidden" class="barangsatuan_id" value="'.$value['satuan_id'].'">'.
												'<input type="hidden" class="barangobat_dept_id" value="'.$value['obat_dept_id'].'">'.
											'</td></tr>';
										}
									}
								?>
							</tbody>
						</table>
					</div>
					<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
				</div>
				<br><br>
	        </div>
			<div class="modal fade" id="inout" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal informasi" role="form" method="post" id="submitinoutunit">
					<div class="modal-dialog">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<div class="form-group">
		        					<label class="control-label col-md-3" >Tanggal 
									</label>
									<div class="col-md-4" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" id="tglInOut" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" >In / Out 
									</label>
									<div class="col-md-4">
						         		<select class="form-control select" name="iober" id="iober">
											<option value="IN" selected>IN</option>
											<option value="OUT">OUT</option>					
										</select>
									</div>	
								</div>
								<div class="form-group">
		        					<label class="control-label col-md-3" >Jumlah </label>
									<div class="col-md-4" >
					         			<input type="text" class="form-control" id="jmlInOutBer" name="jmlInOutBer" placeholder="Jumlah">
									</div>
								</div>
								<div class="form-group">
		        					<label class="control-label col-md-3" >Sisa Stok </label>
									<div class="col-md-4" >
					         			<input type="text" class="form-control" id="sisaInOutBer" name="sisaInOutBer" placeholder="Sisa Stok" readonly="">
									</div>
								</div>
								<div class="form-group">
		        					<label class="control-label col-md-3" >Keterangan </label>
									<div class="col-md-6" >
										<textarea class="form-control" id="keteranganIO" placeholder="Keterangan"></textarea>
									</div>
								</div>
		        			</div>
		        			<div class="modal-footer">
		        				<input type="hidden" id="inout_obat_dept_id">
		        				<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
		 			       		<button type="submit" class="btn btn-success">Simpan</button>
					      	</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal fade" id="edInvenBer" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
	        			<form class="form-horizontal" role="form">
			            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
								<thead>
									<tr class="info" >
										<th  style="text-align:center"> Waktu </th>
										<th  style="text-align:left"> IN / OUT </th>
										<th  style="text-align:left"> Jumlah </th>
										<th  style="text-align:left"> Stok Akhir </th>
									</tr>
								</thead>
								<tbody id="tbodydetailobatinventori">
									<tr>
										<td colspan="4" style="text-align:center">Tidak ada Detail</td>
									</tr>
								</tbody>
							</table>
						</form>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
				      	</div>
					</div>
				</div>
			</div>

			<div class="dropdown" id="btnBawahMintaObat">
	            <div id="titleInformasi">Permintaan Farmasi</div>
	            <div id="btnBawahMintaObat" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoMintaObat">
            	<form class="form-horizontal" role="form" method="post" id="permintaanfarmasibersalin">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="noPermFarmBers" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanfarmasi" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="ketObatFarBers" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>
					<a href="#modalMintaFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<!-- <th width="20"> No. </th> -->
										<th> Nama Obat </th>
										<th>Tanggal Kadaluarsa</th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Stok Gudang </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputMintaFar" class="addKosong">
								</tbody>
							</table>
						</div>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning" type="button" id="batalpermintaanfarmasi">RESET</button>
								<button class="btn btn-success" type="submit">SIMPAN</button> 
							</span>
						</div>
					</div>	
				</form>
			</div>	    
			<br>
			<div class="modal fade" id="modalMintaFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<div class="form-group">
		        				<form method="post" class="form-horizontal" role="form" id="formobatfarmasi">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncifarmasi" placeholder="Nama Obat"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
								</form>
								<div style="margin-right:10px;margin-left:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%;">
										<thead>
											<tr class="info">
												<th>Nama Obat</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Stok Gudang</th>
												<th>Tgl Kadaluarsa</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodyobatpermintaanfarmasi">
											<tr>
												<td colspan="6" style="text-align:center">Cari data Obat</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
	           	
	       	<div class="dropdown" id="btnBawahRetDepartemen">
	            <div id="titleInformasi">Retur Farmasi</div>
	            <div id="btnBawahRetFarmasi" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
           	<div id="infoRetDepartemen">
            	<form class="form-horizontal" role="form" method="post" id="formsubmitretur">
            		<div class="informasi">
            			<br>
            			<div class="form-group">
            				<div class="col-md-2">
            					<label class="control-label">Nomor Retur</label>
            				</div>
            				<div class="col-md-3">
            					<input type="text" class="form-control" name="noRetFarBers" id="noRetFarBers" placeholder="Nomor Retur"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
            					<label class="control-label">Tanggal Retur</label>
            				</div>
            				<div class="col-md-2">
            					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control" id="waktureturbersalin" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
            			</div>
            			<div class="form-group">
							<div class="col-md-2">
            					<label class="control-label">Keterangan</label>
            				</div>
            				<div class="col-md-3">
								<textarea class="form-control" id="ketObatRetFarBers" name="ketObatRetFarBers"></textarea>	
							</div>
            			</div>
            		</div>

            		<a href="#modalRetFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left : 40px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>
					
					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabRetur">
								<thead>
									<tr class="info" >
										<th > Nama Obat </th>
										<th > Tanggal Kadaluarsa</th>
										<th > Satuan </th>
										<th > Merek </th>
										<th > Stok Unit </th>
										<th > Jumlah Retur </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputRetFar" class="addKosong">
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning" type="button" id="batalreturfarmasi">RESET</button>
								<button class="btn btn-success" type="submit">SIMPAN</button>
							</span>
						</div>
						<br>
					</div>
				</form>
			</div>
			<div class="modal fade" id="modalRetFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<div class="form-group">
		        				<form method="post" role="form" class="form-horizontal" id="formsearchobatretur">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncireturbersalin" placeholder="Nama Obat"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>
								</form>
								<div style="margin-left:10px; margin-right:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa">
										<thead>
											<tr class="info">
												<td>Nama Obat</td>
												<td>Satuan</td>
												<td>Merek</td>
												<td>Stok Unit</td>
												<td>Tgl Kadaluarsa</td>
												<td width="10%">Pilih</td>
											</tr>
										</thead>
										<tbody id="tbodyreturbersalin">
											<tr>
												<td style="text-align:center" colspan="6">Cari data Obat</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>	
			<br>
	    </div>

        <div class="tab-pane" id="logistik">
        	<div class="modal fade" id="modalbarang" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:800px">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
	        			</div>
	        			<div class="modal-body">

		        			<div class="form-group">
		        				<form method="post" class="form-horizontal" role="form" id="formmintabarang">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncimintabarang" placeholder="Nama barang"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
								</form>
								<div style="margin-right:10px;margin-left:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%">
										<thead>
											<tr class="info">
												<th>Nama Barang</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Tahun Pengadaan</th>
												<th>Stok Gudang</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodybarangpermintaan">
											<tr>
												<td colspan="6" style="text-align:center">Cari data Barang</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
	       	<div class="dropdown" id="btnBawahInventoriBarang">
	            <div id="titleInformasi">Inventori</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoInventoriBarang">
				<div class="form-group" >
					<div class="portlet-body" style="margin: 30px 10px 20px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tblinventorigudangunit">
							<thead>
								<tr class="info" >
									<th width="20">No.</th>
									<th > Nama Barang </th>
									<th > Merek </th>
									<th > Harga </th>
									<th > Stok </th>
									<th > Satuan </th>
									<th > Tahun Pengadaan</th>
									<th > Sumber Dana</th>
									<th width="100"> Action </th>
								</tr>
							</thead>
							<tbody id="tbodyinventoribarang">
								<?php 
									if (isset($inventoribarang)) {
										if (!empty($inventoribarang)) {
											$i = 1;
											foreach ($inventoribarang as $value) {
												echo '<tr>
														<td>'.($i++).'</td>
														<td>'.$value['nama'].'</td>
														<td>'.$value['nama_merk'].'</td>
														<td>'.$value['harga'].'</td>
														<td>'.$value['stok'].'</td>
														<td>'.$value['satuan'].'</td>
														<td>'.$value['tahun_pengadaan'].'</td>
														<td>'.$value['sumber_dana'].'</td>
														<td style="text-align:center">
															<input type="hidden" class="barang_detail_inout" value="'.$value['barang_detail_id'].'">
															<a href="#inoutbar" data-toggle="modal" class="edBarang" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
															<a href="#edInvenBerBar" data-toggle="modal" class="detailinvenbarang"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
														</td>
													</tr>';
											}
										}
									}
								?>
									
							</tbody>
						</table>
					</div>
					<form method="post" action="<?php echo base_url() ?>fisioterapi/homelab/excel_barang_unit">
						<button class="btn btn-info" type="submit" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
						<input type="hidden" class="my_dept_id" name="my_dept_id" value="<?php echo $dept_id ?>">
					</form>
					<br>
	        	</div>
	        </div>
			<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<form class="form-horizontal" role="form" style="margin-left:30px;" id="forminoutbarang">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<div class="form-group">
			        				<label class="control-label col-md-3" >Tanggal </label>
									<div class="col-md-6" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" id="tanggalinout" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
									</div>
								</div>
										
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" >In / Out </label>
									<div class="col-md-6">
						         		<select class="form-control select" name="io" id="io">
											<option value="IN" selected>IN</option>
											<option value="OUT">OUT</option>					
										</select>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Jumlah in/out</label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="jmlInOut" name="jmlInOut" placeholder="Jumlah">
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Sisa Stok </label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="sisaInOut" name="sisaInOut" placeholder="Sisa Stok" readonly>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Keterangan </label>
									<div class="col-md-6" >
										<textarea class="form-control" id="keteranganIObarang" placeholder="Keterangan"></textarea>
									</div>
								</div>										
		        			</div>
		        			<div class="modal-footer">
		        				<input type="hidden" id="id_barang_inoutprocess">
		 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			       		<button type="submit" class="btn btn-success">Simpan</button>
					      	</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<form class="form-horizontal" role="form">
				            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th> Waktu </th>
											<th> IN / OUT </th>
											<th> Jumlah </th>
											<th> Keterangan </th>
										</tr>
									</thead>
									<tbody id="tbodydetailbrginventori">
										<tr>
											<td colspan="4" style="text-align:center">Tidak ada detail in-out</td>
										</tr>
											
									</tbody>
								</table>
							</form>
							
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
			<br>

			<div class="dropdown" id="btnBawahPermintaanBarang" style="margin-left:10px;width:98.5%">
	            <div id="titleInformasi">Permintaan Logistik</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoPermintaanBarang">
            	<form class="form-horizontal" role="form" method="post" id="permintaanbarangunit">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="nomorpermintaanbarang" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanbarang" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="keteranganpermintaanbarang" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>
					<a href="#modalbarang" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Barang</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<th> Nama Barang </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Tahun Pengadaan </th>
										<th> Stok Gudang </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputmintabarang">
									<?php echo '<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>'; ?>
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning" type="reset" id="batalpermintaanfarmasi">RESET</button>
								<button class="btn btn-success" type="submit">SIMPAN</button>
							</span>
						</div>
						<br>
					</div>	
				</form>
			</div>	    
			<br>
	    </div>

        <div class="tab-pane" id="laporan" style="margin-left:40px">   
        	<div id="lab">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Fisioterapi</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;padding:0px 30px 0px 30px;" role="form">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>  
	        <br>
			<div id="radiolog">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Radiologi</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;padding:0px 30px 0px 30px;" role="form">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>
	        <br>
	        <div id="fisio">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Fisioterapi</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;padding:0px 30px 0px 30px;" role="form">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>  
	        <br>      
        </div>
        
        <div class="tab-pane" id="master"> 
        	<div class="dropdown" id="tambahpen" style="margin-left:10px;width:98.5%" id="btnBawahRiwTerimaObat">
	            <div id="titleInformasi">Tambah Tarif Penunjang</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
            <br>

            <div id="itambahpen" class="tutupBiru">
	            <form class="form-horizontal" role="form" id="tambah_tindakan_penunjang">
		        	<div class="informasi">
		            	<div class="form-group">
		            		<label class="control-label col-md-2">Nama Tindakan</label>
		            		<div class="col-md-3">
		            			<input type="text" class="form-control" id="tambah_nama_tindakan" name="namaTarifPjg" placeholder="Nama Penunjang" required>
		            		</div>
		            	</div>
		            	<div class="form-group">
		            		<label class="control-label col-md-2">Kategori</label>
		            		<div class="col-md-3">
		            			<input type="text" class="form-control" id="tambah_kategori" name="namaTarifPjg" placeholder="Kategori" required>
								<!-- <select class="form-control select" name="kategoriLab" id="kategoriLab">
									<option selected>Pilih</option>
									<option value="Hematologi">Hematologi</option>
									<option value="Kimia Klinik">Kimia Klinik</option>
									<option value="Urine">Urine</option>
									<option value="Feses">Feses</option>					
									<option value="Imuno-Serologi">Imuno-Serologi</option>					
									<option value="Mikrobiologi">Mikrobiologi</option>
									<option value="Lain-lain">Lain-lain</option>
								</select>	 -->
							</div>
		            	</div>

		            	<div class="form-group">
		            		<label class="control-label col-md-2">Group</label>
		            		<div class="col-md-3">
		            			<input type="hidden" id="id_icd9_group">
		            			<input type="text" class="form-control" placeholder="ICD-9CM" data-toggle="modal" data-target="#searchICDIX" id="icd9_group">
							</div>
		            	</div>

		            </div>
					<br>
	            	<div class="form-group">
	            		<div class="portlet-body" style="margin: 0px 25px 0px 25px">
						
							<table class="table table-striped table-bordered table-hover table-responsive">
								<thead>
									<tr class="info" >
										<th  style="text-align:center"> Jenis Tarif </th>
										<th  style="text-align:center"> BAKHP </th>
										<th  style="text-align:center"> JS </th>
										<th  style="text-align:center"> JP </th>
										<th  style="text-align:center"> Total </th>
										<th  style="text-align:center"> Clear </th>
									</tr>
								</thead>
								<tbody id="tbody_master">
									<tr>
										<td>Kelas VIP</td>
										<td><input type="text" class="form-control bakhp" id="BAKHPPjgLabVIP" name="BAKHPPjgLabVIP" placeholder="0"></td>
										<td><input type="text" class="form-control js" id="JSPjgLabVIP" name="JSPjgLabVIP" placeholder="0"></td>
										<td><input type="text" class="form-control jp" id="JPPjgLabVIP" name="JPPjgLabVIP" placeholder="0"></td>									
										<td><input type="text" class="form-control total" id="TotalPjgLabVIP" name="TotalPjgLabVIP" value="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow1"><i class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
									</tr>
									
									<tr>
										<td>Kelas Utama</td>
										<td><input type="text" class="form-control bakhp" id="BAKHPPjgLabUtama" name="BAKHPPjgLabUtama" placeholder="0"></td>
										<td><input type="text" class="form-control js" id="JSPjgLabUtama" name="JSPjgLabUtama" placeholder="0"></td>
										<td><input type="text" class="form-control jp" id="JPPjgLabUtama" name="JPPjgLabUtama" placeholder="0"></td>									
										<td><input type="text" class="form-control total" id="TotalPjgLabUtama" name="TotalPjgLabUtama" value="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow2"><i class="glyphicon glyphicon-repeat" data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
									</tr>

									<tr>
										<td> Kelas I</td>
										<td><input type="text" class="form-control bakhp"  id="BAKHPPjgLabKlsI" name="BAKHPPjgLabKlsI" placeholder="0"></td>
										<td><input type="text" class="form-control js" id="JSPjgLabKlsI" name="JSPjgLabKlsI" placeholder="0"></td>
										<td><input type="text" class="form-control jp" id="JPPjgLabKlsI" name="JPPjgLabKlsI" placeholder="0"></td>									
										<td><input type="text" class="form-control total" id="TotalPjgLabKlsI" name="TotalPjgLabKlsI" value="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow3"><i class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
									</tr>
									<tr>
										<td>Kelas II</td>
										<td><input type="text" class="form-control bakhp" id="BAKHPPjgLabKlsII" name="BAKHPPjgLabKlsII" placeholder="0"></td>
										<td><input type="text" class="form-control js" id="JSPjgLabKlsII" name="JSPjgLabKlsII" placeholder="0"></td>
										<td><input type="text" class="form-control jp" id="JPPjgLabKlsII" name="JPPjgLabKlsII" placeholder="0"></td>									
										<td><input type="text" class="form-control total" id="TotalPjgLabKlsII" name="TotalPjgLabKlsII" value="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow4"><i class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
									</tr>
									<tr>
										<td>Kelas III</td>
										<td><input type="text" class="form-control bakhp" id="BAKHPPjgLabKlsIII" name="BAKHPPjgLabKlsIII" placeholder="0"></td>
										<td><input type="text" class="form-control js" id="JSPjgLabKlsIII" name="JSPjgLabKlsIII" placeholder="0"></td>
										<td><input type="text" class="form-control jp" id="JPPjgLabKlsIII" name="JPPjgLabKlsIII" placeholder="0"></td>									
										<td><input type="text" class="form-control total" id="TotalPjgLabKlsIII" name="TotalPjgLabKlsIII" value="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow5"><i class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
										
									</tr>
								</tbody>
							</table>
						</div>
	            	</div>
	            	
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success" >SIMPAN</button> 
						</span>
					</div>
					<br><br>

	            </form>
	        </div>

	        <div class="modal fade" id="searchICDIX" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Search ICD-9CM</h3>
				   			</div>
							<div class="modal-body">
								<div class="form-group">
									<div class="form-group">
									<form method="post" id="submit_icd9">
										<div class="col-md-4" style="margin-left:35px;">
											<input type="text" class="form-control" name="katakunci" id="katakunci_icd9" placeholder="Nama icd"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</form>
									</div>		
									<div style="margin-left:20px; margin-right:20px;"><hr></div>
									<div class="portlet-body" style="margin: 0px 0px 0px 40px">
										<table class="table table-striped table-bordered table-hover tabelinformasi" style="width:90%;">
											<thead>
												<tr class="info">
													<td>Nama ICD-9CM</td>
													<td width="10%">Pilih</td>
												</tr>
											</thead>
											<tbody id="tbody_icd9">
												<tr>
													<td colspan="2">Cari Tindakan ICD-9CM</td>
												</tr>

											</tbody>
										</table>												
									</div>
								</div>
		       				</div>
			        		<br>
			        		<div class="modal-footer">
			        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	
						    </div>
						</div>
					</div>
			</div>

			<div class="dropdown" id="jaspel">
	            <div id="titleInformasi">Jasa Pelayanan</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="ijaspel" class="tutupBiru">
	            <form method="POST" id="submitJasaPelayanan" action="<?php echo base_url() ?>fisioterapi/homelab/print_jaspel">
	            <div class="form-horizontal">
		            <div class="informasi">
			            <div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Periode </label>
							<div class="col-md-3" style="margin-left:-15px">
								<div class="input-daterange input-group" id="datepicker">
								    <input type="text" id="mulai_date" style="cursor:pointer;background-color:white;" class="form-control" name="start" data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
								    <div class="input-group-addon">to</div>
								    <input type="text" id="sampai_date" style="cursor:pointer;background-color:white;" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2"> <i class="glyphicon glyphicon-filter"></i>&nbsp;Cara Bayar</label>
							<div class="input-group col-md-2">
								<select class="form-control select" name="cara_bayar" id="carabayar">
									<option value="" selected>Pilih Cara Bayar</option>
									<option value="Umum">Umum</option>
									<option value="BPJS" id="op-bpjs">BPJS</option>
									<option value="Jamkesmas" >Jamkesmas</option>
									<option value="Asuransi" id="op-asuransi">Asuransi</option>
									<option value="Kontrak" id="op-kontrak">Kontrak</option>
									<option value="Gratis" >Gratis</option>
									<option value="Lain">Lain-lain</option>
								</select>
							</div>	
						</div>

						<div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Paramedis</label>
							<div class="input-group col-md-2">
								<input type="hidden" id="jpparamedis_id" name="id_petugas_jaspel" value="">
								<input type="text" class="form-control" id="jpparamedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
							</div>

						</div>

					</div>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span class="customSpan">
								<button type="button" id="btn_filter_jaspel" class="btn btn-warning">FILTER</button> 
							</span>
					</div>
					<br>

				</div>

		    	<div class="portlet-body" style="margin: 10px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover tableDTUtama" id="tabelJpPoliinap">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal</th>
								<th>Tindakan</th>
								<th>Cara Bayar</th>
								<th>Paramedis</th>
								<th>Jasa Pelayanan</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<?php
								$no = 0;
								foreach ($jasalayan as $data) {
									echo '
										<tr>
											<td style="text-align:center">'.++$no.'</td>
											<td style="text-align:center">'.$data['tanggal'].'</td>
											<td>'.$data['nama_tindakan'].'</td>
											<td>'.$data['cara_bayar'].'</td>
											<td>'.$data['nama_petugas'].'</td>
											<td style="text-align:right">'.$data['jp'].'</td>
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>
				<button type="submit" class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button> 
				<br><br>
			</form>
				<br><br>
			</div>
	    </div>
   		
   		<div class="tab-pane" id="tagihan" style="padding-bottom:50px;"> 
        	<form method="POST" class="form-horizontal" id="submitTagihanSearch">
		       	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search">&nbsp;&nbsp;</i>Nama Pasien / Rekam Medis <span class="required" style="color : red">* </span>
					</label>
					<div class="col-md-4" style="width:420px;">		
						<input type="text" id="search_tagihan" class="form-control" placeholder="Masukkan Nama atau Nomor Rekam Medis Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        <a onclick="setStatus('26');" data-toggle="modal" class="btn btn-warning"> Tambah Invoice Baru</a>
				</div>	
			</form>
			
			<hr class="garis">

			<div class="portlet box red">
				<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_tagihan">
						<thead>
							<tr class="info">
								<th style="text-align:center;width:20px;">No.</th>
								<th>Unit</th>
								<th>Nomor Invoice</th>
								<th>Nomor Visit</th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Cara Bayar</th>
								<th style="text-align:center;width:25px;">Action</th>
							</tr>
						</thead>
						<tbody>

							<!-- <tr>
								<td>1</td>
								<td>Bersalin</td>
								<td>1212121</td>
								<td>32323</td>	
								<td>123123</td>									
								<td>Selena</td>
								<td>Rumahnya</td>
								<td>Ansuransi</td>
								<td style="text-align:center">
									<a href="<?php echo base_url() ?>fisioterapi/invoicenonbpjs" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>
								</td>										
							</tr>
							<tr>
								<td>2</td>
								<td>Bersalin</td>
								<td>1212121</td>
								<td>32323</td>	
								<td>123123</td>									
								<td>Jems</td>
								<td>Rumahnya</td>
								<td>BPJS</td>
								<td style="text-align:center">
									<a href="<?php echo base_url() ?>fisioterapi/invoicebpjs" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>
								</td>										
							</tr> -->
						</tbody>
					</table>
				</div>			
			</div> 

        </div>

    </div>

</div>
				
<script type="text/javascript">
	$(document).ready( function(){
		$('.addNewMintaFar').on('click',function(){
			tambahPermintaanFarmasi('#addinputMintaFar','.addNewMintaFar');
		});

		$('.addNewRetFar').on('click',function(){
			tambahReturFarmasi('#addinputRetFar','.addNewRetFar');
		});

		$('.addNewLog').on('click',function(){
			tambahPermintaanLogistik('#addinputMintaLog','.addNewLog');
		});

		$("#tambahpen").click(function(){
			$("#itambahpen").slideToggle();
		});

		$("#jaspel").click(function(){
			$("#ijaspel").slideToggle();
		});

		$("#bwinvent").click(function(){
			$("#ibwinvent").slideToggle();
		});

		$("#bwpermintaanfarmasi").click(function(){
			$("#ibwpermintaanfarmasi").slideToggle();
		});

		$("#bwreturfarmasi").click(function(){
			$("#ibwreturfarmasi").slideToggle();
		});

		$("#bwinlogistik").click(function(){
			$("#ibwinlogistik").slideToggle();
		});

		$("#bwpermintaanlogistik").click(function(){
			$("#ibwpermintaanlogistik").slideToggle();
		});


		$(".clearRow1").click(function(e){
			e.preventDefault();
			$(".cekrow1").val('');
		});

		$(".clearRow2").click(function(e){
			e.preventDefault();
			$(".cekrow2").val('');
		});
		$(".clearRow3").click(function(e){
			e.preventDefault();
			$(".cekrow3").val('');
		});
		$(".clearRow4").click(function(e){
			e.preventDefault();
			$(".cekrow4").val('');
		});
		$(".clearRow5").click(function(e){
			e.preventDefault();
			$(".cekrow5").val('');
		});

		$('.tambahlab').click(function(){
			$('.tablab a[href="#Hematologi"]').tab('show');
			$('#Hematologi').addClass('active');
		});

		//-------------- search APS -----------------------------//
		$('#search_aps').submit(function(event){
			event.preventDefault();
			var item = {};
			item['search'] = $('#input_aps').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>fisioterapi/homelab/search_listaps',
				success:function(data){
					var t = $('#tableAPS').DataTable();
					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						var action = '<td style="text-align:center"><a href="#tambahPeri" class="tambahlab" data-toggle="modal" ><i class="glyphicon glyphicon-check"></i></a></td>';

						t.row.add([
							(i+1),
							changeDateTime(data[i]['waktu']),
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['jenis_kelamin'],
							data[i]['jenis_periksa'],
							action,
							i
						]).draw();
					}
				},error:function(data){
					
				}
			});

		});


		$('#search_rujuk').submit(function(event){
			event.preventDefault();
			var item = {};
			item['search'] = $('#input_searchRujuk').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>fisioterapi/homelab/search_listrujuk',
				success:function(data){
					var t = $('#table_rujuk').DataTable();
					t.clear().draw();
					
					for(var i = 0; i<data.length; i++){
						var action = '<td style="text-align:center"><a href="#tambahPeri" class="tambahlab" data-toggle="modal" ><i class="glyphicon glyphicon-check"></i></a></td>';

						t.row.add([
							(i+1),
							data[i]['nama_dept'],
							changeDateTime(data[i]['waktu']),
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['nama_petugas'],
							data[i]['jenis_periksa'],
							action,
							i
						]).draw();
					}
				},error:function(data){
					
				}
			});

		});

		//------------ submit jenis pemeriksaan -----------------//
		var pasien;
		var selected;
		$('#table_rujuk').on('click','tr', function (e) {
			pasien = 'rujuk';
			selected = $(this);
			e.preventDefault();
		});

		$('#tableAPS').on('click','tr', function (e) {
			pasien = 'aps';
			selected = $(this);
			e.preventDefault();
		});

		$('#submit_pemeriksaan').submit(function(event){
			event.preventDefault();
			var item = [];

			$("input:checkbox[name='jenisperiksa']:checked").each(function(){
			    item.push($(this).val());
			});

			for(var i = 0; i < item.length; i++){
				var value = {};
				value['nama_tindakan'] = item[i];
				value['penunjang_id'] = $('#penunjang_id').val();
				value['kelas_pelayanan'] = $('#kelas_periksa').val();

				$.ajax({
					type:'POST',
					data:value,
					url:'<?php echo base_url() ?>fisioterapi/homelab/submit_pemeriksaan',
					success:function(data){
						console.log(data);
						
					}, error:function(data){
						console.log(data);
					}
				});
			}

			get_listaps();
			get_listrujuk();
			get_listlab();
			$('#tambahPeri').modal('hide');
		});

		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			var autodata = [];
			var iddata = [];

			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>fisioterapi/homelab/get_paramedis',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_petugas']);
							iddata.push(data[i]['petugas_id']);
						}
						console.log(autodata);
					}
				});
			}

			$input.typeahead({source:autodata, autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = autodata.indexOf(current);

			    $('#paramedis_id').val(iddata[index]);
			    
			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});
		});

		$('#submit_periksa').submit(function(event){
			event.preventDefault();
			
			$('#tbody_periksa tr').each(function(){
				var item = {};
				var hasil = $(this).children('td').children('.hasil').val();
				var nilai = $(this).children('td').children('.nilai').val();
				var faktur = $(this).children('td').children('.onfaktur').val();
				var keterangan = $(this).children('td').children('.keterangan').val();
				var id = $(this).children('td').children('.penunjang_detail_id').val();

				item['hasil'] = hasil;
				item['nilai_normal'] = nilai;
				item['on_faktur'] = faktur;
				item['keterangan'] = keterangan;
				item['penunjang_detail_id'] = id;
				item['status'] = $('#phasil_status').val();
				item['pemeriksa'] = $('#paramedis_id').val();
				item['penunjang_id'] = $('#phasil_penunjang_id').val();

				$.ajax({
					type:"POST",
					data:item,
					url:'<?php echo base_url()?>fisioterapi/homelab/update_hasilperiksa',
					success:function(data){
						myAlert('Data Berhasil Disimpan')
					},error:function(data){
						console.log(data);
						alert('gagal');
					}
				});
			}).promise().done(get_listlab());

		});	

		$('#submitTagihanSearch').submit(function(event){
			event.preventDefault();

			var item = {};
			item['search'] = $('#search_tagihan').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo  base_url(); ?>fisioterapi/homelab/search_tagihan',
				success:function(data){
					console.log(data);
					var t = $('#table_tagihan').DataTable();
					var no = 0;
					var action;
					t.clear().draw();

					for(var i=0; i<data.length; i++){
						no++;
						if(data[i]['carapembayaran'] == "BPJS"){
							action = '<a href="<?php echo base_url() ?>fisioterapi/invoicebpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}else{
							action = '<a href="<?php echo base_url() ?>fisioterapi/invoicenonbpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}

						t.row.add([
							no,
							data[i]['nama_dept'],
							data[i]['no_invoice'],
							data[i]['visit_id'],
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['alamat_skr'],
							data[i]['carapembayaran'],
							action,
							i
						]).draw();
					}

				}
			});
		});

		//------------------ farmasi here ---------------------//
		$("#tbodyinventoriunit").on('click', 'tr td a.printobat', function (e) {
			var obat_dept_id = $(this).closest('tr').find('td .barangobat_dept_id').val();

			 $.ajax({
		    	type: "POST",
		    	url: "<?php echo base_url()?>farmasi/homegudangobat/get_detail_obat_bydeptid/" + obat_dept_id, //benar
		    	success: function (data) {
		    		console.log(data);
		    		$('#tbodydetailobatinventori').empty();
		    		for(var i = 0; i < data.length ; i++){
		    			var a = "";
		    			var jlh = "";
		    			if(data[i]['masuk'] == 0) {a = "OUT"} else a = "IN";
		    			if(data[i]['masuk'] == 0)  {jlh = data[i]['keluar']} else jlh = data[i]['masuk'];
		    			$('#tbodydetailobatinventori').append(
							'<tr>'+
								'<td style="text-align:center">'+format_date(data[i]['tanggal'])+'</td>'+
								'<td>'+a+'</td>'+
								'<td>'+jlh+'</td>'+
								'<td>'+data[i]['total_stok']+'</td>'+
							'</tr>'
		    			)
		    		}
		    	},
		    	error: function (data) {
		    		myAlert('gagal');
		    	}
		    })
		})

		var this_io_obat;
		$('#tbodyinventoriunit').on('click','tr td a.inoutobat', function (e) {
			e.preventDefault();
			this_io_obat = $(this);
			var obat_dept_id = $(this).closest('tr').find('td .barangobat_dept_id').val();
			var jlh = $(this).closest('tr').find('td').eq(5).text();

			$('#inout_obat_dept_id').val(obat_dept_id);
			$('#sisaInOutBer').val(jlh);

			$('#jmlInOutBer').on('change', function (e) {
				e.preventDefault();

				var is_in = $('#iober').find('option:selected').val();
				var jmlInOut = $('#jmlInOutBer').val();
				var sisa = jlh;//$('#sisaInOut').val();
				var hasil ="";
				if (is_in == 'IN') {
					hasil = Number(jmlInOut) + Number(sisa);
				}else{			
					hasil = Number(sisa) - Number(jmlInOut);
				}

				if (jmlInOut == '') {
					hasil = Number(sisa);
				}
				$('#sisaInOutBer').val(hasil);			
			})

		})

		$('#submitinoutunit').submit(function (e) {
			e.preventDefault();

			var item = {};
			item['obat_dept_id'] = $('#inout_obat_dept_id').val();
			item['jumlah'] = $('#jmlInOutBer').val();
			item['sisa'] = $('#sisaInOutBer').val();
			item['is_out'] = $('#iober').find('option:selected').val();
			item['tanggal'] = $('#tglInOut').val();
		    item['keterangan'] = $('#keteranganIO').val();

		    if (item['jumlah'] != "") {
			    $.ajax({
			    	type: "POST",
			    	data: item,
			    	url: "<?php echo base_url()?>bersalin/homebersalin/input_in_out",
			    	success: function (data) {

			    		if (data == "true") {
			    			myAlert('data berhasil disimpan');
			    			$('#keteranganIO').val('');
			    			$('#jmlInOutBer').val('');
			    			this_io_obat.closest('tr').find('td').eq(5).text(item['sisa']);
			    			$('#inout').modal('hide');	
			    		} else{
			    			myAlert('gagal, terdapat kesalahan');
			    		};
			    		
			    	},
			    	error: function (data) {
			    		myAlert('gagal');
			    	}
			    })
			} else{
				myAlert('isi data dengan benar');
				$('#jmlInOutBer').focus();
			};		
		})

		$('#formobatfarmasi').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['katakunci'] = $('#katakuncifarmasi').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>bersalin/homebersalin/get_obat_gudang',
				success: function (data) {
					console.log(data);
					$('#tbodyobatpermintaanfarmasi').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodyobatpermintaanfarmasi').append(
								'<tr>'+
									'<td style="display:none">'+data[i]['obat_detail_id']+'</td>'+
									'<td style="display:none">'+data[i]['tgl_kadaluarsa']+'</td>'+
									'<td style="display:none">'+data[i]['obat_id']+'</td>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['total_stok']+'</td>'+
									'<td>'+format_date(data[i]['tgl_kadaluarsa'])+'</td>'+
									'<td style="text-align:center"><a href="#" class="addNewMintaFar"><i class="glyphicon glyphicon-check"></i></a></td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodyobatpermintaanfarmasi').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		
		$('#tbodyobatpermintaanfarmasi').on('click','tr td a.addNewMintaFar', function (e) {
			e.preventDefault();

			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputMintaFar').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputMintaFar').append(
				'<tr><td style="display:none">'+cols[0]+'</td>'+
				'<td style="display:none">'+cols[2]+'</td>'+
				'<td>'+cols[3]+'</td>'+
				'<td>'+format_date(cols[1])+'</td>'+
				'<td>'+cols[4]+'</td>'+
				'<td>'+cols[5]+'</td>'+
				'<td>'+cols[6]+'</td>'+
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>'
			)
		})

		$('#permintaanfarmasibersalin').submit(function (e) {
			e.preventDefault();
			$('#addinputMintaFar').find('tr td.dataKosong').closest('tr').remove();
			var item = {};
			item['no_permintaan'] = $('#noPermFarmBers').val();
			item['tanggal_request'] = $('#tglpermintaanfarmasi').val();
			item['keterangan_request'] = $('#ketObatFarBers').val();

			//jlh = 9, obat_id = 1, obat_detail_id = 0
			var data = [];
		    $('#addinputMintaFar').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				$('#addinputMintaFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}
			
		    item['data'] = data;
		    var a = confirm('yakin disimpan ?');
		    if (a == true) {
			    $.ajax({
					type: "POST",
					data: item,
					url: '<?php echo base_url()?>fisioterapi/homelab/submit_permintaan_bersalin',
					success: function (data) {
						console.log(data);
						if (data['error'] == 'n'){
							$('#addinputMintaFar').empty();
							$('#noPermFarmBers').val('');
							$('#ketObatFarBers').val('');
							$('#addinputMintaFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
						}
						myAlert(data['message']);
					},
					error: function (data) {
						console.log(data);
					}
				})
			};
		})

		$('#batalpermintaanfarmasi').on('click',function (e) {
			e.preventDefault();
			$('#addinputMintaFar').empty();
			$('#addinputMintaFar').append('<tr><td colspan="6" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
		})

		$('#formsearchobatretur').submit(function (e) {
			e.preventDefault();
			var item ={};
			item['katakunci'] = $('#katakuncireturbersalin').val();

			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>fisioterapi/homelab/get_obat_retur',
				success: function (data) {
					//console.log(data);
					$('#tbodyreturbersalin').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodyreturbersalin').append(
								'<tr>'+
									'<td style="display:none">'+data[i]['obat_detail_id']+'</td>'+
									'<td style="display:none">'+data[i]['tgl_kadaluarsa']+'</td>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['total_stok']+'</td>'+
									'<td>'+format_date(data[i]['tgl_kadaluarsa'])+'</td>'+
									'<td style="text-align:center"><a href="#" class="addNewReturFar"><i class="glyphicon glyphicon-check"></i></a></td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodyreturbersalin').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tbodyreturbersalin').on('click', 'tr td a.addNewReturFar', function (e) {
			e.preventDefault();
			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputRetFar').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputRetFar').append(
				'<tr><td style="display:none">'+cols[0]+'</td>'+//obat detail id
				'<td>'+cols[2]+'</td>'+  //nama
				'<td>'+format_date(cols[1])+'</td>'+ //tanggal kadaluarsa
				'<td>'+cols[3]+'</td>'+ //satuan
				'<td>'+cols[4]+'</td>'+ //merk
				'<td>'+cols[5]+'</td>'+ //stok unit
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah retur
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>'
			)
		})

		$('#formsubmitretur').submit(function (e) {
			e.preventDefault();
			$('#addinputRetFar').find('tr td.dataKosong').closest('tr').remove();
			var item = {};
			item['no_returdept'] = $('#noRetFarBers').val();
			item['waktu'] = $('#waktureturbersalin').val();
			item['keterangan'] = $('#ketObatRetFarBers').val();

			//jlh = 8, obat_id = 1, obat_detail_id = 0
			var data = [];
		    $('#addinputRetFar').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				 $('#addinputRetFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;
		    var a = confirm('yakin diproses ?');
		    if (a == true) {
			    $.ajax({
					type: "POST",
					data: item,
					url: '<?php echo base_url()?>fisioterapi/homelab/submit_retur_bersalin',
					success: function (data) {
						console.log(data);
						if (data['error'] == 'n'){
							$('#addinputRetFar').empty();
							$('#noRetFarBers').val('');
							$('#ketObatRetFarBers').val('');
						}
						myAlert(data['message']);
					},
					error: function (data) {
						console.log(data);
					}
				})
			};
		})

		$('#submitfilterfarmasiunit').submit(function (e) {
			e.preventDefault();
			var filter = {};
			filter['filterby'] = $('#filterInv').find('option:selected').val();
			filter['valfilter'] = $('#filterby').val();
			submit_filter(filter);
		})
		
		$('#expired').on('click', function (e) {
			e.preventDefault();
			var filter = {};
			filter['expired'] = '0';
			submit_filter(filter)
		})

		$('#expired3').on('click', function (e) {
			e.preventDefault();
			var filter = {};
			filter['expired'] = '3';
			submit_filter(filter)
		})

		$('#expired6').on('click', function (e) {
			e.preventDefault();
			var filter = {};
			filter['expired'] = '6';
			submit_filter(filter);
		})

		//------------------ end farmasi here -----------------//
		
		//------------------ logistik here --------------------//
		$('#permintaanbarangunit').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['no_permintaanbarang'] = $('#nomorpermintaanbarang').val();
			item['tanggal_request'] = $('#tglpermintaanbarang').val();
			item['keterangan_request'] = $('#keteranganpermintaanbarang').val();

			var data = [];
			$('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
		    $('#addinputmintabarang').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;

		    $.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>fisioterapi/homelab/submit_permintaan_barangunit',
				success: function (data) {
					console.log(data);
					if (data['error'] == 'n'){
						$('#addinputmintabarang').empty();
						$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
						$('#nomorpermintaanbarang').val('');
						$('#keteranganpermintaanbarang').val('');
					}
					myAlert(data['message']);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		
		$('#formmintabarang').submit(function (e) {
			e.preventDefault();
			var item ={};
			item['katakunci'] = $('#katakuncimintabarang').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>bersalin/homebersalin/get_barang_gudang',
				success: function (data) {
					console.log(data);//return false;
					$('#tbodybarangpermintaan').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodybarangpermintaan').append(
								'<tr>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['tahun_pengadaan']+'</td>'+
									'<td>'+data[i]['stok_gudang']+'</td>'+
									'<td style="text-align:center"><a href="#" class="addnewpermintaanbarang"><i class="glyphicon glyphicon-check"></i></a></td>'+
									'<td style="display:none">'+data[i]['barang_stok_id']+'</td>'+
									'<td style="display:none">'+data[i]['barang_id']+'</td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodybarangpermintaan').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		
		$('#tbodybarangpermintaan').on('click', 'tr td a.addnewpermintaanbarang',function (e) {
			e.preventDefault();
			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputmintabarang').append(
				'<tr><td>'+cols[0]+'</td>'+//nama
				'<td>'+cols[1]+'</td>'+  //satuan
				'<td>'+cols[2]+'</td>'+ //merk
				'<td>'+cols[3]+'</td>'+ //tahun pengadaan
				'<td>'+cols[4]+'</td>'+ //stok gudang
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah minta
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
				'<td style="display:none">'+cols[6]+'</td>'+ //barang_stok_id
				'<td style="display:none">'+cols[7]+'</td></tr>' //barang_id
			)
		})

		$("#tbodyinventoribarang").on('click', 'tr td a.detailinvenbarang', function (e) {
			var id = $(this).closest('tr').find('td .barang_detail_inout').val();

			 $.ajax({
		    	type: "POST",
		    	url: "<?php echo base_url()?>fisioterapi/homelab/get_detail_inventori/" + id,
		    	success: function (data) {
		    		console.log(data);
		    		$('#tbodydetailbrginventori').empty();
		    		for(var i = 0; i < data.length ; i++){
		    			$('#tbodydetailbrginventori').append(
							'<tr>'+
								'<td>'+format_date(data[i]['tanggal'])+'</td>'+
								'<td>'+data[i]['is_out']+'</td>'+
								'<td>'+data[i]['jumlah']+'</td>'+
								'<td>'+data[i]['keterangan']+'</td>'+
							'</tr>'
		    			)
		    		}
		    	},
		    	error: function (data) {
		    		myAlert('gagal');
		    	}
		    })
		})

		var this_io;
		$('#tbodyinventoribarang').on('click', 'tr td a.edBarang', function (e) {
			e.preventDefault();
			$('#id_barang_inoutprocess').val($(this).closest('tr').find('td .barang_detail_inout').val());
			var jlh = $(this).closest('tr').find('td').eq(4).text();
			this_io = $(this);
			$('#sisaInOut').val(jlh);

			$('#jmlInOut').on('change', function (e) {
				e.preventDefault();

				var is_in = $('#io').find('option:selected').val();
				var jmlInOut = $('#jmlInOut').val();
				var sisa = jlh;//$('#sisaInOut').val();
				var hasil ="";
				if (is_in == 'IN') {
					hasil = Number(jmlInOut) + Number(sisa);
				}else{			
					hasil = Number(sisa) - Number(jmlInOut);
				}

				if (jmlInOut == '') {
					hasil = Number(sisa);
				}
				$('#sisaInOut').val(hasil);			
			})

			$('#io').on('change', function () {
				var jumlah = Number($('#jmlInOut').val());
				var sisa = Number(jlh);//Number($('#sisaInOut').val());

				var isout = $('#io').find('option:selected').val();
				if (isout === 'IN') {
					$('#sisaInOut').val(jumlah + sisa);
				} else{
					$('#sisaInOut').val(sisa - jumlah);
				};
			})
		})

		$('#forminoutbarang').submit(function (e) {
			e.preventDefault();

			var item = {};
			item['barang_detail_id'] = $('#id_barang_inoutprocess').val();
			item['jumlah'] = $('#jmlInOut').val();
			item['sisa'] = $('#sisaInOut').val();
			item['is_out'] = $('#io').find('option:selected').val();
		    item['tanggal'] = $('#tanggalinout').val();
		    item['keterangan'] = $('#keteranganIObarang').val();
		    //console.log(item);return false;
		    if (item['jumlah'] != "") {
			    $.ajax({
			    	type: "POST",
			    	data: item,
			    	url: "<?php echo base_url()?>fisioterapi/homelab/input_in_outbarang",
			    	success: function (data) {
			    		if (data == "true") {
			    			myAlert('data berhasil disimpan');
			    			$('#keteranganIO').val('');
			    			$('#jmlInOut').val('');
			    			this_io.closest('tr').find('td').eq(4).text(item['sisa']);
			    			$('#inoutbar').modal('hide');	
			    		} else{
			    			myAlert('gagal, terdapat kesalahan');
			    		};
			    	},
			    	error: function (data) {
			    		myAlert('gagal');
			    	}
			    })
			} else{
				myAlert('isi data dengan benar');
				$('#jmlInOut').focus();
			};			
		})
 
		//------------------ end logistik here ----------------//

		$('#search_hasil').submit(function(event){
			event.preventDefault();

			var item = {};
			item['search'] = $('#input_hasil').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?=base_url()?>fisioterapi/homelab/search_hasil',
				success:function(data){
					var t = $('#tableHasil').DataTable();

					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						var pengirim, status;

						if(data[i]['status'] != "Selesai"){
							var action = '<center><a href="#tambahHasil" data-toggle="modal" onclick="Periksa(&quot;'+data[i]['penunjang_id']+'&quot;)"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Pemeriksaan"></i></a>'+
										'<a href="#view" data-toggle="modal" ><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Lihat Detail"></i></a><center>';
						}
						else{
							var action = '<center><a href="#view" data-toggle="modal" onclick="LihatHasil(&quot;'+data[i]['penunjang_id']+'&quot;)"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Lihat Detail"></i></a></center>';
						}	

						if(!data[i]['nama_petugas'])
							pengirim = data[i]['pengirim'];
						else
							pengirim = data[i]['nama_petugas'];

						if(data[i]['status'] == "PROSES")
							status = "Belum";
						else
							status = data[i]['status'];

						t.row.add([
							(i+1),
							changeDateTime(data[i]['waktu']),
							data[i]['rm_id'],
							data[i]['nama'],
							pengirim,
							data[i]['nama_dept'],
							status,
							action,
							i
						]).draw();
					}

				}
			});
		});

		$('#btn_filter_jaspel').click(function(event){
			event.preventDefault();
			var item = {};
			item['mulai'] = $('#mulai_date').val();
			item['sampai'] = $('#sampai_date').val();
			item['carabayar'] = $('#carabayar').val();
			item['paramedis'] = "";

			if($('#jpparamedis').val() != "")
				item['paramedis'] = $('#jpparamedis_id').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>fisioterapi/homelab/search_jasapelayanan',
				success:function(data){
					
					var t = $('#tabelJpPoliinap').DataTable();

					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						t.row.add([
							(i+1),
							data[i]['tanggal'],
							data[i]['nama_tindakan'],
							data[i]['cara_bayar'],
							data[i]['nama_petugas'],
							data[i]['jp'],
							i
						]).draw();
					}

					console.log(data);
				},error:function(data){
					alert('error');
					console.log(data);

				}
			});
		});

		$('#jpparamedis').focus(function(){
			var $input = $('#jpparamedis');
			var autodata = [];
			var iddata = [];

			if(autodata.length==0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
					success:function(data){
						console.log(data);
						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_petugas']);
							iddata.push(data[i]['petugas_id']);
						}
						console.log(autodata);

						$input.typeahead({source:autodata, 
				            autoSelect: true}); 

						$input.change(function() {
						    var current = $input.typeahead("getActive");
						    var index = autodata.indexOf(current);

						    $('#jpparamedis_id').val(iddata[index]);
						    
						    if (current) {
						        // Some item from your model is active!
						        if (current.name == $input.val()) {
						            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
						        } else {
						            // This means it is only a partial match, you can either add a new item 
						            // or take the active if you don't want new items
						        }
						    } else {
						        // Nothing is active so it is a new value (or maybe empty value)
						    }
						});
					}
				});
			}
		});

		$('#tambah_tindakan_penunjang').submit(function(e){
			e.preventDefault();
			var item = {};
			item['nama_tindakan'] = $('#tambah_nama_tindakan').val();
			item['kategori'] = $('#tambah_kategori').val();

			item['js_vip'] = $('#JSPjgLabVIP').val();
			item['jp_vip'] = $('#JPPjgLabVIP').val();
			item['bakhp_vip'] = $('#BAKHPPjgLabVIP').val();

			item['js_utama'] = $('#JSPjgLabUtama').val();
			item['jp_utama'] = $('#JPPjgLabUtama').val();
			item['bakhp_utama'] = $('#BAKHPPjgLabUtama').val();

			item['js_I'] = $('#JSPjgLabKlsI').val();
			item['jp_I'] = $('#JPPjgLabKlsI').val();
			item['bakhp_I'] = $('#BAKHPPjgLabKlsI').val();

			item['js_II'] = $('#JSPjgLabKlsII').val();
			item['jp_II'] = $('#JPPjgLabKlsII').val();
			item['bakhp_II'] = $('#BAKHPPjgLabKlsII').val();

			item['js_III'] = $('#JSPjgLabKlsIII').val();
			item['jp_III'] = $('#JPPjgLabKlsIII').val();
			item['bakhp_III'] = $('#BAKHPPjgLabKlsIII').val();

			// console.log(item);
			$.ajax({
				type:'POST',
				data:item,
				url:'<?=base_url()?>fisioterapi/homelab/save_tindakan_penunjang',
				success:function(data){
					myAlert('Data Berhasil Ditambahkan');
					$(':input','#tambah_tindakan_penunjang')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');

					console.log(data);
				},error:function(data){
					console.log(data);
				}
			});
		});

		//-------------------- master here ------------------//
		$('#tbody_master').on('change','tr td .bakhp', function(){
			//var total = $(this).closest('tr').find('.total').val();
			var js = $(this).closest('tr').find('.js').val();
			var jp = $(this).closest('tr').find('.jp').val();
			var bakhp = $(this).closest('tr').find('.bakhp').val();
			
			var ditotal = Number(js)+Number(jp)+Number(bakhp);

			$(this).closest('tr').find('.total').val(ditotal);
		})

		$('#tbody_master').on('change','tr td .jp', function(){
			//var total = $(this).closest('tr').find('.total').val();
			var js = $(this).closest('tr').find('.js').val();
			var jp = $(this).closest('tr').find('.jp').val();
			var bakhp = $(this).closest('tr').find('.bakhp').val();
			
			var ditotal = Number(js)+Number(jp)+Number(bakhp);

			$(this).closest('tr').find('.total').val(ditotal);
		})

		$('#tbody_master').on('change','tr td .js', function(){
			//var total = $(this).closest('tr').find('.total').val();
			var js = $(this).closest('tr').find('.js').val();
			var jp = $(this).closest('tr').find('.jp').val();
			var bakhp = $(this).closest('tr').find('.bakhp').val();
			
			var ditotal = Number(js)+Number(jp)+Number(bakhp);

			$(this).closest('tr').find('.total').val(ditotal);
		})

		//------------------------- copy ini kesemua --------------------------//

		$('#submit_icd9').submit(function(e){
			e.preventDefault();
			var item = {};
			item['search'] = $('#katakunci_icd9').val();

			$.ajax({
				type:'POST',
				data:item,
				url:"<?=base_url()?>laboratorium/homelab/search_icd9",
				success:function(data){
					$('#tbody_icd9').empty();

					for(var i=0; i<data.length; i++){
						$('#tbody_icd9').append(
							'<tr>'+
								'<td>'+data[i]['keterangan']+'</td>'+
								'<td style="text-align:center"><a style="cursor:pointer;" class="add_icd9"><i class="glyphicon glyphicon-check"><input type="hidden" class="kode_icd9" value="'+data[i]['id']+'"></i></a></td>'+
							'</tr>'
						);
					}
					console.log(data);
				}, error:function(data){
					console.log(data);
				}
			});
		})

		$('#tbody_icd9').on('click','.add_icd9', function(){
			var id = $(this).children('.kode_icd9').val();
			var keterangan = $(this).closest('tr').find('td').eq(0).text();
			
			$('#id_icd9_group').val(id);
			$('#icd9_group').val(keterangan);

			$('#searchICDIX').modal('hide');
		})
		// ------------------------ end of copy -------------------------------//
	});

	function get_listaps(){
		$.ajax({
			type:'POST',
			url:'<?php echo base_url() ?>fisioterapi/homelab/get_listaps',
			success:function(data){
				var t = $('#tableAPS').DataTable();
				t.clear().draw();

				for(var i = 0; i<data.length; i++){
					var action = '<td style="text-align:center"><a href="#tambahPeri" class="tambahlab" data-toggle="modal" ><i class="glyphicon glyphicon-check"></i></a></td>';

					t.row.add([
						(i+1),
						changeDateTime(data[i]['waktu']),
						data[i]['rm_id'],
						data[i]['nama'],
						data[i]['jenis_kelamin'],
						data[i]['jenis_periksa'],
						action,
						i
					]).draw();
				}
			}
		});
	}

	function get_listrujuk(){
		$.ajax({
			type:'POST',
			url:'<?php echo base_url() ?>fisioterapi/homelab/get_listrujuk',
			success:function(data){
				var t = $('#table_rujuk').DataTable();
				t.clear().draw();
				
				for(var i = 0; i<data.length; i++){
					var action = '<td style="text-align:center"><a href="#tambahPeri" class="tambahlab" data-toggle="modal" ><i class="glyphicon glyphicon-check"></i></a></td>';

					t.row.add([
						(i+1),
						data[i]['nama_dept'],
						changeDateTime(data[i]['waktu']),
						data[i]['rm_id'],
						data[i]['nama'],
						data[i]['nama_petugas'],
						data[i]['jenis_periksa'],
						action,
						i
					]).draw();
				}
			}
		});
	}

	function get_listlab(){
		$.ajax({
			type:'POST',
			url:'<?php echo base_url() ?>fisioterapi/homelab/get_listperiksa',
			success:function(data){
				var t = $('#tableHasil').DataTable();

				t.clear().draw();

				for(var i = 0; i<data.length; i++){
					var pengirim, status;

					if(data[i]['status'] != "Selesai"){
						var action = '<center><a href="#tambahHasil" data-toggle="modal" onclick="Periksa(&quot;'+data[i]['penunjang_id']+'&quot;)"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Pemeriksaan"></i></a>'+
									'<a href="#view" data-toggle="modal" ><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Lihat Detail"></i></a><center>';
					}
					else{
						var action = '<center><a href="#view" data-toggle="modal" onclick="LihatHasil(&quot;'+data[i]['penunjang_id']+'&quot;)"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Lihat Detail"></i></a></center>';
					}	

					if(!data[i]['nama_petugas'])
						pengirim = data[i]['pengirim'];
					else
						pengirim = data[i]['nama_petugas'];

					if(data[i]['status'] == "PROSES")
						status = "Belum";
					else
						status = data[i]['status'];

					t.row.add([
						(i+1),
						changeDateTime(data[i]['waktu']),
						data[i]['rm_id'],
						data[i]['nama'],
						pengirim,
						data[i]['nama_dept'],
						status,
						action,
						i
					]).draw();
				}

				$('#tambahHasil').modal('hide');

				console.log(data);
			}
		});
	}

	function changeDateTime(tgl_lahir){
		var getdate = tgl_lahir.split(" ");
		var remove = getdate[0].split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0];

		return tgl;
	}

	function changeDate(tgl_lahir){
		var remove = tgl_lahir.split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0];

		return tgl;
	}

	function pemeriksaan(penunjang){
		$('#penunjang_id').val(penunjang);
	}

	function Periksa(penunjang){
		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>fisioterapi/homelab/get_detailperiksa/'+penunjang,
			success:function(data){
				$('#phasil_rm').text(data['rm_id']);
				$('#phasil_penunjang_id').val(data['penunjang_id']);
				$('#phasil_nama').text(data['nama'])
				$('#phasil_alamat').text(data['alamat_skr']);
				$('#phasil_kelamin').text(data['jenis_kelamin']);
				$('#phasil_tlahir').text(changeDate(data['tanggal_lahir']));

				if(!data['nama_petugas']){
					$('#phasil_pengirim').text(data['pengirim']);
				}else{
					$('#phasil_pengirim').text(data['nama_petugas']);
				}

				var text = data['tanggal_lahir'];
				var from = text.split("-");
				var born = new Date(from[0], from[1] - 1, from[2]);

				$('#phasil_umur').text(getAge(born));

				$('#phasil_penunjang').val(data['nama_dept']);
			}
		});

		$.ajax({
			type:'POST',
			url:'<?php echo base_url() ?>fisioterapi/homelab/get_allperiksa/'+penunjang,
			success:function(data){
				$('#tbody_periksa').empty();

				for(var i=0; i<data.length; i++){
					if(!data[i]['pemeriksa']){
						$('#paramedis_id').val(data[i]['pemeriksa']);
						$('#paramedis').val(data[i]['nama_petugas']);
					}

					$('#tbody_periksa').append(
						'<tr>'+
							'<td style="text-align:center">'+(i+1)+'</td>'+
							'<td>'+data[i]['nama_tindakan']+'</td>'+
							'<td>'+
								'<input type="hidden" class="penunjang_detail_id" value="'+data[i]['penunjang_detail_id']+'">'+
								'<input type="text" class="form-control hasil" name="inputPeriksa" value="'+data[i]['hasilp']+'" placeholder="Hasil">'+
							'</td>'+
							'<td>'+
								'<input type="text" class="form-control nilai" name="inputNormal" value="'+data[i]['nilaip']+'" placeholder="Nilai Normal">'+
							'</td>'+
							'<td>'+
								'<input type="text" class="form-control onfaktur" name="inputNormal" value="'+data[i]['on_faktur']+'" placeholder="Nilai Normal">'+
							'</td>'+
							'<td>'+
								'<input type="text" class="form-control keterangan" name="inputKeterangan" value="'+data[i]['keteranganp']+'" placeholder="Keterangan">'+
							'</td>'+
						'</tr>'
					);
				}
			}
		});
	}

	function LihatHasil(penunjang){

		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>fisioterapi/homelab/get_detailperiksa/'+penunjang,
			success:function(data){
				console.log(data);
				$('#lhasil_rm').text(data['rm_id']);
				$('#lhasil_nama').text(data['nama'])
				$('#lhasil_alamat').text(data['alamat_skr']);
				$('#lhasil_jenis').text(data['jenis_kelamin']);
				$('#lhasil_tlahir').text(changeDate(data['tanggal_lahir']));

				if(!data['nama_petugas']){
					$('#lhasil_pengirim').text(data['pengirim']);
				}else{
					$('#lhasil_pengirim').text(data['nama_petugas']);
				}

				var text = data['tanggal_lahir'];
				var from = text.split("-");
				var born = new Date(from[0], from[1] - 1, from[2]);

				$('#lhasil_umur').text(getAge(born));

				$('#lhasil_penunjang').val(data['nama_dept']);
			}
		});

		$.ajax({
			type:'POST',
			url:'<?php echo base_url() ?>fisioterapi/homelab/get_allperiksa/'+penunjang,
			success:function(data){
				$('#tbody_lihathasil').empty();

				for(var i=0; i<data.length; i++){
					$('#lhasil_pemeriksa').val(data[i]['nama_petugas']);
					$('#lhasil_status').val(data[i]['status_p']);

					$('#tbody_lihathasil').append(
						'<tr>'+
							'<td style="text-align:center">'+(i+1)+'</td>'+
							'<td>'+data[i]['nama_tindakan']+'</td>'+
							'<td style="text-align:right">'+data[i]['hasilp']+'</td>'+
							'<td style="text-align:right">'+data[i]['nilaip']+'</td>'+
							'<td>'+data[i]['keteranganp']+'</td>'+
						'</tr>'
					);
				}
			}
		})

		document.getElementById('inAction').innerHTML = '<button type="reset" class="btn btn-warning" data-dismiss="modal">Kembali</button><a href="<?=base_url()?>laboratorium/homelab/cetak_hasil/'+penunjang+'/FISIOTERAPI" target="_blank"><button type="button" class="btn btn-info">Cetak</button></a>';
	}

	function getAge(date) {
		  var now = new Date();
		  var today = new Date(now.getYear(),now.getMonth(),now.getDate());

		  var yearNow = now.getYear();
		  var monthNow = now.getMonth();
		  var dateNow = now.getDate();

		  var dob = date;

		  var yearDob = dob.getYear();
		  var monthDob = dob.getMonth();
		  var dateDob = dob.getDate();
		  var age = {};
		  var ageString = "";
		  var yearString = "";
		  var monthString = "";
		  var dayString = "";


		  yearAge = yearNow - yearDob;

		  if (monthNow >= monthDob)
		    var monthAge = monthNow - monthDob;
		  else {
		    yearAge--;
		    var monthAge = 12 + monthNow -monthDob;
		  }

		  if (dateNow >= dateDob)
		    var dateAge = dateNow - dateDob;
		  else {
		    monthAge--;
		    var dateAge = 31 + dateNow - dateDob;

		    if (monthAge < 0) {
		      monthAge = 11;
		      yearAge--;
		    }
		  }

		  age = {
		      years: yearAge,
		      months: monthAge,
		      days: dateAge
		      };

			  if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
			    ageString = age.years +" Tahun  ";
			  else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
			    ageString =  age.days + " Hari.";
			  else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
			    ageString = age.years + " Tahun.";
			  else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
			    ageString = age.years+" Tahun ";
			  else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
			    ageString = age.months + " Bulan ";
			  else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
			    ageString = age.years + " Tahun ";
			  else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
			    ageString = age.months + " Bulan.";
			  else ageString = "Belum lahir";

			  return ageString;
	}

	function submit_filter (filter) {
		$.ajax({
			type: "POST",
			data: filter,
			url: "<?php echo base_url()?>fisioterapi/homelab/submit_filter_farmasi",
			success:function (data) {
				console.log(data);
				$('#tbodyinventoriunit').empty();
				var t = $('#tabelinventoriunit').DataTable();

				t.clear().draw();
				for (var i =  0; i < data.length; i++) {
					var last = '<a href="#" class="inoutobat" data-toggle="modal" data-target="#inout"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
								'<a href="#edInvenBer" data-toggle="modal" class="printobat"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>'+
								'<input type="hidden" class="barangmerk_id" value="'+data[i]['merk_id']+'">'+
								'<input type="hidden" class="barangjenis_obat_id" value="'+data[i]['jenis_obat_id']+'">'+
								'<input type="hidden" class="barangsatuan_id" value="'+data[i]['satuan_id']+'">'+
								'<input type="hidden" class="barangobat_dept_id" value="'+data[i]['obat_dept_id']+'">';
					var tgl_kadaluarsa = format_date(data[i]['tgl_kadaluarsa']);
					t.row.add([
						(Number(i+1)),
						data[i]['nama'],
						data[i]['no_batch'],
						data[i]['harga_jual'],
						data[i]['nama_merk'],
						data[i]['total_stok'],
						data[i]['satuan'],								
						tgl_kadaluarsa,
						last
					]).draw();
				}

				t.on( 'order.dt search.dt', function () {
			        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			            cell.innerHTML = i+1;
			        } );
			    } ).draw();

				$('[data-toggle="tooltip"]').tooltip();
					
			},
			error:function (data) {
				console.log(data);
			}
		})
	}

	function setStatus(departmen){
		var u = "<?php echo base_url() ?>fisioterapi/";
		localStorage.setItem('department', departmen);
		localStorage.setItem('url', u);
		window.location.href="<?php echo base_url() ?>invoice/tambahinvoice";
	}
	
</script>							