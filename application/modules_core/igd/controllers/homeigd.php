<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homeigd extends Operator_base {

	function __construct(){

		parent:: __construct();
		$this->load->model("m_homeigd");
		$this->load->model('logistik/m_gudangbarang');
		$this->load->model("rawatjalan/m_homerawatjalan");
		$this->load->model("bersalin/m_homebersalin");
		$this->load->model('rekammedis/m_olahrekammedis');
		$data['page_title'] = "IGD";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$data['content'] = 'home';
		$data['pasien_igd'] = $this->m_homeigd->get_antrian_pasien();

		$dept = $this->m_homerawatjalan->get_dept_id("IGD");
		$data['dept_id'] = $dept['dept_id'];
		$data['obatunit'] = $this->m_homerawatjalan->get_obat_unit($dept['dept_id']);
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($dept['dept_id']);
		$data['jasalayan'] = $this->m_homeigd->get_jasapelayanan($dept['dept_id']);
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}

	public function search_pasien(){
		$insert = $_POST['search'];

		$result = $this->m_homeigd->search_pasien($insert);

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	public function search_antrian(){
		$result = $this->m_homeigd->get_antrian_pasien();

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	public function search_tagihan(){
		$search = $_POST['search'];

		$result = $this->m_homeigd->search_tagihan($search);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function submit_permintaan_bersalin($value='')
	{
		$this->form_validation->set_rules('no_permintaan', 'nomor permitaan', 'required|trim|xss_clean|is_unique[obat_permintaan.no_permintaan]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaan'] = $_POST['no_permintaan'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$dept = $this->m_homerawatjalan->get_dept_id("IGD");
			$insert['dept_id'] = $dept['dept_id'];

			$val = $_POST['data'];
			$result = $this->m_homerawatjalan->insert_permintaan($insert);
			if($result){
				foreach ($val as $key) {
					$ins['obat_id'] = $key[1];
					$ins['obat_detail_id'] = $key[0];
					$ins['jumlah_request'] =  $key[9];
					$ins['obat_permintaan_id'] = $result;

					$elny = $this->m_homerawatjalan->insert_detail_permintaan($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_retur_bersalin()
	{
		$this->form_validation->set_rules('no_returdept', 'Nomor Retur', 'required|trim|xss_clean|is_unique[obat_retur_dept.no_returdept]');
		$this->form_validation->set_message('is_unique', 'Nomor Retur sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['status'] = 'belum diterima';
			$insert['no_returdept'] = $_POST['no_returdept'];
			$dept = $this->m_homerawatjalan->get_dept_id("IGD");
			$insert['dept_id'] = $dept['dept_id'];	
			$insert['petugas_input'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['keterangan'] = $_POST['keterangan'];
			$tgl =  DateTime::createFromFormat('d/m/Y H:i',$_POST['waktu']);
			$insert['waktu'] = $tgl->format('Y-m-d H:i');

			$val = $_POST['data'];

			$id = $this->m_homerawatjalan->submit_retur_dept($insert);
			if ($id) {
				foreach ($val as $key) {
					$ins['retur_dept_id'] = $id;
					$ins['obat_detail_id'] = $key[0];
					$ins['jumlah'] = $key[8];

					$res = $this->m_homerawatjalan->insert_detail_returdept($ins);
					//ubah stok di gudang dan unit, yang ubah gudang bukan unit :D
				}

				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function get_obat_retur()
	{
		$katakunci = $_POST['katakunci'];
		$dept = $this->m_homerawatjalan->get_dept_id("IGD")['dept_id'];
		$result = $this->m_homebersalin->get_obat_farmasi_unit($katakunci, $dept);

		header('Content-Type: application/json');
	 	echo json_encode($result); 
	}

	public function submit_permintaan_barangunit($value='')
	{
		$this->form_validation->set_rules('no_permintaanbarang', 'nomor permitaan', 'required|trim|xss_clean|is_unique[barang_permintaan.no_permintaanbarang]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaanbarang'] = $_POST['no_permintaanbarang'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$dept = $this->m_homerawatjalan->get_dept_id("IGD");
			$insert['dept_id'] = $dept['dept_id'];

			$val = $_POST['data'];
			$result = $this->m_homerawatjalan->insert_permintaanbarang($insert);
			if($result){
				foreach ($val as $key) {
					$ins['barang_id'] = $key[8];
					$ins['barang_stok_id'] = $key[7];
					$ins['jumlah_request'] =  $key[9];
					$ins['barang_permintaan_id'] = $result;

					$elny = $this->m_homerawatjalan->insert_detail_permintaanbarang($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function get_detail_inventori($id)
	{
		$dept = $this->m_homerawatjalan->get_dept_id("IGD");
		$dept_id = $dept['dept_id'];
		$res = $this->m_gudangbarang->get_detail_inventori($id, $dept_id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function input_in_outbarang($value='')
	{
		$insert['barang_detail_id'] = $_POST['barang_detail_id'];
		$tgl = DateTime::createFromFormat('d/m/Y H:i', $_POST['tanggal']);
		$insert['tanggal'] = $tgl->format('Y-m-d H:i');
		$insert['is_out'] = $_POST['is_out'];
		$insert['jumlah'] = $_POST['jumlah'];
		$insert['keterangan'] = $_POST['keterangan'];
		$dept = $this->m_homerawatjalan->get_dept_id("IGD");
		$insert['barang_dept_id'] = $dept['dept_id'];

		$res = $this->m_gudangbarang->input_in_out($insert);
		if ($res) {
			$ins['barang_detail_id'] = $_POST['barang_detail_id'];
			$ins['dept_id'] = $dept['dept_id'];
			$ins['stok'] = $_POST['sisa'];
			$ins['tanggal_stok'] = date('Y-m-d H:i:s');
			$ins['keterangan_stok'] = "IN - OUT";

			$res = $this->m_gudangbarang->input_riwayat_out($ins);
			if ($res) {
				$message = "true";
			}else{
				$message = "false";
			}
		}else{
			$message = "false";
		}

		header('Content-Type: application/json');
	 	echo(json_encode($message));
	}

	public function excel_barang_unit()
	{
		$dept_id = $this->m_homerawatjalan->get_dept_id("IGD")['dept_id'];
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($dept_id);
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];
		$data['awal'] = date('d F Y');

		$this->load->view('bersalin/excel_inventori',$data);
	}

	public function search_jasapelayanan(){
		$mulai = $this->date_db($_POST['mulai']);
		$sampai = $this->date_db($_POST['sampai']);
		$carabayar = $_POST['carabayar'];
		$paramedis = $_POST['paramedis'];
		$dept = $this->m_homerawatjalan->get_dept_id("IGD")['dept_id'];

		$result = $this->m_homeigd->search_jasapelayanan($dept, $mulai, $sampai, $carabayar, $paramedis);		

		//echo $result;
		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}


	public function print_jaspel()
	{
		$mulai = $this->fdate_db($this->input->post('start'));
		$sampai = $this->fdate_db($this->input->post('end'));
		$carabayar = $this->input->post('cara_bayar');
		$paramedis = $this->input->post('id_petugas_jaspel');
		$dept = $this->m_homerawatjalan->get_dept_id("IGD")['dept_id'];

		$result = $this->m_homeigd->search_jasapelayanan($dept, $mulai, $sampai, $carabayar, $paramedis);

		$res = [];
		$i = 0;
		foreach ($result as $value) {
			$value['tanggal'] = substr($value['tanggal'],0,10);
			$res[i] = $value;
		}
		
		$data['jaspel'] = $res;
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];
		$data['awal'] = $this->input->post('start');
		$data['akhir'] = $this->input->post('end');

		$this->load->view('bersalin/excel_jaspel', $data);
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}
}
