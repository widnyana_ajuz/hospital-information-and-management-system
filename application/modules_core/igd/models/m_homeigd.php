<?php

class m_homeigd extends CI_Model {

// function __construct() {
//         // Call the Model constructor
//         parent::__construct();
//     }
    public function search_pasien($search){
        $sql = "SELECT * FROM pasien p, visit v, visit_igd r LEFT JOIN master_dept md ON r.unit_asal = md.dept_id WHERE p.rm_id = v.rm_id AND v.visit_id = r.visit_id AND v.status_visit = 'REGISTRASI' AND r.waktu_keluar is NULL AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '%$search%')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_antrian_pasien(){
        $sql = "SELECT * FROM pasien p, visit v, visit_igd r LEFT JOIN master_dept md ON r.unit_asal = md.dept_id WHERE p.rm_id = v.rm_id AND v.visit_id = r.visit_id AND v.status_visit = 'REGISTRASI' AND r.waktu_keluar is NULL ORDER BY r.waktu_masuk ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

	public function get_pasien($rm_id)
    {
    	$sql = "SELECT * FROM pasien where rm_id = ?";
    	//$this->db->where($rm_id);
    	$query = $this->db->query($sql,$rm_id);
    	if ($query) {
    		return $query->row_array();
    	}else{
    		return false;
    	}
    }

    public function search_tagihan($search){
        $sql = "SELECT *, t.cara_bayar as carapembayaran FROM tagihan t, pasien p, visit v, visit_igd vr, master_dept m 
                WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.sub_visit = vr.igd_id AND vr.unit_tujuan = m.dept_id
                AND vr.visit_id = v.visit_id AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '%$search%') AND v.status_visit <> 'DONE'
                ";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_dept_id($select){
        $sql = "SELECT dept_id FROM master_dept WHERE nama_dept = '$select' LIMIT 1";
        $query = $this->db->query($sql);
        if ($query) {
            return $query->row_array();
        }else{
            return false;
        }        
    }

    public function get_jasapelayanan($dept_id){
        $sql = "SELECT v.waktu_tindakan, mt.nama_tindakan ,v.care_id, vr.cara_bayar, p.nama_petugas, v.jp, v.paramedis_lain FROM visit_care v, visit_igd vr, petugas p, master_tindakan mt, master_tindakan_detail mtd WHERE v.paramedis = p.petugas_id AND v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = vr.igd_id AND v.dept_id = $dept_id";
        return $this->db->query($sql)->result_array();  
    }

    public function search_jasapelayanan($dept, $mulai, $sampai, $carabayar, $paramedis){
        if($carabayar != "" && $paramedis != ""){
            $sql = "SELECT v.waktu_tindakan as tanggal, mt.nama_tindakan ,v.care_id, vr.cara_bayar, p.nama_petugas, v.paramedis_lain, v.jp FROM visit_care v, visit_igd vr, petugas p, master_tindakan mt, master_tindakan_detail mtd WHERE v.paramedis = p.petugas_id AND v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = vr.igd_id AND v.dept_id = $dept  AND v.paramedis = '$paramedis' AND vr.cara_bayar = '$carabayar' AND v.waktu_tindakan BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59'";
        }
        else if($carabayar == '' && $paramedis != ''){
            $sql = "SELECT v.waktu_tindakan as tanggal, mt.nama_tindakan ,v.care_id, vr.cara_bayar, p.nama_petugas, v.paramedis_lain, v.jp FROM visit_care v, visit_igd vr, petugas p, master_tindakan mt, master_tindakan_detail mtd WHERE v.paramedis = p.petugas_id AND v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = vr.igd_id AND v.dept_id = $dept  AND v.paramedis = '$paramedis' AND v.waktu_tindakan BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59' ";
        }else if($paramedis == '' && $carabayar != ''){
            $sql = "SELECT v.waktu_tindakan as tanggal, mt.nama_tindakan ,v.care_id, vr.cara_bayar, p.nama_petugas, v.paramedis_lain, v.jp FROM visit_care v, visit_igd vr, petugas p, master_tindakan mt, master_tindakan_detail mtd WHERE v.paramedis = p.petugas_id AND v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = vr.igd_id AND v.dept_id = $dept AND vr.cara_bayar = '$carabayar' AND v.waktu_tindakan BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59'";
        }else{
            $sql = "SELECT v.waktu_tindakan as tanggal, mt.nama_tindakan ,v.care_id, vr.cara_bayar, p.nama_petugas, v.paramedis_lain, v.jp FROM visit_care v, visit_igd vr, petugas p, master_tindakan mt, master_tindakan_detail mtd WHERE v.paramedis = p.petugas_id AND v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = vr.igd_id AND v.dept_id = $dept AND v.waktu_tindakan BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59' ";
        }

        //return $sql;
        return $this->db->query($sql)->result_array();          
    }
}

?>