<br>
<div class="title" id="rowfix" style="position:fixed; z-index:10; width:98.5%">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>igd/homeigd"> IGD</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>igd/igddetail"><?php echo $pasien['nama']; ?> / <?php  
								$datetime1 = new DateTime();
								$datetime2 = new DateTime($pasien['tanggal_lahir']);
								$interval = $datetime1->diff($datetime2);
																
								if($interval->y > 0)
									echo $interval->y ." Tahun ";
								
							?> / <?php echo $pasien['jenis_kelamin']; ?></a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Identitas Pasien</a>
	</li>
</div>

<input type="hidden" id="v_id" value="<?php echo $visit_id; ?>">
<input type="hidden" id="igd_id" value="<?php echo $igd_id; ?>">
<input type="hidden" id="dept_id" value="<?php echo $dept_id; ?>">

<div class="navigation" style="margin-left: 10px; margin-top:100px;" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	    <li class="active"><a class="cl" href="#identitas" class="cl" data-toggle="tab">Identitas Pasien</a></li>
	    <li ><a href="#ok" class="cl" data-toggle="tab">Overview Klinik</a></li>
	    <li><a href="#rm" class="cl" data-toggle="tab">Overview IGD</a></li>
	    <li><a href="#resep" class="cl" data-toggle="tab">Pemberian Resep</a></li>
	    <li><a href="#penunjang" class="cl" data-toggle="tab">Pemeriksaan Penunjang</a></li>
	    <li><a href="#orderkamar" class="cl" data-toggle="tab">Order Kamar Operasi</a></li>
	    <li><a href="#konsul" class="cl" data-toggle="tab">Order Konsultasi Gizi</a></li>
	    <li><a href="#riwayat" class="cl" data-toggle="tab">Riwayat Penyakit</a></li>
	    <li><a href="#resume" class="cl" data-toggle="tab">Resume Pulang</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
		<div class="tab-pane active" id="identitas">
    		<div class="dropdown">
        		<div id="titleInformasi">Identitas Pasien</div>
 			</div>
          	
            <div class="informasi" id="info1">
	            <form class="form-horizontal" role="form">
	            	
	           		<div class="form-group">
						<label class="control-label col-md-3" >Jenis Identitas Pasien</label>
						<div class="col-md-1">
							<input  id="jnsIdentitas" name="jenis_identitas" value="<?php echo $pasien['jenis_id']; ?>"style="border:0px;background-color:transparent;font-weight:bold" disabled />
						</div>					
						
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3" >Nomor Identitas Pasien</label>
						<div class="col-md-3">
							<input  id="NomorID" name="nomor_identitas" value="<?php echo $pasien['no_id']; ?>" style="border:0px;background-color:transparent;font-weight:bold" disabled />
						</div>
					</div>	
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">

					<div class="form-group">
						<label class="control-label col-md-3">No RM</label>
						<div class="col-md-4">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="rm_id" name="rm_id" value="<?php echo $pasien['rm_id']; ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">

					<div class="form-group">
						<label class="control-label col-md-3">Nama Lengkap </label>
						<div class="col-md-4">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="NamaLengkap" name="NamaLengkap" value="<?php echo $pasien['nama']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Alias </label>
						<div class="col-md-1">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="alias" name="alias" value="<?php echo $pasien['alias']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Jenis Kelamin</label>
						<div class="col-md-1">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="jk" name="jk" value="<?php echo $pasien['jenis_kelamin']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Golongan Darah </label>
						<div class="col-md-1">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="goldarah" name="goldarah" value="<?php echo $pasien['gol_darah']; ?>" disabled />												
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Agama </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="agama" name="agama" value="<?php echo $pasien['agama']; ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">

					<div class="form-group">
						<label class="control-label col-md-3">Tempat Lahir </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="newTempatLahir" name="tempat_lahir" value="<?php echo $pasien['tempat_lahir']; ?>" disabled/>
						</div>
																		
					</div>			
					<div class="form-group">
						<label class="control-label col-md-3">Tanggal Lahir </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" class="input-medium date-picker" maxlength="12" type="text" value="<?php
								$tgl = strtotime($pasien['tanggal_lahir']);
								$hasil = date('d F Y', $tgl); 
								echo $hasil; ?>" 
								data-date-format="dd/mm/yyyy" id="TanggalLahir" placeholder="Tanggal Lahir" disabled />
						</div>												
					</div>			

					<div class="form-group">
						<label class="control-label col-md-3">Umur</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="umur" name="umur" 
							value="<?php  
								$datetime1 = new DateTime();
								$datetime2 = new DateTime($pasien['tanggal_lahir']);
								$interval = $datetime1->diff($datetime2);
																
								if($interval->y > 0)
									echo $interval->y ." tahun ";
								if($interval->m > 0)
									echo $interval->m." bulan ";
								if($interval->d > 0)
									echo $interval->d ." hari";
							?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">

					<div class="form-group">
						<label class="control-label col-md-3">Status Kawin</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="status kawin" name="statuskawin" value="<?php echo $pasien['status_perkawinan']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Pendidikan Terakhir</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="pendidikan" name="pendidikan" value="<?php echo $pasien['pendidikan']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Pekerjaan </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="Pekerjaan" name="pekerjaan" value="<?php echo $pasien['pekerjaan']; ?>" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Nomor Telepon</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="nomorPasien" name="nomor_pasien" value="<?php echo $pasien['no_telp']; ?>" disabled />
						</div>						
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">

					<div class="form-group">
						<label class="control-label col-md-3">Alamat </label>
						<div class="col-md-5">
							<input style="border:0px;background-color:transparent;width:900px;font-weight:bold" id="Alamat" name="alamat" value="<?php echo $pasien['alamat_skr']; ?>" disabled />
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Alamat KTP</label>
						<div class="col-md-5">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="AlamatKTP" name="alamatKTP" value="<?php echo $pasien['alamat_ktp']; ?>" disabled />
						</div>						
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Wilayah </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="provinsi" name="provinsi" value="<?php echo $pasien['nama_prov']; ?>" disabled />
						</div>											
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kabupaten </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="kabupaten" name="kabupaten" value="<?php echo $pasien['nama_kab']; ?>" disabled />
						</div>												
						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kecamatan </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="kecamatan" name="kecamatan" value="<?php echo $pasien['nama_kec']; ?>" disabled />
						</div>
						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kelurahan </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="kelurahan" name="kelurahan" value="<?php echo $pasien['kel_id_skr']; ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
						
					<div class="form-group">
						<label class="control-label col-md-3">Cara Pembayaran</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="cara_bayar" name="cara_bayar" value="<?php echo $pasien['cara_bayar']; ?>" disabled />
						</div>						
					</div>
				</form>
			</div>
			<br><br>
		</div>

		<div class="tab-pane" id="rm"> 
			<div class="dropdown" id="btnBawahTambahCare">
		        <div id="titleInformasi">Penanganan IGD  
		        </div>
		        <div class="btnBawah floatright" style="margin-top:-25px;">
		           	<i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i>
		        </div>
		   	</div>
			<div class="informasi" id="tbhCare">
	 			<form class="form-horizontal" role="form" method="POST" id="submitOverIGD">
			       	<br>
	 				<div class="form-group">
						<label class="control-label col-md-3">Waktu</label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="date_igd" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii:ss" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i:s");?>">
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Anamnesa</label>
						<div class="col-md-4">
							<textarea required class="form-control isian" id="anamnesa_igd" name="anamnesa" placeholder="Anamnesa" required></textarea>
						</div>
					</div>

					<fieldset class="fsStyle">
						<legend>
			                Tanda Vital
						</legend>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Tekanan Darah</label>
							<div class="col-md-2">
								<input type="text" required class="form-control" id="tekanandarah_igd" name="takanandarah" placeholder="Tekanan Darah" >
							</div>
							<label class="control-label col-md-2">mmHg</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Temperatur</label>
							<div class="col-md-2">
								<input type="number" required class="form-control" id="temperatur_igd" name="temperatur" placeholder="Temperatur" >
							</div>
							<label class="control-label col-md-2">&deg;C</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Nadi</label>
							<div class="col-md-2">
								<input type="number" required class="form-control" id="nadi_igd" name="nadi" placeholder="Nadi" >
							</div>
							<label class="control-label col-md-2">x/menit</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Pernapasan</label>
							<div class="col-md-2">
								<input type="number" required class="form-control" id="pernapasan_igd" name="pernapasan" placeholder="Pernapasan" >
							</div>
							<label class="control-label col-md-2">x/menit</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Berat Badan</label>
							<div class="col-md-2">
								<input type="number" required class="form-control" id="berat_igd" name="berat" placeholder="Berat Badan" >
							</div>
							<label class="control-label col-md-2">Kg</label>
						</div>
			  		</fieldset>

			  		<fieldset class="fsStyle">
						<legend>
			                Pemeriksaan Fisik
						</legend>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Kepala & Leher</label>
							<div class="col-md-5">
								<textarea required class="form-control" id="kepala_igd" name="kepala" placeholder="Hasil Pemeriksaan" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Thorax & ABD</label>
							<div class="col-md-5">
								<textarea required class="form-control" id="thorax_igd" name="thorax" placeholder="Hasil Pemeriksaan" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Extremitas</label>
							<div class="col-md-5">
								<textarea required class="form-control" id="ex_igd" name="ex" placeholder="Hasil Pemeriksaan" ></textarea>
							</div>
						</div>

					</fieldset>

			  		<fieldset class="fsStyle">
						<legend>
			                Diagnosa & Terapi
						</legend>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Dokter Jaga</label>
							<div class="col-md-3">
								<input type="hidden" id="dokter_igd">
								<input type="text" required class="form-control" style="cursor:pointer;background-color:white" readonly id="ndokter_igd" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Perawat Jaga</label>
							<div class="col-md-3">
								<input type="hidden" id="perawat_igd">
								<input type="text" required class="form-control" style="cursor:pointer;background-color:white" readonly id="nperawat_igd" placeholder="Search Perawat" data-toggle="modal" data-target="#searchPerawat">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Diagnosa Utama</label>
							<div class="col-md-1">
								<input type="text" required style="cursor:pointer;background-color:white" class="form-control isian di1" id="kode_utama_igd" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" required style="cursor:pointer;background-color:white" class="form-control isian di1" id="nama_utama_igd" placeholder="Nama Diagnosa" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Diagnosa Sekunder</label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian di2" id="kode_sek1_igd" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian di2" id="nama_sek1_igd" placeholder="Nama Diagnosa" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<label class="control-label">1</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;"></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian di3" id="kode_sek2_igd" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian di3" id="nama_sek2_igd" placeholder="Nama Diagnosa" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<label class="control-label">2</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;"></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian di4" id="kode_sek3_igd" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian di4" id="nama_sek3_igd" placeholder="Nama Diagnosa" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<label class="control-label">3</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;"></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian di5" id="kode_sek4_igd" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian di5" id="nama_sek4_igd" placeholder="Nama Diagnosa" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<label class="control-label">4</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Detail Diagnosa</label>
							<div class="col-md-4">
								<textarea class="form-control" id="detailDiagnosa_igd" name="detailDiagnosa" placeholder="Detail Diagnosa" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Terapi</label>
							<div class="col-md-4">
								<textarea class="form-control" id="terapi_igd" name="terapi" placeholder="Terapi" ></textarea>
							</div>
						</div>
						<!-- <div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Alergi</label>
							<div class="col-md-3">
								<input type="text" class="form-control isian" id="alergi" name="alergi" placeholder="Alergi">
							</div>
						</div> -->
			  		</fieldset>

					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>
			</div>
			<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:20px;margin-left:20px;">
			<div class="portlet-body" style="margin: 20px 10px 0px 10px">
				<?php
					echo "<input type='hidden' id='jml_overigd' value='".count($igd_overview_history)."' >";
				?>
				<table id="tableOverviewIGD" class="table table-striped table-bordered table-hover table-responsive tableDTUtama">
					<thead>
						<tr class="info" style="text_align: center;">
							<th width="20">No.</th>
							<th>Waktu</th>
							<th>Anamnesa</th>
							<th>Dokter Jaga</th>
							<th>Perawat Jaga</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							if(!empty($igd_overview_history)){
								foreach ($igd_overview_history as $over) {
									$i++;
									echo'<tr>';
										echo'<td>'.$i.'</td>';
										echo'<td>IGD</td>';
										echo'<td>'.$over['anamnesa'].'</td>';
										echo'<td>'.$over['nama_dokter'].'</td>';
										echo'<td>'.$over['nama_perawat'].'</td>';
										echo'<td style="text-align:center;"><a href="#riwayat_igd" data-toggle="modal" onClick="detailOverIGD(&quot;'.$over['id'].'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>';
									echo'</tr>';
								}
							}
						?>
					</tbody>
				</table>
			</div>

		 	<div class="dropdown" id="btnBawahCare">
		 	  	<div id="titleInformasi" >Uraian Tindakan IGD</div>
		        <div id="btnBawahCare" class="btnBawah floatright"  style="margin-top:-25px;"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>

	    <div class="modal fade" id="tambahTindakan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakanIGD">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tindakan Perawatan</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="tin_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
									</div>
			        			</div>
			        			<div class="form-group">
									<label class="control-label col-md-4">Tindakan</label>
									<div class="col-md-6">
										<input type="hidden" id="idtindakan_klinik">
										<input type="text" class="form-control" id="namatindakan" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Search Tindakan" required>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"></label>
									<div class="col-md-6">
										<textarea class="form-control" id="namatindakanarea" readonly placeholder="Tindakan"></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Kelas Pelayanan</label>
									<div class="col-md-5">	
										<input type="hidden" id="idtindakdetail">
										<select class="form-control" name="kelas_tindakan" id="kelas_klinik" required>
											<option value="">Pilih Kelas</option>
											<option value="Kelas VIP">VIP</option>
											<option value="Kelas Utama">Utama</option>
											<option value="Kelas I">Kelas I</option>
											<option value="Kelas II">Kelas II</option>
											<option value="Kelas III">Kelas III</option>
										</select>
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="hidden" id="js_klinik">
										<input type="hidden" id="jp_klinik">
										<input type="hidden" id="bakhp_klinik">
										<input type="text" class="form-control" id="tarif" name="tarif" placeholder="Tarif" readonly > 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="number" class="form-control" id="onfaktur" name="onfaktur" placeholder="On Faktur" required >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Jumlah</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" readonly>
									</div>
			        			</div>

								<div class="form-group">
									<label class="control-label col-md-4">Paramedis</label>
									<div class="col-md-5">	
										<input type="hidden" id="paramedis_id">
										<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
									</div>
			        			</div>
			        			<div class="form-group">
									<label class="control-label col-md-4">Paramedis Lain</label>
									<div class="col-md-6">	
										<textarea class="form-control" id="paramedis_lain" placeholder="Paramedis Lain"></textarea>
									</div>
			        			</div>
			        			
		        			</div>
	       				</div>
		        		<br><br>
		        		<div class="modal-footer">
		        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
		 			     	<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

			<div class="tabelinformasi" id="tabelcare">
				<form class="form-horizontal" role="form" method="POST" style="margin-left:20px;margin-right:20px;">
					<!-- <div class="form-group">
						<br>
						<a href="#tambahTindakan" data-toggle="modal"  style="margin-left:15px;font-size:11pt;"	><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah">&nbsp;Tambah Pengadaan</i></a>
						<div class="clearfix"></div>        
					</div> -->

					<div class="form-group">
						<a href="#tambahTindakan" data-toggle="modal"  style="margin-left:15px;font-size:11pt;"	><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah">&nbsp;Tambah Pengadaan</i></a>
						<div class="clearfix"></div>
					</div>

				    <div class="form-group">
				        <div class="portlet-body" style="margin: 0px 10px 0px 10px">
				        	<?php
					        	echo '<input type="hidden" id="jml_tindak_igd" value="'.count($visit_care_igd).'" >';
					        ?>
				            <table class="table table-striped table-bordered tableDTUtama table-hover" id="tableCareIGD">
								<thead>
									<tr class="info">
										<th style="width:10px;">No.</th>
										<th>Waktu</th>
										<th>Tindakan</th>
										<th>Jasa Sarana</th>
										<th>Jasa Pelayanan</th>
										<th>BAKHP</th>
										<th>On faktur</th>
										<th>Paramedis</th>
										<th>Jumlah</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<?php  
										if (!empty($visit_care_igd)) {
											$i = 0;
											foreach($visit_care_igd as $value){
												$i++;
												echo "<tr>";
													echo "<td>".$i."</td>";										
													echo "<td>".$value['waktu_tindakan']."</td>";									
													echo "<td>".$value['nama_tindakan']."</td>";												
													echo "<td>".$value['j_sarana']."</td>";										
													echo "<td>".$value['j_pelayanan']."</td>";
													echo "<td>".$value['bakhp_this']."</td>";										
													echo "<td>".$value['on_faktur']."</td>";
													echo "<td>".$value['nama_petugas']."</td>";										
													echo "<td>".$value['jumlah']."</td>";
													echo "<td style='text-align:center'><a style='cursor:pointer;' class='hapusTindakan'><input type='hidden' class='getid' value='".$value['care_id']."''><i class='glyphicon glyphicon-trash'></i></a></td>";
												echo "</tr>";
											}
										}
									?>	
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>
			<br>
	     
	        <div class="modal fade" id="riwayat_igd" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="riwkondok1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Penanganan IGD</h3>
				   			</div>
							<div class="modal-body" style="padding-left:80px;">

				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="imod_date" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
									</div>
			        			</div>	
			        			<div class="form-group">
									<label class="control-label col-md-4">Anamnesa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="imod_anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
									</div>
								</div>

								<fieldset class="fsStyle">
									<legend>
						                Tanda Vital
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Tekanan Darah</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_tekanandarah" name="takanandarah" placeholder="Tekanan Darah" readonly>
										</div>
										<label class="control-label col-md-2">mmHg</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Temperatur</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_temperatur" name="temperatur" placeholder="Temperatur" readonly>
										</div>
										<label class="control-label col-md-2">&deg;C</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Nadi</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_nadi" name="nadi" placeholder="Nadi" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Pernapasan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Berat Badan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_berat" name="berat" placeholder="Berat Badan" readonly>
										</div>
										<label class="control-label col-md-2">Kg</label>
									</div>
						  		</fieldset>

						  		<fieldset class="fsStyle">
									<legend>
						                Pemeriksaan Fisik
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4">Kepala & Leher</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_kepala" name="kepala" readonly placeholder="Hasil Pemeriksaan" ></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Thorax & ABD</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_thorax" name="thorax" readonly placeholder="Hasil Pemeriksaan" ></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Extremitas</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_ex" name="ex" readonly placeholder="Hasil Pemeriksaan" ></textarea>
										</div>
									</div>
								</fieldset>

						  		<fieldset class="fsStyle">
									<legend>
						                Diagnosa & Terapi
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Dokter Pemeriksa</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="imod_dokter" placeholder="Search Dokter" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Diagnosa Utama</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_utama" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_utama" placeholder=" Diagnosa" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Diagnosa Sekunder</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_sek1" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_sek1" placeholder=" Diagnosa" readonly>
										</div>
										<label class="control-label">1</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4"></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_sek2" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_sek2" placeholder=" Diagnosa" readonly>
										</div>
										<label class="control-label">2</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4"></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_sek3" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_sek3" placeholder=" Diagnosa" readonly>
										</div>
										<label class="control-label">3</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4"></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_sek4" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_sek4" placeholder=" Diagnosa" readonly>
										</div>
										<label class="control-label">4</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Detail Diagnosa</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_detailDiagnosa" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Terapi</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_terapi" name="terapi" placeholder="Terapi" readonly></textarea>
										</div>
									</div>
									<!-- <div class="form-group">
										<label class="control-label col-md-4" >Alergi</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="alergi" name="alergi" placeholder="Alergi" readonly>
										</div>
									</div> -->
						  		</fieldset>
				        	</div>
			        		
			        		<div class="modal-footer">
			        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						    </div>
						</div>
					</div>
				</form>
			</div>
	        <br>
		</div> 

		<div class="tab-pane" id="ok"> 
			<div class="dropdown"  id="btnBawahTambahCare">
		        <div id="titleInformasi">Konsultasi Dokter
		        </div>
		        <div class="btnBawah floatright" style="margin-top:-25px;">
		           	<i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i>
		        </div>
		    </div>
	        <div class="tabelinformasi" id="tbhCare">
					
				<div class="portlet-body" style="margin: 20px 10px 0px 10px">
					<?php
					echo "<input type='hidden' id='jml_over' value='".count($overview_history)."' >";
					?>
					<table id="tableOverviewKlinik" class="table table-striped table-bordered table-hover table-responsive tableDTUtama">
						<thead>
							<tr class="info" style="text_align: center;">
								<th width="20">No.</th>
								<th>Unit</th>
								<th>Anamnesa</th>
								<th>Dokter Pemeriksa</th>
								<th width="20">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$i = 0;
								if(!empty($overview_history)){
									foreach ($overview_history as $over) {
										$i++;
										echo'<tr>';
											echo'<td>'.$i.'</td>';
											echo'<td>IGD</td>';
											echo'<td>'.$over['anamnesa'].'</td>';
											echo'<td>'.$over['nama_petugas'].'</td>';
											echo'<td style="text-align:center;"><a href="#riwayat_over" data-toggle="modal" onClick="detailOver(&quot;'.$over['id'].'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>';
										echo'</tr>';
									}
								}
							?>
						</tbody>
					</table>												
				</div>
			</div>
		 	
		 	<div class="dropdown" id="btnBawahCare">
		 	  	<div id="titleInformasi" >Uraian Tindakan Klinik</div>
		        <div id="btnBawahCare" class="btnBawah floatright"  style="margin-top:-25px;"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>
	        <br>
	        
			<div class="tabelinformasi" id="tabelcare">
				<form class="form-horizontal" role="form" method="POST" style="margin-left:20px;margin-right:20px;">
					
				    <div class="form-group">
				        <div class="portlet-body" style="margin: 0px 10px 0px 10px">
					        <?php
					        	echo '<input type="hidden" id="jmlh_table" value="'.count($visit_care).'" >';
					        ?>
				            <table class="table table-striped table-bordered table-hover tableDTUtama" id="tableCareKlinik">
								<thead>
									<tr class="info">
										<th style="width:10px;">No.</th>
										<th>Waktu</th>
										<th>Tindakan</th>
										<th>Jasa Sarana</th>
										<th>Jasa Pelayanan</th>
										<th>BAKHP</th>
										<th>On faktur</th>
										<th>Paramedis</th>
										<th>Jumlah</th>
										<th width="80">Delete</th>
									</tr>
								</thead>
								<tbody id="tbody_tindakan">
									<?php  
										if (!empty($visit_care)) {
											$i = 0;
											foreach($visit_care as $value){
												$i++;
												echo "<tr>";
													echo "<td>".$i."</td>";										
													echo "<td>".$value['waktu_tindakan']."</td>";									
													echo "<td>".$value['nama_tindakan']."</td>";												
													echo "<td>".$value['j_sarana']."</td>";										
													echo "<td>".$value['j_pelayanan']."</td>";
													echo "<td>".$value['bakhp_this']."</td>";										
													echo "<td>".$value['on_faktur']."</td>";
													echo "<td>".$value['nama_petugas']."</td>";										
													echo "<td>".$value['jumlah']."</td>";
													echo "<td style='text-align:center'><a style='cursor:pointer;' class='hapusTindakanKlinik'><input type='hidden' class='getid' value='".$value['care_id']."''><i class='glyphicon glyphicon-trash'></i></a></td>";
												echo "</tr>";
											}
										}
									?>
								</tbody>
							</table>
							</table>
						</div>
					</div>
				</form>
			</div>

	        <br>
		</div>

		<div class="tab-pane" id="resep">
	 		<div class="dropdown" id="btnBawahTambahResep">
    		    <div id="titleInformasi">Tambah Resep</div>
        		<div id="btnBawahTambahResep" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
        	</div>
            <br>
        	<div class="informasi" id="tambahResep">
        		<form class="form-horizontal" role="form" method="POST" id="submitresep">
					<div class="form-group">
						<label class="control-label col-md-3">Dokter</label>
						<div class="col-md-3">
							<input type="hidden" id="resep_id_dokter">
							<input type="text" class="form-control" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" id="resep_namadokter" required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Tanggal</label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="resep_date" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Deskripsi Resep</label>
						<div class="col-md-5">
							<textarea class="form-control" name="deskripsiResep" placeholder="Deskripsi Resep" id="resep_deskripsi" required></textarea>							
						</div>
					</div>

					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>	
			</div>

	 		<div class="dropdown" id="btnBawahTabelResep">
		        <div id="titleInformasi">Riwayat Tabel Resep</div>
		        <div id="btnBawahTabelResep" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
            <br>

        	<div id="tblResep">
	        	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
	        		<input type="hidden" id="jml_resep" value="<?php echo count($visit_resep); ?>">
					<table class="table table-striped table-bordered table-hover tableDTUtama" id="tableResep">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Dokter</th>
								<th>Tanggal</th>
								<th>Deskripsi Resep</th>
								<th>Status Bayar</th>
								<th>Status Ambil</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<?php  
							if (!empty($visit_resep)) {
								$i = 0;
								foreach ($visit_resep as $value) {
									$i++;
									$tgl = strtotime($value['tanggal']);
									$hasil = date('d F Y', $tgl); 
									echo '<tr>';
									echo '<td>'.$i.'</td>';
									echo '<td>'.$value['nama_petugas'].'</td>';										
									echo '<td>'.$hasil.'</td>';										
									echo '<td>'.$value['resep'].'</td>';										
									echo '<td>'.$value['status_bayar'].'</td>';										
									echo '<td>'.$value['status_ambil'].'</td>';				
									echo '<td style="text-align:center">';
										echo '<a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value="'.$value['resep_id'].'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>';
									echo '</td>';
									echo '</tr>';
								}
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
        </div>

        <div class="tab-pane" id="penunjang">
	        <div class="dropdown" id="btnBawahPenunjang">
		        <div id="titleInformasi">Pemeriksaan Penunjang</div>
		        <div class="btnBawah" id="btnBawahPenunjang"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>
		    <br>

            <div class="informasi" id="infoPenunjang">
	            <form class="form-horizontal" id="submit_penunjang">
	          		<div class="form-group">
						<label class="control-label col-md-3">Tanggal</label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="tun_date" class="form-control isian calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
							</div>
						</div>
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3" >Tujuan Penunjang</label>
						<div class="col-md-2">			
							<select class="form-control select" name="depTujuan" id="tun_tujuan" required>
								<option value="" selected>Pilih Unit Penunjang</option>
								<?php
									foreach ($penunjang as $data) {
										echo '<option value="'.$data['dept_id'].'">'.$data['nama_dept'].'</option>';
									}
								?>
							</select>		
						</div>							
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" >Pengirim</label>
						<div class="col-md-3">
							<input type="hidden" id="tun_iddokter">
							<input type="text" class="form-control" id="tun_namadokter" placeholder="Search Pengirim" data-toggle="modal" data-target="#searchDokter" required>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3" >Jenis Pemeriksaan</label>
						<div class="col-md-5">
							<textarea class="form-control" rows="5" id="tun_jenis" placeholder="Jenis Penunjang" required></textarea>
						</div>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>		
				<br>
	        </div>

	        <div class="dropdown" id="btnBawahTabelRiwayat">
		        <div id="titleInformasi">Tabel Riwayat Pemeriksaan</div>
		        <div id="btnBawahTabelRiwayat" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
            <br>

        	<div class="tabelinformasi" id="tblRiwayat">
	        	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
	        		<?php echo '<input type="hidden" id="tun_jumlah" value="'.count($visit_penunjang).'">'?>
					<table class="table table-striped table-bordered tableDTUtama table-hover" id="table_penunjang">
						<thead>
							<tr class="info">
								<th width="3%"> No. </th>
								<th> Tanggal Tindakan</th>
								<th> Departemen Penunjang</th>
								<th> Unit Rujukan</th>
								<th> Status </th>
								<th style="width:20px;"> Details</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 0;
								foreach ($visit_penunjang as $data) {
									$tgl = strtotime($data['waktu']);
									$hasil = date('d F Y', $tgl); 
									
									$no++;
									echo '
										<tr>
											<td>'.$no.'</td>
											<td align="center">'.$hasil.'</td>								
											<td>'.$data['unit_tujuan'].'</td>										
											<td>'.$data['unit_asal'].'</td>										
											<td>'.$data['status'].'</td>											
											<td style="text-align:center">
												<input type="hidden" class="idpenunjang" value="'.$data['penunjang_id'].'">
												<a href="#viewRiwayat" data-toggle="modal" class="detailpenunjang" data-placement="top"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Details"></i></a>
											</td>												
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="modal fade" id="viewRiwayat" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-dialog" style="width:1300px;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        	<h3 class="modal-title" id="myModalLabel">Hasil Pemeriksaan</h3>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-3 nama goright">Order ID:</label>
										<div class="col-md-9 nama" id="dp_penunjang">	0001 </div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-3 goright">Tanggal Tindakan:</label>
										<div class="col-md-8" id="dp_date">
											12 Desember 2012
										</div>
									</div>
								</div>
								
								
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-4 goright">Departemen Penunjang:</label>
										<div class="col-md-8" id="dp_dept"> Labolatorium	</div>
									</div>
								</div>
								
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-3 goright">Pemeriksa:</label>
										<div class="col-md-8" id="dp_periksa" >terserah</div>
									</div>
								</div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-4 goright">Status Hasil:</label>
										<div class="col-md-8" id="dp_status">SELESAI</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<hr/>
							<table class="table table-striped table-bordered table-hover" id="tabelHasilPenunjang">
								<thead>
									<tr class="info">
										<th>
											Jenis Pemeriksaan
										</th>
										<th>
											 Hasil Pemeriksaan
										</th>
										<th>
											Nilai Normal
										</th>
										<th>
											 Keterangan/Rujukan
										</th>
									</tr>
								</thead>
								<tbody id="tbody_detail">
									<!-- <tr>
										<td>
											 Pemeriksaan
										</td>
										<td>
											 Hasil Pemeriksaan
										</td>
										<td>
											Nilai Normal
										</td>
										<td>
											 Keterangan/Rujukan
										</td>							
									</tr> -->
								</tbody>
							</table>
						</div>
						<div class="modal-footer">
				 			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>        </div>

        <div class="tab-pane" id="orderkamar">
        	<div class="dropdown" id="btnBawahOrder">
	            <div id="titleInformasi">Order Kamar Operasi</div>
	            <div class="btnBawah" id="btnBawahOrder"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>

	        <div class="informasi" id="infoKamar">
	            <form class="form-horizontal" method="POST" id="submit_order_operasi">
	          		<div class="form-group">
						<label class="control-label col-md-3">Waktu Pelaksanaan</label>
						<div class="col-md-3" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="operasi_date" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii:ss" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i:s");?>">
							</div>
						</div>
					</div>	

					<div class="form-group">
						<label class="control-label col-md-3">Dokter</label>
						<div class="col-md-3">
							<input type="hidden" id="iddokter_o">
							<input type="text" class="form-control" id="namadokter_o" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" required>
						</div>
					</div>

					<!-- <div class="form-group">
						<label class="control-label col-md-3" >Tujuan Order</label>
						<div class="col-md-3">			
							<select class="form-control select" name="order" id="order">
								<option value="Kamar Operasi" selected>Kamar Operasi</option>
								<option value="Laboratorium">Laboratorium</option>
								<option value="Radiologi"  >Radiologi</option>
								<option value="Fisioterapi" >Fisioterapi</option>
							</select>		
						</div>							
					</div> -->

					<div class="form-group">
						<label class="control-label col-md-3" >Jenis Operasi</label>
						<div class="col-md-3">			
							<select class="form-control select" name="operasi_jenis" id="operasi_jenis" required>
								<option value="" selected>Pilih Jenis Operasi</option>
								<option value="Kecil">Kecil</option>
								<option value="Sedang">Sedang</option>
								<option value="Besar">Besar</option>
								<option value="Khusus">Khusus</option>
							</select>
				 		</div>
					</div>
							
					<div class="form-group">
						<label class="control-label col-md-3" >Detail Operasi</label>
						<div class="col-md-5">			
							<textarea class="form-control" rows="5" id="operasi_detail" placeholder="Detail Operasi" required></textarea>
				 		</div>
					</div>

					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>	
				</form>
				<br>
			</div>	<!-- End Dropdown -->

			<div class="dropdown" id="btnTableKamarOperasi">
	            <div id="titleInformasi">Riwayat Operasi</div>
	            <div class="btnBawah" id="btnTableKamarOperasi"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	           	<br>

	        <div class="tabelinformasi" id="tabelKamar">
	        	<input type="hidden" id="jml_data" value="<?php echo count($order_operasi); ?>">
	           	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tableOpeasi" >
						<thead>
							<tr class="info">
								<th width="20">No. </th>
								<th width="200">Waktu Tindakan</th>
								<th>Dokter</th>
								<th>Status</th>
								<th>Keterangan Order</th>
								<th width="20">Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i = 1;
							if(!empty($order_operasi)){
								foreach ($order_operasi as $value) {
									echo"
										<tr>
											<td>".$i."</td>
											<td>".$value['waktu_mulai']."</td>
											<td>".$value['nama_petugas']."</td>										
											<td>Kamar Operasi</td>
											<td>".$value['alasan']."</td>
											<td style='text-align:center'>
												<i class='glyphicon glyphicon-trash hapusorder' data-toggle='tooltip' data-placement='top' style='cursor:pointer;' title='Hapus'><input type='hidden' class='getid' value='".$value['order_operasi_id']."'></i>
											</td>										
										</tr>
									";
									$i++;
								}
							}
						?>
						</tbody>
					</table>
				</div>	<br><br>
			</div>	<!-- End Dropdown -->
        </div>

        <div class="tab-pane" id="konsul">
        	<div class="dropdown" id="btnBawahOrderKonsul">
	            <div id="titleInformasi">Order Konsultasi Gizi</div>
	            <div class="btnBawah" id="btnBawahOrderKonsul"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>
	       
	        <div class="informasi" id="infoKonsul">
		      	<form class="form-horizontal" role="form" id="submit_gizi">
		          		<div class="form-group">
							<label class="control-label col-md-3">Tanggal Konsultasi</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" id="konsul_date" style="cursor:pointer;background-color:white;" class="form-control isian calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
								</div>
							</div>
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3" >Konsultan Gizi</label>
							<div class="col-md-3">
								<input type="hidden" id="konsul_idDokter">
								<input type="text" class="form-control" id="konsul_dokter" placeholder="Search Konsultan" data-toggle="modal" data-target="#searchDokter">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Kajian Gizi</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="konsul_kajiangizi" placeholder="Kajian Gizi"></textarea>
								
						 	</div>				
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Anamnesa Diet</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="konsul_anemdiet" placeholder="Anamnesa Diet"></textarea>
								
						 	</div>		
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Kajian Diet</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="konsul_kajiandiet"  placeholder="Kajian Diet"></textarea>
								
						 	</div>		
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Detail Menu Sehari-hari</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="konsul_detail"  placeholder="Detail Menu"></textarea>
								
						 	</div>		
						</div>

						<br>
						<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
						<div style="margin-left:80%">
							<span style="padding:0px 10px 0px 10px;">
								<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
								<button type="submit" class="btn btn-success">SIMPAN</button> 
							</span>
						</div>
				</form>
			</div>	

			<div class="dropdown" id="btnTabelKonsultasi">
	            <div id="titleInformasi">Riwayat Konsultasi Gizi</div>
	           	<div class="btnBawah" id="btnTabelKonsultasi"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>

	        <div class="tabelinformasi" id="tabelKonsultasi" >
	           	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
	           		<input type="hidden" id="jml_gizi" value="<?php echo count($gizi); ?>">
					<table class="table table-striped table-bordered table-hover tableDTUtama" id="tableKonsultasi">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal Konsultasi </th>
								<th>Konsultan</th>
								<th>Kajian Gizi</th>
								<th>Anamnesa Diet</th>
								<th>Kajian Diet</th>
								<th>Detail Menu Sehari-hari</th>
								<th width="100">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!empty($gizi)){
								$no = 0;
								foreach ($gizi as $data) {
									$tgl = strtotime($data['tanggal']);
									$hasil = date('d F Y', $tgl);

									$no++;
									echo'
										<tr>
											<td>'.$no.'</td>
											<td>'.$hasil.'</td>
											<td>'.$data['nama_petugas'].'</td>
											<td>'.$data['kajian_gizi'].'</td>										
											<td>'.$data['anamnesa_diet'].'</td>
											<td>'.$data['kajian_diet'].'</td>
											<td>'.$data['detail_menu'].'</td>
											<td style="text-align:center">
												<a style="cursor:pointer;" class="hapusgizi"><input type="hidden" class="getid" value="'.$data['gizi_id'].'"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
												<a href="#print"><i class="glyphicon glyphicon-print"  data-toggle="tooltip" data-placement="top" title="Print"></i></a>
											</td>										
										</tr>
									';
									$i++;
								}
							}
							?>
						</tbody>
					</table>
				</div>	<br>
			</div>	
        </div>   

        <div class="tab-pane" id="riwayat">
         	<div class="dropdown" id="rwp1">
            	<div id="titleInformasi">Riwayat Klinik</div>
            	<div class="btnBawah" id="btnBawahRiwayat"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div class="portlet-body" id="tblrwp1" style="margin: 20px 10px 0px 10px">
            	
				<table class="table table-striped table-bordered table-hover table-responsive tableDT">
					<thead>
						<tr class="info" style="text_align: center;">
							<th width="20">No.</th>
							<th>Tanggal</th>
							<th>Unit</th>
							<th>Anamnesa</th>
							<th>Dokter Pemeriksa</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($riwayat_klinik as $data) {
								$i++;
								$tgl = strtotime($data['waktu']);
								$hasil = date('d F Y H:i:s', $tgl);

								echo '
									<tr>
										<td>'.$i.'</td>
										<td style="text-align:center">'.$hasil.'</td>
										<td>'.$data['nama_dept'].'</td>
										<td>'.$data['anamnesa'].'</td>
										<td>'.$data['nama_petugas'].'</td>
										<td style="text-align:center;"><a href="#riwayat_over" data-toggle="modal" onClick="detailOver(&quot;'.$data['id'].'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>
									</tr>
								';
							}
						?>
					</tbody>
				</table>												
			</div>
			<br>
            <div class="dropdown" id="rwp2">
            	<div id="titleInformasi">Riwayat IGD</div>
            	<div class="btnBawah" id="btnBawahRiwayat"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div class="portlet-body" id="tblrwp2" style="margin: 20px 10px 0px 10px">
            	
				<table class="table table-striped table-bordered table-hover table-responsive tableDT">
					<thead>
						<tr class="info" style="text_align: center;">
							<th width="20">No.</th>
							<th>Tanggal</th>
							<th>Anamnesa</th>
							<th>Dokter Jaga</th>
							<th>Perawat Jaga</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($riwayat_igd as $data) {
								$i++;
								$tgl = strtotime($data['waktu']);
								$hasil = date('d F Y H:i:s', $tgl);

								echo '
									<tr>
										<td>'.$i.'</td>
										<td style="text-align:center">'.$hasil.'</td>										
										<td>'.$data['anamnesa'].'</td>
										<td>'.$data['rdokter'].'</td>
										<td>'.$data['rperawat'].'</td>
										<td style="text-align:center;"><a href="#riwayat_over" data-toggle="modal" onClick="detailOver(&quot;'.$data['id'].'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>
									</tr>
								';
							}
						?>
					</tbody>
				</table>												
			</div>
			<div class="modal fade" id="riwigd" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="riwkondok">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Penanganan IGD</h3>
				   			</div>
							<div class="modal-body" style="padding-left:80px;">

				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
									</div>
			        			</div>	
			        			<div class="form-group">
									<label class="control-label col-md-4">Anamnesa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
									</div>
								</div>

								<fieldset class="fsStyle">
									<legend>
						                Tanda Vital
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Tekanan Darah</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="tekanandarah" name="takanandarah" placeholder="Tekanan Darah" readonly>
										</div>
										<label class="control-label col-md-2">mmHg</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Temperatur</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="temperatur" name="temperatur" placeholder="Temperatur" readonly>
										</div>
										<label class="control-label col-md-2">&deg;C</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Nadi</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="nadi" name="nadi" placeholder="Nadi" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Pernapasan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Berat Badan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="berat" name="berat" placeholder="Berat Badan" readonly>
										</div>
										<label class="control-label col-md-2">Kg</label>
									</div>
						  		</fieldset>

						  		<fieldset class="fsStyle">
									<legend>
						                Pemeriksaan Fisik
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Kepala & Leher</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="kepalaleher" name="kepalaleher" placeholder="Kepala & Leher" readonly>
										</div>
										
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Thorax & ABD</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="thorax" name="thorax" placeholder="Thorax & ABD" readonly>
										</div>
										
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Extremitas</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="Extremitas" name="Extremitas" placeholder="Extremitas" readonly>
										</div>
										
									</div>
									
						  		</fieldset>

						  		<fieldset class="fsStyle">
									<legend>
						                Diagnosa & Terapi
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Dokter Jaga</label>
										<div class="col-md-7">
											<input type="text" style="background-color:white" class="form-control" id="dokter" placeholder="Search Dokter" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Perawat Jaga</label>
										<div class="col-md-7">
											<input type="text" style="background-color:white" class="form-control" id="dokter" placeholder="Search Dokter" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Diagnosa Utama</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_utama" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" placeholder="Keterangan" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Diagnosa Sekunder</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_sek1" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" placeholder="Keterangan" readonly>
										</div>
										<label class="control-label">1</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" ></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_sek2" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" placeholder="Keterangan" readonly>
										</div>
										<label class="control-label">2</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" ></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_sek3" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" placeholder="Keterangan" readonly>
										</div>
										<label class="control-label">3</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" ></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_sek4" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" placeholder="Keterangan" readonly>
										</div>
										<label class="control-label">4</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Detail Diagnosa</label>
										<div class="col-md-7">
											<textarea class="form-control" id="detailDiagnosa" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Terapi</label>
										<div class="col-md-7">
											<textarea class="form-control" id="terapi" name="terapi" placeholder="Terapi" readonly></textarea>
										</div>
									</div>
									
						  		</fieldset>
				        	</div>
			        		
			        		<div class="modal-footer">
			        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						    </div>
						</div>
					</div>
				</form>
			</div>
			<br>

			<div class="dropdown" id="rwp3">
            	<div id="titleInformasi">Riwayat Perawatan</div>
            	<div class="btnBawah" id="btnBawahRiwayat"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div class="portlet-body" id="tblrwp3" style="margin: 20px 10px 0px 10px">
            	
            	<table class="table table-striped table-bordered table-hover table-responsive tableDT">
					<thead>
						<tr class="info" style="text_align: center;">
							<th style="width:10px;">No.</th>
							<th>Tanggal</th>
							<th>Waktu</th>
							<th>Dokter Visit</th>
							<th>Diagnosa Utama</th>
							<th>Diagnosa Sekunder</th>
							<th>Perkembangan Penyakit</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td style="text-align:center">12 Desember 2012</td>
							<td>12:12</td>
							<td>Jems</td>
							<td>Bebas</td>
							<td>Bebas</td>
							<td>Bebas</td>
							<td style="text-align:center;"><a href="#riwperawatan" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>
						</tr>
					</tbody>
				</table>												
			</div>
			<div class="modal fade" id="riwperawatan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="riwkondok">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Perawatan</h3>
				   			</div>
							<div class="modal-body" style="padding-left:80px;">

				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
									</div>
			        			</div>	

			        			<div class="form-group">
									<label class="control-label col-md-4">Dokter Visit</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="dokterv" name="dokterv" placeholder="Dokter" readonly></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Petugas</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="petugas" name="petugas" placeholder="Petugas" readonly></textarea>
									</div>
								</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Anamnesa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Utama</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_utama" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Sekunder</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek1" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="label-cntrol">1</label>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_se2" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="label-cntrol">2</label>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek3" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="label-cntrol">3</label>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek4" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="label-cntrol">4</label>
								</div>


								<div class="form-group">
									<label class="control-label col-md-4">Perbangan Penyakit</label>
									<div class="col-md-7">
										<textarea class="form-control" id="perkembangan" name="perkembangan" placeholder="Perkembangan Penyakit" readonly></textarea>
									</div>
								</div>

								<fieldset class="fsStyle">
									<legend>
						                Tanda Vital
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Tekanan Darah</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="tekanandarah" name="takanandarah" placeholder="Tekanan Darah" readonly>
										</div>
										<label class="control-label col-md-2">mmHg</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Temperatur</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="temperatur" name="temperatur" placeholder="Temperatur" readonly>
										</div>
										<label class="control-label col-md-2">&deg;C</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Nadi</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="nadi" name="nadi" placeholder="Nadi" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Pernapasan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Berat Badan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="berat" name="berat" placeholder="Berat Badan" readonly>
										</div>
										<label class="control-label col-md-2">Kg</label>
									</div>
						  		</fieldset>

				        	</div>
			        		
			        		<div class="modal-footer">
			        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						    </div>
						</div>
					</div>
				</form>
			</div>
			<br>
        </div>

		<div class="tab-pane" id="resume">
        	<div class="dropdown">
	            <div id="titleInformasi">Resume Pulang Pasien <span style="color:red; margin-left:30px;font-style:italic;">WAJIB DIISI!</span> </div>
	            <!-- <div class="btnBawah" id="btnBawahResumePulang"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>  -->
            </div>

            <div class="informasi" id="infoResumePulang">
            	<form class="form-horizontal" role="form" id="submitresume">
            		<div class="form-group">
						<label class="control-label col-md-3">Waktu Keluar <span class="required">* </span></label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="res_date" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
							</div>
						</div>
					</div>	
	
					<div class="form-group">
						<label class="control-label col-md-3" >Alasan Keluar <span class="required">* </span>
						</label>
						<div class="col-md-4">			
							<select class="form-control select" name="alasanKeluarPasien" id="res_alasankeluar">
								<option value="" selected>Pilih</option>									
								<option value="Pasien Dipulangkan">Pasien Dipulangkan</option>									
								<option value="Atas Permintaan Sendiri">Atas Permintaan Sendiri</option>
								<option value="Rujuk Rumah Sakit Lain"  >Rujuk ke Rumah Sakit Lain</option>
								<option value="Pasien Meninggal" >Pasien Meninggal</option>
								<option value="Rujuk Rawat Inap" >Rujuk Rawat Inap</option>
							</select>		
						</div>							
					</div>	
					
					<div class="form-group" id="alasanPlg">
						<label class="control-label col-md-3" >Alasan Pulang
						</label>
						<div class="col-md-4">			
							<textarea class="form-control" rows="3" id="res_alasanpulang"></textarea>
					 	</div>
					</div>
					
					<div class="form-group" id="isiRujuk">
						<label class="control-label col-md-3" >Isian Rumah Sakit Rujukan<span class="required">* </span>
						</label>
						<div class="col-md-4">			
							<input type="text" class="form-control" name="isianRSRujuk" id="res_rsrujuk" placeholder="Rumah Sakit Rujukan">
					 	</div>
					</div>		

					<div class="form-group" id="detPasienMeninggal">
						<label class="control-label col-md-3" >Detail Pasien Meninggal<span class="required">* </span>
						</label>
						<div class="col-md-4">			
							<select class="form-control select" name="detPasDie" id="res_dmeninggal">
								<option value="sebelum dirawat" selected>Meninggal sebelum dirawat</option>
								<option value="sesudah dirawat > 48">Meninggal sesudah dirawat > 48 jam</option>
								<option value="sesudah dirawat < 48">Meninggal sesudah dirawat < 48 jam</option>
							</select>
					 	</div>							
					</div>

					<div class="form-group" id="pasienMeninggal">
						<label class="control-label col-md-3">Waktu Pasien Meninggal</label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="res_datemeninggal" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
							</div>
						</div>
					</div>
					
					<div class="form-group" id="ketMati">
						<label class="control-label col-md-3"> Keterangan Meninggal
						</label>
						<div class="col-md-4">			
						<textarea class="form-control" rows="3" id="res_ketmeninggal"></textarea>
					 	</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3" >Jenis Pelayanan<span class="required">* </span>
						</label>
						<div class="col-md-4">			
							<select class="form-control select" id="res_jenis">
								<option value="" selected>Pilih Jenis Pelayanan</option>
								<option value="">Bedah</option>
								<option value="">Non Bedah</option>
								<option value="">Kecelakaan Lalulintas</option>
								<option value="">Kebidanan</option>
								<option value="">Psikiatrik</option>
								<option value="">Anak</option>
							</select>		
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Golongan Sebab Penyakit Luar</label>
						<div class="col-md-4">
							<input type="hidden" id="res_idsebab">
							<input type="text" style="cursor:pointer;background-color:white" class="form-control isian" id="res_sebab" placeholder="Golongan Sebab Penyakit" data-toggle="modal" data-target="#searchGolongan" readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline">
							<label class="control-label col-md-3">Kasus Trauma <span class="required">* </span></label>
							<div class="col-md-8">
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_lantas" value="lakalantas"> Lakalantas
								</label>
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_lkerja" value="laka kerja"> Laka Kerja
								</label>
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_lakart" value="laka rumah tangga"> Laka Rumah Tangga
								</label>
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_aniaya" value="Penganiayaan"> Penganiayaan
								</label>
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_intoksin" value="Intoksinasi"> Intoksinasi
								</label>
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_gigitan" value="Gigitan"> Gigitan
								</label>
								
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline">
							<label class="control-label col-md-3">Kasus Non-Trauma <span class="required">* </span></label>
							<div class="col-md-8">
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_non1" value="Penyakit Dalam"> Penyakit Dalam
								</label>
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_non2" value="Bedah"> Bedah
								</label>
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_non3" value="Anak"> Anak
								</label>
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_non4" value="Obgsyn"> Obgsyn
								</label>
								<label class="checkbox-inline">
								  	<input type="checkbox" id="res_non5" value="Dan lain-lain"> Dan lain-lain
								</label>
								
								
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline">
							<label class="control-label col-md-3">Jenis Kunjungan <span class="required">* </span></label>
							<div class="col-md-4">
								<div class="radio-list">
									<label>
										<input type="radio" name="res_kunjungan" id="kunlama" value="0"/><span style="margin-left:15px;font-size:10pt;font-width:normal;font-size:10pt;font-width:normal">Kunjungan Lama </span> 
									</label>
									<label style="margin-left:40px;">
										<input type="radio"  name="res_kunjungan" id="kunbaru" value="1"/><span style="margin-left:15px;font-size:10pt;font-width:normal">Kunjungan Baru </span>
									</label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline">
							<label class="control-label col-md-3">Jenis Kasus <span class="required">* </span></label>
							<div class="col-md-4">
								<div class="radio-list">
									<label>
										<input type="radio"  name="res_jk" id="klama" value="0"/><span style="margin-left:15px;font-size:10pt;font-width:normal">Kasus Lama </span> 
									</label>
									<label style="margin-left:40px;">
										<input type="radio"  name="res_jk" id="kbaru" value="1"/><span style="margin-left:15px;font-size:10pt;font-width:normal">Kasus Baru </span>
									</label>
								</div>
							</div>
						</div>
					</div>

					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>	
				<br>
            </div>
            <br>
        </div>

		<!-- Modal tambah diagnosa -->
		<div class="modal fade" id="searchDiagnosa" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Diagnosa</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">
							<form method="POST" id="search_diagnosa">	
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="katakunci_diag" placeholder="Kata kunci"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div>	
							</form>
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" style="table-layout:fixed" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th width="25%;">Kode Diagnosa</th>
										<th>Keterangan</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_diagnosa">
									<tr><td colspan="3" style="text-align:center;">Cari Data Diagnosa</td></tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

			<div class="modal fade" id="searchGolongan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Golongan Sebab Penyakit Luar</h3>
	        			</div>
	        			<div class="modal-body">
	        				<form method="POST" id="submit_sebab">
							<div class="form-group">	
								<div class="col-md-5">
									<input type="text" class="form-control" name="katakunci" id="sebab_katakunci" placeholder="Kata kunci"/>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-info">Cari</button>
								</div>	
							</div>
							</form>
							<br>	
							<div style="margin-left:5px; margin-right:5px;"><hr></div>
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
									<thead>
										<tr class="info">
											<td width="30%;">No Daftar Terperinci</td>
											<td>Golongan Sebab Penyakit Luar</td>
											<td width="10%">Pilih</td>
										</tr>
									</thead>
									<tbody id="tbody_sebab">
										<tr>
											<td colspan="3" style="text-align:center;">Cari Data Golongan Sebab</td>
										</tr>
									</tbody>
								</table>												
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="searchDokter" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Dokter</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">	
						<form method="POST" id="search_dokter">
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="inputDokter" placeholder="Nama dokter"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div>	
						</form>
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th>Nama Dokter</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_dokter">									
									<tr>
										<td colspan='2' style="text-align:center;">Cari Data Dokter</td>
									</tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

			<div class="modal fade" id="searchPerawat" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Perawat</h3>
	        			</div>
	        			<div class="modal-body">
	        			<form id="submitperawat">
							<div class="form-group">	
								<div class="col-md-5">
									<input type="text" class="form-control" name="katakunci" id="katakunciperawat" placeholder="Nama Perawat"/>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-info">Cari</button>
								</div>	
							</div>	
						</form>
							<br>	
							<div style="margin-left:5px; margin-right:5px;"><hr></div>
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
									<thead>
										<tr class="info">
											<th>Nama Perawat</th>
											<th width="10%">Pilih</th>
										</tr>
									</thead>
									<tbody id="tbody_perawat">
										<tr>
											<td colspan='2' style="text-align:center;">Cari Data Perawat</td>
										</tr>
									</tbody>
								</table>												
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
			
	</div>

	<div class="modal fade" id="riwayat_over" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="riwkondok">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Konsultasi Dokter</h3>
				   			</div>
							<div class="modal-body" style="padding-left:80px;">

				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="detail_waktu" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
									</div>
			        			</div>	
			        			<div class="form-group">
									<label class="control-label col-md-4">Anamnesa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="detail_anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
									</div>
								</div>

								<fieldset class="fsStyle">
									<legend>
						                Tanda Vital
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Tekanan Darah</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_tekanan" name="takanandarah" placeholder="Tekanan Darah" readonly>
										</div>
										<label class="control-label col-md-2">mmHg</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Temperatur</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_temperatur" name="temperatur" placeholder="Temperatur" readonly>
										</div>
										<label class="control-label col-md-2">&deg;C</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Nadi</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_nadi" name="nadi" placeholder="Nadi" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Pernapasan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Berat Badan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_berat" name="berat" placeholder="Berat Badan" readonly>
										</div>
										<label class="control-label col-md-2">Kg</label>
									</div>
						  		</fieldset>

						  		<fieldset class="fsStyle">
									<legend>
						                Diagnosa & Terapi
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Dokter Pemeriksa</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="detail_dokter" placeholder="Search Dokter" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Diagnosa Utama</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="detail_kutama" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="detail_dutama" placeholder="Keterangan" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Diagnosa Sekunder</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="detail_ksek1" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="detail_dsek1" placeholder="Keterangan" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" > </label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="detail_ksek2" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="detail_dsek2" placeholder="Keterangan" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" > </label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="detail_ksek3" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="detail_dsek3" placeholder="Keterangan" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" > </label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="detail_ksek4" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="detail_dsek4" placeholder="Keterangan" readonly>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-4" >Detail Diagnosa</label>
										<div class="col-md-7">
											<textarea class="form-control" id="detail_detail" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Terapi</label>
										<div class="col-md-7">
											<textarea class="form-control" id="detail_terapi" name="terapi" placeholder="Terapi" readonly></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Alergi</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="detail_alergi" name="alergi" placeholder="Alergi" readonly>
										</div>
									</div>
						  		</fieldset>
				        	</div>
			        		
			        		<div class="modal-footer">
			        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						    </div>
						</div>
					</div>
				</form>
			</div>
	        <br>
		</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#kelas_klinik').prop('disabled', true);
		//---------------------- overview klinik -------------------//

		$("#submitOver").submit(function(e){
			e.preventDefault();
			// alert('ok');
			// return false;
			var item = {};
		    var number = 1;
		    item[number] = {};
			item[number]['waktu'] = $('#inputdate').val();
			item[number]['visit_id'] = $('#v_id').val();
			item[number]['rj_id'] = $('#igd_id').val();
			item[number]['tekanan_darah'] = $("#tekanandarahOver").val();
			item[number]['anamnesa'] = $('#anamnesaOver').val();
			item[number]['temperatur'] = $('#tempOver').val();
			item[number]['nadi'] = $('#nadiOver').val();
			item[number]['pernapasan'] = $('#pernapasanOver').val();
			item[number]['berat_badan'] = $('#beratOver').val();
			item[number]['dokter'] = $('#id_dokterOver').val();
			item[number]['diagnosa1'] = $('#k_utama_over').val();
			item[number]['diagnosa2'] = $('#k_sek_over').val();
			item[number]['diagnosa3'] = $('#k_sek_over2').val();
			item[number]['diagnosa4'] = $('#k_sek_over3').val();
			item[number]['diagnosa5'] = $('#k_sek_over4').val();
			item[number]['detail_diagnosa'] = $('#detail_over').val();
			item[number]['terapi'] = $('#terapi_over').val();
			item[number]['alergi'] = $('#alergi_over').val();

			console.log(item);
			save_overview(item);

			return false;
		});
		
		var diagnosa = 0;
		$('.d1').click(function(){
			diagnosa = 1;
		});

		$('.d2').click(function(){
			diagnosa = 2;
		});

		$('.d3').click(function(){
			diagnosa = 3;
		});

		$('.d4').click(function(){
			diagnosa = 4;
		});

		$('.d5').click(function(){
			diagnosa = 5;		
		});

		$('.di1').click(function(){
			diagnosa = 11;
		});

		$('.di2').click(function(){
			diagnosa = 12;
		});

		$('.di3').click(function(){
			diagnosa = 13;
		});

		$('.di4').click(function(){
			diagnosa = 14;
		});

		$('.di5').click(function(){
			diagnosa = 15;		
		});

		$('#search_diagnosa').submit(function(e){
			e.preventDefault();
			var key = {};
			key['search'] = $('#katakunci_diag').val();

			$.ajax({
				type:'POST',
				data:key,
				url:'<?php echo base_url() ?>igd/igddetail/search_diagnosa',
				success:function(data){
					$('#tbody_diagnosa').empty();

					if(data.length>0){
						for(var i = 0; i<data.length;i++){
							$('#tbody_diagnosa').append(
								'<tr>'+
									'<td>'+data[i]['diagnosis_id']+'</td>'+
									'<td style="word-wrap: break-word;white-space: pre-wrap; ">'+data[i]['diagnosis_nama']+'</td>'+
									'<td style="text-align:center; cursor:pointer;"><a onclick="get_diagnosa(&quot;'+data[i]['diagnosis_id']+'&quot;, &quot;'+data[i]['diagnosis_nama']+'&quot;, &quot;'+diagnosa+'&quot;)"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_diagnosa').append(
							'<tr><td colspan="3" style="text-align:center;">Data Diagnosa Tidak Ditemukan</td></tr>'
						);
					}
				}, error:function(data){
					console.log(data);
					myAlert(data);
				}
			});
		});
		
		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#paramedis_id').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
	
		var tindakan_temp = [];

		var autodata = [];
		var iddata = [];
		$('#namatindakan').focus(function(){
			var $input = $('#namatindakan');
			
			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>kamaroperasi/invoicenonbpjs/get_master_tindakan',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_tindakan']);
							iddata.push(data[i]['tindakan_id']);
						}
					}
				});
			}

			$input.typeahead({source:autodata, 
	        autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = autodata.indexOf(current);

			    $('#idtindakan_klinik').val(iddata[index]);			
			    $('#namatindakanarea').val(autodata[index]);	

			    $('#kelas_klinik').prop('disabled', false);    

			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

		});
		
		$('#kelas_klinik').prop('disabled', true);

		$('#kelas_klinik').change(function(){
			var item = {};
			item['kelas'] = $(this).val();
			item['nama'] = $('#idtindakan_klinik').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>kamaroperasi/invoicenonbpjs/get_tariftindakan',
				success:function(data){
					var tarif = 0;
					var lingkup = $('#lingkup').val();
					var persen = 0;

					$('#idtindakdetail').val(data['detail_id']);

					tarif = (Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))+((Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))*persen);
					
					$('#js_klinik').val(data['js']);
					$('#jp_klinik').val(data['jp']);
					$('#bakhp_klinik').val(data['bakhp']);
					$('#tarif').val(tarif);
					$('#jumlah').val(tarif);
				}
			});
		});

		$('#onfaktur').keyup(function(){
			var onfaktur = $('#onfaktur').val();
			if(onfaktur == "")
				onfaktur = 0;
			var tarif = $('#tarif').val();
			var jumlah = parseInt(onfaktur)+parseInt(tarif);
			$('#jumlah').val(jumlah);
		});
		
		//--------------------- end overview klinik --------------------//

		//--------------------- all search dokter here --------------------//
		var d_click = 0;

		$('#nama_dOver').click(function(){
			d_click = 1;
		});

		$('#namadokter').click(function(){
			d_click = 2;
		});

		$('#konsul_dokter').click(function(){
			d_click = 3;
		});

		$('#resep_namadokter').click(function(){
			d_click = 4;
		});
		
		$('#ndokter_igd').click(function(){
			d_click = 5;
		});

		$('#tun_namadokter').click(function(){
			d_click = 6;
		});

		$('#namadokter_o').click(function(){
			d_click = 99;
		});

		$('#search_dokter').submit(function(event){
			event.preventDefault();
			var item = {};
			item['dokter'] = $('#inputDokter').val();

			$.ajax({
				type:'POST',
				data:item,
				url:"<?php echo base_url()?>rawatjalan/daftarpasien/search_dokter",
				success:function(data){
					console.log(data);
					$('#tbody_dokter').empty();
 					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							var nama = data[i]['nama_petugas'],
								id = data[i]['petugas_id'];

							$("#tbody_dokter").append(
								'<tr>'+
									'<td>'+nama+'</td>'+
									'<td style="text-align:center"><i class="glyphicon glyphicon-check" style="cursor:pointer;" onclick="getDokter(&quot;'+id+'&quot; , &quot;'+nama+'&quot;, &quot;'+d_click+'&quot;)"></i></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_dokter').empty();
						$('#tbody_dokter').append(
							'<tr>'+
					 			'<td colspan="2"><center>Data Dokter Tidak Ditemukan</center></td>'+
					 		'</tr>'
						);
					}
				},
				error:function(data){

				}
			});
		});

		var p_click = 1;
		$('#submitperawat').submit(function(event){
			var d_item = {};
			d_item['search'] = $(this).val();
			event.preventDefault();

			$.ajax({
				type:'POST',
				data:d_item,
				url:"<?php echo base_url()?>igd/igddetail/search_perawat",
				success:function(data){
					console.log(data);

					$('#tbody_perawat').empty();
 					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							var nama = data[i]['nama_petugas'],
								id = data[i]['petugas_id'];

							$("#tbody_perawat").append(
								'<tr>'+
									'<td>'+nama+'</td>'+
									'<td style="text-align:center"><i class="glyphicon glyphicon-check" style="cursor:pointer;" onclick="getPerawat(&quot;'+id+'&quot; , &quot;'+nama+'&quot;, &quot;'+p_click+'&quot;)"></i></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_perawat').empty();
						$('#tbody_perawat').append(
							'<tr>'+
					 			'<td colspan="2"><center>Data Perawat Tidak Ditemukan</center></td>'+
					 		'</tr>'
						);
					}
				},
				error:function(data){

				}
			});
		});
		//--------------------- end search dokter here --------------------//

		//--------------------- overview IGD here -------------------------//
		$('#submitOverIGD').submit(function(e){
			e.preventDefault();
			var item = {};
			item[1] = {};

			item[1]['waktu'] = $('#date_igd').val();
			item[1]['anamnesa'] = $('#anamnesa_igd').val();
			item[1]['tekanan_darah'] = $('#tekanandarah_igd').val();
			item[1]['temperatur'] = $('#temperatur_igd').val();
			item[1]['nadi'] = $('#nadi_igd').val();
			item[1]['pernapasan'] = $('#pernapasan_igd').val();
			item[1]['berat_badan'] = $('#berat_igd').val();
			item[1]['kepala_leher'] = $('#kepala_igd').val();
			item[1]['thorax_abd'] = $('#thorax_igd').val();
			item[1]['extrimitas'] = $('#ex_igd').val();
			item[1]['dokter'] = $('#dokter_igd').val();
			item[1]['perawat'] = $('#perawat_igd').val();
			item[1]['diagnosa1'] = $('#kode_utama_igd').val();
			item[1]['diagnosa2'] = $('#kode_sek1_igd').val();
			item[1]['diagnosa3'] = $('#kode_sek2_igd').val();
			item[1]['diagnosa4'] = $('#kode_sek3_igd').val();
			item[1]['diagnosa5'] = $('#kode_sek4_igd').val();
			item[1]['detail_diagnosa'] = $('#detailDiagnosa_igd').val();
			item[1]['terapi'] = $('#terapi_igd').val();
			item[1]['visit_id'] = $('#v_id').val();
			item[1]['sub_visit'] = $('#igd_id').val();

			var dokter = $('#ndokter_igd').val();
			var perawat = $('#nperawat_igd').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url()?>igd/igddetail/save_over_igd',
				success: function(data){
					$(':input','#submitOverIGD')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');
					$('#date_igd').val("<?php echo date('d/m/Y H:i:s') ?>");

					console.log(data);

					var jml = $('#jml_overigd').val();
					var no = parseInt(jml)+1;
					var t = $('#tableOverviewIGD').DataTable();

					t.row.add([
						no,
						data['unit'],
						data['anamnesa'],
						dokter,
						perawat,
						'<a href="#riwayat_igd" data-toggle="modal" onClick="detailOverIGD(&quot;'+data['id']+'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a>',
						jml
					]).draw();

					$('#jml_overigd').val(no);
					myAlert("Data Berhasil Ditambahkan");
				}, error:function(data){
					console.log(data);
					myAlert('error');
				}
			});

		});

		$('#tindak_paramedis').focus(function(){
			var $input = $('#tindak_paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#tindak_id_paramedis').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		// $('#tindak_nama_tindakan').focus(function(){
		// 	var $input = $('#tindak_nama_tindakan');
			
		// 	$.ajax({
		// 		type:'POST',
		// 		url:'<?php echo base_url();?>igd/igddetail/get_master_tindakan',
		// 		success:function(data){
		// 			var autodata = [];
		// 			var iddata = [];

		// 			for(var i = 0; i<data.length; i++){
		// 				autodata.push(data[i]['nama_tindakan']);
		// 				iddata.push(data[i]['tindakan_id']);
		// 			}
		// 			console.log(autodata);

		// 			$input.typeahead({source:autodata, 
		// 	            autoSelect: true}); 

		// 			$input.change(function() {
		// 			    var current = $input.typeahead("getActive");
		// 			    var index = autodata.indexOf(current);

		// 			    $('#tindak_id_tindakan').val(iddata[index]);
					    
		// 			    if (current) {
		// 			        // Some item from your model is active!
		// 			        if (current.name == $input.val()) {
		// 			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
		// 			        } else {
		// 			            // This means it is only a partial match, you can either add a new item 
		// 			            // or take the active if you don't want new items
		// 			        }
		// 			    } else {
		// 			        // Nothing is active so it is a new value (or maybe empty value)
		// 			    }
		// 			});
		// 		}
		// 	});
		// });
		

		// $('#tindak_nama_tindakan').change(function() {
		// 	var tindakan_id = $('#tindak_id_tindakan').val();

		// 	if(tindakan_id!=''){
		// 		$.ajax({
		// 			type: "POST",
		// 			data : tindakan_id,
		// 			url: "<?php echo base_url()?>rawatjalan/daftarpasien/get_tindakan/" + tindakan_id,
		// 			success: function (data) {
		// 				var total = parseInt(data['js'])+parseInt(data['jp'])+parseInt(data['bakhp']);

		// 				$('#tindak_js').val(data['js']);
		// 				$('#tindak_jp').val(data['jp']);
		// 				$('#tindak_bakhp').val(data['bakhp']);
		// 				$('#tindak_tarif').val(total);

		// 				//console.log(data);
		// 			},
		// 			error: function (data) {
		// 				alert('gagal');
						
		// 			}
		// 		});
		// 	}else{
		// 		$('#tindak_js').val('');
		// 		$('#tindak_jp').val('');
		// 		$('#tindak_bakhp').val('');
		// 	}
		// });

		$('#tindak_onfaktur').keyup(function(){
			var onfaktur = $('#tindak_onfaktur').val();
			var tarif = $('#tindak_tarif').val();
			var jumlah = parseInt(onfaktur)+parseInt(tarif);
			$('#tindak_jumlah').val(jumlah);
		});

		$('#submitTindakanIGD').submit(function (e) {
			e.preventDefault();
			var item = {};
		    var number = 1;
		    item[number] = {};

			item[number]['waktu_tindakan'] = $('#tin_date').val();
			item[number]['tindakan_id'] = $('#idtindakan_klinik').val();
			item[number]['visit_id'] = $('#v_id').val();//localStorage.getItem('visit_id');//$('#visit_id').val();
			item[number]['on_faktur'] = $('#onfaktur').val();
			item[number]['paramedis'] = $('#paramedis_id').val();
			item[number]['tarif'] = $('#tarif').val();
			item[number]['jumlah'] = $('#jumlah').val();
			item[number]['sub_visit'] = $('#igd_id').val();
			item[number]['js'] = $('#js_klinik').val();
			item[number]['jp'] = $('#jp_klinik').val();
			item[number]['bakhp'] = $('#bakhp_klinik').val();
			item[number]['dept_id'] = $('#dept_id').val();
			item[number]['paramedis_lain'] = $('#paramedis_lain').val();


			$.ajax({
				type: "POST",
				data : item,
				url: "<?php echo base_url()?>igd/igddetail/save_tindakan",
				success: function (data) {
					console.log(data);
					var t = $('#tableCareIGD').DataTable();

					$(':input','#submitTindakanIGD')
					  .not(':button, :submit, :reset ')
					  .val('');
					$('#tin_date').val("<?php echo date('d/m/Y H:i:s') ?>");

					myAlert('Data Berhasil Ditambahkan');
				
					var no = $('#jml_tindak_igd').val();

					var i = parseInt(no)+1;
					t.row.add([
						i,
						data['waktu_tindakan'],
						data['nama_tindakan'],
						data['j_sarana'],
						data['j_pelayanan'],
						data['bakhp_this'],
						data['on_faktur'],
						data['nama_petugas'],
						data['jumlah'],
						"<a class='hapusTindakan' style='cursor:pointer;'><input type='hidden' class='getid' value='"+data['care_id']+"''><i class='glyphicon glyphicon-trash'></i></a>",
						i
					]).draw();
					$('#jml_tindak_igd').val(i);

					$('#kelas_klinik').prop('disabled', true);
				},
				error: function (data) {
					console.log(data);
					myAlert('gagal');
					
				}
			});
			$('#tambahTindakan').modal('hide');
		});
	
		$(document).on('click','.hapusTindakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#igd_id').val();
			var dep = $('#dept_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>igd/igddetail/hapus_tindakan/"+id+"/"+v_id+"/"+dep,
				success:function(data){
					console.log(data);

					var t = $('#tableCareIGD').DataTable();
					$('#jml_tindak_igd').val(data.length);

					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						t.row.add([
							i+1,
							data[i]['waktu_tindakan'],
							data[i]['nama_tindakan'],
							data[i]['j_sarana'],
							data[i]['j_pelayanan'],
							data[i]['bakhp_this'],
							data[i]['on_faktur'],
							data[i]['nama_petugas'],
							data[i]['jumlah'],
							"<a class='hapusTindakan' style='cursor:pointer;'><input type='hidden' class='getid' value='"+data[i]['care_id']+"''><i class='glyphicon glyphicon-trash'></i></a>",
							i
						]).draw();
					}

				},
				error:function(data){
					console.log(data);
				}	
			});
		});
		//--------------------- end overview IGD here ---------------------//

		//---------------------- pemberian resep here ---------------------//
		$('#submitresep').submit(function (e) {
			e.preventDefault();
			var item = {};
		    var number = 1;
		    item[number] = {};

		    item[number]['tanggal'] = $('#resep_date').val();
			item[number]['dokter'] = $('#resep_id_dokter').val();
			item[number]['visit_id'] = $('#v_id').val();
			item[number]['resep'] = $('#resep_deskripsi').val();
			item[number]['sub_visit'] = $('#igd_id').val();

			$.ajax({
				type: "POST",
				data : item,
				url: "<?php echo base_url()?>rawatjalan/daftarpasien/save_visit_resep",
				success: function (data) {
					console.log(data);
					var jml = $('#jml_resep').val();
					var no = parseInt(jml)+1;
					var t = $('#tableResep').DataTable();
					var date = changeDate(data['tanggal']);

					t.row.add([
						no,
						data['nama_petugas'],
						date,
						data['resep'],
						"-",
						"-",
						'<a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value="'+data['resep_id']+'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>',
						no
					]).draw();

					$('#jml_resep').val(no);
					$('#resep_namadokter').val('');
					$('#resep_id_dokter').val('');
					$('#resep_deskripsi').val('');
					$('#resep_date').val("<?php echo date('d/m/Y'); ?>");

					myAlert('Data Berhasil Ditambahkan');
				},
				error: function (data) {
					myAlert('gagal');
					
				}
			});
		});

		$(document).on('click','.hapusresep',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var igd_id = $('#igd_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>igd/igddetail/hapus_resep/"+id+"/"+igd_id,
				success:function(data){
					console.log(data);
					var t = $('#tableResep').DataTable();
					var no;
					$('#jml_resep').val(data.length);

					t.clear().draw();

					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							no = i+1;

							t.row.add([
								no,
								data[i]['nama_petugas'],
								changeDate(data[i]['tanggal']),
								data[i]['resep'],
								"-",
								"-",
								'<a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value="'+data[i]['resep_id']+'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>',
								no
							]).draw();
						}
					}
				},
				error:function(data){
					console.log(data);
				}	
			});
		});
		//---------------------- end pemerian resep here -------------------//

		//---------------------- order kamar operasi here ------------------//
		var item_order = {};
		item_order[1] = {};
		$('#submit_order_operasi').submit(function(e){
			e.preventDefault();

			item_order[1]['visit_id'] = $('#v_id').val();
			item_order[1]['sub_visit'] = $('#igd_id').val();
			item_order[1]['dept_id'] = $('#dept_id').val();
			item_order[1]['dept_tujuan'] = "KAMAR OPERASI"; 
			item_order[1]['pengirim'] = $('#iddokter_o').val();
			item_order[1]['alasan'] = $('#operasi_detail').val();
			item_order[1]['jenis_operasi'] = $('#operasi_jenis').val();
			item_order[1]['waktu_mulai'] = $('#operasi_date').val();

			var dokter = $('#namadokter_o').val();

			$.ajax({
				type: "POST",
				data: item_order,
				url: "<?php echo base_url()?>igd/igddetail/save_order_operasi",
				success: function(data){
					console.log(data);
					var i = $('#jml_data').val();
					var no = parseInt(i)+1;
					var t = $('#tableOpeasi').DataTable();

					t.row.add([
						no,
						data['waktu_mulai'],
						dokter,
						'Kamar Operasi',
						data['alasan'],
						'<center><i class="glyphicon glyphicon-trash hapusorder" data-toggle="tooltip" data-placement="top" title="Hapus" style="cursor:pointer;"><input type="hidden" class="getid" value="'+data['order_operasi_id']+'"></i></center>',
						no

					]).draw();

					$('#jml_data').val(no);

					$(':input','#submit_order_operasi')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');
					$('#operasi_date').val("<?php echo date('d/m/Y H:i:s') ?>");

					myAlert('Data Berhasil Ditambahkan');

				},
				error: function(data){
					console.log(data);
					myAlert('gagal');
				}

			});
		});

		$(document).on('click','.hapusorder',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var igd_id = $('#igd_id').val();
			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>igd/igddetail/hapus_order/"+id+"/"+igd_id,
				success:function(data){
					console.log(data);					
					var no = 0;
					var t = $('#tableOpeasi').DataTable();

					t.clear().draw();
					for(var i = 0; i< data.length; i++){
						no++;
						t.row.add([
							no,
							data[i]['waktu_mulai'],
							data[i]['nama_petugas'],
							'Kamar Operasi',
							data[i]['alasan'],
							'<i class="glyphicon glyphicon-trash hapusorder" data-toggle="tooltip" data-placement="top" title="Hapus" style="cursor:pointer;"><input type="hidden" class="getid" value="'+data[i]['order_operasi_id']+'"></i>',
							no

						]).draw();
					}

					$('#jml_data').val(data.length);
				},
				error:function(data){
					console.log(data);
				}	
			});
		});
		//-------------------- end order kamar operasi here ----------------//

		//--------------------- order konsultasi gizi here -----------------//
		var g_item = {};
		g_item[1] = {};
		$('#submit_gizi').submit(function(e){
			e.preventDefault();
			g_item[1]['tanggal'] = $('#konsul_date').val();
			g_item[1]['visit_id'] = $('#v_id').val();
			g_item[1]['sub_visit'] = $('#igd_id').val();
			g_item[1]['konsultan'] = $('#konsul_idDokter').val();
			g_item[1]['kajian_gizi'] = $('#konsul_kajiangizi').val();
			g_item[1]['anamnesa_diet'] = $('#konsul_anemdiet').val();
			g_item[1]['kajian_diet'] = $('#konsul_kajiandiet').val();
			g_item[1]['detail_menu'] =  $('#konsul_detail').val();
			var konsultan = $('#konsul_dokter').val();

			$.ajax({
				type:'POST',
				data:g_item,
				url:'<?php echo base_url(); ?>igd/igddetail/save_gizi',
				success:function(data){
					console.log(data);
					var jml = parseInt($('#jml_gizi').val());
					var t = $('#tableKonsultasi').DataTable();
					var no = jml+1;

					myAlert("data Berhasil Ditambahkan ");

					var action = '<a style="cursor:pointer;" class="hapusgizi"><input type="hidden" class="getid" value="'+data['gizi_id']+'"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>';
						action += '<a href="#print"><i class="glyphicon glyphicon-print"  data-toggle="tooltip" data-placement="top" title="Print"></i></a>';
					
					t.row.add([
						no,
						changeDate(data['tanggal']),
						konsultan,
						data['kajian_gizi'],
						data['anamnesa_diet'],
						data['kajian_diet'],
						data['detail_menu'],
						action,
						no
					]).draw();

					$('#jml_gizi').val(no);

					$(':input','#submit_gizi')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');
					$('#konsul_date').val("<?php echo date('d/m/Y H:i') ?>");

				},error:function(data){
					console.log(data);
				}
			});

		});

		$(document).on('click','.hapusgizi',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var igd_id = $('#igd_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>igd/igddetail/hapus_gizi/"+id+"/"+igd_id,
				success:function(data){
					console.log(data);
					var t = $('#tableKonsultasi').DataTable();
					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						var no = i+1;
						var action = '<a style="cursor:pointer;" class="hapusgizi"><input type="hidden" class="getid" value="'+data[i]['gizi_id']+'"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>';
							action += '<a href="#print"><i class="glyphicon glyphicon-print"  data-toggle="tooltip" data-placement="top" title="Print"></i></a>';
						
						t.row.add([
							no,
							changeDate(data[i]['tanggal']),
							data[i]['nama_petugas'],
							data[i]['kajian_gizi'],
							data[i]['anamnesa_diet'],
							data[i]['kajian_diet'],
							data[i]['detail_menu'],
							action,
							no
						]).draw();

					}

					$('#jml_gizi').val(data.length);
				},
				error:function(data){
					console.log(data);
				}	
			});
		});

		//-------------------- end order konsultasi gizi -------------------//

		//---------------------- resume pulang here ------------------//
		$('#submit_sebab').submit(function(e){
			e.preventDefault();
			var data = {};
			data['search'] = $('#sebab_katakunci').val();

			$.ajax({
				type:'POST',
				data:data,
				url:"<?php echo base_url()?>igd/igddetail/search_sebab",
				success:function(data){
					console.log(data);
					$('#tbody_sebab').empty();
 					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							var nama = data[i]['sebab_penyakit'],
								kode = data[i]['kode_sebab'],
								id = data[i]['sebab_id'];

							$("#tbody_sebab").append(
								'<tr>'+
									'<td>'+kode+'</td>'+
									'<td>'+nama+'</td>'+
									'<td style="text-align:center"><i class="glyphicon glyphicon-check" style="cursor:pointer;" onclick="getSebab(&quot;'+id+'&quot; , &quot;'+nama+'&quot;)"></i></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_sebab').empty();
						$('#tbody_sebab').append(
							'<tr>'+
					 			'<td colspan="3"><center>Data Sebab Tidak Ditemukan</center></td>'+
					 		'</tr>'
						);
					}
				}
			});
		});

		$('#submitresume').submit(function(e){
			e.preventDefault();
			var item = {};
			item[1] = {};

			item[1]['igd_id'] = $('#igd_id').val();
			item[1]['visit_id'] = $('#v_id').val();
			item[1]['waktu_keluar'] = $('#res_date').val();
			item[1]['alasan_keluar'] = $('#res_alasankeluar').val();
			item[1]['detail_alasan_pulang'] = $('#res_alasanpulang').val();
			item[1]['rs_id'] = $('#res_rsrujuk').val();
			if($('#res_datemeninggal').val()!=""){
				item[1]['waktu_kematian'] = $('#res_datemeninggal').val();
				item[1]['detail_kematian'] = $('#res_dmeninggal').val();
				item[1]['keterangan_kematian'] = $('#res_ketmeninggal').val();
			}
			item[1]['sebab'] = $('#res_idsebab').val();
			item[1]['is_lakalantas'] = ($('#res_lantas').is(":checked"))?'1':'0';
			item[1]['is_lakakerja'] = ($('#res_lkerja').is(":checked"))?'1':'0';
			item[1]['is_lakart'] = ($('#res_lakart').is(":checked"))?'1':'0';
			item[1]['is_penganiayaan'] = ($('#res_aniaya').is(":checked"))?'1':'0';
			item[1]['is_intoksinasi'] = ($('#res_intoksin').is(":checked"))?'1':'0';
			item[1]['is_gigitan'] = ($('#res_gigitan').is(":checked"))?'1':'0';
			item[1]['is_penyakitdalam'] = ($('#res_non1').is(":checked"))?'1':'0';
			item[1]['is_bedah'] = ($('#res_non2').is(":checked"))?'1':'0';
			item[1]['is_anak'] = ($('#res_non3').is(":checked"))?'1':'0';
			item[1]['is_obgsyn'] = ($('#res_non4').is(":checked"))?'1':'0';
			item[1]['is_dll'] = ($('#res_non5').is(":checked"))?'1':'0';

			item[1]['is_kunjungan_baru'] = $('input[name="res_kunjungan"]:checked', '#submitresume').val();
			item[1]['is_kasus_baru'] = $('input[name="res_jk"]:checked', '#submitresume').val();

			$.ajax({
				type:"POST",
				data:item,
				url:'<?php echo base_url();?>igd/igddetail/save_resume',
				success:function(data){
					myAlert('Data Berhasil Ditambahkan');
					console.log(data);
					window.location.href = '<?php echo base_url();?>igd/homeigd';
				},error:function(data){
					myAlert('Error');
					console.log(data);
				}
			})
		});
		//-------------------- end resume pulang here ----------------//

		//------------ pemeriksaan penunjang here --------------//

		$('#submit_penunjang').submit(function(event){
			event.preventDefault();
			var item = {};
			item[1] = {};

			item[1]['visit_id'] = $('#v_id').val();
			item[1]['sub_visit'] = $('#igd_id').val();
			item[1]['dept_tujuan'] = $('#tun_tujuan').val();
			item[1]['pengirim'] = $('#tun_iddokter').val();
			item[1]['jenis_periksa'] = $('#tun_jenis').val();
			item[1]['waktu'] = $('#tun_date').val();
			item[1]['status'] = "BELUM";

			var pengirim = $('#tun_namadokter').val();
			var unit_asal = "IGD";
			var unit_tujuan = $('#tun_tujuan option:selected').html();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>igd/igddetail/save_penunjang',
				success:function(data){
					var t = $('#table_penunjang').DataTable();
					var no = $('#tun_jumlah').val();
					var detail = '<center><input type="hidden" class="idpenunjang" value="'+data['penunjang_id']+'"><a href="#viewRiwayat" data-toggle="modal" class="detailpenunjang" data-placement="top"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Details"></i></a></center>';
					var tanggal = changeDate(data['waktu']);

					t.row.add([
						++no,
						tanggal,
						unit_tujuan,
						unit_asal,
						data['status'],
						detail,
						data['waktu']
					]).draw();

					$('#tun_jumlah').val(no);
					$('#tun_namadokter').val('');
					$('#tun_tujuan').val('');
					$('#tun_jenis').val('');

					myAlert('Data Berhasil Ditambahkan');
					console.log(data);

				},error:function(data){
					console.log(data);
					myAlert('Data Gagal Ditambahkan');
				}
			});
		});

		$('#table_penunjang').on('click', 'tr td .detailpenunjang', function(){
			var penunjang = $(this).closest('tr').find('.idpenunjang').val();

			$.ajax({
				type:'POST',
				url:'<?=base_url()?>rawatjalan/daftarpasien/get_detailpenunjang/'+penunjang,
				success:function(data){
					console.log(data);
					$('#dp_penunjang').text(data[0]['penunjang_id']);
					$('#dp_date').text(changeFakeDateTime(data[0]['waktu']));
					$('#dp_periksa').text(data[0]['nama_petugas']);
					$('#dp_dept').text(data[0]['nama_dept']);
					$('#dp_status').text(data[0]['status_penunjang']);

					$('#tbody_detail').empty();
					
					if(data[0]['status_penunjang']!='BELUM'){
						for(var i=0; i<data.length; i++){
							$('#tbody_detail').append(
								'<tr>'+
									'<td><center>'+data[i]['nama_tindakan']+'</center></td>'+
									'<td>'+data[i]['hasil']+'</td>'+
									'<td>'+data[i]['nilai_normal']+'</td>'+
									'<td>'+data[i]['ket_hasil']+'</td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_detail').append(
							'<tr>'+
								'<td colspan="4"><center>Belum Terdapat Data Pemeriksaan</center></td>'+
							'</tr>'
						);
					}
				},error:function(data){
					console.log(data);
				}
			})
		})

		//------------ end pemeriksaan penunjang here ----------//
		$("#dasbod").val('Identitas Pasien');
		$('.cl').on('click',function (e) {
			e.preventDefault();
			var a = $(this).text();
			$('#dasbod').text(a);
		})

		$('#res_alasankeluar').change(function(){
			var tipe = $(this).val();

			if(tipe == "Pasien Meninggal"){
				$('#alasanPlg').hide();
				$('#isiRujuk').hide();
				$('#pasienMeninggal').show();
				$('#detPasienMeninggal').show();
				$('#ketMati').show();
			}else if(tipe == "Rujuk Rumah Sakit Lain"){
				$('#alasanPlg').hide();
				$('#isiRujuk').show();
				$('#pasienMeninggal').hide();
				$('#detPasienMeninggal').hide();
				$('#ketMati').hide();
			}else if(tipe == "Rujuk Rawat Inap"){
				$('#alasanPlg').hide();
				$('#isiRujuk').hide();
				$('#pasienMeninggal').hide();
				$('#detPasienMeninggal').hide();
				$('#ketMati').hide();
			}else{
				$('#alasanPlg').show();
				$('#isiRujuk').hide();
				$('#pasienMeninggal').hide();
				$('#ketMati').hide();
				$('#detPasienMeninggal').hide();
			}
		})
	});

	function save_overview (item) {
		var petugas = $('#nama_dOver').val();

		$.ajax({
			type: "POST",
			data : item,
			url: "<?php echo base_url()?>igd/igddetail/save_overview",
			success: function (data) {				
				$('#rj_id').val("");
				$('#anamnesaOver').val("");
				$('#tempOver').val("");
				$('#nadiOver').val("");
				$('#pernapasanOver').val("");
				$('#beratOver').val("");
				$('#id_dokterOver').val("");
				$('#nama_dOver').val("");
				$('#k_utama_over').val("");
				$('#k_sek_over').val("");
				$('#k_sek_over2').val("");
				$('#k_sek_over3').val("");
				$('#k_sek_over4').val("");
				$('#dnama1').val("");
				$('#dnama2').val("");
				$('#dnama3').val("");
				$('#dnama4').val("");
				$('#dnama5').val("");
				$('#terapi_over').val("");
				$('#alergi_over').val("");
				$('#detail_over').val("");
				$("#tekanandarahOver").val("");

				console.log(data);

				var jml = $('#jml_over').val();
				var no = parseInt(jml)+1;
				var t = $('#tableOverviewKlinik').DataTable();

				t.row.add([
					no,
					data['unit'],
					data['anamnesa'],
					petugas,
					'<a href="#riwayat_over" data-toggle="modal" onClick="detailOver(&quot;'+data['id']+'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a>',
					jml
				]).draw();

				$('#jml_over').val(no);
				myAlert("Data Berhasil Ditambahkan");
			},
			error: function (data) {
				console.log(data);
				myAlert(data);
			}
		});

		return false;
	}

	function get_diagnosa(id, nama, diagnosa){
		if(diagnosa==1){
			$('#k_utama_over').val(id);
			$('#dnama1').val(nama);
		}else if(diagnosa==2){
			$('#k_sek_over').val(id);
			$('#dnama2').val(nama);
		}else if(diagnosa==3){
			$('#k_sek_over2').val(id);
			$('#dnama3').val(nama);
		}else if(diagnosa==4){
			$('#k_sek_over3').val(id);
			$('#dnama4').val(nama);
		}else if(diagnosa==5){
			$('#k_sek_over4').val(id);
			$('#dnama5').val(nama);
		}

		if(diagnosa==11){
			$('#kode_utama_igd').val(id);
			$('#nama_utama_igd').val(nama);
		}else if(diagnosa==12){
			$('#kode_sek1_igd').val(id);
			$('#nama_sek1_igd').val(nama);
		}else if(diagnosa==13){
			$('#kode_sek2_igd').val(id);
			$('#nama_sek2_igd').val(nama);
		}else if(diagnosa==14){
			$('#kode_sek3_igd').val(id);
			$('#nama_sek3_igd').val(nama);
		}else if(diagnosa==15){
			$('#kode_sek4_igd').val(id);
			$('#nama_sek4_igd').val(nama);
		}

		$('#tbody_diagnosa').empty();
		$('#tbody_diagnosa').append('<tr><td colspan="3" style="text-align:center;">Cari Data Diagnosa</td></tr>');
		$('#katakunci_diag').val("");
		$('#searchDiagnosa').modal('hide');
	}

	function getDokter(id, nama, dokter){
		if(dokter == 1){
			$('#id_dokterOver').val(id);
			$('#nama_dOver').val(nama);
		}
		else if(dokter == 2){
			$("#resepdokter").val(id);
			$("#namadokter").val(nama);
		}
		else if(dokter == 3){
			$("#konsul_idDokter").val(id);
			$("#konsul_dokter").val(nama);	
		}else if(dokter == 4){
			$('#resep_id_dokter').val(id);
			$('#resep_namadokter').val(nama);
		}else if(dokter == 5){
			$('#dokter_igd').val(id);
			$('#ndokter_igd').val(nama);
		}else if(dokter == 6){
			$('#tun_iddokter').val(id);
			$('#tun_namadokter').val(nama);
		}
		else{
			$("#namadokter_o").val(nama);
			$("#iddokter_o").val(id);
		}

		$("#searchDokter").modal('hide');
		$("#inputDokter").val("");
		$('#tbody_dokter').empty();
		$('#tbody_dokter').append('<tr><td colspan="3" style="text-align:center;">Cari Data Dokter</td></tr>');
	}

	function getPerawat(id, nama, perawat){
		if(perawat == 1){
			$('#perawat_igd').val(id);
			$('#nperawat_igd').val(nama);
		}

		$("#searchPerawat").modal('hide');
		$("#katakunciperawat").val("");
		$('#tbody_perawat').empty();
		$('#tbody_perawat').append('<tr><td colspan="3" style="text-align:center;">Cari Data Perawat</td></tr>');
	}

	function detailOver(id){
		var dsek1,
			dsek2,
			dsek3,
			dsek4;
		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>igd/igddetail/get_detail_over/'+id,
			success:function(data){
				console.log(data);
				$('#detail_waktu').val(data['waktu']);
				$('#detail_anamnesa').val(data['anamnesa']);
				$('#detail_tekanan').val(data['tekanan_darah']);
				$('#detail_temperatur').val(data['temperatur']);
				$('#detail_nadi').val(data['nadi']);
				$('#detail_pernapasan').val(data['pernapasan']);
				$('#detail_berat').val(data['berat_badan']);
				$('#detail_dokter').val(data['nama_petugas']);
				$('#detail_kutama').val(data['diagnosa1']);
				$('#detail_ksek1').val(data['diagnosa2']);
				$('#detail_ksek2').val(data['diagnosa3']);
				$('#detail_ksek3').val(data['diagnosa4']);
				$('#detail_ksek4').val(data['diagnosa5']);
				$('#detail_dutama').val(data['diagnosis_nama']);
				$('#detail_detail').val(data['detail_diagnosa']);
				$('#detail_terapi').val(data['terapi']);
				$('#detail_alergi').val(data['alergi']);
				$('#detail_dsek1').val("");
				$('#detail_dsek2').val("");
				$('#detail_dsek3').val("");
				$('#detail_dsek4').val("");

				dsek1 = data['diagnosa2'];
				dsek2 = data['diagnosa3'];
				dsek3 = data['diagnosa4'];
				dsek4 = data['diagnosa5'];

				if(dsek1 != null)
					get_diag_name(dsek1, 1);
				if(dsek2 != null)
					get_diag_name(dsek2, 2);
				if(dsek3 != null)
					get_diag_name(dsek3, 3);
				if(dsek4 != null)
					get_diag_name(dsek4, 4);
					
			}
		});
	}

	function detailOverIGD(id){
		var dsek1,
			dsek2,
			dsek3,
			dsek4;
		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>igd/igddetail/get_detail_over_igd/'+id,
			success:function(data){
				console.log(data);
				$('#imod_date').val(data['waktu']);
				$('#imod_anamnesa').val(data['anamnesa']);
				$('#imod_tekanandarah').val(data['tekanan_darah']);
				$('#imod_temperatur').val(data['temperatur']);
				$('#imod_nadi').val(data['nadi']);
				$('#imod_pernapasan').val(data['pernapasan']);
				$('#imod_berat').val(data['berat_badan']);
				$('#imod_kepala').val(data['kepala_leher']);
				$('#imod_thorax').val(data['thorax_abd']);
				$('#imod_ex').val(data['extrimitas']);
				$('#imod_dokter').val(data['nama_petugas']);
				$('#imod_kode_utama').val(data['diagnosa1']);
				$('#imod_kode_sek1').val(data['diagnosa2']);
				$('#imod_kode_sek2').val(data['diagnosa3']);
				$('#imod_kode_sek3').val(data['diagnosa4']);
				$('#imod_kode_sek4').val(data['diagnosa5']);
				$('#imod_nama_utama').val(data['diagnosis_nama']);
				$('#imod_detailDiagnosa').val(data['detail_diagnosa']);
				$('#imod_terapi').val(data['terapi']);
				$('#imod_nama_sek1').val("");
				$('#imod_nama_sek2').val("");
				$('#imod_nama_sek3').val("");
				$('#imod_nama_sek4').val("");

				dsek1 = data['diagnosa2'];
				dsek2 = data['diagnosa3'];
				dsek3 = data['diagnosa4'];
				dsek4 = data['diagnosa5'];

				if(dsek1 != null)
					get_diag_name(dsek1, 11);
				if(dsek2 != null)
					get_diag_name(dsek2, 12);
				if(dsek3 != null)
					get_diag_name(dsek3, 13);
				if(dsek4 != null)
					get_diag_name(dsek4, 14);
					
			}
		});
	}

	function get_diag_name(kode, i){
		$.ajax({
			type:'POST',
			url:'<?php echo base_url();?>igd/igddetail/get_diag_name/'+kode,
			success:function(data){
				console.log(data);
					if(i==1)
						$('#detail_dsek1').val(data['diagnosis_nama']);
					if(i==2)
						$('#detail_dsek2').val(data['diagnosis_nama']);
					if(i==3)
						$('#detail_dsek3').val(data['diagnosis_nama']);
					if(i==4)
						$('#detail_dsek4').val(data['diagnosis_nama']);

					if(i==11)
						$('#imod_nama_sek1').val(data['diagnosis_nama']);
					if(i==12)
						$('#imod_nama_sek2').val(data['diagnosis_nama']);
					if(i==13)
						$('#imod_nama_sek3').val(data['diagnosis_nama']);
					if(i==14)
						$('#imod_nama_sek4').val(data['diagnosis_nama']);
			}
		});
	}

	function getSebab(id, nama){
		$('#res_sebab').val(nama);
		$('#res_idsebab').val(id);

		$("#searchGolongan").modal('hide');
		$("#sebab_katakunci").val("");
		$('#tbody_sebab').empty();
		$('#tbody_sebab').append('<tr><td colspan="3" style="text-align:center;">Cari Data Perawat</td></tr>');
	}

	function changeDate(tgl_lahir){
		var remove = tgl_lahir.split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="July";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0];

		return tgl;
	}

	function myAlert(text) {
		// simulate loading (for demo purposes only)
		// setTimeout( function() {
		var m = '<p>'+text+'</p>';
		// create the notification
		var notification = new NotificationFx({
			message : m,
			layout : 'growl',
			effect : 'genie',
			type : 'notice', // notice, warning or error
		});

		// show the notification
		notification.show();

		// }, 1200 );
		// disable the button (for demo purposes only)
	}

	function changeFakeDateTime(tgl_lahir){
		var getdate = tgl_lahir.split(" ");
		var remove = getdate[0].split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0];

		return tgl;
	}
</script>
