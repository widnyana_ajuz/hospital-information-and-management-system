<div class="title">
	POLIKLINIK UMUM - INVOICE PASIEN BPJS
</div>
<div class="bar">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>rawatjalan/homerawatjalan">Poliklinik Umum</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>rawatjalan/invoicebpjs">Invoice - Nama Pasien</a>
	</li>
</div>

<input type="hidden" id="visit_id" value="<?php echo $visit_id; ?>"/>
<input type="hidden" id="sub_visit" value="<?php echo $sub_visit; ?>"/>
<input type="hidden" id="no_invoice" value="<?php echo $no_invoice; ?>"/>
<div class="backregis">
	<div id="my-tab-content" class="tab-content">
		<div id="my-tab-content" class="tab-content">
			
			<div class="informasi">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group" style="font-size:16px;">
							<label class="control-label1 col-md-4 nama">Nomor Invoice</label>
							<div class="col-md-4 nama">: <?php echo $no_invoice; ?> </div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Jenis Kunjungan</label>
							<div class="col-md-5">: <?php echo $pasien['tipe_kunjungan']; ?></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Visit ID</label>
							<div class="col-md-5">:	<?php echo $visit_id ?> </div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Kelas Perawatan</label>
							<div class="col-md-5">: Kelas <?php echo $invoice['kelas_perawatan'] ?></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Tanggal Invoice</label>
							<div class="col-md-5">: 
								<?php 
								$tgl = strtotime($invoice['tanggal_invoice']);
								$hasil = date('d F Y', $tgl); 
								echo $hasil;?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Tanggal Kunjungan</label>
							<div class="col-md-5">: <?php
								$tgl = strtotime($pasien['tanggal_visit']);
								$hasil = date('d F Y', $tgl); 
								echo $hasil;
							?></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Nomor Rekam Medis</label>
							<div class="col-md-5">:  <?php echo $pasien['rm_id']; ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Cara Bayar</label>
							<div class="col-md-5">: <?php echo $invoice['cara_bayar']; ?></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Nama Pasien</label>
							<div class="col-md-5">:  <?php echo $pasien['nama']; ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Nomor BPJS</label>
							<div class="col-md-5">: <?php echo $invoice['no_asuransi']; ?></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Alamat</label>
							<div class="col-md-5">: <?php echo $pasien['alamat_skr']; ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Kelas Pelayanan</label>
							<input type="hidden" value="<?php echo $invoice['kelas_pelayanan']; ?>" id="kelas_pelayanan">
							<div class="col-md-5">: <?php echo $invoice['kelas_pelayanan']; ?></div>
						</div>
					</div>
				</div>

			</div>

			<hr class="garis">



			
			<form class="form-horizontal" role="form">

				<div id="tagihadmisi">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Admisi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Admisi Tertagih</th>
										<th>Waktu </th>
										<th>Tarif</th>
										
									</tr>
								</thead>
								<tbody id="tbody_admisi">
									<tr>
										<td colspan="4" align="center">Tidak Terdapat Tagihan Admisi</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tagihankamar" style="margin-top:30px">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Kamar</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihankamar">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Kamar Tertagih</th>
										<th>Masuk dan Keluar</th>
										<th>Lama</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th>On Faktur</th>
										<th>Total</th>
										</tr>
								</thead>
								<tbody id="tbody_ttkamarbpjs">
									<tr>
										<td colspan="9" align="center">Tidak Terdapat Tagihan Kamar</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tagihanakomodasi" style="margin-top:30px">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Makan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanakomodasi">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Akomodasi Tertagih</th>
										<th>Unit</th>
										<th>Jumlah</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th width="100">On Faktur</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody id="tbody_ttakomodasibpjs">
									<tr>
										<td colspan="9" align="center">Tidak Terdapat Tagihan Makan</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tagihantindakanperawatan">
					<div id="titleInformasi" style="margin-bottom:-40px;"><a href="#modalttperawatanbpjs" data-toggle="modal" style="text-align:left;margin-left:-10px;font-size:12pt;color:white"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan Tindakan Perawatan">&nbsp;Tambah Tagihan Tindakan Perawatan</i></a>
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Perawatan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
					<table class="table table-striped table-bordered table-hover" id="tbtagihanperawatan">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Perawatan Tertagih</th>
										<th>Unit</th>
										<th>Waktu</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th>On Faktur</th>
										<th>Total</th>
										<th width="50">Action</th>
									</tr>
								</thead>
								<tbody id="tbody_ttperawatanbpjs">
									<tr>
										<td colspan="10" align="center">Tidak Terdapat Tagihan Admisi</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tambahtindakanpenunjang" style="margin-top:30px">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Penunjang</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
							<table class="table table-striped table-bordered table-hover" id="tbtagihanpenunjang">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Penunjang Tertagih</th>
										<th>Unit</th>
										<th>Waktu</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th width="100">On Faktur</th>
										<th>Total</th>
										
									</tr>
								</thead>
								<tbody id="tbody_ttpenunjangbpjs">
									<tr>
										<td colspan="9" align="center">Tidak Terdapat Tagihan Tindakan Penunjang</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tambahtagihantindakanoperasi" style="margin-top:30px;">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Operasi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanoperasi">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Operasi Tertagih</th>
										<th>Lingkup Operasi</th>
										<th>Waktu</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th>On Faktur</th>
										<th>Total</th>
									
									</tr>
								</thead>
								<tbody id="tbody_ttoperasibpjs">
									<tr>
										<td colspan="9" align="center">Tidak Terdapat Tagihan Tindakan Operasi</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div style="margin-right:40px;">
					<div class="form-group">
						<div class="col-md-2 pull-right">
							<label class="control-label pull-right" style="font-size:1.8em;margin-top:-10px;" id="totaltagihan">1.000.000</label>
						</div>
						<div class="col-md-4 pull-right" style="width:170px; margin-top:5px; text-align:right;">
							Total Tagihan (Rp.) : 
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-2 pull-right">
							<label class="control-label pull-right" style="font-size:1.8em;margin-top:-10px;" id="deposit" >1.000.000</label>
						</div>
						<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
							Deposit (Rp.) : 
						</div>
					</div>


					<div class="form-group">
						<div class="col-md-2 pull-right">
							<label class="control-label pull-right" style="font-size:1.8em;margin-top:-10px;" id="kekurangan">1.000.000</label>
						</div>
						<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
							Kekurangan (Rp.) : 
						</div>
					</div>
				</div>

				<br>
				<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
				<div style="margin-left:80%">
					<span style="padding:0px 10px 0px 10px;">
						<button class="btn btn-info" >CETAK</button> 
						<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
						<button class="btn btn-success" id="trigger">SIMPAN</button> 
					</span>
				</div>
				<br>
			</form>

			<div class="modal fade" id="modalttperawatanbpjs" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Tindakan Perawatan</h3>
				   			</div>
							<div class="modal-body">
								<div class="informasi">
					   				<div class="form-group">
										<label class="control-label col-md-4">Waktu Tindakan</label>
										<div class="col-md-5">	
											<input type="text" id="tin_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
										</div>
				        			</div>
				        			<div class="form-group">
									<label class="control-label col-md-4">Unit</label>
										<div class="col-md-5">	
											<input type="hidden" id="idUnit">
											<input type="text" class="form-control" id="unitTindakan" autocomplete="off" spellcheck="false"  name="unit" placeholder="Unit" >
										</div>
									</div>			
				        			<div class="form-group">
										<label class="control-label col-md-4">Tindakan</label>
										<div class="col-md-6">
											<input type="hidden" id="idtindakan_klinik">
											<textarea class="form-control" id="namatindakan" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Tindakan" ></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-4">Kelas Pelayanan</label>
										<div class="col-md-5">	
											<select class="form-control" name="kelas_tindakan" id="kelas_klinik">
												<option value="">Pilih Kelas</option>
												<option value="VIP">VIP</option>
												<option value="Utama">Utama</option>
												<option value="I">Kelas I</option>
												<option value="II">Kelas II</option>
												<option value="III">Kelas III</option>
											</select>
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Tarif</label>
										<div class="col-md-5">	
											<input type="hidden" id="js_klinik">
											<input type="hidden" id="jp_klinik">
											<input type="hidden" id="bakhp_klinik">
											<input type="text" class="form-control" id="tarif" name="tarif" placeholder="Tarif" readonly > 
										</div>
				        			</div>
				        			
				        			<div class="form-group">
										<label class="control-label col-md-4">On Faktur</label>
										<div class="col-md-5">	
											<input type="number" class="form-control" id="onfaktur" name="onfaktur" placeholder="On Faktur" >
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Jumlah</label>
										<div class="col-md-5">	
											<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" readonly>
										</div>
				        			</div>

									<div class="form-group">
										<label class="control-label col-md-4">Paramedis</label>
										<div class="col-md-5">	
											<input type="hidden" id="paramedis_id">
											<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
										</div>
				        			</div>
				        			
			        			</div>
		       				</div>
			        		<br><br>
			        		<div class="modal-footer">
			        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
						    </div>
						</div>
					</div>
				</form>
			</div>


			<br><br><br>	
		</div>
	</div>
</div>

<script type="text/javascript">
	$(window).ready(function(){
		var nomor = {};
		var jumlahtable = 0;
		var total = 0;
		var deposit = 0;
		var kekurangan = 0;

		nomor['no_invoice'] = $('#no_invoice').val();
		nomor['sub_visit'] = $('#sub_visit').val();
		nomor['kelas'] = $('#kelas_pelayanan').val();
		$.ajax({
			type:'POST',
			data:nomor,
			url:'<?php echo base_url();?>rawatjalan/invoicebpjs/create_tagihan',
			success:function(data){
				console.log(data);

				jumlahtable = data.length;

				if(data.length!=0){
					var no = 0;
					$('#tbody_ttperawatanbpjs').empty();
					for(var i = 0 ; i<data.length; i++){
						no++;
						$('#tbody_ttperawatanbpjs').append(
							'<tr>'+
								'<td>'+no+'</td>'+
								'<td>'+data[i]['nama_tindakan']+'</td>'+
								'<td>'+data[i]['nama_dept']+'</td>'+
								'<td style="text-align:center;">'+data[i]['waktu']+'</td>'+
								'<td style="text-align:right;">'+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['tarif_bpjs'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['selisih'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['total'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:center">'+
									'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
									'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
								'</td>'+
							'</tr>'
						);

						total += Number(data[i]['total']);
						kekurangan += Number(data[i]['total']);
					}
				}else{
					$('#tbody_ttperawatanbpjs').empty();
					$('#tbody_ttperawatanbpjs').append(
						'<tr><td colspan="10" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
					);
				}

				kekurangan -= deposit;
				$('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
				$('#deposit').text(deposit);
				$('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

			}, error:function(data){
				console.log(data);
			}
		});

		var tindakan_temp = [];

		$('#namatindakan').focus(function(){
			var $input = $('#namatindakan');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_master_tindakan',
				success:function(data){
					var autodata = [];
					var iddata = [];
					// var js = [];
					// var jp = [];
					// var bakhp = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_tindakan']);
						iddata.push(data[i]['tindakan_id']);
						// js.push(data[i]['js']);
						// jp.push(data[i]['jp']);
						// bakhp.push(data[i]['bakhp']);
					}

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					 //    $('#idtindakan_klinik').val(iddata[index]);
					 //    $('#js_klinik').val(js[index]);
						// $('#jp_klinik').val(jp[index]);
						// $('#bakhp_klinik').val(bakhp[index]);
						// var total = Number(js[index])+Number(jp[index])+Number(bakhp[index]);
						// $('#tarif').val(total);
						var item = {};
						item['tindakan'] = autodata[index];

						$.ajax({
							type:'POST',
							data: item,
							url:'<?php echo base_url(); ?>rawatjalan/daftarpasien/get_kelas_tindakan',
							success:function(value){

								if(value.length==1){
									$('#idtindakan_klinik').val(value[0]['tindakan_id']);
								    $('#js_klinik').val(value[0]['js']);
									$('#jp_klinik').val(value[0]['jp']);
									$('#bakhp_klinik').val(value[0]['bakhp']);
									var total = Number(value[0]['js'])+Number(value[0]['jp'])+Number(value[0]['bakhp']);
									$('#tarif').val(total);
									$('#kelas_klinik option[value=""]').attr('selected','selected');
									$('#kelas_klinik').prop('disabled', true);
								}else{
									$('#kelas_klinik').empty();
									$('#kelas_klinik').prop('disabled', false);
									$('#kelas_klinik')
									.append($("<option></option>")
									.attr("value", "")
									.text("Pilih Kelas"));

									for(var i=0; i<value.length; i++){
										$('#kelas_klinik')
										.append($("<option></option>")
										.attr("value", value[i]['tindakan_id'])
										.text(value[i]['kelas']));

										tindakan_temp.push(value[i]);
									}
								}
							}, error:function(value){
								console.log(value);
							}
						});

						// alert(index);

						// $('#kelas_klinik').

					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
		
		$('#kelas_klinik').prop('disabled', true);

		$('#kelas_klinik').change(function(){
			console.log(tindakan_temp);
			var id = $('#kelas_klinik').val();

			for(var i = 0; i<tindakan_temp.length; i++){
				if(tindakan_temp[i]['tindakan_id'] == id){
					$('#idtindakan_klinik').val(tindakan_temp[i]['tindakan_id']);
				    $('#js_klinik').val(tindakan_temp[i]['js']);
					$('#jp_klinik').val(tindakan_temp[i]['jp']);
					$('#bakhp_klinik').val(tindakan_temp[i]['bakhp']);
					var total = Number(tindakan_temp[i]['js'])+Number(tindakan_temp[i]['jp'])+Number(tindakan_temp[i]['bakhp']);
					$('#tarif').val(total);
				}
			}
		});

		$('#onfaktur').keyup(function(){
			var onfaktur = $('#onfaktur').val();
			var tarif = $('#tarif').val();
			var jumlah = parseInt(onfaktur)+parseInt(tarif);
			$('#jumlah').val(jumlah);
		});

		$('#unitTindakan').focus(function(){
			var $input = $('#unitTindakan');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/invoicenonbpjs/get_master_dept',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_dept']);
						iddata.push(data[i]['dept_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#idUnit').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		$('#submitTindakan').submit(function (e) {
			e.preventDefault();
			var item = {};
		    var number = 1;
		    item[number] = {};

		    item['waktu'] = $('#tin_date').val();
			item['tindakan_id'] = $('#idtindakan_klinik').val();
			item['on_faktur'] = $('#onfaktur').val();
			item['paramedis'] = $('#paramedis_id').val();
			item['tarif'] = $('#tarif').val();
			item['jumlah'] = $('#jumlah').val();
			item['js'] = $('#js_klinik').val();
			item['jp'] = $('#jp_klinik').val();
			item['bakhp'] = $('#bakhp_klinik').val();
			item['dept_id'] = $('#idUnit').val();
			item['kelas'] = $('#kelas_pelayanan').val();
			item['visit_id']=$('#visit_id').val();
			item['sub_visit']=$('#sub_visit').val();
			item['no_invoice']=$('#no_invoice').val();

			//item[number]['kat_id'] = $('#kategori').find('option:selected').val();

			// console.log(item);
			$.ajax({
				type: "POST",
				data : item,
				url: "<?php echo base_url()?>rawatjalan/invoicebpjs/save_tindakan",
				success: function (data) {
					console.log(data);
					//$('#namaTindakan').find('option:selected').val();
					//$('#visit_id').val('');
					$('#kelas_klinik option[value=""]').attr('selected','selected');
					$('#namatindakan').val('');
					$('#tarif').val('');
					$('#onfaktur').val('');
					$('#paramedis').val('');
					$('#tarif').val('');
					$('#jumlah').val('');
					$('#kelas_klinik').prop('disabled', true);

					//$('#kategori').find('option:selected').val();
					var no = jumlahtable;
					if(no==0){
						$('#tbody_ttperawatanbpjs').empty();
					}
					no++;
					jumlahtable++;

					$('#tbody_ttperawatanbpjs').append(
						'<tr>'+
							'<td>'+no+'</td>'+
							'<td>'+data['nama_tindakan']+'</td>'+
							'<td>'+data['nama_dept']+'</td>'+
							'<td style="text-align:center;">'+data['waktu']+'</td>'+
							'<td style="text-align:right;">'+data['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'<td style="text-align:right;">'+data['tarif_bpjs'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'<td style="text-align:right;">'+data['selisih'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'<td style="text-align:right;">'+data['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'<td style="text-align:right;">'+data['total'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'<td style="text-align:center">'+
								'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data['id']+'">'+
								'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
							'</td>'+
						'</tr>'
					);

					total += Number(data['total']);
					kekurangan += Number(data['total']);

					kekurangan -= deposit;
					$('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
					$('#deposit').text(deposit);
					$('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

				},
				error: function (data) {
					console.log(data);
					alert('gagal');
					
				}
			});

			$('#modalttperawatanbpjs').modal('hide');
		});

		$(document).on('click','.hapusTindakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#visit_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>rawatjalan/invoicenonbpjs/hapus_tindakan/"+id,
				success:function(data){
					console.log(data);

					$('#tbody_ttperawatanbpjs').empty();

					total = 0;
					kekurangan = 0;
					deposit = 0;

					if(data.length!=0){
						var no = 0;
						jumlahtable = data.length;

						for(var i = 0 ; i<data.length; i++){
							no++;
							$('#tbody_ttperawatanbpjs').append(
								'<tr>'+
									'<td>'+no+'</td>'+
									'<td>'+data[i]['nama_tindakan']+'</td>'+
									'<td>'+data[i]['nama_dept']+'</td>'+
									'<td style="text-align:center;">'+data[i]['waktu']+'</td>'+
									'<td style="text-align:right;">'+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
									'<td style="text-align:right;">'+data[i]['tarif_bpjs'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
									'<td style="text-align:right;">'+data[i]['selisih'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
									'<td style="text-align:right;">'+data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
									'<td style="text-align:right;">'+data[i]['total'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
									'<td style="text-align:center">'+
										'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
										'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
									'</td>'+
								'</tr>'
							);

							total += Number(data[i]['total']);
							kekurangan += Number(data[i]['total']);
						}
					}else{
						$('#tbody_ttperawatan').append(
							'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
						);
					}

					kekurangan -= deposit;
					$('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
					$('#deposit').text(deposit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
					$('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

				},
				error:function(data){
					console.log(data);
				}	
			});
		});

		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#paramedis_id').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
	});
</script>