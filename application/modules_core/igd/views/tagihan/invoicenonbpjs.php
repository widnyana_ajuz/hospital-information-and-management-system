<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>igd/homeigd">IGD</a>
		<i class="fa fa-angle-right"></i>
		<a href="#">Invoice - <?php echo $pasien['nama']?></a>
	</li>
</div>

<input type="hidden" id="visit_id" value="<?php echo $visit_id; ?>"/>
<input type="hidden" id="sub_visit" value="<?php echo $sub_visit; ?>"/>
<input type="hidden" id="no_invoice" value="<?php echo $no_invoice; ?>"/>
<div class="backregis" style="margin-top:30px;">
	<div id="my-tab-content" class="tab-content">
			
		<div class="informasi">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="font-size:16px;">
						<label class="control-label1 col-md-4 nama">Nomor Invoice</label>
						<div class="col-md-4 nama">: <?php echo $no_invoice; ?> </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Visit ID</label>
						<div class="col-md-5">:	<?php echo $visit_id ?> </div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Kelas Perawatan</label>
						<div class="col-md-5">: Kelas <?php echo $invoice['kelas_perawatan'] ?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Tanggal Invoice</label>
						<div class="col-md-5">: <?php 
							$tgl = strtotime($invoice['tanggal_invoice']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;
						?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Tanggal Kunjungan</label>
						<div class="col-md-5">: <?php
							$tgl = strtotime($pasien['tanggal_visit']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;
						?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nomor Rekam Medis</label>
						<div class="col-md-5">: <?php echo $pasien['rm_id']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Cara Bayar</label>
						<div class="col-md-5">: <?php echo $invoice['cara_bayar']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Pasien</label>
						<div class="col-md-5">: <?php echo $pasien['nama']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Asuransi</label>
						<div class="col-md-5">: <?php echo $invoice['nama_asuransi']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Alamat</label>
						<div class="col-md-5">: <?php echo $pasien['alamat_skr']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Perusahaan</label>
						<div class="col-md-5">: <?php echo $invoice['nama_perusahaan']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Jenis Kunjungan</label>
						<div class="col-md-5">: <?php echo $pasien['tipe_kunjungan']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nomor Ansuransi </label>
						<div class="col-md-5">: <?php echo $invoice['no_asuransi']; ?> </div>
					</div>
				</div>
			</div>
		</div>

		<hr class="garis">

		<form class="form-horizontal" role="form" id="submitTagihan">
			<div id="tagihadmisi">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Admisi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					<input type="hidden" value="<?php echo $tagihanadmisi['tarif'] ?>" id="t_admisi">
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Admisi Tertagih</th>
									<th>Waktu </th>
									<th>Tarif</th>
									
								</tr>
							</thead>
							<tbody id="tbody_resep">
								<?php
									if(!empty($tagihanadmisi)){
										$tgl = strtotime($tagihanadmisi['waktu']);
										$hasil = date('d F Y - H:i', $tgl);

										echo'
										<tr>
											<td>1</td>
											<td><center>'.$tagihanadmisi['nama_tindakan'].'</center></td>
											<td><center>'.$hasil.'</center></td>
											<td><center>'.number_format($tagihanadmisi['tarif'],0,'','.').'</center></td>
										</tr>
										<input type="hidden" value="'.$tagihanadmisi['tarif'].'" id="t_admisi">
										';
									}else{
										echo'
											<tr>
												<td colspan="4" align="center">Tidak Terdapat Tagihan Admisi</td>
											</tr>
										';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihankamar" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;"><a href="#modalttkamar" id="tambahtagihan" data-toggle="modal" style="text-align:left;margin-left:-10px;font-size:12pt;color:white"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan Kamar">&nbsp;Tambah Tagihan Kamar</i></a>
				<p style="text-align:center;margin-top:-30px;">Tagihan Kamar</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<input type="hidden" id="kamarcount" value="<?php echo count($tagihankamar); ?>">
						<?php 
							$sum = 0;

							foreach ($tagihankamar as $data) {
								if($data['tarif_lain'] != '0')
									$total = intval($data['tarif_lain'])+intval($data['on_faktur']);
								else
									$total = (intval($data['tarif'])*$data['hari'])+intval($data['on_faktur']);

								$sum += $total;
							}

							echo '<input type="hidden" id="t_kamar" value="'.$sum.'">';
						?>
						<table class="table table-striped table-bordered table-hover" id="tbtagihankamar">
							<thead>
								<tr class="info">
									<tr class="info">
									<th width="20">No.</th>
									<th>Kamar Tertagih</th>
									<th>Waktu Masuk </th>
									<th>Waktu Keluar</th>
									<th>Lama</th>
									<th>Tarif</th>
									<th>Tarif Lain</th>
									<th>On Faktur</th>
									<th>Total</th>
									<th width="50">Action</th>
								</tr>
							</thead>
							<tbody id="tbody_ttkamar">

								<?php
									if(!empty($tagihankamar)){
										$no = 0;
										foreach ($tagihankamar as $data) {
											if($data['tarif_lain'] != '0')
												$total = intval($data['tarif_lain'])+intval($data['on_faktur']);
											else
												$total = (intval($data['tarif'])*$data['hari'])+intval($data['on_faktur']);

											echo '
												<tr>
													<td>'.(++$no).'</td>
													<td>'.$data['nama_kamar'].'</td>
													<td>'.$data['tgl_masuk'].'</td>
													<td>'.$data['tgl_keluar'].'</td>
													<td>'.$data['waktu'].'</td>
													<td>'.$data['tarif'].'</td>
													<td>'.$data['tarif_lain'].'</td>
													<td>'.$data['on_faktur'].'</td>
													<td>'.$total.'</td>
													<td style="text-align:center">
														<a style="cursor:pointer" class="hapusKamar"><input type="hidden" class="getid" value="'.$data['id'].'">
														<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
													</td>
												</tr>
											';
										}
									}else{
								?>
									<tr>
										<td colspan="10" align="center">Tidak Terdapat Tagihan Kamar</td>
									</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihanakomodasi" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Makan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanakomodasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Akomodasi Tertagih</th>
									<th>Unit</th>
									<th>Jumlah</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									
								</tr>
							</thead>
							<tbody id="tbody_ttakomodasi">
								<tr>
									<td colspan="7" align="center">Tidak Terdapat Tagihan Makan</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihantindakanperawatan">
				<div id="titleInformasi" style="margin-bottom:-40px;"><a href="#modalttperawatan" data-toggle="modal" style="text-align:left;margin-left:-10px;font-size:12pt;color:white"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan Tindakan Perawatan">&nbsp;Tambah Tagihan Tindakan Perawatan</i></a>
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Perawatan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<input type="hidden" id="t_perawatan"/>
						<table class="table table-striped table-bordered table-hover" id="tbtagihanperawatan">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Perawatan Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									<th width="50">Delete</th>
								</tr>
							</thead>
							<tbody id="tbody_ttperawatan">

							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtindakanpenunjang" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Penunjang</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanpenunjang">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Penunjang Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttpenunjang">
								<?php
									$no = 0;
									$totalpenunjang = 0;
									if(!empty($tagihantunjang)){
										foreach ($tagihantunjang as $data) {
											$tgl = strtotime(substr($data['waktu'], 0, 10));
											$hasil = date('d F Y', $tgl); 

											echo '
												<tr>
													<td>'.++$no.'</td>
													<td>'.$data['nama_tindakan'].'
														<input type="hidden" class="tpenunjang_id" value="'.$data['tindakan_penunjang_id'].'">
														<input type="hidden" class="vpenunjang_id" value="'.$data['penunjang_detail_id'].'">
													</td>
													<td>'.$data['nama_dept'].'</td>
													<td>'.$hasil.'</td>
													<td style="text-align:right;">'.number_format((intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])),0,'','.').'</td>
													<td style="text-align:right;">
														<input type="hidden" class="inputtarif" value="'.(intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])).'">
														<input type="hidden" class="inputtotal">
														'.number_format($data['on_faktur'],0,'','.').'
													</td>
													<td style="text-align:right;" class="t_total">'.number_format((intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])+intval($data['on_faktur'])),0,'','.').'</td>
												</tr>
											';
											$totalpenunjang += intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])+intval($data['on_faktur']);
										}
										echo '<input type="hidden" id="t_penunjang" value="'.$totalpenunjang.'" >';
									}else{
										echo '
											<tr>
												<td colspan="7" align="center">Tidak Terdapat Tagihan Tindakan Penunjang</td>
											</tr>
										';
										echo '<input type="hidden" id="t_penunjang" value="0" >';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtagihantindakanoperasi" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Operasi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
					<table class="table table-striped table-bordered table-hover" id="tbtagihanoperasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Operasi Tertagih</th>
									<th>Lingkup Operasi</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									
								</tr>
							</thead>
							<tbody id="tbody_ttoperasi">
								<?php	
									if(!empty($tindakan)){
										echo'<input type="hidden" id="jml_table" value="no">';
										$no = 0;
										$totaloperasi = 0;
										foreach ($tindakan as $data) {
											echo '
												<tr>
													<td align="center">'.++$no.'</td>
													<td>'.$data['nama_tindakan'].'</td>
													<td>'.$data['lingkup_operasi'].'</td>
													<td style="text-align:center;">'.$data['waktu'].'</td>
													<td style="text-align:right;">'.$data['tarif'].'</td>
													<td style="text-align:right;">'.$data['on_faktur'].'</td>
													<td style="text-align:right;">'.(intval($data['tarif'])+intval($data['on_faktur'])).'</td>
												</tr>
											';	
											$totaloperasi += intval($data['tarif'])+intval($data['on_faktur']);
										}

										echo '<input type="hidden" id="t_operasi" value="'.$totaloperasi.'" >';
									}else{
								?>
								<tr>
									<?php echo'<input type="hidden" id="jml_table" value="yes">'; ?>
									<td colspan="8" align="center">Tidak Terdapat Tagihan Tindakan Operasi</td>
								</tr>
								<?php
									echo '<input type="hidden" id="t_operasi" value="0" >';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>


			<div style="margin-right:40px;">
				<br>
				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="totaltagihan" style="font-size:1.8em;margin-top:-10px;">1.000.000</label>
					</div>
					<div class="col-md-4 pull-right" style="width:170px; margin-top:5px; text-align:right;">
						Total Tagihan (Rp.) : 
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="deposit" style="font-size:1.8em;margin-top:-10px;">0</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Deposit (Rp.) : 
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="kekurangan" style="font-size:1.8em;margin-top:-10px;">1.000.000</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Kekurangan (Rp.) : 
					</div>
				</div>
				<br>
			</div>

			<br>
			<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
			<div style="margin-left:80%">
				<span style="padding:0px 10px 0px 10px;">
					<button class="btn btn-info" id="trigger">CETAK</button> 
					<button class="btn btn-success" id="trigger">SIMPAN</button> 
				</span>
			</div>
			<br>
		</form>

		<br><br><br>		
	</div>
</div>
	
	<div class="modal fade" id="modalttperawatan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Tindakan Perawatan</h3>
				   			</div>
							<div class="modal-body">
								<div class="informasi">
					   				<div class="form-group">
										<label class="control-label col-md-4">Waktu Tindakan</label>
										<div class="col-md-5">	
											<input type="text" id="tin_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
										</div>
				        			</div>
				        			<div class="form-group">
									<label class="control-label col-md-4">Unit</label>
										<div class="col-md-5">	
											<input type="hidden" id="idUnit" value="<?php echo $dept_id ?>">
											<input type="text" class="form-control" id="unitTindakan" value="IGD" name="unit" placeholder="Unit" >
										</div>
									</div>			
				        			<div class="form-group">
										<label class="control-label col-md-4">Tindakan</label>
										<div class="col-md-6">
											<input type="hidden" id="idtindakan_klinik">
											<textarea class="form-control" id="namatindakan" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Tindakan" ></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-4">Kelas Pelayanan</label>
										<div class="col-md-5">	
											<input type="hidden" id="idtindakdetail">
											<select class="form-control" name="kelas_tindakan" id="kelas_klinik">
												<option value="">Pilih Kelas</option>
												<option value="Kelas VIP">VIP</option>
												<option value="Kelas Utama">Utama</option>
												<option value="Kelas I">Kelas I</option>
												<option value="Kelas II">Kelas II</option>
												<option value="Kelas III">Kelas III</option>
											</select>
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Tarif</label>
										<div class="col-md-5">	
											<input type="hidden" id="js_klinik">
											<input type="hidden" id="jp_klinik">
											<input type="hidden" id="bakhp_klinik">
											<input type="text" class="form-control" id="tarif" name="tarif" placeholder="Tarif" readonly > 
										</div>
				        			</div>
				        			
				        			<div class="form-group">
										<label class="control-label col-md-4">On Faktur</label>
										<div class="col-md-5">	
											<input type="number" class="form-control" id="onfaktur" name="onfaktur" placeholder="On Faktur" >
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Jumlah</label>
										<div class="col-md-5">	
											<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" readonly>
										</div>
				        			</div>

									<div class="form-group">
										<label class="control-label col-md-4">Paramedis</label>
										<div class="col-md-5">	
											<input type="hidden" id="paramedis_id">
											<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Paramedis Lain</label>
										<div class="col-md-6">	
											<textarea type="text" class="form-control" id="paramedis_lain" name="paramedis" placeholder="Paramedis Lain" ></textarea>
										</div>
				        			</div>
				        			
			        			</div>
		       				</div>
			        		<br><br>
			        		<div class="modal-footer">
			        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
						    </div>
						</div>
					</div>
				</form>
			</div>

	<div class="modal fade" id="modalttkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form class="form-horizontal" role="form" method="POST" id="submitKamar">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
		   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Kamar</h3>
		   			</div>
					<div class="modal-body">
						<div class="informasi">
			   										
		        			<div class="form-group">
								<label class="control-label col-md-4">Kamar Tertagih</label>
								<div class="col-md-5">
									<input type="hidden" id="ttk_kamarid" value="<?php echo $kamar_igd['kamar_id'] ?>">
									<input type="text" class="form-control" id="ttk_namakamar" name="kamartertagih" placeholder="Kamar Tertagih" value="<?php echo $kamar_igd['nama_kamar'] ?>"> 
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">Waktu Masuk</label>
								<div class="col-md-5">	
									<div class="input-icon">
										<i class="fa fa-calendar"></i>
										<?php
											$tgl = strtotime(substr($waktu_masuk, 0, 10));
											$hasil = date('d/m/Y H:i', $tgl); 
										?>
										<input type="text" style="cursor:pointer;background-color:white" id="ttk_date_in" class="form-control" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo $hasil;?>" readonly>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">Waktu Keluar</label>
								<div class="col-md-5">	
									<div class="input-icon">
										<i class="fa fa-calendar"></i>
										<input type="text" style="cursor:pointer;background-color:white" id="ttk_date_out" class="form-control" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">Lama</label>
								<div class="col-md-5">	
									<input type="text" class="form-control" id="ttk_lama" name="lama" placeholder="Masukan Waktu Keluar" readonly> 
								</div>
		        			</div>

		        			<div class="form-group">
								<label class="control-label col-md-4">Tarif</label>
								<div class="col-md-5">	
								<input type="hidden" value="<?php echo $kamar_igd['tarif_kamar'] ?>" id="ttk_tarifkamarhari">
									<input type="text" class="form-control" id="ttk_tarifttkamar" name="tarifttkamar" placeholder="Tarif" readonly placeholder="Tarif Kamar"> 
								</div>
		        			</div>

		        			<div class="form-group">
								<label class="control-label col-md-4">Tarif Lain</label>
								<div class="col-md-5">	
									<input type="text" class="form-control" id="ttk_totallain" name="total" placeholder="Tarif Lain" >
								</div>
		        			</div>

		        			<div class="form-group">
								<label class="control-label col-md-4">On Faktur</label>
								<div class="col-md-5">	
									<input type="text" class="form-control" id="ttk_faktur" name="total" placeholder="On Faktur">
								</div>
		        			</div>
	        			</div>
       				</div>
	        		<br>
	        		<div class="modal-footer">
	        			<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
	 			     	<button type="submit" class="btn btn-success">Simpan</button>
				    </div>
				</div>
			</div>
		</form>
	</div>

<script type="text/javascript">
	$(window).ready(function(){
		var nomor = {};
		var jumlahtable = 0;
		var total = 0;
		var deposit = 0;
		var kekurangan = 0;

		nomor['no_invoice'] = $('#no_invoice').val();
		nomor['sub_visit'] = $('#sub_visit').val();
		$.ajax({
			type:'POST',
			data:nomor,
			url:'<?php echo base_url();?>rawatjalan/invoicenonbpjs/create_tagihan',
			success:function(data){
				console.log(data);

				jumlahtable = data.length;

				if(data.length!=0){
					var no = 0;

					for(var i = 0 ; i<data.length; i++){
						no++;
						$('#tbody_ttperawatan').append(
							'<tr>'+
								'<td>'+no+'</td>'+
								'<td>'+data[i]['nama_tindakan']+'</td>'+
								'<td>'+data[i]['nama_dept']+'</td>'+
								'<td style="text-align:center;">'+data[i]['waktu']+'</td>'+
								'<td style="text-align:right;">'+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:center">'+
									'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
									'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
								'</td>'+
							'</tr>'
						);

						total += Number(data[i]['jumlah']);
					}
				}else{
					$('#tbody_ttperawatan').append(
						'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
					);
				}

				$('#t_perawatan').val(total);
				sum_all_tagihan();
			}
		});

		$(document).on('click','.hapusTindakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#visit_id').val();

			var con = confirm('Anda Yakin untuk Menghapus?');

			if(con==false)
				return false;

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>rawatjalan/invoicenonbpjs/hapus_tindakan/"+id,
				success:function(data){
					console.log(data);

					$('#tbody_ttperawatan').empty();

					total = 0;
					kekurangan = 0;
					deposit = 0;

					if(data.length!=0){
						var no = 0;

						for(var i = 0 ; i<data.length; i++){
							no++;
							$('#tbody_ttperawatan').append(
								'<tr>'+
									'<td>'+no+'</td>'+
									'<td>'+data[i]['nama_tindakan']+'</td>'+
									'<td>'+data[i]['nama_dept']+'</td>'+
									'<td style="text-align:center;">'+data[i]['waktu']+'</td>'+
									'<td style="text-align:right;">'+(data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
									'<td style="text-align:right;">'+(data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
									'<td style="text-align:right;">'+(data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
									'<td style="text-align:center">'+
										'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
										'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
									'</td>'+
								'</tr>'
							);

							total += Number(data[i]['jumlah']);
							
							$('#t_perawatan').val(total);

						}
					}else{
						$('#tbody_ttperawatan').append(
							'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
						);
					}

					jumlahtable= data.length;
					sum_all_tagihan();
				},
				error:function(data){
					console.log(data);
				}	
			});
		});

		var autodata = [];
		var iddata = [];
		$('#namatindakan').focus(function(){
			var $input = $('#namatindakan');
			
			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>kamaroperasi/invoicenonbpjs/get_master_tindakan',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_tindakan']);
							iddata.push(data[i]['tindakan_id']);
						}
					}
				});
			}

			$input.typeahead({source:autodata, 
	        autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = autodata.indexOf(current);

			    $('#idtindakan_klinik').val(iddata[index]);			

			    $('#kelas_klinik').prop('disabled', false);    

			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

		});
		
		$('#kelas_klinik').prop('disabled', true);

		$('#kelas_klinik').change(function(){
			var item = {};
			item['kelas'] = $(this).val();
			item['nama'] = $('#idtindakan_klinik').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>kamaroperasi/invoicenonbpjs/get_tariftindakan',
				success:function(data){
					var tarif = 0;
					var lingkup = $('#lingkup').val();
					var persen = 0;

					$('#idtindakdetail').val(data['detail_id']);

					tarif = (Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))+((Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))*persen);
					
					$('#js_klinik').val(data['js']);
					$('#jp_klinik').val(data['jp']);
					$('#bakhp_klinik').val(data['bakhp']);
					$('#tarif').val(tarif);
					$('#jumlah').val(tarif);
				}
			});
		});

		$('#onfaktur').keyup(function(){
			var onfaktur = $('#onfaktur').val();
			var tarif = $('#tarif').val();
			var jumlah = Number(onfaktur)+Number(tarif);
			$('#jumlah').val(jumlah);
		});

		$('#onfaktur').change(function(){
			var tarif = $('#tarif').val();
			var onfaktur = $(this).val();
			var jumlah = Number(tarif)+Number(onfaktur);
			
			$('#jumlah').val(jumlah);
		});

		$('#unitTindakan').focus(function(){
			var $input = $('#unitTindakan');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/invoicenonbpjs/get_master_dept',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_dept']);
						iddata.push(data[i]['dept_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#idUnit').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#paramedis_id').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		$('#submitTindakan').submit(function(event){
			event.preventDefault();
			var item = {};

			item['waktu'] = $('#tin_date').val();
			item['tindakan_id'] = $('#idtindakdetail').val();
			item['on_faktur'] = $('#onfaktur').val();
			item['paramedis'] = $('#paramedis_id').val();
			item['tarif'] = $('#tarif').val();
			item['jumlah'] = $('#jumlah').val();
			item['js'] = $('#js_klinik').val();
			item['jp'] = $('#jp_klinik').val();
			item['bakhp'] = $('#bakhp_klinik').val();
			item['dept_id'] = $('#idUnit').val();
			item['visit_id']=$('#visit_id').val();
			item['sub_visit']=$('#sub_visit').val();
			item['no_invoice']=$('#no_invoice').val();
			item['paramedis_lain']=$('#paramedis_lain').val();

			var nama_tindakan = $('#namatindakan').val();
			var nama_dept = $('#unitTindakan').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>rawatjalan/invoicenonbpjs/save_tindakan',
				success:function(data){
					var no = jumlahtable;
					no++;

					console.log(item);

					if(jumlahtable==0)
						$('#tbody_ttperawatan').empty();

					$('#tbody_ttperawatan').append(
						'<tr>'+
							'<td>'+no+'</td>'+
							'<td>'+nama_tindakan+'</td>'+
							'<td>'+nama_dept+'</td>'+
							'<td style="text-align:center;">'+data[0]['waktu']+'</td>'+
							'<td style="text-align:right;">'+(data[0]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
							'<td style="text-align:right;">'+(data[0]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
							'<td style="text-align:right;">'+(data[0]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
							'<td style="text-align:center">'+
								'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[0]['id']+'">'+
								'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
							'</td>'+
						'</tr>'
					);

					$(':input','#submitTindakan')
					  .not(':button, :submit, :reset')
					  .val('');
					$('#tin_date').val("<?php echo date('d/m/Y H:i') ?>");

					$('#modalttperawatan').modal('hide');

					total += Number(data[0]['jumlah']);
					
					$('#t_perawatan').val(total);

					myAlert('Data Berhasil Ditambahkan');

					sum_all_tagihan();
				},error:function(data){
					alert('error');
					console.log(data);
				}
			});
		});

		$('#submitTagihan').submit(function(event){
			event.preventDefault();
			window.location.href = "<?php echo base_url();?>igd/homeigd";
		});

		$('#ttk_date_in').change(function(){
			var date1 = $('#ttk_date_in').val();
			var date2 = $('#ttk_date_out').val();
			get_days(date1, date2);
		});

		$('#ttk_date_out').change(function(){
			var date1 = $('#ttk_date_in').val();
			var date2 = $('#ttk_date_out').val();
			get_days(date1, date2);
		});

		$('#submitKamar').submit(function(event){
			event.preventDefault();
			var item = {};
			item[1] = {};

			item[1]['no_invoice'] = $('#no_invoice').val();
			item[1]['tgl_masuk'] = $('#ttk_date_in').val();
			item[1]['tgl_keluar'] = $('#ttk_date_out').val();
			item[1]['kamar_id'] = $('#ttk_kamarid').val();
			item[1]['tarif'] = $('#ttk_tarifkamarhari').val();
			item[1]['tarif_lain'] = $('#ttk_totallain').val();
			item[1]['on_faktur'] = $('#ttk_faktur').val();

			var namakamar = $('#ttk_namakamar').val();
			var lama = $('#ttk_lama').val();
			var total = Number($('#ttk_faktur').val())+Number($('#ttk_tarifttkamar').val());

			$.ajax({
				type:"POST",
				data:item,
				url:"<?=base_url()?>igd/invoicenonbpjs/save_tagihan_kamar",
				success:function(data){

					console.log(data);
					myAlert("Data Berhasil Disimpan");
					
					$('#tbody_ttkamar').empty();
					var sum = 0;

					for(var i = 0; i< data.length; i++){
						var total = 0;
						if(Number(data[i]['tarif_lain']) == 0){
							total = (Number(data[i]['tarif'])*Number(data[i]['hari']))+Number(data[i]['on_faktur']);
						}
						else{
							total = Number(data[i]['tarif_lain'])+Number(data[i]['on_faktur']);
						}

						$('#tbody_ttkamar').append(
							'<tr>'+
								'<td>'+(i+1)+'</td>'+
								'<td>IGD</td>'+
								'<td>'+data[i]['tgl_masuk']+'</td>'+
								'<td>'+data[i]['tgl_keluar']+'</td>'+
								'<td>'+data[i]['waktu']+'</td>'+
								'<td>'+data[i]['tarif']+'</td>'+
								'<td>'+data[i]['tarif_lain']+'</td>'+
								'<td>'+data[i]['on_faktur']+'</td>'+
								'<td>'+total+'</td>'+
								'<td style="text-align:center">'+
									'<a style="cursor:pointer" class="hapusKamar"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
									'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
								'</td>'+
							'</tr>'
						);

						sum += total;
					}

					$('#kamarcount').val(data.length);
					$('#t_kamar').val(sum);
					sum_all_tagihan();

					$('#modalttkamar').modal('hide');
				},error:function(data){
					console.log(data);
					myAlert("Data Tidak Dapat Disimpan");
				}
			});
		});

		// $('#ttk_faktur').keyup(function(){
		// 	sum_faktur_tarif();
		// });

		$('#tbody_ttkamar').on('click', ' tr td .hapusKamar', function(){
			var id = $(this).children('.getid').val();
			var invoice = $('#no_invoice').val();

			var action = confirm('Anda Yakin untuk Menghapus?');

			if(action == false)
				return false

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>igd/invoicenonbpjs/hapus_kamar/"+id+"/"+invoice,
				success:function(data){
					console.log(data);

					$('#tbody_ttkamar').empty();
					var sum = 0;
					for(var i = 0; i< data.length; i++){
						var total = 0;
						if(Number(data[i]['tarif_lain']) == 0){
							total = (Number(data[i]['tarif'])*Number(data[i]['hari']))+Number(data[i]['on_faktur']);
						}
						else{
							total = Number(data[i]['tarif_lain'])+Number(data[i]['on_faktur']);
						}

						$('#tbody_ttkamar').append(
							'<tr>'+
								'<td>'+(i+1)+'</td>'+
								'<td>IGD</td>'+
								'<td>'+data[i]['tgl_masuk']+'</td>'+
								'<td>'+data[i]['tgl_keluar']+'</td>'+
								'<td>'+data[i]['waktu']+'</td>'+
								'<td>'+data[i]['tarif']+'</td>'+
								'<td>'+data[i]['tarif_lain']+'</td>'+
								'<td>'+data[i]['on_faktur']+'</td>'+
								'<td>'+total+'</td>'+
								'<td style="text-align:center">'+
									'<a style="cursor:pointer" class="hapusKamar"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
									'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
								'</td>'+
							'</tr>'
						);

						sum += total;
					}

					$('#t_kamar').val(sum);
					sum_all_tagihan();

					$('#kamarcount').val(data.length);
				},error:function(data){
					console.log(data);
				}
			});
		});

	});

	function sum_all_tagihan(){
		var admisi = Number($('#t_admisi').val());
		var kamar = Number($('#t_kamar').val());
		var tindakan = Number($('#t_perawatan').val());
		var penunjang = Number($('#t_penunjang').val());
		var operasi = Number($('#t_operasi').val());

		var deposit = 0;
		var total = admisi+kamar+tindakan+penunjang+operasi;
		var kekurangan = total-deposit;

		$('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
		$('#deposit').text(deposit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
		$('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
	}

	function get_days(d1, d2){

		var date1 = new Date(changeDate(d1));
		var date2 = new Date(changeDate(d2));
		// get total seconds between the times
		var delta = Math.abs(date1 - date2) / 1000;

		// calculate (and subtract) whole days
		var days = Math.floor(delta / 86400);
		delta -= days * 86400;

		// calculate (and subtract) whole hours
		var hours = Math.floor(delta / 3600) % 24;
		delta -= hours * 3600;

		// calculate (and subtract) whole minutes
		var minutes = Math.floor(delta / 60) % 60;
		delta -= minutes * 60;

		// what's left is seconds
		var seconds = delta % 60;  // in theory the modulus is not required

		$('#ttk_lama').val(days+" Hari "+hours+" Jam")

		var tarif = $('#ttk_tarifkamarhari').val();
		var total = Number(tarif)*days;

		$('#ttk_tarifttkamar').val(total);

		//sum_faktur_tarif();
	}

	function sum_faktur_tarif(){
		var kamar = $('#ttk_temptotal').val();
		var total = Number(kamar);

		$('#ttk_tarifttkamar').val(total);
	}

	function changeDate(tgl_lahir){
		var remove = tgl_lahir.split(" ");
		var tanggalbos = remove[0].split("/");
		
		var tgl = tanggalbos[2]+"-"+tanggalbos[1]+"-"+tanggalbos[0]+" "+remove[1];

		return tgl;
	}

	function numberWithCommas(x) {
	    var parts = x.toString().split(".");
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	}
</script>