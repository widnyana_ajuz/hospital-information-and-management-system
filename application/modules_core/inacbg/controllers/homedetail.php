<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homedetail extends Operator_base {
	
	function __construct(){

		parent:: __construct();
		$this->load->model("m_inacbg");
		$data['page_title'] = "Input INA-CBG";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		redirect(base_url().'inacbg/homeinacbg');
	}

	public function proses($rm_id, $no_invoice){
		// load template
		$data['content'] = 'detail';
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['pasien'] = $this->m_inacbg->get_pasien($rm_id, $no_invoice);
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$sub_visit = $data['pasien']['sub_visit'];
		$visit_id = $data['pasien']['visit_id'];
		$data['no_invoice'] = $no_invoice;

		

		$data['datavisit'] = $this->m_inacbg->get_datavisit($sub_visit);
		$data['datadiagnosa'] = $this->m_inacbg->get_datadiagnosa($sub_visit);
		$data['datatindakan'] = $this->m_inacbg->get_datatindakan($sub_visit);
		$data['datatarif'] = $this->m_inacbg->get_datatarif($no_invoice);

		// print_r($data['datatindakan']); die;
		// $dept_id = substr($sub_visit, 0, 2);
		// $datadept = $this->m_inacbg->get_datadept($dept_id);
		//print_r($sub_visit);
		// print_r($data['datadiagnosa']); die;
		$this->load->view('base/operator/template', $data);	
	}

	public function create_inacbg(){
		$insert['no_invoice'] = $_POST['no_invoice'];
		$insert['kode_rs'] = $_POST['kode_rs'];
		$insert['kelas_rs'] = $_POST['kelas_rs'];
		$insert['no_rm'] = $_POST['no_rm'];
		$insert['kelas_perawatan'] = $_POST['kelas_perawatan'];
		$insert['biaya_perawatan'] = $_POST['biaya_perawatan'];
		$insert['jenis_perawatan'] = $_POST['jenis_perawatan'];
		$insert['tgl_masuk'] = $_POST['tgl_masuk'];
		$insert['tgl_keluar'] = $_POST['tgl_keluar'];
		$insert['lama_rawat'] = $_POST['lama_rawat'];
		$insert['tgl_lahir'] = $_POST['tgl_lahir'];
		$insert['umur_tahun'] = $_POST['umur_tahun'];
		$insert['umur_hari'] = $_POST['umur_hari'];
		$insert['jenis_kelamin'] = $_POST['jenis_kelamin'];
		$insert['cara_pulang'] = $_POST['cara_pulang'];
		$insert['berat_lahir'] = $_POST['berat_lahir'];
		$insert['diagnosa_utama'] = $_POST['diagnosa_utama'];

		if(isset($_POST['d1'])){$insert['diagnosa_sekunder1'] = $_POST['d1'];}else{$insert['diagnosa_sekunder1'] = "";}
		if(isset($_POST['d2'])){$insert['diagnosa_sekunder2'] = $_POST['d2'];}else{$insert['diagnosa_sekunder2'] = "";}
		if(isset($_POST['d3'])){$insert['diagnosa_sekunder3'] = $_POST['d3'];}else{$insert['diagnosa_sekunder3'] = "";}
		if(isset($_POST['d4'])){$insert['diagnosa_sekunder4'] = $_POST['d4'];}else{$insert['diagnosa_sekunder4'] = "";}
		if(isset($_POST['d5'])){$insert['diagnosa_sekunder5'] = $_POST['d5'];}else{$insert['diagnosa_sekunder5'] = "";}
		if(isset($_POST['d6'])){$insert['diagnosa_sekunder6'] = $_POST['d6'];}else{$insert['diagnosa_sekunder6'] = "";}
		if(isset($_POST['d7'])){$insert['diagnosa_sekunder7'] = $_POST['d7'];}else{$insert['diagnosa_sekunder7'] = "";}
		if(isset($_POST['d8'])){$insert['diagnosa_sekunder8'] = $_POST['d8'];}else{$insert['diagnosa_sekunder8'] = "";}
		if(isset($_POST['d9'])){$insert['diagnosa_sekunder9'] = $_POST['d9'];}else{$insert['diagnosa_sekunder9'] = "";}
		if(isset($_POST['d10'])){$insert['diagnosa_sekunder10'] = $_POST['d10'];}else{$insert['diagnosa_sekunder10'] = "";}
		if(isset($_POST['d11'])){$insert['diagnosa_sekunder11'] = $_POST['d11'];}else{$insert['diagnosa_sekunder11'] = "";}
		if(isset($_POST['d12'])){$insert['diagnosa_sekunder12'] = $_POST['d12'];}else{$insert['diagnosa_sekunder12'] = "";}
		if(isset($_POST['d13'])){$insert['diagnosa_sekunder13'] = $_POST['d13'];}else{$insert['diagnosa_sekunder13'] = "";}
		if(isset($_POST['d14'])){$insert['diagnosa_sekunder14'] = $_POST['d14'];}else{$insert['diagnosa_sekunder14'] = "";}
		if(isset($_POST['d15'])){$insert['diagnosa_sekunder15'] = $_POST['d15'];}else{$insert['diagnosa_sekunder15'] = "";}
		if(isset($_POST['d16'])){$insert['diagnosa_sekunder16'] = $_POST['d16'];}else{$insert['diagnosa_sekunder16'] = "";}
		if(isset($_POST['d17'])){$insert['diagnosa_sekunder17'] = $_POST['d17'];}else{$insert['diagnosa_sekunder17'] = "";}
		if(isset($_POST['d18'])){$insert['diagnosa_sekunder18'] = $_POST['d18'];}else{$insert['diagnosa_sekunder18'] = "";}
		if(isset($_POST['d19'])){$insert['diagnosa_sekunder19'] = $_POST['d19'];}else{$insert['diagnosa_sekunder19'] = "";}
		if(isset($_POST['d20'])){$insert['diagnosa_sekunder20'] = $_POST['d20'];}else{$insert['diagnosa_sekunder20'] = "";}
		if(isset($_POST['d21'])){$insert['diagnosa_sekunder21'] = $_POST['d21'];}else{$insert['diagnosa_sekunder21'] = "";}
		if(isset($_POST['d22'])){$insert['diagnosa_sekunder22'] = $_POST['d22'];}else{$insert['diagnosa_sekunder22'] = "";}
		if(isset($_POST['d23'])){$insert['diagnosa_sekunder23'] = $_POST['d23'];}else{$insert['diagnosa_sekunder23'] = "";}
		if(isset($_POST['d24'])){$insert['diagnosa_sekunder24'] = $_POST['d24'];}else{$insert['diagnosa_sekunder24'] = "";}
		if(isset($_POST['d25'])){$insert['diagnosa_sekunder25'] = $_POST['d25'];}else{$insert['diagnosa_sekunder25'] = "";}
		if(isset($_POST['d26'])){$insert['diagnosa_sekunder26'] = $_POST['d26'];}else{$insert['diagnosa_sekunder26'] = "";}
		if(isset($_POST['d27'])){$insert['diagnosa_sekunder27'] = $_POST['d27'];}else{$insert['diagnosa_sekunder27'] = "";}
		if(isset($_POST['d28'])){$insert['diagnosa_sekunder28'] = $_POST['d28'];}else{$insert['diagnosa_sekunder28'] = "";}
		if(isset($_POST['d29'])){$insert['diagnosa_sekunder29'] = $_POST['d29'];}else{$insert['diagnosa_sekunder29'] = "";}

		if(isset($_POST['t1'])){$insert['tindakan1'] = $_POST['t1'];}else{$insert['tindakan1'] = "";}
		if(isset($_POST['t2'])){$insert['tindakan2'] = $_POST['t2'];}else{$insert['tindakan2'] = "";}
		if(isset($_POST['t3'])){$insert['tindakan3'] = $_POST['t3'];}else{$insert['tindakan3'] = "";}
		if(isset($_POST['t4'])){$insert['tindakan4'] = $_POST['t4'];}else{$insert['tindakan4'] = "";}
		if(isset($_POST['t5'])){$insert['tindakan5'] = $_POST['t5'];}else{$insert['tindakan5'] = "";}
		if(isset($_POST['t6'])){$insert['tindakan6'] = $_POST['t6'];}else{$insert['tindakan6'] = "";}
		if(isset($_POST['t7'])){$insert['tindakan7'] = $_POST['t7'];}else{$insert['tindakan7'] = "";}
		if(isset($_POST['t8'])){$insert['tindakan8'] = $_POST['t8'];}else{$insert['tindakan8'] = "";}
		if(isset($_POST['t9'])){$insert['tindakan9'] = $_POST['t9'];}else{$insert['tindakan9'] = "";}
		if(isset($_POST['t10'])){$insert['tindakan10'] = $_POST['t10'];}else{$insert['tindakan10'] = "";}
		if(isset($_POST['t11'])){$insert['tindakan11'] = $_POST['t11'];}else{$insert['tindakan11'] = "";}
		if(isset($_POST['t12'])){$insert['tindakan12'] = $_POST['t12'];}else{$insert['tindakan12'] = "";}
		if(isset($_POST['t13'])){$insert['tindakan13'] = $_POST['t13'];}else{$insert['tindakan13'] = "";}
		if(isset($_POST['t14'])){$insert['tindakan14'] = $_POST['t14'];}else{$insert['tindakan14'] = "";}
		if(isset($_POST['t15'])){$insert['tindakan15'] = $_POST['t15'];}else{$insert['tindakan15'] = "";}
		if(isset($_POST['t16'])){$insert['tindakan16'] = $_POST['t16'];}else{$insert['tindakan16'] = "";}
		if(isset($_POST['t17'])){$insert['tindakan17'] = $_POST['t17'];}else{$insert['tindakan17'] = "";}
		if(isset($_POST['t18'])){$insert['tindakan18'] = $_POST['t18'];}else{$insert['tindakan18'] = "";}
		if(isset($_POST['t19'])){$insert['tindakan19'] = $_POST['t19'];}else{$insert['tindakan19'] = "";}
		if(isset($_POST['t20'])){$insert['tindakan20'] = $_POST['t20'];}else{$insert['tindakan20'] = "";}
		if(isset($_POST['t21'])){$insert['tindakan21'] = $_POST['t21'];}else{$insert['tindakan21'] = "";}
		if(isset($_POST['t22'])){$insert['tindakan22'] = $_POST['t22'];}else{$insert['tindakan22'] = "";}
		if(isset($_POST['t23'])){$insert['tindakan23'] = $_POST['t23'];}else{$insert['tindakan23'] = "";}
		if(isset($_POST['t24'])){$insert['tindakan24'] = $_POST['t24'];}else{$insert['tindakan24'] = "";}
		if(isset($_POST['t25'])){$insert['tindakan25'] = $_POST['t25'];}else{$insert['tindakan25'] = "";}
		if(isset($_POST['t26'])){$insert['tindakan26'] = $_POST['t26'];}else{$insert['tindakan26'] = "";}
		if(isset($_POST['t27'])){$insert['tindakan27'] = $_POST['t27'];}else{$insert['tindakan27'] = "";}
		if(isset($_POST['t28'])){$insert['tindakan28'] = $_POST['t28'];}else{$insert['tindakan28'] = "";}
		if(isset($_POST['t29'])){$insert['tindakan29'] = $_POST['t29'];}else{$insert['tindakan29'] = "";}
		if(isset($_POST['t30'])){$insert['tindakan30'] = $_POST['t30'];}else{$insert['tindakan30'] = "";}

		$insert['record_id'] = $_POST['record_id'];
		$insert['kode_cbg'] = $_POST['kode_cbg'];
		$insert['tarif_cbg'] = $_POST['tarif_cbg'];
		$insert['deskripsi_cbg'] = $_POST['deskripsi_cbg'];
		$insert['Alos'] = $_POST['alos'];
		$insert['nama_pasien'] = $_POST['nama_pasien'];
		$insert['dokter_pj'] = $_POST['dokter_pj'];
		$insert['no_skp'] = $_POST['no_skp'];
		$insert['no_kartu_peserta'] = $_POST['no_kartu_peserta'];
		$insert['surat_rujukan'] = $_POST['surat_rujukan'];
		$insert['Bhp'] = $_POST['bhp'];
		$insert['severiti_lv3'] = $_POST['severiti_lv3'];
		$insert['type_tarif'] = $_POST['type_tarif'];

		$in = $this->m_inacbg->save_filetext($insert);
		//print_r($insert); die;

		$this->load->view('text_inacbgs', $insert);	
		redirect(base_url()."inacbg/homeinacbg");
	}

}
