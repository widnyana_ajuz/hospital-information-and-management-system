<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );


class Homeinacbg extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_inacbg");
		$data['page_title'] = "Input INA-CBG";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load template
		$data['content'] = 'home';
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$data['datapasien'] = $this->m_inacbg->get_datapasien();
		
		$this->load->view('base/operator/template', $data);
	}
	
}
