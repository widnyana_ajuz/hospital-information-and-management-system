<?php
class m_inacbg extends CI_Model{
	public function get_datapasien(){
		$sql = "SELECT t.no_invoice, p.rm_id, p.rm_id, p.nama, t.no_asuransi, t.kelas_perawatan, t.kelas_bpjs FROM tagihan t, visit v, pasien p WHERE t.visit_id = v.visit_id AND v.rm_id = p.rm_id AND t.cara_bayar = 'BPJS'";
		return $this->db->query($sql)->result_array();
	}

	public function get_pasien($rm_id, $invoice){
		$sql = "SELECT p.*, t.no_invoice, p.rm_id, p.rm_id, p.nama, t.no_asuransi, t.kelas_perawatan, t.kelas_bpjs, t.visit_id, t.sub_visit, v.tipe_kunjungan FROM tagihan t, visit v, pasien p WHERE t.visit_id = v.visit_id AND v.rm_id = p.rm_id AND t.cara_bayar = 'BPJS' AND p.rm_id = '$rm_id' AND t.no_invoice = '$invoice'";
		return $this->db->query($sql)->row_array();
	}

	public function get_datavisit($sub){
		$sql = "SELECT vik.waktu_masuk as tanggal_masuk, vik.waktu_keluar as tanggal_keluar, vi.alasan_keluar as cara_pulang FROM visit_ri vi, visit_inap_kamar vik WHERE vi.ri_id = vik.ri_id AND vi.ri_id = '$sub'
			UNION ALL
			SELECT waktu_masuk as tanggal_masuk, waktu_keluar as tanggal_masuk, vj.alasan_keluar as cara_pulang FROM visit_rj vj WHERE vj.rj_id = '$sub'
			UNION ALL
			SELECT waktu_masuk as tanggal_masuk, waktu_keluar as tanggal_masuk, vd.alasan_keluar as cara_pulang FROM visit_igd vd WHERE vd.igd_id = '$sub'
		";
		return $this->db->query($sql)->row_array();
	}

	public function get_datadiagnosa($sub){
		$sql = "SELECT m1.diagnosis_nama d1, m2.diagnosis_nama d2, m3.diagnosis_nama d3, m4.diagnosis_nama d4, m5.diagnosis_nama d5 
					FROM overview_klinik o LEFT JOIN master_diagnosa m1 ON o.diagnosa1 = m1.diagnosis_id
					LEFT JOIN master_diagnosa m2 ON o.diagnosa2 = m2.diagnosis_id
				    LEFT JOIN master_diagnosa m3 ON o.diagnosa3 = m3.diagnosis_id
				    LEFT JOIN master_diagnosa m4 ON o.diagnosa4 = m4.diagnosis_id
				    LEFT JOIN master_diagnosa m5 ON o.diagnosa5 = m5.diagnosis_id
				    WHERE o.rj_id = '$sub' LIMIT 1
				UNION ALL
				SELECT m1.diagnosis_nama d1, m2.diagnosis_nama d2, m3.diagnosis_nama d3, m4.diagnosis_nama d4, m5.diagnosis_nama d5 
					FROM visit_perawatan_dokter o LEFT JOIN master_diagnosa m1 ON o.diagnosa_utama = m1.diagnosis_id
					LEFT JOIN master_diagnosa m2 ON o.sekunder1 = m2.diagnosis_id
				    LEFT JOIN master_diagnosa m3 ON o.sekunder2 = m3.diagnosis_id
				    LEFT JOIN master_diagnosa m4 ON o.sekunder3 = m4.diagnosis_id
				    LEFT JOIN master_diagnosa m5 ON o.sekunder4 = m5.diagnosis_id
				    WHERE o.sub_visit = '$sub' LIMIT 1
		";
		return $this->db->query($sql)->row_array();	
	}

	public function get_datatindakan($sub){
		$sql = "SELECT nama_tindakan, prosedur, (v.js+v.jp+v.bakhp+v.on_faktur) as total FROM visit_care v, master_tindakan_detail mtd, master_tindakan mt LEFT JOIN master_icd9cm mi ON mt.icd9cm - mi.id WHERE v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = '$sub'";
		return $this->db->query($sql)->result_array();
	}

	public function get_datatarif($no_invoice){
		$sql = "SELECT (tarif+on_faktur) as total FROM tagihan_akomodasi WHERE no_invoice = '$no_invoice'
					UNION ALL
					SELECT IF(tarif_lain = 0, ((tarif+on_faktur)*(IF(datediff(tgl_keluar, tgl_masuk)=0, 1, datediff(tgl_keluar, tgl_masuk)))), tarif_lain) as total FROM tagihan_kamar WHERE no_invoice = '$no_invoice'
					UNION ALL
					SELECT (tarif+on_faktur) as total FROM tagihan_operasi WHERE no_invoice = '$no_invoice'
					UNION ALL
					SELECT (tarif+on_faktur) as total FROM tagihan_penunjang WHERE no_invoice = '$no_invoice'
					UNION ALL
					SELECT (tarif+on_faktur) as total FROM tagihan_perawatan WHERE no_invoice = '$no_invoice'
				";
		return $this->db->query($sql)->result_array();
	}

	public function get_datadept($dept_id){
		$sql = "SELECT * FROM master_dept WHERE dept_id = $dept_id";
		return $this->db->query($sql)->row_array();
	}	

	public function save_filetext($value='')
    {
    	$query = $this->db->insert('input_file_teks',$value);
    	if ($query) {
    		return true;
    	}else{
    		return false;
    	}
    }

}
?>