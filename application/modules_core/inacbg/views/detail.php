<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>inacbg/homeinacbg">Input INA-CBG</a>
	</li>
</div>

<div class="backregis" style="margin-top:30px;">
	<div id="my-tab-content" class="tab-content">		
			<div class="dropdown" id="btnBawahInventori" >
	            <div id="titleInformasi">Input INA-CBG's</div>
	        </div>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>inacbg/homedetail/create_inacbg">
				<div class="informasi">
					<table border="0" width="100%" class="tbladdinacbg">
						<tr>
							<td width="20">1.</td>
							<td width="30%">Kode Rumah Sakit</td>
							<td colspan="3"> <input type="text" class="form-control input-sm" name="kode_rs" required placeholder="Kode Rumah Sakit" style="width:190px;"> </td>
							<input type="hidden" name="no_invoice" value="<?php echo $no_invoice;?>">
						</tr>
						<tr>
							<td width="20">2.</td>
							<td width="30%">Kelas Rumah Sakit</td>
							<td colspan="3"> <div class="input-group col-md-3">
									<select class="form-control input-sm" name="kelas_rs" id="kelasrs" required>
										<option value="" selected>Pilih</option>
										<option value="A">A</option>
										<option value="B">B</option>
										<option value="C">C</option>
										<option value="D">D</option>							
									</select>
								</div>	
							</td>
						</tr>
						<tr>
							<td width="20">3.</td>
							<td width="30%">Nomor Rekam Medis</td>
							<td colspan="3"> <input value="<?php echo $pasien['rm_id'] ?>" type="text" class="form-control input-sm" name="no_rm" placeholder="Nomor Rekam Medis" style="width:190px;" readonly> </td>
						</tr>
						<tr>
							<td width="20">4.</td>
							<td width="30%">Kelas Perawatan</td>
							<td colspan="3"><input value="<?php echo $pasien['kelas_perawatan'] ?>" type="text" class="form-control input-sm" name="kelas_perawatan" placeholder="III/II/I/Utama/VIP" style="width:190px;" readonly>  </td>
						</tr>
						<tr>
							<td width="20">5.</td>
							<td width="30%">Biaya Perawatan</td>
							<td colspan="3"> 
								<div class="input-group col-md-3">
									<span class="input-group-addon" id="basic-addon1">Rp.</span>
									<input type="text" class="form-control input-sm" name="biaya_perawatan" value="<?php
										$total = 0;
										foreach ($datatarif as $data) {
											$total += intval($data['total']);
										}
										echo $total;
									?>" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<td width="20">6.</td>
							<td width="30%">Jenis Perawatan</td>
							<td colspan="3"> <input type="text" value="<?php echo $pasien['tipe_kunjungan'] ?>"  class="form-control input-sm" name="jenis_perawatan" style="width:190px;" readonly> </td>
						</tr>
						<tr>
							<td width="20">7.</td>
							<td width="30%">Tanggal Masuk</td>
							<td colspan="3"> <input type="text" value="<?php 
							$tgl = strtotime($datavisit['tanggal_masuk']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;?>" class="form-control input-sm" name="tgl_masuk" style="width:190px;" readonly> </td>
						</tr>
						<tr>
							<td width="20">8.</td>
							<td width="30%">Tanggal Keluar</td>
							<td colspan="3"><input type="text" value="<?php 
							$tgl = strtotime($datavisit['tanggal_keluar']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;?>" class="form-control input-sm" name="tgl_keluar" style="width:190px;" readonly> </td>
						</tr>
						<tr>
							<td width="20">9.</td>
							<td width="30%">Lama Rawat</td>
							<td colspan="3">
								<div class="input-group col-md-3">
									<input type="text" class="form-control input-sm" name="lama_rawat" value="<?php
										$startTimeStamp = strtotime($datavisit['tanggal_keluar']);
										$endTimeStamp = strtotime($datavisit['tanggal_masuk']);

										$timeDiff = abs($endTimeStamp - $startTimeStamp);

										$numberDays = $timeDiff/86400; 
										echo intval($numberDays);
									?>" readonly>
									<span class="input-group-addon" id="basic-addon1" style="width:70px;">hari</span>
								</div>
							</td>
						</tr>
						<tr>
							<td width="20">10.</td>
							<td width="30%">Tanggal Lahir</td>
							<td colspan="3"><input value="<?php 
							$tgl = strtotime($pasien['tanggal_lahir']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;?>" type="text" class="form-control input-sm" name="tgl_lahir" style="width:190px;" readonly> </td>
						</tr>
						<tr>
							<td width="20">11.</td>
							<td width="30%">Umur Tahun</td>
							<td colspan="3">
								<div class="input-group col-md-3">
									<input type="text" class="form-control input-sm" name="umur_tahun" readonly value="<?php  
										$datetime1 = new DateTime();
										$datetime2 = new DateTime($pasien['tanggal_lahir']);
										$interval = $datetime1->diff($datetime2);
																		
										if($interval->y > 0)
											echo $interval->y;
									?>">
									<span class="input-group-addon" id="basic-addon1" style="width:70px;">tahun</span>
								</div>
							</td>
						</tr>
						<tr>
							<td width="20">12.</td>
							<td width="30%">Umur Hari</td>
							<td colspan="3"> 
								<div class="input-group col-md-3">
									<input type="text" class="form-control input-sm" name="umur_hari" readonly value="<?php
										$startTimeStamp = date('Y-m-d h:i:s');
										$endTimeStamp = strtotime($datavisit['tanggal_masuk']);

										$timeDiff = abs($endTimeStamp - $startTimeStamp);

										$numberDays = $timeDiff/86400; 
										echo intval($numberDays);
									?>">
									<span class="input-group-addon" id="basic-addon1" style="width:70px;">hari</span>
								</div>
							</td>
						</tr>
						<tr>
							<td width="20">13.</td>
							<td width="30%">Jenis Kelamin </td>
							<td colspan="3"> <input type="text" value="<?php echo $pasien['jenis_kelamin']; ?>" class="form-control input-sm" name="jenis_kelamin" style="width:190px;" readonly> </td>
						</tr>
						<tr>
							<td width="20">14.</td>
							<td width="30%">Cara Pulang</td>
							<td colspan="3"><input type="text" value="<?php echo $datavisit['cara_pulang']; ?>" class="form-control input-sm" name="cara_pulang" style="width:190px;" readonly>  </td>
						</tr>
						<tr>
							<td width="20">15.</td>
							<td width="30%">Berat Lahir</td>
							<td colspan="3"> <div class="input-group col-md-3">
									<input type="text" class="form-control input-sm" name="berat_lahir">
									<span class="input-group-addon" id="basic-addon1" style="width:70px;">gram</span>
								</div>
							</td>
						</tr>
						<tr>
							<td width="20">16.</td>
							<td width="30%">Diagnosa Utama</td>
							<td colspan="3"><input type="text" value="<?php if(!empty($datadiagnosa)) echo $datadiagnosa['d1'] ?>" class="form-control input-sm" name="diagnosa_utama" style="width:190px;" readonly></td>
						</tr>
						<tr>
							<td width="20">17.</td>
							<td width="30%">Diagnosa Sekunder</td>
							<!-- <td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 1" data-toggle="modal" data-target="#searchDiagnosa" name="dns1" style="width:190px;"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 11" data-toggle="modal" data-target="#searchDiagnosa" name="dns11" style="width:190px;"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 21" data-toggle="modal" data-target="#searchDiagnosa" name="dns21" style="width:190px;"></td> -->
						</tr>
						<tr>
							<td width="20"></td>
							<td>
							<a href="#tambahDiagnosa" data-toggle="modal"><i class="fa fa-plus" style="margin-left:0px;font-size:11pt;">&nbsp;Tambah Diagnosa</i></a>
							<div class="clearfix"></div>
							</td>
							
						</tr>
						<tr>
							<td width="20"></td>
							<td colspan="4">
								<div class="portlet-body" style="margin: 10px 80px 0px 0px">
								<table class="table table-striped table-bordered table-hover table-responsive">
									<thead>
										<tr class="info" >
											<th width="20"> No. </th>
											<th> Diagnosa </th>
											<th width="80"> Action </th>			
										</tr>
									</thead>
									<tbody id="tbody_sekunder">
										<?php
											$no = 0;

										if(!empty($datadiagnosa)){
											if($datadiagnosa['d2']!=""){
												echo '
													<tr>
														<td style="text-align:center">'.++$no.'</td>
														<td><input type="text" class="form-control" value="'.$datadiagnosa['d2'].'" id="diagnosaTindakan" name="d1" placeholder="Diagnosa"></td>
														<td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>
													</tr>
												';
											}
											if($datadiagnosa['d3']!=""){
												echo '
													<tr>
														<td style="text-align:center">'.++$no.'</td>
														<td><input type="text" class="form-control" value="'.$datadiagnosa['d3'].'" id="diagnosaTindakan" name="d2" placeholder="Diagnosa"></td>
														<td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>
													</tr>
												';
											}
											if($datadiagnosa['d4']!=""){
												echo '
													<tr>
														<td style="text-align:center">'.++$no.'</td>
														<td><input type="text" class="form-control" value="'.$datadiagnosa['d4'].'" id="diagnosaTindakan" name="d3" placeholder="Diagnosa"></td>
														<td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>
													</tr>
												';
											}
											if($datadiagnosa['d5']!=""){
												echo '
													<tr>
														<td style="text-align:center">'.++$no.'</td>
														<td><input type="text" class="form-control" value="'.$datadiagnosa['d5'].'" id="diagnosaTindakan" name="d4" placeholder="Diagnosa"></td>
														<td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>
													</tr>
												';
											}
										}
										?>
										
									</tbody>
								</table>
							</div>
							</td>
						</tr>
						
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<td width="20">18.</td>
							<td width="30%">Prosedur/Tindakan ICD-9-CM</td>
							<!-- <td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 1" data-toggle="modal" data-target="#searchDiagnosa" name="dns1" style="width:190px;"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 11" data-toggle="modal" data-target="#searchDiagnosa" name="dns11" style="width:190px;"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 21" data-toggle="modal" data-target="#searchDiagnosa" name="dns21" style="width:190px;"></td> -->
						</tr>
						<tr>
							<td width="20"></td>
							<td>
							<a href="#searchICD" data-toggle="modal"><i class="fa fa-plus" style="margin-left:0px;font-size:11pt;">&nbsp;Tambah Prosedur/Tindakan ICD-9-CM</i></a>
							<div class="clearfix"></div>
							</td>
							
						</tr>
						<tr>
							<td width="20"></td>
							<td colspan="4">
								<div class="portlet-body" style="margin: 10px 80px 0px 0px">
								<table class="table table-striped table-bordered table-hover table-responsive">
									<thead>
										<tr class="info" >
											<th width="20"> No. </th>
											<th> Prosedur </th>
											<th width="80"> Action </th>
										</tr>
									</thead>
									<tbody id="tbody_prosedur">
										
									</tbody>
								</table>
							</div>
							</td>
						</tr>


						<!-- <tr>
							<td width="20">18.</td>
							<td width="30%">Prosedur/Tindakan ICD-9-CM</td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 1" data-toggle="modal" data-target="#searchICD9" name="dns1" style="width:190px;"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 11" data-toggle="modal" data-target="#searchICD9" name="dns11" style="width:190px;"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 21" data-toggle="modal" data-target="#searchICD9" name="dns21" style="width:190px;"></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 2" data-toggle="modal" data-target="#searchICD9" name="dns2" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 12" data-toggle="modal" data-target="#searchICD9" name="dns12" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 22" data-toggle="modal" data-target="#searchICD9" name="dns22" style="width:190px;" ></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 3" data-toggle="modal" data-target="#searchICD9" name="dns3" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 13" data-toggle="modal" data-target="#searchICD9" name="dns13" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 23" data-toggle="modal" data-target="#searchICD9" name="dns23" style="width:190px;" ></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 4" data-toggle="modal" data-target="#searchICD9" name="dns4" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 14" data-toggle="modal" data-target="#searchICD9" name="dns14" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 24" data-toggle="modal" data-target="#searchICD9" name="dns24" style="width:190px;" ></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 5" data-toggle="modal" data-target="#searchICD9" name="dns1" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 15" data-toggle="modal" data-target="#searchICD9" name="dns15" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 25" data-toggle="modal" data-target="#searchICD9" name="dns25" style="width:190px;" ></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 6" data-toggle="modal" data-target="#searchICD9" name="dns6" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 16" data-toggle="modal" data-target="#searchICD9" name="dns16" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 26" data-toggle="modal" data-target="#searchICD9" name="dns26" style="width:190px;" ></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 7" data-toggle="modal" data-target="#searchICD9" name="dns7" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 17" data-toggle="modal" data-target="#searchICD9" name="dns17" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 27" data-toggle="modal" data-target="#searchICD9" name="dns27" style="width:190px;" ></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 8" data-toggle="modal" data-target="#searchICD9" name="dns8" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 18" data-toggle="modal" data-target="#searchICD9" name="dns18" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 28" data-toggle="modal" data-target="#searchICD9" name="dns28" style="width:190px;" ></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 9" data-toggle="modal" data-target="#searchICD9" name="dns9" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 19" data-toggle="modal" data-target="#searchICD9" name="dns19" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 29" data-toggle="modal" data-target="#searchICD9" name="dns29" style="width:190px;" ></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%"></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 10" data-toggle="modal" data-target="#searchICD9" name="dns10" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 20" data-toggle="modal" data-target="#searchICD9" name="dns20" style="width:190px;" ></td>
							<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 30" data-toggle="modal" data-target="#searchICD9" name="dns30" style="width:190px;" ></td>
						</tr>
						<tr> -->
							<td width="20">19.</td>
							<td width="30%">Record ID/No. Urut dalam file tersebut</td>
							<td colspan="3"><input type="text" class="form-control input-sm" name="record_id" style="width:190px;"></td>
						</tr>
						<tr>
							<td width="20">20.</td>
							<td width="30%">Kode CBG</td>
							<td colspan="3"><input type="text" class="form-control input-sm" name="kode_cbg" style="width:190px;" readonly></td>
						</tr>
						<tr>
							<td width="20">21.</td>
							<td width="30%">Tarif CBG</td>
							<td colspan="3"><input type="text" class="form-control input-sm" name="tarif_cbg" style="width:190px;" readonly></td>
						</tr>
						<tr>
							<td width="20">22.</td>
							<td width="30%">Deskripsi CBG</td>
							<td colspan="3"><input type="text" class="form-control input-sm" name="deskripsi_cbg" style="width:190px;" readonly></td>
						</tr>
						<tr>
							<td width="20">23.</td>
							<td width="30%">ALOS</td>
							<td colspan="3"><input type="text" class="form-control input-sm" name="alos" value="0" style="width:130px;" readonly></td>
						</tr>
						<tr>
							<td width="20">24.</td>
							<td width="30%">Nama Pasien</td>
							<td colspan="3"><input type="text" value="<?php echo $pasien['nama'] ?>" class="form-control input-sm" name="nama_pasien" style="width:190px;" readonly></td>
						</tr>
						<tr>
							<td width="20">25.</td>
							<td width="30%">Dokter Penanggung Jawab</td>
							<td colspan="3"><input type="text" class="form-control input-sm"  id="paramedis" autocomplete="off" spellcheck="false"  name="dokter_pj" placeholder="Dokter Penanggung Jawab"  style="width:190px;" ></td>
						</tr>
						<tr>
							<td width="20">26.</td>
							<td width="30%">Nomor SKP</td>
							<td colspan="3"><input type="text" class="form-control input-sm" name="no_skp" style="width:190px;"></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td width="30%">Nomor Kartu Peserta</td>
							<td colspan="3"><input type="text" class="form-control input-sm" name="no_kartu_peserta" style="width:190px;"></td>
						</tr>
						<tr>
							<td width="20">27.</td>
							<td width="30%">Surat Rujukan</td>
							<td colspan="3">
								<div class="input-group col-md-3">
									<select class="form-control input-sm" name="surat_rujukan" id="surat_rujukan">
										<option value="" selected>Pilih</option>
										<option value="ada">Ada</option>
										<option value="tanpa surat rujukan">Tanpa Surat Rujukan</option>
																
									</select>
								</div>		
							</td>
						</tr>
						<tr>
							<td width="20">28.</td>
							<td width="30%">BHP (jika ada)</td>
							<td colspan="3"><input type="text" class="form-control input-sm" name="bhp" style="width:190px;"></td>
						</tr>
						<tr>
							<td width="20">29.</td>
							<td width="30%">Harga BHP</td>
							<td colspan="3">
								<div class="input-group col-md-3">
									<span class="input-group-addon" id="basic-addon1">Rp.</span>
									<input type="text" class="form-control input-sm" name="harga_bhp">
								</div>
							</td>
						</tr>
						<tr>
							<td width="20">30.</td>
							<td width="30%">Severiti level 3</td>
							<td colspan="3">
								<div class="input-group col-md-3">
									<select class="form-control input-sm" name="severiti_lv3" id="severiti_lv3">
										<option value="" selected>Pilih</option>
										<option value="ada">Ada</option>
										<option value="tidak ada">Tidak Ada</option>
																
									</select>
								</div>	
							</td>
						</tr>
						<tr>
							<td width="20">31.</td>
							<td width="30%">Tipe Tarif sesuai Rumah Sakit</td>
							<td colspan="3"><input type="text" class="form-control input-sm" name="type_tarif" style="width:190px;"></td>
						</tr>

					</table>
				</div>
				<br>
				
				<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
				<div style="margin-left:80%">
					<span style="padding:0px 10px 0px 10px;">
						<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
						<button type="submit" class="btn btn-success">SIMPAN</button> 
					</span>
				</div>
				<br>

			</form>

	</div>

	<div class="modal fade" id="tambahDiagnosa" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Diagnosa</h3>
        			</div>
        			<div class="modal-body">
        				<form class="form-horizontal" role="form" method="post" id="search_diagnosa">
							<div class="form-group">	
								<div class="col-md-5">
									<input type="text" class="form-control" name="katakunci" id="katakunci_diag" placeholder="Kata kunci"/>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-info">Cari</button>
								</div>	
							</div>	
						</form>
						
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table style="table-layout:fixed" class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa" style="table-layout:fixed">
								<thead>
									<tr class="info">
										<th width="30%;">Kode Diagnosa</th>
										<th>Keterangan</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_diagnosa">
									<tr>
										<td style="text-align:center;" colspan="3">Cari Diagnosa</td>
									</tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
	</div>

	<div class="modal fade" id="searchICD" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h3 class="modal-title" id="myModalLabel">Pilih ICD-9CM</h3>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<form class="form-horizontal" role="form" method="post" id="cari_icd">
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Kata kunci"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div>
						</form>
					</div>
					<br>
					<div style="margin-left:5px; margin-right:5px;"><hr></div>
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover" id="tabelSearchICD">
							<thead>
								<tr class="info">
									<th width="30%;">Kode</th>
									<th>Keterangan</th>
									<th width="10%">Pilih</th>
								</tr>
							</thead>
							<tbody id="tbody_icd">
								<tr>
									<td style="text-align:center;" class="kosong" colspan="4">Cari ICD-9CM</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				</div>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">
	$(window).ready(function(){
		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
		
		$('#search_diagnosa').submit(function(e){
			e.preventDefault();
			var key = $('#katakunci_diag').val();

			$.ajax({
				type:'POST',
				url:'<?php echo base_url() ?>bersalin/bersalindetail/search_diagnosa/'+key,
				success:function(data){
					$('#tbody_diagnosa').empty();

					if(data.length>0){
						for(var i = 0; i<data.length;i++){
							$('#tbody_diagnosa').append(
								'<tr>'+
									'<td>'+data[i]['diagnosis_id']+'</td>'+
									'<td max-width="60%" style="word-wrap: break-word;white-space: pre-wrap; ">'+data[i]['diagnosis_nama']+'</td>'+
									'<td style="text-align:center; cursor:pointer;"><a onclick="get_diagnosa(&quot;'+data[i]['diagnosis_id']+'&quot;, &quot;'+data[i]['diagnosis_nama']+'&quot;)"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_diagnosa').append(
							'<tr><td colspan="3" style="text-align:center;">Data Diagnosa Tidak Ditemukan</td></tr>'
						);
					}
				}, error:function(data){
					console.log(data);
					myAlert('gagal');
				}
			});
		});

		$('#cari_icd').submit(function(e){
	      e.preventDefault();
	      var katakunci = {};
	      katakunci['search'] = $('#katakunci').val();
	      $.ajax({
	        type: "POST",
	        data: katakunci,
	        url: "<?php echo base_url()?>master/addtarif/search_icd",
	        success: function(data){
	          $('#tbody_icd').empty();
	          if (data.length > 0) {
	            for (var i = 0; i < data.length; i++) {
	              $('#tbody_icd').append(
	                '<tr>'+
	                  '<td>' + data[i]['kode']+'</td>'+
	                  '<td>' + data[i]['prosedur']+'</td>'+
	                   '<td style="text-align:center; cursor:pointer;"><a onclick="get_icd9cm(&quot;'+data[i]['kode']+'&quot;, &quot;'+data[i]['prosedur']+'&quot;)"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>'+
	                '</tr>'
	              )
	            };
	          }
	        },
	        error: function(data){
	          console.log(data);
	        }
	      })
	    });
	});

	function get_diagnosa(id, nama){
		var row = $('#tbody_sekunder tr').length;

		if((row+1)<30)
			$('#tbody_sekunder').append('<tr><td style="text-align:center">'+(row+1)+'</td><td><input type="text" class="form-control" value="'+nama+'" id="diagnosaTindakan" name="d'+(row+1)+'" placeholder="Diagnosa"></td><td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');

		$('#tbody_diagnosa').empty();
		$('#tbody_diagnosa').append('<tr><td colspan="3" style="text-align:center;">Cari Data Diagnosa</td></tr>');
		$('#katakunci_diag').val("");
		$('#tambahDiagnosa').modal('hide');
	}

	function get_icd9cm(id, nama){
		var row = $('#tbody_prosedur tr').length;

		if((row+1)<31)
			$('#tbody_prosedur').append(
					'<tr>'+
						'<td style="text-align:center">'+(row+1)+'</td>'+
						'<td><input value="'+nama+'" type="text" class="form-control" id="tarifSekunder" name="t'+(row+1)+'" placeholder="Prosedur"></td>'+
						'<td><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
					'</tr>'
				);

		$('#tbody_diagnosa').empty();
		$('#tbody_diagnosa').append('<tr><td colspan="3" style="text-align:center;">Cari Data Diagnosa</td></tr>');
		$('#katakunci_diag').val("");
		$('#tambahDiagnosa').modal('hide');
	}
</script>