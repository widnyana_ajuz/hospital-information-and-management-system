<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>inacbg/homeinacbg">INPUT INA-CBG's</a>
	</li>
</div>

<div class="backregis" style="margin-top:30px;">
	<div id="my-tab-content" class="tab-content">		
			<div class="dropdown" id="btnBawahInventori" >
	            <div id="titleInformasi">Tabel INA-CBG's</div>
	        </div>
			<div class="portlet-body" style="margin: 10px 10px 0px 10px">
				<table class="table table-striped table-bordered table-hover tableDT" id="tableinacbgbpjs">
					<thead>
						<tr class="info">
							<th width="20">No.</th>
							<th>#Rekam Medis</th>
							<th>Nama Pasien</th>
							<th>Nomor BPJS</th>
							<th>Kelas BPJS</th>
							<th>Kelas Perawatan</th>
							<th width="80">Action</th>
						</tr>
					</thead>
					<tbody id="tbody_resep">
						<?php
							$no = 0;
							foreach ($datapasien as $data) {
								echo '
									<tr>
										<td width="20" align="center">'.++$no.'</td>
										<td>'.$data['rm_id'].'</td>
										<td>'.$data['nama'].'</td>
										<td style="text-align:right">'.$data['no_asuransi'].'</td>
										<td>Kelas '.$data['kelas_bpjs'].'</td>
										<td>Kelas '.$data['kelas_perawatan'].'</td>
										<td style="text-align:center">
											<a href="'.base_url().'inacbg/homedetail/proses/'.$data['rm_id'].'/'.$data['no_invoice'].'">
											<i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
										</td>
									</tr>			
								';
							}
						?>
						<!-- <tr>
							<td width="20" align="center">No.</td>
							<td style="text-align:right">123</td>
							<td>Bejo</td>
							<td style="text-align:right">123123</td>
							<td>kelas 1</td>
							<td>Kelas 1</td>
							<td style="text-align:center">
								<a href="<?php echo base_url() ?>inacbg/homedetail">
								<i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
							</td>
						</tr> -->
					</tbody>
				</table>
			</div>
	</div>
</div>

