<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homeinstalasigizi extends Operator_base {
	protected $dept_id;

	function __construct()
	{
		parent:: __construct();
		$this->load->model('m_gizi');
		$data['page_title'] = "Instalasi Gizi";
		$this->dept_id = $this->m_gizi->get_dept_id('INSTALASI GIZI')['dept_id'];
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$this->session->set_userdata($data);
		// load template
		$data['content'] = 'home';
		$data['paketmakan'] = $this->m_gizi->get_all_paket();
		$data['referensi'] = $this->m_gizi->get_all_referensi();
		$data['totreferensi'] = $this->m_gizi->get_referensi_total();
		$data['list_makan'] = $this->m_gizi->get_all_list_by_date(date('Y-m-d'));
		$data['javascript'] = 'j_instalasi';
		$this->load->view('base/operator/template', $data);
	}

	public function get_all_paket()
	{
		$res = $this->m_gizi->get_all_paket();
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function getdetailpaket($id)
	{
		$res = $this->m_gizi->get_detail_paket($id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function get_all_list_by_date()
	{
		$tgl = DateTime::createFromFormat('d/m/Y', $_POST['tgl']);
		$newDateString = $tgl->format('Y-m-d');
		$result = $this->m_gizi->get_all_list_by_date($newDateString);

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function get_tipe_diet()
	{
		$result = $this->m_gizi->get_tipe_diet();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_paket_by_diet($id)
	{
		$result = $this->m_gizi->get_paket_by_diet($id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function add_paket_makan()
	{
		$this->form_validation->set_rules('tipe_diet', 'Tipe Diet', 'required|trim|xss_clean');
		$this->form_validation->set_message('required', 'isi semua data dengan benar');

		if ($this->form_validation->run() == TRUE) {
			$data = $_POST['data'];
			$insert = array(
					'nama_paket' => $_POST['nama_paket'],
					'kelas' => $_POST['kelas'],
					'tipe_diet' => $_POST['tipe_diet'],
					'harga_total' => $_POST['harga_total']
			);

			$id = $this->m_gizi->add_paket($insert);
			if ($id) {
				foreach ($data as $value) {
					$insdetail = array(
						'menu' => $value[4],
						'jenis' => $value[7],
						'harga' => $value[6],
						'paket_id' => $id
					);
					$res = $this->m_gizi->add_paket_detail($insdetail);
					if ($res) {
						$result = $id;
					}
				}
			} else {
				$result = array(
					'message' => 'Gagal menyimpan data',
					'error' => 'y'
				);
			}
		}else {
			$result = array(
				'message' => strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			 );
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function edit_paket_makan($id)
	{
		$update = array('nama_paket' => $_POST['nama_paket']);
		$res = $this->m_gizi->update_paket($id, $update);
		if ($res) {
			$result = array(
				'message' => 'Data berhasil disimpan',
				'error' => 'n'
			 );
		}else {
			$result = array(
				'message' => 'Gagal mengubah data',
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function edit_menu_paket($id)
	{
		$data = $_POST['data'];
		$update = array(
				'nama_paket' => $_POST['nama_paket'],
				'harga_total' => $_POST['harga_total']
		);
		$res_upd = $this->m_gizi->update_paket($id, $update);

		if ($res_upd) {
			foreach ($data as $value) {
				$insdetail = array(
					'menu' => $value[5],
					'jenis' => $value[8],
					'harga' => $value[7],
					'paket_id' => $id
				);
				$res = $this->m_gizi->add_paket_detail($insdetail);
				if ($res) {
					$result = array(
						'message' => 'Data berhasil disimpan',
						'error' => 'n'
					 );
				}
			}
		}else {
			$result = array(
				'message' => 'Gagal mengubah data',
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function delete_detail($id_paket, $id_detail, $hargabaru)
	{ //delete detail, update harga total paket
		$res = $this->m_gizi->delete_detail_paket($id_detail);

		if ($res) {
			$update['harga_total'] = $hargabaru;

			$res_upd = $this->m_gizi->update_paket($id_paket, $update);

			if ($res_upd) {
				$result = array(
					'message' => 'Data berhasil dihapus',
					'error' => 'n'
				);
			}
		}else{
			$result = array(
				'message' => 'Gagal menghapus data',
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function addreferensi()
	{
		$this->form_validation->set_rules('jenis_makanan', 'Jenis Makanan', 'required|trim|xss_clean');
		$this->form_validation->set_rules('takaran', 'Takaran Saji', 'required|trim|xss_clean');
		$this->form_validation->set_rules('kalori', 'Kalori', 'required|trim|xss_clean');

		$this->form_validation->set_message('required', 'isi semua data dengan benar');
$result = "";
		if ($this->form_validation->run() == TRUE) {
			$insert['jenis_makanan'] = $_POST['jenis_makanan'];
			$insert['takaran'] = $_POST['takaran'];
			$insert['kalori'] = $_POST['kalori'];

			$result = $this->m_gizi->add_referensi($insert);

		}else {
			$result = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}
		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function updatereferensi($id)
	{
		$data['jenis_makanan'] = $_POST['jenis_makanan'];
		$data['takaran'] = $_POST['takaran'];
		$data['kalori'] = $_POST['kalori'];
		$this->m_gizi->update_referensi($id, $data);
	}

	public function update_permintaan($id)
	{
		$data['paket_makan'] = $_POST['paket_makan'];
		$data['status'] = $_POST['status'];
		$this->m_gizi->update_permintaan($id, $data);
	}


}
