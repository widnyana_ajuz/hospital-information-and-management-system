<?php
  class M_gizi extends CI_Model
  {
    function __construct(){}

    public function get_dept_id($nama)
    {
      $sql = "SELECT dept_id from master_dept WHERE nama_dept LIKE '$nama'";
      $res = $this->db->query($sql);
      if ($res){
        return $res->row_array();
      }
      else {
        return false;
      }
    }

    public function get_all_referensi()
    {
      $sql = "SELECT * FROM gizi_referensi";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_referensi_total()
    {
      $sql = "SELECT COUNT(id) AS total FROM gizi_referensi";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

    public function add_referensi($value)
    {
      $res = $this->db->insert('gizi_referensi', $value);
      if ($res) {
        return $this->db->insert_id();
      }
      else {
        return false;
      }
    }

    public function update_referensi($id, $data)
    {
      $this->db->where('id', $id);
      $res = $this->db->update('gizi_referensi', $data);
      if ($res) {
        return true;
      }else {
        return false;
      }
    }

    public function get_tipe_diet()
    {
      $sql = "SELECT id, tipe_diet FROM master_tipe_diet";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_all_paket()
    {
      $sql = "SELECT gpm.id AS id, nama_paket, kelas, mtd.tipe_diet AS tipe, harga_total FROM gizi_paket_makan gpm, master_tipe_diet mtd WHERE gpm.tipe_diet = mtd.id";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_paket_by_diet($diet)
    {
      $sql = "SELECT id, nama_paket FROM gizi_paket_makan WHERE tipe_diet = $diet";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_detail_paket($id)
    {
      $sql = "SELECT * FROM gizi_paket_makan_detail WHERE paket_id = $id ORDER BY jenis ASC, harga DESC";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function add_paket($value)
    {
      $result =  $this->db->insert('gizi_paket_makan', $value);
			if ($result) {
				return $this->db->insert_id();
			}else{
				return false;
			}
    }

    public function update_paket($id, $data)
    {
      $this->db->where('id', $id);
      $res = $this->db->update('gizi_paket_makan', $data);
      if ($res) {
        return true;
      }else {
        return false;
      }
    }

    public function add_paket_detail($value)
    {
      $result =  $this->db->insert('gizi_paket_makan_detail', $value);
			if ($result) {
				return $this->db->insert_id();
			}else{
				return false;
			}
    }

    public function delete_detail_paket($id)
    {
      $this->db->where('id', $id);
      $result = $this->db->delete('gizi_paket_makan_detail');
      if($result){
        return true;
      }else {
        return false;
      }
    }

    public function get_all_list_by_date($date)
    {
      $sql = "SELECT vpm.makan_id AS id, nama_dept AS unit, nama_kamar, nama_bed, vpm.visit_id, v.rm_id, nama, paket_makan, gpm.nama_paket AS nama_paket, mtd.tipe_diet AS tipe_diet, vpm.tipe_diet AS id_tipe, vpm.keterangan AS keterangan, status
              FROM visit_permintaan_makan vpm LEFT JOIN gizi_paket_makan gpm ON vpm.paket_makan=gpm.id
	             LEFT JOIN master_kamar mk ON vpm.kamar_id=mk.kamar_id
               LEFT JOIN master_bed mb ON vpm.bed_id=mb.bed_id
               LEFT JOIN master_tipe_diet mtd ON vpm.tipe_diet=mtd.id
               LEFT JOIN visit v ON vpm.visit_id=v.visit_id
               LEFT JOIN pasien p ON v.rm_id=p.rm_id
               LEFT JOIN master_dept md ON SUBSTRING(vpm.sub_visit,1,2)=md.dept_id
              WHERE waktu_permintaan LIKE '$date%'";

      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function update_permintaan($id, $data)
    {
      $this->db->where('makan_id', $id);
      $res = $this->db->update('visit_permintaan_makan', $data);
      if ($res) {
        return true;
      }else {
        return false;
      }
    }


  }
?>
