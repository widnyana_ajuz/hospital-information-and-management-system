<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>instalasigizi/home">INSTALASI GIZI</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Master Paket Makan</a>
	</li>
</div>
<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	  <li class="active"><a href="#master" class="cl" data-toggle="tab">Master Paket Makan</a></li>
	  <li><a href="#referensi" class="cl" data-toggle="tab">Master Referensi Gizi</a></li>
	  <li><a href="#list" class="cl" data-toggle="tab">List Permintaan Makan</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
		<div class="modal fade" id="detailPaket" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content" >
					<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    				<h3 class="modal-title" id="myModalLabel">Detail Paket</h3>
    			</div>
    			<div class="modal-body">
        		<form class="form-horizontal" role="form">
							<div class="form-group">
		         		<label class="control-label col-md-3">Nama Paket</label>
					   		<div class="col-md-5">
					   			<input type="text" class="form-control" placeholder="Nama Paket" id="namaPaket" name="namaPaket" readonly>
					   		</div>
	          	</div>
							<div class="form-group">
								<label class="control-label col-md-3">Kelas</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="klsPaket" name="klsPaket"  placeholder="Kelas" readonly>
								</div>
							</div>
							<div class="form-group">
							 <label class="control-label col-md-3">Tipe Diet</label>
							 <div class="col-md-5">
								 <input type="text" class="form-control" id="tipeDiet" name="tipeDiet" placeholder="Tipe Diet" readonly>
							 </div>
						 </div>
		      		<div class="form-group">
								<label class="control-label col-md-3">Harga Total</label>
								<div class="col-md-5">
									<div class="input-group">
							      <div class="input-group-addon">Rp</div>
							    	<input type="text" class="form-control text-right" id="hrgPkt" name="hrgPkt" placeholder="Harga Total" readonly>
							    </div>
					   		</div>
							</div>
							<br>
		        	<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th width="50%">Menu</th>
										<th>Jenis</th>
										<th>Harga</th>
									</tr>
								</thead>
								<tbody id="tbodydetailpaket">
									<tr><td colspan="4" style="text-align:center">Tidak ada detail</td></tr>
								</tbody>
							</table>
 						</form>
        	</div>
        	<div class="modal-footer">
 			    	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			    </div>
				</div>
			</div>
		</div>

    <div class="modal fade" id="edPaket" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-400px;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:1000px;" >
					<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h3 class="modal-title" id="myModalLabel">Edit Paket</h3>
        	</div>
					<form class="form-horizontal" role="form" style="margin-left:40px">
        		<div class="modal-body">
							<div class="form-group">
								<label class="control-label col-md-3">Nama Paket</label>
								<div class="col-md-5">
									<input type="text" class="form-control" placeholder="Nama Paket" id="ednamaPaket" name="ednamaPaket" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Kelas</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="edkelasIG" id="edkelasIG" readonly>
								</div>
							</div>
							<div class="form-group">
		          	<label class="control-label col-md-3">Tipe Diet</label>
					     	<div class="col-md-5">
					     		<input type="text" id="ed_tipediet" class="form-control" placeholder="Tipe Diet Penyakit" readonly>
					     	</div>
	            </div>

		    			<div class="form-group">
								<label class="control-label col-md-3">Harga Total</label>
								<div class="col-md-5">
									<div class="input-group">
							      <div class="input-group-addon">Rp</div>
							    	<input type="number" class="form-control text-right" id="hrgPktIG" name="hrgPktIG" placeholder="Harga Total" readonly>
							    </div>

									<input type="hidden" id="idedit" name="idedit">
									<input type="hidden" id="hargasebelumedit" name="hargasebelumedit">
					   		</div>
							</div>
		        	<a href="#" class="edtambahmenu" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Menu Makanan" style="margin-left : 0px">&nbsp;Tambah Menu</i></a>
							<div class="clearfix"></div>
		          <div class="form-group">
							  <div class="portlet-body" style="margin: 10px 40px 0px 10px">
							    <table class="table table-striped table-bordered table-hover">
										<thead>
											<tr class="info">
												<th width="20">No.</th>
												<th width="50%">Menu</th>
												<th >Jenis</th>
												<th width="25%">Harga</th>
												<th >Action</th>
											</tr>
										</thead>
										<tbody id="editmenu">
										</tbody>
									</table>
								</div>
							</div>
        		</div>
        		<div class="modal-footer">
							<button type="button" id="btnBatal" class="btn btn-danger">Batal</button>
							<button type="submit" class="btn btn-success" id="btnsimpanedpaket">Simpan</button>
			      </div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal fade" id="edList" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content" >
					<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h3 class="modal-title" id="myModalLabel">Ubah Permintaan Makan</h3>
        	</div>
					<form class="form-horizontal" role="form" method="post" id="frmeditpermintaan">
        		<div class="modal-body">
							<div class="form-group">
								<label class="control-label col-md-2">Paket</label>
								<div class="col-md-10">
									<input type="hidden" id="paket_id">
									<input type="hidden" id="tipediet_id">
									<input type="text" class="form-control" id="ac_namapaket" autocomplete="off" spellcheck="false" placeholder="Nama Paket Makan">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Status</label>
								<div class="col-md-5">
									<select class="form-control" id="stsList" name="stsList">
										<option value="Sudah Dikirim">Sudah Dikirim</option>
										<option value="Belum Dikirim">Belum Dikirim</option>
									</select>
								</div>
								<input type="hidden" id="idpermintaan">
							</div>
        		</div>
        		<div class="modal-footer">
							<button type="reset" class="btn btn-danger" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-success">Simpan</button>
			      </div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal fade" id="edGizi" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content" >
					<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h3 class="modal-title" id="myModalLabel">Edit Referensi Gizi</h3>
        	</div>
					<form class="form-horizontal" role="form">
        		<div class="modal-body">
							<div class="form-group">
								<label class="control-label col-md-3">Jenis Makanan</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="edJnsRG" name="edJnsRG" placeholder="Jenis Makanan" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Takaran Saji</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" class="form-control" id="edTkrnSaji" name="edTkrnSaji" required>
										<span class="input-group-addon">gram</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Kalori</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" class="form-control" id="edKkalRG" name="edKkalRG" required>
										<span class="input-group-addon">kkal&nbsp;</span>
									</div>
									<input type="hidden" id="idref" name="idref">
								</div>
							</div>
        		</div>
        		<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
							<button type="submit" id="btnsimpanedref" class="btn btn-success">Simpan</button>
			      </div>
					</form>
				</div>
			</div>
		</div>

    <div class="tab-pane active" id="master">
    	<div class="dropdown" id="btnBawahAdaanGudang"  style="margin-left:10px;width:98.5%">
	      <div id="titleInformasi">Tambah Paket Makan</div>
	      <div class="btnBawah" ><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
      </div>
      <br>
      <div class="tutupBiru" id="infoAdaanGudang">
      	<form class="form-horizontal" role="form" method="post" id="frmsimpanpaket">
      		<div class="informasi">
					<div class="form-group">
	    			<label class="control-label col-md-3">Nama Paket</label>
	    			<div class="col-md-4">
	    				<input type="text" class="form-control" id="nmPaketIG" name="nmPaketIG" placeholder="Nama Paket" required>
	    			</div>
	    		</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kelas</label>
						<div class="col-md-3">
							<select class="form-control select" name="kelasIG" id="kelasIG" required>
								<option value="" selected>Pilih</option>
								<option value="Kelas III">Kelas III</option>
								<option value="Kelas II">Kelas II</option>
								<option value="Kelas I">Kelas I</option>
								<option value="Utama">Utama</option>
								<option value="VIP">VIP</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Tipe Diet Penyakit</label>
						<div class="col-md-4">
							<input type="hidden" id="diet_id">
							<input type="text" class="form-control" id="tipedietval" autocomplete="off" spellcheck="off"  name="tipedietval" placeholder="Tipe Diet Penyakit" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Harga Total</label>
						<div class="col-md-2">
							<div class="input-group">
								<div class="input-group-addon">Rp</div>
								<input type="text" class="form-control text-right" id="hrgTotIG" name="hrgTotIG" value="0" readonly>
							</div>
						</div>
					</div>
        	<br>
        	</div>

					<hr class="garis">
				<div class="informasi">
					<br>
					<a href="#" class="tmbhMenu" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Menu Makanan" style="margin-left : 0px">&nbsp;Tambah Menu</i></a>
					<div class="clearfix"></div>
				</div>
					<div class="form-group">
					  <div class="portlet-body" style="margin: 10px 25px 0px 25px">
					    <table class="table table-striped table-bordered table-hover">
								<thead>
									<tr class="info">
										<th>Menu</th>
										<th width="200">Jenis</th>
										<th width="200">Harga</th>
										<th width="80">Action</th>
									</tr>
								</thead>
								<tbody id="addMenu">
									<tr class="kosong"><td colspan="4" style="text-align:center">Tambah detail paket makan</td></tr>
								</tbody>
							</table>
						</div>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning resetpaket">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button>
						</span>
					</div>
					<br>
      	</form>
      </div>

 			<div class="dropdown"  id="btnBawahRiwaAdaGudang" style="margin-left:10px;width:98.5%">
        <div id="titleInformasi">Daftar Paket Makan</div>
        <div  class="btnBawah" ><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
      </div>
      <br>
    	<div class="tutupBiru" id="infoRiwaAdaGudang">
      	<form class="form-horizontal" role="form">
	    		<div class="form-group">
				    <div class="portlet-body" style="margin: 0px 25px 0px 25px">
			        <table class="table table-striped table-bordered table-hover tableDT" id="tabelpaket">
								<thead>
									<tr class="info">
										<th width="3%">No.</th>
										<th width="50px;">Nama Paket</th>
										<th style="width:12%;">Kelas</th>
										<th style="width:10%;">Tipe Diet</th>
										<th width="50px;">Harga Total</th>
										<th width="5%">Action</th>
									</tr>
								</thead>
								<tbody id="tbodypaketmakan">
									<?php
										if (isset($paketmakan)) {
											if (!empty($paketmakan)) {
												$i = 0;
												foreach ($paketmakan as $value) {
													echo '<tr>
														<td class="text-center">'.(++$i).'</td>
														<td>'.$value['nama_paket'].'</td>
														<td>'.$value['kelas'].'</td>
														<td>'.$value['tipe'].'</td>
														<td class="text-right">'.number_format($value['harga_total'], 0, '', '.').'</td>
														<td style="text-align:center">
															<input type="hidden" id="idpaket" value="'.$value['id'].'">
															<a href="#detailPaket" class="viewpaket" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>
															<a href="#edPaket" class="editpaket" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
														</td>
													</tr>';
												}
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
	      </form>
	    </div>
    </div>

    <div class="tab-pane" id="referensi">
      <div class="dropdown" id="btnBawahReferensi" style="margin-left:10px;width:98.5%">
	      <div id="titleInformasi">Tambah Referensi Gizi</div>
	      <div  class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
      </div>
      <br>
      <div class="tutupBiru" id="infoReferensi">
      	<form class="form-horizontal" id="tambahreferensi" role="form" method="post">
				<div class="informasi">
					<div class="form-group">
						<label class="control-label col-md-3">Jenis Makanan</label>
						<div class="col-md-3">
							<input type="text" class="form-control" id="jnsRG" name="jnsRG" placeholder="Jenis Makanan" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Takaran Saji</label>
						<div class="col-md-2">
							<div class="input-group">
								<input type="text" class="form-control text-right" id="tkrnSaji" name="tkrnSaji" placeholder="0" required>
								<span class="input-group-addon">gram</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kalori</label>
						<div class="col-md-2">
							<div class="input-group">
								<input type="text" class="form-control text-right" id="kkalRG" name="kkalRG" placeholder="0" required>
								<span class="input-group-addon">kkal&nbsp;</span>
							</div>
						</div>
					</div>
				</div>
    			<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button class="btn btn-success">SIMPAN</button>
						</span>
					</div>
					<br>
	      </form>
	    </div>

	    <div class="dropdown"  id="btnBawahReferensi2" style="margin-left:10px;width:98.5%">
	      <div id="titleInformasi">Daftar Referensi Gizi</div>
	      <div  class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
      </div>
      <br>
      <div class="tutupBiru" id="infoReferensi2">
      	<form class="form-horizontal" role="form">
	    		<div class="form-group">
				    <div class="portlet-body" style="margin: 0px 25px 0px 25px">
							<?php
								if(isset($totreferensi))
								{
									echo '<input type="hidden" id="totalreferensi" value="'.$totreferensi['total'].'">';
								}else{
									echo '<input type="hidden" id="totalreferensi" value="0">';
								}
							?>
		          <table class="table table-striped table-bordered table-hover tableDT" id="tabelreferensi">
								<thead>
									<tr class="info">
										<th style="width:3%;">No.</th>
										<th width="50px;">Jenis Makanan</th>
										<th style="width:12%;">Takaran Saji (gram)</th>
										<th style="width:10%;">Kalori (kkal)</th>
										<th style="width:3%;">Action</th>
									</tr>
								</thead>
								<tbody id="tbodyreferensi">
									<?php
										if (isset($referensi)) {
											if (!empty($referensi)) {
												$i = 0;
												foreach ($referensi as $value) {
													echo '<tr>
														<td align="center">'.(++$i).'</td>
														<td class="jnsval">'.$value['jenis_makanan'].'</td>
														<td align="right" class="tkrnval">'.number_format($value['takaran'], 0, '', '.').'</td>
														<td align="right" class="kalval">'.number_format($value['kalori'], 0, '', '.').'</td>
														<td style="text-align:center">
															<input type="hidden" class="ref_id" value="'.$value['id'].'">
															<a href="#edGizi" class="editreferensi" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
														</td>
													</tr>';
												}
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
	      </form>
	    </div>
    </div>

    <div class="tab-pane" id="list">
    	<div class="dropdown"  style="margin-left:10px;width:98.5%">
	      <div id="titleInformasi">List Permintaan Makan</div>
	     
      </div>
      <br>
      	<form class="form-horizontal" role="form">
      			<div class="informasi">
					<div class="form-group">
						<label class="control-label col-md-3">Tanggal Permintaan Makan</label>
						<div class="col-md-2" style="margin-left:-100px">
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer; background-color:white" class="form-control" data-date-autoclose="true" data-date-format="dd/mm/yyyy" data-provide="datepicker" id="tgl_list" value="<?php echo date('d/m/Y')?>" readonly>
							</div>
						</div>
					</div>
				</div>
          <div class="form-group">
					  <div class="portlet-body" style="margin: 0px 25px 0px 25px">
				      <table class="table table-striped table-bordered table-hover tableDTUtamaScroll">
								<thead>
									<tr class="info">
										<th style="width:10%;">No.</th>
										<th >Unit</th>
										<th >Ruang</th>
										<th >Bed</th>
										<th >Nama Pasien</th>
										<th >Nama Paket</th>
										<th >Tipe Diet Penyakit</th>
										<th >Keterangan</th>
										<th >Status</th>
										<th style="width:20%;">Action</th>
									</tr>
								</thead>
								<tbody id="tbodypermintaan">
									<?php
										if (isset($list_makan)) {
											if (!empty($list_makan)) {
												$i = 0;
												foreach ($list_makan as $value) {
													echo '<tr>
														<td>'.(++$i).'</td>
														<td>'.$value['unit'].'</td>
														<td>'.$value['nama_kamar'].'</td>
														<td>'.$value['nama_bed'].'</td>
														<td>'.$value['nama'].'</td>
														<td class="paketval">'.$value['nama_paket'].'</td>
														<td>'.$value['tipe_diet'].'</td>
														<td>'.$value['keterangan'].'</td>
														<td class="statusval">'.$value['status'].'</td>
														<td style="text-align:center">
															<a href="#edList" class="editpermintaan" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
															<input type="hidden" class="idpermintaan_val" value="'.$value['id'].'">
															<input type="hidden" class="idpaket_val" value="'.$value['paket_makan'].'">
															<input type="hidden" class="idtipe_val" value="'.$value['id_tipe'].'">
														</td>
													</tr>';
												}
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
        </form>
    </div>
  </div>
</div>


<script type="text/javascript">
	$(document).ready( function(){
			// $('.tmbhMenu').on('click',function(e){
			// 	e.preventDefault();
			// 	tambahMenu('#addMenu','.tmbhMenu');
			// });
			//
			// $('.editmenu').on('click',function(e){
			// 	e.preventDefault();
			// 	tambahMenu('#editmenu','.editmenu');
			// });

			$("#infoRiwaAdaGudang").hide();
			$("#infoReferensi2").hide();
			$("#btnBawahReferensi").click(function(){
				$("#infoReferensi").slideToggle();
			});

			$("#btnBawahReferensi2").click(function(){
				$("#infoReferensi2").slideToggle();
			});
	});

</script>
