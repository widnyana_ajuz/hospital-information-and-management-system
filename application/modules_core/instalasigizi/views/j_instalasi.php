<script type="text/javascript">
  $(document).ready(function(){
    //autonumeric
    jQuery(function($) {
      $('#tkrnSaji').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#kkalRG').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#edTkrnSaji').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#edKkalRG').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
    });

    //tambah referensi baru
    $('#tambahreferensi').submit(function(e){
      e.preventDefault();
      var item = {};
      item['jenis_makanan'] = $('#jnsRG').val();
      item['takaran'] = $('#tkrnSaji').autoNumeric('get');
      item['kalori'] = $('#kkalRG').autoNumeric('get');

      $.ajax({
        type: "POST",
        data: item,
        url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/addreferensi",
        success: function(data){
          $('#jnsRG').val('');
          $('#tkrnSaji').val('');
          $('#kkalRG').val('');
          myAlert('DATA BERHASIL DITAMBAH');

          var tot = Number($('#totalreferensi').val()) + 1;
          $('#totalreferensi').val(tot);

          var tabel = $('#tabelreferensi').DataTable();
          var act = '<input type="hidden" class="ref_id" value="'+data+'">'+
          '<a href="#edGizi" class="editreferensi" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>';

          tabel.row.add([
            tot,
            item['jenis_makanan'],
            item['takaran'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
            item['kalori'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
            act
          ]).draw();
        },
        error: function (data) {
					console.log(data);
				}
      })
    });

    //ambil data, tampilin di popup edit referensi
    var tdganti = '';
    $('#tbodyreferensi').on('click', 'tr td a.editreferensi', function(e){
      e.preventDefault();

      tdganti = $(this);

      $('#edJnsRG').val($(this).closest('tr').find('td').eq(1).text());
      $('#edTkrnSaji').val($(this).closest('tr').find('td').eq(2).text());
      $('#edKkalRG').val($(this).closest('tr').find('td').eq(3).text());
      $('#idref').val($(this).closest('tr').find('td .ref_id').val());
    });

    //simpan editan referensi
    $('#frmeditref').submit(function(e){
      e.preventDefault();
      var item = {};
      var id = $('#idref').val();
      item['jenis_makanan'] = $('#edJnsRG').val();
      item['takaran'] = $('#edTkrnSaji').autoNumeric('get');
      item['kalori'] = $('#edKkalRG').autoNumeric('get');

      $.ajax({
        type: "POST",
        data: item,
        url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/updatereferensi/" + id,
        success: function(data){
          tdganti.closest('tr').find('td.jnsval').text($('#edJnsRG').val());
          tdganti.closest('tr').find('td.tkrnval').text($('#edTkrnSaji').val());
          tdganti.closest('tr').find('td.kalval').text($('#edKkalRG').val());
          $('#edGizi').modal('hide');
          myAlert('DATA BERHASIL DIUBAH');
        },
        error: function(data){
          console.log(data);
        }
      })
    });

    //masukin data ke autocomplete tipe diet
    $('#tipedietval').focus(function(){
      var $input = $('#tipedietval');

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/get_tipe_diet",
        success: function(data){
          var autodata = [];
          var iddata = [];
          for (var i = 0; i < data.length; i++) {
            autodata.push(data[i]['tipe_diet']);
            iddata.push(data[i]['id']);
          }
          $input.typeahead({
            autoSelect: true,
            source: autodata
          });

          $input.change(function(){
            var current = $input.typeahead("getActive");
            var index = autodata.indexOf(current);

            $('#diet_id').val(iddata[index]);

            if (current) {
                // Some item from your model is active!
                if (current.name == $input.val()) {
                    // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
                } else {
                    // This means it is only a partial match, you can either add a new item
                    // or take the active if you don't want new items
                }
            } else {
                // Nothing is active so it is a new value (or maybe empty value)
            }
          });

        },
        error: function(data){
          console.log(data);
        }
      });
    });


    //tambah baris detail paket makan
    var counter = 0;
    $('.tmbhMenu').on('click', function(e){
      e.preventDefault();
      counter += 1;

      $('#addMenu').find('tr.kosong').remove();
      $('#addMenu').append(
        '<tr>'+
          '<td><input type="text" class="form-control" id="namaMenu" name="namaMenu" required></td>'+
          '<td>'+
            '<select class="form-control" required>'+
              '<option value="" selected>Pilih</option>'+
              '<option value="Makan">Makan</option>'+
              '<option value="Snack">Snack</option>'+
            '</select>'+
          '</td>'+
          '<td>'+
            '<div class="input-group">'+
              '<div class="input-group-addon">Rp</div>'+
              '<input type="text" class="form-control text-right hargamenu" id="hrgMenu'+counter+'" value="0">'+
            '</div>'+
            '<input type="hidden" class="valhargamenu">'+
          '</td>'+
          '<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
        '</tr>'
      );
      $('#hrgMenu'+counter+'').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
    })

    $('#addMenu').on('change', 'tr td .hargamenu', function(e){
      e.preventDefault();
      $(this).closest('tr').find('td .valhargamenu').val($(this).autoNumeric('get'));
      hitung_harga();
    })

    $('#addMenu').on('click', 'tr td a.removeRow', function(e){
      e.preventDefault();
      $(this).closest('tr').remove();
      hitung_harga();
    })

    $('.resetpaket').on('click', function(e){
      $('#addMenu').empty('');
      $('#addMenu').append('<tr class="kosong"><td colspan="4" style="text-align:center">Tambah detail paket makan</td></tr>');
    })

    //simpan paket makan
    $('#frmsimpanpaket').submit(function(e){
      e.preventDefault();
      var item={};
      item['nama_paket'] = $('#nmPaketIG').val();
      item['kelas'] = $('#kelasIG').find('option:selected').val();
      item['tipe_diet'] = $('#diet_id').val();
      item['diet'] = $('#tipedietval').val();

      $('#addMenu').find('tr.kosong').remove();
      var datadetail = hitung_harga();

      if (datadetail.length == 0) {
        alert('Isi detail paket makan!');
        $('#addMenu').append('<tr class="kosong"><td colspan="4" style="text-align:center">Tambah detail paket makan</td></tr>');
        return false;
      }

      item['data'] = datadetail;
      item['harga_total'] = $('#hrgTotIG').val().toString().replace(/[^\d\,\-\ ]/g, '');

      $.ajax({
        type: "POST",
        data: item,
        url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/add_paket_makan",
        success: function(data){
          console.log(data);
          resetpaket();
          myAlert('DATA BERHASIL DITAMBAH');

          window.location.replace("<?php echo base_url()?>instalasigizi/homeinstalasigizi");
        },
        error: function(data){
          console.log(data);
        }
      });
    })

    //popup edit paket makan
    $('#tbodypaketmakan').on('click', 'tr td a.editpaket', function(e){
      e.preventDefault();
      var id = $(this).closest('tr').find('td #idpaket').val();

      $('#ednamaPaket').val($(this).closest('tr').find('td').eq(1).text());
      $('#edkelasIG').val($(this).closest('tr').find('td').eq(2).text());
      $('#ed_tipediet').val($(this).closest('tr').find('td').eq(3).text());
      $('#hrgPktIG').val($(this).closest('tr').find('td').eq(4).text());
      $('#idedit').val(id);

      $('#hargasebelumedit').val($('#hrgPktIG').val().toString().replace(/[^\d\,\-\ ]/g, ''));

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/getdetailpaket/" + id,
        success: function(data){
          console.log(data);
          if (data.length > 0) {
            $('#editmenu').empty();
            for (var i = 0; i < data.length; i++) {
              $('#editmenu').append(
                '<tr><td>'+(i+1)+'</td>'+
                  '<td>'+data[i]['menu']+'</td>'+
                  '<td>'+data[i]['jenis']+'</td>'+
                  '<td class="text-right">'+data[i]['harga'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td style="display:none">'+data[i]['id']+'</td>'+
                  '<td style="text-align:center"><a href="#" class="delete"><i class="glyphicon glyphicon-trash"></i></a></td>'+
                '</tr>'
              )
            };
          }
          else{
            $('#editmenu').empty();
            $('#editmenu').append('<tr class="kosong"><td colspan="5" style="text-align:center">Tidak ada detail</td></tr>');
          }
        },
        error: function(data){
          console.log(data);
        }
      });
    })

    //tambah baris detail paket makan di popup edit
    var counter_edit=0;
    $('.edtambahmenu').on('click', function(e){
      e.preventDefault();
      counter_edit += 1;
      $('#editmenu').find('tr.kosong').remove();
      $('#editmenu').append(
        '<tr class="tambah"><td></td>'+
          '<td><input type="text" class="form-control" id="ednamaMenu" name="ednamaMenu" required></td>'+
          '<td>'+
            '<select class="form-control" required>'+
              '<option value="" selected>Pilih</option>'+
              '<option value="Makan">Makan</option>'+
              '<option value="Snack">Snack</option>'+
            '</select>'+
          '</td>'+
          '<td>'+
            '<div class="input-group">'+
              '<div class="input-group-addon">Rp</div>'+
              '<input type="text" class="form-control text-right editharga" id="edhrgMenu'+counter_edit+'" value="0">'+
            '</div>'+
            '<input type="hidden" class="valeditharga">'+
          '</td>'+
          '<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
        '</tr>'
      );
      $('#edhrgMenu'+counter_edit+'').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
    })

    $('#editmenu').on('change', 'tr td .editharga', function(e){
      e.preventDefault();
      $(this).closest('tr').find('td .valeditharga').val($(this).autoNumeric('get'));
      hitung_harga_edit();
    })

    $('#editmenu').on('click', 'tr td a.removeRow', function(e){
      e.preventDefault();
      $(this).closest('tr').remove();
      hitung_harga_edit();
    })

    //simpan edit detail paket makan
    $('#edfrmsimpanpaket').submit(function(e){
      e.preventDefault();
      var item={};
      item['nama_paket'] = $('#ednamaPaket').val();
      var id = $('#idedit').val();
      item['id'] = id;
      console.log(item);

      $('#editmenu').find('tr.kosong').remove();
      var datadetail = hitung_harga_edit();

      if (datadetail.length == 0) {
        //gak ada tambahan data
        $.ajax({
          type: "POST",
          data: item,
          url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/edit_paket_makan/" +id,
          success: function(data){
            console.log(data);
            $('#edPaket').modal('hide');
            myAlert('DATA BERHASIL DIUBAH');
            window.location.replace("<?php echo base_url()?>instalasigizi/homeinstalasigizi");
          },
          error: function(data){
            console.log(data);
          }
        })
      }else{
        item['data'] = datadetail;
        item['harga_total'] = $('#hrgPktIG').val().toString().replace(/[^\d\,\-\ ]/g, '');

        $.ajax({
          type: "POST",
          data: item,
          url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/edit_menu_paket/" + id,
          success: function(data){
            console.log(data);
            $('#edPaket').modal('hide');
            myAlert('DATA BERHASIL DIUBAH');
            window.location.replace("<?php echo base_url()?>instalasigizi/homeinstalasigizi");
          },
          error: function(data){
            console.log(data);
          }
        });
      }
    })

    //delete detail paket makan
    $('#editmenu').on('click', '.delete', function(e){
      e.preventDefault();

      var x = confirm("Yakin akan menghapus?");
      if(x) {
        var hargadelete = $(this).closest('tr').find('td').eq(3).text().toString().replace(/[^\d\,\-\ ]/g, '');

        var hargabaru = Number($('#hargasebelumedit').val()) - Number(hargadelete);
        $('#hrgPktIG').val(hargabaru.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        $('#hargasebelumedit').val(hargabaru);

        var id_paket = $('#idedit').val();
        var id_detail = $(this).closest('tr').find('td').eq(4).text();

        var trremove = $(this);

        $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/delete_detail/" + id_paket + "/" + id_detail + "/" + Number(hargabaru),
          success: function(data){
            trremove.closest('tr').remove();
            myAlert('DATA BERHASIL DIHAPUS');
          },
          error: function(data){
            console.log(data);
          }
        })
      }
      else {
        return false;
      }

    });

    //button batal edit paket makan
    $('#btnBatal').on('click', function(e){
      $('#edPaket').modal('hide');
      window.location.replace("<?php echo base_url()?>instalasigizi/homeinstalasigizi");
    })

    //view detail paket makan
    $('#tbodypaketmakan').on('click', 'tr td a.viewpaket', function(e){
      e.preventDefault();
      var id = $(this).closest('tr').find('td #idpaket').val();
      $('#namaPaket').val($(this).closest('tr').find('td').eq(1).text());
      $('#klsPaket').val($(this).closest('tr').find('td').eq(2).text());
      $('#tipeDiet').val($(this).closest('tr').find('td').eq(3).text());
      $('#hrgPkt').val($(this).closest('tr').find('td').eq(4).text());

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/getdetailpaket/" + id,
        success: function(data){
          console.log(data);
          if (data.length > 0) {
            $('#tbodydetailpaket').empty();
            for (var i = 0; i < data.length; i++) {
              $('#tbodydetailpaket').append(
                '<tr><td>'+(i+1)+'</td>'+
                  '<td>'+data[i]['menu']+'</td>'+
                  '<td>'+data[i]['jenis']+'</td>'+
                  '<td class="text-right">'+data[i]['harga'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                '</tr>'
              )
            };
          }
          else{
            $('#tbodydetailpaket').empty();
            $('#tbodydetailpaket').append('<tr><td colspan="4" style="text-align:center">Tidak ada detail</td></tr>');
          }
        },
        error: function(data){
          console.log(data);
        }
      });
    });

    //filter tanggal list permintaan makan
    $('#tgl_list').on('change', function(e){
      var tanggal ={};
      tanggal['tgl'] = $(this).val();
      //console.log('tgl: ' + tanggal);
      $.ajax({
        type: "POST",
        data: tanggal,
        url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/get_all_list_by_date/",
        success: function(data){
          console.log(data);
          $('#tbodypermintaan').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              var c = i+1;
              $('#tbodypermintaan').append(
                '<tr>'+
                  '<td>'+c+'</td>'+
                  '<td>'+data[i]['unit']+'</td>'+
                  '<td>'+data[i]['nama_kamar']+'</td>'+
                  '<td>'+data[i]['nama_bed']+'</td>'+
                  '<td>'+data[i]['nama']+'</td>'+
                  '<td class="paketval">'+data[i]['nama_paket']+'</td>'+
                  '<td>'+data[i]['tipe_diet']+'</td>'+
                  '<td>'+data[i]['keterangan']+'</td>'+
                  '<td class="statusval">'+data[i]['status']+'</td>'+
                  '<td style="text-align:center">'+
                    '<a href="#edList" class="editpermintaan" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
                    '<input type="hidden" class="idpermintaan_val" value="'+data[i]['id']+'">'+
                    '<input type="hidden" class="idpaket_val" value="'+data[i]['paket_makan']+'">'+
                    '<input type="hidden" class="idtipe_val" value="'+data[i]['id_tipe']+'">'+
                  '</td>'+
                '</tr>'
              )
            };
          }else {
            $('#tbodypermintaan').append('<tr><td colspan="10" text-align="center">Tidak ada data</td></tr>');
          }
        },
        error: function(data){
          console.log(data);
        }
      })

    })

    //autocomplete nama paket
    $('#ac_namapaket').focus(function(){
      var $input = $('#ac_namapaket');


      var tipediet = $('#tipediet_id').val();

console.log(tipediet);
      $.ajax({
        type: "POST",
        //url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/get_paket_by_diet/"+ tipediet,
        url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/get_all_paket",
        success: function(data){
          var autodata = [];
          var iddata = [];

          for (var i = 0; i < data.length; i++) {
            autodata.push(data[i]['nama_paket']);
            iddata.push(data[i]['id']);
          }

          $input.typeahead({
            autoSelect: true,
            source: autodata
          });

          $input.change(function(){
            var current = $input.typeahead("getActive");
            var index = autodata.indexOf(current);

            $('#paket_id').val(iddata[index]);

            if (current) {
                // Some item from your model is active!
                if (current.name == $input.val()) {
                    // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
                } else {
                    // This means it is only a partial match, you can either add a new item
                    // or take the active if you don't want new items
                }
            } else {
                // Nothing is active so it is a new value (or maybe empty value)
            }
          });

        },
        error: function(data){
          console.log(data);
        }
      });
    });

    var tdpermintaan = '';
    $('#tbodypermintaan').on('click', 'tr td a.editpermintaan', function(e){
      e.preventDefault();
      tdpermintaan = $(this);
      $('#idpermintaan').val($(this).closest('tr').find('td .idpermintaan_val').val());
      $('#ac_namapaket').val($(this).closest('tr').find('td.paketval').text());
      $('#paket_id').val($(this).closest('tr').find('td .idpaket_val').val());
      $('#tipediet_id').val($(this).closest('tr').find('td .idtipe_val').val());
    })

    //ubah permintaan makan
    $('#frmeditpermintaan').submit(function(e){
      e.preventDefault();
      var item = {};

      var id = $('#idpermintaan').val();
      item['paket_makan'] = $('#paket_id').val();
      item['status'] = $('#stsList').val();

      $.ajax({
        type: "POST",
        data: item,
        url: "<?php echo base_url()?>instalasigizi/homeinstalasigizi/update_permintaan/" + id,
        success: function(data){
          tdpermintaan.closest('tr').find('td.statusval').text($('#stsList').val());
          tdpermintaan.closest('tr').find('td.paketval').text($('#ac_namapaket').val());
          myAlert('DATA BERHASIL DIUBAH');
          $('#edList').modal('hide');
        },
        error: function(data){
          console.log(data);
        }
      })
    })

  });

  function hitung_harga(){
    var data = [];
    $('#addMenu').find('tr').each(function (rowIndex, r) {
        var cols = [];
        $(this).find('td').each(function (colIndex, c) {
            cols.push(c.textContent);
        });
        $(this).find('td input').each(function (colIndex, c) {
            cols.push(c.value);
        });
        $(this).find('td select option:selected').each(function(colIndex, c){
          cols.push(c.value);
        });
        data.push(cols);
    });
    console.log(data);
    var total = 0;

    for (var i = data.length - 1; i >= 0; i--) {
			total += Number(data[i][6]);
		};

    $('#hrgTotIG').val(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

    return data;
  }

  function hitung_harga_edit(){
    var data = [];
    $('#editmenu').find('tr.tambah').each(function (rowIndex, r) {
        var cols = [];
        $(this).find('td').each(function (colIndex, c) {
            cols.push(c.textContent);
        });
        $(this).find('td input').each(function (colIndex, c) {
            cols.push(c.value);
        });
        $(this).find('td select option:selected').each(function(colIndex, c){
          cols.push(c.value);
        });
        data.push(cols);
    });

    console.log(data);

    var total = Number($('#hargasebelumedit').val());

    console.log('total: ' + total);

    for (var i = data.length - 1; i >= 0; i--) {
			total += Number(data[i][7]);
		};
    console.log('total2: ' + total);
    //return false;
    console.log(data);
    $('#hrgPktIG').val(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

    return data;
  }

  function resetpaket(){
    $('#nmPaketIG').val('');
    $("#kelasIG option[value='']").attr("selected", "selected");
    $('#diet_id').val('');
    $('#tipedietval').val('');
    $('#addMenu').empty('');
    $('#addMenu').append('<tr class="kosong"><td colspan="4" style="text-align:center">Tambah detail paket makan</td></tr>');
    $('#hrgTotIG').val('0');
  }

</script>
