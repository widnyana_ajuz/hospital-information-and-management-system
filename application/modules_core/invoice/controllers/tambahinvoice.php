<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Tambahinvoice extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_tambahinvoice");
		$data['page_title'] = "Tambah Invoice";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load template
		$data['content'] = 'tambahinvoice';
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}

	public function get_visit_id(){
		$search = $_POST['search'];
		$dept = $_POST['department'];
		$result = $this->m_tambahinvoice->get_visit_id($search, $dept);

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function get_new_invoice($visit_id, $dept){
		$id = $this->m_tambahinvoice->get_new_invoice($visit_id);

		if($id){
			$vir = intval($id['value']) + 1;
			if (strlen($vir) == "1") {
				$vir = '0'. $vir;
			}
			$insert['invoice'] = $dept.$visit_id."".($vir);
		}else{
			$insert['invoice'] = $dept.$visit_id."01";
		}

		header('Content-Type: application/json');
		echo json_encode($insert);
	}

	public function save_invoice(){
		foreach($_POST as $value){
			$insert = $value;
		}

		$tgl = $this->date_db($insert['tanggal_invoice']);
		$insert['tanggal_invoice'] = $tgl;

		$save = $this->m_tambahinvoice->save_invoice($insert);

		//membuat tagihan admisi
		$lama_baru = $this->m_tambahinvoice->get_statuspasien($insert['visit_id']);
		$tindakan_admisi = $this->m_tambahinvoice->get_tindakan_id($lama_baru);

		$tagihan['tindakan_id'] = $tindakan_admisi['detail_id'];
		$tagihan['js'] = $tindakan_admisi['js'];
		$tagihan['jp'] = $tindakan_admisi['jp'];
		$tagihan['bakhp'] = $tindakan_admisi['bakhp'];
		$tagihan['no_invoice'] = $insert['no_invoice'];
		$tagihan['petugas_input'] = $this->user['user_name'];
		$tagihan['waktu'] = date('Y-m-d H:i:s');
		$tagihan['tarif'] = Intval($tagihan['js'])+Intval($tagihan['jp'])+Intval($tagihan['bakhp']);

		$save = $this->m_tambahinvoice->save_tagihanadmisi($tagihan);

		header('Content-Type: application/json');
		echo json_encode($tagihan);

	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}
}
