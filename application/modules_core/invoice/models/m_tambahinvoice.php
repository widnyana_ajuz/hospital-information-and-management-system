<?php

class m_tambahinvoice extends CI_Model {
	public function get_visit_id($search, $dept){
		$sql = "SELECT v.visit_id, vj.rj_id as sub_visit, p.nama, m.nama_dept, v.tanggal_visit, p.alamat_skr, p.rm_id, m.jenis, vj.unit_tujuan FROM visit v, pasien p, master_dept m , visit_rj vj  WHERE vj.rj_id NOT IN (SELECT sub_visit FROM tagihan) AND v.visit_id = vj.visit_id AND v.rm_id = p.rm_id AND vj.unit_tujuan = m.dept_id AND vj.is_bayar = 0 AND v.status_visit <> 'DONE' AND (p.nama LIKE '%$search%' OR v.visit_id LIKE '%$search%') AND vj.waktu_keluar IS NOT NULL AND vj.unit_tujuan = $dept
			UNION ALL 
			SELECT v.visit_id, vi.ri_id as sub_visit, p.nama, m.nama_dept, v.tanggal_visit, p.alamat_skr, p.rm_id, m.jenis, vi.unit_tujuan FROM visit v, pasien p, master_dept m , visit_ri vi, visit_inap_kamar vik WHERE vik.ri_id = vi.ri_id AND vi.ri_id NOT IN (SELECT sub_visit FROM tagihan) AND v.visit_id = vi.visit_id AND v.rm_id = p.rm_id AND vi.unit_tujuan = m.dept_id  AND vi.is_bayar = 0 AND v.status_visit <> 'DONE' AND (p.nama LIKE '%$search%' OR v.visit_id LIKE '%$search%') AND vik.waktu_keluar IS NOT NULL AND vi.unit_tujuan = $dept
			UNION ALL
			SELECT v.visit_id, vd.igd_id as sub_visit, p.nama, m.nama_dept, v.tanggal_visit, p.alamat_skr, p.rm_id, m.jenis, vd.unit_tujuan FROM visit v, pasien p, master_dept m , visit_igd vd WHERE vd.igd_id NOT IN (SELECT sub_visit FROM tagihan) AND v.visit_id = vd.visit_id AND v.rm_id = p.rm_id AND vd.unit_tujuan = m.dept_id  AND v.status_visit <> 'DONE' AND (p.nama LIKE '%$search%' OR v.visit_id LIKE '%$search%') AND vd.unit_tujuan = $dept
			UNION ALL
			SELECT v.visit_id, vd.penunjang_id as sub_visit, p.nama, m.nama_dept, v.tanggal_visit, p.alamat_skr, p.rm_id, m.jenis, vd.dept_tujuan as unit_tujuan FROM visit v, pasien p, master_dept m , visit_penunjang vd WHERE vd.penunjang_id NOT IN (SELECT sub_visit FROM tagihan) AND v.visit_id = vd.visit_id AND v.rm_id = p.rm_id AND vd.dept_tujuan = m.dept_id AND v.status_visit <> 'DONE' AND (p.nama LIKE '%$search%' OR v.visit_id LIKE '%$search%') AND vd.pengirim = 'APS' AND vd.dept_tujuan = $dept
			-- UNION ALL
			-- SELECT v.visit_id, l.operasi_id as sub_visit, p.nama, m.nama_dept, v.tanggal_visit, p.alamat_skr, p.rm_id, m.jenis, vd.dept_tujuan as unit_tujuan FROM visit v, pasien p, master_dept m, order_operasi vd, operasi_rencana r,  operasi_laporan l WHERE  l.operasi_id NOT IN (SELECT sub_visit FROM tagihan) AND v.visit_id = vd.visit_id AND v.rm_id = p.rm_id AND vd.dept_tujuan = m.dept_id AND l.rencana_id = r.rencana_id AND r.order_operasi_id = vd.order_operasi_id  AND l.is_bayar = 0 AND v.status_visit <> 'DONE' AND (p.nama LIKE '%$search%' OR v.visit_id LIKE '%$search%')
		";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function get_new_invoice($id){
		$sql = "SELECT max(substr(no_invoice, 13, 2)) as value from tagihan WHERE visit_id = '$id' ";

    	$query = $this->db->query($sql);
    	if ($query) {
    		return $query->row_array();
    	}else{
    		return false;
	    }
	}

	public function save_invoice($value='')
    {
    	$query = $this->db->insert('tagihan',$value);
    	if ($query) {
    		return true;
    	}else{
    		return false;
    	}
    }

    public function save_tagihanadmisi($value='')
    {
    	$query = $this->db->insert('tagihan_admisi',$value);
    	if ($query) {
    		return true;
    	}else{
    		return false;
    	}
    }

    public function get_statuspasien($visit){
    	$sql = "SELECT is_pasien_lama from visit WHERE visit_id = '$visit' ";
    	$query = $this->db->query($sql);
    	$result = $query->row_array();
    	if($result['is_pasien_lama']==1)
    		return "Pasien Lama";
    	return "Pasien Baru";
    }

    public function get_tindakan_id($nama){
    	$sql = "SELECT detail_id, js, jp, bakhp from master_tindakan_detail mtd, master_tindakan mt WHERE mt.tindakan_id = mtd.tindakan_id AND mt.nama_tindakan = '$nama' AND mtd.kelas = 'Kelas VIP' ";
    	$query = $this->db->query($sql);
    	return $query->row_array();
    }
}
?>