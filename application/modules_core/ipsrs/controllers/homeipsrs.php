<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homeipsrs extends Operator_base {
	protected $dept_id;
	function __construct(){

		parent:: __construct();
		$this->load->model('m_ipsrs');	
		$this->load->model('bersalin/m_homebersalin');
		$this->load->model('bersalin/m_bersalin');
		$this->load->model('logistik/m_gudangbarang');	
		$this->dept_id = $this->m_ipsrs->get_dept_id('IPSRS')['dept_id'];

	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['page_title'] = 'IPS-RS';
		$this->session->set_userdata($data);
		$data['content'] = 'ipsrs';
		$data['javascript'] = 'j_ipsrs';
		$data['riwayat_ipsrs'] =  $this->m_ipsrs->get_riwayat_kegiatan_ipsrs();
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($this->dept_id);
		$this->load->view('base/operator/template', $data);
	}

	public function get_all_dept()
	{
		$result = $this->m_ipsrs->get_all_dept();

		header('Content-Type: application/json');
		echo json_encode($result);
	}
	/*kegiatan*/
	public function get_barang_unit()
	{
		$katakunci = $_POST['katakunci'];
		$result = $this->m_ipsrs->get_barang_unit($katakunci, $this->dept_id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function submit_kegiatan_unit()
	{
		$insert['unit_pengguna'] = $_POST['unit_pengguna'];
		$insert['tanggal'] = $_POST['tanggal'];
		$insert['keterangan'] = $_POST['keterangan'];
		$insert['petugas_input'] = $this->session->userdata('session_operator')['petugas_id'];

		$item = $_POST['data'];

		$id = $this->m_ipsrs->insert_kegiatan_ipsrs($insert);
		foreach ($item as $key) {
			$params = array(
				'ipsrs_id' => $id, 
				'barang_detail_id' => $key[4],
				'stok_unit' => $key[1],
				'jumlah' => $key[6]
				);

			$this->m_ipsrs->insert_detail_kegiatan($params);
		}
		$unit = $this->m_ipsrs->get_nama_unit($insert['unit_pengguna'])['nama_dept'];
		$ptgs = $this->m_ipsrs->get_nama_petugas($insert['petugas_input'])['nama_petugas'];
		$result = array(
			'tanggal' => $insert['tanggal'], 
			'unit' => $unit,
			'petugas' => $ptgs,
			'keterangan' => $insert['keterangan'],
			'id' => $id
			);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_detail_kegiatan($id='')
	{
		$result = $this->m_ipsrs->get_detail_kegiatan($id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}
	/*akhir kegiatan*/

	/*logistik*/
	public function input_in_outbarang($value='')
	{
		$insert['barang_detail_id'] = $_POST['barang_detail_id'];
		$tgl = DateTime::createFromFormat('d/m/Y H:i', $_POST['tanggal']);
		$insert['tanggal'] = $tgl->format('Y-m-d H:i');
		$insert['is_out'] = $_POST['is_out'];
		$insert['jumlah'] = $_POST['jumlah'];
		$insert['keterangan'] = $_POST['keterangan'];
		$insert['barang_dept_id'] = $this->dept_id;

		$res = $this->m_gudangbarang->input_in_out($insert);
		if ($res) {
			$ins['barang_detail_id'] = $_POST['barang_detail_id'];
			$ins['dept_id'] = $this->dept_id;
			$ins['stok'] = $_POST['sisa'];
			$ins['tanggal_stok'] = date('Y-m-d H:i:s');
			$ins['keterangan_stok'] = "IN - OUT";

			$res = $this->m_gudangbarang->input_riwayat_out($ins);
			if ($res) {
				$message = "true";
			}else{
				$message = "false";
			}
		}else{
			$message = "false";
		}

		header('Content-Type: application/json');
	 	echo(json_encode($message));
	}

	public function get_detail_inventori($id)
	{
		$res = $this->m_gudangbarang->get_detail_inventori($id, $this->dept_id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function get_barang_gudang()
	{
		$katakunci = $_POST['katakunci'];
		$elny2 = $this->m_homebersalin->get_barang_gudang($katakunci,$this->dept_id,'24');
		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_permintaan_barangunit($value='')
	{
		$this->form_validation->set_rules('no_permintaanbarang', 'nomor permitaan', 'required|trim|xss_clean|is_unique[barang_permintaan.no_permintaanbarang]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaanbarang'] = $_POST['no_permintaanbarang'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$insert['dept_id'] = $this->dept_id;

			$val = $_POST['data'];
			$result = $this->m_homebersalin->insert_permintaanbarang($insert);
			if($result){
				foreach ($val as $key) {
					$ins['barang_id'] = $key[8];
					$ins['barang_stok_id'] = $key[7];
					$ins['jumlah_request'] =  $key[9];
					$ins['barang_permintaan_id'] = $result;

					$elny = $this->m_homebersalin->insert_detail_permintaanbarang($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}
	/*akhir logistik*/

	
	public function laporan_ipsrs()
	{
		$tanggal = $this->input->post('start');
		$format = DateTime::createFromFormat('d/m/Y', $tanggal);
		$tgl = $format->format('Y-m-d');

		$data['result'] = $this->m_ipsrs->excel_ipsrs($tgl);
		$data['tanggal'] = $tanggal;
		
		$this->load->view('ipsrs/ipsrs_laporan', $data);
	}
}
