<?php  
	/**
	* 
	*/
	class m_ipsrs extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function get_dept_id($nama)
		{
			$sql = "SELECT dept_id from master_dept where nama_dept LIKE '$nama'";
			$res = $this->db->query($sql);
			if ($res) {
				return $res->row_array();
			}else{
				return false;
			}
		}

		public function get_barang_unit($katakunci, $dept_id)
		{
			$sql = "SELECT * from barang a left join barang_detail b on a.barang_id = b.barang_id
					left join (select * from barang_stok order by barang_stok_id desc) c 
					on b.barang_detail_id = c.barang_detail_id
					left join barang_merk d on d.merk_id =  a.merk left join obat_satuan e
					on e.satuan_id = a.satuan_id 
					left join barang_penyedia bo on bo.penyedia_id = a.penyedia_id
					where c.dept_id = '$dept_id' and a.nama LIKE '%$katakunci%' group by b.barang_detail_id";
			$res = $this->db->query($sql);
			if ($res->num_rows() > 0) {
				return $res->result_array();
			}else{
				return array();
			}
		}

		public function get_riwayat_kegiatan_ipsrs()
		{
			$sql = "SELECT *, a.keterangan as ket 
					from ipsrs_kegiatan a 
					left join master_dept md on md.dept_id = a.unit_pengguna 
					left join petugas p on p.petugas_id = a.petugas_input";
			$res = $this->db->query($sql);
			if ($res->num_rows() > 0) {
				return $res->result_array();
			}else{
				return array();
			}
		}

		public function get_all_dept()
		{
			$sql = "SELECT * from master_dept";
			$res = $this->db->query($sql);
			if ($res->num_rows() > 0) {
				return $res->result_array();
			}else{
				return array();
			}
		}

		public function insert_kegiatan_ipsrs($insert)
		{
			$this->db->insert('ipsrs_kegiatan', $insert);
			return $this->db->insert_id();
		}

		public function insert_detail_kegiatan($insert)
		{
			$this->db->insert('ipsrs_kegiatan_detail', $insert);
			return $this->db->insert_id();
		}

		public function get_nama_unit($id)
		{
			$sql = "SELECT nama_dept from master_dept where dept_id = '$id'";
			$res = $this->db->query($sql);
			if ($res->num_rows() > 0) {
				return $res->row_array();
			}else{
				return false;
			}
		}

		public function get_nama_petugas($id)
		{
			$sql = "SELECT nama_petugas from petugas where petugas_id = '$id'";
			$res = $this->db->query($sql);
			if ($res->num_rows() > 0) {
				return $res->row_array();
			}else{
				return false;
			}
		}

		public function get_detail_kegiatan($id='')
		{
			$sql = "SELECT d.nama, os.satuan, b.*
					from ipsrs_kegiatan_detail b left join barang_detail c on c.barang_detail_id = b.barang_detail_id
					left join barang d on d.barang_id = c.barang_id 
					left join obat_satuan os on os.satuan_id = d.satuan_id
					where b.ipsrs_id = '$id'";
			$res = $this->db->query($sql);
			if ($res->num_rows() > 0) {
				return $res->result_array();
			}else{
				return array();
			}
		}

		public function excel_ipsrs($tgl)
		{
			$sql = "SELECT d.nama, os.satuan, b.*, p.nama_petugas, md.nama_dept
					from ipsrs_kegiatan a left join ipsrs_kegiatan_detail b on a.ipsrs_id = b.ipsrs_id
					left join barang_detail c on c.barang_detail_id = b.barang_detail_id
					left join barang d on d.barang_id = c.barang_id 
					left join obat_satuan os on os.satuan_id = d.satuan_id
					left join master_dept md on md.dept_id = a.unit_pengguna
					left join petugas p on p.petugas_id = a.petugas_input
					where a.tanggal = '$tgl'";
			$res = $this->db->query($sql);
			if ($res->num_rows() > 0) {
				return $res->result_array();
			}else{
				return array();
			}
		}

	}
?>