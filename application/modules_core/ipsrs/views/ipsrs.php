<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>ipsrs/homeipsrs">IPS-RS</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Kegiatan</a>
	</li>
</div>

<div class="navigation" style="margin-left: 10px" >
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	    <li class="active"><a href="#kegiatan" class="cl" data-toggle="tab">Kegiatan</a></li>
	    <li><a href="#logistik" class="cl" data-toggle="tab">Logistik</a></li>
	   	<li><a href="#laporan" class="cl" data-toggle="tab">Laporan</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
		<div class="tab-pane active" id="kegiatan">
			<div class="dropdown" id="btnBawah" style="margin-left:10px;width:98.5%">
				<div id="titleInformasi">Catatan Kegiatan</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			</div>
			<div class="tabelinformasi" id="info1">
				<form class="form-horizontal" role="form" style="margin-top:25px;" id="submit_kegiatan">
					<div class="informasi">
						<div class="form-group" >
							<label class="control-label col-md-3">Tanggal</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="i_tgl" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
								</div>
							</div>
						</div>

						<div class="form-group">
		            		<label class="control-label col-md-3">Unit </label>
		            		<div class="col-md-2">
								<input type="text" class="form-control" autocomplete="off" spellcheck="false" id="i_unit" name="unit" placeholder="Unit" />
								<input type="hidden" id="i_id_unit">
							</div>
						</div>

						<div class="form-group">
		            		<label class="control-label col-md-3">Uraian </label>
		            		<div class="col-md-4">
								<textarea class="form-control" id="i_uraian" placeholder="Uraian"></textarea>
							</div>
						</div>

					</div>
					<br>
					<hr class="garis" style="margin-left:10px;">
					
					<div class="tabelinformasi">
						<a href="#modaltmbpenglog" data-toggle="modal" style="margin-left:20px;"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tamah Penggunaan Logistik">&nbsp;Tambah Penggunaan Logistik</i></a>
						<div class="clearfix"></div>

			        	<div class="portlet-body" style="margin: 0px 20px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" >
								<thead>
									<tr class="info">
										<th>Nama Barang</th>
										<th>Stok Unit</th>
										<th>Qty Pemakaian</th>
										<th>Satuan</th>
										<th width="20">Action</th>
									</tr>
								</thead>
								<tbody id="tbody_ipsrs">
									
								</tbody>
							</table>
						</div>

						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span style="padding:0px 10px 0px 10px;">
								<button class="btn btn-warning" type="submit">RESET</button>&nbsp;
								<button class="btn btn-success" type="submit">SIMPAN</button>
							</span>
						</div>
						<br>
					</div>
				</form>
			</div>

			<div class="modal fade" id="modaltmbpenglog" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Tambah Penggunaan Logistik</h3>
	        			</div>
	        			<div class="modal-body">
		        			<form method="post" id="search_barang_ipsrs">
			        			<div class="form-group">	
									<div class="col-md-5">
										<input type="text" class="form-control" name="katakunci" id="katakunci_ipsrs" placeholder="Pilih Barang"/>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-info">Cari</button>
									</div>	
								</div>
							</form>	
							<br>
							<div style="margin-top:30px"><hr class="garis"></div>		
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="barang_IPSRS" style="font-size:99.5%">
									<thead>
										<tr class="info">
											<th>Nama Barang</th>
											<th>Stok Unit</th>
											<th>Satuan</th>
											<th width="10%">Pilih</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="4"><center>cari barang</center></td>
										</tr>
									</tbody>
								</table>												
							</div>
						</div>
	        			
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>	
	        <br>

	        <div class="dropdown" id="btnBawahRetDistributor" style="margin-left:10px;width:98.5%">
	            <div id="titleInformasi">Riwayat Kegiatan</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div> 
            <div class="tabelinformasi" id="infoRetDistributor">
            	
          
				<div class="portlet box red">
					<div class="portlet-body" style="margin: 25px 10px 0px 10px">
						<input type="hidden" id="jml_riwayat_ipsrs" value="<?php echo count($riwayat_ipsrs) ?>">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="table_riwayat_ipsrs">
							<thead>
								<tr class="info" >
									<th style="width:30px"> No.</th>
									<th width="200"> Tanggal </th>
									<th> Unit </th>
									<th> Petugas </th>
									<th> Keterangan </th>
									<th width="100"> Action </th>
								</tr>
							</thead>
							<tbody>
								<?php  
									if (isset($riwayat_ipsrs)) {
										$i = 0;
										foreach ($riwayat_ipsrs as $key) {
											$tgl = DateTime::createFromFormat('Y-m-d', $key['tanggal']);
											echo '<tr>	
													<td>'.(++$i).'</td>
													<td>'.$tgl->format('d F Y').'</td>
													<td>'.$key['nama_dept'].'</td>
													<td>'.$key['nama_petugas'].'</td>
													<td>'.$key['ket'].'</td>
													<td style="text-align:center">
														<input type="hidden" class="ipsrs_id" value="'.$key['ipsrs_id'].'">
														<a href="#" class="viewdetriwayat" data-toggle="modal" data-target="#detriwayat"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Detail">
														</i></a>	
													</td>						
												</tr>';
										}
									}
								?>
							</tbody>
						</table>
					</div>					
				</div>

				<div class="modal fade" id="detriwayat" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Kegiatan</h3>
		        			</div>
		        			<div class="modal-body">
			        			<form class="form-horizontal" role="form" style="margin-left:30px;">
									<div class="form-group">
			        					<label class="control-label col-md-3" >Tanggal 
										</label>
										<div class="col-md-4" >
						         			<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input type="text" id="riw_tgl" style="cursor:pointer;" class="form-control calder" readonly>
											</div>
										</div>
									</div>
									<div class="form-group">
			        					<label class="control-label col-md-3" >Unit
										</label>
										<div class="col-md-4" >
						         			<input type="text" id="riw_unit" readonly class="form-control" placeholder="Unit">
										</div>
									</div>

									<div class="form-group">
			        					<label class="control-label col-md-3" >Petugas
										</label>
										<div class="col-md-4" >
						         			<input type="text" id="riw_ptgs" readonly class="form-control" placeholder="Petugas">
										</div>
									</div>

									<!-- <div class="form-group">
										<label class="control-label col-md-3" >Status
										</label>
										<div class="col-md-4" >
						         			<select class="form-control" disabled name="Status" id="satus" >
												<option value="Selesai" selected>Selesai </option>
												<option value="Belum Selesai">Belum Selesai </option>
											</select>
										</div>
									</div> -->

									<div class="form-group">
										<label class="control-label col-md-3" >Uraian
										</label>
										<div class="col-md-5">
											<textarea readonly id="riw_ket" class="form-control" placeholcer="Uraian Tindakan"></textarea>
										</div>
									</div>

									<br>

									<div class="portlet-body" style="margin: 0px 10px 0px -15px">
										<table class="table table-striped table-bordered table-hover table-responsive" >
											<thead>
												<tr class="info">
													<th style="text-align:center; width:20px;">No. </th>
													<th>Nama Barang</th>
													<th>Stok Unit</th>
													<th>Qty Pemakaian</th>
													<th>Satuan</th>
												</tr>
											</thead>
											<tbody id="tbl_riw">
												<tr>
													<td>1</td>
													<td>Suntikan</td>
													<td style="text-align:right">12</td>
													<td style="text-align:right">10</td>
													<td>Buah</td>
												</tr>
											</tbody>
										</table>
									</div>
								</form>
							</div>

		        			<div class="modal-footer">
		 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					      	</div>
						</div>
					</div>
				</div>

				<div class="modal fade" id="editriwayat" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Kegiatan</h3>
		        			</div>
		        			<div class="modal-body">
			        			<form class="form-horizontal" role="form" style="margin-left:30px;">
									<div class="form-group">
			        					<label class="control-label col-md-3" >Tanggal 
										</label>
										<div class="col-md-4" >
						         			<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input type="text" style="cursor:pointer;background-color:white" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
											</div>
										</div>
									</div>
									<div class="form-group">
			        					<label class="control-label col-md-3" >Unit
										</label>
										<div class="col-md-4" >
						         			<input type="text" class="form-control" placeholder="Unit">
										</div>
									</div>

									<div class="form-group">
			        					<label class="control-label col-md-3" >Petugas
										</label>
										<div class="col-md-4" >
						         			<input type="text" class="form-control" placeholder="Petugas">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3" >Status
										</label>
										<div class="col-md-4" >
						         			<select class="form-control" name="Status" id="satus" >
												<option value="Selesai" selected>Selesai </option>
												<option value="Belum Selesai">Belum Selesai </option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3" >Uraian
										</label>
										<div class="col-md-5">
											<textarea class="form-control" placeholcer="Uraian Tindakan"></textarea>
										</div>
									</div>

									<br>

									<div class="portlet-body" style="margin: 0px 10px 0px -15px">
										<table class="table table-striped table-bordered table-hover table-responsive" >
											<thead>
												<tr class="info">
													<th style="text-align:center; width:20px;">No. </th>
													<th>Nama Barang</th>
													<th>Stok Unit</th>
													<th width="150">Qty Pemakaian</th>
													<th>Satuan</th>
												</tr>
											</thead>
											<tbody id="tblby">
												<tr>
													<td>1</td>
													<td>Suntikan</td>
													<td style="text-align:right">12</td>
													<td style="text-align:right"><input type="text" class="form-control" name="qty"></td>
													<td>Buah</td>
												</tr>
											</tbody>
										</table>
									</div>
								</form>
							</div>

		        			<div class="modal-footer">
		 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
		 			       		<button type="button" class="btn btn-success" data-dismiss="modal">Simpan</button>
					      	</div>
						</div>
					</div>
				</div>
            </div>
            <br>
		</div>

		<div class="tab-pane" id="logistik">
        	<div class="modal fade" id="modalbarang" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:800px">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
	        			</div>
	        			<div class="modal-body">

		        			<div class="form-group">
		        				<form method="post" class="form-horizontal" role="form" id="formmintabarang">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncimintabarang" placeholder="Nama barang"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
								</form>
								<div style="margin-right:10px;margin-left:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%">
										<thead>
											<tr class="info">
												<th>Nama Barang</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Tahun Pengadaan</th>
												<th>Stok Gudang</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodybarangpermintaan">
											<tr>
												<td colspan="6" style="text-align:center">Cari data Barang</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
	       	<div class="dropdown" id="btnBawahInventoriBarang">
	            <div id="titleInformasi">Inventori</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoInventoriBarang">
				<div class="form-group" >
					<div class="portlet-body" style="margin: 30px 10px 20px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tblinventorigudangunit">
							<thead>
								<tr class="info" >
									<th width="20">No.</th>
									<th > Nama Barang </th>
									<th > Merek </th>
									<th > Harga </th>
									<th > Stok </th>
									<th > Satuan </th>
									<th > Tahun Pengadaan</th>
									<th > Sumber Dana</th>
									<th width="100"> Action </th>
								</tr>
							</thead>
							<tbody id="tbodyinventoribarang">
								<?php 
									if (isset($inventoribarang)) {
										if (!empty($inventoribarang)) {
											$i = 1;
											foreach ($inventoribarang as $value) {
												echo '<tr>
														<td align="center">'.($i++).'</td>
														<td>'.$value['nama'].'</td>
														<td>'.$value['nama_merk'].'</td>
														<td align="right">'.$value['harga'].'</td>
														<td align="right">'.$value['stok'].'</td>
														<td>'.$value['satuan'].'</td>
														<td align="center">'.$value['tahun_pengadaan'].'</td>
														<td>'.$value['sumber_dana'].'</td>
														<td style="text-align:center">
															<input type="hidden" class="barang_detail_inout" value="'.$value['barang_detail_id'].'">
															<a href="#inoutbar" data-toggle="modal" class="edBarang" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
															<a href="#edInvenBerBar" data-toggle="modal" class="detailinvenbarang"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
														</td>
													</tr>';
											}
										}
									}
								?>
									
							</tbody>
						</table>
					</div>
					<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
					<br>
	        	</div>
	        </div>
			<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<form class="form-horizontal" role="form" style="margin-left:30px;" id="forminoutbarang">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<div class="form-group">
			        				<label class="control-label col-md-3" >Tanggal </label>
									<div class="col-md-6" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" id="tanggalinout" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
									</div>
								</div>
										
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" >In / Out </label>
									<div class="col-md-6">
						         		<select class="form-control select" name="io" id="io">
											<option value="IN" selected>IN</option>
											<option value="OUT">OUT</option>					
										</select>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Jumlah in/out</label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="jmlInOut" name="jmlInOut" placeholder="Jumlah">
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Sisa Stok </label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="sisaInOut" name="sisaInOut" placeholder="Sisa Stok" readonly>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Keterangan </label>
									<div class="col-md-6" >
										<textarea class="form-control" id="keteranganIO" placeholder="Keterangan"></textarea>
									</div>
								</div>										
		        			</div>
		        			<div class="modal-footer">
		        				<input type="hidden" id="id_barang_inoutprocess">
		 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			       		<button type="submit" class="btn btn-success">Simpan</button>
					      	</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<form class="form-horizontal" role="form">
				            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th> Waktu </th>
											<th> IN / OUT </th>
											<th> Jumlah </th>
											<th> Keterangan </th>
										</tr>
									</thead>
									<tbody id="tbodydetailbrginventori">
										<tr>
											<td colspan="4" style="text-align:center">Tidak ada detail in-out</td>
										</tr>
											
									</tbody>
								</table>
							</form>
							
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
			<br>

			<div class="dropdown" id="btnBawahPermintaanBarang" style="margin-left:10px;width:98.5%">
	            <div id="titleInformasi">Permintaan Logistik</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoPermintaanBarang">
            	<form class="form-horizontal" role="form" method="post" id="permintaanbarangunit">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="nomorpermintaanbarang" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanbarang" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="keteranganpermintaanbarang" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>
					<a href="#modalbarang" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Barang</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<th> Nama Barang </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Tahun Pengadaan </th>
										<th> Stok Gudang </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputmintabarang">
									<?php echo '<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>'; ?>
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning" type="reset" id="batalpermintaanfarmasi">RESET</button>&nbsp;
								<button class="btn btn-success" type="submit">SIMPAN</button>
							</span>
						</div>
						<br>
					</div>	
				</form>
			</div>	    
			<br>
	    </div>

		<div class="tab-pane" id="laporan" style="margin-left:40px">
			 <div id="sensusharian" style="width:100%">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan IPSRS</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
        			method="post" action="<?php echo base_url() ?>ipsrs/homeipsrs/laporan_ipsrs">
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="input-group col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" class="form-control isian" name="start" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right" style="margin-right:30px">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div> 
	        <br>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready( function(){
		$("#infoPermintaanBarang").hide();
		$("#btnBawahPermintaanBarang").click(function(){
			$("#infoPermintaanBarang").slideToggle();
		});

	});

</script>