<script type="text/javascript">
	$(document).ready(function () {
		$('#search_barang_ipsrs').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['katakunci'] = $('#katakunci_ipsrs').val();

			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>ipsrs/homeipsrs/get_barang_unit',
				success: function (data) {
					console.log(data);
					if (data.length > 0) {
						$('#barang_IPSRS tbody').empty();
						for (var i = 0; i < data.length; i++) {
							$('#barang_IPSRS tbody').append(
								'<tr>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['stok']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td style="text-align:center">'+
										'<input type="hidden" class="stok_id" value="'+data[i]['barang_detail_id']+'">'+
										'<a href="#" class ="addNewips"><i class="glyphicon glyphicon-check"></i></a></td>'+
								'</tr>'
								)
						};
					};
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$(document).on('click', 'a.addNewips', function (e) {
			e.preventDefault();
			var nama = $(this).closest('tr').find('td').eq(0).text();
			var stok = $(this).closest('tr').find('td').eq(1).text();
			var satuan = $(this).closest('tr').find('td').eq(2).text();
			var id = $(this).closest('tr').find('td .stok_id').val();

			$('#tbody_ipsrs').append(
					'<tr>'+
						'<td>'+nama+'</td>'+
						'<td align="right">'+stok+'</td>'+
						'<td align="center" width="135"><input type="number" class="form-control" style="width:130px;" placeholder="0"></td>'+
						'<td>'+satuan+'</td>'+
						'<td style="display:none">'+id+'</td>'+
						'<td><center><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></center></td>'+
					'</tr>'
				)
		})

		//submit
		$('#i_unit').focus(function(){
			var $input = $('#i_unit');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>ipsrs/homeipsrs/get_all_dept',//sama di igd :D
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_dept']);
						iddata.push(data[i]['dept_id']);
					}
					console.log(data);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#i_id_unit').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		$('#submit_kegiatan').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['unit_pengguna'] = $('#i_id_unit').val();
			item['tanggal']= format_date3($('#i_tgl').val());
			item['keterangan'] = $('#i_uraian').val();
			if ($('#i_id_unit').val() == '') {myAlert('Pilih Unit'); $('#i_unit').focus();return false;};
			if ($('#tbody_ipsrs tr').length == 0) {myAlert('isi detail');return false;};

			var data = [];
		    $('#tbody_ipsrs').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
		    item['data'] = data;
		    console.log(item);//return false;
		    $.ajax({
		    	type: "POST",
		    	data: item,
		    	url: "<?php echo base_url()?>ipsrs/homeipsrs/submit_kegiatan_unit",
		    	success: function (data) {
		    		console.log(data);
		    		var t = $('#table_riwayat_ipsrs').DataTable();
		    		var no = $('#jml_riwayat_ipsrs').val();
		    		var last = '<center><input type="hidden" class="ipsrs_id" value="'+data['id']+'">'+
								'<a href="#" class="viewdetriwayat" data-toggle="modal" data-target="#detriwayat"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Detail">'+
								'</i></a></center>'
		    		t.row.add([
		    			Number(++no),
		    			format_date(data['tanggal']),
		    			data['unit'],
		    			data['petugas'],
		    			data['keterangan'],
		    			last,
		    			''
		    			]).draw();
		    		myAlert('data berhasil disimpan');

		    		$('#jml_riwayat_ipsrs').val(no);

		    		$(document).on('click', '.viewdetriwayat', function (e) {
						e.preventDefault();
						var id = $(this).closest('tr').find('td .ipsrs_id').val();
						viewdetriwayat($(this),id);
					})
		    	},
		    	error: function (data) {
		    		console.log(data);
		    	}
		    })
		})

		$(document).on('click', '.viewdetriwayat', function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .ipsrs_id').val();
			viewdetriwayat($(this),id);
		})
		

		/*inventori*/
		var this_io;
		$('#tbodyinventoribarang').on('click', 'tr td a.edBarang', function (e) {
			e.preventDefault();
			$('#id_barang_inoutprocess').val($(this).closest('tr').find('td .barang_detail_inout').val());
			var jlh = $(this).closest('tr').find('td').eq(4).text();
			this_io = $(this);
			$('#sisaInOut').val(jlh);

			$('#jmlInOut').on('change', function (e) {
				e.preventDefault();

				var is_in = $('#io').find('option:selected').val();
				var jmlInOut = $('#jmlInOut').val();
				var sisa = jlh;//$('#sisaInOut').val();
				var hasil ="";
				if (is_in == 'IN') {
					hasil = Number(jmlInOut) + Number(sisa);
				}else{			
					hasil = Number(sisa) - Number(jmlInOut);
				}

				if (jmlInOut == '') {
					hasil = Number(sisa);
				}
				$('#sisaInOut').val(hasil);			
			})

			$('#io').on('change', function () {
				var jumlah = Number($('#jmlInOut').val());
				var sisa = Number(jlh);//Number($('#sisaInOut').val());

				var isout = $('#io').find('option:selected').val();
				if (isout === 'IN') {
					$('#sisaInOut').val(jumlah + sisa);
				} else{
					$('#sisaInOut').val(sisa - jumlah);
				};
			})
		})
	
		$('#forminoutbarang').submit(function (e) {
			e.preventDefault();

			var item = {};
			item['barang_detail_id'] = $('#id_barang_inoutprocess').val();
			item['jumlah'] = $('#jmlInOut').val();
			item['sisa'] = $('#sisaInOut').val();
			item['is_out'] = $('#io').find('option:selected').val();
		    item['tanggal'] = $('#tanggalinout').val();
		    item['keterangan'] = $('#keteranganIO').val();
		    //console.log(item);return false;
		    if (item['jumlah'] != "") {
			    $.ajax({
			    	type: "POST",
			    	data: item,
			    	url: "<?php echo base_url()?>ipsrs/homeipsrs/input_in_outbarang",
			    	success: function (data) {
			    		if (data == "true") {
			    			myAlert('data berhasil disimpan');
			    			$('#keteranganIO').val('');
			    			$('#jmlInOut').val('');
			    			this_io.closest('tr').find('td').eq(4).text(item['sisa']);
			    			$('#inoutbar').modal('hide');	
			    		} else{
			    			myAlert('gagal, terdapat kesalahan');
			    		};
			    	},
			    	error: function (data) {
			    		myAlert('gagal');
			    	}
			    })
			} else{
				myAlert('isi data dengan benar');
				$('#jmlInOut').focus();
			};			
		})

		$("#tbodyinventoribarang").on('click', 'tr td a.detailinvenbarang', function (e) {
			var id = $(this).closest('tr').find('td .barang_detail_inout').val();

			 $.ajax({
		    	type: "POST",
		    	url: "<?php echo base_url()?>ipsrs/homeipsrs/get_detail_inventori/" + id,
		    	success: function (data) {
		    		console.log(data);
		    		$('#tbodydetailbrginventori').empty();
		    		for(var i = 0; i < data.length ; i++){
		    			$('#tbodydetailbrginventori').append(
							'<tr>'+
								'<td>'+format_date(data[i]['tanggal'])+'</td>'+
								'<td>'+data[i]['is_out']+'</td>'+
								'<td>'+data[i]['jumlah']+'</td>'+
								'<td>'+data[i]['keterangan']+'</td>'+
							'</tr>'
		    			)
		    		}
		    	},
		    	error: function (data) {
		    		myAlert('gagal');
		    	}
		    })
		})
		
		$('#formmintabarang').submit(function (e) {
			e.preventDefault();
			var item ={};
			item['katakunci'] = $('#katakuncimintabarang').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>ipsrs/homeipsrs/get_barang_gudang',
				success: function (data) {
					console.log(data);//return false;
					$('#tbodybarangpermintaan').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodybarangpermintaan').append(
								'<tr>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['tahun_pengadaan']+'</td>'+
									'<td>'+data[i]['stok_gudang']+'</td>'+
									'<td style="text-align:center"><a href="#" class="addnewpermintaanbarang"><i class="glyphicon glyphicon-check"></i></a></td>'+
									'<td style="display:none">'+data[i]['barang_stok_id']+'</td>'+
									'<td style="display:none">'+data[i]['barang_id']+'</td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodybarangpermintaan').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tbodybarangpermintaan').on('click', 'tr td a.addnewpermintaanbarang',function (e) {
			e.preventDefault();
			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputmintabarang').append(
				'<tr><td>'+cols[0]+'</td>'+//nama
				'<td>'+cols[1]+'</td>'+  //satuan
				'<td>'+cols[2]+'</td>'+ //merk
				'<td>'+cols[3]+'</td>'+ //tahun pengadaan
				'<td>'+cols[4]+'</td>'+ //stok gudang
				'<td align="center" width="160"><input type="number" class="form-control" style="width:150px" placeholder="0"></td>'+ //jumlah minta
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
				'<td style="display:none">'+cols[6]+'</td>'+ //barang_stok_id
				'<td style="display:none">'+cols[7]+'</td></tr>' //barang_id
			)
		})

		$('#permintaanbarangunit').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['no_permintaanbarang'] = $('#nomorpermintaanbarang').val();
			item['tanggal_request'] = $('#tglpermintaanbarang').val();
			item['keterangan_request'] = $('#keteranganpermintaanbarang').val();

			var data = [];
			$('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
		    $('#addinputmintabarang').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;

		    $.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>ipsrs/homeipsrs/submit_permintaan_barangunit',
				success: function (data) {
					console.log(data);
					if (data['error'] == 'n'){
						$('#addinputmintabarang').empty();
						$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
						$('#nomorpermintaanbarang').val('');
						$('#keteranganpermintaanbarang').val('');
					}
					myAlert(data['message']);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*akhir inventori*/
	})

	function viewdetriwayat (this_tr, id) {
		$.ajax({
	    	type: "POST",
	    	url: "<?php echo base_url()?>ipsrs/homeipsrs/get_detail_kegiatan/" + id,
	    	success: function (data) {
	    		var ptg = this_tr.closest('tr').find('td').eq(3).text();
	    		var tgl = this_tr.closest('tr').find('td').eq(1).text();
	    		var unit = this_tr.closest('tr').find('td').eq(2).text();
	    		var ket = this_tr.closest('tr').find('td').eq(4).text();
	    		$('#riw_tgl').val(tgl);$('#riw_unit').val(unit);
	    		$('#riw_ptgs').val(ptg);$('#riw_ket').val(ket);

	    		if (data.length > 0) {
	    			$('#tbl_riw').empty();
	    			for (var i = 0; i < data.length; i++) {
	    				$('#tbl_riw').append(
	    					'<tr>'+
	    					'<td>'+Number(i+1)+'</td>'+
	    					'<td>'+data[i]['nama']+'</td>'+
	    					'<td>'+data[i]['jumlah']+'</td>'+
	    					'<td>'+data[i]['stok_unit']+'</td>'+
	    					'<td>'+data[i]['satuan']+'</td>'

	    					)
	    			};
	    		};
	    	},
	    	error: function (data) {
	    		console.log(data);
	    	}
	    })
	}
</script>