<script type="text/javascript">
	$(document).ready(function () {
		var diagnosa = 0;
		$('.ov').click(function(){diagnosa = 12;});
		$('.ov1').click(function(){diagnosa = 13;});
		$('.ov2').click(function(){diagnosa = 14;});
		$('.ov3').click(function(){diagnosa = 15;});
		$('.ov4').click(function(){diagnosa = 16;});
		$('.resume1').click(function(){diagnosa = 17;});
		$('.resume2').click(function(){diagnosa = 18;});
		$('.resume3').click(function(){diagnosa = 19;});
		$('.resume4').click(function(){diagnosa = 20;});
		$('.resume5').click(function(){diagnosa = 21;});
		$('.resume6').click(function(){diagnosa = 22;});
		$('#search_diagnosa').submit(function(e){
			e.preventDefault();
			var key = $('#katakunci_diag').val();

			$.ajax({
				type:'POST',
				url:'<?php echo base_url() ?>bersalin/bersalindetail/search_diagnosa/'+key,
				success:function(data){
					$('#tbody_diagnosa').empty();

					if(data.length>0){
						for(var i = 0; i<data.length;i++){
							$('#tbody_diagnosa').append(
								'<tr>'+
									'<td>'+data[i]['diagnosis_id']+'</td>'+
									'<td max-width="60%"  style="word-wrap: break-word;white-space: pre-wrap; ">'+data[i]['diagnosis_nama']+'</td>'+
									'<td style="text-align:center; cursor:pointer;"><a onclick="get_diagnosa(&quot;'+data[i]['diagnosis_id']+'&quot;, &quot;'+data[i]['diagnosis_nama']+'&quot;, &quot;'+diagnosa+'&quot;)"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_diagnosa').append(
							'<tr><td colspan="3" style="text-align:center;">Data Diagnosa Tidak Ditemukan</td></tr>'
						);
					}
				}, error:function(data){
					console.log(data);
					myAlert('gagal');
				}
			});
		});

		var d_click = 0;

		$('#dokteroverperawatan').click(function(){d_click = 4;});
		$('#dokterpenolongbersalin').click(function(){d_click = 5;});
		$('#resep_namadokter').click(function(){d_click = 6;});
		$('#namadokter_o').click(function(){d_click = 7;});
		$('#konsul_dokter').click(function(){d_click = 8;});
		$('#namadokter_resume1').click(function(){d_click = 9;});
		$('#namadokter_resume2').click(function(){d_click = 10;});
		$('#tun_namadokter').click(function(){d_click = 11;});

		$('#form_dokter').submit(function(event){
			var item = {};
			item['katakunci'] = $('#inputDokter').val();
			event.preventDefault();

			$.ajax({
				type:'POST',
				data: item,
				url:"<?php echo base_url()?>bersalin/bersalindetail/search_dokter",
				success:function(data){
					console.log(data);
					$('#tbody_dokter').empty();
 					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							var nama = data[i]['nama_petugas'],
								id = data[i]['petugas_id'];

							$("#tbody_dokter").append(
								'<tr>'+
									'<td>'+nama+'</td>'+
									'<td style="text-align:center"><i class="glyphicon glyphicon-check" style="cursor:pointer;" onclick="getDokter(&quot;'+id+'&quot; , &quot;'+nama+'&quot;, &quot;'+d_click+'&quot;)"></i></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_dokter').empty();
						$('#tbody_dokter').append(
							'<tr>'+
					 			'<td colspan="2"><center>Data Tidak Ditemukan</center></td>'+
					 		'</tr>'
						);
					}
				},
				error:function(data){

				}
			});
		});

		var p_click = 0;

		$('#perawatasuhan1').click(function(){p_click = 2;});
		$('#perawatasuhan2').click(function(){p_click = 3;});

		$('#katakunciperawat').keyup(function(event){
			var p_item = $('#katakunciperawat').val();
			event.preventDefault();

			$.ajax({
				type:'POST',
				url:"<?php echo base_url()?>bersalin/bersalindetail/search_perawat/"+p_item,
				success:function(data){
					console.log(data);
					$('#tbody_perawat').empty();
 					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							var nama = data[i]['nama_petugas'],
								id = data[i]['petugas_id'];

							$("#tbody_perawat").append(
								'<tr>'+
									'<td>'+nama+'</td>'+
									'<td style="text-align:center"><i class="glyphicon glyphicon-check" style="cursor:pointer;" onclick="getPerawat(&quot;'+id+'&quot; , &quot;'+nama+'&quot;, &quot;'+p_click+'&quot;)"></i></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_perawat').empty();
						$('#tbody_perawat').append(
							'<tr>'+
					 			'<td colspan="2"><center>Data Tidak Ditemukan</center></td>'+
					 		'</tr>'
						);
					}
				},
				error:function(data){

				}
			});
		});

		$('#formcariasisten').submit(function (e) {
			e.preventDefault();
			var p = $('#keyasisten').val();
			$.ajax({
				type:'POST',
				url:"<?php echo base_url()?>bersalin/bersalindetail/search_asisten/"+p,
				success:function(data){
					console.log(data);
					$('#tbodyasisten').empty();
 					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							var nama = data[i]['nama_petugas'],
								id = data[i]['petugas_id'];

							$("#tbodyasisten").append(
								'<tr>'+
									'<td>'+nama+'</td>'+
									'<td style="text-align:center">'+
										'<a class="inputasistenbersalin"><i class="glyphicon glyphicon-check" style="cursor:pointer;"></i></a>'+
										'<input type="hidden" class="idasist" value="'+id+'">'+
									'</td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbodyasisten').empty();
						$('#tbodyasisten').append(
							'<tr>'+
					 			'<td colspan="2"><center>Data Tidak Ditemukan</center></td>'+
					 		'</tr>'
						);
					}
				},
				error:function(data){

				}
			});
		})
		$('#tbodyasisten').on('click', 'tr td a.inputasistenbersalin', function (e) {
			e.preventDefault();
			$('#penolongbersalin').val($(this).closest('tr').find('td').eq(0).text());
			$('#id_penolongbersalin').val($(this).closest('tr').find('td .idasist').val());
			$('#searchAsisten').modal('hide');
		})

		/*=================== akhir modal========================*/
	
		/*overview kliniks*/
		$('#tbody_overview').on('click', 'tr td a.viewdetailoverviewklinis', function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .overviewid_detail').val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/bersalindetail/get_detail_overview_klinis/" + id,
				success: function (data) {
					console.log(data);
					$('#waktutindakanklinis').val(data['waktu']);
					$('#anamnesaklinis').val(data['anamnesa']);
					$('#tekanandarahklinis').val(data['tekanan_darah']);
					$('#temperaturklinis').val(data['temperatur']);
					$('#nadiklinis').val(data['nadi']);
					$('#pernapasanklinis').val(data['pernapasan']);
					$('#beratklinis').val(data['berat_badan']);
					$('#dokterklinis').val(data['nama_petugas']);
					$('#kode_utamaklinis').val(data['diag_u']);
					$('#diagutamaklinis').val(data['diagnosa_utama']);
					$('#sekunderklinis1_1').val(data['diagnosa_1']);
					$('#sekunderklinis2_2').val(data['diagnosa_2']);
					$('#sekunderklinis3_3').val(data['diagnosa_3']);
					$('#sekunderklinis4_4').val(data['diagnosa_4']);
					$('#sekunderklinis1').val(data['diag_1']);
					$('#sekunderklinis2').val(data['diag_2']);
					$('#sekunderklinis3').val(data['diag_3']);
					$('#sekunderklinis4').val(data['diag_4']);
					$('#detailDiagnosaklinis').val(data['detail_diagnosa']);
					$('#terapiklinis').val(data['terapi']);
					$('#alergiklinis').val(data['alergi']);

				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		/*akhir overview klinis*/

		/*overview igd*/
		$('#tbody_overviewigd').on('click', 'tr td a.viewdetailoverviewigd', function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .overviewigdid_detail').val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/bersalindetail/get_detail_overview_igd/" + id,
				success: function (data) {
					//console.log(data);
					$('#waktutindakanigd').val(data['waktu']);
					$('#anamnesaigd').val(data['anamnesa']);
					$('#tekanandarahigd').val(data['tekanan_darah']);
					$('#temperaturigd').val(data['temperatur']);
					$('#nadiigd').val(data['nadi']);
					$('#pernapasanigd').val(data['pernapasan']);
					$('#beratigd').val(data['berat_badan']);
					$('#dokterigd').val(data['nama_petugas']);
					$('#kode_utamaigd').val(data['diag_u']);
					$('#diagutamaigd').val(data['diagnosa_utama']);
					$('#igd1igd').val(data['diagnosa_1']);
					$('#igd2igd').val(data['diagnosa_2']);
					$('#igd3igd').val(data['diagnosa_3']);
					$('#igd4igd').val(data['diagnosa_4']);
					$('#kode_sek1igd').val(data['diag_1']);
					$('#kode_sek2igd').val(data['diag_2']);
					$('#kode_sek3igd').val(data['diag_3']);
					$('#kode_sek4igd').val(data['diag_4']);
					$('#detailDiagnosaigd').val(data['detail_diagnosa']);
					$('#kepalaleherigd').val(data['kepala_leher']);
					$('#thoraxigd').val(data['thorax_abd']);
					$('#extremitasigd').val(data['extrimitas']);
					$('#terapiigd').val(data['terapi']);

				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*akhir overview igd*/

		/*overview perawatan*/
		$('#submitoverviewperawatan').submit(function (e) {
			e.preventDefault();
			var item = {};
			item[1] = {};
			item[1]['visit_id'] = $('#v_id_perawatan').val();
			item[1]['sub_visit'] = $('#ri_id_perawatan').val();
			item[1]['waktu_visit'] = format_date3($('#waktukunjungandokter').val());
			item[1]['dokter_visit'] = $('#id_dokteroverperawatan').val();
			item[1]['anamnesa'] = $('#anamnesaoverperawatan').val();
			item[1]['diagnosa_utama'] = $('#kode_utamaoverperawatan').val();
			item[1]['sekunder1'] = $('#kode_sek1overperawatan').val();
			item[1]['sekunder2'] = $('#kode_sek2overperawatan').val();
			item[1]['sekunder3'] = $('#kode_sek3overperawatan').val();
			item[1]['sekunder4'] = $('#kode_sek4overperawatan').val();
			item[1]['perkembangan_penyakit'] = $('#perkembanganoverperawatan').val();
			item[1]['tekanan_darah'] = $('#tekanandarahoverperawatan').val();
			item[1]['temperatur'] = $('#temperaturoverperawatan').val();
			item[1]['nadi'] = $('#nadioverperawatan').val();
			item[1]['pernafasan'] = $('#pernapasanoverperawatan').val();
			item[1]['berat_badan'] = $('#beratoverperawatan').val();

			if(item[1]['dokter_visit'] == ''){
				myAlert('isi data dengan benar');$('#dokteroverperawatan').focus();return false;
			}
			if(item[1]['diagnosa_utama'] == ''){
				myAlert('isi data dengan benar');$('#kode_utamaoverperawatan').focus();return false;
			}

			$.ajax({
				type: "POST",
				data: item,
				url: "<?php echo base_url()?>bersalin/bersalindetail/submitoverviewperawatan",
				success: function (data) {
					console.log(data);
					var jml = $('#jml_overkunjungan').val();
					var no = parseInt(jml)+1;					
					var last = '<center><a href="#riwperawatan" class="viewdetailriwperawatan" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a>'+
							'<input type="hidden" class="id_detailriwperawatan" value="'+data['kunjungan_dok_id']+'"></center>'					

					var t = $('#tableoverviewperawatan').DataTable();
					var dok =  $('#dokteroverperawatan').val();
					var diag_utama = $('#diagutamaoverperawatan').val();
					var waktu = format_date(data['waktu_visit']);
					t.row.add([no,waktu,dok,diag_utama,'Bersalin',last]).draw();

					$('#waktukunjungandokter').val("<?php echo date('d/m/Y H:i') ?>");
					$(':input','#submitOverKlinis')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');

					$('#jml_overkunjungan').val(no);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tableoverviewperawatan tbody').on('click', 'tr td a.viewdetailriwperawatan', function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .id_detailriwperawatan').val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/bersalindetail/get_detail_over_perawatan/" + id,
				success: function (data) {
					console.log(data);
					$(':input','#riwperawatan')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');
					$('#waktutindakanrawat').val(format_date(data['waktu_visit']));
					$('#dokterrawat').val(data['dokter']);
					$('#anamnesarawat').val(data['anamnesa']);
					$('#kode_utamarawat').val(data['diag_u']);
					$('#diagnosautamarawat').val(data['diagnosa_utama']);
					$('#kode_sek1rawat').val(data['diag_1']);
					$('#sek1rawat').val(data['diagnosa_1']);
					$('#kode_sek2rawat').val(data['diag_2']);
					$('#sek2rawat').val(data['diagnosa_2']);
					$('#kode_sek3rawat').val(data['diag_3']);
					$('#sek3rawat').val(data['diagnosa_3']);
					$('#kode_sek4rawat').val(data['diag_4']);
					$('#sek4rawat').val(data['diagnosa_4']);
					$('#perkembanganrawat').val(data['perkembangan_penyakit']);
					$('#tekanandarahrawat').val(data['tekanan_darah']);
					$('#temperaturrawat').val(data['temperatur']);
					$('#nadirawat').val(data['nadi']);
					$('#pernapasanrawat').val(data['pernafasan']);
					$('#beratrawat').val(data['berat_badan']);
				},
				error: function (data) {
					console.log(data);
				}

			})
		})

		//asuhan keperawatan
		$('#submitasuhankeperawatan').submit(function (e) {
			e.preventDefault();
			var item = {};
			item[0] = {};
			item[0]['visit_id'] = $('#v_id_asuhan').val();
			item[0]['sub_visit'] = $('#ri_id_asuhan').val();
			item[0]['waktu_tindakan'] = format_date3($('#waktuasuhan').val());
			item[0]['perawat1'] = $('#idperawatasuh1').val();
			item[0]['perawat2'] = $('#idperawatasuh2').val();
			item[0]['perjalanan_penyakit'] = $('#perjalananpenyakitasuhan').val();
			item[0]['pemberian_obat'] = $('#pemberianobatasuhan').val();
			item[0]['diet'] = $('#dietasuhan').val();
			if(item[0]['perawat1'] == ''){
				myAlert('isi data dengan benar');$('#perawatasuhan1').focus();return false;
			}

			$.ajax({
				type: "POST",
				data: item,
				url: "<?php echo base_url()?>bersalin/bersalindetail/submit_asuhan",
				success: function (data) {
					console.log(data);
					var jml = $('#jml_overasuhan').val();
					var no = parseInt(jml)+1;					
					var last = '<center><input type="hidden" class="asuhan_id" value="'+data['asuhan_id']+'">'+
								'<a href="#datasuh" class="lihat_asuh" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>'+
								'<a href="" class="hapus_asuh"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a></center>'

					var t = $('#tabelasuhan').DataTable();
					var p1 =  $('#perawatasuhan1').val();
					var p2 =  $('#perawatasuhan2').val();
					var waktu = format_date(data['waktu_tindakan']);
					t.row.add([no,waktu,p1,p2,'ISOLASI',last, 'tambah']).draw();

					$('#waktuasuhan').val("<?php echo date('d/m/Y H:i') ?>");
					$(':input','#submitasuhankeperawatan')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');

					$('#jml_overasuhan').val(no);

					$('#tabelasuhan tbody a.lihat_asuh').on('click', function (e) {
						e.preventDefault();
						var id = $(this).closest('tr').find('td .asuhan_id').val();
						console.log(id);
						$.ajax({
							type: "POST",
							url: "<?php echo base_url()?>bersalin/bersalindetail/get_detail_asuhan/" +id,
							success: function (data) {
								console.log(data);
								$(':input','#detail_asuh')
								  .not(':button, :submit, :reset, :hidden')
								  .val('');

								  $('#waktu_asuh').val(format_date(data['waktu_tindakan']));
								  $('#perawat1_asuh').val(data['perawat1']);
								  $('#perawat2_asuh').val(data['perawat2']);
								  $('#perjalanan_asuh').val(data['perjalanan_penyakit']);
								  $('#pemberianobat_asuh').val(data['pemberian_obat']);
								  $('#diet_asuh').val(data['diet']);
							},
							error: function (data) {
								console.log(data);
							}
						})
					})

					var this_asuh;
					$('#tabelasuhan tbody a.hapus_asuh').on('click', function (e) {
						e.preventDefault();
						this_asuh = $(this);
						var id = $(this).closest('tr').find('td .asuhan_id').val();
						$.ajax({
							type: "POST",
							url: "<?php echo base_url()?>bersalin/bersalindetail/hapus_asuhan/" +id,
							success: function (data) {
								console.log(data);
								var t = $('#tabelasuhan').DataTable();
								t.row( this_asuh.parents('tr') ).remove().draw();
								t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
							            cell.innerHTML = i+1;
							        } );
								$('#jml_overasuhan').val(Number($('#jml_overasuhan').val()) - 1);
								myAlert('Data Berhasil Dihapus');
								
							},
							error: function (data) {
								console.log(data);
							}
						})
					})
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		
		$('#tabelasuhan tbody a.lihat_asuh').on('click', function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .asuhan_id').val();
			console.log(id);
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/bersalindetail/get_detail_asuhan/" +id,
				success: function (data) {
					console.log(data);
					$(':input','#detail_asuh')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');

					  $('#waktu_asuh').val(format_date(data['waktu_tindakan']));
					  $('#perawat1_asuh').val(data['perawat1']);
					  $('#perawat2_asuh').val(data['perawat2']);
					  $('#perjalanan_asuh').val(data['perjalanan_penyakit']);
					  $('#pemberianobat_asuh').val(data['pemberian_obat']);
					  $('#diet_asuh').val(data['diet']);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		var this_asuh;
		$('#tabelasuhan tbody a.hapus_asuh').on('click', function (e) {
			e.preventDefault();
			this_asuh = $(this);
			var id = $(this).closest('tr').find('td .asuhan_id').val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/bersalindetail/hapus_asuhan/" +id,
				success: function (data) {
					console.log(data);
					var t = $('#tabelasuhan').DataTable();
					t.row( this_asuh.parents('tr') ).remove().draw();
					t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
					$('#jml_overasuhan').val(Number($('#jml_overasuhan').val()) - 1);
					myAlert('Data Berhasil Dihapus');
					
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*akhir overview perawatan*/

		/*visit resep*/
		$('#submitresep').submit(function (e) {
			e.preventDefault();
			var item = {};
		    var number = 1;
		    item[number] = {};

		    item[number]['tanggal'] = $('#resep_date').val();
			item[number]['dokter'] = $('#resep_id_dokter').val();
			item[number]['visit_id'] = $('#v_id_resep').val();
			item[number]['resep'] = $('#resep_deskripsi').val();
			item[number]['sub_visit'] = $('#r_id_resep').val();
			//console.log(item);return false;
			if (item[number]['dokter'] == '') {
				myAlert('pilih dokter'); $('#resep_namadokter').focus();return false;
			};
			$.ajax({
				type: "POST",
				data : item,
				url: "<?php echo base_url()?>bersalin/bersalindetail/save_visit_resep",
				success: function (data) {
					console.log(data);
					var jml = $('#jml_resep').val();
					var no = parseInt(jml)+1;
					var t = $('#tableResep').DataTable();
					var date = format_date(data['tanggal']);

					t.row.add([
						no,
						data['nama_petugas'],
						date,
						data['resep'],
						"BELUM",
						"BELUM",
						'<center><a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value="'+data['resep_id']+'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a></center>',
						no
					]).draw();

					$('#jml_resep').val(no);
					$('#resep_namadokter').val('');
					$('#resep_id_dokter').val('');
					$('#resep_deskripsi').val('');
					$('#resep_date').val("<?php echo date('d/m/Y'); ?>");

					myAlert('Data Berhasil Ditambahkan');
				},
				error: function (data) {
					myAlert('gagal');
					console.log(data);
				}
			});
		});

		$('#tableResep').on('click','.hapusresep',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var igd_id = $('#igd_id').val();
			var t = $('#tableResep').DataTable();
			t.row( $(this).parents('tr') ).remove().draw();
			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>bersalin/bersalindetail/hapus_resep/"+id+"/"+igd_id,
				success:function(data){
					console.log(data);
					
					var no;
					$('#jml_resep').val(Number($('#jml_resep').val()) - 1);
					t.on( 'order.dt search.dt', function () {
			        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			            cell.innerHTML = i+1;
			        } );
			    } ).draw();
				},
				error:function(data){
					console.log(data);
				}	
			});
		});
		/*akhir visit resep*/

		/*order operasi*/
		var item_order = {};
		item_order[1] = {};
		$('#submit_order_operasi').submit(function(e){
			e.preventDefault();
			if($('#iddokter_o').val() == '') {myAlert('pilih dokter');$('#namadokter_o').focus();return false};
			if($('#operasi_jenis').find('option:selected').val() == '') {myAlert('pilih jenis operasi');return false};
			item_order[1]['visit_id'] = $('#v_id_operasi').val();
			item_order[1]['sub_visit'] = $('#r_id_operasi').val();
			item_order[1]['dept_id'] = $('#dept_id').val();
			item_order[1]['dept_tujuan'] = "KAMAR OPERASI"; 
			item_order[1]['pengirim'] = $('#iddokter_o').val();
			item_order[1]['alasan'] = $('#operasi_detail').val();
			item_order[1]['jenis_operasi'] = $('#operasi_jenis').find('option:selected').val();
			item_order[1]['waktu_mulai'] = $('#operasi_date').val();
			//console.log(item_order);return false;
			$.ajax({
				type: "POST",
				data: item_order,
				url: "<?php echo base_url()?>bersalin/bersalindetail/save_order_operasi",
				success: function(data){
					console.log(data);
					var i = $('#jml_data_order').val();
					var no = parseInt(i)+1;
					var t = $('#tableOpeasi').DataTable();

					t.row.add([
						no,
						data['waktu_mulai'],
						data['nama_petugas'],
						'Kamar Operasi',
						data['det_operasi'],
						'<center><i class="glyphicon glyphicon-trash hapusorder" data-toggle="tooltip" data-placement="top" title="Hapus" style="cursor:pointer;"><input type="hidden" class="getid" value="'+data['order_operasi_id']+'"></i></center>',
						no

					]).draw();

					$('#jml_data_order').val(no);

					$(':input','#submit_order_operasi')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');
					$('#operasi_date').val("<?php echo date('d/m/Y H:i:s') ?>");

					myAlert('Data Berhasil Ditambahkan');

				},
				error: function(data){
					console.log(data);
					myAlert('gagal');
				}

			});
		});
		
		var this_order;
		$(document).on('click','.hapusorder',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();this_order = $(this);
			//var ri_id = $('#r_id_operasi').val();
			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>bersalin/bersalindetail/hapus_order/"+id,//+"/"+ri_id
				success:function(data){
					console.log(data);					
					var no = 0;
					var t = $('#tableOpeasi').DataTable();
					t.row( this_order.parents('tr') ).remove().draw();
					t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
					$('#jml_data_order').val(Number($('#jml_data_order').val()) - 1);
				},
				error:function(data){
					console.log(data);
				}	
			});
		});
		/*akhir order operasi*/

		/*visit gizi*/
		var g_item = {};
		g_item[1] = {};
		$('#submit_gizi').submit(function(e){
			e.preventDefault();
			if ($('#konsul_idDokter').val() == '') {myAlert('pilih dokter');$('#konsul_dokter').focus();return false;};
			g_item[1]['tanggal'] = $('#konsul_date').val();
			g_item[1]['visit_id'] = $('#v_id_gizi').val();
			g_item[1]['sub_visit'] = $('#r_id_gizi').val();
			g_item[1]['konsultan'] = $('#konsul_idDokter').val();
			g_item[1]['kajian_gizi'] = $('#konsul_kajiangizi').val();
			g_item[1]['anamnesa_diet'] = $('#konsul_anemdiet').val();
			g_item[1]['kajian_diet'] = $('#konsul_kajiandiet').val();
			g_item[1]['detail_menu'] =  $('#konsul_detail').val();
			var konsultan = $('#konsul_dokter').val();

			$.ajax({
				type:'POST',
				data:g_item,
				url:'<?php echo base_url(); ?>bersalin/bersalindetail/save_gizi',
				success:function(data){
					console.log(data);
					var t = $('#tableKonsultasi').DataTable();

					var jml = parseInt($('#jml_gizi').val());
					var no = jml+1;

					myAlert("data Berhasil Ditambahkan ");

					var action = '<center><a style="cursor:pointer;" class="hapusgizi"><input type="hidden" class="getid" value="'+data['gizi_id']+'"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>';
						action += '<a href="#print"><i class="glyphicon glyphicon-print"  data-toggle="tooltip" data-placement="top" title="Print"></i></a></center>';
					
					t.row.add([
						no,
						format_date(data['tanggal']),
						konsultan,
						data['kajian_gizi'],
						data['anamnesa_diet'],
						data['kajian_diet'],
						data['detail_menu'],
						action,
						no
					]).draw();

					$('#jml_gizi').val(no);

					$(':input','#submit_gizi')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');
					$('#konsul_date').val("<?php echo date('d/m/Y H:i') ?>");

				},error:function(data){
					console.log(data);
				}
			});

		});
		var this_gizi;
		$(document).on('click','.hapusgizi',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();this_gizi = $(this);
			//var igd_id = $('#igd_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>bersalin/bersalindetail/hapus_gizi/"+id,//+"/"+igd_id,
				success:function(data){
					console.log(data);
					var t = $('#tableKonsultasi').DataTable();
					t.row( this_gizi.parents('tr') ).remove().draw();
					t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
					$('#jml_gizi').val(Number($('#jml_gizi').val()) - 1);
				},
				error:function(data){
					console.log(data);
				}	
			});
		});
		/*akhir visit gizi*/

		/*permintaan makan*/
		$('#tipediet').on('change',function () {
			var tipe = $(this).find('option:selected').val();

			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/bersalindetail/get_paket_makan/"+tipe,
				success: function (data) {
					$('#paketmakan').empty();
					$('#paketmakan').append('<option value="" selected>Pilih</option>');
					for (var i = 0; i < data.length; i++) {
						$('#paketmakan').append('<option value="'+data[i]['id']+'">'+data[i]['nama_paket']+'</option>')
					};
					console.log(data);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#edittipediet').on('change',function () {
			var tipe = $(this).find('option:selected').val();

			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/bersalindetail/get_paket_makan/"+tipe,
				success: function (data) {
					$('#paketmakanedit').empty();

					$('#paketmakanedit').append('<option value="" selected>Pilih</option>');
					for (var i = 0; i < data.length; i++) {
						$('#paketmakanedit').append('<option value="'+data[i]['id']+'">'+data[i]['nama_paket']+'</option>')
					};
					console.log(data);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#sumbitpermintaanmakan').submit(function (e) {
			e.preventDefault();
			var tipe = $('#tipediet').find('option:selected').val();
			var paket = $('#paketmakan').find('option:selected').val();
			if (tipe == '') {myAlert('pilih tipe diet');$('#tipediet').focus();return false;};
			if (paket == '') {myAlert('pilih paket makan');$('#paketmakan').focus();return false;};

			var namatipe = $('#tipediet').find('option:selected').text();
			var namapaket = $('#paketmakan').find('option:selected').text();
			//console.log(namatipe + namapaket); return false;
			var item = {};
			item[0] = {};
			item[0]['waktu_permintaan'] = $('#tanggalmakan').val();
			item[0]['visit_id'] = $('.visit_id_makan').val();
			item[0]['sub_visit'] = $('.ri_id_makan').val();
			item[0]['kamar_id'] = $('.kamar_id_makan').val();
			item[0]['bed_id'] = $('.bed_id_makan').val();
			item[0]['tipe_diet'] = $('#tipediet').find('option:selected').val();
			item[0]['paket_makan'] = $('#paketmakan').find('option:selected').val();
			item[0]['keterangan'] = $('#keteranganmakan').val();
			
			$.ajax({
				type: "POST",
				data: item,
				url: "<?php echo base_url()?>bersalin/bersalindetail/submit_permintaan_makan",
				success: function (data) {
					var t = $('#tabelriwayatmakan').DataTable();
					var jml = parseInt($('#jml_makan').val());
					var no = jml+1;

					var last = '<center><a href="#editpaketmakan" data-toggle="modal" class="editmakan"><i class="glyphicon glyphicon-edit"  data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
								'<a href="#" class="hapusmakan"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a></center>'+
								'<input type="hidden" class="makan_id" value="'+data['makan_id']+'">'
					t.row.add([
							no,
							'<center>'+format_date(data['waktu_permintaan'])+'</center>',
							namatipe,
							namapaket,
							data['keterangan'],
							'Belum Dikirim',
							last,
							'faf'
						]).draw();
					t.on('order.dt search.dt', function () {
				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
				    }).draw();
					$('#keteranganmakan').val('');
					$("#tipediet option[value='']").attr("selected", "selected");
					$("#paketmakan option[value='']").attr("selected", "selected");
					$('#tanggalmakan').val('<?php echo date("d/m/Y H:i:s") ?>')
					$('#jml_makan').val(no);
					$('[data-toggle="tooltip"]').tooltip();
					console.log(data);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
	
		var this_makan;
		$('#tabelriwayatmakan tbody').on('click', 'tr td a.editmakan',function (e) {
			e.preventDefault();
			this_makan = $(this).closest('tr');
			$('#edit_makan_id').val(this_makan.find('td .makan_id').val());
			var a = this_makan.find('td').eq(1).text();
			var b = this_makan.find('td').eq(2).text();
			var c = this_makan.find('td').eq(3).text();
			$('#editwaktumakan').val(format_date2(a));
			$('#editketeranganmakan').val(this_makan.find('td').eq(4).text());
		})

		$('#submit_edit_makan').submit(function (e) {
			e.preventDefault();

			var id = $('#edit_makan_id').val();

			var namatipe = $('#edittipediet').find('option:selected').text();
			var namapaket = $('#paketmakanedit').find('option:selected').text();
			if ($('#edittipediet').find('option:selected').val() == '') {myAlert('pilih tipe diet');$('#edittipediet').focus();return false;};
			if ($('#paketmakanedit').find('option:selected').val() == '') {myAlert('pilih paket makan');$('#paketmakanedit').focus();return false;};
			var item = {};
			item[0] = {};
			item[0]['waktu_permintaan'] = format_date3($('#editwaktumakan').val());
			item[0]['tipe_diet'] = $('#edittipediet').find('option:selected').val();
			item[0]['paket_makan'] = $('#paketmakanedit').find('option:selected').val();
			item[0]['keterangan'] = $('#editketeranganmakan').val();

			$.ajax({
				type: "POST",
				data: item,
				url: "<?php echo base_url()?>bersalin/bersalindetail/edit_permintaan_makan/" + id,
				success: function (data) {
					this_makan.find('td').eq(1).text(format_date(data['waktu_permintaan']));
					this_makan.find('td').eq(2).text(namatipe);
					this_makan.find('td').eq(3).text(namapaket);
					this_makan.find('td').eq(4).text(data['keterangan']);
					$('#editpaketmakan').modal('hide');
					console.log(data);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
	
		var this_makan_delete;
		$('#tabelriwayatmakan tbody').on('click', 'tr td a.hapusmakan',function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .makan_id').val();
			this_makan_delete = $(this);
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/bersalindetail/hapus_permintaan_makan/" + id,
				success: function (data) {
					console.log(data);
					var t = $('#tabelriwayatmakan').DataTable();
					t.row(this_makan_delete.parents('tr')).remove().draw();
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*akhir permintaan makan*/
		/*penunjang*/
		$('#submit_penunjang').submit(function(event){
			event.preventDefault();
			var item = {};
			item[1] = {};

			item[1]['visit_id'] = $('.visit_id').val();
			item[1]['sub_visit'] = $('.ri_id').val();
			item[1]['dept_tujuan'] = $('#tun_tujuan').val();
			item[1]['pengirim'] = $('#tun_iddokter').val();
			item[1]['jenis_periksa'] = $('#tun_jenis').val();
			item[1]['waktu'] = $('#tun_date').val();
			item[1]['status'] = "BELUM";

			var pengirim = $('#tun_namadokter').val();
			var unit_asal = "ISOLASI";
			var unit_tujuan = $('#tun_tujuan option:selected').html();
			
			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>isolasi/isolasidetail/save_penunjang',
				success:function(data){
					var t = $('#table_penunjang').DataTable();
					var no = $('#tun_jumlah').val();
					var detail = '<center><input type="hidden" class="idpenunjang" value="'+data['penunjang_id']+'"><a class="detailpenunjang" href="#viewRiwayat" data-toggle="modal" data-placement="top"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Details"></i></a></center>';
					var tanggal = format_date(data['waktu']);

					t.row.add([
						++no,
						tanggal,
						unit_tujuan,
						unit_asal,
						data['status'],
						detail,
						data['waktu']
					]).draw();

					$('#tun_jumlah').val(no);
					myAlert('Data Berhasil Ditambahkan');
					console.log(data);

				},error:function(data){
					console.log(data);
					myAlert('Data Gagal Ditambahkan');
				}
			});
		});

		$('#table_penunjang').on('click', 'tr td .detailpenunjang', function(){
			var penunjang = $(this).closest('tr').find('.idpenunjang').val();

			$.ajax({
				type:'POST',
				url:'<?=base_url()?>rawatjalan/daftarpasien/get_detailpenunjang/'+penunjang,
				success:function(data){
					console.log(data);
					$('#dp_penunjang').text(data[0]['penunjang_id']);
					$('#dp_date').text(format_date(data[0]['waktu']));
					$('#dp_periksa').text(data[0]['nama_petugas']);
					$('#dp_dept').text(data[0]['nama_dept']);
					$('#dp_status').text(data[0]['status_penunjang']);

					$('#tbody_detail').empty();
					
					if(data[0]['status_penunjang']!='BELUM'){
						for(var i=0; i<data.length; i++){
							$('#tbody_detail').append(
								'<tr>'+
									'<td><center>'+data[i]['nama_tindakan']+'</center></td>'+
									'<td>'+data[i]['hasil']+'</td>'+
									'<td>'+data[i]['nilai_normal']+'</td>'+
									'<td>'+data[i]['ket_hasil']+'</td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_detail').append(
							'<tr>'+
								'<td colspan="4"><center>Belum Terdapat Data Pemeriksaan</center></td>'+
							'</tr>'
						);
					}
				},error:function(data){
					console.log(data);
				}
			})
		})

		/*akhir penunjang*/
		/*resume pulang*/
		$('#submit_sebab').submit(function(e){
			e.preventDefault();
			var data = {};
			data['search'] = $('#sebab_katakunci').val();

			$.ajax({
				type:'POST',
				data:data,
				url:"<?php echo base_url()?>igd/igddetail/search_sebab", //sama di igd
				success:function(data){
					console.log(data);
					$('#tbody_sebab').empty();
 					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							var nama = data[i]['sebab_penyakit'],
								kode = data[i]['kode_sebab'],
								id = data[i]['sebab_id'];

							$("#tbody_sebab").append(
								'<tr>'+
									'<td>'+kode+'</td>'+
									'<td>'+nama+'</td>'+
									'<td style="text-align:center"><i class="glyphicon glyphicon-check" style="cursor:pointer;" onclick="getSebab(&quot;'+id+'&quot; , &quot;'+nama+'&quot;)"></i></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_sebab').empty();
						$('#tbody_sebab').append(
							'<tr>'+
					 			'<td colspan="3"><center>Data Sebab Tidak Ditemukan</center></td>'+
					 		'</tr>'
						);
					}
				}
			});
		});

		$('#submitresume').submit(function(e){
			e.preventDefault();
			var item = {};
			item[1] = {};

			item[1]['ri_id'] = $('#ri_id_resume').val();
			item[1]['visit_id'] = $('#v_id_resume').val();
			//item[1]['waktu_keluar'] = $('#res_date').val();
			item[1]['alasan_keluar'] = $('#alasanKeluarPasien').val();
			item[1]['detail_alasan_pulang'] = $('#alasanPulang').val();
			var rs = $('#rumasakitlain').val();
			item[1]['diagnosa_akhir'] = $('#kode_utama_masuk').val();
			item[1]['diagnosa_utama'] = $('#res_kode_utama').val();
			item[1]['sekunder1'] = $('#res_kode_sek1').val();
			item[1]['sekunder2'] = $('#res_kode_sek1').val();
			item[1]['sekunder3'] = $('#res_kode_sek1').val();
			item[1]['sekunder4'] = $('#res_kode_sek1').val();
			
			if ($('#alasanKeluarPasien').val() == 'Pasien Meninggal') {
				item[1]['detail_kematian'] = $('#res_dmeninggal').val();
				item[1]['keterangan_kematian'] = $('#res_ketmeninggal').val();
				if($('#res_datemeninggal').val()!="")
					item[1]['waktu_kematian'] = $('#res_datemeninggal').val();
			};
			if (rs != '') {item[1]['alasan_keluar'] = "dirujur ke " + rs};
			
			item[1]['sebab'] = $('#res_idsebab').val();

			item[1]['is_kasus_baru'] = $('input[name="res_jk"]:checked', '#submitresume').val();	
			var bed_id = $('.bed_id').val();
			//console.log(item);return false;
			$.ajax({
				type:"POST",
				data:item,
				url:'<?php echo base_url();?>bersalin/bersalindetail/save_resume/' + bed_id,
				success:function(data){
					myAlert('Data Berhasil Ditambahkan');
					console.log(data);
					window.location.href = '<?php echo base_url();?>isolasi/homeisolasi';
				},
				error:function (data) {
					console.log(data);
				}
			})
		});
		/*akhir resume pulang*/

		/*tindakan*/
		$('#js_paramedis').focus(function(){
			var $input = $('#js_paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>bersalin/bersalindetail/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#paramedis_id').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		var autodata = [];
		var iddata = [];
		$('#namatindakan').focus(function(){
			var $input = $('#namatindakan');
			
			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>bersalin/bersalindetail/get_master_tindakan',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_tindakan']);
							iddata.push(data[i]['tindakan_id']);
						}
					}
				});
			}

			$input.typeahead({source:autodata, 
	        autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = autodata.indexOf(current);

			    $('#idtindakan_klinik').val(iddata[index]);
			    $('#preview_tindakan').val(autodata[index]);				

			    $('#kelas_klinik').prop('disabled', false);    

			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

		});
		
		$('#kelas_klinik').prop('disabled', true);

		$('#kelas_klinik').change(function(){
			var item = {};
			item['kelas'] = $(this).val();
			item['nama'] = $('#idtindakan_klinik').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>bersalin/bersalindetail/get_tariftindakan',
				success:function(data){
					console.log(data);
					var tarif = 0;
					var lingkup = $('#lingkup').val();
					var persen = 0;

					$('#idtindakdetail').val(data['detail_id']);

					tarif = (Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))+((Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))*persen);
					
					$('#js_klinik').val(data['js']);
					$('#jp_klinik').val(data['jp']);
					$('#bakhp_klinik').val(data['bakhp']);
					$('#js_tarif').val(tarif);
					$('#js_jumlah').val(tarif);
				}
			});
		});

		$('#js_onfaktur').keyup(function(){
			var onfaktur = $('#js_onfaktur').val();
			var tarif = $('#js_tarif').val();
			var jumlah = Number(onfaktur)+Number(tarif);
			$('#js_jumlah').val(jumlah);
		});

		$('#js_onfaktur').change(function(){
			var tarif = $('#js_tarif').val();
			var onfaktur = $(this).val();
			var jumlah = Number(tarif)+Number(onfaktur);
			
			$('#js_jumlah').val(jumlah);
		});

		$('#submitTindakanPerawatan').submit(function (e) {
			e.preventDefault();
			if ($('#namatindakan').val() == '') {myAlert('pilih tindakan');$('#namatindakan').focus();return false;};
			if ($('#js_paramedis').val() == '') {myAlert('pilih dokter');$('#js_paramedis').focus();return false;};

			var item = {};
		    var number = 1;
		    item[number] = {};

		    item[number]['waktu_tindakan'] = format_date3($('#tin_date').val());
			item[number]['tindakan_id'] = $('#idtindakdetail').val();
			item[number]['visit_id'] = $('.visit_id_t').val();
			item[number]['sub_visit'] = $('.ri_id_t').val();
			item[number]['js'] = $('#js_klinik').val();
			item[number]['jp'] = $('#jp_klinik').val();
			item[number]['bakhp'] = $('#bakhp_klinik').val();
			item[number]['on_faktur'] = $('#js_onfaktur').val();
			item[number]['paramedis'] = $('#paramedis_id').val();
			item[number]['tarif'] = $('#js_tarif').val();
			item[number]['jumlah'] = $('#js_jumlah').val();
			item[number]['paramedis_lain'] =$('#js_paramedis_lain').val();
			item[number]['dept_id'] = $('#dept_id').val();

			$.ajax({
				type: "POST",
				data : item,
				url: "<?php echo base_url()?>bersalin/bersalindetail/save_tindakan",
				success: function (data) {
					console.log(data);
					var t = $('#tableCareRawat').DataTable();

					$(':input','#submitTindakanPerawatan')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');
					$('#tin_date').val("<?php echo date('d/m/Y H:i:s') ?>");

					myAlert('Data Berhasil Ditambahkan');
				
					var no = $('#jml_tindak_rawat').val();

					var i = parseInt(no)+1;
					t.row.add([
						i,
						format_date(data['waktu_tindakan']),
						data['nama_tindakan'],
						data['j_sarana'],
						data['j_pelayanan'],
						data['bakhp_this'],
						data['on_faktur'],
						data['nama_petugas'],
						data['jumlah'],
						"<center><a class='hapusTindakan' style='cursor:pointer;'><input type='hidden' class='getid' value='"+data['care_id']+"''><i class='glyphicon glyphicon-trash'></i></a></center>",
						i
					]).draw();
					$('#jml_tindak_rawat').val(i);
					$('#tambahTindakanok').modal('hide');
				},
				error: function (data) {
					console.log(data);
					myAlert('gagal');
					
				}
			});
			$('#tambahTindakan').modal('hide');
		});

		var care;
		$(document).on('click','.hapusTindakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#igd_id').val();
			care = $(this);
			//var dep = $('#dept_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>bersalin/bersalindetail/hapus_tindakan/"+id+"/"+v_id,//+"/"+dep,
				success:function(data){
					console.log(data);

					var t = $('#tableCareRawat').DataTable();
					$('#jml_tindak_rawat').val(Number($('#jml_tindak_rawat').val()) - 1);
					t.row(care.parents('tr')).remove().draw();
					t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			            cell.innerHTML = i+1;
			        } );
				},
				error:function(data){
					console.log(data);
				}	
			});
		});

		/*akhir tindakan*/

	})

	function get_diagnosa(id, nama, diagnosa){
		//myAlert(diagnosa);
		switch(diagnosa){
			case '12': $('#kode_utamaoverperawatan').val(id);$('#diagutamaoverperawatan').val(nama);break;
			case '13': $('#kode_sek1overperawatan').val(id);$('#diagsek1overperawatan').val(nama);break;
			case '14': $('#kode_sek2overperawatan').val(id);$('#diagsek2overperawatan').val(nama);break;
			case '15': $('#kode_sek3overperawatan').val(id);$('#diagsek3overperawatan').val(nama);break;
			case '16': $('#kode_sek4overperawatan').val(id);$('#diagsek4overperawatan').val(nama);break;
			case '17': $('#res_kode_utama').val(id);$('#res_diag_utama').val(nama);break;
			case '18': $('#res_kode_sek1').val(id);$('#res_sek1').val(nama);break;
			case '19': $('#res_kode_sek2').val(id);$('#res_sek2').val(nama);break;
			case '20': $('#res_kode_sek3').val(id);$('#res_sek3').val(nama);break;
			case '21': $('#res_kode_sek4').val(id);$('#res_sek4').val(nama);break;
			case '22': $('#kode_utama_masuk').val(id);$('#res_utama_masuk').val(nama);break;
		}

		$('#tbody_diagnosa').empty();
		$('#tbody_diagnosa').append('<tr><td colspan="3" style="text-align:center;">Cari Data Diagnosa</td></tr>');
		$('#katakunci_diag').val("");
		$('#searchDiagnosa').modal('hide');
	}
	function getDokter(id, nama, dokter){
		switch(dokter){
			case '4': $('#id_dokteroverperawatan').val(id);$('#dokteroverperawatan').val(nama);break;
			case '5': $('#id_dokterpenolongbersalin').val(id);$('#dokterpenolongbersalin').val(nama);break;
			case '6': $('#resep_id_dokter').val(id);$('#resep_namadokter').val(nama);break;
			case '7': $('#iddokter_o').val(id);$('#namadokter_o').val(nama);break;
			case '8': $('#konsul_idDokter').val(id);$('#konsul_dokter').val(nama);break;
			case '9': $('#iddokter_resume1').val(id);$('#namadokter_resume1').val(nama);break;
			case '10': $('#iddokter_resume2').val(id);$('#namadokter_resume2').val(nama);break;
			case '11': $('#tun_iddokter').val(id);$('#tun_namadokter').val(nama);break;
		}		

		$("#searchDokter").modal('hide');
		$("#inputDokter").val("");
		$('#tbody_dokter').empty();
		$('#tbody_dokter').append('<tr><td colspan="2" style="text-align:center;">Cari dokter</td></tr>');
	}

	function getPerawat(id, nama, perawat){
		switch(perawat){
			case '2': $('#idperawatasuh1').val(id);$('#perawatasuhan1').val(nama);break;
			case '3': $('#idperawatasuh2').val(id);$('#perawatasuhan2').val(nama);break;
		}		

		$("#searchPerawat").modal('hide');
		$("#katakunciperawat").val("");
		$('#tbody_perawat').empty();
		$('#tbody_perawat').append('<tr><td colspan="2" style="text-align:center;">Cari perawat</td></tr>');
	}

	function getPerawat(id, nama, perawat){
		switch(perawat){
			case '1': $('#id_perawat_jaga').val(id);$('#perawatoverigd').val(nama);break;
			case '2': $('#idperawatasuh1').val(id);$('#perawatasuhan1').val(nama);break;
			case '3': $('#idperawatasuh2').val(id);$('#perawatasuhan2').val(nama);break;
		}		

		$("#searchPerawat").modal('hide');
		$("#katakunciperawat").val("");
		$('#tbody_perawat').empty();
		$('#tbody_perawat').append('<tr><td colspan="2" style="text-align:center;">Cari perawat</td></tr>');
	}

	function getPaket(id, nama){
		$("#searchPaketMakanan").modal('hide');
		$("#id_paket").val(id);
		$("#searchpaket").val(nama);
		$("#katakuncipaket").val("");
	}

	function save_overview (item) {
		var petugas = $('#nama_dOver').val();

		$.ajax({
			type: "POST",
			data : item,
			url: "<?php echo base_url()?>bersalin/bersalindetail/save_overview",
			success: function (data) {				
				$('#inputdate').val("<?php echo date('d/m/Y H:i') ?>");
				$(':input','#submitOverKlinis')
				  .not(':button, :submit, :reset, :hidden')
				  .val('');

				console.log(data);
				var jml = $('#jml_over').val();
				var no = parseInt(jml)+1;

				var t = $('#tabelhistoryoverklinis').DataTable();
				var last = '<a href="#riwayatoverviewklinis" class="viewdetailoverviewklinis" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a>'+
						'<input type="hidden" class="overviewid_detail" value="'+data['id']+'">'

				t.row.add([
					no,
					'Bersalin',
					data['anamnesa'],
					petugas,
					last,
					"fa"						
				]).draw();
				
				$('#jml_over').val(no);
				myAlert("Data Berhasil Disimpan");
			},
			error: function (data) {
				console.log(data);
				myAlert(data);
			}
		});

		return false;
	}

	function detailOver(id){
		var dsek1,
			dsek2,
			dsek3,
			dsek4;
		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>bersalin/bersalindetail/get_detail_over/'+id,
			success:function(data){
				console.log(data);
				$(':input','#riwkondok')
				  .not(':button, :submit, :reset, :hidden')
				  .val('');
				$('#detail_waktu').val(data['waktu']);
				$('#detail_anamnesa').val(data['anamnesa']);
				$('#detail_tekanan').val(data['tekanan_darah']);
				$('#detail_temperatur').val(data['temperatur']);
				$('#detail_nadi').val(data['nadi']);
				$('#detail_pernapasan').val(data['pernapasan']);
				$('#detail_berat').val(data['berat_badan']);
				$('#detail_dokter').val(data['nama_petugas']);
				$('#detail_kutama').val(data['diag_u']);
				$('#detail_ksek1').val(data['diag_1']);
				$('#detail_ksek2').val(data['diag_2']);
				$('#detail_ksek3').val(data['diag_3']);
				$('#detail_ksek4').val(data['diag_4']);
				$('#detail_dutama').val(data['diagnosa_nama']);
				$('#detail_detail').val(data['detail_diagnosa']);
				$('#detail_terapi').val(data['terapi']);
				$('#detail_alergi').val(data['alergi']);
				$('#detail_dsek1').val(data['diagnosa_1']);
				$('#detail_dsek2').val(data['diagnosa_2']);
				$('#detail_dsek3').val(data['diagnosa_3']);
				$('#detail_dsek4').val(data['diagnosa_4']);
			}
		});
	}

	function detailOverIgd(id){
		var dsek1,
			dsek2,
			dsek3,
			dsek4;
		$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/bersalindetail/get_detail_overview_igd/" + id,
				success: function (data) {
					//console.log(data);
					$(':input','#riwayatpenangananigd')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');
					$('#waktutindakanigd').val(data['waktu']);
					$('#anamnesaigd').val(data['anamnesa']);
					$('#tekanandarahigd').val(data['tekanan_darah']);
					$('#temperaturigd').val(data['temperatur']);
					$('#nadiigd').val(data['nadi']);
					$('#pernapasanigd').val(data['pernapasan']);
					$('#beratigd').val(data['berat_badan']);
					$('#dokterigd').val(data['nama_petugas']);
					$('#kode_utamaigd').val(data['diag_u']);
					$('#diagutamaigd').val(data['diagnosa_utama']);
					$('#igd1igd').val(data['diagnosa_1']);
					$('#igd2igd').val(data['diagnosa_2']);
					$('#igd3igd').val(data['diagnosa_3']);
					$('#igd4igd').val(data['diagnosa_4']);
					$('#kode_sek1igd').val(data['diag_1']);
					$('#kode_sek2igd').val(data['diag_2']);
					$('#kode_sek3igd').val(data['diag_3']);
					$('#kode_sek4igd').val(data['diag_4']);
					$('#detailDiagnosaigd').val(data['detail_diagnosa']);
					$('#kepalaleherigd').val(data['kepala_leher']);
					$('#thoraxigd').val(data['thorax_abd']);
					$('#extremitasigd').val(data['extrimitas']);
					$('#terapiigd').val(data['terapi']);

				},
				error: function (data) {
					console.log(data);
				}
			})
	}

	function detailOverPerawatan (id) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>bersalin/bersalindetail/get_detail_over_perawatan/" + id,
			success: function (data) {
				console.log(data);
				$(':input','#riwperawatan')
					  .not(':button, :submit, :reset, :hidden')
					  .val('');
				$('#waktutindakanrawat').val(format_date(data['waktu_visit']));
				$('#dokterrawat').val(data['dokter']);
				$('#anamnesarawat').val(data['anamnesa']);
				$('#kode_utamarawat').val(data['diag_u']);
				$('#diagnosautamarawat').val(data['diagnosa_utama']);
				$('#kode_sek1rawat').val(data['diag_1']);
				$('#sek1rawat').val(data['diagnosa_1']);
				$('#kode_sek2rawat').val(data['diag_2']);
				$('#sek2rawat').val(data['diagnosa_2']);
				$('#kode_sek3rawat').val(data['diag_3']);
				$('#sek3rawat').val(data['diagnosa_3']);
				$('#kode_sek4rawat').val(data['diag_4']);
				$('#sek4rawat').val(data['diagnosa_4']);
				$('#perkembanganrawat').val(data['perkembangan_penyakit']);
				$('#tekanandarahrawat').val(data['tekanan_darah']);
				$('#temperaturrawat').val(data['temperatur']);
				$('#nadirawat').val(data['nadi']);
				$('#pernapasanrawat').val(data['pernafasan']);
				$('#beratrawat').val(data['berat_badan']);
			},
			error: function (data) {
				console.log(data);
			}

		})
	}

	function getSebab(id, nama){
		$('#res_sebab').val(nama);
		$('#res_idsebab').val(id);

		$("#searchGolongan").modal('hide');
		$("#sebab_katakunci").val("");
		$('#tbody_sebab').empty();
		$('#tbody_sebab').append('<tr><td colspan="3" style="text-align:center;">Cari Data</td></tr>');
	}
</script>