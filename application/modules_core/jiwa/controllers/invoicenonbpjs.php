<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Invoicenonbpjs extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_invoicenonbpjs");
		$data['page_title'] = "Invoice Non BPJS";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load template
		//$data['content'] = 'tagihan/invoicenonbpjs';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		//$this->load->view('base/operator/template', $data);
		redirect('jiwa/homejiwa');
	}

	public function invoice($no_invoice){
		$data['content'] = 'tagihan/invoicenonbpjs';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$invoice = $this->m_invoicenonbpjs->get_visit_id($no_invoice);
		$visit_id = $invoice['visit_id'];
		$sub_visit = $invoice['sub_visit'];
		$data['visit_id'] = $visit_id;
		$data['sub_visit'] = $invoice['sub_visit'];
		$data['no_invoice'] = $no_invoice;
		$data['invoice'] = $invoice;
		$data['deposit'] = $this->m_invoicenonbpjs->get_deposit($visit_id)['deposit'];
		
		$pasien = $this->m_invoicenonbpjs->get_data_pasien($visit_id);
		$data['pasien'] = $pasien;

		$data['tagihanadmisi'] = $this->m_invoicenonbpjs->get_tagihanadmisi($no_invoice);
		$data['tagihantunjang'] = $this->m_invoicenonbpjs->get_tagihantunjang($sub_visit);
		$data['tindakan'] = $this->m_invoicenonbpjs->get_tindakanoperasi($no_invoice);
		$data['paket_makan'] = $this->m_invoicenonbpjs->get_tagihanakomodasi($no_invoice);
		
		$this->load->view('base/operator/template', $data);	
	}

	public function get_master_dept(){
		$result = $this->m_invoicenonbpjs->get_master_dept();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_all_master_dept(){
		$result = $this->m_invoicenonbpjs->get_all_master_dept();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function create_tagihanakomodasi(){
		$no_invoice = $_POST['no_invoice'];
		$sub_visit = $_POST['sub_visit'];

		$tindakan = $this->m_invoicenonbpjs->get_dataakomodasi($sub_visit);

		foreach ($tindakan as $value) {
			$value['no_invoice'] = $no_invoice;
			$value['jumlah'] = $value['tarif'];
			
			$insert = $this->m_invoicenonbpjs->insert_tagihanakomodasi($value);
		}

		$result = $this->m_invoicenonbpjs->get_tagihanakomodasi($no_invoice);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function create_tagihankamar(){
		$no_invoice = $_POST['no_invoice'];
		$sub_visit = $_POST['sub_visit'];

		$tindakan = $this->m_invoicenonbpjs->get_datakamar($sub_visit);

		foreach ($tindakan as $value) {
			$value['no_invoice'] = $no_invoice;			
			
			$insert = $this->m_invoicenonbpjs->insert_tagihankamar($value);
		}

		$result = $this->m_invoicenonbpjs->get_tagihankamar($no_invoice);

		$final = [];
		$i = 0;
		foreach ($result as $data) {

			$startTimeStamp = strtotime($data['tgl_masuk']);
			$endTimeStamp = strtotime($data['tgl_keluar']);

			$timeDiff = abs($endTimeStamp - $startTimeStamp);

			$numberDays = $timeDiff/86400;  // 86400 seconds in one day
			$mod = abs(($timeDiff % 86400)/3600);

			// and you might want to convert to integer
			$numberDays = intval($numberDays);
			$mod = intval($mod);
			
			$data['waktu'] = $numberDays." Hari ".$mod." Jam";
			$data['hari'] = $numberDays;

			$final[$i] = $data;
			$i++;
		}

		header('Content-Type: application/json');
		echo json_encode($final);
	}

	public function get_paket_makan(){
		$result = $this->m_invoicenonbpjs->get_paket_makan();

		header('Content-type: application/json');
		echo json_encode($result);
	}
	
	public function hapus_tagihanmakan($id){
		$temp = $this->m_invoicenonbpjs->get_datatagihan($id);
		$no_invoice = $temp['no_invoice'];
		$makan_id = $temp['makan_id'];

		$input = $this->m_invoicenonbpjs->hapus_permintaan($makan_id);
		$input = $this->m_invoicenonbpjs->hapus_tagihanakomodasi($id);

		$result = $this->m_invoicenonbpjs->get_tagihanakomodasi($no_invoice);
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}
}
