<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );


class Homeoperasi extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_homeoperasi");
		$this->load->model("rawatjalan/m_homerawatjalan");
		$this->load->model('logistik/m_gudangbarang');
		$this->load->model('bersalin/m_homebersalin');
		$this->load->model('rekammedis/m_olahrekammedis');
		$this->load->model('farmasi/m_obat');
		$data['page_title'] = "Kamar Operasi";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'daftar/list';
		// load template
		$data['content'] = 'home';
		$data['javascript'] = 'javascript/j_home';
		$data['listorder'] = $this->m_homeoperasi->get_listorder();
		$data['listoperasi'] = $this->m_homeoperasi->get_listoperasi();
		$data['listriwayat'] = $this->m_homeoperasi->get_listriwayat();
		$dept = $this->m_homerawatjalan->get_dept_id("KAMAR OPERASI");
		$data['dept_id'] = $dept['dept_id'];
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($dept['dept_id']);
		$data['obatunit'] = $this->m_homerawatjalan->get_obat_unit($dept['dept_id']);

		$this->load->view('base/operator/template', $data);
	}

	public function get_listorder(){
		$result = $this->m_homeoperasi->get_listorder();

		header('Content-type: application/json');
		echo json_encode($result);		
	}

	public function search_operasi(){
		$key = $_POST['search'];

		$result = $this->m_homeoperasi->search_operasi($key);

		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function search_listriwayat(){
		$key = $_POST['search'];

		$result = $this->m_homeoperasi->search_listriwayat($key);

		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function get_dnama(){
		$d1 = $_POST['d2'];
		$d2 = $_POST['d3'];
		$d3 = $_POST['d4'];
		$d4 = $_POST['d5'];

		if($d1!=""){
			$nama1 = $this->m_homeoperasi->get_dnama($d1);
			$result['nama1'] = $nama1['diagnosis_nama'];
		}else{
			$result['nama1'] = "";
		}

		if($d2!=""){
			$nama2 = $this->m_homeoperasi->get_dnama($d2);
			$result['nama2'] = $nama2['diagnosis_nama'];
		}else{
			$result['nama2'] = "";
		}

		if($d3!=""){
			$nama3 = $this->m_homeoperasi->get_dnama($d3);
			$result['nama3'] = $nama3['diagnosis_nama'];
		}else{
			$result['nama3'] = "";
		}

		if($d4!=""){
			$nama4 = $this->m_homeoperasi->get_dnama($d4);
			$result['nama4'] = $nama4['diagnosis_nama'];
		}else{
			$result['nama4'] = "";
		}

		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function hapus_order($id){
        $input = $this->m_homeoperasi->hapus_order_operasi($id);

        $key = $_POST['search'];
        $result = $this->m_homeoperasi->search_operasi($key);

        header('Content-Type:application/json');
		echo(json_encode($result));
    }

    public function submit_operasi($visit_id){
		foreach ($_POST as $value) {
			$insert = $value;
		}

		$tgl = $this->date_db($insert['waktu_rencana']);
		$insert['waktu_rencana'] = $tgl;

		$id = $this->m_homeoperasi->get_last_order_operasi($visit_id);

		if($id){
			$vir = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vir) == "1") {
				$vir = '0'. $vir;
			}
			$insert['rencana_id'] = "OR".$visit_id."".($vir);
		}else{
			$insert['rencana_id'] = "OR".$visit_id."01";
		}

		$in = $this->m_homeoperasi->submit_operasi($insert);

		$update['status'] = 'RENCANA';
		$up = $this->m_homeoperasi->update_status($insert['order_operasi_id'], $update);

		if($in && $up)
			return true;
		return false;
	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}

	public function save_resep(){
		foreach ($_POST as $value) {
			$insert = $value;
		}

		$visit_id = $insert['visit_id'];
		$id = $this->m_homeoperasi->get_last_visit_resep($insert['visit_id']);
		if($id){
			$vir = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vir) == "1") {
				$vir = '000'. $vir;
			}else if(strlen($vir) == "2"){
				$vir = '00' . $vir;
			}else if (strlen($vir) == "3") {
				$vir = '0' . $vir;
			}
			$insert['resep_id'] = "RE".$visit_id."".($vir);
		}else{
			$insert['resep_id'] = "RE".$visit_id."0001";
		}

		$tgl = $this->fdate_db($insert['tanggal']);
		$insert['tanggal'] = $tgl;
		$hasil = $this->m_homeoperasi->save_visit_resep($insert);
		
		header('Content-Type:application/json');
		echo(json_encode($insert));
	}

	public function get_resep($sub){
		$result = $this->m_homeoperasi->get_resep($sub);

		header('Content-Type: application/json');
		echo(json_encode($result));
	}

	public function search_listoperasi(){
		$key = $_POST['search'];
		$result = $this->m_homeoperasi->search_listoperasi($key);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_tagihan(){
		$search = $_POST['search'];

		$result = $this->m_homeoperasi->search_tagihan($search);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function submit_permintaan_bersalin($value='')
	{
		$this->form_validation->set_rules('no_permintaan', 'nomor permitaan', 'required|trim|xss_clean|is_unique[obat_permintaan.no_permintaan]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaan'] = $_POST['no_permintaan'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$dept = $this->m_homerawatjalan->get_dept_id("KAMAR OPERASI");
			$insert['dept_id'] = $dept['dept_id'];

			$val = $_POST['data'];
			$result = $this->m_homerawatjalan->insert_permintaan($insert);
			if($result){
				foreach ($val as $key) {
					$ins['obat_id'] = $key[1];
					$ins['obat_detail_id'] = $key[0];
					$ins['jumlah_request'] =  $key[9];
					$ins['obat_permintaan_id'] = $result;

					$elny = $this->m_homerawatjalan->insert_detail_permintaan($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_filter_farmasi()
	{
		$dept = $this->m_homerawatjalan->get_dept_id("KAMAR OPERASI");

		if (isset($_POST['filterby'])) {
			$filterby = $_POST['filterby'];
			$filterval = $_POST['valfilter'];
			
			$result = $this->m_homerawatjalan->filter_farmasi($filterby,$filterval, $dept['dept_id']);			
		}else if (isset($_POST['expired'])) {
			$filterby = $_POST['expired'];
			$now = date('Y-m-d');
			$result = $this->m_homerawatjalan->filter_farmasi_expired($filterby,$now,$dept['dept_id']);
		}

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function get_obat_retur()
	{
		$katakunci = $_POST['katakunci'];
		$dept = $this->m_homerawatjalan->get_dept_id("KAMAR OPERASI")['dept_id'];
		$result = $this->m_homebersalin->get_obat_farmasi_unit($katakunci, $dept);

		header('Content-Type: application/json');
	 	echo json_encode($result); 
	}

	public function submit_retur_bersalin()
	{
		$this->form_validation->set_rules('no_returdept', 'Nomor Retur', 'required|trim|xss_clean|is_unique[obat_retur_dept.no_returdept]');
		$this->form_validation->set_message('is_unique', 'Nomor Retur sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['status'] = 'belum diterima';
			$insert['no_returdept'] = $_POST['no_returdept'];
			$dept = $this->m_homerawatjalan->get_dept_id("KAMAR OPERASI");
			$insert['dept_id'] = $dept['dept_id'];
			$insert['petugas_input'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['keterangan'] = $_POST['keterangan'];
			$tgl =  DateTime::createFromFormat('d/m/Y H:i',$_POST['waktu']);
			$insert['waktu'] = $tgl->format('Y-m-d H:i');

			$val = $_POST['data'];

			$id = $this->m_homerawatjalan->submit_retur_dept($insert);
			if ($id) {
				foreach ($val as $key) {
					$ins['retur_dept_id'] = $id;
					$ins['obat_detail_id'] = $key[0];
					$ins['jumlah'] = $key[8];

					$res = $this->m_homerawatjalan->insert_detail_returdept($ins);
					//ubah stok di gudang dan unit, yang ubah gudang bukan unit :D
				}

				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_permintaan_barangunit($value='')
	{
		$this->form_validation->set_rules('no_permintaanbarang', 'nomor permitaan', 'required|trim|xss_clean|is_unique[barang_permintaan.no_permintaanbarang]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaanbarang'] = $_POST['no_permintaanbarang'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$dept = $this->m_homerawatjalan->get_dept_id("KAMAR OPERASI");
			$insert['dept_id'] = $dept['dept_id'];

			$val = $_POST['data'];
			$result = $this->m_homerawatjalan->insert_permintaanbarang($insert);
			if($result){
				foreach ($val as $key) {
					$ins['barang_id'] = $key[8];
					$ins['barang_stok_id'] = $key[7];
					$ins['jumlah_request'] =  $key[9];
					$ins['barang_permintaan_id'] = $result;

					$elny = $this->m_homerawatjalan->insert_detail_permintaanbarang($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function excel_barang_unit()
	{
		$dept_id = $this->input->post('my_dept_id');
		$dept_id = $this->m_homerawatjalan->get_dept_id("KAMAR OPERASI")['dept_id'];
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($dept_id);
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];
		$data['awal'] = date('d F Y');

		$this->load->view('bersalin/excel_inventori',$data);
	}
}
