<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Invoicebpjs extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_invoicenonbpjs");
		$this->load->model("m_invoicebpjs");
		$data['page_title'] = "Invoice BPJS";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		redirect('laboratorium/homelab');
	}

	public function invoice($no_invoice){
		$data['content'] = 'tagihan/invoicebpjs';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$invoice = $this->m_invoicenonbpjs->get_visit_id($no_invoice);
		$visit_id = $invoice['visit_id'];
		$sub_visit = $invoice['sub_visit'];
		$data['visit_id'] = $visit_id;
		$data['sub_visit'] = $invoice['sub_visit'];
		$data['no_invoice'] = $no_invoice;
		$data['invoice'] = $invoice;
		$data['dept_id'] = $this->m_invoicenonbpjs->get_deptid('KAMAR OPERASI');

		$pasien = $this->m_invoicenonbpjs->get_data_pasien($visit_id);
		$data['pasien'] = $pasien;
		
		$data['tindakan'] = $this->m_invoicenonbpjs->get_tindakan($no_invoice);

		$temp = $this->m_invoicebpjs->get_tagihantunjang($sub_visit);
		$data['tagihantunjang'] = $temp;

		$kelas = $invoice['kelas_pelayanan'];
		$i = 0;
		$insert = [];
		foreach ($temp as $value) {
			$nama = $value['nama_tindakan'];
			$bpjs = $this->m_invoicebpjs->get_tindakanbpjs($nama , $kelas);
			$value['tarif_bpjs'] = intval($bpjs['js'])+intval($bpjs['jp'])+intval($bpjs['bakhp']);
			$value['tarif'] = intval($value['js'])+intval($value['jp'])+intval($value['bakhp']);
			$value['selisih'] = $value['tarif']-$value['tarif_bpjs'];

			$insert[$i++] = $value;
		}

		$data['tagihantunjang'] = $insert;
		$data['tagihanadmisi'] = $this->m_invoicebpjs->get_tagihanadmisi($no_invoice);

		$this->load->view('base/operator/template', $data);
	}

	public function get_master_dept(){
		$result = $this->m_invoicenonbpjs->get_master_dept();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_master_tindakan(){
    	$result = $this->m_invoicenonbpjs->get_master_tindakan();

    	header('Content-Type: application/json');
    	echo json_encode($result);	
    }

    public function get_tariftindakan(){
    	$id = $_POST['nama'];
    	$kelas = $_POST['kelas'];

    	$result = $this->m_invoicenonbpjs->get_tariftindakan($id, $kelas);

    	header('Content-Type: application/json');
    	echo json_encode($result);
    }

    public function save_tagihan(){
    	foreach($_POST as $value){
    		$insert = $value;
    	}

    	$tanggal = $this->sdate_db($insert['waktu']);
    	$insert['waktu'] = $tanggal;

    	$insert['id'] = intval($this->m_invoicenonbpjs->get_last_id())+1;

    	$in = $this->m_invoicenonbpjs->save_tagihan($insert);

    	header('Content-Type: application/json');
    	echo json_encode($insert);
    }

    public function hapus_tindakan($id){
		$temp = $this->m_invoicenonbpjs->get_datatagihan($id);
		$no_invoice = $temp['no_invoice'];

		$input = $this->m_invoicenonbpjs->hapus_tagihan($id);

		$result = $this->m_invoicenonbpjs->get_tindakan($no_invoice);
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function sdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}
}
?>


