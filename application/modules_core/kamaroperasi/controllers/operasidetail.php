<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Operasidetail extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_operasidaftarkan");
	}

	public function index($page = 0)
	{
		redirect('laboratorium/homelab');
	}

	public function detail($operasi_id, $order_id){
		$data['content'] = 'detailoperasi';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$v = $this->m_operasidaftarkan->get_dataoperasi($order_id);
		$visit_id = $v['visit_id'];
		$sub_visit = $v['sub_visit'];
		$data['dataoperasi'] = $v;
		$data['order_id'] = $order_id;
		$data['operasi_id'] = $operasi_id;

		$data['detailoperasi'] = $this->m_operasidaftarkan->get_detailoperasi($operasi_id);

		$this->load->view('base/operator/template', $data);
	}

	public function cetak($operasi_id, $order_id){
		$data['content'] = 'detailoperasi';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$v = $this->m_operasidaftarkan->get_dataoperasi($order_id);
		$visit_id = $v['visit_id'];
		$sub_visit = $v['sub_visit'];
		$data['dataoperasi'] = $v;

		$data['detailoperasi'] = $this->m_operasidaftarkan->get_detailoperasi($operasi_id);

		$this->load->helper('pdf_helper');
	  	$this->load->view('kamaroperasi/cetak_operasi', $data);
	}

}
