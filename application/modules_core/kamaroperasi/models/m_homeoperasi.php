<?php
	class m_homeoperasi extends CI_Model {


		public function search_operasi($key){
			$sql = "SELECT v.tanggal_visit,v.visit_id, sv.sub_visit, v.rm_id, p.nama, pt.nama_petugas, md.nama_dept, o.order_operasi_id, ov.diagnosa1, d.diagnosis_nama, ov.over_id, ov.diagnosa1, ov.diagnosa2, ov.diagnosa3, ov.diagnosa4, ov.diagnosa5, md.jenis, o.waktu_mulai FROM master_diagnosa d, visit v, (SELECT rj_id as sub_visit FROM visit_rj UNION SELECT ri_id as sub_visit FROM visit_ri UNION SELECT igd_id as sub_visit FROM visit_igd) sv, pasien p, petugas pt, master_dept md, order_operasi o, (SELECT max(id) as over_id, visit_id, rj_id as sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_klinik GROUP BY visit_id  UNION ALL SELECT max(id) as over_id, visit_id, sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_igd  GROUP BY visit_id UNION ALL SELECT max(kunjungan_dok_id) as over_id, visit_id, sub_visit, diagnosa_utama, sekunder1, sekunder2, sekunder3, sekunder4 FROM visit_perawatan_dokter  GROUP BY visit_id) ov WHERE o.visit_id = v.visit_id AND v.rm_id = p.rm_id AND o.dept_id = md.dept_id AND o.pengirim = pt.petugas_id AND o.sub_visit = sv.sub_visit AND o.sub_visit = ov.sub_visit AND ov.diagnosa1 = d.diagnosis_id AND (p.nama LIKE '%$key%' OR p.rm_id like '%$key%') AND o.status='LIST'";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function get_listorder(){
			$sql = "SELECT v.tanggal_visit,v.visit_id, sv.sub_visit, v.rm_id, p.nama, pt.nama_petugas, md.nama_dept, o.order_operasi_id, ov.diagnosa1, d.diagnosis_nama, ov.over_id, ov.diagnosa1, ov.diagnosa2, ov.diagnosa3, ov.diagnosa4, ov.diagnosa5, md.jenis, o.waktu_mulai FROM master_diagnosa d, visit v, (SELECT rj_id as sub_visit FROM visit_rj UNION SELECT ri_id as sub_visit FROM visit_ri UNION SELECT igd_id as sub_visit FROM visit_igd) sv, pasien p, petugas pt, master_dept md, order_operasi o, (SELECT max(id) as over_id, visit_id, rj_id as sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_klinik GROUP BY visit_id  UNION ALL SELECT max(id) as over_id, visit_id, sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_igd  GROUP BY visit_id UNION ALL SELECT max(kunjungan_dok_id) as over_id, visit_id, sub_visit, diagnosa_utama, sekunder1, sekunder2, sekunder3, sekunder4 FROM visit_perawatan_dokter  GROUP BY visit_id) ov WHERE o.visit_id = v.visit_id AND v.rm_id = p.rm_id AND o.dept_id = md.dept_id AND o.pengirim = pt.petugas_id AND o.sub_visit = sv.sub_visit AND o.sub_visit = ov.sub_visit AND ov.diagnosa1 = d.diagnosis_id AND o.status='LIST' ORDER BY o.waktu_mulai ASC";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function get_listoperasi(){
			$sql = "SELECT v.tanggal_visit,v.visit_id, r.waktu_rencana, sv.sub_visit, v.rm_id, p.nama, pt.nama_petugas, md.nama_dept, o.order_operasi_id, ov.diagnosa1, d.diagnosis_nama, ov.over_id, ov.diagnosa1, ov.diagnosa2, ov.diagnosa3, ov.diagnosa4, ov.diagnosa5, md.jenis, o.waktu_mulai, r.rencana_id FROM master_diagnosa d, visit v, operasi_rencana r ,(SELECT rj_id as sub_visit FROM visit_rj UNION SELECT ri_id as sub_visit FROM visit_ri UNION SELECT igd_id as sub_visit FROM visit_igd) sv, pasien p, petugas pt, master_dept md, order_operasi o, (SELECT max(id) as over_id, visit_id, rj_id as sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_klinik GROUP BY visit_id  UNION ALL SELECT max(id) as over_id, visit_id, sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_igd  GROUP BY visit_id UNION ALL SELECT max(kunjungan_dok_id) as over_id, visit_id, sub_visit, diagnosa_utama, sekunder1, sekunder2, sekunder3, sekunder4 FROM visit_perawatan_dokter  GROUP BY visit_id) ov WHERE o.visit_id = v.visit_id AND v.rm_id = p.rm_id AND o.dept_id = md.dept_id AND o.pengirim = pt.petugas_id AND o.sub_visit = sv.sub_visit AND o.sub_visit = ov.sub_visit AND ov.diagnosa1 = d.diagnosis_id AND r.order_operasi_id = o.order_operasi_id AND o.status='RENCANA' ORDER BY r.waktu_rencana ASC";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function search_listoperasi($key){
			$sql = "SELECT v.tanggal_visit,v.visit_id, r.waktu_rencana, sv.sub_visit, v.rm_id, p.nama, pt.nama_petugas, md.nama_dept, o.order_operasi_id, ov.diagnosa1, d.diagnosis_nama, ov.over_id, ov.diagnosa1, ov.diagnosa2, ov.diagnosa3, ov.diagnosa4, ov.diagnosa5, md.jenis, o.waktu_mulai, r.rencana_id FROM master_diagnosa d, visit v, operasi_rencana r ,(SELECT rj_id as sub_visit FROM visit_rj UNION SELECT ri_id as sub_visit FROM visit_ri UNION SELECT igd_id as sub_visit FROM visit_igd) sv, pasien p, petugas pt, master_dept md, order_operasi o, (SELECT max(id) as over_id, visit_id, rj_id as sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_klinik GROUP BY visit_id  UNION ALL SELECT max(id) as over_id, visit_id, sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_igd  GROUP BY visit_id UNION ALL SELECT max(kunjungan_dok_id) as over_id, visit_id, sub_visit, diagnosa_utama, sekunder1, sekunder2, sekunder3, sekunder4 FROM visit_perawatan_dokter  GROUP BY visit_id) ov WHERE o.visit_id = v.visit_id AND v.rm_id = p.rm_id AND o.dept_id = md.dept_id AND o.pengirim = pt.petugas_id AND o.sub_visit = sv.sub_visit AND o.sub_visit = ov.sub_visit AND ov.diagnosa1 = d.diagnosis_id AND r.order_operasi_id = o.order_operasi_id AND (p.nama LIKE '%$key%' OR p.rm_id LIKE '%$key') AND o.status='LIST' ORDER BY r.waktu_rencana ASC";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function get_listriwayat(){
			$sql = "SELECT v.tanggal_visit,v.visit_id, r.waktu_rencana, sv.sub_visit, v.rm_id, p.nama, pt.nama_petugas, md.nama_dept, o.order_operasi_id, ov.diagnosa1, d.diagnosis_nama, ov.over_id, ov.diagnosa1, ov.diagnosa2, ov.diagnosa3, ov.diagnosa4, ov.diagnosa5, md.jenis, ol.waktu_mulai, ol.waktu_selesai, r.rencana_id, ol.operasi_id FROM master_diagnosa d, visit v, operasi_rencana r , operasi_laporan ol,(SELECT rj_id as sub_visit FROM visit_rj UNION SELECT ri_id as sub_visit FROM visit_ri UNION SELECT igd_id as sub_visit FROM visit_igd) sv, pasien p, petugas pt, master_dept md, order_operasi o, (SELECT max(id) as over_id, visit_id, rj_id as sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_klinik GROUP BY visit_id  UNION ALL SELECT max(id) as over_id, visit_id, sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_igd  GROUP BY visit_id UNION ALL SELECT max(kunjungan_dok_id) as over_id, visit_id, sub_visit, diagnosa_utama, sekunder1, sekunder2, sekunder3, sekunder4 FROM visit_perawatan_dokter  GROUP BY visit_id) ov WHERE o.visit_id = v.visit_id AND v.rm_id = p.rm_id AND o.dept_id = md.dept_id AND ol.dokter_bedah = pt.petugas_id AND r.rencana_id = ol.rencana_id AND o.sub_visit = sv.sub_visit AND o.sub_visit = ov.sub_visit AND ol.diagnosa_post = d.diagnosis_id AND r.order_operasi_id = o.order_operasi_id AND o.status='SELESAI' ORDER BY ol.waktu_mulai ASC";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function search_listriwayat($key){
			$sql = "SELECT v.tanggal_visit,v.visit_id, r.waktu_rencana, sv.sub_visit, v.rm_id, p.nama, pt.nama_petugas, md.nama_dept, o.order_operasi_id, ov.diagnosa1, d.diagnosis_nama, ov.over_id, ov.diagnosa1, ov.diagnosa2, ov.diagnosa3, ov.diagnosa4, ov.diagnosa5, md.jenis, ol.waktu_mulai, ol.waktu_selesai, r.rencana_id, ol.operasi_id FROM master_diagnosa d, visit v, operasi_rencana r , operasi_laporan ol,(SELECT rj_id as sub_visit FROM visit_rj UNION SELECT ri_id as sub_visit FROM visit_ri UNION SELECT igd_id as sub_visit FROM visit_igd) sv, pasien p, petugas pt, master_dept md, order_operasi o, (SELECT max(id) as over_id, visit_id, rj_id as sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_klinik GROUP BY visit_id  UNION ALL SELECT max(id) as over_id, visit_id, sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_igd  GROUP BY visit_id UNION ALL SELECT max(kunjungan_dok_id) as over_id, visit_id, sub_visit, diagnosa_utama, sekunder1, sekunder2, sekunder3, sekunder4 FROM visit_perawatan_dokter  GROUP BY visit_id) ov WHERE o.visit_id = v.visit_id AND v.rm_id = p.rm_id AND o.dept_id = md.dept_id AND ol.dokter_bedah = pt.petugas_id AND r.rencana_id = ol.rencana_id AND o.sub_visit = sv.sub_visit AND o.sub_visit = ov.sub_visit AND ol.diagnosa_post = d.diagnosis_id AND r.order_operasi_id = o.order_operasi_id AND  (p.nama LIKE '%$key%' OR p.rm_id LIKE '%$key') AND o.status='SELESAI' ORDER BY ol.waktu_mulai ASC";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function get_dnama($id){
			$sql = "SELECT * FROM master_diagnosa WHERE diagnosis_id = '$id' LIMIT 1";
			$query = $this->db->query($sql);
			$result = $query->row_array();
			return $result;
		}

		public function hapus_order_operasi($id){
	        $result = $this->db->delete('order_operasi',array('order_operasi_id'=>$id));
	        return $result;
    	}

    	public function submit_operasi($value){
    		$query = $this->db->insert('operasi_rencana',$value);
	        if ($query) {
	            return true;
	        }else{
	            return false;
	        }
    	}

    	public function update_status($id, $data){
    		$this->db->where('order_operasi_id', $id);
	        $update = $this->db->update('order_operasi', $data);

	        if($update)
	            return true;
	        else
	            return false;
    	}

    	public function get_last_order_operasi($visit_id){
	        $sql = "SELECT max(r.rencana_id) as value from operasi_rencana r, order_operasi o WHERE r.order_operasi_id = o.order_operasi_id AND o.visit_id = '$visit_id'";
	        $query = $this->db->query($sql);
	        if ($query) {
	            return $query->row_array();
	        }else{
	            return false;
	        }
	    }

	    public function get_last_visit_resep($visit_id){
	        $sql = "SELECT max(resep_id) as value from visit_resep WHERE visit_id = '$visit_id'";
	        $query = $this->db->query($sql);
	        if ($query) {
	            return $query->row_array();
	        }else{
	            return false;
	        }
	    }

	    public function save_visit_resep($value=''){
	        $query = $this->db->insert('visit_resep',$value);
	        if ($query) {
	            return true;
	        }else{
	            return false;
	        }
	    }

	    public function get_resep($sub){
	    	$sql = "SELECT * FROM visit_resep v, petugas p WHERE v.dokter = p.petugas_id AND sub_visit = '$sub'";
	    	$query = $this->db->query($sql);
	    	$result = $query->result_array();
	    	return $result;
	    }

	    public function search_tagihan($search){
    	$sql = "SELECT *, t.cara_bayar as carapembayaran FROM tagihan t, pasien p, visit v, master_dept m, (SELECT substr(sub_visit, 1, 2) as departemen, no_invoice FROM tagihan) sb
    			WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND sb.departemen = m.dept_id
    			AND sb.no_invoice = t.no_invoice AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '%$search%')
    			";

    	$query = $this->db->query($sql);
    	return $query->result_array();
    }
	}
?>