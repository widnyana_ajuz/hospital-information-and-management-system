<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->setPageOrientation('P');
$title = "RUMAH SAKIT DATU SANGGUL RANTAU";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData('logo-login-backup.png', '11px', $title, "Detail Operasi \n".date('Y'));
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();

	//begin the content rendering
    $content = '
    <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-top: solid 1px #000;
			border-left: solid 1px #000;
			border-right: solid 1px #000;
			border-bottom: solid 1px #000;
			font-size: 8pt;
			vertical-align:middle;
			line-height:20px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			font-weight: bold;
		}
		.center {
			text-align:center;
		}
		.italic {
			font-style:italic;
		}
    </style>
	<!-- Hasil Evaluasi Kelas -->
	<div class="hasil_kelas">
	<br>
	<table>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="50%" colspan="2">
			<h3>Info Pasien</h3>
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="50%" colspan="2">
			<h3>Info Visit</h3>
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			No Rekam Medis
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['rm_id'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Tanggal
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.date("d F Y H:i", strtotime($dataoperasi['tanggal_visit'])).'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Nama Pasien
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['nama'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Diagnosa
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['diagnosis_nama'].'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Jenis Kelamin
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['jenis_kelamin'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Dokter
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['nama_petugas'].'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Golongan Darah
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['gol_darah'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Tempat Diagnosa
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['tempat_operasi'].'
			</td>
		</tr>
	</table>

	<br><br>
	<table>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="50%" colspan="2">
			<h3>Detail Pelaksanaan Operasi</h3>
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="50%" colspan="2">
			
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			No Order
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['rencana_id'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Tanggal Rencana
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.date("d F Y H:i", strtotime($dataoperasi['waktu_rencana'])).'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Pengirim
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['nama_petugas'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Keterangan
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dataoperasi['alasan'].'
			</td>
		</tr>
	</table>

	<br><br>
	<table>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="50%" colspan="2">
			<h3>Laporan Operasi</h3>
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="50%" colspan="2">
			
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Ruangan 
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['ruangan'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Dokter Anak	
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['nama_anak'].'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Dokter Ahli Bedah
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['nama_bedah'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Asisten
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['asisten'].'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Dokter Anestesi
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['nama_anestesi'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			OnLop
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['onlop'].'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Instrument
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['instrumen'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Lingkup Operasi
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['lingkup_operasi'].'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Diagnosa Pra Bedah
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['diagnosa_pra'].' - '.$detailoperasi['diag_pra'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Diagnosa Post Bedah
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['diagnosa_post'].' - '.$detailoperasi['diag_post'].'
			</td>
		</tr>

		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Waktu Mulai Operasi
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.date("d F Y H:i", strtotime($detailoperasi['waktu_mulai'])).'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Waktu Selesai Operasi
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.date("d F Y H:i", strtotime($detailoperasi['waktu_selesai'])).'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Jenis Operasi
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['jenis_operasi'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Jenis Jaringan yang Dikirim
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['jenis_jaringan_kirim'].'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			PA Jaringan
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['pa_jaringan'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Uraian Tindakan
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$detailoperasi['uraian_tindakan'].'
			</td>
		</tr>
	</table>

	<br><br><br><br><br><br><br><br>
	
	<table>
		<tbody>
			<tr>
				<td style="border-top:none;border-right:none;border-left:none;border-bottom:none">
				</td>
				<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none;text-align:center">
					Petugas Paramedis <br>
					<br><br>
					( .................................... )
				</td>
			</tr>
		</tbody>
	</table>
	';
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('Laporan_Invoice.pdf', 'I');
?>

<!-- <table class="table" id="hasil-evaluasi-dosen">
			<tbody>
				<tr>
					<td width="40%" style="text-align:center"><b>Nama Tindakan</b></td>
					<td width="20%" style="text-align:center"><b>Unit</b></td>
					<td width="20%" style="text-align:center"><b>Waktu</b></td>
					<td width="20%" style="text-align:center"><b>Subtotal</b></td>
				</tr>
				'.$baris.'
				<tr>
					<td colspan="3" style="text-align:right"><b>Total Tagihan Pendukung</b></td>
					<td style="text-align:right">'.number_format($total,2,".",",").'</td>
				</tr>
			</tbody>
		</table> -->
