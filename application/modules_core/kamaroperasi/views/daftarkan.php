<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kamaroperasi/homeoperasi">Kamar Operasi</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kamaroperasi/operasidaftarkan">Daftarkan - nama Pasien</a>
	</li>
</div>

<input type="hidden" id="visit_id" value="<?php echo $dataoperasi['visit_id'];?>">
<input type="hidden" id="sub_visit" value="<?php echo $dataoperasi['sub_visit'];?>">
<input type="hidden" id="order_id" value="<?php echo $dataoperasi['order_operasi_id'];?>">
<input type="hidden" id="rencana_id" value="<?php echo $dataoperasi['rencana_id'];?>">

<div class="navigation1" style="min-height:800px;border-radius:5px;" >
		<div style="padding-top:10px"></div>

		<div class="dropdown">
			<div id="titleInformasi">Form Pengisian Detail Antrian Kamar Operasi</div>
		</div>
		<br>
		
        <form class="form-horizontal" role="form" id="submit_laporan">
        <div class="informasi">
            <table width="100%">
            	<tr>
            		<td width="50%">
            			<fieldset class="fsStyle">
							<legend>
				                Info Pasien
							</legend>
							<div class="form-group">
								<label class="control-label col-md-5">No. Rekam Medis</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="norm" name="norm" placeholder="No Rekam Medis" readonly value="<?php echo $dataoperasi['rm_id'] ?>">
								</div>	
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-5">Nama Pasien</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="npas" name="npas" placeholder="Nama" readonly value="<?php echo $dataoperasi['nama'] ?> ">
								</div>	
							</div>
							<div class="form-group">
								<label class="control-label col-md-5">Jenis Kelamin</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="jk" name="jk" placeholder="Jenis Kelamin" readonly value="<?php echo $dataoperasi['jenis_kelamin'] ?>">
								</div>	
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-5">Golongan Darah</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="goldara" name="goldara" placeholder="golongan Darah" readonly value="<?php echo $dataoperasi['gol_darah']; ?>">
								</div>	
							</div>
						</fieldset>
            		</td>
            		<td width="50%">
            			<fieldset class="fsStyle">
							<legend>
				                Info Visit
							</legend>
							<div class="form-group">
								<label class="control-label col-md-5">Tanggal</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="tglvst" name="tglvst" placeholder="12 Mei 2012" readonly value="<?php 
									$tgl = strtotime($dataoperasi['tanggal_visit']);
									$hasil = date('d F Y');
									echo $hasil; ?>">
								</div>	
							</div>
							<div class="form-group">
								<label class="control-label col-md-5">Diagnosa</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="diagnosa" name="diagnosa" readonly value="<?php echo $dataoperasi['diagnosis_nama']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-5">Dokter</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="nmdok" name="nmdok" placeholder="Nama Dokter" readonly value="<?php echo $dataoperasi['nama_petugas']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-5">Tempat Diagnosa</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="tmptdiag" name="tmptdiag" placeholder="Tempat Diagnosa" readonly value="<?php echo $dataoperasi['tempat_operasi']; ?>">
								</div>
							</div>
						</fieldset>
            		</td>
            	</tr>
            	<tr>
            		<td colspan="2">
            			<fieldset class="fsStyle">
							<legend>
				                Detail Pelaksanaan Operasi
							</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">No. Order</label>
										<div class="col-md-6">
											<input type="text" class="form-control" id="noor" name="noor" placeholder="No Order" readonly value="<?php echo $dataoperasi['rencana_id'];?>">
										</div>	
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5" style="text-align:left">Tanggal Rencana</label>
										<div class="col-md-6">
											<input type="text" class="form-control" id="tglren" name="tglren" placeholder="10 Mei 2012" readonly value="<?php 
											$tgl = strtotime($dataoperasi['waktu_rencana']);
											$hasil = date('d F Y H:i');
											echo $hasil; ?>">
										</div>	
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Pengirim</label>
										<div class="col-md-6">
											<input type="text" class="form-control" id="pengirim" name="pengirim" placeholder="Pemgirim" readonly value="<?php echo $dataoperasi['nama_petugas']; ?>">
										</div>	
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5" style="text-align:left">Keterangan</label>
										<div class="col-md-6">
											<textarea class="form-control" id="ket" name="ket" placeholder="Keterangan" readonly><?php echo $dataoperasi['alasan'];?></textarea> 
										</div>	
									</div>
								</div>
							</div>
						</fieldset>
            		</td>
            	</tr>
            	<tr>
            		<td colspan="2">
            			<fieldset class="fsStyle">
							<legend>
				                Laporan Operasi
							</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Ruangan</label>
										<div class="col-md-6">
											<input type="text" class="form-control" id="lap_ruangan" name="ruangan" placeholder="Ruangan">
										</div>	
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group" id="specialisasi">
										<label class="control-label col-md-5" style="text-align:left">Specialisasi Kamar Operasi</label>
										<div class="col-md-6">
											<select class="form-control select" name="special" id="lap_specialisasi" >
												<option value="Bedah" selected>Bedah</option>
												<option value="Obstetrik & Ginekologi" >Obstetrik & Ginekologi</option>
												<option value="Bedah Saraf">Bedah Saraf</option>
												<option value="THT">THT</option>
												<option value="Mata">Mata</option>
												<option value="Kulit & Kelamin">Kulit & Kelamin</option>
												<option value="Gigi & Mulut">Gigi & Mulut</option>
												<option value="Bedah Anak">Bedah Anak</option>
												<option value="Kardiovaskuler">Kardiovaskuler</option>
												<option value="Bedah Orthopedi">Bedah Orthopedi</option>
												<option value="Thorak">Thorak</option>
												<option value="Digestive">Digestive</option>
												<option value="Urologi">Urologi</option>
												<option value="Lain-Lain">Lain-Lain</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Dokter Ahli Bedah</label>
										<div class="col-md-6">
											<input type="hidden" id="lap_idokterbedah"> 
											<input type="text" class="form-control" style="cursor:pointer;background-color:white" readonly id="lap_ndokterbedah" placeholder="Dokter Bedah" data-toggle="modal" data-target="#searchDokter">
										</div>	
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5"  style="text-align:left">Dokter Anak</label>
										<div class="col-md-6">
											<input type="hidden" id="lap_idokteranak">
											<input type="text" class="form-control" style="cursor:pointer;background-color:white" readonly id="lap_ndokteranak" placeholder="Dokter Anak" data-toggle="modal" data-target="#searchDokter">
										</div>	
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Dokter Anastesi</label>
										<div class="col-md-6">
											<input type="hidden" id="lap_idokteranastesi">
											<input type="text" class="form-control" style="cursor:pointer;background-color:white" readonly id="lap_ndokteranastesi" placeholder="Dokter Anastesi" data-toggle="modal" data-target="#searchDokter">
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group" id="group-asisten">
										<label class="control-label col-md-5" style="text-align:left">Asisten</label>
										<div class="col-md-6">
											<input type="text" class="form-control nama-asisten" id="namaasisten" name="namaasisten" placeholder="Nama Asisten">
											<a href="#" id="tambah-asisten" style="margin-top:10px; font-size:10pt;"><i class="fa fa-plus"></i>Tambah Asisten</a>
										</div>	
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Instrument</label>
										<div class="col-md-6">
											<input type="text" class="form-control" id="lap_instrument" name="inst" placeholder="Instrument">
										</div>	
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5" style="text-align:left">OnLop</label>
										<div class="col-md-6">
											<input type="text" class="form-control" id="lap_onlop" name="onlop" placeholder="OnLop">
										</div>	
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Diagnosa Pra Bedah</label>
										<div class="col-md-2">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian" id="kode_pra" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian" id="nama_pra" name="prabedah" placeholder="Nama Diagnosa" data-toggle="modal" data-target="#searchDiagnosa" readonly>
										</div>	
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5" style="text-align:left" >Diagnosa Post Bedah</label>
										<div class="col-md-2">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian" id="kode_post" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian" id="nama_post" name="postbedah" placeholder="Nama Diagnosa" data-toggle="modal" data-target="#searchDiagnosa" readonly>
										</div>	

									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Waktu Mulai Operasi</label>
										<div class="col-md-6" >
											<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input type="text" style="cursor:pointer;background-color:white" id="lap_datestart" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy - hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y - H:i");?>">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5" style="text-align:left">Waktu Selesai Operasi</label>
										<div class="col-md-6" >
											<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input type="text" style="cursor:pointer;background-color:white" id="lap_dateend" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy - hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y - H:i");?>">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group" id="jenisOperasi">
										<label class="control-label col-md-5">Jenis Operasi</label>
										<div class="col-md-6">
											<select class="form-control select" name="jarak" id="lap_jenis">
												<option value="" selected>Pilih</option>
												<option value="kecil" >Kecil</option>
												<option value="sedang" >Sedang</option>
												<option value="besar" >Besar</option>
												<option value="khususs" >Khusus</option>
											</select>
										</div>	
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group" id="group-dokter">
										<label class="control-label col-md-5" style="text-align:left">Lingkup Operasi</label>
										<div class="col-md-6">
											<select class="form-control select" name="jarak" id="lap_lingkup">
												<option value="Elektif" selected>Elektif</option>
												<option value="Emergency" >Emergency</option>
												
											</select>
										</div>	
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">PA Jaringan</label>
										<div class="col-md-6">
											<textarea class="form-control" name="pajaringan" id="lap_pajaringan" placeholder="Keterangan"></textarea>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5" style="text-align:left;">Jenis Jaringan yang Dikirim</label>
										<div class="col-md-6">
											<textarea class="form-control" name="jaringankirim" id="lap_jenisjaringan" placeholder="Keterangan"></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Uraian Tindakan</label>
										<div class="col-md-6">
											<textarea class="form-control" name="uraiantindakan" id="lap_uraian" placeholder="Keterangan"></textarea>
										</div>
									</div>
								</div>
							</div>

							
						</fieldset>
            		</td>
            	</tr>
            	
            </table>				
			
		</div>
			<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
			<div style="margin-left:80%">
				<span class="customSpan">
					<button type="reset" class="btn btn-warning">RESET</button>
				 	<button type="submit" id="simpanOver" class="btn btn-success">SIMPAN</button>	
				</span>
			</div>

			<div class="modal fade" id="penolong" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Penolong </h3>
	        			</div>
	        			<div class="modal-body">
		        			<div class="form-group">
								<div class="form-group">	
									<div class="col-md-5" style="margin-left:15px;">
										<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama Obat"/>
									</div>
									<div class="col-md-2">
										<button type="button" class="btn btn-info">Cari</button>
									</div>
									<br><br>	
								</div>		
								<div style="margin-left:10px; margin-right:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 30px 0px 20px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelpenolong">
										<thead>
											<tr class="info">
												<th>Nama Penolong</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Khrisna</td>
												<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
											</tr>
											<tr>
												<td>Abadi</td>
												<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
											</tr>

										</tbody>
									</table>												
								</div>
								<div style="margin-left:10px; margin-right:10px;"><hr></div>
								<div class="form-group">	
									<div class="col-md-4" style="margin-left:15px;">
										<input type="text" class="form-control" name="new" id="new" placeholder="Tambah Baru"/>
									</div>
									<div class="col-md-2">
										<button type="button" class="btn btn-success" style="width:150px;">Tambah Baru</button>
									</div>
									<br>	
								</div>	
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>	

		</form>
		
		<div class="modal fade" id="searchDokter" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Dokter</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">
						<form method="POST" id="submit_dokter">
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="dokter_katakunci" placeholder="Nama dokter"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div>	
						</form>
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th>Nama Dokter</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_dokter">
									<tr><td colspan="2"><center>Cari Data Dokter</center></td></tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="searchDokteranak" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Dokter Anak</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">	
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama dokter"/>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-info">Cari</button>
							</div>	
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th>Nama Dokter</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Jems</td>
										<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
									</tr>
									<tr>
										<td>Putu</td>
										<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
									</tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="searchDokteranastesi" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Dokter Anastesi</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">	
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama dokter"/>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-info">Cari</button>
							</div>	
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th>Nama Dokter</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Jems</td>
										<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
									</tr>
									<tr>
										<td>Putu</td>
										<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
									</tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="searchDiagnosa" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Diagnosa</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">	
						<form method="POST" id="submit_diagnosa">
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="diag_katakunci" placeholder="Kata kunci"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div>	
						</form>
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" style="table-layout:fixed" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th width="25%;">Kode Diagnosa</th>
										<th>Keterangan</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_diagnosa">
									<tr><td colspan="3"><center>Cari Data Diagnosa</center></td></tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

		<br>
	
</div>
