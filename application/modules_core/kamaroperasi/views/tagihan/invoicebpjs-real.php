<div class="title">
	KAMAR OPERASI - INVOICE PASIEN NON BPJS
</div>
<div class="bar">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kamaroperasi/homeoperasi">Kamar Operasi</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kamaroperasi/invoicenonbpjs">Invoice - Nama Pasien</a>
	</li>
</div>

<input type="hidden" id="visit_id" value="<?php echo $visit_id; ?>"/>
<input type="hidden" id="sub_visit" value="<?php echo $sub_visit; ?>"/>
<input type="hidden" id="no_invoice" value="<?php echo $no_invoice; ?>"/>
<input type="hidden" id="dept_id" value="<?php echo $dept_id; ?>"/>
<input type="hidden" id="kelas_pelayanan" value="<?php echo $invoice['kelas_pelayanan']; ?>"/>
<div class="backregis">
	<div id="my-tab-content" class="tab-content">
			
		<div class="informasi">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group" style="font-size:16px;">
							<label class="control-label1 col-md-4 nama">Nomor Invoice</label>
							<div class="col-md-4 nama">: <?php echo $no_invoice; ?> </div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Jenis Kunjungan</label>
							<div class="col-md-5">: <?php echo $pasien['tipe_kunjungan']; ?></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Visit ID</label>
							<div class="col-md-5">:	<?php echo $visit_id ?> </div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Kelas Perawatan</label>
							<div class="col-md-5">: Kelas <?php echo $invoice['kelas_perawatan'] ?></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Tanggal Invoice</label>
							<div class="col-md-5">: 
								<?php 
								$tgl = strtotime($invoice['tanggal_invoice']);
								$hasil = date('d F Y', $tgl); 
								echo $hasil;?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Tanggal Kunjungan</label>
							<div class="col-md-5">: <?php
								$tgl = strtotime($pasien['tanggal_visit']);
								$hasil = date('d F Y', $tgl); 
								echo $hasil;
							?></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Nomor Rekam Medis</label>
							<div class="col-md-5">:  <?php echo $pasien['rm_id']; ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Cara Bayar</label>
							<div class="col-md-5">: <?php echo $invoice['cara_bayar']; ?></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Nama Pasien</label>
							<div class="col-md-5">:  <?php echo $pasien['nama']; ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Nomor BPJS</label>
							<div class="col-md-5">: <?php echo $invoice['no_asuransi']; ?></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Alamat</label>
							<div class="col-md-5">: <?php echo $pasien['alamat_skr']; ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Kelas Pelayanan</label>
							<input type="hidden" value="<?php echo $invoice['kelas_pelayanan']; ?>" id="kelas_pelayanan">
							<div class="col-md-5">: Kelas <?php echo $invoice['kelas_pelayanan']; ?></div>
						</div>
					</div>
				</div>

		</div>

		<hr class="garis">

		<form class="form-horizontal" role="form" id="submitTagihan">
			<div id="tagihadmisi">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Admisi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Admisi Tertagih</th>
									<th>Waktu </th>
									<th>Tarif</th>
								</tr>
							</thead>
							<tbody id="tbody_resep">
								<?php
									if(!empty($tagihanadmisi)){
										$tgl = strtotime($tagihanadmisi['waktu']);
										$hasil = date('d F Y - H:i', $tgl);

										echo'
										<tr>
											<td>1</td>
											<td><center>'.$tagihanadmisi['nama_tindakan'].'</center></td>
											<td><center>'.$hasil.'</center></td>
											<td><center>'.number_format($tagihanadmisi['tarif'],0,'','.').'</center></td>
										</tr>
										';
									}else{
										echo'
											<tr>
												<td colspan="4" align="center">Tidak Terdapat Tagihan Admisi</td>
											</tr>
										';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>
			
			<div id="tagihankamar" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Kamar</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihankamar">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Kamar Tertagih</th>
									<th>Masuk dan Keluar</th>
									<th>Lama</th>
									<th>Tarif</th>
									<th>Tarif BPJS</th>
									<th>Selisih</th>
									<th>On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttkamar">
								<tr>
									<td colspan="9" align="center">Tidak Terdapat Tagihan Kamar</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihanakomodasi" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Makan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanakomodasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Akomodasi Tertagih</th>
									<th>Unit</th>
									<th>Jumlah</th>
									<th>Tarif</th>
									<th>Tarif BPJS</th>
									<th>Selisih</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttakomodasi">
								<tr>
									<td colspan="9" align="center">Tidak Terdapat Tagihan Makan</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihantindakanperawatan" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Perawatan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanperawatan">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Perawatan Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th>Tarif BPJS</th>
									<th>Selisih</th>
									<th>On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttperawatan">
								<tr>
									<td colspan="9" align="center">Tidak Terdapat Tagihan Tindakan Perawatan</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtindakanpenunjang" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Penunjang</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihanpenunjang">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Penunjang Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th>Tarif BPJS</th>
									<th>Selisih</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttpenunjang">
								<?php
										$no = 0;
										if(!empty($tagihantunjang)){
											foreach ($tagihantunjang as $data) {
												$tgl = strtotime(substr($data['waktu'], 0, 10));
												$hasil = date('d F Y', $tgl); 

												echo '
													<tr>
														<td>'.++$no.'</td>
														<td>'.$data['nama_tindakan'].'
															<input type="hidden" class="tpenunjang_id" value="'.$data['tindakan_penunjang_id'].'">
															<input type="hidden" class="vpenunjang_id" value="'.$data['penunjang_detail_id'].'">
														</td>
														<td>'.$data['nama_dept'].'</td>
														<td>'.$hasil.'</td>
														<td style="text-align:right;">'.number_format((intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])),0,'','.').'</td>
														<td style="text-align:right;">'.number_format($data['tarif_bpjs'],0,'','.').'</td>
														<td style="text-align:right;">'.number_format($data['selisih'],0,'','.').'</td>
														<td style="text-align:right;">
															<input type="hidden" class="inputtarif" value="'.$data['selisih'].'">
															<input type="hidden" class="inputtarifbpjs" value="'.$data['tarif_bpjs'].'">
															<input type="hidden" class="inputtarifreal" value="'.$data['tarif'].'">
															'.number_format($data['on_faktur'],0,'','.').'
															<input type="hidden" class="inputtotal">
														</td>
														<td style="text-align:right;" class="t_total">'.number_format(intval($data['selisih'])+intval($data['on_faktur']),0,'','.').'</td>
													</tr>
												';
											}
										}else{
											echo '
												<tr>
													<td colspan="9" align="center">Tidak Terdapat Tagihan Tindakan Penunjang</td>
												</tr>
											';
										}
									?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtagihantindakanoperasi">
				<div id="titleInformasi" style="margin-bottom:-40px;"><a href="#modalttoperasi" data-toggle="modal" style="text-align:left;margin-left:-10px;font-size:12pt;color:white"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan Tindakan Operasi">&nbsp;Tambah Tagihan Tindakan Operasi</i></a>
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Operasi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanoperasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Operasi Tertagih</th>
									<th>Lingkup Operasi</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th>Tarif BPJS</th>
									<th>Selisih</th>
									<th>On Faktur</th>
									<th>Total</th>
									<th width="50">Action</th>
								</tr>
							</thead>
							<tbody id="tbody_ttoperasi">
								<?php	
									if(!empty($tindakan)){
										echo'<input type="hidden" id="jml_table" value="no">';
										$no = 0;
										foreach ($tindakan as $data) {
											echo '
												<tr>
													<td align="center">'.++$no.'</td>
													<td>'.$data['nama_tindakan'].'</td>
													<td>'.$data['lingkup_operasi'].'</td>
													<td style="text-align:center;">'.$data['waktu'].'</td>
													<td style="text-align:right;">'.$data['tarif'].'</td>
													<td style="text-align:right;">'.$data['tarif_bpjs'].'</td>
													<td style="text-align:right;">'.$data['selisih'].'</td>
													<td style="text-align:right;">'.$data['on_faktur'].'</td>
													<td style="text-align:right;">'.(intval($data['selisih'])+intval($data['on_faktur'])).'</td>
													<td style="text-align:center">
														<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'.$data['id'].'">
														<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
													</td>
												</tr>
											';	
										}
									}else{
								?>
								<tr>
									<?php echo'<input type="hidden" id="jml_table" value="yes">'; ?>
									<td colspan="10" align="center">Tidak Terdapat Tagihan Tindakan Operasi</td>
								</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div style="margin-right:40px;">
				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="totaltagihan" style="font-size:1.8em;margin-top:-10px;">
							<?php
							$total = 0;
							foreach ($tindakan as $data) {
								$total += (intval($data['selisih'])+intval($data['on_faktur']));
							}
							
							echo $total;
							echo '</label>';
							echo '<input type="hidden" id="jumlahsemua" value = "'.$total.'">';
							?>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Total Tagihan (Rp.) : 
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" style="font-size:1.8em;margin-top:-10px;">0</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Deposit (Rp.) : 
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="kekurangan" style="font-size:1.8em;margin-top:-10px;">
							<?php
							$total = 0;
							foreach ($tindakan as $data) {
								$total += (intval($data['selisih'])+intval($data['on_faktur']));
							}

							echo $total;
							?>
						</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Kekurangan (Rp.) : 
					</div>
				</div>
			</div>

			<div class="pull-right" style="margin-right:40px;">
				<br>
				<button type="submit" class="btn btn-info">CETAK</button>
				<a href="<?php echo base_url() ?>kasirtindakan/tambahinvoice" class="btn btn-warning">KEMBALI</a>
				<button type="reset" class="btn btn-danger">BATAL</button>
   				<!-- <button type="submit" class="btn btn-success">SIMPAN</button> -->
   				<button type="submit" class="btn btn-info">BAYAR</button>
			</div>
			<br><br>
		</form>

		<div class="modal fade" id="modalttoperasi" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Tindakan Perawatan</h3>
				   			</div>
							<div class="modal-body">
								<div class="informasi">
					   				<div class="form-group">
										<label class="control-label col-md-4">Waktu Tindakan</label>
										<div class="col-md-5">	
											<input type="text" id="tin_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
										</div>
				        			</div>
				        			<div class="form-group">
									<label class="control-label col-md-4">Lingkup Operasi</label>
										<div class="col-md-5">	
											<select class="form-control" name="kelas_tindakan" id="lingkup">
												<option value="">Pilih Lingkup Operasi</option>
												<option value="Elektif">Elektif</option>
												<option value="Emergency">Emergency</option>
											</select>
										</div>
									</div>			
				        			<div class="form-group">
										<label class="control-label col-md-4">Tindakan</label>
										<div class="col-md-6">
											<input type="hidden" id="idtindakan_klinik">
											<input type="hidden" id="idtindakdetail">
											<textarea class="form-control" id="namatindakan" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Tindakan" ></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-4">Kelas Pelayanan</label>
										<div class="col-md-5">	
											<select class="form-control" name="kelas_tindakan" id="kelas_klinik">
												<option value="">Pilih Kelas</option>
												<option value="Kelas VIP">VIP</option>
												<option value="Kelas Utama">Utama</option>
												<option value="Kelas I">Kelas I</option>
												<option value="Kelas II">Kelas II</option>
												<option value="Kelas III">Kelas III</option>
											</select>
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Tarif</label>
										<div class="col-md-5">	
											<input type="hidden" id="js_klinik">
											<input type="hidden" id="jp_klinik">
											<input type="hidden" id="bakhp_klinik">
											<input type="text" class="form-control" id="tarif" name="tarif" placeholder="Tarif" readonly > 
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Tarif BPJS</label>
										<div class="col-md-5">	
											<input type="text" class="form-control" id="tarif_bpjs" name="tarif" placeholder="Tarif BPJS" readonly > 
										</div>
				        			</div>
				        			
				        			<div class="form-group">
										<label class="control-label col-md-4">Selisih</label>
										<div class="col-md-5">	
											<input type="text" class="form-control" id="selisih" name="Selisih" placeholder="Selisih" readonly > 
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">On Faktur</label>
										<div class="col-md-5">	
											<input type="number" class="form-control" id="onfaktur" name="onfaktur" placeholder="On Faktur" >
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Jumlah</label>
										<div class="col-md-5">	
											<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" readonly>
										</div>
				        			</div>

									<div class="form-group">
										<label class="control-label col-md-4">Paramedis</label>
										<div class="col-md-5">	
											<input type="hidden" id="paramedis_id">
											<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
										</div>
				        			</div>
				        			
			        			</div>
		       				</div>
			        		<br><br>
			        		<div class="modal-footer">
			        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
						    </div>
						</div>
					</div>
				</form>
			</div>

		<br><br><br>	
	</div>
</div>


<script type="text/javascript">
	$(window).ready(function(event){
		var nomor = {};
		nomor['no_invoice'] = $('#no_invoice').val();
		nomor['sub_visit'] = $('#sub_visit').val();
		nomor['kelas'] = $('#kelas_pelayanan').val();

		console.log(nomor);
		$.ajax({
			type:'POST',
			data:nomor,
			url:'<?php echo base_url();?>rawatjalan/invoicebpjs/create_tagihan',
			success:function(data){
				console.log(data);

				jumlahtable = data.length;

				if(data.length!=0){
					var no = 0;
					$('#tbody_ttperawatan').empty();
					for(var i = 0 ; i<data.length; i++){
						no++;
						$('#tbody_ttperawatan').append(
							'<tr>'+
								'<td>'+no+'</td>'+
								'<td>'+data[i]['nama_tindakan']+'</td>'+
								'<td>'+data[i]['nama_dept']+'</td>'+
								'<td style="text-align:center;">'+data[i]['waktu']+'</td>'+
								'<td style="text-align:right;">'+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['tarif_bpjs'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['selisih'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['total'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'</tr>'
						);

						total += Number(data[i]['total']);
						kekurangan += Number(data[i]['total']);
					}
				}else{
					$('#tbody_ttperawatan').empty();
					$('#tbody_ttperawatan').append(
						'<tr><td colspan="10" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
					);
				}

				// kekurangan -= deposit;
				// $('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
				// $('#deposit').text(deposit);
				// $('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

			}, error:function(data){
				console.log(data);
			}
		});

		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#paramedis_id').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
		
		var autodata = [];
		var iddata = [];
		$('#namatindakan').focus(function(){
			var $input = $('#namatindakan');
			
			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>kamaroperasi/invoicenonbpjs/get_master_tindakan',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_tindakan']);
							iddata.push(data[i]['tindakan_id']);
						}
					}
				});
			}

			$input.typeahead({source:autodata, 
	        autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = autodata.indexOf(current);

			    $('#idtindakan_klinik').val(iddata[index]);			    

			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

		});

		$('#kelas_klinik').change(function(){
			var item = {};
			item['kelas'] = $(this).val();
			item['nama'] = $('#idtindakan_klinik').val();
			var tarif = 0;
			var tarifbpjs = 0;

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>kamaroperasi/invoicenonbpjs/get_tariftindakan',
				success:function(data){
					var lingkup = $('#lingkup').val();
					var persen = 0;

					$('#idtindakdetail').val(data['detail_id']);

					if(lingkup == 'Elektif'){
						persen = 0;
					}else{
						persen = 25/100;
					}

					tarif = (Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))+((Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))*persen);
					
					$('#js_klinik').val(data['js']);
					$('#jp_klinik').val(data['jp']);
					$('#bakhp_klinik').val(data['bakhp']);
					$('#tarif').val(tarif);

					var itembpjs = {};
					itembpjs['kelas'] = "Kelas "+$('#kelas_pelayanan').val();
					itembpjs['nama'] = $('#idtindakan_klinik').val();

					$.ajax({
						type:'POST',
						data:itembpjs,
						url:'<?php echo base_url() ?>kamaroperasi/invoicenonbpjs/get_tariftindakan',
						success:function(data){
							var lingkup = $('#lingkup').val();
							var persen = 0;

							$('#idtindakdetail').val(data['detail_id']);

							if(lingkup == 'Elektif'){
								persen = 0;
							}else{
								persen = 25/100;
							}

							tarifbpjs = (Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))+((Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))*persen);

							$('#tarif_bpjs').val(tarifbpjs);

							var nonbpjs = Number($('#tarif').val());
							var total = tarif-tarifbpjs;
							if(total<0)
								total=0;

							$('#jumlah').val(total);
							$('#selisih').val(total);
						}
					});
				}
			});

		});

		$('#onfaktur').keyup(function(){
			var tarif = $('#selisih').val();
			var onfaktur = $(this).val();
			var jumlah = Number(tarif)+Number(onfaktur);
			console.log(tarif);
			$('#jumlah').val(jumlah);
		});

		$('#onfaktur').change(function(){
			var tarif = $('#selisih').val();
			var onfaktur = $(this).val();
			var jumlah = Number(tarif)+Number(onfaktur);
			
			$('#jumlah').val(jumlah);
		});

		$('#submitTindakan').submit(function(event){
			event.preventDefault();

			var item = {};
			item[1] = {};
			item[1]['no_invoice'] = $('#no_invoice').val();
			item[1]['tindakan_id'] = $('#idtindakdetail').val();
			item[1]['lingkup_operasi'] = $('#lingkup').val();
			item[1]['dept_id'] = $('#dept_id').val();
			item[1]['waktu'] = $('#tin_date').val();
			item[1]['tarif'] = $('#tarif').val();
			item[1]['tarif_bpjs'] = $('#tarif_bpjs').val();
			item[1]['selisih'] = $('#selisih').val();
			item[1]['on_faktur'] = $('#onfaktur').val();
			item[1]['petugas_input'] = $('#paramedis_id').val();
			var petugas = $('#paramedis').val();
			var tindakan = $('#namatindakan').val();
			var jumlah = $('#jumlah').val();
			
			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>kamaroperasi/invoicenonbpjs/save_tagihan',
				success:function(data){
					console.log(data);
					myAlert('Data Berhasil Ditambahkan');

					$(':input','#submitTindakan')
					  .not(':button, :submit, :reset')
					  .val('');
					$('#tin_date').val("<?php echo date('d/m/Y H:i') ?>");

					var no = ($('#tbody_ttoperasi').children('tr').length)+1;

					if($('#jml_table').val() == "yes"){
						$('#tbody_ttoperasi').empty();
						no = 1;
					}

					$('#tbody_ttoperasi').append(
						'<tr>'+
							'<td align="center">'+no+'</td>'+
							'<td>'+tindakan+'</td>'+
							'<td>'+data['lingkup_operasi']+'</td>'+
							'<td style="text-align:center;">'+data['waktu']+'</td>'+
							'<td style="text-align:right;">'+data['tarif']+'</td>'+
							'<td style="text-align:right;">'+data['tarif_bpjs']+'</td>'+
							'<td style="text-align:right;">'+data['selisih']+'</td>'+
							'<td style="text-align:right;">'+data['on_faktur']+'</td>'+
							'<td style="text-align:right;">'+jumlah+'</td>'+
							'<td style="text-align:center">'+
								'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data['id']+'">'+
								'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
							'</td>'+
						'</tr>'
					);

					var total = $('#jumlahsemua').val();
					var baru = Number(total)+Number(jumlah);

					$('#modalttoperasi').modal('hide');
					$('#jumlahsemua').val(baru);
					$('#totaltagihan').text(baru);
					$('#kekurangan').text(baru);
				}
			});
		});

		$(document).on('click','.hapusTindakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#visit_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>kamaroperasi/invoicenonbpjs/hapus_tindakan/"+id,
				success:function(data){
					console.log(data);

					$('#tbody_ttoperasi').empty();

					var totaltagihan = 0;
					var kekurangan = 0;
					var total = 0;

					if(data.length!=0){
						var no = 0;

						for(var i = 0 ; i<data.length; i++){
							no++;
							total = 0;

							total = Number(data[i]['on_faktur'])+Number(data[i]['selisih']);

							$('#tbody_ttoperasi').append(
								'<tr>'+
									'<td align="center">'+no+'</td>'+
									'<td>'+data[i]['nama_tindakan']+'</td>'+
									'<td>'+data[i]['lingkup_operasi']+'</td>'+
									'<td style="text-align:center;">'+data[i]['waktu']+'</td>'+
									'<td style="text-align:right;">'+data[i]['tarif']+'</td>'+
									'<td style="text-align:right;">'+data[i]['tarif_bpjs']+'</td>'+
									'<td style="text-align:right;">'+data[i]['selisih']+'</td>'+
									'<td style="text-align:right;">'+data[i]['on_faktur']+'</td>'+
									'<td style="text-align:right;">'+total+'</td>'+
									'<td style="text-align:center">'+
										'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
										'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
									'</td>'+
								'</tr>'
							);
							totaltagihan += Number(total);
							kekurangan += Number(total);
						}

						$('#jumlahsemua').val(totaltagihan);

					}else{
						$('#tbody_ttoperasi').append(
							'<input type="hidden" id="jml_table" value="yes">'+
							'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
						);
					}

					$('#totaltagihan').text(totaltagihan);
					$('#kekurangan').text(totaltagihan);

				},
				error:function(data){
					console.log(data);
				}	
			});
		});

	});
</script>