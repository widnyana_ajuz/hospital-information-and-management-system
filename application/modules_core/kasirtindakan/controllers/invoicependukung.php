<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Invoicependukung extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_invoicependukung");
		$data['page_title'] = "Invoice Pendukung";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load template
		$data['content'] = 'invoicependukung';
		$this->load->view('base/operator/template', $data);
	}

	public function invoice_jenazah($no_invoice)
	{
		$data['content'] = 'invoicependukung';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;

		$jenazah = $this->m_invoicependukung->get_data_jenazah($no_invoice);
		$id = $jenazah['id'];
		$detail = $this->m_invoicependukung->get_detail_jenazah($id);
		$data['pendukung'] = $jenazah;
		$data['detail'] = $detail;
		$data['unit'] = 'Perawatan Jenazah';
		$data['unit2'] = 'jenazah';

		$this->load->view('base/operator/template', $data);
	}

	public function invoice_mediko($no_invoice)
	{
		$data['content'] = 'invoicependukung';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;

		$mediko = $this->m_invoicependukung->get_data_mediko($no_invoice);
		$id = $mediko['id'];
		$detail = $this->m_invoicependukung->get_detail_mediko($id);
		$data['pendukung'] = $mediko;
		$data['detail'] = $detail;
		$data['unit'] = 'Mediko Legal';
		$data['unit2'] = 'mediko';

		$this->load->view('base/operator/template', $data);
	}

	public function bayar_tagihan($no_invoice)
	{
		$update['is_bayar'] = 1;
		$update['tgl_bayar'] = date('Y-m-d H:i:s');

		//cek dari perawatan jenazah/mediko legal
		if (strtolower($_POST['unit']) == 'perawatan jenazah') {
			//update is bayar, tgl bayar di perawatan jenazah
			$this->m_invoicependukung->update_jenazah($no_invoice, $update);
		}elseif (strtolower($_POST['unit']) == 'mediko legal') {
			//update is bayar, tgl bayar di mediko legal
			$this->m_invoicependukung->update_mediko($no_invoice, $update);
		}
	}

	public function cetak_nota($unit, $no_invoice)
	{
		if (strtolower($unit == 'jenazah')) {
			$jenazah = $this->m_invoicependukung->get_data_jenazah($no_invoice);
			$id = $jenazah['id'];
			$detail = $this->m_invoicependukung->get_detail_jenazah($id);
			$data['pendukung'] = $jenazah;
			$data['detail'] = $detail;
			$data['unit'] = 'Perawatan Jenazah';

		}elseif ($unit == 'mediko') {
			$mediko = $this->m_invoicependukung->get_data_mediko($no_invoice);
			$id = $mediko['id'];
			$detail = $this->m_invoicependukung->get_detail_mediko($id);
			$data['pendukung'] = $mediko;
			$data['detail'] = $detail;
			$data['unit'] = 'Perawatan Jenazah';
		}

		$this->load->helper('pdf_helper');
	  $this->load->view('kasirtindakan/nota_pendukung', $data);
	}

}
