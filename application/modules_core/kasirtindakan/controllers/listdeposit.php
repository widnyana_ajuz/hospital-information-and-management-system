<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );


class Listdeposit extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_listdeposit");
		$data['page_title'] = "Deposit";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		// load template
		redirect(base_url().'kasirtindakan/tambahdeposit');
	}

	public function detail(){
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'listdeposit';
		
		$data['visit'] = $_POST['dep_visitid'];
		$data['rm_id'] = $_POST['dep_norm'];
		$data['nama'] = $_POST['dep_nama'];
		$data['alamat'] = $_POST['dep_alamat'];
		$data['kamar'] = $_POST['dep_kamar'];
		$data['deposit'] = $this->m_listdeposit->get_deposit($data['visit']);

		$this->load->view('base/operator/template', $data);
	}

	public function details($visit_id){
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'listdeposit';
		
		$datapasien = $this->m_listdeposit->get_datapasien($visit_id);
		$data['visit'] = $datapasien['visit_id'];
		$data['rm_id'] = $datapasien['rm_id'];
		$data['nama'] = $datapasien['nama'];
		$data['alamat'] = $datapasien['alamat_skr'];
		$data['kamar'] = $datapasien['nama_kamar'];
		$data['deposit'] = $this->m_listdeposit->get_deposit($visit_id);

		$this->load->view('base/operator/template', $data);
	}

	public function save_deposit(){
		foreach ($_POST as $data) {
			$insert = $data;
		}

		$insert['petugas'] = $this->user['petugas_id'];
		$insert['tanggal_deposit'] = $this->date_db($insert['tanggal_deposit']);
		$insert['id_deposit'] = ($this->m_listdeposit->get_depositid()+1);

		$in = $this->m_listdeposit->save_deposit($insert);

		$result = $insert;
		$result['nama_petugas'] = $this->user['user_name'];
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function hapus_deposit($deposit, $visit){
		$del = $this->m_listdeposit->hapus_deposit($deposit);

		$result = $this->m_listdeposit->get_deposit($visit);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}
}
