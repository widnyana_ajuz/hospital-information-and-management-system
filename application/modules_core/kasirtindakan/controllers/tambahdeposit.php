<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );


class Tambahdeposit extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_tambahdeposit");
		$data['page_title'] = "Deposit";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load fdf_add_template(fdf_document, newpage, filename, template, rename)
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'tambahdeposit';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}

	public function search_visit(){
		$search = $_POST['search'];

		$result = $this->m_tambahdeposit->search_visit($search);

		header('Content-Type: application/json');
		echo json_encode($result);
	}
}
