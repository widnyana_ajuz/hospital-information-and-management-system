<?php
class m_homekasirtindakan extends CI_Model {
	public function search_tagihan($search){
    	$sql = "SELECT *, t.cara_bayar as carapembayaran FROM tagihan t, pasien p, visit v, visit_rj vr, master_dept m 
    			WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.sub_visit = vr.rj_id AND vr.unit_tujuan = m.dept_id
    			AND vr.visit_id = v.visit_id AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '$search')
    			";

    	$query = $this->db->query($sql);
    	return $query->result_array();
    }

    public function get_listkasir(){
    	$sql = "SELECT v.visit_id, p.rm_id, p.nama, p.alamat_skr, t.cara_bayar as carapembayaran, t.no_invoice FROM tagihan t, pasien p, visit v
    			WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.visit_id = v.visit_id AND t.is_bayar = 0
    			";
    	$query = $this->db->query($sql);
    	return $query->result_array();
    }

    public function get_searchlistkasir($search){
        $sql = "SELECT v.visit_id, p.rm_id, p.nama, p.alamat_skr, t.cara_bayar as carapembayaran, t.no_invoice FROM tagihan t, pasien p, visit v
                WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.visit_id = v.visit_id AND t.is_bayar = 0 AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '$search')
                ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_riwayatkasir(){
        $sql = "SELECT v.visit_id, p.rm_id, p.nama, p.alamat_skr, t.cara_bayar as carapembayaran, t.no_invoice FROM tagihan t, pasien p, visit v
                WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.visit_id = v.visit_id AND t.is_bayar = 1
                ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_searchriwayatkasir($search){
        $sql = "SELECT v.visit_id, p.rm_id, p.nama, p.alamat_skr, t.cara_bayar as carapembayaran, t.no_invoice FROM tagihan t, pasien p, visit v
                WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.visit_id = v.visit_id AND t.is_bayar = 0 AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '$search')
                ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_listdeposit(){
        $sql = "SELECT * FROM (SELECT sum(jumlah) as jumlah, visit_id, tanggal_deposit FROM DEPOSIT GROUP BY visit_id) d, visit v, pasien p WHERE d.visit_id = v.visit_id AND v.rm_id = p.rm_id AND v.status_visit <> 'DONE'   
                ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function  search_deposit($search){
        $sql = "SELECT * FROM 
                ";

        $query = $this->db->query($sql);
        return $query->result_array();   
    }

    public function get_listpendukung()
    {
        $sql = "SELECT 'MEDIKO LEGAL' AS unit, no_invoice, nama, umur, jenis_kelamin, alamat, id, is_bayar
                FROM mediko_perawatan
                UNION
                SELECT 'PERAWATAN JENAZAH' AS unit, no_invoice, nama, umur, jenis_kelamin, alamat, id, is_bayar
                FROM jenazah_perawatan
                ORDER BY is_bayar, unit, no_invoice";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function search_pendukung($search){
        $sql = "SELECT 'MEDIKO LEGAL' AS unit, no_invoice, nama, umur, jenis_kelamin, alamat, id, is_bayar
                FROM mediko_perawatan
                WHERE nama LIKE '%$search%'
                UNION
                SELECT 'PERAWATAN JENAZAH' AS unit, no_invoice, nama, umur, jenis_kelamin, alamat, id, is_bayar
                FROM jenazah_perawatan
                WHERE nama LIKE '%$search%'
                ORDER BY is_bayar, unit, no_invoice";

        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
?>
