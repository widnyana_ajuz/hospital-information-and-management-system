<?php
class m_invoicependukung extends CI_Model
{
  public function get_data_jenazah($no_invoice)
  {
    $sql = "SELECT * FROM jenazah_perawatan WHERE no_invoice = '$no_invoice'";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
  }

  public function get_detail_jenazah($id){
		$sql = "SELECT perawatan_id, nama_tindakan, waktu, jpd.bakhp+jpd.js+jpd.jp AS tarif, on_faktur, total
            FROM jenazah_perawatan_detail jpd LEFT JOIN master_tindakan_detail mtd ON jpd.detail_tindakan_id=mtd.detail_id
            LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
            WHERE perawatan_id='$id'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

  public function get_data_mediko($no_invoice)
  {
    $sql = "SELECT * FROM mediko_perawatan WHERE no_invoice = '$no_invoice'";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
  }

  public function get_detail_mediko($id){
		$sql = "SELECT perawatan_id, nama_tindakan, waktu, mpd.bakhp+mpd.js+mpd.jp AS tarif, on_faktur, total
            FROM mediko_perawatan_detail mpd LEFT JOIN master_tindakan_detail mtd ON mpd.detail_tindakan_id=mtd.detail_id
            LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
            WHERE perawatan_id='$id'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

  public function update_jenazah($no_invoice, $update)
  {
      $this->db->where('no_invoice', $no_invoice);
      $update = $this->db->update('jenazah_perawatan', $update);
      if($update)
          return true;
      else
          return false;
  }

  public function update_mediko($no_invoice, $update)
  {
      $this->db->where('no_invoice', $no_invoice);
      $update = $this->db->update('mediko_perawatan', $update);
      if($update)
          return true;
      else
          return false;
  }

}

?>
