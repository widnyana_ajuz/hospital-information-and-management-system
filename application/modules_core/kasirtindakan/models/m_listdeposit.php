<?php
class m_listdeposit extends CI_Model {
	public function get_deposit($visit){
		$sql = "SELECT * FROM deposit d, petugas p WHERE d.petugas = p.petugas_id AND d.visit_id = '$visit' ";

		$query = $this->db->query($sql);
    	return $query->result_array();
	}


	public function save_deposit($input){
		$insert = $this->db->insert('deposit',$input);
		if ($insert) {
			return true;
		} else {
			return false;
		}
	}

	public function get_depositid(){
		$sql = "SELECT max(id_deposit) as id FROM deposit";

		$query = $this->db->query($sql);
    	return $query->row_array()['id'];
	}

	public function hapus_deposit($id){
        $result = $this->db->delete('deposit',array('id_deposit'=>$id));
        return $result;
    }

    public function get_datapasien($visit){
    	// $sql = "SELECT * FROM visit v, pasien p WHERE v.rm_id = p.rm_id AND d.visit_id = '$visit' ";
    	$sql = "SELECT * FROM visit v, pasien p, visit_ri vi, visit_inap_kamar vik, master_kamar mk WHERE v.rm_id = p.rm_id AND v.visit_id = vi.visit_id AND vi.ri_id = vik.ri_id AND vik.kamar_id = mk.kamar_id AND v.visit_id = '$visit' LIMIT 1";
		$query = $this->db->query($sql);
    	return $query->row_array();
    }
}
?>