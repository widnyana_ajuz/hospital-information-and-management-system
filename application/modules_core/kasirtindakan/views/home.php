<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kasirtindakan/homekasirtindakan">KASIR</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">List Invoice</a>
	</li>
</div>

<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	   	<li class="active"><a href="#list" class="cl" data-toggle="tab">List Invoice</a></li>
	   	<li><a href="#listPendukung" class="cl" data-toggle="tab">List Invoice Pendukung</a></li>
	    <li><a href="#riwayat" class="cl" data-toggle="tab">Riwayat Pembayaran</a></li>
	    <li><a href="#deposit" class="cl" data-toggle="tab">Deposit</a></li>
	  <!--   <li><a href="#export" class="cl" data-toggle="tab">Export INA-CBG's</a></li> -->
	</ul>

	<div id="my-tab-content" class="tab-content">

		<div class="tab-pane active" id="list">
			<form method="POST" id="search_submit_list">
		       	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">	
						<input type="text" id="input_slist" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        <a href="<?php echo base_url() ?>invoice/tambahinvoice" data-toggle="modal" class="btn btn-warning"> Tambah Invoice Baru</a>
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">LIST INVOICE</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_list">
						<thead>
							<tr class="info">
								<th style="text-align:center;width:20px;">No.</th>
								<th>Nomor Invoice</th>
								<th>Nomor Visit</th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Cara Bayar</th>
								<th style="text-align:center;width:25px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 1;
								foreach ($listkasir as $data) {
									echo'
										<tr>
											<td align="center">'.$no++.'</td>
											<td>'.$data['no_invoice'].'</td>
											<td>'.$data['visit_id'].'</td>	
											<td>'.$data['rm_id'].'</td>									
											<td>'.$data['nama'].'</td>
											<td>'.$data['alamat_skr'].'</td>
											<td>'.$data['carapembayaran'].'</td>
											<td style="text-align:center">
									';

									if($data['carapembayaran']=='BPJS')
										echo '<a href="'.base_url().'kasirtindakan/invoicebpjs/invoice/'.$data['no_invoice'].'"><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>';
									else
										echo '<a href="'.base_url().'kasirtindakan/invoicenonbpjs/invoice/'.$data['no_invoice'].'" ><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>';
									echo '	</td>										
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>			
			</div>
			</div>
			<br>
			<br>
			<br>
			<br> 
	    </div>

	    <div class="tab-pane" id="listPendukung">
			<form method="POST" id="form_search_pendukung">
		    	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">
						<input type="text" id="input_search_pendukung" class="form-control" placeholder="Masukkan Nama Pasien" autofocus>
			   		</div>
			    	<button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
				</div>
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px; margin-left: -50px;">LIST INVOICE PENDUKUNG</p>
			</div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">
				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_pendukung">
							<thead>
								<tr class="info">
									<th style="text-align:center;width:20px;">No.</th>
									<th>Unit</th>
									<th>Nomor Invoice</th>
									<th>Nama Pasien</th>
									<th>Umur</th>
									<th>Jenis Kelamin</th>
									<th>Alamat</th>
									<th>Status Bayar</th>
									<th style="text-align:center;width:25px;">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									if (!empty($listpendukung)) {
										$a = 0;
										foreach ($listpendukung as $value) {
											if ($value['unit'] == 'PERAWATAN JENAZAH') {
												$act = 'invoice_jenazah';
											}elseif ($value['unit'] == 'MEDIKO LEGAL') {
												$act = 'invoice_mediko';
											}

											if ($value['is_bayar'] == 0) {
												$stat = 'Belum Bayar';
											}elseif ($value['is_bayar'] == 1) {
												$stat = 'Sudah Bayar';
											}

											echo "<tr>
												<td align='right'>".(++$a)."</td>
												<td>".$value['unit']."</td>
												<td>".$value['no_invoice']."</td>
												<td>".$value['nama']."</td>
												<td align='right'>".$value['umur']." tahun</td>
												<td>".$value['jenis_kelamin']."</td>
												<td>".$value['alamat']."</td>
												<td>".$stat."</td>
												<td align='center'>
													<a href='".base_url()."kasirtindakan/invoicependukung/".$act."/".$value['no_invoice']."'><i class='glyphicon glyphicon-ok' data-toggle='tooltip' data-placement='top' title='Proses'></i></a>
												</td>
											</tr>";
										}
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
	  </div>
    	
	    <div class="tab-pane" id="riwayat">
	    	<form method="POST" id="search_submit_riwayat">
		       	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">		
						<input type="text" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus id="input_riwayat">
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">RIWAYAT INVOICE</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_riwayat_kasir">
						<thead>
							<tr class="info">
								<th style="text-align:center;width:20px;">No.</th>
								<!-- <th>Unit</th> -->
								<th>Nomor Invoice</th>
								<th>Nomor Visit</th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Cara Bayar</th>
								<th style="text-align:center;width:25px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 1;
								foreach ($listriwayat as $data) {
									echo'
										<tr>
											<td align="center">'.$no++.'</td>
											<td>'.$data['no_invoice'].'</td>
											<td>'.$data['visit_id'].'</td>	
											<td>'.$data['rm_id'].'</td>									
											<td>'.$data['nama'].'</td>
											<td>'.$data['alamat_skr'].'</td>
											<td>'.$data['carapembayaran'].'</td>
											<td style="text-align:center">
									';

									if($data['carapembayaran']=='BPJS')
										echo '<a href="'.base_url().'kasirtindakan/viewinvoicebpjs/invoice/'.$data['no_invoice'].'"><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>';
									else
										echo '<a href="'.base_url().'kasirtindakan/viewinvoicenonbpjs/invoice/'.$data['no_invoice'].'" ><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>';
									echo '	</td>										
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>			
			</div> 
			</div>
			<br>
			<br>
			<br>
			<br>
	    </div>

	    <div class="tab-pane" id="deposit">
	    	<form method="POST" id="search_submit">
		       	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">		
						<input type="text" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        <a href="<?php echo base_url() ?>kasirtindakan/tambahdeposit" data-toggle="modal" class="btn btn-warning"> Tambah Deposit Baru</a>
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">DEPOSIT</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:10px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -95px 0px">
					<table class="table table-striped table-bordered table-hover tableDTUtama" id="">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Unit</th>
								<th>#Rekam Medis</th>
								<th>Nomor Visit</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Tanggal Masuk</th>
								<th>Total Deposit</th>
								<th width="80">Action</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<?php
								$no = 1;
								foreach ($listdeposit as $data) {
									echo '
										<tr>
											<td align="center">'.$no++.'</td>
											<td>Bersalin</td>
											<td>'.$data['rm_id'].'</td>
											<td>'.$data['visit_id'].'</td>
											<td>'.$data['nama'].'</td>
											<td>'.$data['alamat_skr'].'</td>
											<td style="text-align:center">'.$data['tanggal_visit'].'</td>											
											<td style="text-align:right">'.$data['jumlah'].'</td>
											<td style="text-align:center">
												<a href="'.base_url().'kasirtindakan/listdeposit/details/'.$data['visit_id'].'">
													<i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Kelola deposit"></i>
												</a>
											</td>
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>
	    </div>

	</div>

</div>

<script type="text/javascript">
	$(window).ready(function(){
		$('#search_submit_riwayat').submit(function(e){
			e.preventDefault();

			var item = {};

			item['search'] = $('#input_riwayat').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?=base_url()?>kasirtindakan/homekasirtindakan/get_searchriwayatkasir',
				success:function(data){
					console.log(data);
					var r = $('#table_riwayat_kasir').DataTable();

					r.clear().draw();

					for(var i=0; i<data.length;i++){
						if(data[i]['carapembayaran']=='BPJS')
							var action = '<a href="<?=base_url()?>kasirtindakan/viewinvoicebpjs/invoice/'+data[i]['no_invoice']+'"><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>';
						else
							var action = '<a href="<?=base_url()?>kasirtindakan/viewinvoicenonbpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>';

						r.row.add([
							(i+1),
							data[i]['no_invoice'],
							data[i]['visit_id'],
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['alamat_skr'],
							data[i]['carapembayaran'],
							action
						]).draw();
					}
				},error:function(data){
					console.log(data);
				}
			});
		})


		$('#search_submit_list').submit(function(e){
			e.preventDefault();
			var item = {};

			item['search'] = $('#input_slist').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?=base_url()?>kasirtindakan/homekasirtindakan/get_searchlistkasir',
				success:function(data){
					console.log(data);
					var r = $('#table_list').DataTable();

					r.clear().draw();

					for(var i=0; i<data.length;i++){
						if(data[i]['carapembayaran']=='BPJS')
							var action = '<a href="<?=base_url()?>kasirtindakan/invoicebpjs/invoice/'+data[i]['no_invoice']+'"><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>';
						else
							var action = '<a href="<?=base_url()?>kasirtindakan/invoicenonbpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>';

						r.row.add([
							(i+1),
							data[i]['no_invoice'],
							data[i]['visit_id'],
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['alamat_skr'],
							data[i]['carapembayaran'],
							action
						]).draw();
					}
				},error:function(data){
					console.log(data);
				}
			});
		})
	})
</script>

