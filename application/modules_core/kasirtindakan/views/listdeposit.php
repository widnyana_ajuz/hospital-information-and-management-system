<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kasirtindakan/homekasirtindakan">DEPOSIT</a>
	</li>
</div>

<input type="hidden" value="<?php echo $visit; ?>" id="visit_id">
<div class="backregis" style="margin-top:30px;">
	<div id="my-tab-content" class="tab-content">

		<div class="informasi">		
			<form class="form-horizontal" method="POST" role="form">
	            <table width="95%" >
	            	<tr>
	            		<td width="50%">
	            			<fieldset class="fsStyle">
								<legend>
					                Informasi Pasien
								</legend>
								<br>
								<div class="form-group">
									<label class="control-label1 col-md-4">Nomor Rekam Medis</label>
									<div class="col-md-5 nama">
										: &nbsp;&nbsp;<?php echo $rm_id ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label1 col-md-4">Nama Pasien</label>
									<div class="col-md-5 nama">
										: &nbsp;&nbsp;<?php echo $nama ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label1 col-md-4">Alamat</label>
									<div class="col-md-5 nama">
										: &nbsp;&nbsp;<?php echo $alamat ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label1 col-md-4">Kamar Rawat Inap</label>
									<div class="col-md-5 nama">
										: &nbsp;&nbsp;<?php echo $kamar ?>
									</div>
								</div>
								<br>
							</fieldset>
	            		</td>
	            		<td width="50%">
	            			<fieldset class="fsStyle">
								<legend>
					                Riwayat Deposit
								</legend>

								<a href="#modaldeposit" data-toggle="modal" style="margin-left:5px;"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Deposit">&nbsp;Tambah Deposit</i></a>
				
						    	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover tableDTUtama" id="table_deposit">
										<thead>
											<tr class="info">
												<th width="20">No.</th>
												<th>Tanggal</th>
												<th>Jumlah</th>
												<th>Petugas</th>
												<th width="70">Delete</th>
											</tr>
										</thead>
										<tbody id="tbody_deposit">
											<?php
												$no = 0;
												foreach ($deposit as $data) {
													echo '
														<tr>
															<td width="20" align="center">'.++$no.'</td>
															<td style="text-align:center">'.$data['tanggal_deposit'].'</td>
															<td style="text-align:right">'.$data['jumlah'].'</td>
															<td>'.$data['nama_petugas'].'</td>
															<td style="text-align:center">
																<a style="cursor:pointer;" class="hapus"><input type="hidden" class="deposit_id" value="'.$data['id_deposit'].'">
																<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
															</td>
														</tr>
													';
												}
											?>
										</tbody>
									</table>
								</div>
							</fieldset>	
	            		</td>
	            	</tr>
	            </table>				
			</form>
	
		<br>
	</div>
</div>

<div class="modal fade" id="modaldeposit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-form-horizontal informasi" role="form" id="save_deposit">
			<div class="modal-content">
				<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    				<h3 class="modal-title" id="myModalLabel">Tambah Deposit</h3>
    			</div>
    			<div class="modal-body">
					<div class="informasi">
						<div class="form-group">
							<label class="control-label col-md-4">Tanggal Deposit</label>
							<div class="input-group col-md-4" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" id="tambah_date" style="cursor:pointer;" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4"> Jumlah Deposit </label>
							<div class="input-group col-md-4">
								<input type="text" id="tambah_deposit" class="form-control" name="jumlahdepo" placeholder="Jumlah Deposit">
							</div>
						</div>
					</div>
    			</div>
    			<div class="modal-footer">
			       		<button type="reset" class="btn btn-danger" data-dismiss="modal">Batal</button>
			       		<button type="submit" class="btn btn-success">Deposit</button>
		      	</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(window).ready(function(){
		$('#save_deposit').submit(function(e){
			e.preventDefault();
			var item = {};
			item[1] = {};

			item[1]['tanggal_deposit'] = $('#tambah_date').val();
			item[1]['jumlah'] = $('#tambah_deposit').val();
			item[1]['visit_id'] = $('#visit_id').val();

			console.log(item);
			$.ajax({
				type:'POST',
				data:item,
				url:'<?=base_url()?>kasirtindakan/listdeposit/save_deposit',
				success:function(data){
					console.log(data);
					var no = $('#tbody_deposit').children('tr').length;
					var t = $('#table_deposit').DataTable();
					var action = '<center><a style="cursor:pointer;" class="hapus"><input type="hidden" class="deposit_id" value="'+data['id_deposit']+'">';
						action+= '<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a></center>';

					t.row.add([
						(no+1),
						data['tanggal_deposit'],
						data['jumlah'],
						data['nama_petugas'],
						action
					]).draw();

					$('#tambah_deposit').val('');
					$('#modaldeposit').modal('hide');

				},error:function(data){
					alert('gagal');
					console.log(data);
				}
			});
		});

		$('#table_deposit').on('click','tr td .hapus', function(){
			var deposit = $(this).closest('tr').find('.deposit_id').val();	
			var visit = $('#visit_id').val();
			var con = confirm('Anda Yakin Ingin Menghapus Data?');

			if(con){
				$.ajax({
					type:'POST',
					url:'<?=base_url()?>kasirtindakan/listdeposit/hapus_deposit/'+deposit+'/'+visit,
					success:function(data){
						console.log(data);
						var t = $('#table_deposit').DataTable();
						t.clear().draw();
						for(var i=0; i<data.length; i++){
							var action = '<center><a style="cursor:pointer;" class="hapus"><input type="hidden" class="deposit_id" value="'+data[i]['id_deposit']+'">';
								action+= '<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a></center>';

							t.row.add([
								(i+1),
								data[i]['tanggal_deposit'],
								data[i]['jumlah'],
								data[i]['nama_petugas'],
								action
							]).draw();
						}
					}, error:function(data){
						console.log(data);
					}
				});
			}
		});
	})
</script>