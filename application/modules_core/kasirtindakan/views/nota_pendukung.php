<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->setPageOrientation('P');
$title = "RUMAH SAKIT DATU SANGGUL RANTAU";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData('logo-login-backup.png', '11px', $title, "NOTA TRANSAKSI TINDAKAN \n".date('Y'));
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
	$total = 0;

	$baris = '';
	if (!empty($detail)){

		foreach ($detail as $row){
				$baris .= '<tr>
							<td width="40%" style="text-align:left">'.$row['nama_tindakan'].'</td>
							<td width="20%" style="text-align:center">'.$unit.'</td>
							<td width="20%" style="text-align:center">'.date("d F Y H:i:s", strtotime($row['waktu'])).'</td>
							<td width="20%" style="text-align:right">'.number_format($row['total'],2,".",",").'</td>
					</tr>';
			$total += $row['total'];
		}
	}

	if ($pendukung['tgl_bayar']) {
		$tanggal = date('d/m/Y H:i:s',strtotime($pendukung['tgl_bayar']));
	}else{
		$tanggal = date('d/m/Y H:i:s');
	}


	//begin the content rendering
    $content = '
    <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-top: solid 1px #000;
			border-left: solid 1px #000;
			border-right: solid 1px #000;
			border-bottom: solid 1px #000;
			font-size: 8pt;
			vertical-align:middle;
			line-height:20px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			font-weight: bold;
		}
		.center {
			text-align:center;
		}
		.italic {
			font-style:italic;
		}
    </style>
	<!-- Hasil Evaluasi Kelas -->
	<div class="hasil_kelas">
	<br>
	<table>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="15%">
			<b>#No Transaksi</b><br>'.$pendukung['no_invoice'].'
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="20%">
			<b>Tanggal Transaksi</b><br>'.$tanggal.'
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="20%">
			<b>Nama Pasien</b><br>'.$pendukung['nama'].'
			</td>
		</tr>
		</table>

		<br><br>
		<table class="table" id="hasil-evaluasi-dosen">
			<tbody>
				<tr>
					<td width="40%" style="text-align:center"><b>Nama Tindakan</b></td>
					<td width="20%" style="text-align:center"><b>Unit</b></td>
					<td width="20%" style="text-align:center"><b>Waktu</b></td>
					<td width="20%" style="text-align:center"><b>Subtotal</b></td>
				</tr>
				'.$baris.'
				<tr>
					<td colspan="3" style="text-align:right"><b>Total Tagihan Pendukung</b></td>
					<td style="text-align:right">'.number_format($total,2,".",",").'</td>
				</tr>
			</tbody>
		</table>
	</div>
	<b>STATUS PEMBAYARAN</b> : &nbsp;&nbsp;LUNAS &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;  BELUM LUNAS <br>
	* <i> lingkari yang dipilih </i>
	<br>
	<table>
		<tbody>
			<tr>
				<td style="border-top:none;border-right:none;border-left:none;border-bottom:none">
				</td>
				<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none;text-align:center">
					Petugas Kasir <br>
					<br><br>
					( .................................... )
				</td>
			</tr>
		</tbody>
	</table>
	';
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('Laporan_Invoice.pdf', 'I');
?>
