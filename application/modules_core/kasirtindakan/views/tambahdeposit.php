<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kasirtindakan/homekasirtindakan">DEPOSIT</a>
	</li>
</div>

<div class="backregis" style="margin-top:30px;">
	<div id="my-tab-content" class="tab-content">
		<div class="dropdown">
            <div id="titleInformasi">Form Tambah Deposit</div>
       	</div>
       	<br>

       	<div class="informasi">
         	<form class="form-horizontal" method="POST" role="form" action="<?=base_url()?>kasirtindakan/listdeposit/detail">
       			<table width="100%">
       				<tr>
       					<td width="50%">
       						
							<div class="form-group">
								<label class="control-label col-md-5">Visit ID</label>
								<div class="input-group col-md-3">
									<input type="text" class="form-control" style="cursor:pointer;background-color:white" id="dep_visitid" name="dep_visitid" placeholder="Search Visit ID" data-toggle="modal" data-target="#searchvisit" required>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-5">Tanggal</label>
								<div class="input-group col-md-3" >
									<div class="input-icon">
										<i class="fa fa-calendar"></i>
										<input type="text" id="dep_date" style="cursor:pointer; background-color:white" class="form-control" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" required>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-5">Nomor Rekam Medis</label>
								<div class="input-group col-md-4">
									<input type="text" readonly class="form-control" id="dep_norm" name="dep_norm" placeholder="Nomor Rekam Medis">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-5"> Nama Pasien</label>
								<div class="input-group col-md-4">
									<input type="text" readonly class="form-control" id="dep_nama" name="dep_nama" placeholder="Nama Pasien">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-5">Alamat </label>
								<div class="input-group col-md-5">
									<input type="text" readonly class="form-control" id="dep_alamat" name="dep_alamat" placeholder="Alamat">
								</div>
							</div>
       					</td>

       					<td width="50%">
	       						
       						<div class="form-group" style="margin-bottom:215px;">
								<label class="control-label col-md-5">Kamar Rawat Inap </label>
								<div class="input-group col-md-3">
									<input type="text" readonly class="form-control" id="dep_kamar" name="dep_kamar" placeholder="Kamar Rawat Inap">
								</div>
							</div>

							
       					</td>
       				</tr>
       			</table>

       			<br>
       			<div class="pull-right" style="margin-right:20px">
	       			<button type="reset" class="btn btn-warning" data-dismiss="modal">Reset</button>&nbsp;&nbsp;&nbsp;
	  			 	<button type="submit" class="btn btn-success" id="ivnonbpjs">Tambah Deposit</a>
	  			 </div>
       		</form>
       		<br><br><br>
       	</div>

	</div>
</div>

<div class="modal fade" id="searchvisit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h3 class="modal-title" id="myModalLabel">Cari Visit ID</h3>
			</div>
			<div class="modal-body">
				<form method="POST" id="submit_searchvisit">
				<div class="form-group">
					<div class="col-md-6">
						<input type="text" class="form-control" name="keyword" id="keywordsearch" placeholder="Masukan Visit ID / Nama Pasien">
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-info">Cari</button>
					</div>	
				</div>	
				</form>
				<br><br>
				<div style="margin-left:5px; margin-right:5px;"><hr></div>
				<div class="portlet-body" style="margin: 0px 5px 0px 5px">
					<table class="table table-striped table-bordered table-hover" id="tabelsearchvisitid">
						<thead>
							<tr class="info">
								<th>Visit ID</th>
								<th>Nama Pasien</th>
								<th width="10%">Pilih</th>
							</tr>
						</thead>
						<tbody id="tbody_visit">
							<tr>
								<td colspan="3"><center>Cari Visit Pasien</center></td>
							</tr>
						</tbody>
					</table>												
				</div>
			</div>
			<div class="modal-footer">
		       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
	      	</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(window).ready(function(){

		$('#submit_searchvisit').submit(function(event){
			event.preventDefault();
			var item = {};
			item['search'] = $('#keywordsearch').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?=base_url()?>kasirtindakan/tambahdeposit/search_visit',
				success:function(data){
					$('#tbody_visit').empty();

					for(var i = 0; i< data.length; i++){
						$('#tbody_visit').append(
							'<tr>'+
								'<td>'+data[i]['visit_id']+'</td>'+
								'<td>'+data[i]['nama']+'</td>'+
								'<td style="text-align:center; cursor:pointer;">'+
									'<input type="hidden" class="visitid" value="'+data[i]['visit_id']+'">'+
									'<input type="hidden" class="rm_id" value="'+data[i]['rm_id']+'">'+
									'<input type="hidden" class="nama_pasien" value="'+data[i]['nama']+'">'+
									'<input type="hidden" class="alamat" value="'+data[i]['alamat_skr']+'">'+
									'<input type="hidden" class="kamar_inap" value="'+data[i]['nama_kamar']+'">'+
									'<a href="#" class="selectvisit"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a>'+
								'</td>'+
							'</tr> '
						);
					}

					if(data.length==0){
						$('#tbody_visit').append(
							'<tr>'+
								'<td colspan="3"><center>Data Visit Pasien Tidak Ditemukan</center></td>'+
							'</tr>'
						);
					}
				}
			});
		});

		$('#tbody_visit').on('click', 'tr td .selectvisit', function(){
			var visit_id = $(this).closest('tr').find('.visitid').val();
			var rm_id = $(this).closest('tr').find('.rm_id').val();
			var nama = $(this).closest('tr').find('.nama_pasien').val();
			var alamat = $(this).closest('tr').find('.alamat').val();
			var kamar = $(this).closest('tr').find('.kamar_inap').val();

			$('#dep_visitid').val(visit_id);
			$('#dep_norm').val(rm_id);
			$('#dep_nama').val(nama);
			$('#dep_alamat').val(alamat);
			$('#dep_kamar').val(kamar);

			$('#tbody_visit').append(
				'<tr>'+
					'<td colspan="3"><center>Cari Visit Pasien</center></td>'+
				'</tr>'
			);

			$('#keywordsearch').val('');

			$('#searchvisit').modal('hide');
		});	
	});
</script>