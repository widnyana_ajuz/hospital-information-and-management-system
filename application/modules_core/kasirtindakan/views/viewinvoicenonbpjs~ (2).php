<style type="text/css">
	/* General styles for the modal */

	/* 
	Styles for the html/body for special modal where we want 3d effects
	Note that we need a container wrapping all content on the page for the 
	perspective effects (not including the modals and the overlay).
	*/
	.md-perspective,
	.md-perspective body {
		height: 100%;
		overflow: hidden;
	}

	.md-perspective body  {
		background: #222;
		-webkit-perspective: 600px;
		-moz-perspective: 600px;
		perspective: 600px;
	}

	.container {
		background: #e74c3c;
		min-height: 100%;
	}

	.md-modal {
		position: fixed;
		top: 50%;
		left: 50%;
		width: 50%;
		max-width: 630px;
		min-width: 320px;
		height: auto;
		z-index: 2000;
		visibility: hidden;
		-webkit-backface-visibility: hidden;
		-moz-backface-visibility: hidden;
		backface-visibility: hidden;
		-webkit-transform: translateX(-50%) translateY(-50%);
		-moz-transform: translateX(-50%) translateY(-50%);
		-ms-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}

	.md-show {
		visibility: visible;
	}

	.md-overlay {

		position: fixed;
		width: 100%;
		height: 100%;
		visibility: hidden;
		top: 0;
		left: 0;
		z-index: 1050;
		opacity: 0;
		background: rgba(143,27,15,0.8);
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show ~ .md-overlay {
		opacity: 1;
		visibility: visible;
	}

	/* Content styles */
	.md-content {
		color: #fff;
		background: #e74c3c;
		position: relative;
		border-radius: 3px;
		margin: 0 auto;
	}

	.md-content h3 {
		margin: 0;
		padding: 0.4em;
		text-align: center;
		font-size: 2.4em;
		font-weight: 300;
		opacity: 0.8;
		background: rgba(0,0,0,0.1);
		border-radius: 3px 3px 0 0;
	}

	.md-content > div {
		padding: 15px 40px 30px;
		margin: 0;
		font-weight: 300;
		font-size: 1.15em;
	}

	.md-content > div p {
		margin: 0;
		padding: 10px 0;
	}

	.md-content > div ul {
		margin: 0;
		padding: 0 0 30px 20px;
	}

	.md-content > div ul li {
		padding: 5px 0;
	}

	.md-content button {
		display: block;
		margin: 0 auto;
		font-size: 0.8em;
	}

	/* Individual modal styles with animations/transitions */

	/* Effect 1: Fade in and scale up */
	.md-effect-1 .md-content {
		-webkit-transform: scale(0.7);
		-moz-transform: scale(0.7);
		-ms-transform: scale(0.7);
		transform: scale(0.7);
		opacity: 0;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show.md-effect-1 .md-content {
		-webkit-transform: scale(1);
		-moz-transform: scale(1);
		-ms-transform: scale(1);
		transform: scale(1);
		opacity: 1;
	}

	/* Effect 2: Slide from the right */
	.md-effect-2 .md-content {
		-webkit-transform: translateX(20%);
		-moz-transform: translateX(20%);
		-ms-transform: translateX(20%);
		transform: translateX(20%);
		opacity: 0;
		-webkit-transition: all 0.3s cubic-bezier(0.25, 0.5, 0.5, 0.9);
		-moz-transition: all 0.3s cubic-bezier(0.25, 0.5, 0.5, 0.9);
		transition: all 0.3s cubic-bezier(0.25, 0.5, 0.5, 0.9);
	}

	.md-show.md-effect-2 .md-content {
		-webkit-transform: translateX(0);
		-moz-transform: translateX(0);
		-ms-transform: translateX(0);
		transform: translateX(0);
		opacity: 1;
	}

	/* Effect 3: Slide from the bottom */
	.md-effect-3 .md-content {
		-webkit-transform: translateY(20%);
		-moz-transform: translateY(20%);
		-ms-transform: translateY(20%);
		transform: translateY(20%);
		opacity: 0;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show.md-effect-3 .md-content {
		-webkit-transform: translateY(0);
		-moz-transform: translateY(0);
		-ms-transform: translateY(0);
		transform: translateY(0);
		opacity: 1;
	}

	/* Effect 4: Newspaper */
	.md-effect-4 .md-content {
		-webkit-transform: scale(0) rotate(720deg);
		-moz-transform: scale(0) rotate(720deg);
		-ms-transform: scale(0) rotate(720deg);
		transform: scale(0) rotate(720deg);
		opacity: 0;
	}

	.md-show.md-effect-4 ~ .md-overlay,
	.md-effect-4 .md-content {
		-webkit-transition: all 0.5s;
		-moz-transition: all 0.5s;
		transition: all 0.5s;
	}

	.md-show.md-effect-4 .md-content {
		-webkit-transform: scale(1) rotate(0deg);
		-moz-transform: scale(1) rotate(0deg);
		-ms-transform: scale(1) rotate(0deg);
		transform: scale(1) rotate(0deg);
		opacity: 1;
	}

	/* Effect 5: fall */
	.md-effect-5.md-modal {
		-webkit-perspective: 1300px;
		-moz-perspective: 1300px;
		perspective: 1300px;
	}

	.md-effect-5 .md-content {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform: translateZ(600px) rotateX(20deg); 
		-moz-transform: translateZ(600px) rotateX(20deg); 
		-ms-transform: translateZ(600px) rotateX(20deg); 
		transform: translateZ(600px) rotateX(20deg); 
		opacity: 0;
	}

	.md-show.md-effect-5 .md-content {
		-webkit-transition: all 0.3s ease-in;
		-moz-transition: all 0.3s ease-in;
		transition: all 0.3s ease-in;
		-webkit-transform: translateZ(0px) rotateX(0deg);
		-moz-transform: translateZ(0px) rotateX(0deg);
		-ms-transform: translateZ(0px) rotateX(0deg);
		transform: translateZ(0px) rotateX(0deg); 
		opacity: 1;
	}

	/* Effect 6: side fall */
	.md-effect-6.md-modal {
		-webkit-perspective: 1300px;
		-moz-perspective: 1300px;
		perspective: 1300px;
	}

	.md-effect-6 .md-content {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform: translate(30%) translateZ(600px) rotate(10deg); 
		-moz-transform: translate(30%) translateZ(600px) rotate(10deg);
		-ms-transform: translate(30%) translateZ(600px) rotate(10deg);
		transform: translate(30%) translateZ(600px) rotate(10deg); 
		opacity: 0;
	}

	.md-show.md-effect-6 .md-content {
		-webkit-transition: all 0.3s ease-in;
		-moz-transition: all 0.3s ease-in;
		transition: all 0.3s ease-in;
		-webkit-transform: translate(0%) translateZ(0) rotate(0deg);
		-moz-transform: translate(0%) translateZ(0) rotate(0deg);
		-ms-transform: translate(0%) translateZ(0) rotate(0deg);
		transform: translate(0%) translateZ(0) rotate(0deg);
		opacity: 1;
	}

	/* Effect 7:  slide and stick to top */
	.md-effect-7{
		top: 0;
		-webkit-transform: translateX(-50%);
		-moz-transform: translateX(-50%);
		-ms-transform: translateX(-50%);
		transform: translateX(-50%);
	}

	.md-effect-7 .md-content {
		-webkit-transform: translateY(-200%);
		-moz-transform: translateY(-200%);
		-ms-transform: translateY(-200%);
		transform: translateY(-200%);
		-webkit-transition: all .3s;
		-moz-transition: all .3s;
		transition: all .3s;
		opacity: 0;
	}

	.md-show.md-effect-7 .md-content {
		-webkit-transform: translateY(0%);
		-moz-transform: translateY(0%);
		-ms-transform: translateY(0%);
		transform: translateY(0%);
		border-radius: 0 0 3px 3px;
		opacity: 1;
	}

	/* Effect 8: 3D flip horizontal */
	.md-effect-8.md-modal {
		-webkit-perspective: 1300px;
		-moz-perspective: 1300px;
		perspective: 1300px;
	}

	.md-effect-8 .md-content {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform: rotateY(-70deg);
		-moz-transform: rotateY(-70deg);
		-ms-transform: rotateY(-70deg);
		transform: rotateY(-70deg);
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
		opacity: 0;
	}

	.md-show.md-effect-8 .md-content {
		-webkit-transform: rotateY(0deg);
		-moz-transform: rotateY(0deg);
		-ms-transform: rotateY(0deg);
		transform: rotateY(0deg);
		opacity: 1;
	}

	/* Effect 9: 3D flip vertical */
	.md-effect-9.md-modal {
		-webkit-perspective: 1300px;
		-moz-perspective: 1300px;
		perspective: 1300px;
	}

	.md-effect-9 .md-content {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform: rotateX(-70deg);
		-moz-transform: rotateX(-70deg);
		-ms-transform: rotateX(-70deg);
		transform: rotateX(-70deg);
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
		opacity: 0;
	}

	.md-show.md-effect-9 .md-content {
		-webkit-transform: rotateX(0deg);
		-moz-transform: rotateX(0deg);
		-ms-transform: rotateX(0deg);
		transform: rotateX(0deg);
		opacity: 1;
	}

	/* Effect 10: 3D sign */
	.md-effect-10.md-modal {
		-webkit-perspective: 1300px;
		-moz-perspective: 1300px;
		perspective: 1300px;
	}

	.md-effect-10 .md-content {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform: rotateX(-60deg);
		-moz-transform: rotateX(-60deg);
		-ms-transform: rotateX(-60deg);
		transform: rotateX(-60deg);
		-webkit-transform-origin: 50% 0;
		-moz-transform-origin: 50% 0;
		transform-origin: 50% 0;
		opacity: 0;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show.md-effect-10 .md-content {
		-webkit-transform: rotateX(0deg);
		-moz-transform: rotateX(0deg);
		-ms-transform: rotateX(0deg);
		transform: rotateX(0deg);
		opacity: 1;
	}

	/* Effect 11: Super scaled */
	.md-effect-11 .md-content {
		-webkit-transform: scale(2);
		-moz-transform: scale(2);
		-ms-transform: scale(2);
		transform: scale(2);
		opacity: 0;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show.md-effect-11 .md-content {
		-webkit-transform: scale(1);
		-moz-transform: scale(1);
		-ms-transform: scale(1);
		transform: scale(1);
		opacity: 1;
	}

	/* Effect 12:  Just me */
	.md-effect-12 .md-content {
		-webkit-transform: scale(0.8);
		-moz-transform: scale(0.8);
		-ms-transform: scale(0.8);
		transform: scale(0.8);
		opacity: 0;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show.md-effect-12 ~ .md-overlay {
		/*background: #e74c3c;*/
		background: rgba(255,255,255,0.98);
	} 

	.md-effect-12 .md-content h3,
	.md-effect-12 .md-content {
		background: transparent;
	}

	.md-show.md-effect-12 .md-content {
		-webkit-transform: scale(1);
		-moz-transform: scale(1);
		-ms-transform: scale(1);
		transform: scale(1);
		opacity: 1;
	}

	/* Effect 13: 3D slit */
	.md-effect-13.md-modal {
		-webkit-perspective: 1300px;
		-moz-perspective: 1300px;
		perspective: 1300px;
	}

	.md-effect-13 .md-content {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform: translateZ(-3000px) rotateY(90deg);
		-moz-transform: translateZ(-3000px) rotateY(90deg);
		-ms-transform: translateZ(-3000px) rotateY(90deg);
		transform: translateZ(-3000px) rotateY(90deg);
		opacity: 0;
	}

	.md-show.md-effect-13 .md-content {
		-webkit-animation: slit .7s forwards ease-out;
		-moz-animation: slit .7s forwards ease-out;
		animation: slit .7s forwards ease-out;
	}

	@-webkit-keyframes slit {
		50% { -webkit-transform: translateZ(-250px) rotateY(89deg); opacity: .5; -webkit-animation-timing-function: ease-out;}
		100% { -webkit-transform: translateZ(0) rotateY(0deg); opacity: 1; }
	}

	@-moz-keyframes slit {
		50% { -moz-transform: translateZ(-250px) rotateY(89deg); opacity: .5; -moz-animation-timing-function: ease-out;}
		100% { -moz-transform: translateZ(0) rotateY(0deg); opacity: 1; }
	}

	@keyframes slit {
		50% { transform: translateZ(-250px) rotateY(89deg); opacity: 1; animation-timing-function: ease-in;}
		100% { transform: translateZ(0) rotateY(0deg); opacity: 1; }
	}

	/* Effect 14:  3D Rotate from bottom */
	.md-effect-14.md-modal {
		-webkit-perspective: 1300px;
		-moz-perspective: 1300px;
		perspective: 1300px;
	}

	.md-effect-14 .md-content {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform: translateY(100%) rotateX(90deg);
		-moz-transform: translateY(100%) rotateX(90deg);
		-ms-transform: translateY(100%) rotateX(90deg);
		transform: translateY(100%) rotateX(90deg);
		-webkit-transform-origin: 0 100%;
		-moz-transform-origin: 0 100%;
		transform-origin: 0 100%;
		opacity: 0;
		-webkit-transition: all 0.3s ease-out;
		-moz-transition: all 0.3s ease-out;
		transition: all 0.3s ease-out;
	}

	.md-show.md-effect-14 .md-content {
		-webkit-transform: translateY(0%) rotateX(0deg);
		-moz-transform: translateY(0%) rotateX(0deg);
		-ms-transform: translateY(0%) rotateX(0deg);
		transform: translateY(0%) rotateX(0deg);
		opacity: 1;
	}

	/* Effect 15:  3D Rotate in from left */
	.md-effect-15.md-modal {
		-webkit-perspective: 1300px;
		-moz-perspective: 1300px;
		perspective: 1300px;
	}

	.md-effect-15 .md-content {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform: translateZ(100px) translateX(-30%) rotateY(90deg);
		-moz-transform: translateZ(100px) translateX(-30%) rotateY(90deg);
		-ms-transform: translateZ(100px) translateX(-30%) rotateY(90deg);
		transform: translateZ(100px) translateX(-30%) rotateY(90deg);
		-webkit-transform-origin: 0 100%;
		-moz-transform-origin: 0 100%;
		transform-origin: 0 100%;
		opacity: 0;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show.md-effect-15 .md-content {
		-webkit-transform: translateZ(0px) translateX(0%) rotateY(0deg);
		-moz-transform: translateZ(0px) translateX(0%) rotateY(0deg);
		-ms-transform: translateZ(0px) translateX(0%) rotateY(0deg);
		transform: translateZ(0px) translateX(0%) rotateY(0deg);
		opacity: 1;
	}

	/* Effect 16:  Blur */
	.md-show.md-effect-16 ~ .md-overlay {
		background: rgba(180,46,32,0.5);
	}

	.md-show.md-effect-16 ~ .container {
		-webkit-filter: blur(3px);
		-moz-filter: blur(3px);
		filter: blur(3px);
	}

	.md-effect-16 .md-content {
		-webkit-transform: translateY(-5%);
		-moz-transform: translateY(-5%);
		-ms-transform: translateY(-5%);
		transform: translateY(-5%);
		opacity: 0;
	}

	.md-show.md-effect-16 ~ .container,
	.md-effect-16 .md-content {
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show.md-effect-16 .md-content {
		-webkit-transform: translateY(0);
		-moz-transform: translateY(0);
		-ms-transform: translateY(0);
		transform: translateY(0);
		opacity: 1;
	}

	/* Effect 17:  Slide in from bottom with perspective on container */
	.md-show.md-effect-17 ~ .container {
		height: 100%;
		overflow: hidden;
		-webkit-transition: -webkit-transform 0.3s;
		-moz-transition: -moz-transform 0.3s;
		transition: transform 0.3s;
	}	

	.md-show.md-effect-17 ~ .container,
	.md-show.md-effect-17 ~ .md-overlay  {
		-webkit-transform: rotateX(-2deg);
		-moz-transform: rotateX(-2deg);
		-ms-transform: rotateX(-2deg);
		transform: rotateX(-2deg);
		-webkit-transform-origin: 50% 0%;
		-moz-transform-origin: 50% 0%;
		transform-origin: 50% 0%;
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
	}

	.md-effect-17 .md-content {
		opacity: 0;
		-webkit-transform: translateY(200%);
		-moz-transform: translateY(200%);
		-ms-transform: translateY(200%);
		transform: translateY(200%);
	}

	.md-show.md-effect-17 .md-content {
		-webkit-transform: translateY(0);
		-moz-transform: translateY(0);
		-ms-transform: translateY(0);
		transform: translateY(0);
		opacity: 1;
		-webkit-transition: all 0.3s 0.2s;
		-moz-transition: all 0.3s 0.2s;
		transition: all 0.3s 0.2s;
	}

	/* Effect 18:  Slide from right with perspective on container */
	.md-show.md-effect-18 ~ .container {
		height: 100%;
		overflow: hidden;
	}

	.md-show.md-effect-18 ~ .md-overlay {
		background: rgba(143,27,15,0.8);
		-webkit-transition: all 0.5s;
		-moz-transition: all 0.5s;
		transition: all 0.5s;
	}

	.md-show.md-effect-18 ~ .container,
	.md-show.md-effect-18 ~ .md-overlay {
		-webkit-transform-style: preserve-3d;
		-webkit-transform-origin: 0% 50%;
		-webkit-animation: rotateRightSideFirst 0.5s forwards ease-in;
		-moz-transform-style: preserve-3d;
		-moz-transform-origin: 0% 50%;
		-moz-animation: rotateRightSideFirst 0.5s forwards ease-in;
		transform-style: preserve-3d;
		transform-origin: 0% 50%;
		animation: rotateRightSideFirst 0.5s forwards ease-in;
	}

	@-webkit-keyframes rotateRightSideFirst {
		50% { -webkit-transform: translateZ(-50px) rotateY(5deg); -webkit-animation-timing-function: ease-out; }
		100% { -webkit-transform: translateZ(-200px); }
	}

	@-moz-keyframes rotateRightSideFirst {
		50% { -moz-transform: translateZ(-50px) rotateY(5deg); -moz-animation-timing-function: ease-out; }
		100% { -moz-transform: translateZ(-200px); }
	}

	@keyframes rotateRightSideFirst {
		50% { transform: translateZ(-50px) rotateY(5deg); animation-timing-function: ease-out; }
		100% { transform: translateZ(-200px); }
	}

	.md-effect-18 .md-content {
		-webkit-transform: translateX(200%);
		-moz-transform: translateX(200%);
		-ms-transform: translateX(200%);
		transform: translateX(200%);
		opacity: 0;
	}

	.md-show.md-effect-18 .md-content {
		-webkit-transform: translateX(0);
		-moz-transform: translateX(0);
		-ms-transform: translateX(0);
		transform: translateX(0);
		opacity: 1;
		-webkit-transition: all 0.5s 0.1s;
		-moz-transition: all 0.5s 0.1s;
		transition: all 0.5s 0.1s;
	}

	/* Effect 19:  Slip in from the top with perspective on container */
	.md-show.md-effect-19 ~ .container {
		height: 100%;
		overflow: hidden;
	}

	.md-show.md-effect-19 ~ .md-overlay {
		-webkit-transition: all 0.5s;
		-moz-transition: all 0.5s;
		transition: all 0.5s;
	}

	.md-show.md-effect-19 ~ .container,
	.md-show.md-effect-19 ~ .md-overlay {
		-webkit-transform-style: preserve-3d;
		-webkit-transform-origin: 50% 100%;
		-webkit-animation: OpenTop 0.5s forwards ease-in;
		-moz-transform-style: preserve-3d;
		-moz-transform-origin: 50% 100%;
		-moz-animation: OpenTop 0.5s forwards ease-in;
		transform-style: preserve-3d;
		transform-origin: 50% 100%;
		animation: OpenTop 0.5s forwards ease-in;
	}

	@-webkit-keyframes OpenTop {
		50% { 
			-webkit-transform: rotateX(10deg); 
			-webkit-animation-timing-function: ease-out; 
		}
	}

	@-moz-keyframes OpenTop {
		50% { 
			-moz-transform: rotateX(10deg); 
			-moz-animation-timing-function: ease-out; 
		}
	}

	@keyframes OpenTop {
		50% { 
			transform: rotateX(10deg); 
			animation-timing-function: ease-out; 
		}
	}

	.md-effect-19 .md-content {
		-webkit-transform: translateY(-200%);
		-moz-transform: translateY(-200%);
		-ms-transform: translateY(-200%);
		transform: translateY(-200%);
		opacity: 0;
	}

	.md-show.md-effect-19 .md-content {
		-webkit-transform: translateY(0);
		-moz-transform: translateY(0);
		-ms-transform: translateY(0);
		transform: translateY(0);
		opacity: 1;
		-webkit-transition: all 0.5s 0.1s;
		-moz-transition: all 0.5s 0.1s;
		transition: all 0.5s 0.1s;
	}

	@media screen and (max-width: 32em) {
		body { font-size: 75%; }
	}
</style>

<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>bersalin/homebersalin">Nama Pasien Bersalin</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>bersalin/tagihannonbpjs">Invoice</a>
	</li>
</div>

<input type="hidden" id="visit_id" value="<?php echo $visit_id; ?>"/>
<input type="hidden" id="sub_visit" value="<?php echo $sub_visit; ?>"/>
<input type="hidden" id="no_invoice" value="<?php echo $no_invoice; ?>"/>
<div class="backregis" style="margin-top:30px;	">
	<div id="my-tab-content" class="tab-content">
			
		<div class="informasi">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="font-size:16px;">
						<label class="control-label1 col-md-4 nama">Nomor Invoice</label>
						<div class="col-md-4 nama">: <?php echo $no_invoice; ?> </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Visit ID</label>
						<div class="col-md-5">:	<?php echo $visit_id ?> </div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Kelas Perawatan</label>
						<div class="col-md-5">: Kelas <?php echo $invoice['kelas_perawatan'] ?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Tanggal Invoice</label>
						<div class="col-md-5">: <?php 
							$tgl = strtotime($invoice['tanggal_invoice']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;
						?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Tanggal Kunjungan</label>
						<div class="col-md-5">: <?php
							$tgl = strtotime($pasien['tanggal_visit']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;
						?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nomor Rekam Medis</label>
						<div class="col-md-5">: <?php echo $pasien['rm_id']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Cara Bayar</label>
						<div class="col-md-5">: <?php echo $invoice['cara_bayar']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Pasien</label>
						<div class="col-md-5">: <?php echo $pasien['nama']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Asuransi</label>
						<div class="col-md-5">: <?php echo $invoice['nama_asuransi']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Alamat</label>
						<div class="col-md-5">: <?php echo $pasien['alamat_skr']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Perusahaan</label>
						<div class="col-md-5">: <?php echo $invoice['nama_perusahaan']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Jenis Kunjungan</label>
						<div class="col-md-5">: <?php echo $pasien['tipe_kunjungan']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nomor Ansuransi </label>
						<div class="col-md-5">: <?php echo $invoice['no_asuransi']; ?> </div>
					</div>
				</div>
			</div>
		</div>

		<hr class="garis">

		<form class="form-horizontal" role="form" id="submitInvoice">
			<div id="tagihadmisi">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Admisi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Admisi Tertagih</th>
									<th>Waktu </th>
									<th>Tarif</th>
									
								</tr>
							</thead>
							<tbody id="tbody_resep">
								<?php
									if(!empty($tagihanadmisi)){
										$tgl = strtotime($tagihanadmisi['waktu']);
										$hasil = date('d F Y - H:i', $tgl);

										echo'
										<tr>
											<td>1</td>
											<td><center>'.$tagihanadmisi['nama_tindakan'].'</center></td>
											<td><center>'.$hasil.'</center></td>
											<td><center>'.number_format($tagihanadmisi['tarif'],0,'','.').'</center></td>
										</tr>
										<input type="hidden" value="'.$tagihanadmisi['tarif'].'" id="tarif_admisi">
										';
									}else{
										echo'
											<tr>
												<td colspan="4" align="center">Tidak Terdapat Tagihan Admisi</td>
											</tr>
										';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>
			
			<div id="tagihankamar">
				<div id="titleInformasi" style="margin-bottom:-40px; margin-top:30px">
				<p style="text-align:center;margin-top:-30px;">Tagihan Kamar</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihankamar">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Kamar Tertagih</th>
									<th>Waktu Masuk </th>
									<th>Waktu Keluar</th>
									<th>Lama</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttkamar">
								<!-- <tr>
									<td align="center">1</td>
									<td>007</td>
									<td style="text-align:center;">12 Desember 2012 - 12:12</td>
									<td style="text-align:center;">12 Desember 2014 - 13:13</td>
									<td>2 tahun</td>
									<td style="text-align:right;">1000</td>
									<td>
										<input type="checkbox" class="check" style="float:left; margin-right:10px; margin-top:10px;" class="checktarif">
										<input type="number" class="form-control input-sm kamar_totallain" style="float:left; width:100px; margin-right:-20px;" name="onfakturakomodasi" readonly="true">
									</td>
									<td style="text-align:right;">299</td>
									<td style="text-align:right;">1000</td>
									<td style="text-align:center">
										<a href="#">
										<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
									</td>
								</tr> -->
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihanakomodasi">
				<div id="titleInformasi" style="margin-bottom:-40px; margin-top:30px">
				<p style="text-align:center;margin-top:-30px;">Tagihan Makan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
							<table class="table table-striped table-bordered table-hover" id="tbtagihanakomodasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Akomodasi Tertagih</th>
									<th>Unit</th>									
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttakomodasi">
								<!-- <tr>
									<td align="center">1</td>
									<td>123</td>
									<td>asd</td>
									<td style="text-align:right;">123</td>
									<td style="text-align:right;">123</td>
									<td style="text-align:right;"><input type="text" class="form-control input-sm" style="width:80px" name="onfakturakomodasi"></td>
									<td style="text-align:right;">123</td>
									<td style="text-align:center">
										<a href="#">
										<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
									</td>
								</tr> -->
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihantindakanperawatan">
				<div id="titleInformasi" style="margin-bottom:-40px;"><a href="#modalttperawatan" data-toggle="modal" style="text-align:left;margin-left:-10px;font-size:12pt;color:white"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan Tindakan Perawatan">&nbsp;Tambah Tagihan Tindakan Perawatan</i></a>
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Perawatan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihanperawatan">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Perawatan Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									<th width="50">Delete</th>
								</tr>
							</thead>
							<tbody id="tbody_ttperawatan">
								
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtindakanpenunjang" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Penunjang</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanpenunjang">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Penunjang Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									
								</tr>
							</thead>
							<tbody id="tbody_ttpenunjang">
								<?php
									$no = 0;
									$totalpenunjang = 0;
									if(!empty($tagihantunjang)){
										foreach ($tagihantunjang as $data) {
											$tgl = strtotime(substr($data['waktu'], 0, 10));
											$hasil = date('d F Y', $tgl); 

											echo '
												<tr>
													<td>'.++$no.'</td>
													<td>'.$data['nama_tindakan'].'
														<input type="hidden" class="tpenunjang_id" value="'.$data['tindakan_penunjang_id'].'">
														<input type="hidden" class="vpenunjang_id" value="'.$data['penunjang_detail_id'].'">
													</td>
													<td>'.$data['nama_dept'].'</td>
													<td>'.$hasil.'</td>
													<td style="text-align:right;">'.number_format((intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])),0,'','.').'</td>
													<td style="text-align:right;">
														<input type="hidden" class="inputtarif" value="'.(intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])).'">
														<input type="hidden" class="inputtotal">
														'.number_format($data['on_faktur'],0,'','.').'
													</td>
													<td style="text-align:right;" class="t_total">'.number_format((intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])+intval($data['on_faktur'])),0,'','.').'</td>
												</tr>
											';
											$totalpenunjang += intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])+intval($data['on_faktur']);
										}
										echo '<input type="hidden" id="t_penunjang" value="'.$totalpenunjang.'" >';
									}else{
										echo '
											<tr>
												<td colspan="7" align="center">Tidak Terdapat Tagihan Tindakan Penunjang</td>
											</tr>
										';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtagihantindakanoperasi" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;"><a href="#modalttoperasi" data-toggle="modal" style="text-align:left;margin-left:-10px;font-size:12pt;color:white"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan Tindakan Operasi">&nbsp;Tambah Tagihan Tindakan Operasi</i></a>
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Operasi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
					<table class="table table-striped table-bordered table-hover" id="tbtagihanoperasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Operasi Tertagih</th>
									<th>Lingkup Operasi</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									
								</tr>
							</thead>
							<tbody id="tbody_ttoperasi">
								<?php	
									if(!empty($tindakan)){
										echo'<input type="hidden" id="jml_table" value="no">';
										$no = 0;
										$totaloperasi = 0;
										foreach ($tindakan as $data) {
											echo '
												<tr>
													<td align="center">'.++$no.'</td>
													<td>'.$data['nama_tindakan'].'</td>
													<td>'.$data['lingkup_operasi'].'</td>
													<td style="text-align:center;">'.$data['waktu'].'</td>
													<td style="text-align:right;">'.$data['tarif'].'</td>
													<td style="text-align:right;">'.$data['on_faktur'].'</td>
													<td style="text-align:right;">'.(intval($data['tarif'])+intval($data['on_faktur'])).'</td>
												</tr>
											';	
											$totaloperasi += intval($data['tarif'])+intval($data['on_faktur']);
										}

										echo '<input type="hidden" id="t_operasi" value="'.$totaloperasi.'" >';
									}else{
								?>
								<tr>
									<?php echo'<input type="hidden" id="jml_table" value="yes">'; ?>
									<td colspan="8" align="center">Tidak Terdapat Tagihan Tindakan Operasi</td>
								</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>


			<div style="margin-right:40px;">
				<div class="form-group">
					<div class="col-md-2 pull-right">
						<input type="hidden" id="realtotaltagihan">
						<input type="hidden" id="nilai_total">
						<label class="control-label pull-right" id="totaltagihan" style="font-size:1.8em;margin-top:-10px;">0</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Total Tagihan (Rp.) : 
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 pull-right">
						<input type="hidden" value="<?php echo $deposit?>" id="getdeposit">
						<label class="control-label pull-right" id="deposit" style="font-size:1.8em;margin-top:-10px;">0</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Deposit (Rp.) : 
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-2 pull-right">
						<input type="hidden" id="nilai_kekurangan">
						<label class="control-label pull-right" id="kekurangan" style="font-size:1.8em;margin-top:-10px;">0</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Kekurangan (Rp.) : 
					</div>
				</div>
			</div>

			<div class="pull-right" style="margin-right:40px;">
				<br>
				<button type="submit" class="btn btn-info">CETAK</button>
				<a href="<?php echo base_url() ?>kasirtindakan/homekasirtindakan" class="btn btn-warning">KEMBALI</a>
				<button type="reset" class="btn btn-danger">BATAL</button>
   				<!-- <button type="submit" class="btn btn-success">SIMPAN</button> -->
   				<input id="model_bayar" style="cursor:pointer;color:white;width:80px" readonly data-modal="modal-12" class="btn btn-success md-trigger" value="BAYAR">
			</div>
			<br><br>
		</form>

		<div class="modal fade" id="modalttkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Kamar</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   										
			        			<div class="form-group">
									<label class="control-label col-md-4">Kamar Tertagih</label>
									<div class="col-md-5">
										<input type="text"  class="typeahead form-control" autocomplete="off" spellcheck="false" id="kamartertagih" name="kamartertagih" placeholder="Kamar Tertagih"  > 
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Waktu Masuk</label>
									<div class="col-md-5">	
										<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" class="form-control" readonly data-date-format="dd/mm/yyyy - hh:ii" data-provide="datetimepicker" placeholder="<?php echo date("d/m/Y - H:i");?>">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Waktu Keluar</label>
									<div class="col-md-5">	
										<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" class="form-control" readonly data-date-format="dd/mm/yyyy - hh:ii" data-provide="datetimepicker" placeholder="<?php echo date("d/m/Y - H:i");?>">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Lama</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="lama" name="lama" placeholder="Lama" readonly> 
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="tarifttkamar" name="tarifttkamar" placeholder="Tarif" > 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="onfakturrrkamar" name="onfakturrrkamar" placeholder="On Faktur" >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Total</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="total" name="total" placeholder="Total" readonly >
									</div>
			        			</div>

								<div class="form-group">
									<label class="control-label col-md-4">Paramedis</label>
									<div class="col-md-5">	
										<input type="text" class="typeahead form-control" name="paramedis" id="kaparamedis" placeholder="Search Paramedis" autocomplete="off" spellcheck="false">		
									</div>
			        			</div>
			        			
		        			</div>
	       				</div>
		        		<br>
		        		<div class="modal-footer">
		        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
		 			     	<button type="submit" class="btn btn-success">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

		<div class="modal fade" id="modalttakomodasi" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakanMakan">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Makan</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="makan_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Akomodasi Tertagih</label>
									<div class="col-md-5">
										<input type="hidden" id="makan_id">
										<textarea class="form-control" id="makan_paket" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Akomodasi Tertagih" required></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Unit</label>
									<div class="col-md-5">	
										<input type="hidden" id="makan_unitid">
										<input type="text" class="form-control" id="makan_unit" name="unitakomodasi" placeholder="Unit"> 
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="makan_tarif" name="tarifakomodasi" placeholder="Tarif" readonly> 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="makan_onfaktur" name="onfakturakomodasi" placeholder="On Faktur" >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Total</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="makan_total" name="totalakomodasi" placeholder="Total" readonly >
									</div>
			        			</div>
			        			
		        			</div>
	       				</div>
		        		<br>
		        		<div class="modal-footer">
		        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
		 			     	<button type="submit" class="btn btn-success">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

		<div class="modal fade" id="modalttperawatan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakanRawat">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Tindakan Perawatan</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="tin_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
									</div>
			        			</div>
			        			<div class="form-group">
								<label class="control-label col-md-4">Unit</label>
									<div class="col-md-5">	
										<input type="hidden" id="idUnit">
										<input type="text" class="form-control" id="unitTindakan" autocomplete="off" spellcheck="false"  name="unit" placeholder="Unit" required >
									</div>
								</div>			
			        			<div class="form-group">
									<label class="control-label col-md-4">Tindakan</label>
									<div class="col-md-6">
										<input type="hidden" id="idtindakan_klinik">
										<input type="text" class="form-control" id="namatindakan" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Search Tindakan" required>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"></label>
									<div class="col-md-6">
										<textarea class="form-control" id="namatindakanarea" readonly placeholder="Tindakan"></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Kelas Pelayanan</label>
									<div class="col-md-5">	
										<input type="hidden" id="idtindakdetail">
										<select class="form-control" name="kelas_tindakan" id="kelas_klinik" required>
											<option value="">Pilih Kelas</option>
											<option value="Kelas VIP">VIP</option>
											<option value="Kelas Utama">Utama</option>
											<option value="Kelas I">Kelas I</option>
											<option value="Kelas II">Kelas II</option>
											<option value="Kelas III">Kelas III</option>
										</select>
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="hidden" id="js_klinik">
										<input type="hidden" id="jp_klinik">
										<input type="hidden" id="bakhp_klinik">
										<input type="text" class="form-control" id="tarif" name="tarif" placeholder="Tarif" readonly > 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="number" class="form-control" id="onfaktur" name="onfaktur" placeholder="On Faktur" required >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Jumlah</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" readonly>
									</div>
			        			</div>

								<div class="form-group">
									<label class="control-label col-md-4">Paramedis</label>
									<div class="col-md-5">	
										<input type="hidden" id="paramedis_id">
										<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Paramedis Lain</label>
									<div class="col-md-6">	
										<textarea class="form-control" id="paramedis_lain" placeholder="Paramedis Lain"></textarea>
									</div>
			        			</div>
		        			</div>
	       				</div>
		        		<br><br>
		        		<div class="modal-footer">
		        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
		 			     	<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

		<br><br><br>	

		<div class="modal fade" id="modalttoperasi" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Tindakan Perawatan</h3>
				   			</div>
							<div class="modal-body">
								<div class="informasi">
					   				<div class="form-group">
										<label class="control-label col-md-4">Waktu Tindakan</label>
										<div class="col-md-5">	
											<input type="text" id="tin_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
										</div>
				        			</div>
				        			<div class="form-group">
									<label class="control-label col-md-4">Lingkup Operasi</label>
										<div class="col-md-5">	
											<select class="form-control" name="kelas_tindakan" id="lingkup">
												<option value="">Pilih Lingkup Operasi</option>
												<option value="Elektif">Elektif</option>
												<option value="Emergency">Emergency</option>
											</select>
										</div>
									</div>			
				        			<div class="form-group">
										<label class="control-label col-md-4">Tindakan</label>
										<div class="col-md-6">
											<input type="hidden" id="idtindakan_klinik">
											<input type="hidden" id="idtindakdetail">
											<textarea class="form-control" id="namatindakan" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Tindakan" ></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-4">Kelas Pelayanan</label>
										<div class="col-md-5">	
											<select class="form-control" name="kelas_tindakan" id="kelas_klinik">
												<option value="">Pilih Kelas</option>
												<option value="Kelas VIP">VIP</option>
												<option value="Kelas Utama">Utama</option>
												<option value="Kelas I">Kelas I</option>
												<option value="Kelas II">Kelas II</option>
												<option value="Kelas III">Kelas III</option>
											</select>
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Tarif</label>
										<div class="col-md-5">	
											<input type="hidden" id="js_klinik">
											<input type="hidden" id="jp_klinik">
											<input type="hidden" id="bakhp_klinik">
											<input type="text" class="form-control" id="tarif" name="tarif" placeholder="Tarif" readonly > 
										</div>
				        			</div>
				        			
				        			<div class="form-group">
										<label class="control-label col-md-4">On Faktur</label>
										<div class="col-md-5">	
											<input type="number" class="form-control" id="onfaktur" name="onfaktur" placeholder="On Faktur" >
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Jumlah</label>
										<div class="col-md-5">	
											<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" readonly>
										</div>
				        			</div>

									<div class="form-group">
										<label class="control-label col-md-4">Paramedis</label>
										<div class="col-md-5">	
											<input type="hidden" id="paramedis_id">
											<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
										</div>
				        			</div>
				        			
			        			</div>
		       				</div>
			        		<br><br>
			        		<div class="modal-footer">
			        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
						    </div>
						</div>
					</div>
				</form>
		</div>

		<div class="md-modal md-effect-12" id="modal-12">
			<div class="md-content">
				<h3 class="modal-title" id="myModalLabel" style="color:black">BAYAR</h3>
				<hr class="garis" style="width:600px;border-color:black">
			
			<form method="POST" id="submit_pembayaran">
				<div style="margin-left:50px;">
					<div class="form-group">
						<div class="col-md-4" style="width:200px; margin-top:5px; text-align:right;color:black">
							Total Tagihan (Rp.) : 
						</div>
						<div class="col-md-4">
							<input type="hidden" id="bayar_total">
							<label class="control-label" id="bayar_text_total" style="width:200px; margin-top:5px; text-align:right;color:black">1.000.000</label>
						</div>
						
					</div><br><br>

					<div class="form-group">
						<div class="col-md-4" style="width:200px; margin-top:5px; text-align:right;color:black">
							Deposit (Rp.) : 
						</div>
						<div class="col-md-4">
							<input type="hidden" id="bayar_deposit">
							<label class="control-label" id="bayar_text_deposit" style="width:200px; margin-top:5px; text-align:right;color:black">1.000.000</label>
						</div>
						
					</div><br><br>

					<div class="form-group">
						<div class="col-md-4" style="width:200px; margin-top:5px; text-align:right;color:black">
							Kekurangan (Rp.) : 
						</div>
						<div class="col-md-4">
							<input type="hidden" id="bayar_kekurangan">
							<label class="control-label" id="bayar_text_kekurangan" style="width:200px; margin-top:5px; text-align:right;color:black">1.000.000</label>
						</div>
						
					</div><br><br>
					
					<div class="form-group">
						<div class="col-md-4" style="width:200px; margin-top:5px; text-align:right;color:black">
							Bayar : 
						</div>
						<div class="input-group col-md-4">
							<span class="input-group-addon" id="basic-addon1">Rp.</span>
							<input type="text" required id="bayar_pembayaran" style="text-align:right" class="form-control" name="bayar">
						</div>
						
					</div>

					<div class="form-group">
						<div class="col-md-4" style="width:200px; margin-top:5px; text-align:right;color:black">
							Kembalian : 
						</div>
						<div class="input-group col-md-4">
							<span class="input-group-addon" id="basic-addon1">Rp.</span>
							<input type="text" id="bayar_kembalian" readonly style="text-align:right" class="form-control" name="kembali">
						</div>
						
					</div><br><br>

					<button type="submit"  class=" btn btn-success">BAYAR</button> <!-- md-close -->
				</div>
			</form>

			</div>
		</div>

		<div class="md-overlay"></div><!-- the overlay element -->
		
	</div>
</div>

<script type="text/javascript">
	$(window).ready(function(){
		var nomor = {};
		var jumlahtable = 0;
		var total = 0;
		var deposit = 0;
		var kekurangan = 0;

		$("#dasbod").val('Identitas Pasien');
		$('.cl').on('click',function (e) {
			e.preventDefault();
			var a = $(this).text();
			$('#dasbod').val(a);
		})

		nomor['no_invoice'] = $('#no_invoice').val();
		nomor['sub_visit'] = $('#sub_visit').val();
		$.ajax({
			type:'POST',
			data:nomor,
			url:'<?php echo base_url();?>rawatjalan/invoicenonbpjs/create_tagihan',
			success:function(data){

				jumlahtable = data.length;

				if(data.length!=0){
					var no = 0;

					for(var i = 0 ; i<data.length; i++){
						no++;
						$('#tbody_ttperawatan').append(
							'<tr>'+
								'<td>'+no+'</td>'+
								'<td>'+data[i]['nama_tindakan']+'</td>'+
								'<td>'+data[i]['nama_dept']+'</td>'+
								'<td style="text-align:center;">'+data[i]['waktu']+'</td>'+
								'<td style="text-align:right;">'+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:center">'+
									'<input type="hidden" class="total_tagihanrawat" value="'+data[i]['jumlah']+'">'+
									'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
									'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
								'</td>'+
							'</tr>'
						);

						total += Number(data[i]['jumlah']);
						kekurangan += Number(data[i]['jumlah']);
					}
				}else{
					$('#tbody_ttperawatan').append(
						'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
					);
				}

				sumTotal();

			}
		});

		var jumlahmakan = 0;
		$.ajax({
			type:'POST',
			data:nomor,
			url:'<?php echo base_url();?>bersalin/invoicenonbpjs/create_tagihanakomodasi',
			success:function(data){
				
				jumlahmakan = data.length;

				if(data.length!=0){
					var no = 0;
					var tarifsemua = 0;

					for(var i = 0 ; i<data.length; i++){
						no++;
						tarifsemua = Number(data[i]['jumlah'])+Number(data[i]['on_faktur']);

						$('#tbody_ttakomodasi').append(
							'<tr>'+
								'<td>'+no+'</td>'+
								'<td>'+data[i]['nama_paket']+'</td>'+
								'<td>'+data[i]['nama_dept']+'</td>'+
								'<td style="text-align:right;">'+
									'<input type="hidden" class="getid_makan" value="'+data[i]['tmakan_id']+'">'+
									'<input type="hidden" class="tarif_makan" value="'+data[i]['tarif']+'">'+
									'<input type="hidden" class="total_tagihanmakan" value="'+tarifsemua+'">'+ //realtotal_makan
									''+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td align="right">'+data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+
									'<input type="hidden" class="total_makan" value="'+data[i]['jumlah']+'">'+
									''+tarifsemua.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'</tr>'
						);

						sumTotal();
					}
				}else{
					$('#tbody_ttakomodasi').append(
						'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Makan</td></tr>'
					);
				}
			},error:function(data){
				console.log(data);
			}
		});

		var jumlahkamar = 0;
		$.ajax({
			type:'POST',
			data:nomor,
			url:'<?php echo base_url();?>bersalin/invoicenonbpjs/create_tagihankamar',
			success:function(data){
				console.log(data);

				jumlahkamar = data.length;

				if(data.length!=0){
					var no = 0;

					for(var i = 0 ; i<data.length; i++){
						var tarifdb = Number(data[i]['tarif'])*Number(data[i]['hari']);
						var tarif = 0;
						var cek = "";
						var ro = "readonly";
						var selected = 0;

						if(data[i]['tarif_lain']!="0"){
							tarif = Number(data[i]['tarif_lain']);
							cek = "checked";
							ro = "";
							selected = Number(data[i]['tarif_lain']);
						}else{
							tarif = tarifdb;
						}
						var totalkamar = tarif+Number(data[i]['on_faktur']);

						no++;
						$('#tbody_ttkamar').append(
							'<tr>'+
								'<td align="center">'+(i+1)+'</td>'+
								'<td>'+data[i]['nama_kamar']+'</td>'+
								'<td style="text-align:center;">'+data[i]['tgl_masuk']+'</td>'+
								'<td style="text-align:center;">'+data[i]['tgl_keluar']+'</td>'+
								'<td>'+data[i]['waktu']+'</td>'+
								// '<td style="text-align:right;">'+(Number(data[i]['tarif'])*Number(data[i]['hari'])).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td>'+
									'<input type="hidden" class="getid_kamar" value="'+data[i]['tkamar_id']+'">'+
									'<input type="hidden" class="kamar_tarifdb" value="'+Number(data[i]['tarif'])*Number(data[i]['hari'])+'">'+
									'<input type="hidden" class="kamar_realtotal" value="'+tarif+'">'+
									'<input type="hidden" class="total_tagihankamar" value="'+totalkamar+'">'+
									'<input type="checkbox" class="check checktarif" '+cek+' style="float:left; margin-right:10px; margin-top:10px;">'+
									'<input type="number" class="form-control input-sm kamar_totallain" style="float:left; width:100px; margin-right:-20px;" value="'+selected+'" '+ro+'>'+
								'</td>'+
								'<td><input type="number" class="form-control input-sm faktur_kamar" style="width:80px" name="onfakturakomodasi" value="'+data[i]['on_faktur']+'"></td>'+
								'<td style="text-align:right;">'+
									''+totalkamar.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'</tr>'
						);

					}

					sumTotal();
				}else{
					$('#tbody_ttkamar').append(
						'<tr><td colspan="9" style="text-align:center;">Tidak Terdapat Tagihan Makan</td></tr>'
					);
				}

			},error:function(data){
				console.log(data);
			}
		});

		$('#tbody_ttakomodasi').on('keyup', 'tr td .faktur_makan', function(){
			var total = $(this).closest('tr').find('.tarif_makan').val();
			var faktur = $(this).val();
			var all = Number(total)+Number(faktur);

			$(this).closest('tr').find('.total_tagihanmakan').val(all);
			$(this).closest('tr').find('td').eq(5).text(all.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

			var totalsemua = $('#realtotaltagihan').val();
			var totalall = 0;
			var allfakur = 0;

			sumTotal();
		});		

		$('#submitTindakanRawat').submit(function(event){
			event.preventDefault();
			var item = {};

			item['waktu'] = $('#tin_date').val();
			item['tindakan_id'] = $('#idtindakdetail').val();
			item['on_faktur'] = $('#onfaktur').val();
			item['paramedis'] = $('#paramedis_id').val();
			item['paramedis_lain'] = $('#paramedis_lain').val();
			item['tarif'] = $('#tarif').val();
			item['jumlah'] = $('#jumlah').val();
			item['js'] = $('#js_klinik').val();
			item['jp'] = $('#jp_klinik').val();
			item['bakhp'] = $('#bakhp_klinik').val();
			item['dept_id'] = $('#idUnit').val();
			item['visit_id']=$('#visit_id').val();
			item['sub_visit']=$('#sub_visit').val();
			item['no_invoice']=$('#no_invoice').val();

			var nama_tindakan = $('#namatindakan').val();
			var nama_dept = $('#unitTindakan').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>rawatjalan/invoicenonbpjs/save_tindakan',
				success:function(data){
					var no = jumlahtable;
					no++;

					console.log(item);

					if(jumlahtable==0)
						$('#tbody_ttperawatan').empty();

					$('#tbody_ttperawatan').append(
						'<tr>'+
							'<td>'+no+'</td>'+
							'<td>'+nama_tindakan+'</td>'+
							'<td>'+nama_dept+'</td>'+
							'<td style="text-align:center;">'+data[0]['waktu']+'</td>'+
							'<td style="text-align:right;">'+(data[0]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
							'<td style="text-align:right;">'+(data[0]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
							'<td style="text-align:right;">'+(data[0]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
							'<td style="text-align:center">'+
								'<input type="hidden" class="total_tagihanrawat" value="'+data[0]['jumlah']+'">'+
								'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[0]['id']+'">'+
								'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
							'</td>'+
						'</tr>'
					);

					$(':input','#submitTindakanRawat')
					  .not(':button, :submit, :reset')
					  .val('');
					$('#tin_date').val("<?php echo date('d/m/Y H:i') ?>");

					$('#modalttperawatan').modal('hide');

					sumTotal();

					myALert('Data Berhasil Ditambahkan');
					console.log(data);
				},error:function(data){
					alert('error');
					console.log(data);
				}
			});
		});


		$(document).on('click','.hapusTindakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#visit_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>rawatjalan/invoicenonbpjs/hapus_tindakan/"+id,
				success:function(data){
					console.log(data);

					$('#tbody_ttperawatan').empty();

					total = 0;
					kekurangan = 0;
					deposit = 0;

					if(data.length!=0){
						var no = 0;

						for(var i = 0 ; i<data.length; i++){
							no++;
							$('#tbody_ttperawatan').append(
								'<tr>'+
									'<td>'+no+'</td>'+
									'<td>'+data[i]['nama_tindakan']+'</td>'+
									'<td>'+data[i]['nama_dept']+'</td>'+
									'<td style="text-align:center;">'+data[i]['waktu']+'</td>'+
									'<td style="text-align:right;">'+(data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
									'<td style="text-align:right;">'+(data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
									'<td style="text-align:right;">'+(data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
									'<td style="text-align:center">'+
										'<input type="hidden" class="total_tagihanrawat" value="'+data[i]['jumlah']+'">'+
										'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
										'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
									'</td>'+
								'</tr>'
							);

							total += Number(data[i]['jumlah']);
						}
					}else{
						$('#tbody_ttperawatan').append(
							'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
						);
					}

					jumlahtable= data.length;

					sumTotal();

				},
				error:function(data){
					console.log(data);
				}	
			});
		});

		var autodata = [];
		var iddata = [];
		$('#namatindakan').focus(function(){
			var $input = $('#namatindakan');
			
			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>kamaroperasi/invoicenonbpjs/get_master_tindakan',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_tindakan']);
							iddata.push(data[i]['tindakan_id']);
						}
					}
				});
			}

			$input.typeahead({source:autodata, 
	        autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = autodata.indexOf(current);

			    $('#idtindakan_klinik').val(iddata[index]);			
			    $('#namatindakanarea').val(autodata[index]);	

			    $('#kelas_klinik').prop('disabled', false);    

			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

		});
		
		$('#kelas_klinik').prop('disabled', true);

		$('#kelas_klinik').change(function(){
			var item = {};
			item['kelas'] = $(this).val();
			item['nama'] = $('#idtindakan_klinik').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>kamaroperasi/invoicenonbpjs/get_tariftindakan',
				success:function(data){
					var tarif = 0;
					var lingkup = $('#lingkup').val();
					var persen = 0;

					$('#idtindakdetail').val(data['detail_id']);

					tarif = (Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))+((Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))*persen);
					
					$('#js_klinik').val(data['js']);
					$('#jp_klinik').val(data['jp']);
					$('#bakhp_klinik').val(data['bakhp']);
					$('#tarif').val(tarif);
					$('#jumlah').val(tarif);
				}
			});
		});

		$('#onfaktur').keyup(function(){
			var onfaktur = $('#onfaktur').val();
			var tarif = $('#tarif').val();
			var jumlah = Number(onfaktur)+Number(tarif);
			$('#jumlah').val(jumlah);
		});

		$('#onfaktur').change(function(){
			var tarif = $('#tarif').val();
			var onfaktur = $(this).val();
			var jumlah = Number(tarif)+Number(onfaktur);
			
			$('#jumlah').val(jumlah);
		});

		$('#unitTindakan').focus(function(){
			var $input = $('#unitTindakan');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>bersalin/invoicenonbpjs/get_all_master_dept',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_dept']);
						iddata.push(data[i]['dept_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#idUnit').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
		
		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#paramedis_id').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		var paket = [];
		var idpaket = [];
		var hargapaket = [];
		$('#makan_paket').focus(function(){
			var $input = $('#makan_paket');
			
			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>bersalin/invoicenonbpjs/get_paket_makan',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							paket.push(data[i]['nama_paket']);
							idpaket.push(data[i]['id']);
							hargapaket.push(data[i]['harga_total']);
						}
					}
				});
			}

			$input.typeahead({source:paket, 
	        autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = paket.indexOf(current);

			    $('#makan_id').val(idpaket[index]);	
			    $('#makan_tarif').val(hargapaket[index]);	

			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

		});

		$('#makan_unit').focus(function(){
			var $input = $('#makan_unit');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>bersalin/invoicenonbpjs/get_master_dept',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_dept']);
						iddata.push(data[i]['dept_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#idUnit').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
		
		$('#makan_onfaktur').keyup(function(){
			var onfaktur = $(this).val();
			var tarif = $('#makan_tarif').val();

			$('#makan_total').val(Number(tarif)+Number(onfaktur));
		});

		$('#makan_onfaktur').change(function(){
			var onfaktur = $(this).val();
			var tarif = $('#makan_tarif').val();

			$('#makan_total').val(Number(tarif)+Number(onfaktur));
		});

		$('#makan_tarif').change(function(){
			var tarif = $(this).val();
			var onfaktur = $('#makan_onfaktur').val();

			$('#makan_total').val(Number(tarif)+Number(onfaktur));
		});

		$(document).on('click','.hapusTindakanMakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#visit_id').val();

			$.ajax({
				type:'POST',
				url:'<?php echo base_url() ?>bersalin/invoicenonbpjs/hapus_tagihanmakan/'+id,
				success:function(data){

					jumlahmakan = data.length;

					$('#tbody_ttakomodasi').empty();

					if(data.length!=0){
						var no = 0;

						for(var i = 0 ; i<data.length; i++){
							no++;
							$('#tbody_ttakomodasi').append(
								'<tr>'+
									'<td>'+no+'</td>'+
									'<td>'+data[i]['nama_paket']+'</td>'+
									'<td>'+data[i]['nama_dept']+'</td>'+
									'<td style="text-align:right;">'+
										'<input type="hidden" class="tarif_makan" value="'+data[i]['tarif']+'">'+
										'<input type="hidden" class="realtotal_makan" value="'+data[i]['jumlah']+'">'+
										''+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
									'<td><input type="number" class="form-control input-sm faktur_makan" style="width:80px" name="onfakturakomodasi" value="'+data[i]['on_faktur']+'"></td>'+
									'<td style="text-align:right;">'+
										'<input type="hidden" class="total_makan" value="'+data[i]['jumlah']+'">'+
										''+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
									'<td style="text-align:center">'+
										'<a style="cursor:pointer" class="hapusTindakanMakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
										'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
									'</td>'+
								'</tr>'
							);

							total += Number(data[i]['jumlah']);
							kekurangan += Number(data[i]['jumlah']);
							$('#realtotaltagihan').val(total)
							$('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
							$('#deposit').text(deposit);
							$('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
						}
					}else{
						$('#tbody_ttakomodasi').append(
							'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Makan</td></tr>'
						);
					}
				},error:function(data){
					console.log(data);
				}
			});
		});
		
		$('#tbody_ttkamar').on('change','tr td .checktarif', function(){
			var bol = this.checked;
			var lain = Number($(this).closest('tr').find('.kamar_totallain').val());
			var tarifdb = Number($(this).closest('tr').find('.kamar_tarifdb').val());
			var kfaktur = Number($(this).closest('tr').find('.faktur_kamar').val());

			if(bol){
				$(this).closest('tr').find('.kamar_totallain').attr('readonly',false);
				$(this).closest('tr').find('.kamar_realtotal').val(lain);
				$(this).closest('tr').find('.total_tagihankamar').val(lain+kfaktur);
				$(this).closest('tr').find('td').eq(8).text((lain+kfaktur).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			}else{
				$(this).closest('tr').find('.kamar_totallain').attr('readonly',true);
				$(this).closest('tr').find('.kamar_totallain').val('0');

				$(this).closest('tr').find('.kamar_realtotal').val(tarifdb);
				$(this).closest('tr').find('.total_tagihankamar').val(tarifdb+kfaktur);
				$(this).closest('tr').find('td').eq(8).text((tarifdb+kfaktur).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			}

			sumTotal()
		});

		$('#tbody_ttkamar').on('keyup','tr td .kamar_totallain', function(){
			var hargalain = Number($(this).val());
			var kfaktur = Number($(this).closest('tr').find('.faktur_kamar').val());

			$(this).closest('tr').find('.kamar_realtotal').val(hargalain);
			$(this).closest('tr').find('.total_tagihankamar').val(hargalain+kfaktur);
			$(this).closest('tr').find('td').eq(8).text((hargalain+kfaktur).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

			sumTotal()
		});

		$('#tbody_ttkamar').on('keyup','tr td .faktur_kamar', function(){
			var kfaktur = $(this).val();
			var real = $(this).closest('tr').find('.kamar_realtotal').val();
			var tfaktur = Number(real)+Number(kfaktur);

			if(kfaktur == "")
				kfaktur = 0;

			$(this).closest('tr').find('td').eq(8).text((Number(tfaktur)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			$(this).closest('tr').find('.total_tagihankamar').val(tfaktur);

			sumTotal();
		});


		$('#submitInvoice').submit(function(event){
			event.preventDefault();

			$('#tbody_ttkamar tr').each(function(){
				var item = {};
				item[1] = {};
				var bol = $(this).find('.checktarif').checked;

				item[1]['id'] = $(this).find('.getid_kamar').val();
				item[1]['on_faktur'] = $(this).find('.faktur_kamar').val();
				item[1]['tarif_lain'] = 0;

				if(bol)
					item[1]['tarif_lain'] = $(this).find('.kamar_totallain').val();

				$.ajax({
					type:'POST',
					data:item,
					url:'<?php echo base_url() ?>bersalin/invoicebpjs/update_tagihankamar',
					success:function(data){
						console.log(data);
					},error:function(data){
						console.log(data);
					}
				});

			});

			$('#tbody_ttakomodasi tr').each(function(){
				var item = {};
				item[1] = {};
				item[1]['id'] = $(this).find('.getid_makan').val();
				item[1]['on_faktur'] = $(this).find('.faktur_makan').val();

				$.ajax({
					type:'POST',
					data:item,
					url:'<?php echo base_url() ?>bersalin/invoicebpjs/update_tagihanmakan',
					success:function(data){
						console.log(data);
					},error:function(data){
						console.log(data);
					}
				});
			});

			// myALert("Data Berhasil Ditambahkan");
			window.location.href = "<?php echo base_url() ?>bersalin/homebersalin";
		});

		$('#model_bayar').click(function(){
			var total = $('#totaltagihan').text();
			var deposit = $('#deposit').text();
			var kekurangan = $('#kekurangan').text();

			$('#bayar_kekurangan').val($('#nilai_kekurangan').val());
			$('#bayar_total').val($('#nilai_total').val());
			$('#bayar_text_deposit').text(deposit);
			$('#bayar_text_kekurangan').text(kekurangan);
			$('#bayar_text_total').text(total);

		})

		$('#bayar_pembayaran').keyup(function(){
			var kekurangan = Number($('#bayar_kekurangan').val());
			var bayar = Number($(this).val());

			var kembalian = 0;
			kembalian = bayar-kekurangan;

			$('#bayar_kembalian').val(kembalian);
		})

		$('#submit_pembayaran').submit(function(e){
			e.preventDefault();
			var bayar = Number($('#bayar_pembayaran').val());
			var kekurangan = Number($('#bayar_kekurangan').val());
			var visit = $('#visit_id').val();

			var item = {};
			item[1] = {};

			item[1]['no_invoice'] = $('#no_invoice').val();
			item[1]['total_bayar'] = $('#nilai_total').val();

			if(bayar>kekurangan){
				$.ajax({
					type:'POST',
					data:item,
					url:'<?=base_url()?>kasirtindakan/invoicenonbpjs/do_pembayaran/'+visit,
					success:function(data){
						console.log(data);
						alert('data berhasil');
					},error:function(data){
						console.log(data);
						alert('data gagal');
					}
				});

				$('#modal-12').fadeOut();
				$('.md-overlay').fadeOut();
			}
		})
	});

	function sumTotal(){
		var total = 0;
		var deposit = Number($('#getdeposit').val());
		var kekurangan = 0;
		var totalrawat = 0;
		var totalkamar = 0;
		var totalmakan = 0;
		var totaladmisi = 0;
		var totaltunjang = 0;
		var totaloperasi = 0;

		$('.total_tagihanrawat').each(function(){
			totalrawat += Number($(this).val());
		});

		$('.total_tagihankamar').each(function(){
			totalkamar += Number($(this).val());
			console.log($(this).val());
		});

		$('.total_tagihanmakan').each(function(){
			totalmakan += Number($(this).val());
		});

		totaladmisi = Number($('#tarif_admisi').val());
		totaltunjang = Number($('#t_penunjang').val());
		totaloperasi = Number($('#t_operasi').val());
		
		// console.log(totalmakan+" "+totalkamar+" "+totalrawat);

		total = totalkamar+totalmakan+totalrawat+totaladmisi+totaltunjang+totaloperasi;
		kekurangan = total-deposit;

		$('#nilai_total').val(total);
		$('#nilai_kekurangan').val(kekurangan);
		$('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
		$('#deposit').text(deposit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
		$('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
	}
</script>

