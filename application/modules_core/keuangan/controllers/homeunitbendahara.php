<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php');

class Homeunitbendahara extends Operator_base {
	protected $dept_id;

	function __construct(){
		parent:: __construct();
		$this->load->model('m_realisasi');
		$data['page_title'] = "Unit Realisasi";
		$this->load->model('bersalin/m_homebersalin');
		$this->load->model('logistik/m_gudangbarang');
		$this->dept_id = $this->m_realisasi->get_dept_id('UNIT REALISASI')['dept_id'];
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$this->session->set_userdata($data);

		// load template
		$data['content'] = 'unitbendahara/home';
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($this->dept_id);
		$data['riwayatpermintaanbarang'] = $this->m_realisasi->get_riwayatpermintaan($this->dept_id);
		$data['riwayat'] = $this->m_realisasi->get_all_riwayat();
		$data['buku'] = $this->m_realisasi->get_buku_kas();
		$data['last_saldo'] = $this->m_realisasi->get_last_saldo();
		$data['units'] = $this->m_realisasi->unit_has_pendapatan();
		$data['javascript'] = 'unitbendahara/j_realisasi';

		$this->load->view('base/operator/template', $data);
	}

	public function get_buku_by_date()
	{
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tgl1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tgl2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		if ($date1 < $date2) {
			$result = $this->m_realisasi->get_kas_date_filter($date1, $date2);
		}else {
			$result = $this->m_realisasi->get_kas_date_filter($date2, $date1);
		}

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function get_detail_riwayat($id)
	{
		$res = $this->m_realisasi->get_detail_riwayat($id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function search_realisasi()
	{
		$katakunci = $_POST['search'];
		$res = $this->m_realisasi->search_realisasi($katakunci);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function insert_aktivitas()
	{
		$saldo = $this->m_realisasi->get_last_saldo();
		if (empty($saldo)) {
			$saldo=0;
		}

		$saldobaru = $saldo;
		if ($_POST['kategori'] == 'PENERIMAAN') {
			$saldobaru = $saldo['saldo'] + $_POST['total_aktivitas'];
		}else {
			$saldobaru = $saldo['saldo'] - $_POST['total_aktivitas'];
		}

		$tgl = DateTime::createFromFormat('d/m/Y', $_POST['tanggal']);
		$insert = array(
				'tanggal' => $tgl->format('Y-m-d'),
				'kode_perkiraan' => $_POST['kode_perkiraan'],
				'uraian' => $_POST['uraian'],
				'no_faktur' => $_POST['no_faktur'],
				'atas_nama' => $_POST['atas_nama'],
				'kategori' => $_POST['kategori'],
				'jenis_aktivitas' => $_POST['jenis_aktivitas'],
				'nilai_aktivitas' => $_POST['nilai_aktivitas'],
				'ppn' => $_POST['ppn'],
				'pph21' => $_POST['pph21'],
				'pph22' => $_POST['pph22'],
				'total_aktivitas' => $_POST['total_aktivitas'],
				'saldo' => $saldobaru
		);
		$id = $this->m_realisasi->add_aktivitas($insert);
		header('Content-Type: application/json');
	 	echo json_encode($saldobaru);
	}

	public function print_detail_rba($id)
	{
		$data['rba'] = $this->m_realisasi->get_riwayat_by_id($id);
		$data['rba_detail'] = $this->m_realisasi->get_detail_riwayat($id);
		$this->load->view('keuangan/unitbendahara/excel_rba', $data);
	}

	public function print_aktivitas_kas()
	{
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		if ($date1 < $date2) {
			$data['awal'] = $date1;
			$data['akhir'] = $date2;
			$data['kas'] = $this->m_realisasi->get_kas_date_filter($date1, $date2);
		}else {
			$data['awal'] = $date2;
			$data['akhir'] = $date1;
			$data['kas'] = $this->m_realisasi->get_kas_date_filter($date2, $date1);
		}
		$this->load->view('keuangan/unitbendahara/excel_aktivitas_kas', $data);
	}

		/*permintaan logistik*/
	public function get_barang_gudang()
	{
		$katakunci = $_POST['katakunci'];
		$elny2 = $this->m_homebersalin->get_barang_gudang($katakunci,$this->dept_id,'24');
		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_permintaan_barangunit($value='')
	{
		$this->form_validation->set_rules('no_permintaanbarang', 'nomor permitaan', 'required|trim|xss_clean|is_unique[barang_permintaan.no_permintaanbarang]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaanbarang'] = $_POST['no_permintaanbarang'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$insert['dept_id'] = $this->dept_id;

			$val = $_POST['data'];
			$result = $this->m_homebersalin->insert_permintaanbarang($insert);
			if($result){
				foreach ($val as $key) {
					$ins['barang_id'] = $key[8];
					$ins['barang_stok_id'] = $key[7];
					$ins['jumlah_request'] =  $key[9];
					$ins['barang_permintaan_id'] = $result;

					$elny = $this->m_homebersalin->insert_detail_permintaanbarang($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function input_in_outbarang($value='')
	{
		$insert['barang_detail_id'] = $_POST['barang_detail_id'];
		$tgl = DateTime::createFromFormat('d/m/Y H:i', $_POST['tanggal']);
		$insert['tanggal'] = $tgl->format('Y-m-d H:i');
		$insert['is_out'] = $_POST['is_out'];
		$insert['jumlah'] = $_POST['jumlah'];
		$insert['keterangan'] = $_POST['keterangan'];
		$insert['barang_dept_id'] = $this->dept_id;

		$res = $this->m_gudangbarang->input_in_out($insert);
		if ($res) {
			$ins['barang_detail_id'] = $_POST['barang_detail_id'];
			$ins['dept_id'] = $this->dept_id;
			$ins['stok'] = $_POST['sisa'];
			$ins['tanggal_stok'] = date('Y-m-d H:i:s');
			$ins['keterangan_stok'] = "IN - OUT";

			$res = $this->m_gudangbarang->input_riwayat_out($ins);
			if ($res) {
				$message = "true";
			}else{
				$message = "false";
			}
		}else{
			$message = "false";
		}

		header('Content-Type: application/json');
	 	echo(json_encode($message));
	}

	public function get_detail_inventori($id)
	{
		$res = $this->m_gudangbarang->get_detail_inventori($id, $this->dept_id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function get_detailpersetujuan($id)
	{
		$res = $this->m_realisasi->get_detailpersetujuan($id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function print_riwayat_permintaan($id)
	{
		$data['result'] = $this->m_realisasi->get_riwayatpermintaan_by_id($id);
		$data['detail'] = $this->m_realisasi->get_detailpersetujuan($id);
		
		$this->load->view('keuangan/unitbendahara/excel_riwayat_permintaan', $data);	
	}
	/*akhir logistik*/

	 //rekap pendapatan per jenis
  public function print_pendapatan_per_jenis()
	{
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_jenis1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_jenis2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		$jumlah = array();
		$i=0;

		$jenis1 = $this->m_realisasi->get_kategori_tindakan();
		foreach ($jenis1 as $value) {
			$jenis = $value['jenis_pendapatan'];
			$jumlah[$i]['jenis'] = $jenis;
			if ($date1 < $date2) {
				$jumlah[$i]['jumlah'] = $this->m_realisasi->get_rekap_per_jenis($jenis, $date1, $date2);
			}else {
				$jumlah[$i]['jumlah'] = $this->m_realisasi->get_rekap_per_jenis($jenis, $date2, $date1);
			}
			$i++;
		}

		$jenis2 = $this->m_realisasi->get_jenis_pendapatan();

		foreach ($jenis2 as $value) {
			$jenis = $value['jenis_pendapatan'];
			$jumlah[$i]['jenis'] = $jenis;
			if ($date1 < $date2) {
				$jumlah[$i]['jumlah'] = $this->m_realisasi->get_pendapatan_by_jenis_date($jenis, $date1, $date2);
			}else {
				$jumlah[$i]['jumlah'] = $this->m_realisasi->get_pendapatan_by_jenis_date($jenis, $date2, $date1);
			}
			$i++;
		}

		if ($date1 < $date2) {
			$data['awal'] = $date1;
			$data['akhir'] = $date2;
		}else {
			$data['awal'] = $date2;
			$data['akhir'] = $date1;
		}

		$data['rekap_pendapatan'] = $jumlah;
		$this->load->view('keuangan/unitbendahara/excel_pendapatan_per_jenis', $data);
	}

  public function print_rekap_unit()
  {
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_unit1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_unit2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		//cek unit: rawat inap, poliklinik, penunjang, pendukung
		$unit = $_POST['select_jenis'];
		$jenis = $this->m_realisasi->cek_unit($unit);

		if (strtoupper($jenis['jenis']) == 'RAWAT INAP') {
			//ambil dari tagihan perawatan, tagihan akomodasi
			if ($date1 < $date2) {
				$rekap = $this->m_realisasi->get_rekap_ri($unit, $date1, $date2);
			}else {
				$rekap = $this->m_realisasi->get_rekap_ri($unit, $date2, $date1);
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'POLIKLINIK') {
			//ambil dari tagihan perawatan
			if ($date1 < $date2) {
				$rekap = $this->m_realisasi->get_rekap_poli($unit, $date1, $date2);
			}else {
				$rekap = $this->m_realisasi->get_rekap_poli($unit, $date2, $date1);
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'PENUNJANG') {
			//ambil dari tagihan operasi, tagihan penunjang
			if (strtoupper($jenis['nama_dept']) == 'KAMAR OPERASI') {
				if ($date1 < $date2) {
					$rekap = $this->m_realisasi->get_rekap_operasi($date1, $date2);
				}else {
					$rekap = $this->m_realisasi->get_rekap_operasi($date2, $date1);
				}
			}else {
				if ($date1 < $date2) {
					$rekap = $this->m_realisasi->get_rekap_penunjang($unit, $date1, $date2);
				}else {
					$rekap = $this->m_realisasi->get_rekap_penunjang($unit, $date2, $date1);
				}
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'PENDUKUNG') {
			//ambil dari jenazah perawatan, mediko legal, keuangan pendapatan (utd), tagihan makan (instalasi gizi)
			if (strtoupper($jenis['nama_dept']) == 'PERAWATAN JENAZAH') {
				if ($date1 < $date2) {
					$rekap = $this->m_realisasi->get_rekap_jenazah($date1, $date2);
				}else {
					$rekap = $this->m_realisasi->get_rekap_jenazah($date2, $date1);
				}
			}elseif (strtoupper($jenis['nama_dept']) == 'MEDIKO LEGAL') {
				if ($date1 < $date2) {
					$rekap = $this->m_realisasi->get_rekap_mediko($date1, $date2);
				}else {
					$rekap = $this->m_realisasi->get_rekap_mediko($date2, $date1);
				}
			}elseif (strtoupper($jenis['nama_dept']) == 'INSTALASI GIZI') {
				if ($date1 < $date2) {
					$rekap = $this->m_realisasi->get_rekap_makan($date1, $date2);
				}else {
					$rekap = $this->m_realisasi->get_rekap_makan($date2, $date1);
				}
			}
		}

		if ($date1 < $date2) {
			$data['awal'] = $date1;
			$data['akhir'] = $date2;
		}else {
			$data['awal'] = $date2;
			$data['akhir'] = $date1;
		}
		$data['unit'] = $jenis['nama_dept'];
		$data['rekap_unit'] = $rekap;
		$this->load->view('keuangan/unitbendahara/excel_pendapatan_per_unit', $data);
  }

	public function print_rekap_jsjpbakhp()
	{
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_unit1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_unit2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		//cek unit: rawat inap, poliklinik, penunjang, pendukung
		$unit = $_POST['select_jenis'];
		$jenis = $this->m_realisasi->cek_unit($unit);

		if (strtoupper($jenis['jenis']) == 'RAWAT INAP') {
			//ambil dari tagihan perawatan, kamar gak ada js jp bakhp
			if ($date1 < $date2) {
				$rekap = $this->m_realisasi->get_rekap_poli($unit, $date1, $date2);
			}else {
				$rekap = $this->m_realisasi->get_rekap_poli($unit, $date2, $date1);
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'POLIKLINIK') {
			//ambil dari tagihan perawatan
			if ($date1 < $date2) {
				$rekap = $this->m_realisasi->get_rekap_poli($unit, $date1, $date2);
			}else {
				$rekap = $this->m_realisasi->get_rekap_poli($unit, $date2, $date1);
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'PENUNJANG') {
			//ambil dari tagihan operasi, tagihan penunjang
			if (strtoupper($jenis['nama_dept']) == 'KAMAR OPERASI') {
				if ($date1 < $date2) {
					$rekap = $this->m_realisasi->get_rekap_operasi($date1, $date2);
				}else {
					$rekap = $this->m_realisasi->get_rekap_operasi($date2, $date1);
				}
			}else {
				if ($date1 < $date2) {
					$rekap = $this->m_realisasi->get_rekap_penunjang($unit, $date1, $date2);
				}else {
					$rekap = $this->m_realisasi->get_rekap_penunjang($unit, $date2, $date1);
				}
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'PENDUKUNG') {
			//ambil dari jenazah perawatan, mediko legal, keuangan pendapatan (utd), tagihan makan (instalasi gizi)
			if (strtoupper($jenis['nama_dept']) == 'PERAWATAN JENAZAH') {
				if ($date1 < $date2) {
					$rekap = $this->m_realisasi->get_rekap_jenazah($date1, $date2);
				}else {
					$rekap = $this->m_realisasi->get_rekap_jenazah($date2, $date1);
				}
			}elseif (strtoupper($jenis['nama_dept']) == 'MEDIKO LEGAL') {
				if ($date1 < $date2) {
					$rekap = $this->m_realisasi->get_rekap_mediko($date1, $date2);
				}else {
					$rekap = $this->m_realisasi->get_rekap_mediko($date2, $date1);
				}
			}
		}

		if ($date1 < $date2) {
			$data['awal'] = $date1;
			$data['akhir'] = $date2;
		}else {
			$data['awal'] = $date2;
			$data['akhir'] = $date1;
		}

		$data['unit'] = $jenis['nama_dept'];
		$data['rekap_unit'] = $rekap;
		$this->load->view('keuangan/unitbendahara/excel_pendapatan_per_jsjpbakhp', $data);
	}

}
