<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homeunitpendapatan extends Operator_base {
	protected $ses;
	protected $dept_id;
	function __construct() {
		parent:: __construct();

		$this->load->model('logistik/m_gudangbarang');
		$this->load->model('m_pendapatan');
		$this->load->model('bersalin/m_homebersalin');
		$this->load->model('farmasi/m_obat');
		$this->dept_id = $this->m_gudangbarang->get_dept_id('UNIT PENDAPATAN')['dept_id'];
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$data['page_title'] = 'Unit Pendapatan';
		$this->session->set_userdata($data);

		$data['jaspel'] = $this->m_pendapatan->get_jaspel();
		$data['jassep_apotek'] = $this->m_pendapatan->get_jasa_resep_apotek();
		$data['jp_kamar_operasi'] = $this->m_pendapatan->get_jasa_kamar_operasi();
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($this->dept_id);

		$data['pendapatan'] = $this->m_pendapatan->get_all_pendapatan();
		$data['units'] = $this->m_pendapatan->unit_has_pendapatan();
		$data['units2'] = $this->m_pendapatan->unit_has_pendapatan2();
		$data['content'] = 'unitpendapatan/home';
		$data['javascript'] = 'unitpendapatan/j_home';
		$this->load->view('base/operator/template', $data);
	}

	public function get_all_unit()
	{
		$result = $this->m_pendapatan->get_all_unit();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	/*pendatapan*/
	public function get_pendapatan_by_date()
	{
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tgl1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tgl2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		if ($date1 < $date2) {
			$result = $this->m_pendapatan->get_pendapatan_filter($date1, $date2);
		}else {
			$result = $this->m_pendapatan->get_pendapatan_filter($date2, $date1);
		}

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function search_pendapatan()
	{
		$katakunci = $_POST['search'];
		$res = $this->m_pendapatan->search_pendapatan($katakunci);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function insert_pendapatan()
	{
		$tgl = DateTime::createFromFormat('d/m/Y', $_POST['tanggal']);
		$insert = array(
				'tanggal' => $tgl->format('Y-m-d'),
				'kategori_pendapatan' => $_POST['kategori_pendapatan'],
				'jenis_pendapatan' => $_POST['jenis_pendapatan'],
				'instansi_pembayar' => $_POST['instansi_pembayar'],
				'jumlah' => $_POST['jumlah']
		);
		$id = $this->m_pendapatan->add_pendapatan($insert);
		header('Content-Type: application/json');
	 	echo json_encode($id);
	}

	public function delete_pendapatan($id)
	{
		$res = $this->m_pendapatan->delete_pendapatan($id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}
	/*end of pendapatan*/

	/*jaspel*/
	public function filter_jaspel()
	{
		$insert['mulai'] = $_POST['mulai'];
		$insert['akhir'] = $_POST['akhir'];
		$insert['cara_bayar'] = $_POST['cara_bayar'];
		$insert['petugas_id'] = $_POST['petugas_id'];
		$unit = $_POST['unit'];
		$result = $this->m_pendapatan->get_filter_jaspel($insert,$unit);
		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function excel_jasa_pelayanan()
	{
		$insert['mulai'] = $this->fdate_db($this->input->post('start'));
		$insert['akhir'] = $this->fdate_db($this->input->post('end'));
		$insert['cara_bayar'] = $this->input->post('cara_bayar');
		$insert['petugas_id'] = $this->input->post('id_petugas_jaspel');
		$unit = $this->input->post('id_unit_jaspel');
		$result = $this->m_pendapatan->get_filter_jaspel($insert,$unit);

		$data['jaspel'] = $result;
		if ($unit!='') {
			$data['nama_dept'] = $this->m_pendapatan->get_dept_nama($unit)['nama_dept'];
		}else{
			$data['nama_dept'] = 'SEMUA UNIT';
		}

		$data['awal'] = $this->input->post('start');
		$data['akhir'] = $this->input->post('end');

		$this->load->view('bersalin/excel_jaspel', $data);
	}

	/*akhir jaspel*/


	/*jaspel apotek*/
	public function filter_jasa_resep()
	{
		$insert['start'] = $_POST['start'];
		$insert['end'] = $_POST['end'];
		$insert['cara_bayar'] =  $_POST['cara_bayar'];
		$insert['unit'] = $_POST['unit'];
		$insert['paramedis'] = $_POST['paramedis'];
		$insert['now'] = date('Y-m-d');

		$result = $this->m_pendapatan->get_filter_jasa_resep_apotek($insert);

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function excel_jasa_resep()
	{
		$insert['start'] = $this->fdate_db($this->input->post('start'));
		$insert['end'] = $this->fdate_db($this->input->post('end'));
		$insert['cara_bayar'] =  $this->input->post('cara_bayar');
		$insert['unit'] = $this->input->post('ap_unit_id');
		$insert['paramedis'] = $this->input->post('ap_paramedis_id');
		$insert['now'] = date('Y-m-d');

		$result = $this->m_pendapatan->get_filter_jasa_resep_apotek($insert);

		$data['jassep'] = $result;
		$data['awal'] = $this->input->post('start');
		$data['akhir'] = $this->input->post('end');
		if ($insert['unit']!='') {
			$data['nama_dept'] = $this->m_pendapatan->get_dept_nama($insert['unit'])['nama_dept'];
		}else{
			$data['nama_dept'] = 'SEMUA UNIT';
		}
		$this->load->view('keuangan/unitpendapatan/excel_jasa_resep', $data);
	}

	/*akhir jaspel apotek*/

	/*jasa kamar operasi*/
	public function filter_jasa_kamar_operasi($value='')
	{
		$insert['start'] = $_POST['start'];
		$insert['end'] = $_POST['end'];
		$insert['dokter'] = $_POST['dokter'];
		$insert['cara_bayar'] = $_POST['cara_bayar'];
		$result = $this->m_pendapatan->get_filter_kamar_operasi($insert);

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function excel_kamar_operasi($value='')
	{
		$insert['start'] = $this->fdate_db($this->input->post('start'));
		$insert['end'] = $this->fdate_db($this->input->post('end'));
		$insert['dokter'] = $this->input->post('ko_paramedis_id');
		$insert['cara_bayar'] = $this->input->post('carabayar');
		$result = $this->m_pendapatan->get_filter_kamar_operasi($insert);

		$data['jp_kamar_operasi'] = $result;
		$data['awal'] = $this->input->post('start');
		$data['akhir'] = $this->input->post('end');
		$this->load->view('keuangan/unitpendapatan/excel_jasa_kamar_operasi', $data);
	}
	/*akhir jasa kamar operasi*/

	/*logistik*/
	public function get_detail_inventori($id)
	{
		$res = $this->m_gudangbarang->get_detail_inventori($id, $this->dept_id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}
	public function input_in_outbarang($value='')
	{
		$insert['barang_detail_id'] = $_POST['barang_detail_id'];
		$tgl = DateTime::createFromFormat('d/m/Y H:i', $_POST['tanggal']);
		$insert['tanggal'] = $tgl->format('Y-m-d H:i');
		$insert['is_out'] = $_POST['is_out'];
		$insert['jumlah'] = $_POST['jumlah'];
		$insert['keterangan'] = $_POST['keterangan'];
		$insert['barang_dept_id'] = $this->dept_id;

		$res = $this->m_gudangbarang->input_in_out($insert);
		if ($res) {
			$ins['barang_detail_id'] = $_POST['barang_detail_id'];
			$ins['dept_id'] = $this->dept_id;
			$ins['stok'] = $_POST['sisa'];
			$ins['keterangan_stok'] = "IN - OUT";

			$res = $this->m_gudangbarang->input_riwayat_out($ins);
			if ($res) {
				$message = "true";
			}else{
				$message = "false";
			}
		}else{
			$message = "false";
		}

		header('Content-Type: application/json');
	 	echo(json_encode($message));
	}

	public function get_barang_gudang()
	{
		$katakunci = $_POST['katakunci'];
		$elny2 = $this->m_homebersalin->get_barang_gudang($katakunci,$this->dept_id,'24');
		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_permintaan_barangunit($value='')
	{
		$this->form_validation->set_rules('no_permintaanbarang', 'nomor permitaan', 'required|trim|xss_clean|is_unique[barang_permintaan.no_permintaanbarang]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaanbarang'] = $_POST['no_permintaanbarang'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$insert['dept_id'] = $this->dept_id;

			$val = $_POST['data'];
			$result = $this->m_homebersalin->insert_permintaanbarang($insert);
			if($result){
				foreach ($val as $key) {
					$ins['barang_id'] = $key[8];
					$ins['barang_stok_id'] = $key[7];
					$ins['jumlah_request'] =  $key[9];
					$ins['barang_permintaan_id'] = $result;

					$elny = $this->m_homebersalin->insert_detail_permintaanbarang($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	/*logistik akhir*/

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i:s',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}


	/*rekap pendapatan*/
	//pendapatan pelayanan-non pelayanan
	public function print_pendapatan()
	{
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		$jumlah = array();
		$jenis = $this->m_pendapatan->get_jenis_pendapatan();
		$i=0;

		foreach ($jenis as $value) {
			$jenis = $value['jenis_pendapatan'];
			$jumlah[$i]['jenis'] = $jenis;
			if ($date1 < $date2) {
				$jumlah[$i]['jumlah'] = $this->m_pendapatan->get_pendapatan_by_jenis_date($jenis, $date1, $date2);
			}else {
				$jumlah[$i]['jumlah'] = $this->m_pendapatan->get_pendapatan_by_jenis_date($jenis, $date2, $date1);
			}
			$i++;
		}

		if ($date1 < $date2) {
			$data['awal'] = $date1;
			$data['akhir'] = $date2;
		}else {
			$data['awal'] = $date2;
			$data['akhir'] = $date1;
		}
		$data['pendapatan'] = $jumlah;
		$this->load->view('keuangan/unitpendapatan/excel_pendapatan', $data);
	}

	//rekap pendapatan per jenis penerimaan
	public function print_pendapatan_per_jenis()
	{
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_jenis1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_jenis2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		$jumlah = array();
		$i=0;

		//ambil jumlah pendapatan per kategori tindakan
		$jenis1 = $this->m_pendapatan->get_kategori_tindakan();
		foreach ($jenis1 as $value) {
			$jenis = $value['jenis_pendapatan'];
			$jumlah[$i]['jenis'] = $jenis;

			if ($jenis == 'Administrasi') {
				if ($date1 < $date2) {
					$jumlah[$i]['jumlah'] = $this->m_pendapatan->get_rekap_administrasi($date1, $date2);
				}else {
					$jumlah[$i]['jumlah'] = $this->m_pendapatan->get_rekap_administrasi($date2, $date1);
				}
			}else {
				if ($date1 < $date2) {
					$jumlah[$i]['jumlah'] = $this->m_pendapatan->get_rekap_per_jenis($jenis, $date1, $date2);
				}else {
					$jumlah[$i]['jumlah'] = $this->m_pendapatan->get_rekap_per_jenis($jenis, $date2, $date1);
				}
			}

			$i++;
		}

		//ambil jumlah pendapatan per jenis pendapatan(dari unit pendapatan)
		$jenis2 = $this->m_pendapatan->get_jenis_pendapatan();
		foreach ($jenis2 as $value) {
			$jenis = $value['jenis_pendapatan'];
			$jumlah[$i]['jenis'] = $jenis;
			if ($date1 < $date2) {
				$jumlah[$i]['jumlah'] = $this->m_pendapatan->get_pendapatan_by_jenis_date($jenis, $date1, $date2);
			}else {
				$jumlah[$i]['jumlah'] = $this->m_pendapatan->get_pendapatan_by_jenis_date($jenis, $date2, $date1);
			}
			$i++;
		}

		if ($date1 < $date2) {
			$data['awal'] = $date1;
			$data['akhir'] = $date2;
		}else {
			$data['awal'] = $date2;
			$data['akhir'] = $date1;
		}

		$data['rekap_pendapatan'] = $jumlah;
		$this->load->view('keuangan/unitpendapatan/excel_pendapatan_per_jenis', $data);
	}

	//rekap pendapatan per unit
	public function print_rekap_unit()
	{
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_unit1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_unit2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		//cek unit: rawat inap, poliklinik, penunjang, pendukung
		$unit = $_POST['select_jenis'];
		$jenis = $this->m_pendapatan->cek_unit($unit);

		if (strtoupper($jenis['jenis']) == 'RAWAT INAP') {
			//ambil dari tagihan perawatan, tagihan akomodasi
			if ($date1 < $date2) {
				$rekap = $this->m_pendapatan->get_rekap_ri($unit, $date1, $date2);
			}else {
				$rekap = $this->m_pendapatan->get_rekap_ri($unit, $date2, $date1);
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'POLIKLINIK') {
			//ambil dari tagihan perawatan
			if ($date1 < $date2) {
				$rekap = $this->m_pendapatan->get_rekap_poli($unit, $date1, $date2);
			}else {
				$rekap = $this->m_pendapatan->get_rekap_poli($unit, $date2, $date1);
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'PENUNJANG') {
			//ambil dari tagihan operasi, tagihan penunjang
			if (strtoupper($jenis['nama_dept']) == 'KAMAR OPERASI') {
				if ($date1 < $date2) {
					$rekap = $this->m_pendapatan->get_rekap_operasi($date1, $date2);
				}else {
					$rekap = $this->m_pendapatan->get_rekap_operasi($date2, $date1);
				}
			}else {
				if ($date1 < $date2) {
					$rekap = $this->m_pendapatan->get_rekap_penunjang($unit, $date1, $date2);
				}else {
					$rekap = $this->m_pendapatan->get_rekap_penunjang($unit, $date2, $date1);
				}
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'PENDUKUNG') {
			//ambil dari jenazah perawatan, mediko legal, keuangan pendapatan (utd), tagihan makan (instalasi gizi)
			if (strtoupper($jenis['nama_dept']) == 'PERAWATAN JENAZAH') {
				if ($date1 < $date2) {
					$rekap = $this->m_pendapatan->get_rekap_jenazah($date1, $date2);
				}else {
					$rekap = $this->m_pendapatan->get_rekap_jenazah($date2, $date1);
				}
			}elseif (strtoupper($jenis['nama_dept']) == 'MEDIKO LEGAL') {
				if ($date1 < $date2) {
					$rekap = $this->m_pendapatan->get_rekap_mediko($date1, $date2);
				}else {
					$rekap = $this->m_pendapatan->get_rekap_mediko($date2, $date1);
				}
			}elseif (strtoupper($jenis['nama_dept']) == 'INSTALASI GIZI') {
				if ($date1 < $date2) {
					$rekap = $this->m_pendapatan->get_rekap_makan($date1, $date2);
				}else {
					$rekap = $this->m_pendapatan->get_rekap_makan($date2, $date1);
				}
			}
		}

		if ($date1 < $date2) {
			$data['awal'] = $date1;
			$data['akhir'] = $date2;
		}else {
			$data['awal'] = $date2;
			$data['akhir'] = $date1;
		}
		$data['unit'] = $jenis['nama_dept'];
		$data['rekap_unit'] = $rekap;
		$this->load->view('keuangan/unitpendapatan/excel_pendapatan_per_unit', $data);
	}

	public function print_rekap_jsjpbakhp()
	{
		$tgl1 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_rekap1']);
		$tgl2 = DateTime::createFromFormat('d/m/Y', $_POST['tanggal_rekap2']);

		$date1 = $tgl1->format('Y-m-d');
		$date2 = $tgl2->format('Y-m-d');

		//cek unit: rawat inap, poliklinik, penunjang, pendukung
		$unit = $_POST['select_jenis'];
		$jenis = $this->m_pendapatan->cek_unit($unit);

		if (strtoupper($jenis['jenis']) == 'RAWAT INAP') {
			//ambil dari tagihan perawatan, kamar gak ada js jp bakhp
			if ($date1 < $date2) {
				$rekap = $this->m_pendapatan->get_rekap_poli($unit, $date1, $date2);
			}else {
				$rekap = $this->m_pendapatan->get_rekap_poli($unit, $date2, $date1);
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'POLIKLINIK') {
			//ambil dari tagihan perawatan
			if ($date1 < $date2) {
				$rekap = $this->m_pendapatan->get_rekap_poli($unit, $date1, $date2);
			}else {
				$rekap = $this->m_pendapatan->get_rekap_poli($unit, $date2, $date1);
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'PENUNJANG') {
			//ambil dari tagihan operasi, tagihan penunjang
			if (strtoupper($jenis['nama_dept']) == 'KAMAR OPERASI') {
				if ($date1 < $date2) {
					$rekap = $this->m_pendapatan->get_rekap_operasi($date1, $date2);
				}else {
					$rekap = $this->m_pendapatan->get_rekap_operasi($date2, $date1);
				}
			}else {
				if ($date1 < $date2) {
					$rekap = $this->m_pendapatan->get_rekap_penunjang($unit, $date1, $date2);
				}else {
					$rekap = $this->m_pendapatan->get_rekap_penunjang($unit, $date2, $date1);
				}
			}
		}

		elseif (strtoupper($jenis['jenis']) == 'PENDUKUNG') {
			//ambil dari jenazah perawatan, mediko legal, keuangan pendapatan (utd), tagihan makan (instalasi gizi)
			if (strtoupper($jenis['nama_dept']) == 'PERAWATAN JENAZAH') {
				if ($date1 < $date2) {
					$rekap = $this->m_pendapatan->get_rekap_jenazah($date1, $date2);
				}else {
					$rekap = $this->m_pendapatan->get_rekap_jenazah($date2, $date1);
				}
			}elseif (strtoupper($jenis['nama_dept']) == 'MEDIKO LEGAL') {
				if ($date1 < $date2) {
					$rekap = $this->m_pendapatan->get_rekap_mediko($date1, $date2);
				}else {
					$rekap = $this->m_pendapatan->get_rekap_mediko($date2, $date1);
				}
			}
		}

		if ($date1 < $date2) {
			$data['awal'] = $date1;
			$data['akhir'] = $date2;
		}else {
			$data['awal'] = $date2;
			$data['akhir'] = $date1;
		}

		$data['unit'] = $jenis['nama_dept'];
		$data['rekap_unit'] = $rekap;
		$this->load->view('keuangan/unitpendapatan/excel_pendapatan_per_jsjpbakhp', $data);
	}
	/*end of rekap pendapatan*/
}
