<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php');

class Homeunitperencanaan extends Operator_base {
	protected $dept_id;

	function __construct(){
		parent:: __construct();
		$this->load->model('m_perencanaan');
		$this->load->model('bersalin/m_homebersalin');
		$this->load->model('logistik/m_gudangbarang');
		$data['page_title'] = "Unit Perencanaan";
		$this->dept_id = $this->m_perencanaan->get_dept_id('UNIT PERENCANAAN')['dept_id'];
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$this->session->set_userdata($data);

		// load template
		$data['content'] = 'unitperencanaan/home';
		$data['perencanaan_gudang'] = $this->m_perencanaan->get_all_perencanaan_gudang();
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($this->dept_id);
		$data['riwayat'] = $this->m_perencanaan->get_all_riwayat();
		$data['totalriwayat'] = $this->m_perencanaan->get_riwayat_total();
		$data['javascript'] = 'unitperencanaan/j_perencanaan';

		$this->load->view('base/operator/template', $data);
	}

	public function get_all_riwayat()
	{
		$res = $this->m_perencanaan->get_all_riwayat();
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function get_detail_riwayat($id)
	{
		$res = $this->m_perencanaan->get_detail_riwayat($id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function insert_rba()
	{
		//insert rba
		$data = $_POST['data'];
		$tgl = DateTime::createFromFormat('d/m/Y', $_POST['tanggal']);
		$insert = array(
				'no_formulir' => $_POST['no_formulir'],
				'tanggal' => $tgl->format('Y-m-d'),
				'thn_anggaran' => $_POST['thn_anggaran'],
				'urusan_pemerintahan' => $_POST['urusan_pemerintahan'],
				'organizer' => $_POST['organizer'],
				'program' => $_POST['program'],
				'kegiatan' => $_POST['kegiatan'],
				'lokasi' => $_POST['lokasi'],
				'sumber_dana' => $_POST['sumber_dana']
		);

		$id = $this->m_perencanaan->add_rencana($insert);

		if($id){
			foreach ($data as $value) {
				$insdetail = array(
					'rba_id' => $id,
					'kode_rekening' => $value[0],
					'belanja' => $value[2],
					'uraian' => $value[4],
					'volume' => $value[16],
					'satuan' => $value[8],
					'harga_satuan' => $value[17],
					'jumlah' => $value[18]
				);
				$res = $this->m_perencanaan->add_rencana_detail($insdetail);
				if ($res) {
					$result = $id;
				}
			}
		}else{
			$result = array(
				'message' => 'Gagal menyimpan data',
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function print_detail_rba($id)
	{
		$data['rba'] = $this->m_perencanaan->get_riwayat_by_id($id);
		$data['rba_detail'] = $this->m_perencanaan->get_detail_riwayat($id);
		$this->load->view('keuangan/unitperencanaan/excel_rba', $data);
	}

	public function print_pengadaan($jenis, $id)
	{
		$data['result'] = $this->m_perencanaan->get_perencanaan_gudang_by_id($jenis, $id);
		$data['detail'] = $this->m_perencanaan->get_detail_pengadaan($jenis, $id);
		
		$this->load->view('keuangan/unitperencanaan/excel_perencanaan', $data);
	}

	public function view_detail_pengadaan($jenis,$id)
	{
		$result = $this->m_perencanaan->get_detail_pengadaan($jenis, $id);
		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

		/*permintaan logistik*/
	public function get_barang_gudang()
	{
		$katakunci = $_POST['katakunci'];
		$elny2 = $this->m_homebersalin->get_barang_gudang($katakunci,$this->dept_id,'24');
		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_permintaan_barangunit($value='')
	{
		$this->form_validation->set_rules('no_permintaanbarang', 'nomor permitaan', 'required|trim|xss_clean|is_unique[barang_permintaan.no_permintaanbarang]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaanbarang'] = $_POST['no_permintaanbarang'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$insert['dept_id'] = $this->dept_id;

			$val = $_POST['data'];
			$result = $this->m_homebersalin->insert_permintaanbarang($insert);
			if($result){
				foreach ($val as $key) {
					$ins['barang_id'] = $key[8];
					$ins['barang_stok_id'] = $key[7];
					$ins['jumlah_request'] =  $key[9];
					$ins['barang_permintaan_id'] = $result;

					$elny = $this->m_homebersalin->insert_detail_permintaanbarang($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function input_in_outbarang($value='')
	{
		$insert['barang_detail_id'] = $_POST['barang_detail_id'];
		$tgl = DateTime::createFromFormat('d/m/Y H:i', $_POST['tanggal']);
		$insert['tanggal'] = $tgl->format('Y-m-d H:i');
		$insert['is_out'] = $_POST['is_out'];
		$insert['jumlah'] = $_POST['jumlah'];
		$insert['keterangan'] = $_POST['keterangan'];
		$insert['barang_dept_id'] = $this->dept_id;

		$res = $this->m_gudangbarang->input_in_out($insert);
		if ($res) {
			$ins['barang_detail_id'] = $_POST['barang_detail_id'];
			$ins['dept_id'] = $this->dept_id;
			$ins['stok'] = $_POST['sisa'];
			$ins['tanggal_stok'] = date('Y-m-d H:i:s');
			$ins['keterangan_stok'] = "IN - OUT";

			$res = $this->m_gudangbarang->input_riwayat_out($ins);
			if ($res) {
				$message = "true";
			}else{
				$message = "false";
			}
		}else{
			$message = "false";
		}

		header('Content-Type: application/json');
	 	echo(json_encode($message));
	}

	public function get_detail_inventori($id)
	{
		$res = $this->m_gudangbarang->get_detail_inventori($id, $this->dept_id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}


	/*akhir logistik*/

}
