<?php
	/**
	*
	*/
	class m_pendapatan extends CI_Model
	{

		function __construct()
		{
			parent::__construct();
		}

		public function get_all_unit()
		{
			return $this->db->query("SELECT * from master_dept")->result_array();
		}

		/*jaspel*/
		public function get_jaspel($dept_id='')
	    {
	    	$sql = "SELECT *, substr(vc.waktu_tindakan, 1,10) as tanggal
	    			from (select *, count(tindakan_id) as jlh_tindakan,
	    						sum(jp) as total from visit_care group by paramedis, tindakan_id) vc
	    			left join master_tindakan_detail mt on mt.detail_id = vc.tindakan_id
	    			left join master_tindakan tt on tt.tindakan_id = mt.tindakan_id
	    			left join (select ri_id,cara_bayar from visit_ri) ri on ri.ri_id = vc.sub_visit
					left join petugas p on p.petugas_id = vc.paramedis
					left join master_dept md on vc.dept_id = md.dept_id";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
	    }

	    public function get_filter_jaspel($insert,$dept_id)
	    {
	    	$mulai = $insert['mulai'];
	    	$akhir = $insert['akhir'];
	    	$cara_bayar = $insert['cara_bayar'];
	    	$petugas_id = $insert['petugas_id'];
	    	$sql = "SELECT *, substr(vc.waktu_tindakan, 1,10) as tanggal from (select *, count(tindakan_id) as jlh_tindakan,
	    						sum(jp) as total from visit_care group by paramedis, tindakan_id) vc
	    			left join master_tindakan_detail mt on mt.detail_id = vc.tindakan_id
	    			left join master_tindakan tt on tt.tindakan_id = mt.tindakan_id
	    			left join (select ri_id,cara_bayar from visit_ri) ri on ri.ri_id = vc.sub_visit
					left join petugas p on p.petugas_id = vc.paramedis
					left join master_dept md on md.dept_id = vc.dept_id
					where
					substr(vc.waktu_tindakan,1,10) >= '$mulai' and  substr(vc.waktu_tindakan,1,10) <= '$akhir'";
			if ($dept_id != '') {
				$sql .= " and vc.dept_id = '$dept_id'";
			}
			if ($cara_bayar != '') {
				if ($petugas_id != '') {
					$sql .= " and ri.cara_bayar LIKE '$cara_bayar' and vc.paramedis = '$petugas_id'";
				}else{
					$sql .= " and ri.cara_bayar LIKE '$cara_bayar'";
				}
			}else if ($petugas_id != '') {
				if ($cara_bayar != '') {
					$sql .= " and ri.cara_bayar LIKE '$cara_bayar' and vc.paramedis = '$petugas_id'";
				}else{
					$sql .= " and vc.paramedis = '$petugas_id'";
				}
			}

			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
	    }

	    public function get_dept_nama($dept)
		{
			$sql = "SELECT nama_dept from master_dept where dept_id = '$dept'";
			return $this->db->query($sql)->row_array();
		}
	    /*akhir jaspel*/

	    /*jaspel apotik*/
	    public function get_jasa_resep_apotek()
		{
			$sql = "SELECT *, md.nama_dept as dept_resep, sum(apd.management) as management, sum(apd.remunisasi) as remunisasi,
					sum(apd.apotek) as apotek, sum(apd.jasadokter) as jasadokter
					FROM apotek_penjualan ap left join apotek_penjualan_detail apd on apd.no_nota = ap.no_nota
					left join visit_resep vr on vr.resep_id = ap.resep_id
					left join visit v on v.visit_id = vr.visit_id
					left join pasien p on p.rm_id = v.rm_id
					left join petugas pe on pe.petugas_id = ap.dokter_id
					left join master_dept md on md.dept_id = substr(vr.sub_visit, 1,2)
					group by ap.no_nota";
			$result = $this->db->query($sql);
			if($result)
				return $result->result_array();
			else
				return false;
		}

		public function get_filter_jasa_resep_apotek($insert)
		{
			$start = $insert['start'];
			$end = $insert['end'];
			$cara_bayar =  $insert['cara_bayar'];
			$unit = $insert['unit'];
			$paramedis = $insert['paramedis'];
			$sql = "SELECT *, md.nama_dept as dept_resep, sum(apd.management) as management, sum(apd.remunisasi) as remunisasi,
					sum(apd.apotek) as apotek, sum(apd.jasadokter) as jasadokter
					FROM apotek_penjualan ap left join apotek_penjualan_detail apd on apd.no_nota = ap.no_nota
					left join visit_resep vr on vr.resep_id = ap.resep_id
					left join visit v on v.visit_id = vr.visit_id
					left join pasien p on p.rm_id = v.rm_id
					left join petugas pe on pe.petugas_id = ap.dokter_id
					left join master_dept md on md.dept_id = substr(vr.sub_visit, 1,2)
					where ";
			if($start == '' and $end == '' and $cara_bayar == '' and $unit == '' and $paramedis==''){

			}else if ($start != '') {
				if ($end == '') {
					$end = $insert['now'];
				}

				$sql .= "ap.waktu_penjualan > '$start' and ap.waktu_penjualan < '$end' group by ap.no_nota";
			}else if ($cara_bayar != '') {
				$sql .= "ap.cara_bayar LIKE '$cara_bayar' group by ap.no_nota";
			}else if ($unit != '') {
				$sql .= "substr(vr.sub_visit, 1,2) = '$unit' group by ap.no_nota";
			}else if ($paramedis != '') {
				$sql .= "ap.dokter_id = '$paramedis' group by ap.no_nota";
			}

			$result = $this->db->query($sql);
			if($result)
				return $result->result_array();
			else
				return false;
		}

	    /*akhir jaspel apotik*/

	    /*jasa kamar operasi*/
	    public function get_jasa_kamar_operasi($value='')
	    {
	    	$sql = "SELECT u.cara_bayar,md.nama_tindakan,mt.js,mt.jp,mt.bakhp,a.*,b.order_operasi_id, p.nama, pa.d_anestesi,pb.d_bedah, pan.d_anak
	    			from tagihan_operasi t left join tagihan u on u.no_invoice = t.no_invoice
	    			left join operasi_laporan a on a.operasi_id = u.sub_visit
	    			left join operasi_rencana b on a.rencana_id = b.rencana_id
	    			left join order_operasi c on b.order_operasi_id = c.order_operasi_id
	    			left join visit v on v.visit_id = c.visit_id
	    			left join pasien p on p.rm_id = v.rm_id
	    			left join master_tindakan_detail mt on mt.detail_id = t.tindakan_id
	    			left join master_tindakan md on md.tindakan_id = mt.tindakan_id
	    			left join (select petugas_id, nama_petugas as d_bedah from petugas) pb on pb.petugas_id = a.dokter_bedah
	    			left join (select petugas_id, nama_petugas as d_anestesi from petugas) pa on pa.petugas_id = a.dokter_anestesi
	    			left join (select petugas_id, nama_petugas as d_anak from petugas) pan on pan.petugas_id = a.dokter_anak";

	    	$result = $this->db->query($sql);
			if($result)
				return $result->result_array();
			else
				return false;
	    }

	    public function get_filter_kamar_operasi($insert)
	    {
	    	$dokter = $insert['dokter'];
	    	$start = $insert['start'];
	    	$end = $insert['end'];
	    	$cara_bayar = $insert['cara_bayar'];

	    	$sql = "SELECT u.cara_bayar,md.nama_tindakan,mt.js,mt.jp,mt.bakhp,a.*,b.order_operasi_id, p.nama, pa.d_anestesi,pb.d_bedah, pan.d_anak
	    			from tagihan_operasi t left join tagihan u on u.no_invoice = t.no_invoice
	    			left join operasi_laporan a on a.operasi_id = u.sub_visit
	    			left join operasi_rencana b on a.rencana_id = b.rencana_id
	    			left join order_operasi c on b.order_operasi_id = c.order_operasi_id
	    			left join visit v on v.visit_id = c.visit_id
	    			left join pasien p on p.rm_id = v.rm_id
	    			left join master_tindakan_detail mt on mt.detail_id = t.tindakan_id
	    			left join master_tindakan md on md.tindakan_id = mt.tindakan_id
	    			left join (select petugas_id, nama_petugas as d_bedah from petugas) pb on pb.petugas_id = a.dokter_bedah
	    			left join (select petugas_id, nama_petugas as d_anestesi from petugas) pa on pa.petugas_id = a.dokter_anestesi
	    			left join (select petugas_id, nama_petugas as d_anak from petugas) pan on pan.petugas_id = a.dokter_anak
	    			where substr(a.waktu_mulai,1,10) >= '$start' and substr(a.waktu_mulai, 1,2) <= '$end'";

	    	if ($cara_bayar != '') {
	    		$sql .= " and u.cara_bayar LIKE '$cara_bayar'";
	    	}
	    	if ($dokter != '') {
	    		$sql .= " and a.dokter_bedah LIKE '$dokter'";
	    	}

	    	$result = $this->db->query($sql);
			if($result)
				return $result->result_array();
			else
				return false;
	    }


	    /*jasa kamar operasi*/

			/*pendapatan*/
			public function get_all_pendapatan()
	    {
	      $sql = "SELECT * from keu_pendapatan ORDER BY tanggal, id DESC";
	      $res = $this->db->query($sql);
	      if ($res){
	        return $res->result_array();
	      }
	      else {
	        return false;
	      }
	    }

	    public function get_pendapatan_filter($date1, $date2)
	    {
	      $sql = "SELECT * FROM keu_pendapatan WHERE tanggal BETWEEN '$date1' AND '$date2' ORDER BY tanggal, id DESC";
	      $res = $this->db->query($sql);
	      if ($res) {
	        return $res->result_array();
	      }else {
	        return false;
	      }
	    }

	    public function search_pendapatan($katakunci)
	    {
	      $sql = "SELECT * FROM keu_pendapatan WHERE kategori_pendapatan LIKE '%$katakunci%' OR jenis_pendapatan LIKE '%$katakunci%' OR instansi_pembayar LIKE '%$katakunci%' ORDER BY tanggal, id DESC";
	      $res = $this->db->query($sql);
	      if ($res->num_rows() >0) {
	        return $res->result_array();
	      }else {
	        return false;
	      }
	    }

	    public function add_pendapatan($data)
	    {
	      $result =  $this->db->insert('keu_pendapatan', $data);
				if ($result) {
					return $this->db->insert_id();
				}else{
					return false;
				}
	    }

	    public function delete_pendapatan($id)
	    {
	      $this->db->where('id', $id);
	      $result =  $this->db->delete('keu_pendapatan');
				if ($result) {
					return true;
				}else{
					return false;
				}
	    }
			/*end of pendapatan*/

		/*rekap pendapatan*/
			//rekap pendapatan pelayanan-non pelayanan
			public function get_jenis_pendapatan()
			{
				$sql = "SELECT DISTINCT(jenis_pendapatan) AS jenis_pendapatan from keu_pendapatan ORDER BY kategori_pendapatan DESC, jenis_pendapatan ASC";
	      $res = $this->db->query($sql);
	      if ($res){
	        return $res->result_array();
	      }
	      else {
	        return false;
	      }
			}

			public function get_pendapatan_by_jenis_date($jenis, $date1, $date2)
			{
				$a = 0;
				$secs = strtotime($date2) - strtotime($date1);
				$days = $secs/86400;

				for ($i=$days; $i >= 0; $i--) {
					$date =  date('Y-m-d',strtotime($date2."-".$i." day"));

					$sql = "SELECT SUM(IFNULL(jumlah, 0)) AS dapat FROM keu_pendapatan WHERE jenis_pendapatan='$jenis' AND tanggal='$date'";
					$r = $this->db->query($sql);
	    		$result[$a] = $r->row_array();
					$a++;
				}
				return $result;
			}

			//pendapatan per jenis penerimaan
			public function get_kategori_tindakan()
			{
				$sql = "SELECT keterangan AS jenis_pendapatan FROM master_tindakan_kategori WHERE keterangan NOT IN('AMBULANCE', 'INSTALASI UTD')";
	      $res = $this->db->query($sql);
	      if ($res){
	        return $res->result_array();
	      }
	      else {
	        return false;
	      }
			}

			public function get_rekap_administrasi($date1, $date2)
			{
				$a = 0;
				$secs = strtotime($date2) - strtotime($date1);
				$days = $secs/86400;

				for ($i=$days; $i >= 0; $i--) {
					$date =  date('Y-m-d',strtotime($date2."-".$i." day"));

					$sql = "SELECT SUM(IFNULL(tarif, 0)) AS dapat
									FROM tagihan_admisi ta
									LEFT JOIN master_tindakan_detail mtd ON ta.tindakan_id=mtd.detail_id
									LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
									LEFT JOIN master_tindakan_kategori mtk ON mt.kat_id=mtk.kat_id
									LEFT JOIN tagihan t ON ta.no_invoice = t.no_invoice
									WHERE is_bayar=1 AND ta.waktu LIKE '$date%' AND mtk.keterangan LIKE 'Administrasi'";

					$r = $this->db->query($sql);
					$result[$a] = $r->row_array();
					$a++;
				}
				return $result;
			}

			public function get_rekap_per_jenis($jenis, $date1, $date2)
			{
				$a = 0;
				$secs = strtotime($date2) - strtotime($date1);
				$days = $secs/86400;

				for ($i=$days; $i >= 0; $i--) {
					$date =  date('Y-m-d',strtotime($date2."-".$i." day"));

					$sql = "SELECT SUM(IFNULL(jumlah, 0)) AS dapat
									FROM tagihan_perawatan tp
									LEFT JOIN master_tindakan_detail mtd ON tp.tindakan_id=mtd.detail_id
									LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
									LEFT JOIN master_tindakan_kategori mtk ON mt.kat_id=mtk.kat_id
									LEFT JOIN tagihan t ON tp.no_invoice = t.no_invoice
									WHERE is_bayar=1 AND tp.waktu LIKE '$date%' AND mtk.keterangan LIKE '$jenis%'";

					$r = $this->db->query($sql);
					$result[$a] = $r->row_array();
					$a++;
				}
				return $result;
			}

			//pendapatan per unit
			public function unit_has_pendapatan()
			{
				$sql = "SELECT * FROM master_dept WHERE jenis IN('POLIKLINIK', 'RAWAT INAP', 'PENUNJANG', 'PENDUKUNG') AND nama_dept NOT IN('UNIT TRANSFUSI DARAH') ORDER BY JENIS DESC, nama_dept ASC";
				$res = $this->db->query($sql);
	      if ($res){
	        return $res->result_array();
	      }
	      else {
	        return false;
	      }
			}

			public function unit_has_pendapatan2()
			{
				//tanpa instalasi gizi
				$sql = "SELECT * FROM master_dept WHERE jenis IN('POLIKLINIK', 'RAWAT INAP', 'PENUNJANG', 'PENDUKUNG') AND nama_dept NOT IN('UNIT TRANSFUSI DARAH', 'INSTALASI GIZI') ORDER BY JENIS DESC, nama_dept ASC";
				$res = $this->db->query($sql);
	      if ($res){
	        return $res->result_array();
	      }
	      else {
	        return false;
	      }
			}

			public function cek_unit($unit)
			{
				$sql = "SELECT * FROM master_dept WHERE dept_id=$unit";
				$res = $this->db->query($sql);
	      if ($res){
	        return $res->row_array();
	      }
	      else {
	        return false;
	      }
			}

			public function get_rekap_poli($unit, $date1, $date2)
			//pendapatan dari poliklinik
			{
				$date1 =  date('Y-m-d',strtotime($date1));
				$date2 =  date('Y-m-d',strtotime($date2));

				$sql = "SELECT nama_tindakan, kelas, COUNT(tp.tindakan_id) AS jumlah_tindakan,
									SUM(tp.js-tp.js_bpjs) AS total_js, SUM(tp.jp-tp.jp_bpjs) AS total_jp, SUM(tp.bakhp-tp.bakhp_bpjs) AS total_bakhp,
									SUM(tp.js-tp.js_bpjs+tp.jp-tp.jp_bpjs+tp.bakhp-tp.bakhp_bpjs) AS total_tarif,
									SUM(IFNULL(on_faktur, 0)) AS total_onfaktur,
									SUM(tp.js-tp.js_bpjs+tp.jp-tp.jp_bpjs+tp.bakhp-tp.bakhp_bpjs+on_faktur) AS total_pendapatan
								FROM tagihan_perawatan tp
								LEFT JOIN master_tindakan_detail mtd ON tp.tindakan_id=mtd.detail_id
								LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
								LEFT JOIN tagihan t ON tp.no_invoice=t.no_invoice
								WHERE is_bayar=1 AND dept_id=$unit AND (tp.waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
								GROUP BY tp.tindakan_id";

				$r = $this->db->query($sql);
				$result = $r->result_array();

				return $result;
			}

			public function get_rekap_ri($unit, $date1, $date2)
			//pendapatan dari rawat inap
			{
				$date1 =  date('Y-m-d',strtotime($date1));
				$date2 =  date('Y-m-d',strtotime($date2));

				$sql = "SELECT nama_tindakan, kelas, COUNT(tp.tindakan_id) AS jumlah_tindakan,
									SUM(tp.js-tp.js_bpjs+tp.jp-tp.jp_bpjs+tp.bakhp-tp.bakhp_bpjs) AS total_tarif,
									SUM(IFNULL(on_faktur, 0)) AS total_onfaktur,
									SUM(tp.js-tp.js_bpjs+tp.jp-tp.jp_bpjs+tp.bakhp-tp.bakhp_bpjs+on_faktur) AS total_pendapatan
								FROM tagihan_perawatan tp
								LEFT JOIN master_tindakan_detail mtd ON tp.tindakan_id=mtd.detail_id
								LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
								LEFT JOIN tagihan t ON tp.no_invoice=t.no_invoice
								WHERE is_bayar=1 AND dept_id=$unit AND (tp.waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
								GROUP BY tp.tindakan_id

								UNION

								SELECT nama_kamar AS nama_tindakan, kelas_kamar AS kelas, SUM(DATEDIFF(tgl_keluar, tgl_masuk)) AS jumlah_tindakan, CASE WHEN tarif_lain > 0 THEN tarif_lain ELSE SUM(DATEDIFF(tgl_keluar, tgl_masuk)) * tarif END AS total_tarif, SUM(on_faktur) AS total_onfaktur, (CASE WHEN tarif_lain > 0 THEN tarif_lain ELSE SUM(DATEDIFF(tgl_keluar, tgl_masuk)) * tarif END + SUM(on_faktur)) AS total_pendapatan
								FROM tagihan_kamar tk LEFT JOIN master_kamar mk ON tk.kamar_id=mk.kamar_id
								LEFT JOIN master_bed mb ON tk.bed_id=mb.bed_id
								LEFT JOIN tagihan t ON tk.no_invoice=t.no_invoice
								WHERE is_bayar=1 AND dept_id=$unit AND (tgl_keluar BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
								GROUP BY tk.kamar_id";

				$r = $this->db->query($sql);
				$result = $r->result_array();

				return $result;
			}

			public function get_rekap_jenazah($date1, $date2)
			{
				$date1 =  date('Y-m-d',strtotime($date1));
				$date2 =  date('Y-m-d',strtotime($date2));

				$sql = "SELECT nama_tindakan, jp.kelas_pelayanan AS kelas, COUNT(jpd.detail_tindakan_id) AS jumlah_tindakan, SUM(jpd.bakhp) AS total_bakhp, SUM(jpd.js) AS total_js, SUM(jpd.jp) AS total_jp, SUM(jpd.bakhp+jpd.js+jpd.jp) As total_tarif, SUM(jpd.on_faktur) AS total_onfaktur, SUM(jpd.total) AS total_pendapatan
								FROM jenazah_perawatan_detail jpd LEFT JOIN master_tindakan_detail mtd ON jpd.detail_tindakan_id=mtd.detail_id
								LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
								LEFT JOIN jenazah_perawatan jp ON jpd.perawatan_id=jp.id
								WHERE is_bayar=1 AND (jpd.waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
								GROUP BY jpd.detail_tindakan_id";

				$r = $this->db->query($sql);
				$result = $r->result_array();

				return $result;
			}

			public function get_rekap_mediko($date1, $date2)
			{
				$date1 =  date('Y-m-d',strtotime($date1));
				$date2 =  date('Y-m-d',strtotime($date2));

				$sql = "SELECT nama_tindakan, mp.kelas_pelayanan AS kelas, COUNT(mpd.detail_tindakan_id) AS jumlah_tindakan, SUM(mpd.bakhp) AS total_bakhp, SUM(mpd.js) AS total_js, SUM(mpd.jp) AS total_jp, SUM(mpd.bakhp+mpd.js+mpd.jp) AS total_tarif, SUM(mpd.on_faktur) AS total_onfaktur, SUM(mpd.total) AS total_pendapatan
								FROM mediko_perawatan_detail mpd LEFT JOIN master_tindakan_detail mtd ON mpd.detail_tindakan_id=mtd.detail_id
								LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
								LEFT JOIN mediko_perawatan mp ON mpd.perawatan_id=mp.id
								WHERE is_bayar=1 AND (mpd.waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
								GROUP BY mpd.detail_tindakan_id";

				$r = $this->db->query($sql);
				$result = $r->result_array();

				return $result;
			}

			public function get_rekap_makan($date1, $date2)
			{
				$date1 =  date('Y-m-d',strtotime($date1));
				$date2 =  date('Y-m-d',strtotime($date2));

				$sql = "SELECT nama_paket, gpm.kelas AS kelas, COUNT(paket_id) AS jumlah_tindakan, SUM(ta.jumlah) AS total_tarif, SUM(ta.on_faktur) AS total_onfaktur, SUM(ta.jumlah+ta.on_faktur) AS total_pendapatan
								FROM tagihan_akomodasi ta LEFT JOIN gizi_paket_makan gpm ON ta.paket_id=gpm.id
								LEFT JOIN tagihan t ON ta.no_invoice=t.no_invoice
								LEFT JOIN visit_permintaan_makan vpm ON ta.makan_id=vpm.makan_id
								WHERE is_bayar=1 AND (waktu_permintaan BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
								GROUP BY paket_id";

				$r = $this->db->query($sql);
				$result = $r->result_array();

				return $result;
			}

			public function get_rekap_operasi($date1, $date2)
			{
				//tagihan kamar operasi
				$date1 = date('Y-m-d', strtotime($date1));
				$date2 = date('Y-m-d', strtotime($date2));
				$sql = "SELECT nama_tindakan, mtd.kelas AS kelas, COUNT(top.tindakan_id) AS jumlah_tindakan,
									SUM(top.js-top.js_bpjs) AS total_js, SUM(top.jp-top.jp_bpjs) AS total_jp, SUM(top.bakhp-top.bakhp_bpjs) AS total_bpjs,
									SUM(top.js-top.js_bpjs+top.jp-top.jp_bpjs+top.bakhp-top.bakhp_bpjs) AS total_tarif,
									SUM(on_faktur) AS total_onfaktur,
									SUM(top.js-top.js_bpjs+top.jp-top.jp_bpjs+top.bakhp-top.bakhp_bpjs+on_faktur) AS total_pendapatan
								FROM tagihan_operasi top LEFT JOIN master_tindakan_detail mtd ON top.tindakan_id=mtd.detail_id
								LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
								LEFT JOIN tagihan t ON top.no_invoice=t.no_invoice
								WHERE is_bayar=1 AND (waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
								GROUP BY top.tindakan_id";

				$r = $this->db->query($sql);
				$result = $r->result_array();
				return $result;
			}

			public function get_rekap_penunjang($unit, $date1, $date2)
			{
				//tagihan penunjang
				$date1 = date('Y-m-d', strtotime($date1));
				$date2 = date('Y-m-d', strtotime($date2));
				$sql = "SELECT nama_tindakan, jenis_tarif AS kelas, COUNT(tp.tindakan_penunjang_id) AS jumlah_tindakan,
									SUM(tp.js-tp.js_bpjs) AS total_js, SUM(tp.jp-tp.jp_bpjs) AS total_jp, SUM(tp.bakhp-tp.bakhp_bpjs) AS total_bakhp,
									SUM(tp.js - tp.js_bpjs + tp.jp - tp.jp_bpjs + tp.bakhp - tp.bakhp_bpjs) AS total_tarif,
									SUM(on_faktur) AS total_onfaktur,
									SUM(tp.js - tp.js_bpjs + tp.jp - tp.jp_bpjs + tp.bakhp - tp.bakhp_bpjs + on_faktur) AS total_pendapatan
								FROM tagihan_penunjang tp LEFT JOIN master_tindakan_penunjang mtp ON tp.tindakan_penunjang_id=mtp.tindakan_penunjang_id
								LEFT JOIN tagihan t ON tp.no_invoice=t.no_invoice
								WHERE is_bayar=1 AND tp.dept_id=$unit AND (waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
								GROUP BY tp.tindakan_penunjang_id";

				$r = $this->db->query($sql);
				$result = $r->result_array();
				return $result;
			}

			/*end of rekap pendapatan*/
	}
?>
