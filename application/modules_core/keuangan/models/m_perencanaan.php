<?php
  class M_perencanaan extends CI_Model
  {
    function __construct(){}

    public function get_dept_id($nama)
    {
      $sql = "SELECT dept_id from master_dept WHERE nama_dept LIKE '$nama'";
      $res = $this->db->query($sql);
      if ($res){
        return $res->row_array();
      }
      else {
        return false;
      }
    }

    public function get_all_riwayat()
    {
      $sql = "SELECT * FROM keu_perencanaan_rba";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_riwayat_total()
    {
      $sql = "SELECT COUNT(id) AS total FROM keu_perencanaan_rba";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

    public function get_riwayat_by_id($id)
    {
      $sql = "SELECT * FROM keu_perencanaan_rba WHERE id=$id";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

    public function get_detail_riwayat($id)
    {
      $sql = "SELECT * FROM keu_perencanaan_rba_detail WHERE rba_id = $id";
      $res = $this->db->query($sql);
      if ($res) {
       return $res->result_array();
      }else {
       return false;
      }
    }

    public function add_rencana($data)
    {
      $result =  $this->db->insert('keu_perencanaan_rba', $data);
			if ($result) {
				return $this->db->insert_id();
			}else{
				return false;
			}
    }

    public function add_rencana_detail($data)
    {
      $result =  $this->db->insert('keu_perencanaan_rba_detail', $data);
      if ($result) {
        return $this->db->insert_id();
      }else{
        return false;
      }
    }

    public function get_all_perencanaan_gudang()
    {
      $sql ="SELECT a.*, p.nama_petugas 
            from (SELECT *, 'barang' as jenis from barang_rencana_pengadaan 
              union select *,'obat' as jenis from obat_rencana_pengadaan) a 
            left join petugas p on p.petugas_id = a.petugas_input 
            where a.status like 'tunggu' order by a.tanggal desc";
      $res = $this->db->query($sql);
      if ($res) {
       return $res->result_array();
      }else {
       return false;
      }
    }

    public function get_perencanaan_gudang_by_id($jenis, $id)
    {
      switch ($jenis) {
        case 'obat':
          $sql = "SELECT a.*, p.nama_petugas 
                  from (select *,'obat' as jenis from obat_rencana_pengadaan where obat_rencana_id='$id') a 
                  left join petugas p on p.petugas_id = a.petugas_input 
                  where a.status like 'tunggu' order by a.tanggal desc";
          break;
        case 'barang':
          $sql = "SELECT a.*, p.nama_petugas 
                  from (SELECT *, 'barang' as jenis from barang_rencana_pengadaan where barang_rencana_id='$id') a 
                  left join petugas p on p.petugas_id = a.petugas_input 
                  where a.status like 'tunggu' order by a.tanggal desc";
          break;
      }

      if ($sql !='') {
        $res = $this->db->query($sql);
        if ($res) {
         return $res->row_array();
        }else {
         return false;
        }
      }
    }

    public function get_detail_pengadaan($jenis, $id)
    {
      switch ($jenis) {
        case 'obat':
          $sql = "SELECT od.*,od.hps as harga,o.nama,os.satuan,mp.nama_penyedia
                  FROM obat_rencana_pengadaan_detail od  
                  left join obat o on o.obat_id = od.obat_id left join obat_satuan os on os.satuan_id = o.satuan_id
                  left join master_penyedia mp on mp.penyedia_id  =  od.penyedia_id
                  where od.obat_rencana_id = '$id'";
          break;
        case 'barang':
          $sql = "SELECT a.*,b.nama,c.nama_penyedia,d.satuan from barang_rencana_pengadaan_detail a
                  left join barang b on b.barang_id = a.barang_id
                  left join barang_penyedia c on c.penyedia_id = a.penyedia_id
                  left join obat_satuan d on d.satuan_id = b.satuan_id
                  where a.barang_rencana_id = '$id'";
          break;
      }

      if ($sql !='') {
        $res = $this->db->query($sql);
        if ($res) {
         return $res->result_array();
        }else {
         return false;
        }
      }
    }

  }
?>
