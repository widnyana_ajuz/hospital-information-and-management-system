<?php
  Class M_realisasi extends CI_Model
  {
    function __construct(){}

    public function get_dept_id($nama)
    {
      $sql = "SELECT dept_id from master_dept WHERE nama_dept LIKE '$nama'";
      $res = $this->db->query($sql);
      if ($res){
        return $res->row_array();
      }
      else {
        return false;
      }
    }

    public function get_all_riwayat()
    {
      $sql = "SELECT * FROM keu_perencanaan_rba";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_riwayat_by_id($id)
    {
      $sql = "SELECT * FROM keu_perencanaan_rba WHERE id=$id";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

     public function get_detail_riwayat($id)
    {
      $sql = "SELECT * FROM keu_perencanaan_rba_detail WHERE rba_id = $id";
      $res = $this->db->query($sql);
      if ($res) {
       return $res->result_array();
      }else {
       return false;
      }
    }

    public function get_buku_kas()
    {
      $sql = "SELECT * FROM keu_aktivitas_kas";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_kas_date_filter($date1, $date2)
    {
      $sql = "SELECT * FROM keu_aktivitas_kas WHERE tanggal BETWEEN '$date1' AND '$date2'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_last_saldo()
    {
      $sql = "SELECT saldo FROM keu_aktivitas_kas ORDER BY id DESC";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

    public function search_realisasi($katakunci)
    {
      $sql = "SELECT * FROM keu_aktivitas_kas WHERE kode_perkiraan LIKE '%$katakunci%' OR uraian LIKE '%$katakunci%' OR no_faktur LIKE '%$katakunci%' OR atas_nama LIKE '%$katakunci%' OR kategori LIKE '%$katakunci%' OR jenis_aktivitas LIKE '%$katakunci%'";
      $res = $this->db->query($sql);
      if ($res->num_rows() >0) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function add_aktivitas($data)
    {
      $result =  $this->db->insert('keu_aktivitas_kas', $data);
			if ($result) {
				return $this->db->insert_id();
			}else{
				return false;
			}
    }

    public function get_riwayatpermintaan($dept_id)
    {
      $sql = "SELECT a.*, p.nama_petugas, b.nama_dept from barang_permintaan a left join master_dept b
          on b.dept_id = a.dept_id left join petugas p on p.petugas_id = a.petugas_respond 
          where a.is_responded = '1' and a.dept_id = '$dept_id'";
      $res = $this->db->query($sql);
      if ($res->num_rows() > 0) {
        return $res->result_array();
      }else{
        return false;
      }
    }

    public function get_riwayatpermintaan_by_id($id)
    {
      $sql = "SELECT a.*, p.nama_petugas, b.nama_dept from barang_permintaan a left join master_dept b
          on b.dept_id = a.dept_id left join petugas p on p.petugas_id = a.petugas_respond 
          where a.is_responded = '1' AND barang_permintaan_id='$id'";
      $res = $this->db->query($sql);
      if ($res->num_rows() > 0) {
        return $res->row_array();
      }else{
        return false;
      }
    }

    public function get_detailpersetujuan($id)
		{
			$sql = "SELECT * from barang_permintaan_detail a 
					left join (select * from barang_stok order by barang_stok_id desc) b 
						on b.barang_stok_id = a.barang_stok_id
					left join barang c on c.barang_id = a.barang_id 
					left join obat_satuan bs on bs.satuan_id = c.satuan_id
					left join barang_merk bm on bm.merk_id = c.merk
					where a.barang_permintaan_id = '$id'";
			$res = $this->db->query($sql);
			if ($res->num_rows() > 0) {
				return $res->result_array();
			}else{
				return false;
			}
		}

	#rekap pendapatan
    //rekap pendapatan pelayanan-non pelayanan
    public function get_jenis_pendapatan()
    {
      $sql = "SELECT DISTINCT(jenis_pendapatan) AS jenis_pendapatan from keu_pendapatan ORDER BY kategori_pendapatan DESC, jenis_pendapatan ASC";
      $res = $this->db->query($sql);
      if ($res){
        return $res->result_array();
      }
      else {
        return false;
      }
    }

    public function get_pendapatan_by_jenis_date($jenis, $date1, $date2)
    {
      $a = 0;
      $secs = strtotime($date2) - strtotime($date1);
      $days = $secs/86400;

      for ($i=$days; $i >= 0; $i--) {
        $date =  date('Y-m-d',strtotime($date2."-".$i." day"));

        $sql = "SELECT SUM(IFNULL(jumlah, 0)) AS dapat FROM keu_pendapatan WHERE jenis_pendapatan='$jenis' AND tanggal='$date'";
        $r = $this->db->query($sql);
        $result[$a] = $r->row_array();
        $a++;
      }
      return $result;
    }

    //rekap pendapatan per jenis penerimaan
    public function get_kategori_tindakan()
    {
      $sql = "SELECT keterangan AS jenis_pendapatan FROM master_tindakan_kategori WHERE keterangan NOT IN('AMBULANCE', 'INSTALASI UTD')";
      $res = $this->db->query($sql);
      if ($res){
        return $res->result_array();
      }
      else {
        return false;
      }
    }

    public function get_rekap_per_jenis($jenis, $date1, $date2)
    {
      $a = 0;
      $secs = strtotime($date2) - strtotime($date1);
      $days = $secs/86400;

      for ($i=$days; $i >= 0; $i--) {
        $date =  date('Y-m-d',strtotime($date2."-".$i." day"));

        $sql = "SELECT SUM(IFNULL(jumlah, 0)) AS dapat
                FROM tagihan_perawatan tp
                LEFT JOIN master_tindakan_detail mtd ON tp.tindakan_id=mtd.detail_id
                LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
                LEFT JOIN master_tindakan_kategori mtk ON mt.kat_id=mtk.kat_id
                LEFT JOIN tagihan t ON tp.no_invoice = t.no_invoice
                WHERE is_bayar=1 AND tp.waktu LIKE '$date%' AND mtk.keterangan LIKE '$jenis%'";

        $r = $this->db->query($sql);
        $result[$a] = $r->row_array();
        $a++;
      }
      return $result;
    }

    //rekap pendapatan per unit
    public function unit_has_pendapatan()
    {
      $sql = "SELECT * FROM master_dept WHERE jenis IN('POLIKLINIK', 'RAWAT INAP', 'PENUNJANG', 'PENDUKUNG') AND nama_dept NOT IN('UNIT TRANSFUSI DARAH') ORDER BY JENIS DESC, nama_dept ASC";
      $res = $this->db->query($sql);
      if ($res){
        return $res->result_array();
      }
      else {
        return false;
      }
    }

    public function cek_unit($unit)
    {
      $sql = "SELECT * FROM master_dept WHERE dept_id=$unit";
      $res = $this->db->query($sql);
      if ($res){
        return $res->row_array();
      }
      else {
        return false;
      }
    }

    public function get_rekap_poli($unit, $date1, $date2)
    //pendapatan dari poliklinik
    {
      $date1 =  date('Y-m-d',strtotime($date1));
      $date2 =  date('Y-m-d',strtotime($date2));

      $sql = "SELECT nama_tindakan, kelas, COUNT(tp.tindakan_id) AS jumlah_tindakan,
                SUM(tp.js-tp.js_bpjs) AS total_js, SUM(tp.jp-tp.jp_bpjs) AS total_jp, SUM(tp.bakhp-tp.bakhp_bpjs) AS total_bakhp,
                SUM(tp.js-tp.js_bpjs+tp.jp-tp.jp_bpjs+tp.bakhp-tp.bakhp_bpjs) AS total_tarif,
                SUM(IFNULL(on_faktur, 0)) AS total_onfaktur,
                SUM(tp.js-tp.js_bpjs+tp.jp-tp.jp_bpjs+tp.bakhp-tp.bakhp_bpjs+on_faktur) AS total_pendapatan
              FROM tagihan_perawatan tp
              LEFT JOIN master_tindakan_detail mtd ON tp.tindakan_id=mtd.detail_id
              LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
              LEFT JOIN tagihan t ON tp.no_invoice=t.no_invoice
              WHERE is_bayar=1 AND dept_id=$unit AND (tp.waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
              GROUP BY tp.tindakan_id";

      $r = $this->db->query($sql);
      $result = $r->result_array();

      return $result;
    }

    public function get_rekap_ri($unit, $date1, $date2)
    //pendapatan dari rawat inap
    {
      $date1 =  date('Y-m-d',strtotime($date1));
      $date2 =  date('Y-m-d',strtotime($date2));

      $sql = "SELECT nama_tindakan, kelas, COUNT(tp.tindakan_id) AS jumlah_tindakan,
                SUM(tp.js-tp.js_bpjs+tp.jp-tp.jp_bpjs+tp.bakhp-tp.bakhp_bpjs) AS total_tarif,
                SUM(IFNULL(on_faktur, 0)) AS total_onfaktur,
                SUM(tp.js-tp.js_bpjs+tp.jp-tp.jp_bpjs+tp.bakhp-tp.bakhp_bpjs+on_faktur) AS total_pendapatan
              FROM tagihan_perawatan tp
              LEFT JOIN master_tindakan_detail mtd ON tp.tindakan_id=mtd.detail_id
              LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
              LEFT JOIN tagihan t ON tp.no_invoice=t.no_invoice
              WHERE is_bayar=1 AND dept_id=$unit AND (tp.waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
              GROUP BY tp.tindakan_id

              UNION

              SELECT nama_kamar AS nama_tindakan, kelas_kamar AS kelas, SUM(DATEDIFF(tgl_keluar, tgl_masuk)) AS jumlah_tindakan, CASE WHEN tarif_lain > 0 THEN tarif_lain ELSE SUM(DATEDIFF(tgl_keluar, tgl_masuk)) * tarif END AS total_tarif, SUM(on_faktur) AS total_onfaktur, (CASE WHEN tarif_lain > 0 THEN tarif_lain ELSE SUM(DATEDIFF(tgl_keluar, tgl_masuk)) * tarif END + SUM(on_faktur)) AS total_pendapatan
              FROM tagihan_kamar tk LEFT JOIN master_kamar mk ON tk.kamar_id=mk.kamar_id
              LEFT JOIN master_bed mb ON tk.bed_id=mb.bed_id
              LEFT JOIN tagihan t ON tk.no_invoice=t.no_invoice
              WHERE is_bayar=1 AND dept_id=$unit AND (tgl_keluar BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
              GROUP BY tk.kamar_id";


      $r = $this->db->query($sql);
      $result = $r->result_array();

      return $result;
    }

    public function get_rekap_jenazah($date1, $date2)
    {
      $date1 =  date('Y-m-d',strtotime($date1));
      $date2 =  date('Y-m-d',strtotime($date2));

      $sql = "SELECT nama_tindakan, jp.kelas_pelayanan AS kelas, COUNT(jpd.detail_tindakan_id) AS jumlah_tindakan, SUM(jpd.bakhp+jpd.js+jpd.jp) As total_tarif, SUM(jpd.on_faktur) AS total_onfaktur, SUM(jpd.total) AS total_pendapatan
              FROM jenazah_perawatan_detail jpd LEFT JOIN master_tindakan_detail mtd ON jpd.detail_tindakan_id=mtd.detail_id
              LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
              LEFT JOIN jenazah_perawatan jp ON jpd.perawatan_id=jp.id
              WHERE is_bayar=1 AND (jpd.waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
              GROUP BY jpd.detail_tindakan_id";

      $r = $this->db->query($sql);
      $result = $r->result_array();

      return $result;
    }

    public function get_rekap_mediko($date1, $date2)
    {
      $date1 =  date('Y-m-d',strtotime($date1));
      $date2 =  date('Y-m-d',strtotime($date2));

      $sql = "SELECT nama_tindakan, mp.kelas_pelayanan AS kelas, COUNT(mpd.detail_tindakan_id) AS jumlah_tindakan, SUM(mpd.bakhp+mpd.js+mpd.jp) AS total_tarif, SUM(mpd.on_faktur) AS total_onfaktur, SUM(mpd.total) AS total_pendapatan
              FROM mediko_perawatan_detail mpd LEFT JOIN master_tindakan_detail mtd ON mpd.detail_tindakan_id=mtd.detail_id
              LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
              LEFT JOIN mediko_perawatan mp ON mpd.perawatan_id=mp.id
              WHERE is_bayar=1 AND (mpd.waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
              GROUP BY mpd.detail_tindakan_id";

      $r = $this->db->query($sql);
      $result = $r->result_array();

      return $result;
    }

    public function get_rekap_makan($date1, $date2)
    {
      $date1 =  date('Y-m-d',strtotime($date1));
      $date2 =  date('Y-m-d',strtotime($date2));

      $sql = "SELECT nama_paket, gpm.kelas AS kelas, COUNT(paket_id) AS jumlah_tindakan, SUM(ta.jumlah) AS total_tarif, SUM(ta.on_faktur) AS total_onfaktur, SUM(ta.jumlah+ta.on_faktur) AS total_pendapatan
              FROM tagihan_akomodasi ta LEFT JOIN gizi_paket_makan gpm ON ta.paket_id=gpm.id
              LEFT JOIN tagihan t ON ta.no_invoice=t.no_invoice
              LEFT JOIN visit_permintaan_makan vpm ON ta.makan_id=vpm.makan_id
              WHERE is_bayar=1 AND (waktu_permintaan BETWEEN '$date1 00:00:00' ANd '$date2 23:59:59')
              GROUP BY paket_id";

      $r = $this->db->query($sql);
      $result = $r->result_array();

      return $result;
    }

    public function get_rekap_operasi($date1, $date2)
    {
      //tagihan kamar operasi
      $date1 = date('Y-m-d', strtotime($date1));
      $date2 = date('Y-m-d', strtotime($date2));
      $sql = "SELECT nama_tindakan, mtd.kelas AS kelas, COUNT(top.tindakan_id) AS jumlah_tindakan,
                SUM(top.js-top.js_bpjs) AS total_js, SUM(top.jp-top.jp_bpjs) AS total_jp, SUM(top.bakhp-top.bakhp_bpjs) AS total_bpjs,
                SUM(top.js-top.js_bpjs+top.jp-top.jp_bpjs+top.bakhp-top.bakhp_bpjs) AS total_tarif,
                SUM(on_faktur) AS total_onfaktur,
                SUM(top.js-top.js_bpjs+top.jp-top.jp_bpjs+top.bakhp-top.bakhp_bpjs+on_faktur) AS total_pendapatan
              FROM tagihan_operasi top LEFT JOIN master_tindakan_detail mtd ON top.tindakan_id=mtd.detail_id
              LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
              LEFT JOIN tagihan t ON top.no_invoice=t.no_invoice
              WHERE is_bayar=1 AND (waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
              GROUP BY top.tindakan_id";

      $r = $this->db->query($sql);
      $result = $r->result_array();
      return $result;
    }

    public function get_rekap_penunjang($unit, $date1, $date2)
    {
      //tagihan penunjang
      $date1 = date('Y-m-d', strtotime($date1));
      $date2 = date('Y-m-d', strtotime($date2));
      $sql = "SELECT nama_tindakan, jenis_tarif AS kelas, COUNT(tp.tindakan_penunjang_id) AS jumlah_tindakan,
                SUM(tp.js-tp.js_bpjs) AS total_js, SUM(tp.jp-tp.jp_bpjs) AS total_jp, SUM(tp.bakhp-tp.bakhp_bpjs) AS total_bakhp,
                SUM(tp.js - tp.js_bpjs + tp.jp - tp.jp_bpjs + tp.bakhp - tp.bakhp_bpjs) AS total_tarif,
                SUM(on_faktur) AS total_onfaktur,
                SUM(tp.js - tp.js_bpjs + tp.jp - tp.jp_bpjs + tp.bakhp - tp.bakhp_bpjs + on_faktur) AS total_pendapatan
              FROM tagihan_penunjang tp LEFT JOIN master_tindakan_penunjang mtp ON tp.tindakan_penunjang_id=mtp.tindakan_penunjang_id
              LEFT JOIN tagihan t ON tp.no_invoice=t.no_invoice
              WHERE is_bayar=1 AND tp.dept_id=$unit AND (waktu BETWEEN '$date1 00:00:00' AND '$date2 23:59:59')
              GROUP BY tp.tindakan_penunjang_id";

      $r = $this->db->query($sql);
      $result = $r->result_array();
      return $result;
    }

#end of rekap pendapatan


  }
?>
