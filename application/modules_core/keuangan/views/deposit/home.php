
<div class="title">
	DEPOSIT RAWAT INAP
</div>
<div class="bar">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>keuangan/homedeposit">Deposit</a>
	</li>
</div>

<div class="backregis">
	<div id="my-tab-content" class="tab-content">
		<form method="POST" id="search_submit">
	       	<div class="search">
				<label class="control-label col-md-3" style="margin-top:5px;">
					<i class="fa fa-search">&nbsp;&nbsp;</i>Nama Pasien / Rekam Medis <span class="required" style="color : red">* </span>
				</label>
				<div class="col-md-4">		
					<input type="text" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
		        </div>
		        <button type="submit" class="btn btn-info">Cari</button>
			</div>	
		</form>
		<br>
		<hr class="garis"><br>

		<div id="">
	    	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
				<table class="table table-striped table-bordered table-hover tableDTUtama" id="">
					<thead>
						<tr class="info">
							<th width="20">No.</th>
							<th>Unit</th>
							<th>#Rekam Medis</th>
							<th>Nomor Visit</th>
							<th>Nama Pasien</th>
							<th>Alamat</th>
							<th>Tanggal Masuk</th>
							<th>Diagnosis</th>
							<th>Total Deposit</th>
							<th width="80">Action</th>
						</tr>
					</thead>
					<tbody id="tbody_resep">
						<tr>
							<td>1</td>
							<td>Bersalin</td>
							<td>123</td>
							<td>321</td>
							<td>Joe Boy</td>
							<td>Rumahnya </td>
							<td style="text-align:center">12 Mei 2012</td>
							<td>SDA - qweqweqwe</td>
							<td style="text-align:right">2000</td>
							<td style="text-align:center">
								<a href="<?php echo base_url();?>keuangan/listdeposit">
									<i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Deposit"></i>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>