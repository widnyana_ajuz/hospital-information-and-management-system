<?php
$sekarang = str_replace('-', "_", date('d-m-Y'));
$title = 'Aktivitas_Kas_'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Aktivitas Kas</title>
  <style>
     .grup-pertanyaan {
     text-align: center;
     border: solid 1px #000;
    }
     table td {
       border-bottom: solid 0.5px #000;
       font-size: 12pt;
       vertical-align:middle;
       line-height:40px;
       width: 300px;
     }
     .keterangan_pertanyaan {
       font-size: 8pt;
     }
     table .nama_matkul{
       text-transform:capitalize;
     }
     table {
       width: 100%;
     }
     table .header {
       background-color: yellow;
       border-top: solid 0.5px #000;
       border-left: solid 0.5px #000;
       border-right: solid 0.5px #000;
       border-bottom: solid 0.2px #000;
       font-weight: bold;
       vertical-align:middle;
       line-height:40px;
     }
     table .body {
       border-top: solid 0.5px #000;
       border-left: solid 0.5px #000;
       border-right: solid 0.5px #000;
       border-bottom: solid 0.2px #000;
     }
     .center {
       text-align:center;
     }
     .right {
       text-align:right;
     }
     .italic {
       font-style:italic;
     }
   </style>
</head>

<body>
  <table>
    <tr>
			<td colspan="14" style="text-align:center;border-bottom:none;"><strong>RIWAYAT AKTIVITAS KAS</strong></td>
		</tr>
		<tr>
			<td colspan="14" style="text-align:center;border-bottom:none;"><strong>RS BHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
  </table>

  <br/>
  <strong>Laporan per tanggal </strong>
	<?php echo date("d F Y", strtotime($awal)); ?> <strong>sampai tanggal</strong> <?php echo date("d F Y", strtotime($akhir)); ?>	<br/>
	<br/>

  <div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th width="3%" style="text-align:center">No.</th>
					<th style="text-align:center">Tanggal</th>
          <th style="text-align:center">Kode Perkiraan</th>
          <th style="text-align:center">Uraian</th>
          <th style="text-align:center">No. Faktur</th>
          <th style="text-align:center">Atas Nama</th>
          <th style="text-align:center">Kategori</th>
          <th style="text-align:center">Jenis Aktivitas</th>
          <th style="text-align:center">Nilai Aktivitas</th>
          <th style="text-align:center">PPn</th>
          <th style="text-align:center">PPh Pasal 21</th>
          <th style="text-align:center">PPh Pasal 22</th>
          <th style="text-align:center">Total Aktivitas</th>
          <th style="text-align:center">Saldo</th>
				</tr>
			</thead>
			<tbody>
        <?php
          $i = 0;
          foreach ($kas as $value) {
            echo '<tr><td>'.(++$i).'</td>';
            echo '<td>'.date("d/m/Y", strtotime($value['tanggal'])).'</td>
              <td>'.$value['kode_perkiraan'].'</td>
              <td>'.$value['uraian'].'</td>
              <td>'.$value['no_faktur'].'</td>
              <td>'.$value['atas_nama'].'</td>
              <td>'.$value['kategori'].'</td>
              <td>'.$value['jenis_aktivitas'].'</td>
              <td>'.number_format($value['nilai_aktivitas'], 0).'</td>
              <td>'.number_format($value['ppn'], 0).'</td>
              <td>'.number_format($value['pph21'], 0).'</td>
              <td>'.number_format($value['pph22'], 0).'</td>
              <td>'.number_format($value['total_aktivitas'], 0).'</td>
              <td>'.number_format($value['saldo'], 0).'</td>
              ';
          }
        ?>
			</tbody>
		</table>
	</div>
</body>
</html>
