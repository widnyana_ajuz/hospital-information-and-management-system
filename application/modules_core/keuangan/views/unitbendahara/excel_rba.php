<?php
$sekarang = str_replace('-', "_", date('d-m-Y'));
$title = 'Rencana_Bisnis_Anggaran_'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Rencana Bisnis Anggaran</title>
  <style>
     .grup-pertanyaan {
     text-align: center;
     border: solid 1px #000;
    }
     table td {
       border-bottom: solid 0.5px #000;
       font-size: 12pt;
       vertical-align:middle;
       line-height:40px;
       width: 300px;
     }
     .keterangan_pertanyaan {
       font-size: 8pt;
     }
     table .nama_matkul{
       text-transform:capitalize;
     }
     table {
       width: 100%;
     }
     table .header {
       background-color: yellow;
       border-top: solid 0.5px #000;
       border-left: solid 0.5px #000;
       border-right: solid 0.5px #000;
       border-bottom: solid 0.2px #000;
       font-weight: bold;
       vertical-align:middle;
       line-height:40px;
     }
     table .body {
       border-top: solid 0.5px #000;
       border-left: solid 0.5px #000;
       border-right: solid 0.5px #000;
       border-bottom: solid 0.2px #000;
     }
     .center {
       text-align:center;
     }
     .right {
       text-align:right;
     }
     .italic {
       font-style:italic;
     }
   </style>
</head>

<body>
  <table>
    <tr>
			<td colspan="17" style="text-align:center;border-bottom:none;"><strong>RENCANA BISNIS DAN ANGGARAN</strong></td>
		</tr>
		<tr>
			<td colspan="17" style="text-align:center;border-bottom:none;"><strong>RS BHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
  </table>

  <br/>
  <table>
    <?php
      $tgl = DateTime::createFromFormat('Y-m-d', $rba['tanggal']);
      echo '<tr><td colspan="2">Nomor Formulir: </td><td colspan="6">'.$rba['no_formulir'].'</td></tr>';
      echo '<tr><td colspan="2">Tanggal: </td><td colspan="6" style="text-align:left">'.$tgl->format("d F Y").'</td></tr>';
      echo '<tr><td colspan="2">Tahun Anggaran: </td><td colspan="6" style="text-align:left">'.$rba['thn_anggaran'].'</td></tr>';
      echo '<tr><td colspan="2">Urusan Pemerintahan: </td><td colspan="6">'.$rba['urusan_pemerintahan'].'</td></tr>';
      echo '<tr><td colspan="2">Organizer: </td><td colspan="6">'.$rba['organizer'].'</td></tr>';
      echo '<tr><td colspan="2">Program: </td><td colspan="6">'.$rba['program'].'</td></tr>';
      echo '<tr><td colspan="2">Kegiatan: </td><td colspan="6">'.$rba['kegiatan'].'</td></tr>';
      echo '<tr><td colspan="2">Lokasi: </td><td colspan="6">'.$rba['lokasi'].'</td></tr>';
      echo '<tr><td colspan="2">Sumber Dana: </td><td colspan="6">'.$rba['sumber_dana'].'</td></tr>';
    ?>
  </table>

	<br/>
	<br/>

  <div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th width="3%" style="text-align:center">No.</th>
					<th style="text-align:center">Kode Rekening</th>
          <th style="text-align:center">Belanja</th>
          <th style="text-align:center">Uraian</th>
          <th style="text-align:center">Volume</th>
          <th style="text-align:center">Satuan</th>
          <th style="text-align:center">Harga Satuan</th>
          <th style="text-align:center">Jumlah</th>
				</tr>
			</thead>
			<tbody>
        <?php
          $i = 0;
          foreach ($rba_detail as $value) {
            echo '<tr><td>'.(++$i).'</td>';
            echo '<td>'.$value['kode_rekening'].'</td>
              <td>'.$value['belanja'].'</td>
              <td>'.$value['uraian'].'</td>
              <td>'.number_format($value['volume'], 0).'</td>
              <td>'.$value['satuan'].'</td>
              <td>'.number_format($value['harga_satuan'], 0).'</td>
              <td>'.number_format($value['jumlah'], 0).'</td>';
          }
        ?>
			</tbody>
		</table>
	</div>
</body>
</html>
