<?php

$string = preg_replace('/\s+/', '_', $nama);
$sekarang = str_replace('-', "_", date('d-m-Y'));
$title = "Riwayat_Permintaan_".$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="10" style="text-align:center;border-bottom:none;"><strong>RIWAYAT PERMINTAAN BARANG</strong></td>
		</tr>
		<tr>
			<td colspan="10" style="text-align:center;border-bottom:none;"><strong>  RS DATU SANGGUL RANTAU</strong></td>
		</tr>
	</table>

	<br/>
	<table>
    <?php
      $tgl = DateTime::createFromFormat('Y-m-d H:i:s', $result['tanggal_respond']);
      echo '<tr><td colspan="2">Nomor: </td><td colspan="6" style="text-align:left">'.$result['no_permintaanbarang'].'</td></tr>';
      echo '<tr><td colspan="2">Tanggal: </td><td colspan="6" style="text-align:left">'.$tgl->format('d F Y H:i:s').'</td></tr>';
      echo '<tr><td colspan="2">Keterangan: </td><td colspan="6" style="text-align:left">'.$result['keterangan_request'].'</td></tr>';
    ?>
  </table>
	<br/><br>

	<!-- Hasil Evaluasi Kelas -->
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th width="3%" style="text-align:center">No.</th>
					<th style="text-align:center">Nama Barang</th>
			          <th style="text-align:center">Satuan</th>
			          <th style="text-align:center">Merek</th>
			          <th style="text-align:center">Stok Gudang</th>
			          <th style="text-align:center">Diminta</th>
			          <th style="text-align:center">Diberikan</th>
			          <th style="text-align:center">Harga</th>
				</tr>
			</thead>
			<tbody>
				<?php
          			$i = 0;
          			foreach ($detail as $value) {
            			echo '<tr><td>'.(++$i).'</td>';
            			echo '<td>'.$value['nama'].'</td>
              			<td>'.$value['satuan'].'</td>
              			<td>'.$value['nama_merk'].'</td>
              			<td>'.$value['stok'].'</td>
              			<td>'.$value['jumlah_request'].'</td>
              			<td>'.$value['jumlah_approved'].'</td>
              			<td>'.number_format($value['harga'], 0).'</td>';
          			}
        		?>
			</tbody>
		</table>
	</div>
</body>
</html>