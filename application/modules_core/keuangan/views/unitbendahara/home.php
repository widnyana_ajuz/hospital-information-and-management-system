<br>
<div class="title">
	<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>keuangan/homeunitbendahara">UNIT REALISASI</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Buku Kas Umum</a>
	</li>
</div>

<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	 	<li class="active"><a href="#kas" class="cl" data-toggle="tab">Buku Kas Umum</a></li>
	 	<li ><a href="#data" class="cl" data-toggle="tab">Rencana Bisnis dan Anggaran</a></li>
	  <li ><a href="#list" class="cl" data-toggle="tab">Permintaan Logistik</a></li>
	  <li><a href="#rba" class="cl" data-toggle="tab">Laporan Rekap Pendapatan</a></li>
	  <li><a href="#logistik" class="cl" data-toggle="tab">Logistik</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
		<div class="modal fade" id="detailRBA" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-400px">
			<div class="modal-dialog">
				<div class="modal-content" style="width:1000px">
					<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h3 class="modal-title" id="myModalLabel">Detail Riwayat RBA</h3>
        	</div>
        	<div class="modal-body">
        		<div class="informasi">
							<form class="form-horizontal" role="form">
								<div class="form-group">
									<label class="control-label col-md-3">Nomor Formulir</label>
									<div class="col-md-4">
										<input type="text" class="form-control" id="no_formulir" name="no_formulir" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Tanggal</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="tgl" name="tgl" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Tahun Anggaran</label>
									<div class="col-md-2">
										<input  type="text" class="form-control" id="thn_anggaran" name="thn_anggaran" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Urusan Pemerintahan</label>
									<div class="col-md-7">
										<input  type="text" class="form-control" id="urusanpemerintahan" name="urusanpemerintahan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Organizer</label>
									<div class="col-md-7">
										<input  type="text" class="form-control" id="organizer" name="organizer" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Program</label>
									<div class="col-md-7">
										<input  type="text" class="form-control" id="program" name="program" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Kegiatan</label>
									<div class="col-md-7">
										<input  type="text" class="form-control" id="kegiatan" name="kegiatan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Lokasi Kegiatan</label>
									<div class="col-md-5">
										<input  type="text" class="form-control" id="lokasikegiatan" name="lokasikegiatan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Sumber Dana</label>
									<div class="col-md-4">
										<input  type="text" class="form-control" id="sumberdana" name="sumberdana" readonly>
									</div>
								</div>
							</form>
						</div>
						<hr class="garis">
						<div class="tabelinformasi">

							<div class="portlet box red">
								<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabeldetailriwayat">
										<thead>
											<tr class="info">
												<th>No</th>
												<th>Kode Rekening</th>
												<th>Belanja</th>
												<th>Uraian</th>
												<th>Volume</th>
												<th>Satuan</th>
												<th>Harga Satuan</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody >
										</tbody>
									</table>
								</div>
							</div>
						</div>
        	</div>
        	<div class="modal-footer">
 			   		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			  	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="viewNP" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-400px">
			<div class="modal-dialog">
				<div class="modal-content" style="width:1000px">
					<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        	<h3 class="modal-title" id="myModalLabel">Detail</h3>
	        </div>
	        <div class="modal-body">
	        	<form class="form-horizontal" role="form">
			      	<div class="informasi" id="info1">
								<div class="form-group">
									<label class="control-label col-md-3">Nomor</label>
									<div class="col-md-4">
										<label class="control-label" id="nmrLog">Nomor</label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Tanggal</label>
									<div class="col-md-4">
										<label class="control-label" id="tgLog">Nomor</label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Keterangan</label>
									<div class="col-md-4">
										<label class="control-label" id="txtket">Nomor</label>
									</div>
								</div>
							</div>
						</form>
						<div class="tabelinformasi">
							<div class="portlet box red">
								<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover table-responsive">
										<thead>
											<tr class="info">
												<th>Nama Barang</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Stok Gudang</th>
												<th>Diminta</th>
												<th style="width:100px;">Diberikan</th>
												<th style="width:100px;">Harga</th>
											</tr>
										</thead>
										<tbody id="tbodydetailriwayatpersetujuan">
										</tbody>
									</table>
								</div>
							</div>
						</div>
	        </div>
	        <div class="modal-footer">
	 			  	<button type="button" class="btn btn-success " >Proses</button>
	 			  	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				  </div>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="list">
			
			<div class="tabelinformasi">
				<div class="portlet box red">
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT">
							<thead>
								<tr class="info">
									<th style="text-align:center;width:20px;">No.</th>
									<th>No. Pengadaan</th>
									<th>Petugas Input</th>
									<th>Keterangan</th>
									<th>Status</th>
									<th width="100">Action</th>
								</tr>
							</thead>
							<tbody id="tbodyriwayatpersetujuan">
								<?php  
									if (isset($riwayatpermintaanbarang)) {
										if (!empty($riwayatpermintaanbarang)) {
											$i=1;
											foreach ($riwayatpermintaanbarang as $value) {
												echo '<tr>
														<td style="text-align:center">'.($i++).'</td>
														<td>'.$value['no_permintaanbarang'].'</td>
														<td>'.$value['nama_petugas'].'</td>
														<td>'.$value['keterangan_request'].'</td>
														<td>'.($value['is_responded'] = 1 ? "Sudah Diproses": "Belum Diproses").'</td>
														<td style="text-align:center">
															<input type="hidden" class="detail_persetujuan_id" value="'.$value['barang_permintaan_id'].'">
															<input type="hidden" class="tgl" value="'.$value['tanggal_request'].'">
															<a href="#" class="cekdetailpersetujuan" data-toggle="modal" data-target="#viewNP">
															<i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>
															<a href="homeunitbendahara/print_riwayat_permintaan/'.$value['barang_permintaan_id'].'"><i class="glyphicon glyphicon-print" data-toggle="tooltip" data-placement="top" title="Print"></i></a>
														</td>						
													</tr>';
											}
										}
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	  	<div class="tab-pane" id="rba">
	  		<div class="informasi" style="margin-left:30px;">
        		<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Pendapatan Per Jenis Penerimaan</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form" method="post" action="<?php echo base_url()?>keuangan/homeunitbendahara/print_pendapatan_per_jenis">
          			<div class="tabelinformasi" >
            			<div class="form-group">
              				<label class="control-label col-md-1"> Tanggal</label>
              				<div class="col-md-3">
                				<div class="input-daterange input-group" id="datepicker">
                  					<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_jenis1" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
                  					<div class="input-group-addon">to</div>
                  					<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_jenis2" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
                				</div>
              				</div>
              				<div class="pull-right" style="margin-right:20px">
                				<div class="col-md-3">
                  				<button class="btn btn-info">Simpan ke Excel(.xls)</button>
                			</div>
              			</div>
            		</div>
          		</div>
        	</form>
      	</div>
		<br>

		<div class="informasi" style="margin-left:30px;">
	    	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Pendapatan Per Unit</div>
	      	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form" method="post" action="<?php echo base_url()?>keuangan/homeunitpendapatan/print_rekap_unit">
		      	<div class="tabelinformasi" >
				    <div class="form-group">
		        		<label class="control-label col-md-1"> Tanggal</label>
		        		<div class="col-md-3">
							<div class="input-daterange input-group" id="datepicker">
							    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_unit1" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
							    <div class="input-group-addon">to</div>
							    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_unit2" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
							</div>
						</div>
		        	</div>
              		<div class="form-group">
                		<label class="control-label col-md-1">Unit</label>
                		<div class="col-md-3">
                  			<select class="form-control" name="select_jenis" required>
                    			<option value="" selected>Pilih</option>
                				<?php
                  					foreach ($units as $value) {
                	    				echo '<option value='.$value['dept_id'].'>'.$value['nama_dept'].'</option>';
              	    				}
                				?>
                  			</select>
		                </div>
                		<div class="pull-right" style="margin-right:20px">
			        		<div class="col-md-3">
			        			<button class="btn btn-info">Simpan ke Excel(.xls)</button>
			        		</div>
		        		</div>
              		</div>
	        	</div>
			</form>
		</div>
		<br>
		
		<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Pendapatan Jasa Pelayanan Medis</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form">
		        	<div class="tabelinformasi" >
			        	<div class="form-group">
	        				<label class="control-label col-md-1"> Tanggal</label>
	        				<div class="col-md-3">
										<div class="input-daterange input-group" id="datepicker">
							    		<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="start"  data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly placeholder="<?php echo date("d/m/Y");?>" />
							    		<div class="input-group-addon">to</div>
							    		<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>" />
										</div>
									</div>
	        				<div class="pull-right" style="margin-right:20px">
		        				<div class="col-md-3">
		        					<button class="btn btn-info">Simpan ke Excel(.xls)</button>
		        				</div>
	        				</div>
	        			</div>
        			</div>
						</form>
					</div>
					<br>
					<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Pendapatan Jasa Pelayanan Farmasi</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form">
			        <div class="tabelinformasi" >
				        <div class="form-group">
		        			<label class="control-label col-md-1"> Tanggal</label>
		        			<div class="col-md-3">
										<div class="input-daterange input-group" id="datepicker">
								    	<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="start"  data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly placeholder="<?php echo date("d/m/Y");?>" />
								    	<div class="input-group-addon">to</div>
								    	<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>" />
										</div>
									</div>
		        			<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
						</form>
					</div>
					<br><br>
	    	</div>
	    	<div class="tab-pane" id="data">
	    		<div class="dropdown" id="btnBawahInventori" style="margin-left:10px;width:98.5%">
	          		<div id="titleInformasi">Riwayat RBA</div>
	      		</div>

					<div class="tabelinformasi">
						<div class="portlet box red">
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tabelriwayatrba">
									<thead>
										<tr class="info">
											<th width="20">No</th>
											<th>Nomor Formulir</th>
											<th>Tanggal</th>
											<th>Tahun Anggaran</th>
											<th>Lokasi</th>
											<th>Sumber Dana</th>
											<th width="100">Action</th>
										</tr>
									</thead>
									<tbody id="tbodyriwayatrba">
										<?php
											if (isset($riwayat)) {
												if (!empty($riwayat)) {
													$i = 0;
													foreach ($riwayat as $value) {
														$tgl = DateTime::createFromFormat('Y-m-d', $value['tanggal']);
														echo '<tr>
															<td>'.(++$i).'</td>
															<td>'.$value['no_formulir'].'</td>
															<td>'.$tgl->format('d F Y').'</td>
															<td>'.$value['thn_anggaran'].'</td>
															<td>'.$value['lokasi'].'</td>
															<td>'.$value['sumber_dana'].'</td>
															<td align="center"><a href="#detailRBA" id="viewdetailrba" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>
																<a href="'.base_url().'keuangan/homeunitbendahara/print_detail_rba/'.$value['id'].'"><i class="glyphicon glyphicon-tasks" data-toggle="tooltip" data-placement="top" title="Export Excel"></i></a>
																<input type="hidden" id="val_urusan" value="'.$value['urusan_pemerintahan'].'">
																<input type="hidden" id="val_organizer" value="'.$value['organizer'].'">
																<input type="hidden" id="val_program" value="'.$value['program'].'">
																<input type="hidden" id="val_kegiatan" value="'.$value['kegiatan'].'">
																<input type="hidden" id="val_lokasi" value="'.$value['lokasi'].'">
																<input type="hidden" id="val_id" value="'.$value['id'].'">
															</td>
														</tr>';
													}
												}
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
	    	</div>

        <div class="tab-pane active" id="kas">
       		<div class="dropdown" id="bbtambah" style="margin-left:10px;width:98.5%">
		          <div id="titleInformasi">Tambah Aktivitas Kas</div>
		          <div id="btnBawahInventori" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
          	</div>
          	<br>
          	<div id="ibbtambah">
				<form class="form-horizontal" id="submit_form" method="POST">
			        <div class="informasi" >
			          	<div class="form-group">
			          		<label class="control-label col-md-2">Tanggal</label>
			          		<div class="col-md-2">
			          			<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" id="add_tgl" style="cursor:pointer;background-color:white" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
								</div>
			            	</div>
			            </div>
			            <div class="form-group">
			            	<label class="control-label col-md-2">Kode Perkiraan</label>
			            	<div class="col-md-3">
			            		<input  type="text" class="form-control" id="add_kode" placeholder="Kode Perkiraan" required>
			            	</div>
			            </div>
			            <div class="form-group">
			            	<label class="control-label col-md-2">Uraian</label>
			            	<div class="col-md-4">
			            		<input  type="text" class="form-control" id="add_uraian" placeholder="Uraian" required>
			            	</div>
			            </div>
			            <div class="form-group">
			            	<label class="control-label col-md-2">No Faktur</label>
			            	<div class="col-md-4">
			            		<input  type="text" class="form-control" id="add_faktur" placeholder="Nomor Faktur" required>
			            	</div>
			            </div>
			            <div class="form-group">
			            	<label class="control-label col-md-2">Atas Nama</label>
			            	<div class="col-md-4">
			            		<input  type="text" class="form-control" id="add_atas" placeholder="Atas Nama" required>
			            	</div>
			            </div>
		            	<div class="form-group">
		            		<label class="control-label col-md-2">Kategori</label>
		            		<div class="col-md-3">
		            			<select class="form-control" id="add_kategori" required>
		            				<option selected>Pilih</option>
		            				<option value="PENERIMAAN">PENERIMAAN</option>
		            				<option value="PENGELUARAN">PENGELUARAN</option>
		            			</select>
		            		</div>
		            	</div>
			            <div class="form-group">
			            	<label class="control-label col-md-2">Jenis Aktivitas</label>
			            	<div class="col-md-3">
											<select class="form-control" id="add_jenis" required>
												<option value="" selected>Pilih</option>
												<option value="BELANJA BARANG DAN JASA">BELANJA BARANG DAN JASA</option>
												<option value="BELANJA MODAL">BELANJA MODAL</option>
												<option value="BELANJA PEGAWAI">BELANJA PEGAWAI</option>
												<option value="JASA MEDIS">JASA MEDIS</option>
												<option value="PENDAPATAN RS">PENDAPATAN RS</option>
												<option value="LAIN-LAIN">LAIN-LAIN</option>
											</select>
			            	</div>
			            </div>
			            <div class="form-group">
			            	<label class="control-label col-md-2">Nilai Aktivitas</label>
			            	<div class="col-md-3">
											<div class="input-group">
												<div class="input-group-addon">Rp</div>
												<input type="text" class="form-control text-right" id="add_nilai" placeholder="0" required>
											</div>
			            	</div>
			            </div>
			            <div class="form-group">
			            	<label class="control-label col-md-2">PPn</label>
			            	<div class="col-md-2">
			            		<div class="input-group">
			            			<input type="text" class="form-control text-right" id="add_ppn" placeholder="0">
			            			<div class="input-group-addon">%</div>
											</div>
			            	</div>
			            </div>
			            <div class="form-group">
			            	<label class="control-label col-md-2">PPh Pasal 21</label>
			            	<div class="col-md-2">
			            		<div class="input-group">
			            			<input type="text" class="form-control text-right" id="add_pph21" placeholder="0">
			            			<div class="input-group-addon">%</div>
			            		</div>
			            	</div>
			            </div>
			            <div class="form-group">
			            	<label class="control-label col-md-2">PPh Pasal 22</label>
			            	<div class="col-md-2">
			            		<div class="input-group">
			            			<input type="text" class="form-control text-right" id="add_pph22" placeholder="0">
			            			<div class="input-group-addon">%</div>
											</div>
			            	</div>
			            </div>
			            <br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span style=" padding:0px 10px 0px 10px;">
								<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
								<button type="submit" class="btn btn-success">SIMPAN</button>
							</span>
						</div>
						<br>
			        </div>
			        <br>
		        </form>
		    </div>
	        
	        <div class="dropdown" id="bbriwayat" style="margin-left:10px;width:98.5%">
	          	<div id="titleInformasi">Riwayat Aktivitas Kas</div>
	          	<div id="btnBawahInventori" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
          	</div>
 		 	<br>
 		 	<div id="ibbriwayat">
				<form method="POST" id="frmsearchkas">
		     		<div class="search" style="margin-left:400px;">
						<label class="control-label col-md-3">
							<i class="fa fa-search">&nbsp;&nbsp;</i>Cari Aktivitas Kas <span class="required" style="color : red">* </span>
						</label>
						<div class="col-md-4" style="margin-left:-50px">
							<input type="text" id="katakuncikas" class="form-control" placeholder="Masukkan Kata Kunci" >
		        		</div>
			      		<button type="submit" class="btn btn-info">Cari</button>
					</div>
				</form>
				<hr class="garis">

				<form class="form-horizontal" method="post" action="<?php echo base_url()?>keuangan/homeunitbendahara/print_aktivitas_kas">
					<label class=" col-md-1" style="margin-right:-60px; padding-top:7px;"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by</label>
					<div class="col-md-3" style="margin-left:30px;">
						<div class="input-daterange input-group" id="datepicker">
					  	<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal1" id="tanggal1"  data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
					  	<div class="input-group-addon">to</div>
					  	<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal2" id="tanggal2"  data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
						</div>
					</div>
					<br>
					<br>
					<div class="informasi" style="margin-left:20px">
						<div class="form-group" >
							<label class="control-label col-md-2">Saldo Akhir :</label>
							<label class="control-label col-md-3" style="font-weight:bold;margin-left:-80px;color:red">
								<?php
									if (isset($last_saldo)) {
										if (!empty($last_saldo)) {
											echo number_format($last_saldo['saldo'], 0 , '', '.');
										}else {
											echo '0';
										}
									}
								?>
							</label>
						</div>
					</div>

					<div class="tabelinformasi">
						<div class="portlet box red">
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover tableDTUtamaScroll table-responsive">
									<thead>
										<tr class="info">
											<th width="20">No.</th>
											<th>Tanggal</th>
											<th>Kode Perkiraan</th>
											<th>Uraian</th>
											<th>No. Faktur</th>
											<th>Atas Nama</th>
											<th>Kategori</th>
											<th>Jenis Aktivitas</th>
											<th>Nilai Aktivitas</th>
											<th>PPn</th>
											<th>PPh Pasal 21</th>
											<th>PPh Pasal 22</th>
											<th>Total Aktivitas</th>
											<th>Saldo</th>
										</tr>
									</thead>
									<tbody id="tbodybukukas">
										<?php
											if (isset($buku)) {
												if (!empty($buku)) {
													$i = 0;
													foreach ($buku as $value) {
														echo '<tr>
															<td>'.(++$i).'</td>
															<td align="center">'.date("d F Y", strtotime($value['tanggal'])).'</td>
															<td>'.$value['kode_perkiraan'].'</td>
															<td>'.$value['uraian'].'</td>
															<td>'.$value['no_faktur'].'</td>
															<td>'.$value['atas_nama'].'</td>
															<td>'.$value['kategori'].'</td>
															<td>'.$value['jenis_aktivitas'].'</td>
															<td class="text-right">'.number_format($value['nilai_aktivitas'], 0, '', '.').'</td>
															<td class="text-right">'.number_format($value['ppn'], 0, '', '.').'</td>
															<td class="text-right">'.number_format($value['pph21'], 0, '', '.').'</td>
															<td class="text-right">'.number_format($value['pph22'], 0 , '', '.').'</td>
															<td class="text-right">'.number_format($value['total_aktivitas'], 0 ,'', '.').'</td>
															<td class="text-right">'.number_format($value['saldo'], 0 , '', '.').'</td>
														</tr>';
													}
												}
											}
										?>
									</tbody>
								</table>
							</div>
							<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
						</div>
					</div>
				</form>
				
			</div>
        </div>

       	<div class="tab-pane" id="logistik">
    	<div class="modal fade" id="modalbarang" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" style="width:900px;">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
        			</div>
        			<div class="modal-body">

	        			<div class="form-group">
	        				<form method="post" class="form-horizontal" role="form" id="formmintabarang">
								<div class="form-group">	
									<div class="col-md-5" style="margin-left:20px;">
										<input type="text" class="form-control" name="katakunci" id="katakuncimintabarang" placeholder="Nama barang"/>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-info">Cari</button>
									</div>
									<br><br>	
								</div>		
							</form>
							<div style="margin-right:10px;margin-left:10px;"><hr></div>
							<div class="portlet-body" style="margin: 0px 20px 0px 15px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%">
									<thead>
										<tr class="info">
											<th>Nama Barang</th>
											<th>Satuan</th>
											<th>Merek</th>
											<th>Tahun Pengadaan</th>
											<th>Stok Gudang</th>
											<th width="10%">Pilih</th>
										</tr>
									</thead>
									<tbody id="tbodybarangpermintaan">
										<tr>
											<td colspan="6" style="text-align:center">Cari data Barang</td>
										</tr>
									</tbody>
								</table>												
							</div>
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>
       	<div class="dropdown" id="btnBawahInventoriBarang">
            <div id="titleInformasi">Inventori</div>
            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
        </div>
        <div id="infoInventoriBarang">
			
			<div class="form-group" >
				<div class="portlet-body" style="margin: 30px 10px 20px 10px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tblinventorigudangunit">
						<thead>
							<tr class="info" >
								<th width="20">No.</th>
								<th > Nama Barang </th>
								<th > Merek </th>
								<th > Harga </th>
								<th > Stok </th>
								<th > Satuan </th>
								<th > Tahun Pengadaan</th>
								<th > Sumber Dana</th>
								<th width="100"> Action </th>

							</tr>
						</thead>
						<tbody id="tbodyinventoribarang">
							<?php 
								if (isset($inventoribarang)) {
									if (!empty($inventoribarang)) {
										$i = 1;
										foreach ($inventoribarang as $value) {
											echo '<tr>
													<td>'.($i++).'</td>
													<td>'.$value['nama'].'</td>
													<td>'.$value['nama_merk'].'</td>
													<td>'.$value['harga'].'</td>
													<td>'.$value['stok'].'</td>
													<td>'.$value['satuan'].'</td>
													<td>'.$value['tahun_pengadaan'].'</td>
													<td>'.$value['sumber_dana'].'</td>
													<td style="text-align:center">
														<input type="hidden" class="barang_detail_inout" value="'.$value['barang_detail_id'].'">
														<a href="#inoutbar" data-toggle="modal" class="edBarang" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
														<a href="#edInvenBerBar" data-toggle="modal" class="detailinvenbarang"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
													</td>
												</tr>';
										}
									}
								}
							?>
								
						</tbody>
					</table>
				</div>
				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
        	</div>
        </div>
		<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<form class="form-horizontal" role="form" style="margin-left:30px;" id="forminoutbarang">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
	        			</div>
	        			<div class="modal-body">
		        			<div class="form-group">
		        				<label class="control-label col-md-3" >Tanggal </label>
								<div class="col-md-6" >
					         		<div class="input-icon">
										<i class="fa fa-calendar"></i>
										<input type="text" style="cursor:pointer;background-color:white" id="tanggalinout" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
									
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" >In / Out </label>
								<div class="col-md-6">
					         		<select class="form-control select" name="io" id="io">
										<option value="IN" selected>IN</option>
										<option value="OUT">OUT</option>					
									</select>
								</div>
							</div>
							<div class="form-group">
		        				<label class="control-label col-md-3" >Jumlah in/out</label>
								<div class="col-md-6" >
					         		<input type="text" class="form-control" id="jmlInOut" name="jmlInOut" placeholder="Jumlah">
								</div>
							</div>
							<div class="form-group">
		        				<label class="control-label col-md-3" >Sisa Stok </label>
								<div class="col-md-6" >
					         		<input type="text" class="form-control" id="sisaInOut" name="sisaInOut" placeholder="Sisa Stok" readonly>
								</div>
							</div>
							<div class="form-group">
		        				<label class="control-label col-md-3" >Keterangan </label>
								<div class="col-md-6" >
									<textarea class="form-control" id="keteranganIO" placeholder="Keterangan"></textarea>
								</div>
							</div>										
	        			</div>
	        			<div class="modal-footer">
	        				<input type="hidden" id="id_barang_inoutprocess">
	 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
	 			       		<button type="submit" class="btn btn-success">Simpan</button>
				      	</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content" >
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
        			</div>
        			<div class="modal-body">
	        			<form class="form-horizontal" role="form">
			            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
								<thead>
									<tr class="info" >
										<th> Waktu </th>
										<th> IN / OUT </th>
										<th> Jumlah </th>
										<th> Keterangan </th>
									</tr>
								</thead>
								<tbody id="tbodydetailbrginventori">
									<tr>
										<td colspan="4" style="text-align:center">Tidak ada detail in-out</td>
									</tr>
										
								</tbody>
							</table>
						</form>
						
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>
		<br>

		<div class="dropdown" id="btnBawahPermintaanBarang" style="margin-left:10px;width:98.5%">
            <div id="titleInformasi">Permintaan Logistik</div>
            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
        </div>
        <div id="infoPermintaanBarang">
        	<form class="form-horizontal" role="form" method="post" id="permintaanbarangunit">
            	<div class="informasi">
            		<br>
        			<div class="form-group">
        				<div class="col-md-2">
        					<label class="control-label">Nomor Permintaan</label>
        				</div>
        				<div class="col-md-3">
        					<input type="text" class="form-control" name="noPermFarmBers" id="nomorpermintaanbarang" placeholder="Nomor Permintaan"/>
						</div>
						<div class="col-md-1"></div>
						<div class="col-md-2">
        					<label class="control-label">Tanggal Permintaan</label>
        				</div>
        				<div class="col-md-2">
        					<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanbarang" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
							</div>
						</div>
        			</div>
        			<div class="form-group">
        				<div class="col-md-2">
        					<label class="control-label">Keterangan</label>
        				</div>
        				<div class="col-md-3">	
							<textarea class="form-control" id="keteranganpermintaanbarang" name="ketObatFarBers"></textarea>	
						</div>
        			</div>
        		</div>
				<a href="#modalbarang" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Barang</i></a>
				<div class="clearfix"></div>

				<div class="portlet box red">
					<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
							<thead>
								<tr class="info" >
									<th> Nama Barang </th>
									<th> Satuan </th>
									<th> Merek </th>
									<th> Tahun Pengadaan </th>
									<th> Stok Gudang </th>
									<th> Jumlah Diminta </th>
									<th width="80"> Action </th>			
								</tr>
							</thead>
							<tbody  id="addinputmintabarang">
								<?php echo '<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>'; ?>
							</tbody>
						</table>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<button class="btn btn-warning" type="reset" id="batalpermintaanfarmasi">RESET</button>
						<button class="btn btn-success" type="submit">SIMPAN</button>
					</div>
					<br>
				</div>	
			</form>
		</div>	    
		<br>
    </div>

	<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" >
				<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        			<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
        		</div>
        		<div class="modal-body">
	        		<form class="form-horizontal informasi" role="form">
		        		<div class="form-group">
		        			<label class="control-label col-md-3" >Tanggal</label>
									<div class="col-md-4" >
			         			<div class="input-icon">
										<i class="fa fa-calendar"></i>
										<input type="text" style="cursor:pointer;background-color:white" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
									</div>
		         		</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" >In / Out</label>
								<div class="col-md-4">
					      	<select class="form-control select" name="ioberbar" id="ioberbar">
										<option value="IN" selected>IN</option>
										<option value="OUT">OUT</option>
									</select>
								</div>
							</div>
							<div class="form-group">
		        		<label class="control-label col-md-3" >Jumlah</label>
								<div class="col-md-4" >
					      	<input type="text" class="form-control" name="jmlInOutBerBar" placeholder="Jumlah">
								</div>
							</div>
							<div class="form-group">
		        		<label class="control-label col-md-3" >Sisa Stok</label>
								<div class="col-md-4" >
					     		<input type="text" class="form-control" name="sisaInOutBerBar" placeholder="Sisa Stok">
								</div>
							</div>
							<div class="form-group">
		        		<label class="control-label col-md-3" >Keterangan</label>
								<div class="col-md-6" >
									<textarea class="form-control" placeholder="Keterangan"></textarea>
								</div>
							</div>
						</form>
	        	</div>
        		<div class="modal-footer">
	        		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
			    	<button type="button" class="btn btn-success" data-dismiss="modal">Simpan</button>
			    </div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" >
				<div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    		<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
    	</div>
    	<div class="modal-body">
    		<form class="form-horizontal" role="form">
	          <table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
							<thead>
								<tr class="info" >
									<th  style="text-align:left" width="10%"> Waktu </th>
									<th  style="text-align:left"> IN / OUT </th>
									<th  style="text-align:left"> Jumlah </th>
									<th  style="text-align:left"> Stok Akhir </th>
									<th  style="text-align:left"> Jenis </th>
									<th  style="text-align:left">  Keterangan </th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</form>
			</div>
			<div class="modal-footer">
		     		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
	    	</div>
			</div>
		</div>
	</div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#bwpermintaanlogistik").click(function(){
			$("#ibwpermintaanlogistik").slideToggle();

		});
		$("#bwinlogistik").click(function(){
			$("#ibwinlogistik").slideToggle();

		});

		$("#bbtambah").click(function(){
			$("#ibbtambah").slideToggle();

		});

		$("#bbriwayat").click(function(){
			$("#ibbriwayat").slideToggle();

		});

		$("#btnBawahPermintaanBarang").click(function(){
			$("#infoPermintaanBarang").slideToggle();

		});
	});
</script>
