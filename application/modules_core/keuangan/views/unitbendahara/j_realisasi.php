<script type="text/javascript">
  $(document).ready(function(){
    $('#add_nilai').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0', vMin:'0'});
    $('#add_ppn').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0', vMin:'0', vMax:'100'});
    $('#add_pph21').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0', vMin:'0', vMax:'100'});
    $('#add_pph22').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0', vMin:'0', vMax:'100'});

    //detail riwayat rba
    $('#tbodyriwayatrba').on('click', '#viewdetailrba', function(e){
      e.preventDefault();
      $('#no_formulir').val($(this).closest('tr').find('td').eq(1).text());
      $('#tgl').val($(this).closest('tr').find('td').eq(2).text());
      $('#thn_anggaran').val($(this).closest('tr').find('td').eq(3).text());
      $('#urusanpemerintahan').val($(this).closest('tr').find('td #val_urusan').val());
      $('#organizer').val($(this).closest('tr').find('td #val_organizer').val());
      $('#program').val($(this).closest('tr').find('td #val_program').val());
      $('#kegiatan').val($(this).closest('tr').find('td #val_kegiatan').val());
      $('#lokasikegiatan').val($(this).closest('tr').find('td').eq(4).text());
      $('#sumberdana').val($(this).closest('tr').find('td').eq(5).text());

      var id = $(this).closest('tr').find('td #val_id').val();

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>keuangan/homeunitbendahara/get_detail_riwayat/" + id,
        success: function(data){
          var tabel = $('#tabeldetailriwayat').DataTable();
          tabel.clear().draw();
          for (var i = 0; i < data.length; i++) {
            tabel.row.add([
              (i+1),
              data[i]['kode_rekening'],
              data[i]['belanja'],
              data[i]['uraian'],
              data[i]['volume'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
              data[i]['satuan'],
              data[i]['harga_satuan'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
              data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
            ]).draw();
          }
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //filter data by date range
    $('#tanggal1').on('change', function(e){
      e.preventDefault();
      var tanggal = {}
      tanggal['tgl1'] = $(this).val();
      tanggal['tgl2'] = $('#tanggal2').val();

      $.ajax({
        type: "POST",
        data: tanggal,
        url: "<?php echo base_url()?>keuangan/homeunitbendahara/get_buku_by_date",
        success: function(data){
          console.log(data);
          $('#tbodybukukas').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              var c = i+1;
              var d = new Date(data[i]['tanggal']);
              var month_names = new Array ( );
              month_names[month_names.length] = "January";
              month_names[month_names.length] = "February";
              month_names[month_names.length] = "March";
              month_names[month_names.length] = "April";
              month_names[month_names.length] = "May";
              month_names[month_names.length] = "June";
              month_names[month_names.length] = "July";
              month_names[month_names.length] = "August";
              month_names[month_names.length] = "September";
              month_names[month_names.length] = "October";
              month_names[month_names.length] = "November";
              month_names[month_names.length] = "December";
              var curr_date = d.getDate();
              var curr_month = d.getMonth(); //Months are zero based
              var curr_year = d.getFullYear();
              var tanggalbaru = curr_date + " " + month_names[curr_month] + " " + curr_year;

              $('#tbodybukukas').append(
                '<tr>'+
                  '<td>'+c+'</td>'+
                  '<td>'+tanggalbaru+'</td>'+
                  '<td>'+data[i]['kode_perkiraan']+'</td>'+
                  '<td>'+data[i]['uraian']+'</td>'+
                  '<td>'+data[i]['no_faktur']+'</td>'+
                  '<td>'+data[i]['atas_nama']+'</td>'+
                  '<td>'+data[i]['kategori']+'</td>'+
                  '<td>'+data[i]['jenis_aktivitas']+'</td>'+
                  '<td class="text-right">'+data[i]['nilai_aktivitas'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['ppn'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['pph21'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['pph22'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['total_aktivitas'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['saldo'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                '</tr>'
              );
            }
          }else {
            $('#tbodybukukas').append('<tr><td colspan="14">Tidak ada data</td></tr>')
          }
        },
        error: function(data){
          console.log(data);;
        }
      })
    })

    $('#tanggal2').on('change', function(e){
      e.preventDefault();
      var tanggal = {}
      tanggal['tgl1'] = $('#tanggal1').val();
      tanggal['tgl2'] = $(this).val();

      $.ajax({
        type: "POST",
        data: tanggal,
        url: "<?php echo base_url()?>keuangan/homeunitbendahara/get_buku_by_date",
        success: function(data){
          console.log(data);
          $('#tbodybukukas').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              var c = i+1;
              var d = new Date(data[i]['tanggal']);
              var month_names = new Array ( );
              month_names[month_names.length] = "January";
              month_names[month_names.length] = "February";
              month_names[month_names.length] = "March";
              month_names[month_names.length] = "April";
              month_names[month_names.length] = "May";
              month_names[month_names.length] = "June";
              month_names[month_names.length] = "July";
              month_names[month_names.length] = "August";
              month_names[month_names.length] = "September";
              month_names[month_names.length] = "October";
              month_names[month_names.length] = "November";
              month_names[month_names.length] = "December";
              var curr_date = d.getDate();
              var curr_month = d.getMonth(); //Months are zero based
              var curr_year = d.getFullYear();
              var tanggalbaru = curr_date + " " + month_names[curr_month] + " " + curr_year;

              $('#tbodybukukas').append(
                '<tr>'+
                  '<td>'+c+'</td>'+
                  '<td>'+tanggalbaru+'</td>'+
                  '<td>'+data[i]['kode_perkiraan']+'</td>'+
                  '<td>'+data[i]['uraian']+'</td>'+
                  '<td>'+data[i]['no_faktur']+'</td>'+
                  '<td>'+data[i]['atas_nama']+'</td>'+
                  '<td>'+data[i]['kategori']+'</td>'+
                  '<td>'+data[i]['jenis_aktivitas']+'</td>'+
                  '<td class="text-right">'+data[i]['nilai_aktivitas'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['ppn'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['pph21'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['pph22'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['total_aktivitas'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['saldo'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                '</tr>'
              );
            }
          }else {
            $('#tbodybukukas').append('<tr><td colspan="14">Tidak ada data</td></tr>')
          }
        },
        error: function(data){
          console.log(data);;
        }
      })
    })

    //search aktivitas kas
    $('#frmsearchkas').submit(function(e){
      e.preventDefault();
      var katakunci = {};
      katakunci['search'] = $('#katakuncikas').val();

      $.ajax({
        type: "POST",
        data: katakunci,
        url: "<?php echo base_url()?>keuangan/homeunitbendahara/search_realisasi",
        success: function(data){
          console.log(data);
          $('#tbodybukukas').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              var c = i+1;
              var d = new Date(data[i]['tanggal']);
              var month_names = new Array ( );
              month_names[month_names.length] = "January";
              month_names[month_names.length] = "February";
              month_names[month_names.length] = "March";
              month_names[month_names.length] = "April";
              month_names[month_names.length] = "May";
              month_names[month_names.length] = "June";
              month_names[month_names.length] = "July";
              month_names[month_names.length] = "August";
              month_names[month_names.length] = "September";
              month_names[month_names.length] = "October";
              month_names[month_names.length] = "November";
              month_names[month_names.length] = "December";
              var curr_date = d.getDate();
              var curr_month = d.getMonth(); //Months are zero based
              var curr_year = d.getFullYear();
              var tanggalbaru = curr_date + " " + month_names[curr_month] + " " + curr_year;

              $('#tbodybukukas').append(
                '<tr>'+
                  '<td>'+c+'</td>'+
                  '<td align="center">'+tanggalbaru+'</td>'+
                  '<td>'+data[i]['kode_perkiraan']+'</td>'+
                  '<td>'+data[i]['uraian']+'</td>'+
                  '<td>'+data[i]['no_faktur']+'</td>'+
                  '<td>'+data[i]['atas_nama']+'</td>'+
                  '<td>'+data[i]['kategori']+'</td>'+
                  '<td>'+data[i]['jenis_aktivitas']+'</td>'+
                  '<td class="text-right">'+data[i]['nilai_aktivitas'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['ppn'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['pph21'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['pph22'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['total_aktivitas'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td class="text-right">'+data[i]['saldo'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                '</tr>'
              );
            }
          }else {
            $('#tbodybukukas').append('<tr><td colspan="14">Tidak ada data</td></tr>')
          }
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //insert data
    $('#submit_form').submit(function(e){
      e.preventDefault();
      var x = confirm("Yakin akan menambahkan data? Data yang sudah ditambahkan tidak dapat dihapus.");
      if (x) {
        var valnilai = Number($('#add_nilai').autoNumeric('get'));
        var valppn = Number($('#add_ppn').autoNumeric('get'))/100 * valnilai;
        var valpph21 = Number($('#add_pph21').autoNumeric('get'))/100 * valnilai;
        var valpph22 = Number($('#add_pph22').autoNumeric('get'))/100 * valnilai;

        var item = {};
        item['tanggal'] = $('#add_tgl').val();
        item['kode_perkiraan'] = $('#add_kode').val();
        item['uraian'] = $('#add_uraian').val();
        item['no_faktur'] = $('#add_faktur').val();
        item['atas_nama'] = $('#add_atas').val();
        item['kategori'] = $('#add_kategori').val();
        item['jenis_aktivitas'] = $('#add_jenis').val();
        item['nilai_aktivitas'] = valnilai;
        item['ppn'] = valppn;
        item['pph21'] = valpph21;
        item['pph22'] = valpph22;
        item['total_aktivitas'] = Number(valnilai) - Number(valppn) - Number(valpph21) - Number(valpph22);

        $.ajax({
          type: "POST",
          data: item,
          url: "<?php echo base_url()?>keuangan/homeunitbendahara/insert_aktivitas",
          success: function(data){
            console.log(data);
            myAlert('DATA BERHASIL DITAMBAH');
            window.location.replace("<?php echo base_url()?>keuangan/homeunitbendahara");
          },
          error: function(data){
            console.log(data);
          }
        })
      }else {
        return false;
      }
    })
    

    /*logistik*/
    var this_io;
    $('#tbodyinventoribarang').on('click', 'tr td a.edBarang', function (e) {
      e.preventDefault();
      $('#id_barang_inoutprocess').val($(this).closest('tr').find('td .barang_detail_inout').val());
      var jlh = $(this).closest('tr').find('td').eq(4).text();
      this_io = $(this);
      $('#sisaInOut').val(jlh);

      $('#jmlInOut').on('change', function (e) {
        e.preventDefault();

        var is_in = $('#io').find('option:selected').val();
        var jmlInOut = $('#jmlInOut').val();
        var sisa = jlh;//$('#sisaInOut').val();
        var hasil ="";
        if (is_in == 'IN') {
          hasil = Number(jmlInOut) + Number(sisa);
        }else{      
          hasil = Number(sisa) - Number(jmlInOut);
        }

        if (jmlInOut == '') {
          hasil = Number(sisa);
        }
        $('#sisaInOut').val(hasil);     
      })

      $('#io').on('change', function () {
        var jumlah = Number($('#jmlInOut').val());
        var sisa = Number(jlh);//Number($('#sisaInOut').val());

        var isout = $('#io').find('option:selected').val();
        if (isout === 'IN') {
          $('#sisaInOut').val(jumlah + sisa);
        } else{
          $('#sisaInOut').val(sisa - jumlah);
        };
      })
    })
  
    $('#forminoutbarang').submit(function (e) {
      e.preventDefault();

      var item = {};
      item['barang_detail_id'] = $('#id_barang_inoutprocess').val();
      item['jumlah'] = $('#jmlInOut').val();
      item['sisa'] = $('#sisaInOut').val();
      item['is_out'] = $('#io').find('option:selected').val();
        item['tanggal'] = $('#tanggalinout').val();
        item['keterangan'] = $('#keteranganIO').val();
        //console.log(item);return false;
        if (item['jumlah'] != "") {
          $.ajax({
            type: "POST",
            data: item,
            url: "<?php echo base_url()?>keuangan/homeunitbendahara/input_in_outbarang",
            success: function (data) {
              if (data == "true") {
                myAlert('data berhasil disimpan');
                $('#keteranganIO').val('');
                $('#jmlInOut').val('');
                this_io.closest('tr').find('td').eq(4).text(item['sisa']);
                $('#inoutbar').modal('hide'); 
              } else{
                myAlert('gagal, terdapat kesalahan');
              };
            },
            error: function (data) {
              myAlert('gagal');
            }
          })
      } else{
        myAlert('isi data dengan benar');
        $('#jmlInOut').focus();
      };      
    })

    $("#tbodyinventoribarang").on('click', 'tr td a.detailinvenbarang', function (e) {
      var id = $(this).closest('tr').find('td .barang_detail_inout').val();

       $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>keuangan/homeunitbendahara/get_detail_inventori/" + id,
          success: function (data) {
            console.log(data);
            $('#tbodydetailbrginventori').empty();
            for(var i = 0; i < data.length ; i++){
              $('#tbodydetailbrginventori').append(
              '<tr>'+
                '<td align="center">'+format_date(data[i]['tanggal'])+'</td>'+
                '<td>'+data[i]['is_out']+'</td>'+
                '<td align="right">'+data[i]['jumlah']+'</td>'+
                '<td>'+data[i]['keterangan']+'</td>'+
              '</tr>'
              )
            }
          },
          error: function (data) {
            myAlert('gagal');
          }
        })
    })
    
    $('#formmintabarang').submit(function (e) {
      e.preventDefault();
      var item ={};
      item['katakunci'] = $('#katakuncimintabarang').val();
      $.ajax({
        type: "POST",
        data: item,
        url: '<?php echo base_url()?>keuangan/homeunitbendahara/get_barang_gudang',
        success: function (data) {
          console.log(data);//return false;
          $('#tbodybarangpermintaan').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              $('#tbodybarangpermintaan').append(
                '<tr>'+
                  '<td>'+data[i]['nama']+'</td>'+
                  '<td>'+data[i]['satuan']+'</td>'+
                  '<td>'+data[i]['nama_merk']+'</td>'+
                  '<td align="center">'+data[i]['tahun_pengadaan']+'</td>'+
                  '<td align="right">'+data[i]['stok_gudang']+'</td>'+
                  '<td style="text-align:center"><a href="#" class="addnewpermintaanbarang"><i class="glyphicon glyphicon-check"></i></a></td>'+
                  '<td style="display:none">'+data[i]['barang_stok_id']+'</td>'+
                  '<td style="display:none">'+data[i]['barang_id']+'</td>'+
                '</tr>'
              )
            };
          }else{
            $('#tbodybarangpermintaan').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
          } 
        },
        error: function (data) {
          console.log(data);
        }
      })
    })

    $('#tbodybarangpermintaan').on('click', 'tr td a.addnewpermintaanbarang',function (e) {
      e.preventDefault();
      var cols = [];
          $(this).closest('tr').find('td').each(function (colIndex, c) {
              cols.push(c.textContent);
          });

          $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
      $('#addinputmintabarang').append(
        '<tr><td>'+cols[0]+'</td>'+//nama
        '<td>'+cols[1]+'</td>'+  //satuan
        '<td>'+cols[2]+'</td>'+ //merk
        '<td>'+cols[3]+'</td>'+ //tahun pengadaan
        '<td>'+cols[4]+'</td>'+ //stok gudang
        '<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah minta
        '<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
        '<td style="display:none">'+cols[6]+'</td>'+ //barang_stok_id
        '<td style="display:none">'+cols[7]+'</td></tr>' //barang_id
      )
    })

    $('#permintaanbarangunit').submit(function (e) {
      e.preventDefault();
      var item = {};
      item['no_permintaanbarang'] = $('#nomorpermintaanbarang').val();
      item['tanggal_request'] = $('#tglpermintaanbarang').val();
      item['keterangan_request'] = $('#keteranganpermintaanbarang').val();

      var data = [];
      $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
        $('#addinputmintabarang').find('tr').each(function (rowIndex, r) {
            var cols = [];
            $(this).find('td').each(function (colIndex, c) {
                cols.push(c.textContent);
            });
            $(this).find('td input[type=number]').each(function (colIndex, c) {
                cols.push(c.value);
            });
            data.push(cols);
        });
      if(data.length == 0){
        $('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
        myAlert('detail tidak ada, isi data dengan benar');
        return false;
      }

        item['data'] = data;

        $.ajax({
        type: "POST",
        data: item,
        url: '<?php echo base_url()?>keuangan/homeunitbendahara/submit_permintaan_barangunit',
        success: function (data) {
          console.log(data);
          if (data['error'] == 'n'){
            $('#addinputmintabarang').empty();
            $('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
            $('#nomorpermintaanbarang').val('');
            $('#keteranganpermintaanbarang').val('');
          }
          myAlert(data['message']);
        },
        error: function (data) {
          console.log(data);
        }
      })
    })

    var this_setuju;
    $('#tbodyriwayatpersetujuan').on('click', 'tr td .cekdetailpersetujuan', function (e) {
      e.preventDefault();
      this_setuju = $(this);
      $('#nmrLog').text($(this).closest('tr').find('td').eq(1).text());
      $('#tgLog').text(format_date4($(this).closest('tr').find('td .tgl').val()));
      $('#txtket').text($(this).closest('tr').find('td').eq(3).text());
      var id = $(this).closest('tr').find('td .detail_persetujuan_id').val();
      $('#detail_persetujuan_idconfirm').val(id);

      $.ajax({
          type: 'POST',
          url: "<?php echo base_url()?>logistik/homegudangbarang/get_detailpersetujuan/" + id,
          success: function (data) {
            console.log(data);
            if (data.length > 0) {
              
              $('#tbodydetailriwayatpersetujuan').empty();
              for (var i = 0; i < data.length; i++) {
                $('#tbodydetailriwayatpersetujuan').append(
                  '<tr>'+
                  '<td>'+data[i]['nama']+'</td>'+
                  '<td style="text-align:left">'+data[i]['satuan']+'</td>'+
                  '<td style="text-align:left">'+data[i]['nama_merk']+'</td>'+
                  '<td style="text-align:right">'+data[i]['stok']+'</td>'+
                  '<td style="text-align:right">'+data[i]['jumlah_request']+'</td>'+
                  '<td style="text-align:right;width:100px;">'+data[i]['jumlah_approved']+'</td>'+
                  '<td style="text-align:right">'+data[i]['harga']+'</td>'+
                '</tr>'
                );
              };
            };
          },
          error: function (data) {
            console.log(data);
          }
        })
    })
    /*logistik unit*/

  }) //end of document ready
</script>
