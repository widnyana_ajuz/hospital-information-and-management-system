<?php
$sekarang = str_replace('-', "_", date('d-m-Y'));
$title = 'Rekap_Pendapatan_Per_Jenis_Penerimaan_'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Rekap Pendapatan Per Jenis Penerimaan</title>
  <style>
     .grup-pertanyaan {
     text-align: center;
     border: solid 1px #000;
    }
     table td {
       border-bottom: solid 0.5px #000;
       font-size: 12pt;
       vertical-align:middle;
       line-height:40px;
       width: 300px;
     }
     .keterangan_pertanyaan {
       font-size: 8pt;
     }
     table .nama_matkul{
       text-transform:capitalize;
     }
     table {
       width: 100%;
     }
     table .header {
       background-color: yellow;
       border-top: solid 0.5px #000;
       border-left: solid 0.5px #000;
       border-right: solid 0.5px #000;
       border-bottom: solid 0.2px #000;
       font-weight: bold;
       vertical-align:middle;
       line-height:40px;
     }
     table .body {
       border-top: solid 0.5px #000;
       border-left: solid 0.5px #000;
       border-right: solid 0.5px #000;
       border-bottom: solid 0.2px #000;
     }
     .center {
       text-align:center;
     }
     .right {
       text-align:right;
     }
     .italic {
       font-style:italic;
     }
   </style>
</head>

<body>
  <table>
    <tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>REKAP PENDAPATAN PER JENIS PENERIMAAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
  </table>

  <br/>
	<strong>Laporan per tanggal </strong>
	<?php echo date("d F Y", strtotime($awal)); ?> <strong>sampai tanggal</strong> <?php echo date("d F Y", strtotime($akhir)); ?>
	<br/>
	<br/>

  <div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th width="3%" style="text-align:center">No.</th>
					<th style="text-align:center">Jenis Penerimaan</th>
          <?php
            $secs = strtotime($akhir) - strtotime($awal);
            $days = $secs/86400;
            for ($i=$days; $i >= 0; $i--) {
              $date =  date('Y-m-d',strtotime($akhir."-".$i." day"));
              echo '<th>'.date("d F Y", strtotime($date)).'</th>';
            }
          ?>
				</tr>
			</thead>
			<tbody>
        <?php
          $i = 0;
          foreach ($rekap_pendapatan as $value) {
            echo '<tr><td>'.(++$i).'</td>';
            echo '<td>'.strtoupper($value['jenis']).'</td>';
            $var = $value['jumlah'];
            foreach ($var as $key) {
              echo '<td class="text-right">'.number_format($key['dapat'], 0).'</td>';
            }
            echo '</tr>';
          }
        ?>
			</tbody>
		</table>
	</div>
</body>
</html>
