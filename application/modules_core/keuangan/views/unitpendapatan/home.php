<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>keuangan/homeunitpendapatan"><?php echo strtoupper('Unit Pendapatan') ?></a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Input Pendapatan</a>
	</li>
</div>
<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	 	<li class="active"><a href="#list" class="cl" data-toggle="tab">Input Pendapatan</a></li>
		<li><a href="#rba" class="cl" data-toggle="tab">Rekap Pendapatan</a></li>
		<li><a href="#jaspel" class="cl" data-toggle="tab">Jasa Pelayanan</a></li>
		<li><a href="#logistik" class="cl" data-toggle="tab">Logistik</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
		<div class="modal fade" id="viewDok" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:100px">
			<div class="modal-dialog">
				<div class="modal-content" style="width:600px">
					<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h3 class="modal-title" id="myModalLabel">Detail</h3>
        	</div>
        	<div class="modal-body">
        		<form class="form-horizontal" role="form">
		      		<div class="informasi" id="info1">
								<div class="form-group">
									<label class="control-label col-md-3">Nomor</label>
									<div class="col-md-4">
										<input type="text" class="form-control" id="nmrLog" name="nmrLog">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Tanggal</label>
									<div class="col-md-4">
										<input type="text" class="form-control" id="tgLog" name="tgLog" data-provide="datepicker">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Keterangan</label>
									<div class="col-md-4">
										<textarea class="form-control"></textarea>
									</div>
								</div>
							</div>
						</form>
						<div class="tabelinformasi">
							<div class="portlet box red">
								<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover table-responsive">
										<thead>
											<tr class="info">
												<th >Nama Obat/Barang</th>
												<th>Penyedia</th>
												<th>Quantity</th>
												<th>Satuan</th>
												<th>Harga</th>
												<th>Total</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>122</td>
												<td>Arbet</td>
												<td>P</td>
												<td>1212121</td>
												<td>Belum Diproses</td>
												<td>asa</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
        	</div>
      		<div class="modal-footer">
 			      <button type="button" class="btn btn-success " >Proses</button>
 			  		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			  	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="searchParamedis" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
	    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	    			<h3 class="modal-title" id="myModalLabel">Pilih Paramedis</h3>
	    		</div>
	    		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-5">
								<input type="text" class="form-control" name="paramkey" id="paramkey" placeholder="Nama Paramedis"/>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-info">Cari</button>
							</div>
						</div>
						<br>
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelsearchparamedis">
								<thead>
									<tr class="info">
										<th>Nama Paramedis</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Jems</td>
										<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
									</tr>
									<tr>
										<td>Putu</td>
										<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
									</tr>
								</tbody>
							</table>
						</div>
	    		</div>
	  			<div class="modal-footer">
		     		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
		    	</div>
				</div>
			</div>
		</div>

		<div class="tab-pane active" id="list">
			<div class="dropdown" id="ddtambah" style="margin-left:10px;width:98.5%">
		      	<div id="titleInformasi">Tambah Pendapatan Pelayanan dan Non Pelayanan</div>
		      	<div  class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
	      	</div>
      		<br>
			<form class="form-horizontal" role="form" id="frmpendapatan" method="post">
				<div class="informasi">
					<div class="form-group">
						<label class="control-label col-md-2">Tanggal</label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" id="add_tanggal" style="cursor:pointer;background-color:white" class="form-control" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Kategori Pendapatan</label>
						<div class="col-md-3">
							<select class="form-control" id="add_kategori" required>
								<option value="" selected>PILIH</option>
								<option value="PENDAPATAN PELAYANAN">PENDAPATAN PELAYANAN</option>
								<option value="PENDAPATAN NON PELAYANAN">PENDAPATAN NON PELAYANAN</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Jenis Pendapatan</label>
						<div class="col-md-3">
							<select class="form-control" id="add_jenis1">
								<option value="" selected>PILIH</option>
								<option value="INSTALASI UTD">INSTALASI UTD</option>
								<option value="AMBULANCE">AMBULANCE</option>
								<option value="KLAIM BPJS">KLAIM BPJS</option>
								<option value="KLAIM JAMKESDA">KLAIM JAMKESDA</option>
								<option value="PERUSAHAAN">PERUSAHAAN</option>
							</select>
							<select class="form-control" id="add_jenis2">
								<option value="" selected>PILIH</option>
								<option value="SEWA AULA">SEWA AULA</option>
								<option value="DIKLAT">DIKLAT</option>
								<option value="STUDI BANDING">STUDI BANDING</option>
								<option value="PARKIR">PARKIR</option>
								<option value="SEWA WARUNG">SEWA WARUNG</option>
								<option value="SEWA RUANG ASKES">SEWA RUANG ASKES</option>
								<option value="SEWA RUANG BAPEL">SEWA RUANG BAPEL</option>
								<option value="SEWA ATM">SEWA ATM</option>
								<option value="BANTUAN LISTRIK ASKES">BANTUAN LISTRIK ASKES</option>
								<option value="BUNGA DEPOSITO">BUNGA DEPOSITO</option>
								<option value="JASA GIRO">JASA GIRO</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Instansi Pembayar</label>
						<div class="col-md-3">
							<input type="text" id="add_instansi" class="form-control" placeholder="Instansi Pembayar" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Jumlah Pendapatan</label>
						<div class="col-md-3">
							<div class="input-group">
								<div class="input-group-addon">Rp</div>
								<input type="text" class="form-control text-right" id="add_jumlah" placeholder="0">
							</div>
						</div>
					</div>
				</div>
				<div class="tabelinformasi">
				 	<br>
				 	<div class="portlet box red">
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<br>

							<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
							<div style="margin-left:80%">
								<span style="padding:0px 10px 0px 10px;">
									<button type="reset" class="btn btn-warning">RESET</button>&nbsp;
									<button type="submit" class="btn btn-success">SIMPAN</button>
								</span>
							</div>
							<br>
						</div>
					</div>
				</div>
			</form>

			<div class="dropdown" id="ddriwayat" style="margin-left:10px;width:98.5%">
	        	<div id="titleInformasi">Riwayat Pendapatan Pelayanan dan Non Pelayanan</div>
          		<div  class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
        	</div>
        	<br>
        	<div id="lddriwayat">
				<form method="POST" id="frmsearch">
					<div class="search">
						<label class="control-label col-md-3"><i class="fa fa-search">&nbsp;&nbsp;</i>Pencarian <span class="required" style="color : red">* </span></label>
						<div class="col-md-4">
							<input type="text" class="form-control" id="katakuncipendapatan" placeholder="Masukkan Kata Kunci">
						</div>
						<button type="submit" class="btn btn-info">Cari</button>
					</div>
				</form>

				<hr class="garis">	
				
				<form class="form-horizontal" method="post" action="<?php echo base_url()?>keuangan/homeunitpendapatan/print_pendapatan">
					<label class=" col-md-1" style="margin-right:-60px; padding-top:7px;"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by</label>
					<div class="col-md-3" style="margin-left:30px;">
						<div class="input-daterange input-group" id="datepicker">
						 <input type="text" style="cursor:pointer;background-color:white" id="tanggal1" name="tanggal1" class="form-control" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
						 <div class="input-group-addon">to</div>
						 <input type="text" style="cursor:pointer;background-color:white" id="tanggal2" name="tanggal2" class="form-control" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
					 	</div>
				 	</div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Tanggal</th>
										<th>Kategori Pendapatan</th>
										<th>Jenis Pendapatan</th>
										<th>Instansi Pembayar</th>
										<th>Jumlah Pendapatan</th>
										<th width="100">Action</th>
									</tr>
								</thead>
								<tbody id="tbodypendapatan">
									<?php
										if (isset($pendapatan)) {
											if (!empty($pendapatan)) {
												$i = 0;
												foreach ($pendapatan as $value) {
													$tgl = DateTime::createFromFormat('Y-m-d', $value['tanggal']);
													echo '<tr>
														<td>'.(++$i).'</td>
														<td>'.$tgl->format('d F Y').'</td>
														<td>'.$value['kategori_pendapatan'].'</td>
														<td>'.$value['jenis_pendapatan'].'</td>
														<td>'.$value['instansi_pembayar'].'</td>
														<td class="text-right">'.number_format($value['jumlah'], 0, '', '.').'</td>
														<td style="text-align:center">
															<input type="hidden" class="val_id" value='.$value['id'].'>
															<a href="#" ><i class="glyphicon glyphicon-trash delete" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
														</td>
													</tr>';
												}
											}
										}
									?>
								</tbody>
							</table>
						</div>
						<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
					</div>
				</form>
				
			</div>
    	</div>

	  	<div class="tab-pane" id="rba">
		  	<div class="informasi" style="margin-left:30px;">
	  			<div id="titleInformasi" style="margin-bottom:-30px;">Pendapatan Per Jenis Penerimaan</div>
	      		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;padding:30px;" method="post" action="<?php echo base_url()?>keuangan/homeunitpendapatan/print_pendapatan_per_jenis">
					<div class="tabelinformasi">
			    	    <div class="form-group" >
		    				<label class="control-label col-md-1">Tanggal</label>
		        			<div class="col-md-3">
								<div class="input-daterange input-group" id="datepicker">
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_jenis1"  data-date-format="dd/mm/yyyy" data-provide="datepicker"  value="<?php echo date("d/m/Y");?>" readonly />
								    <span class="input-group-addon">to</span>
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_jenis2" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
								</div>
							</div>

							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			      					<button class="btn btn-info">Simpan ke Excel(.xls)</button>
			      				</div>
		      				</div>
		      			</div>
	      			</div>
				</form>
			</div>

			<div class="informasi" style="margin-left:30px;">
	    		<div id="titleInformasi" style="margin-bottom:-30px;">Pendapatan Per Unit</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;padding:30px;" role="form" method="post" action="<?php echo base_url()?>keuangan/homeunitpendapatan/print_rekap_unit">
			        <div class="tabelinformasi" >
				        <div class="form-group">
		        			<label class="control-label col-md-1"> Tanggal</label>
		        			<div class="col-md-3">
								<div class="input-daterange input-group" id="datepicker">
								   	<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_unit1" id="tanggal_unit1"  data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
								   	<span class="input-group-addon">to</span>
								   	<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_unit2"  id="tanggal_unit2" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>"  readonly/>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-1">Unit</label>
							<div class="col-md-3">
								<select class="form-control" name="select_jenis" required>
									<option value="" selected>Pilih</option>
									<?php
										foreach ($units as $value) {
											echo '<option value='.$value['dept_id'].'>'.$value['nama_dept'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
		        	</div>
				</form>
			</div>

			<div class="informasi" style="margin-left:30px;">
	    		<div id="titleInformasi" style="margin-bottom:-30px;">Pendapatan Per JS-JP-BAKHP</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;padding:30px;" role="form" method="post" action="<?php echo base_url()?>keuangan/homeunitpendapatan/print_rekap_jsjpbakhp">
			       	<div class="tabelinformasi" >
				       	<div class="form-group">
		        			<label class="control-label col-md-1"> Tanggal</label>
		        			<div class="col-md-3">
								<div class="input-daterange input-group" id="datepicker">
						    		<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_rekap1"  data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
						    		<span class="input-group-addon">to</span>
						    		<input type="text" style="cursor:pointer;background-color:white" class="form-control" name="tanggal_rekap2" data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" readonly />
								</div>
							</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-1">Unit</label>
		        			<div class="col-md-3">
		        				<select class="form-control" name="select_jenis" required>
		        					<option value="" selected>Pilih</option>
									<?php
										foreach ($units2 as $value) {
											echo '<option value='.$value['dept_id'].'>'.$value['nama_dept'].'</option>';
										}
									?>
		        				</select>
		        			</div>
		        			<div class="pull-right" style="margin-right:20px">
			       				<div class="col-md-3">
			      					<button class="btn btn-info">Simpan ke Excel(.xls)</button>
			       				</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<br><br>
		</div>

		<div class="tab-pane" id="jaspel">
			<div class="dropdown" id="rjp">
 	  			<div id="titleInformasi" >Rekap Jasa Pelayanan </div>
	        	<div id="" class="btnBawah floatright" style="margin-top:-25px;"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
    		</div>
    		<br>

    		<form id="irjp" class=" form-horizontal" method="post" action="<?php echo base_url() ?>keuangan/homeunitpendapatan/excel_jasa_pelayanan">
    			<div class="informasi">
					<div class="form-group">
						<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Periode :</label>
						<div class="col-md-3" style="margin-left:-15px">
							<div class="input-daterange input-group" id="datepicker">
							    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="start" data-date-format="dd/mm/yyyy" id="mulai_jaspel" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
							    <span class="input-group-addon">to</span>
							    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" id="akhir_jaspel" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
							</div>
						</div>
					</div>

	    			<div class="form-group">
						<label class="control-label col-md-2"> <i class="glyphicon glyphicon-filter"></i>&nbsp;Nama Paramedis </label>
						<div class="input-group col-md-2">
							<input type="text" class="form-control" id="nama_petugas_jaspel" autocomplete="off" spellcheck="false" placeholder="cari petugas">
							<input type="hidden" name="id_petugas_jaspel" id="id_petugas_jaspel">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2"> <i class="glyphicon glyphicon-filter"></i>&nbsp;Cara Bayar</label>
						<div class="input-group col-md-2">
							<select class="form-control select" name="cara_bayar" id="carabayar_jaspel">
								<option value="" selected>-- SEMUA --</option>
								<option value="Umum">Umum</option>
								<option value="BPJS">BPJS</option>
								<option value="Jamkesmas" >Jamkesmas</option>
								<option value="Asuransi">Asuransi</option>
								<option value="Kontrak">Kontrak</option>
								<option value="Gratis">Gratis</option>
								<option value="Lain-lain">Lain-lain</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp; Unit </label>
						<div class="input-group col-md-2">
							<input type="text" class="form-control" id="nama_unit_jaspel" autocomplete="off" spellcheck="false" placeholder="cari unit">
							<input type="hidden" name="id_unit_jaspel" id="id_unit_jaspel">
						</div>
					</div>
	    		</div>
	    		<br>
				<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
				<div style="margin-left:90%">
					<button type="button" id="btn_filter_jaspel" class="btn btn-warning">FILTER</button>
				</div>
				<br>

    			<div class="portlet-body" style="margin: 10px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover tableDTUtama" id="tabelJpPoliinap">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal</th>
								<th>Tindakan</th>
								<th>Unit</th>
								<th>Cara Bayar</th>
								<th>Paramedis</th>
								<th>Paramedis Lain</th>
								<th>Jasa Pelayanan</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<?php
								if (isset($jaspel) && !empty($jaspel)) {
									$i = 0;
									foreach ($jaspel as $value) {
										$tgl = DateTime::createFromFormat('Y-m-d', $value['tanggal']);
										echo '<tr>
												<td style="text-align:center">'.(++$i).'</td>
												<td style="text-align:center">'.$tgl->format('d F Y').'</td>
												<td>'.$value['nama_tindakan'].'</td>
												<td>'.$value['nama_dept'].'</td>
												<td>'.$value['cara_bayar'].'</td>
												<td>'.$value['nama_petugas'].'</td>
												<td>'.$value['paramedis_lain'].'</td>
												<td style="text-align:right">'.$value['jp'].'</td>
											</tr>';
									}
								}
							?>
						</tbody>
					</table>
				</div>
								<!-- <td style="text-align:center">
									<a href="#" id="btneditjp"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
									<a href="#" id="btnsavejp"><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Simpan"></i></a>
								</td> -->
				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
			</form>

			<div class="dropdown" id="rjpko">
	 	  		<div id="titleInformasi" >Rekap Jasa Pelayanan Kamar Operasi </div>
	        	<div id="" class="btnBawah floatright" style="margin-top:-25px;"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
	    	</div>
	    	<br>

		    <form id="irjpko" method="post" action="<?php echo base_url() ?>keuangan/homeunitpendapatan/excel_kamar_operasi">
	    		<div class="form-horizontal">
					<div class="informasi">
						<div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Periode :</label>
							<div class="col-md-3" style="margin-left:-15px">
								<div class="input-daterange input-group" id="datepicker">
						    	<input type="text" style="cursor:pointer;background-color:white" class="form-control" id="ko_start" name="start" data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
						    	<span class="input-group-addon">to</span>
						    	<input type="text" style="cursor:pointer;background-color:white" class="form-control" id="ko_end" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2"> <i class="glyphicon glyphicon-filter"></i>&nbsp;Cara Bayar</label>
							<div class="input-group col-md-2">
								<select class="form-control select" name="carabayar" id="ko_carabayar">
									<option value="" selected>-- SEMUA --</option>
									<option value="Umum">Umum</option>
									<option value="BPJS">BPJS</option>
									<option value="Jamkesmas" >Jamkesmas</option>
									<option value="Asuransi">Asuransi</option>
									<option value="Kontrak">Kontrak</option>
									<option value="Gratis">Gratis</option>
									<option value="Lain-lain">Lain-lain</option>
								</select>
							</div>
						</div>

			    		<div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp; Nama Paramedis </label>
							<div class="input-group col-md-2">
								<input type="text"  class="form-control" autocomplete="off" spellcheck="false" placeholder="Search Paramedis" id="ko_paramedis">
								<input type="hidden" name="ko_paramedis_id" id="ko_paramedis_id">
							</div>
						</div>
						<br>
					</div>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:90%">
						<span class="customSpan">
							<button type="button" id="btn_filter_kamar_operasi" class="btn btn-warning">FILTER</button>
						</span>
					</div>
					<br>
				</div>
				<div class="portlet-body" style="margin: 10px 10px 0px 10px;">
					<table class="table table-striped table-bordered table-hover tableDTUtamaScroll" id="tabeljaspeloperasi">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal</th>
								<th>Cara Bayar</th>
								<th>Tindakan</th>
								<th>Nama Pasien</th>
								<th>JP</th>
								<th>Dokter Operator</th>
								<th>Jasa Dokter Operator</th>
								<th>Dokter Anastesi</th>
								<th>Jasa Dokter Anastesi</th>
								<th>Dokter Anak</th>
								<th>Jasa Dokter Anak</th>
								<th>Jasa Asisten</th>
								<th>JS</th>
								<th>BAKHP</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<?php
								if (!empty($jp_kamar_operasi)) {
									$i = 0;
									foreach ($jp_kamar_operasi as $key) {
										//$tgl = DateTime::createFromFormat('Y-m-d H:i:s', $key['waktu_selesai']);
										echo '<tr>
												<td style="text-align:center" width="20">'.(++$i).'</td>
												<td style="text-align:center">'.$key['waktu_selesai']/*$tgl->format('d F Y H:i:s')*/.'</td>
												<td>'.$key['cara_bayar'].'</td>
												<td>'.$key['nama_tindakan'].'</td>
												<td>'.$key['nama'].'</td>
												<td style="text-align:right">'.$key['jp'].'</td>
												<td>'.$key['d_bedah'].'</td>
												<td align="right">'.$key['jp'].'</td>
												<td >'.$key['d_anestesi'].'</td>
												<td align="right">0</td>
												<td >'.$key['d_anak'].'</td>
												<td align="right">0</td>
												<td align="right">0</td>
												<td style="text-align:right">'.$key['js'].'</td>
												<td style="text-align:right">'.$key['bakhp'].'</td>
											</tr>';
									}
								}
							?>
						</tbody>
					</table>
					<!-- <input type="text" name="jasadokteroperator" style="text-align:right" id="jasadokteroperator" class="input-sm form-control jasa" value="100">
						<td style="text-align:center">
						<a href="#" id="btneditko"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
						<a href="#" id="btnsaveko"><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Simpan"></i></a>
					</td> -->
				</div>

				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
			</form>

			<div class="dropdown" id="rjpa">
	 	  		<div id="titleInformasi" >Rekap Jasa Resep Apotik </div>
	      		<div id="" class="btnBawah floatright" style="margin-top:-25px;"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
	    	</div>
	    	<br>

	    	<form method="post" id="irjpa" action="<?php echo base_url() ?>keuangan/homeunitpendapatan/excel_jasa_resep">
		    	<div class="form-horizontal">
				    <div class="informasi">
				    	<div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Periode Penjualan :</label>
							<div class="col-md-3" style="margin-left:-15px">
								<div class="input-daterange input-group" id="datepicker">
						    	<input type="text" style="cursor:pointer;background-color:white" class="form-control" id="ap_start" name="start" data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
						    	<span class="input-group-addon">to</span>
						    	<input type="text" style="cursor:pointer;background-color:white" class="form-control" id="ap_end" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2"> <i class="glyphicon glyphicon-filter"></i>&nbsp;Cara Bayar</label>
							<div class="input-group col-md-2">
								<select class="form-control select" name="cara_bayar" id="ap_carabayar">
									<option value="" selected>-- SEMUA --</option>
									<option value="Umum">Umum</option>
									<option value="BPJS">BPJS</option>
									<option value="Jamkesmas" >Jamkesmas</option>
									<option value="Asuransi">Asuransi</option>
									<option value="Kontrak">Kontrak</option>
									<option value="Gratis">Gratis</option>
									<option value="Lain-lain">Lain-lain</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp; Unit </label>
							<div class="input-group col-md-2">
								<input type="text" class="form-control" autocomplete="off" spellcheck="false" name="unit" id="ap_unit" placeholder="Search unit">
								<input type="hidden" name="ap_unit_id" id="ap_unit_id">
							</div>
						</div>
			    		<div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp; Nama Paramedis </label>
							<div class="input-group col-md-2">
								<input type="text"  class="form-control" autocomplete="off" spellcheck="false" placeholder="Search Paramedis" id="ap_paramedis">
								<input type="hidden" name="ap_paramedis_id" id="ap_paramedis_id">
							</div>
						</div>
						<br>
					</div>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:90%">
						<span class="customSpan">
							<!-- <button type="reset" class="btn btn-warning">RESET</button> -->
							<button type="button" id="ap_btn_filter_jaspel" class="btn btn-warning">FILTER</button>
						</span>
					</div>
					<br>
	    		</div>

	    		<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover tableDTUtama" id="tblPerhitunganResep">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal Penjualan</th>
								<th>Unit</th>
								<th>Cara Bayar</th>
								<th>No. Resep</th>
								<th>Nama Pasien</th>
								<th>Dokter</th>
								<th>Manajemen</th>
								<th>Fee Dokter</th>
								<th>Remunisasi</th>
								<th>Farmasi</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<?php
								if (!empty($jassep_apotek)) {
									$i = 0;
									foreach ($jassep_apotek as $value) {

										echo '<tr>
												<td width="20">'.(++$i).'</td>
												<td>'.DateTime::createFromFormat('Y-m-d H:i:s',$value['waktu_penjualan'])->format('d F Y H:i:s').'</td>
												<td>'.$value['dept_resep'].'</td>
												<td>'.$value['cara_bayar'].'</td>
												<td>'.$value['resep_id'].'</td>
												<td>'.$value['nama'].'</td>
												<td>'.$value['nama_petugas'].'</td>
												<td align="right">'.$value['management'].'</td>
												<td align="right">'.$value['jasadokter'].'</td>
												<td align="right">'.$value['remunisasi'].'</td>
												<td align="right">'.$value['apotek'].'</td>
											</tr>';
									}
								}
							?>
						</tbody>
					</table>
				</div>
				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
			</form>
        </div>

        <div class="tab-pane" id="logistik">
        	<div class="modal fade" id="modalbarang" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
	        			</div>
	        			<div class="modal-body">

		        			<div class="form-group">
		        				<form method="post" class="form-horizontal" role="form" id="formmintabarang">
									<div class="form-group">
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncimintabarang" placeholder="Nama barang"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>
									</div>
								</form>
								<div style="margin-right:10px;margin-left:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%">
										<thead>
											<tr class="info">
												<th>Nama Barang</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Tahun Pengadaan</th>
												<th>Stok Gudang</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodybarangpermintaan">
											<tr>
												<td colspan="6" style="text-align:center">Cari data Barang</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
	       	<div class="dropdown" id="bwinlogistik">
	            <div id="titleInformasi">Inventori</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
            </div>
            <div id="ibwinlogistik" class="tutupBiru">

				<div class="form-group" >
					<div class="portlet-body" style="margin: 30px 10px 20px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tblinventorigudangunit">
							<thead>
								<tr class="info" >
									<th width="20">No.</th>
									<th > Nama Barang </th>
									<th > Merek </th>
									<th > Harga </th>
									<th > Stok </th>
									<th > Satuan </th>
									<th > Tahun Pengadaan</th>
									<th > Sumber Dana</th>
									<th width="100"> Action </th>

								</tr>
							</thead>
							<tbody id="tbodyinventoribarang">
								<?php
									if (isset($inventoribarang)) {
										if (!empty($inventoribarang)) {
											$i = 1;
											foreach ($inventoribarang as $value) {
												echo '<tr>
														<td>'.($i++).'</td>
														<td>'.$value['nama'].'</td>
														<td>'.$value['nama_merk'].'</td>
														<td>'.$value['harga'].'</td>
														<td>'.$value['stok'].'</td>
														<td>'.$value['satuan'].'</td>
														<td>'.$value['tahun_pengadaan'].'</td>
														<td>'.$value['sumber_dana'].'</td>
														<td style="text-align:center">
															<input type="hidden" class="barang_detail_inout" value="'.$value['barang_detail_id'].'">
															<a href="#inoutbar" data-toggle="modal" class="edBarang" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
															<a href="#edInvenBerBar" data-toggle="modal" class="detailinvenbarang"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>
														</td>
													</tr>';
											}
										}
									}
								?>

							</tbody>
						</table>
					</div>
					<button class="btn btn-info" style="margin:-130px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
	        	</div>
	        </div>
			<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<form class="form-horizontal" role="form" style="margin-left:30px;" id="forminoutbarang">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<div class="form-group">
			        				<label class="control-label col-md-3" >Tanggal </label>
									<div class="col-md-6" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" id="tanggalinout" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
									</div>
								</div>

								</div>
								<div class="form-group">
									<label class="control-label col-md-3" >In / Out </label>
									<div class="col-md-6">
						         		<select class="form-control select" name="io" id="io">
											<option value="IN" selected>IN</option>
											<option value="OUT">OUT</option>
										</select>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Jumlah in/out</label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="jmlInOut" name="jmlInOut" placeholder="Jumlah">
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Sisa Stok </label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="sisaInOut" name="sisaInOut" placeholder="Sisa Stok" readonly>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Keterangan </label>
									<div class="col-md-6" >
										<textarea class="form-control" id="keteranganIO" placeholder="Keterangan"></textarea>
									</div>
								</div>
		        			</div>
		        			<div class="modal-footer">
		        				<input type="hidden" id="id_barang_inoutprocess">
		 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			       		<button type="submit" class="btn btn-success">Simpan</button>
					      	</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<form class="form-horizontal" role="form">
				            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th> Waktu </th>
											<th> IN / OUT </th>
											<th> Jumlah </th>
											<th> Keterangan </th>
										</tr>
									</thead>
									<tbody id="tbodydetailbrginventori">
										<tr>
											<td colspan="4" style="text-align:center">Tidak ada detail in-out</td>
										</tr>

									</tbody>
								</table>
							</form>

	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
			<br>

			<div class="dropdown" id="bwpermintaanlogistik" style="margin-left:10px;width:98.5%">
	            <div id="titleInformasi">Permintaan Logistik</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
            </div>
            <div id="ibwpermintaanlogistik" class="tutupBiru">
            	<form class="form-horizontal" role="form" method="post" id="permintaanbarangunit">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="nomorpermintaanbarang" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanbarang" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">
								<textarea class="form-control" id="keteranganpermintaanbarang" name="ketObatFarBers"></textarea>
							</div>
	        			</div>
	        		</div>
					<a href="#modalbarang" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Barang</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<th> Nama Barang </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Tahun Pengadaan </th>
										<th> Stok Gudang </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>
									</tr>
								</thead>
								<tbody  id="addinputmintabarang">
									<?php echo '<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>'; ?>
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<button class="btn btn-warning" type="reset" id="batalpermintaanfarmasi">RESET</button>&nbsp;
							<button class="btn btn-success" type="submit">SIMPAN</button>
						</div>
						<br>
					</div>
				</form>
			</div>
			<br>
	    </div>
    </div>
	</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#bwpermintaanlogistik").click(function(){
			$("#ibwpermintaanlogistik").slideToggle();
		});

		$("#bwinlogistik").click(function(){
			$("#ibwinlogistik").slideToggle();
		});

		$("#irjp").hide();
		$("#irjpko").hide();
		$("#irjpa").hide();
		$("#rjp").click(function(){
			$("#irjp").slideToggle();
		});

		$("#rjpko").click(function(){
			$("#irjpko").slideToggle();
		});

		$("#rjpa").click(function(){
			$("#irjpa").slideToggle();
		});

		$("#ddtambah").click(function(){
			$("#frmpendapatan").slideToggle();
		});

		$("#ddriwayat").click(function(){
			$("#lddriwayat").slideToggle();
		});

	});
</script>
