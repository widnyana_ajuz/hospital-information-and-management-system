<script type="text/javascript">
	$(document).ready(function () {
		//pendapatan
		$('#add_jumlah').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0', vMin:'0'});

    $('#add_jenis2').hide();

    $('#add_jenis1').click(function(e){
      if ($('#add_kategori').val() == '') {
        alert('Pilih kategori pendapatan terlebih dulu');
      }
    })

    $('#add_kategori').change(function(e){
      e.preventDefault();
      if ($(this).val() == 'PENDAPATAN PELAYANAN') {
        $('#add_jenis1').show();
        $('#add_jenis2').hide();
      }else if ($(this).val() == 'PENDAPATAN NON PELAYANAN') {
        $('#add_jenis1').hide();
        $('#add_jenis2').show();
      }else {
        $('#add_jenis1').show();
        $('#add_jenis2').hide();
        $('#add_jenis1').val('');
      }
    })

    $('#tanggal1').on('change', function(e){
      e.preventDefault();
      var tanggal = {}
      tanggal['tgl1'] = $(this).val();
      tanggal['tgl2'] = $('#tanggal2').val();

      $.ajax({
        type: "POST",
        data: tanggal,
        url: "<?php echo base_url()?>keuangan/homeunitpendapatan/get_pendapatan_by_date",
        success: function(data){
          console.log(data);
          $('#tbodypendapatan').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              var c = i+1;
              var d = new Date(data[i]['tanggal']);
              var month_names = new Array ( );
              month_names[month_names.length] = "January";
              month_names[month_names.length] = "February";
              month_names[month_names.length] = "March";
              month_names[month_names.length] = "April";
              month_names[month_names.length] = "May";
              month_names[month_names.length] = "June";
              month_names[month_names.length] = "July";
              month_names[month_names.length] = "August";
              month_names[month_names.length] = "September";
              month_names[month_names.length] = "October";
              month_names[month_names.length] = "November";
              month_names[month_names.length] = "December";
              var curr_date = d.getDate();
              var curr_month = d.getMonth(); //Months are zero based
              var curr_year = d.getFullYear();
              var tanggalbaru = curr_date + " " + month_names[curr_month] + " " + curr_year;

              $('#tbodypendapatan').append(
                '<tr>'+
                  '<td>'+c+'</td>'+
                  '<td>'+tanggalbaru+'</td>'+
                  '<td>'+data[i]['kategori_pendapatan']+'</td>'+
                  '<td>'+data[i]['jenis_pendapatan']+'</td>'+
                  '<td>'+data[i]['instansi_pembayar']+'</td>'+
                  '<td class="text-right">'+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td style="text-align:center"><a href="#" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>'+
                '</tr>'
              );
            }
          }else {
            $('#tbodypendapatan').append('<tr><td colspan="7">Tidak ada data</td></tr>');
          }
        },
        error: function(data){
          console.log(data);;
        }
      })
    })

    $('#tanggal2').on('change', function(e){
      e.preventDefault();
      var tanggal = {}
      tanggal['tgl1'] = $('#tanggal1').val();
      tanggal['tgl2'] = $(this).val();

      $.ajax({
        type: "POST",
        data: tanggal,
        url: "<?php echo base_url()?>keuangan/homeunitpendapatan/get_pendapatan_by_date",
        success: function(data){
          console.log(data);
          $('#tbodypendapatan').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              var c = i+1;
              var d = new Date(data[i]['tanggal']);
              var month_names = new Array ( );
              month_names[month_names.length] = "January";
              month_names[month_names.length] = "February";
              month_names[month_names.length] = "March";
              month_names[month_names.length] = "April";
              month_names[month_names.length] = "May";
              month_names[month_names.length] = "June";
              month_names[month_names.length] = "July";
              month_names[month_names.length] = "August";
              month_names[month_names.length] = "September";
              month_names[month_names.length] = "October";
              month_names[month_names.length] = "November";
              month_names[month_names.length] = "December";
              var curr_date = d.getDate();
              var curr_month = d.getMonth(); //Months are zero based
              var curr_year = d.getFullYear();
              var tanggalbaru = curr_date + " " + month_names[curr_month] + " " + curr_year;

              $('#tbodypendapatan').append(
                '<tr>'+
                  '<td>'+c+'</td>'+
                  '<td>'+tanggalbaru+'</td>'+
                  '<td>'+data[i]['kategori_pendapatan']+'</td>'+
                  '<td>'+data[i]['jenis_pendapatan']+'</td>'+
                  '<td>'+data[i]['instansi_pembayar']+'</td>'+
                  '<td class="text-right">'+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td style="text-align:center"><a href="#" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>'+
                '</tr>'
              );
            }
          }else {
            $('#tbodypendapatan').append('<tr><td colspan="7">Tidak ada data</td></tr>');
          }
        },
        error: function(data){
          console.log(data);;
        }
      })
    })

    //search pendapatan
    $('#frmsearch').submit(function(e){
      e.preventDefault();
      var katakunci = {};
      katakunci['search'] = $('#katakuncipendapatan').val();

      $.ajax({
        type: "POST",
        data: katakunci,
        url: "<?php echo base_url()?>keuangan/homeunitpendapatan/search_pendapatan",
        success: function(data){
          console.log(data);
          $('#tbodypendapatan').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              var c = i+1;
              var d = new Date(data[i]['tanggal']);
              var month_names = new Array ( );
              month_names[month_names.length] = "January";
              month_names[month_names.length] = "February";
              month_names[month_names.length] = "March";
              month_names[month_names.length] = "April";
              month_names[month_names.length] = "May";
              month_names[month_names.length] = "June";
              month_names[month_names.length] = "July";
              month_names[month_names.length] = "August";
              month_names[month_names.length] = "September";
              month_names[month_names.length] = "October";
              month_names[month_names.length] = "November";
              month_names[month_names.length] = "December";
              var curr_date = d.getDate();
              var curr_month = d.getMonth(); //Months are zero based
              var curr_year = d.getFullYear();
              var tanggalbaru = curr_date + " " + month_names[curr_month] + " " + curr_year;

              $('#tbodypendapatan').append(
                '<tr>'+
                  '<td>'+c+'</td>'+
                  '<td>'+tanggalbaru+'</td>'+
                  '<td>'+data[i]['kategori_pendapatan']+'</td>'+
                  '<td>'+data[i]['jenis_pendapatan']+'</td>'+
                  '<td>'+data[i]['instansi_pembayar']+'</td>'+
                  '<td class="text-right">'+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
                  '<td style="text-align:center"><a href="#" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>'+
                '</tr>'
              );
            }
          }else {
            $('#tbodypendapatan').append('<tr><td colspan="7">Tidak ada data</td></tr>');
          }
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //simpan pendapatan
    $('#frmpendapatan').submit(function(e){
      e.preventDefault();
      if (($('#add_kategori').val() == 'PENDAPATAN PELAYANAN') && ($('#add_jenis1').val() == '')) {
        alert('Pilih jenis pendapatan');
        $('#add_jenis1').focus();
        return false;
      }else if (($('#add_kategori').val() == 'PENDAPATAN NON PELAYANAN') && ($('#add_jenis2').val() == '')) {
        alert('Pilih jenis pendapatan');
        $('#add_jenis2').focus();
        return false;
      }

      var item = {};
      item['tanggal'] = $('#add_tanggal').val()
      item['kategori_pendapatan'] = $('#add_kategori').val();
      if ($('#add_kategori').val() == 'PENDAPATAN PELAYANAN') {
        item['jenis_pendapatan'] = $('#add_jenis1').val();
      }else if ($('#add_kategori').val() == 'PENDAPATAN NON PELAYANAN') {
        item['jenis_pendapatan'] = $('#add_jenis2').val();
      }
      item['instansi_pembayar'] = $('#add_instansi').val()
      item['jumlah'] = $('#add_jumlah').autoNumeric('get');

      console.log(item);
      $.ajax({
        type: "POST",
        data: item,
        url: "<?php echo base_url()?>keuangan/homeunitpendapatan/insert_pendapatan",
        success: function(data){
          console.log(data)
          myAlert('DATA BERHASIL DITAMBAHKAN');
          window.location.replace("<?php echo base_url()?>keuangan/homeunitpendapatan");
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //delete pendapatan
    $('.delete').click(function(e){
      e.preventDefault();
      var x = confirm('Yakin akan menghapus data?');
      if (x) {
        var id = $(this).closest('tr').find('td .val_id').val();

        $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>keuangan/homeunitpendapatan/delete_pendapatan/" + id,
          success: function(data){
            console.log(data);
            myAlert('DATA BERHASIL DIHAPUS');
            window.location.replace("<?php echo base_url()?>keuangan/homeunitpendapatan");
          },
          error: function(data){
            console.log(daata);
          }
        })
      }else {
        return false;
      }
    })
		
		//end of pendapatan
		$('#nama_petugas_jaspel').focus(function(){
			var $input = $('#nama_petugas_jaspel');

			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>bersalin/homebersalin/get_all_dokter',//sama di igd :D
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(data);

					$input.typeahead({source:autodata,
			            autoSelect: true});

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#id_petugas_jaspel').val(iddata[index]);

					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		$('#nama_unit_jaspel').focus(function(){
			var $input = $('#nama_unit_jaspel');

			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>keuangan/homeunitpendapatan/get_all_unit',//sama di igd :D
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_dept']);
						iddata.push(data[i]['dept_id']);
					}
					console.log(data);

					$input.typeahead({source:autodata,
			            autoSelect: true});

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#id_unit_jaspel').val(iddata[index]);

					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		$('#btn_filter_jaspel').on('click',function (e) {
			e.preventDefault();
			var item ={};
			item['mulai'] = format_date3($('#mulai_jaspel').val());
			item['akhir'] = format_date3($('#akhir_jaspel').val());
			item['cara_bayar'] = $('#carabayar_jaspel').val();
			item['petugas_id'] = $('#id_petugas_jaspel').val();
			item['unit'] = $('#id_unit_jaspel').val();
			console.log(item);
			$.ajax({
				type: "POST",
				data: item,
				url: "<?php echo base_url()?>keuangan/homeunitpendapatan/filter_jaspel",
				success: function  (data) {
					console.log(data);
					var t = $('#tabelJpPoliinap').DataTable();
					t.clear().draw();
					for (var i = 0; i < data.length; i++) {
						t.row.add([
							Number(i+1),
							format_date(data[i]['tanggal']),
							data[i]['nama_tindakan'],
							data[i]['nama_dept'],
							data[i]['cara_bayar'],
							data[i]['nama_petugas'],
							data[i]['jlh_tindakan'],
							data[i]['jp'],
							data[i]['total']
						]).draw();
					};
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		/*jaspel apotek*/
		$('#ap_btn_filter_jaspel').on('click',function (e) {
			e.preventDefault();
			var item = {};
			item['start'] = $('#ap_start').val();
			item['end'] = $('#ap_end').val();
			item['cara_bayar'] =  $('#ap_carabayar').find('option:selected').val();
			item['unit'] = $('#ap_unit_id').val();
			item['paramedis'] = $('#ap_paramedis_id').val();

			$.ajax({
				type: "POST",
				data: item,
				url: "<?php echo base_url()?>keuangan/homeunitpendapatan/filter_jasa_resep",
				success: function (data) {
					console.log(data);
					var t = $('#tblPerhitunganResep').DataTable();
					t.clear().draw();
					for (var i = 0; i < data.length; i++) {
						t.row.add([
							(Number(i) + 1),
							format_date(data[i]['waktu_penjualan']),
							data[i]['dept_resep'],
							data[i]['cara_bayar'],
							data[i]['resep_id'],
							data[i]['nama'],
							data[i]['nama_petugas'],
							data[i]['management'],
							data[i]['jasadokter'],
							data[i]['remunisasi'],
							data[i]['apotek'],
							''
							]).draw();
					};
				}
			})
		})

		$('#ap_paramedis').focus(function(){
			var $input = $('#ap_paramedis');

			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>bersalin/homebersalin/get_all_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata,
			            autoSelect: true});

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#ap_paramedis_id').val(iddata[index]);

					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				},
				error: function (data) {
					console.log(data);
				}
			});
		});

		$('#ap_unit').focus(function(){
			var $input = $('#ap_unit');

			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>keuangan/homeunitpendapatan/get_all_unit',//sama di igd :D
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_dept']);
						iddata.push(data[i]['dept_id']);
					}
					console.log(data);

					$input.typeahead({source:autodata,
			            autoSelect: true});

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#ap_unit_id').val(iddata[index]);

					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
		/*akhir apotek*/

		/*jaspel kamar operasi*/
		$('#ko_paramedis').focus(function(){
			var $input = $('#ko_paramedis');

			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>bersalin/homebersalin/get_all_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata,
			            autoSelect: true});

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#ko_paramedis_id').val(iddata[index]);

					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				},
				error: function (data) {
					console.log(data);
				}
			});
		});

		$('#btn_filter_kamar_operasi').on('click', function (e) {
			e.preventDefault();

			var item = {};
			item['start'] = format_date3($('#ko_start').val());
			item['end'] = format_date3($('#ko_end').val());
			item['dokter'] = $('#ko_paramedis_id').val();
			item['cara_bayar'] = $('#ko_carabayar').val();
			console.log(item);
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url();?>keuangan/homeunitpendapatan/filter_jasa_kamar_operasi',
				success: function (data) {
					console.log(data);
					var t = $('#tabeljaspeloperasi').DataTable();
					t.clear().draw();
					for (var i = 0; i < data.length; i++) {
						var wkt;
						if (data[i]['waktu_selesai'] == '') {
							wkt = ''
						}else{
							wkt = format_date(data[i]['waktu_selesai']);
						}
						t.row.add([
							Number(i+1),
							wkt,
							data[i]['cara_bayar'],
							data[i]['nama_tindakan'],
							data[i]['nama'],
							data[i]['jp'],
							data[i]['d_bedah'],
							data[i]['jp'],
							data[i]['d_anestesi'],
							'0',
							data[i]['d_anak'],
							'0','0',
							data[i]['js'],
							data[i]['bakhp'],
							''
							]).draw();
					};
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*akhir jaspel operasi*/

		/*logistik*/
		var this_io;
		$('#tbodyinventoribarang').on('click', 'tr td a.edBarang', function (e) {
			e.preventDefault();
			$('#id_barang_inoutprocess').val($(this).closest('tr').find('td .barang_detail_inout').val());
			var jlh = $(this).closest('tr').find('td').eq(4).text();
			this_io = $(this);
			$('#sisaInOut').val(jlh);

			$('#jmlInOut').on('change', function (e) {
				e.preventDefault();

				var is_in = $('#io').find('option:selected').val();
				var jmlInOut = $('#jmlInOut').val();
				var sisa = jlh;//$('#sisaInOut').val();
				var hasil ="";
				if (is_in == 'IN') {
					hasil = Number(jmlInOut) + Number(sisa);
				}else{
					hasil = Number(sisa) - Number(jmlInOut);
				}

				if (jmlInOut == '') {
					hasil = Number(sisa);
				}
				$('#sisaInOut').val(hasil);
			})

			$('#io').on('change', function () {
				var jumlah = Number($('#jmlInOut').val());
				var sisa = Number(jlh);//Number($('#sisaInOut').val());

				var isout = $('#io').find('option:selected').val();
				if (isout === 'IN') {
					$('#sisaInOut').val(jumlah + sisa);
				} else{
					$('#sisaInOut').val(sisa - jumlah);
				};
			})
		})

		$('#forminoutbarang').submit(function (e) {
			e.preventDefault();

			var item = {};
			item['barang_detail_id'] = $('#id_barang_inoutprocess').val();
			item['jumlah'] = $('#jmlInOut').val();
			item['sisa'] = $('#sisaInOut').val();
			item['is_out'] = $('#io').find('option:selected').val();
		    item['tanggal'] = $('#tanggalinout').val();
		    item['keterangan'] = $('#keteranganIO').val();
		    //console.log(item);return false;
		    if (item['jumlah'] != "") {
			    $.ajax({
			    	type: "POST",
			    	data: item,
			    	url: "<?php echo base_url()?>keuangan/homeunitpendapatan/input_in_outbarang",
			    	success: function (data) {
			    		if (data == "true") {
			    			myAlert('data berhasil disimpan');
			    			$('#keteranganIO').val('');
			    			$('#jmlInOut').val('');
			    			this_io.closest('tr').find('td').eq(4).text(item['sisa']);
			    			$('#inoutbar').modal('hide');
			    		} else{
			    			myAlert('gagal, terdapat kesalahan');
			    		};
			    	},
			    	error: function (data) {
			    		myAlert('gagal');
			    	}
			    })
			} else{
				myAlert('isi data dengan benar');
				$('#jmlInOut').focus();
			};
		})

		$("#tbodyinventoribarang").on('click', 'tr td a.detailinvenbarang', function (e) {
			var id = $(this).closest('tr').find('td .barang_detail_inout').val();

			 $.ajax({
		    	type: "POST",
		    	url: "<?php echo base_url()?>keuangan/homeunitpendapatan/get_detail_inventori/" + id,
		    	success: function (data) {
		    		console.log(data);
		    		$('#tbodydetailbrginventori').empty();
		    		for(var i = 0; i < data.length ; i++){
		    			$('#tbodydetailbrginventori').append(
							'<tr>'+
								'<td align="center">'+format_date(data[i]['tanggal'])+'</td>'+
								'<td>'+data[i]['is_out']+'</td>'+
								'<td align="right">'+data[i]['jumlah']+'</td>'+
								'<td>'+data[i]['keterangan']+'</td>'+
							'</tr>'
		    			)
		    		}
		    	},
		    	error: function (data) {
		    		myAlert('gagal');
		    	}
		    })
		})

		$('#formmintabarang').submit(function (e) {
			e.preventDefault();
			var item ={};
			item['katakunci'] = $('#katakuncimintabarang').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>keuangan/homeunitpendapatan/get_barang_gudang',
				success: function (data) {
					console.log(data);//return false;
					$('#tbodybarangpermintaan').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodybarangpermintaan').append(
								'<tr>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['tahun_pengadaan']+'</td>'+
									'<td>'+data[i]['stok_gudang']+'</td>'+
									'<td style="text-align:center"><a href="#" class="addnewpermintaanbarang"><i class="glyphicon glyphicon-check"></i></a></td>'+
									'<td style="display:none">'+data[i]['barang_stok_id']+'</td>'+
									'<td style="display:none">'+data[i]['barang_id']+'</td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodybarangpermintaan').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					}
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tbodybarangpermintaan').on('click', 'tr td a.addnewpermintaanbarang',function (e) {
			e.preventDefault();
			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputmintabarang').append(
				'<tr><td>'+cols[0]+'</td>'+//nama
				'<td>'+cols[1]+'</td>'+  //satuan
				'<td>'+cols[2]+'</td>'+ //merk
				'<td>'+cols[3]+'</td>'+ //tahun pengadaan
				'<td>'+cols[4]+'</td>'+ //stok gudang
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah minta
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
				'<td style="display:none">'+cols[6]+'</td>'+ //barang_stok_id
				'<td style="display:none">'+cols[7]+'</td></tr>' //barang_id
			)
		})

		$('#permintaanbarangunit').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['no_permintaanbarang'] = $('#nomorpermintaanbarang').val();
			item['tanggal_request'] = $('#tglpermintaanbarang').val();
			item['keterangan_request'] = $('#keteranganpermintaanbarang').val();

			var data = [];
			$('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
		    $('#addinputmintabarang').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;

		    $.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>keuangan/homeunitpendapatan/submit_permintaan_barangunit',
				success: function (data) {
					console.log(data);
					if (data['error'] == 'n'){
						$('#addinputmintabarang').empty();
						$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
						$('#nomorpermintaanbarang').val('');
						$('#keteranganpermintaanbarang').val('');
					}
					myAlert(data['message']);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*logistik unit*/
	})
</script>
