<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>keuangan/homeunitperencanaan">UNIT PERENCANAAN</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Data RBA</a>
	</li>
</div>
<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
		<li class="active"><a href="#rba" class="cl" data-toggle="tab">Data RBA</a></li>
	  <li><a href="#list" class="cl" data-toggle="tab">Pengadaan Logistik</a></li>
	  <li><a href="#logistik" class="cl" data-toggle="tab">Logistik</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
		<div class="modal fade" id="detailRBA" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-400px">
			<div class="modal-dialog">
				<div class="modal-content" style="width:1000px">
					<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h3 class="modal-title" id="myModalLabel">Detail Riwayat RBA</h3>
        	</div>
        	<div class="modal-body">
        		<div class="informasi">
							<form class="form-horizontal" role="form">
								<div class="form-group">
				      		<label class="control-label col-md-3">Nomor Formulir</label>
				      		<div class="col-md-4">
				      			<input type="text" class="form-control" id="no_formulir" name="no_formulir" readonly>
				      		</div>
				      	</div>
								<div class="form-group">
									<label class="control-label col-md-3">Tanggal</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="tgl" name="tgl" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Tahun Anggaran</label>
									<div class="col-md-2">
										<input  type="text" class="form-control" id="thn_anggaran" name="thn_anggaran" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Urusan Pemerintahan</label>
									<div class="col-md-7">
										<input  type="text" class="form-control" id="urusanpemerintahan" name="urusanpemerintahan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Organizer</label>
									<div class="col-md-7">
										<input  type="text" class="form-control" id="organizer" name="organizer" readonly>
									</div>
								</div>
								<div class="form-group">
				      		<label class="control-label col-md-3">Program</label>
				      		<div class="col-md-7">
				      			<input  type="text" class="form-control" id="program" name="program" readonly>
				      		</div>
				      	</div>
				      	<div class="form-group">
				      		<label class="control-label col-md-3">Kegiatan</label>
				      		<div class="col-md-7">
				      			<input  type="text" class="form-control" id="kegiatan" name="kegiatan" readonly>
				      		</div>
				      	</div>
				      	<div class="form-group">
				      		<label class="control-label col-md-3">Lokasi Kegiatan</label>
				      		<div class="col-md-5">
				      			<input  type="text" class="form-control" id="lokasikegiatan" name="lokasikegiatan" readonly>
				      		</div>
				      	</div>
								<div class="form-group">
									<label class="control-label col-md-3">Sumber Dana</label>
									<div class="col-md-4">
										<input  type="text" class="form-control" id="sumberdana" name="sumberdana" readonly>
									</div>
								</div>
							</form>
						</div>
						<hr class="garis">

						<div class="tabelinformasi">
							<div class="portlet box red">
								<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabeldetailriwayat">
										<thead>
											<tr class="info">
												<th>No</th>
												<th>Kode Rekening</th>
												<th>Belanja</th>
												<th>Uraian</th>
												<th>Volume</th>
												<th>Satuan</th>
												<th>Harga Satuan</th>
												<th>Jumlah</th>
											</tr>
										</thead>
										<tbody >
										</tbody>
									</table>
								</div>
							</div>
						</div>
        	</div>
    			<div class="modal-footer">
		     		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
	      	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modalUraian" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" style="width:750px">
				<div class="modal-content">
					<div class="modal-header">
      			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
      			<h3 class="modal-title" id="myModalLabel">Tambah Rincian Anggaran</h3>
      		</div>
					<form class="form-horizontal" id="frmaddrincian" role="form" method="post">
	      		<div class="modal-body">
	      			<div class="informasi">
								<div class="form-group">
    							<label class="control-label col-md-3">Kode Rekening</label>
    							<div class="col-md-5">
    								<input type="text" class="form-control" id="kdRkng" name="kdRkng" placeholder="Kode Rekening" required>
    							</div>
								</div>
								<div class="form-group">
    							<label class="control-label col-md-3">Belanja</label>
    							<div class="col-md-4">
    								<select class="form-control" id="blnja" name="blnja" required>
    									<option selected value="">Pilih</option>
    									<option value="Langsung">Langsung</option>
    									<option value="Tidak Langsung">Tidak Langsung</option>
    								</select>
    							</div>
								</div>
								<div class="form-group">
    							<label class="control-label col-md-3">Uraian</label>
									<div class="col-md-7">
    								<input type="text" class="form-control" id="uraian" name="uraian" placeholder="Uraian" required>
    							</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Volume</label>
									<div class="col-md-4">
										<input type="text" class="form-control text-right" id="volume" name="volume" placeholder="0" required>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Satuan</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan" required>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Harga Satuan</label>
									<div class="col-md-4">
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" class="form-control text-right" id="hrgSat" name="hrgSat" placeholder="Harga Satuan" value="0">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Jumlah</label>
									<div class="col-md-4">
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" class="form-control text-right" id="jumlah" name="jumlah" value="0" readonly>
										</div>
									</div>
								</div>
							</div>
        		</div>
      			<div class="modal-footer">
			       	<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
			       	<button type="submit" id="btntambah" class="btn btn-success">Tambah</button>
		      	</div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal fade" id="viewNP" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-400px">
			<div class="modal-dialog">
				<div class="modal-content" style="width:1000px">
					<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h3 class="modal-title" id="myModalLabel">Detail</h3>
        	</div>
        	<div class="modal-body">
        		<form class="form-horizontal" role="form">
		        	<div class="informasi" id="info1">
								<div class="form-group">
									<label class="control-label col-md-3">Nomor</label>
									<div class="col-md-4">
										<input type="text" class="form-control" id="nmrLog" name="nmrLog" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Tanggal</label>
									<div class="col-md-4">
										<input type="text" class="form-control" id="tgLog" name="tgLog" data-provide="datepicker" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Keterangan</label>
									<div class="col-md-4">
										<textarea class="form-control" id="ketLog" disabled></textarea>
									</div>
								</div>
							</div>
						</form>
						<div class="tabelinformasi">
							<div class="portlet box red">
								<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover table-responsive" id="tabel_detail_pengadaan">
										<thead>
											<tr class="info">
												<th >Nama Obat/Barang</th>
												<th>Penyedia</th>
												<th>Quantity</th>
												<th>Satuan</th>
												<th>Harga</th>
												<th>Total</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>122</td>
												<td>Arbet</td>
												<td>P</td>
												<td>1212121</td>
												<td>Belum Diproses</td>
												<td>asa</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
        	</div>
        	<div class="modal-footer">
 			    	
 			    	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
 			    	
			    </div>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="list">
			<!-- <div class="informasi" style="margin-right:60px">
			   	<form method="POST" id="search_submit">
				   	<div class="search">
						<label class="control-label col-md-3">
							<i class="fa fa-search" style="margin-left: -180px">&nbsp;&nbsp;</i>
						</label>
						<div class="col-md-4" style="margin-left: -420px">
							<input type="text" class="form-control" placeholder="Masukkan Kata Kunci" autofocus>
						</div>
				      	<button type="submit" class="btn btn-info">Cari</button>
					</div>
				</form>
			</div> 
			<br>
			<hr class="garis"> -->

			<!-- <div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px; margin-left: -50px;">LIST PERMINTAAN</p>
			</div> -->
			<div role="form">
				<div class="portlet box red">
					<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tabel_pengadaan_gudang">
							<thead>
								<tr class="info">
									<th style="text-align:center;width:20px;">No</th>
									<th>No Pengadaan</th>
									<th>Tanggal</th>
									<th>Petugas Input</th>
									<th>Keterangan</th>
									<th width="100">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php  
									if (isset($perencanaan_gudang)) {
										$i=0;
										foreach ($perencanaan_gudang as $key) {
											$tgl = DateTime::createFromFormat('Y-m-d H:i:s', $key['tanggal']);
											echo '<tr>
												<td style="text-align:center">'.(++$i).'</td>
												<td>'.$key['no_pengadaan'].'</td>
												<td align="center">'.$tgl->format('d F Y H:i:s').'</td>
												<td>'.$key['nama_petugas'].'</td>
												<td>'.$key['keterangan'].'</td>
												<td style="text-align:center">
													<input type="hidden" class="jenis_transaksi" value="'.$key['jenis'].'">
													<input type="hidden" class="id_transaksi" value="'.$key['barang_rencana_id'].'">
													<a href="#viewNP" class="view_detail_pengadaan" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>
													<a href="homeunitperencanaan/print_pengadaan/'.$key['jenis'].'/'.$key['barang_rencana_id'].'"><i class="glyphicon glyphicon-print" data-toggle="tooltip" data-placement="top" title="Print"></i></a>
												</td>
											</tr>';
										}
									}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
		</div>

  	<div class="tab-pane active" id="rba">
	  	<div class="dropdown" id="bbtambah" style="margin-left:10px;width:98.5%">
	      	<div id="titleInformasi">Tambah Rencana Bisnis dan Anggaran</div>
	      	<div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
      	</div>
      	<br>
      	<div id="ibbtambahrencana">
			<form class="form-horizontal" id="submit_form" method="POST">
	      	<div class="informasi" >
				<div class="form-group">
		        	<label class="control-label col-md-2">Nomor Formulir</label>
		        	<div class="col-md-3">
		        		<input type="text" class="form-control" id="add_no_formulir" name="add_no_formulir" placeholder="Nomor Formulir" required>
		        	</div>
		        </div>
				<div class="form-group">
					<label class="control-label col-md-2">Tanggal</label>
					<div class="col-md-3">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input type="text" style="cursor:pointer;background-color:white" class="form-control isian" id="add_tgl" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Tahun Anggaran</label>
					<div class="col-md-1">
						<input type="number" class="form-control" id="add_thn_anggaran" placeholder="Tahun Anggaran" value="<?php echo date('Y')?>" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Urusan Pemerintahan</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="add_urusanpemerintahan" placeholder="Urusan Pemerintahan" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Organizer</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="add_organizer" placeholder="Organizer" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Program</label>
					<div class="col-md-7">
						<input  type="text" class="form-control" id="add_program" placeholder="Program" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Kegiatan</label>
					<div class="col-md-7">
						<input  type="text" class="form-control" id="add_kegiatan" placeholder="Kegiatan" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Lokasi Kegiatan</label>
					<div class="col-md-4">
						<input  type="text" class="form-control" id="add_lokasikegiatan" placeholder="Lokasi Kegiatan" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Sumber Dana</label>
					<div class="col-md-3">
						<input  type="text" class="form-control" id="add_sumberdana" placeholder="Sumber Dana" required>
					</div>
				</div>
			</div>
			<hr class="garis">

        	<div class="informasi">
          		<a href="#modalUraian" data-toggle="modal" style="font-size:11pt;margin-left:-50px"><i class="fa fa-plus">&nbsp;Tambah Rincian Anggaran</i></a>
        	</div>
			<div class="tabelinformasi">
				<div class="portlet box red">
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive">
							<thead>
								<tr class="info">
									<th>Kode Rekening</th>
									<th>Belanja</th>
									<th>Uraian</th>
									<th>Volume</th>
									<th>Satuan</th>
									<th>Harga Satuan</th>
									<th>Jumlah</th>
									<th width="20"></th>
								</tr>
							</thead>
							<tbody id="tbodyaddrincian">
								<tr class="kosong"><td colspan="8">Tambah Rincian Anggaran</td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<br>
			<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
			<div style="margin-left:80%">
				<span style=" padding:0px 10px 0px 10px;">
					<button type="reset" id="btnreset" class="btn btn-danger">BATAL</button> &nbsp;
					<button type="submit" class="btn btn-success" id="btnsimpanrencana">SIMPAN</button>
				</span>
			</div>
			<br>
			<br>
			</form>
		</div>

		<div class="dropdown" id="bbriwayat" style="margin-left:10px;width:98.5%">
	      	<div id="titleInformasi">Riwayat RBA</div>
	     	<div id="btnBawahInventori" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>
      	</div>
		
		<div class="tabelinformasi" id="ibbriwayat">
			<div class="portlet box red">
				<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<?php
						if(isset($totalriwayat))
						{
							echo '<input type="hidden" id="totalriwayatrba" value="'.$totalriwayat['total'].'">';
						}else{
							echo '<input type="hidden" id="totalriwayatrba" value="0">';
						}
					?>

					<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tabelriwayatrba">
						<thead>
							<tr class="info">
								<th width="20">No</th>
								<th>Nomor Formulir</th>
								<th>Tanggal</th>
								<th>Tahun Anggaran</th>
								<th>Lokasi</th>
								<th>Sumber Dana</th>
								<th width="100">Action</th>
							</tr>
						</thead>
						<tbody id="tbodyriwayatrba">
							<?php
								if (isset($riwayat)) {
									if (!empty($riwayat)) {
										$i = 0;
										foreach ($riwayat as $value) {
											$tgl = DateTime::createFromFormat('Y-m-d', $value['tanggal']);
											echo '<tr>
												<td>'.(++$i).'</td>
												<td>'.$value['no_formulir'].'</td>
												<td>'.$tgl->format('d F Y').'</td>
												<td>'.$value['thn_anggaran'].'</td>
												<td>'.$value['lokasi'].'</td>
												<td>'.$value['sumber_dana'].'</td>
												<td align="center"><a href="#detailRBA" id="viewdetailrba" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>
													<a href="'.base_url().'keuangan/homeunitperencanaan/print_detail_rba/'.$value['id'].'"><i class="glyphicon glyphicon-tasks" data-toggle="tooltip" data-placement="top" title="Export Excel"></i></a>
													<input type="hidden" id="val_urusan" value="'.$value['urusan_pemerintahan'].'">
													<input type="hidden" id="val_organizer" value="'.$value['organizer'].'">
													<input type="hidden" id="val_program" value="'.$value['program'].'">
													<input type="hidden" id="val_kegiatan" value="'.$value['kegiatan'].'">
													<input type="hidden" id="val_lokasi" value="'.$value['lokasi'].'">
													<input type="hidden" id="val_id" value="'.$value['id'].'">
												</td>
											</tr>';
										}
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<br>
  	</div>

  	<div class="tab-pane" id="logistik">
    	<div class="modal fade" id="modalbarang" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" style="width:900px;">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
        			</div>
        			<div class="modal-body">

	        			<div class="form-group">
	        				<form method="post" class="form-horizontal" role="form" id="formmintabarang">
								<div class="form-group">	
									<div class="col-md-5" style="margin-left:20px;">
										<input type="text" class="form-control" name="katakunci" id="katakuncimintabarang" placeholder="Nama barang"/>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-info">Cari</button>
									</div>
									<br><br>	
								</div>		
							</form>
							<div style="margin-right:10px;margin-left:10px;"><hr></div>
							<div class="portlet-body" style="margin: 0px 20px 0px 15px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%">
									<thead>
										<tr class="info">
											<th>Nama Barang</th>
											<th>Satuan</th>
											<th>Merek</th>
											<th>Tahun Pengadaan</th>
											<th>Stok Gudang</th>
											<th width="10%">Pilih</th>
										</tr>
									</thead>
									<tbody id="tbodybarangpermintaan">
										<tr>
											<td colspan="6" style="text-align:center">Cari data Barang</td>
										</tr>
									</tbody>
								</table>												
							</div>
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>
       	<div class="dropdown" id="btnBawahInventoriBarang">
            <div id="titleInformasi">Inventori</div>
            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
        </div>
        <div id="infoInventoriBarang">
			
			<div class="form-group" >
				<div class="portlet-body" style="margin: 30px 10px 20px 10px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tblinventorigudangunit">
						<thead>
							<tr class="info" >
								<th width="20">No.</th>
								<th > Nama Barang </th>
								<th > Merek </th>
								<th > Harga </th>
								<th > Stok </th>
								<th > Satuan </th>
								<th > Tahun Pengadaan</th>
								<th > Sumber Dana</th>
								<th width="100"> Action </th>

							</tr>
						</thead>
						<tbody id="tbodyinventoribarang">
							<?php 
								if (isset($inventoribarang)) {
									if (!empty($inventoribarang)) {
										$i = 1;
										foreach ($inventoribarang as $value) {
											echo '<tr>
													<td>'.($i++).'</td>
													<td>'.$value['nama'].'</td>
													<td>'.$value['nama_merk'].'</td>
													<td>'.$value['harga'].'</td>
													<td>'.$value['stok'].'</td>
													<td>'.$value['satuan'].'</td>
													<td>'.$value['tahun_pengadaan'].'</td>
													<td>'.$value['sumber_dana'].'</td>
													<td style="text-align:center">
														<input type="hidden" class="barang_detail_inout" value="'.$value['barang_detail_id'].'">
														<a href="#inoutbar" data-toggle="modal" class="edBarang" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
														<a href="#edInvenBerBar" data-toggle="modal" class="detailinvenbarang"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
													</td>
												</tr>';
										}
									}
								}
							?>
								
						</tbody>
					</table>
				</div>
				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
        	</div>
        </div>
		<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<form class="form-horizontal" role="form" style="margin-left:30px;" id="forminoutbarang">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
	        			</div>
	        			<div class="modal-body">
		        			<div class="form-group">
		        				<label class="control-label col-md-3" >Tanggal </label>
								<div class="col-md-6" >
					         		<div class="input-icon">
										<i class="fa fa-calendar"></i>
										<input type="text" style="cursor:pointer;background-color:white" id="tanggalinout" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
									
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" >In / Out </label>
								<div class="col-md-6">
					         		<select class="form-control select" name="io" id="io">
										<option value="IN" selected>IN</option>
										<option value="OUT">OUT</option>					
									</select>
								</div>
							</div>
							<div class="form-group">
		        				<label class="control-label col-md-3" >Jumlah in/out</label>
								<div class="col-md-6" >
					         		<input type="text" class="form-control" id="jmlInOut" name="jmlInOut" placeholder="Jumlah">
								</div>
							</div>
							<div class="form-group">
		        				<label class="control-label col-md-3" >Sisa Stok </label>
								<div class="col-md-6" >
					         		<input type="text" class="form-control" id="sisaInOut" name="sisaInOut" placeholder="Sisa Stok" readonly>
								</div>
							</div>
							<div class="form-group">
		        				<label class="control-label col-md-3" >Keterangan </label>
								<div class="col-md-6" >
									<textarea class="form-control" id="keteranganIO" placeholder="Keterangan"></textarea>
								</div>
							</div>										
	        			</div>
	        			<div class="modal-footer">
	        				<input type="hidden" id="id_barang_inoutprocess">
	 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
	 			       		<button type="submit" class="btn btn-success">Simpan</button>
				      	</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content" >
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
        			</div>
        			<div class="modal-body">
	        			<form class="form-horizontal" role="form">
			            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
								<thead>
									<tr class="info" >
										<th> Waktu </th>
										<th> IN / OUT </th>
										<th> Jumlah </th>
										<th> Keterangan </th>
									</tr>
								</thead>
								<tbody id="tbodydetailbrginventori">
									<tr>
										<td colspan="4" style="text-align:center">Tidak ada detail in-out</td>
									</tr>
										
								</tbody>
							</table>
						</form>
						
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>
		<br>

		<div class="dropdown" id="btnBawahPermintaanBarang" style="margin-left:10px;width:98.5%">
            <div id="titleInformasi">Permintaan Logistik</div>
            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
        </div>
        <div id="infoPermintaanBarang">
        	<form class="form-horizontal" role="form" method="post" id="permintaanbarangunit">
            	<div class="informasi">
            		<br>
        			<div class="form-group">
        				<div class="col-md-2">
        					<label class="control-label">Nomor Permintaan</label>
        				</div>
        				<div class="col-md-3">
        					<input type="text" class="form-control" name="noPermFarmBers" id="nomorpermintaanbarang" placeholder="Nomor Permintaan"/>
						</div>
						<div class="col-md-1"></div>
						<div class="col-md-2">
        					<label class="control-label">Tanggal Permintaan</label>
        				</div>
        				<div class="col-md-2">
        					<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanbarang" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
							</div>
						</div>
        			</div>
        			<div class="form-group">
        				<div class="col-md-2">
        					<label class="control-label">Keterangan</label>
        				</div>
        				<div class="col-md-3">	
							<textarea class="form-control" id="keteranganpermintaanbarang" name="ketObatFarBers"></textarea>	
						</div>
        			</div>
        		</div>
				<a href="#modalbarang" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Barang</i></a>
				<div class="clearfix"></div>

				<div class="portlet box red">
					<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
							<thead>
								<tr class="info" >
									<th> Nama Barang </th>
									<th> Satuan </th>
									<th> Merek </th>
									<th> Tahun Pengadaan </th>
									<th> Stok Gudang </th>
									<th> Jumlah Diminta </th>
									<th width="80"> Action </th>			
								</tr>
							</thead>
							<tbody  id="addinputmintabarang">
								<?php echo '<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>'; ?>
							</tbody>
						</table>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<button class="btn btn-warning" type="reset" id="batalpermintaanfarmasi">RESET</button>
						<button class="btn btn-success" type="submit">SIMPAN</button>
					</div>
					<br>
				</div>	
			</form>
		</div>	    
		<br>
    </div>
</div>



<!-- <script type="text/javascript">
	$(document).ready( function(){
			$('#btnSimpanpengadaaan').on('click',function(e){
				e.preventDefault();
				tambahPengadaanRBA('#addpengadaan', '#btnSimpanpengadaaan');
			});
	});

</script> -->
<script type="text/javascript">
	$(document).ready(function(){
		$("#bwpermintaanlogistik").click(function(){
			$("#ibwpermintaanlogistik").slideToggle();

		});
		$("#bwinlogistik").click(function(){
			$("#ibwinlogistik").slideToggle();

		});

		$("#bbtambah").click(function(){
			$("#ibbtambahrencana").slideToggle();

		});

		$("#bbriwayat").click(function(){
			$("#ibbriwayat").slideToggle();

		});

		$("#btnBawahPermintaanBarang").click(function(){
			$("#infoPermintaanBarang").slideToggle();

		});
	});
</script>
