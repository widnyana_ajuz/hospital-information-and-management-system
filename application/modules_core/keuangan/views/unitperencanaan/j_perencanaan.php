<script type="text/javascript">
  $(document).ready(function(){
    $('#volume').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
    $('#hrgSat').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});

    $('#btnreset').on('click', function(e){
      e.preventDefault();
      reset_rincian();
    })

    //detail riwayat rba
    $('#tbodyriwayatrba').on('click', '#viewdetailrba', function(e){
      e.preventDefault();
      $('#no_formulir').val($(this).closest('tr').find('td').eq(1).text());
      $('#tgl').val($(this).closest('tr').find('td').eq(2).text());
      $('#thn_anggaran').val($(this).closest('tr').find('td').eq(3).text());
      $('#urusanpemerintahan').val($(this).closest('tr').find('td #val_urusan').val());
      $('#organizer').val($(this).closest('tr').find('td #val_organizer').val());
      $('#program').val($(this).closest('tr').find('td #val_program').val());
      $('#kegiatan').val($(this).closest('tr').find('td #val_kegiatan').val());
      $('#lokasikegiatan').val($(this).closest('tr').find('td').eq(4).text());
      $('#sumberdana').val($(this).closest('tr').find('td').eq(5).text());

      var id = $(this).closest('tr').find('td #val_id').val();

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>keuangan/homeunitperencanaan/get_detail_riwayat/" + id,
        success: function(data){
          var tabel = $('#tabeldetailriwayat').DataTable();
          tabel.clear().draw();
          for (var i = 0; i < data.length; i++) {
            tabel.row.add([
              (i+1),
              data[i]['kode_rekening'],
              data[i]['belanja'],
              data[i]['uraian'],
              data[i]['volume'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
              data[i]['satuan'],
              data[i]['harga_satuan'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
              data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
            ]).draw();
          }
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    $('#frmaddrincian').on('change', '#volume', function(e){
      e.preventDefault();
      hitung_jumlah();
    })

    $('#frmaddrincian').on('change', '#hrgSat', function(e){
      e.preventDefault();
      hitung_jumlah();
    })

    $('#frmaddrincian').submit(function(e){
      e.preventDefault();
      var kode = $('#kdRkng').val();
      var belanja = $('#blnja').val();
      var uraian = $('#uraian').val();
      var volume = $('#volume').val();
      var satuan = $('#satuan').val();
      var harga = $('#hrgSat').val();
      var jumlah = $('#jumlah').val();

      $('#tbodyaddrincian').find('tr.kosong').remove();

      var newrow = '<tr> <td>'+kode+'</td>'+
                        '<td>'+belanja+'</td>'+
                        '<td>'+uraian+'</td>'+
                        '<td class="text-right">'+volume+'</td>'+
                        '<td>'+satuan+'</td>'+
                        '<td class="text-right">'+harga+'</td>'+
                        '<td class=text-right>'+jumlah+'</td>'+
                        '<td>'+
                          '<a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a>'+
                          '<input type="hidden" id="val_vol" value="'+$('#volume').autoNumeric('get')+'">'+
                          '<input type="hidden" id="val_satu" value="'+$('#hrgSat').autoNumeric('get')+'">'+
                          '<input type="hidden" id="val_tot" value="'+jumlah.toString().replace(/[^\d\,\-\ ]/g, '')+'">'+
                        '</td>'+
                    '</tr>';
      $('#tbodyaddrincian').append(newrow);
      $('#modalUraian').modal('hide');
    })

    $('#submit_form').submit(function(e){
      e.preventDefault();

      var x = confirm("Yakin akan menambahkan data? Data yang sudah ditambahkan tidak dapat dihapus.");
      if(x){
        var item = {};

        item['no_formulir'] = $('#add_no_formulir').val();
        item['tanggal'] = $('#add_tgl').val();
        item['thn_anggaran'] = $('#add_thn_anggaran').val();
        item['urusan_pemerintahan'] = $('#add_urusanpemerintahan').val();
        item['organizer'] = $('#add_organizer').val();
        item['program'] = $('#add_program').val();
        item['kegiatan'] = $('#add_kegiatan').val();
        item['lokasi'] = $('#add_lokasikegiatan').val();
        item['sumber_dana'] = $('#add_sumberdana').val();

        $('#tbodyaddrincian').find('tr.kosong').remove();

        var data = [];
        $('#tbodyaddrincian').find('tr').each(function(rowIndex, r){
          var cols = [];
          $(this).find('td').each(function(colIndex,c){
            cols.push(c.textContent);
            cols.push(c.value);
          });
          $(this).find('td input').each(function(colIndex,c){
            cols.push(c.value);
          });
          data.push(cols);
        })

        if (data.length == 0) {
          alert('Isi rincian anggaran!');
          $('#tbodyaddrincian').append('<tr class="kosong"><td colspan="8">Tambah Rincian Anggaran</td></tr>');
          return false;
        }
        console.log(data);
        item['data'] = data;

        $.ajax({
          type: "POST",
          data: item,
          url: "<?php echo base_url()?>keuangan/homeunitperencanaan/insert_rba",
          success: function(data){
            console.log(data);
            myAlert('DATA BERHASIL DITAMBAHKAN');
            reset_rincian();

            console.log(item);

            var tot = Number($('#totalriwayatrba').val()) + 1;
            $('#totalriwayatrba').val(tot);

            var tabel = $('#tabelriwayatrba').DataTable();
            var act = '<a href="#detailRBA" id="viewdetailrba" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>' +
            '<a href="#" ><i class="glyphicon glyphicon-tasks" data-toggle="tooltip" data-placement="top" title="Export Excel"></i></a>' +
            '<input type="hidden" id="val_urusan" value="' + item['urusan_pemerintahan']+'">' +
            '<input type="hidden" id="val_organizer" value="' + item['organizer'] +'">' +
            '<input type="hidden" id="val_program" value="'+ item['program'] +'">' +
            '<input type="hidden" id="val_kegiatan" value="' + item['kegiatan'] +'">' +
            '<input type="hidden" id="val_lokasi" value="' + item['lokasi'] +'">' +
            '<input type="hidden" id="val_id" value="' + data +'">';

            tabel.row.add([
              tot,
              item['no_formulir'],
              item['tanggal'],
              item['thn_anggaran'],
              item['lokasi'],
              item['sumber_dana'],
              act
            ]).draw();

          },
          error: function(data){
            console.log(data);
          }
        })
      }else {
        reset_rincian();
        return false;
      }
    })

    $('#tabel_pengadaan_gudang').on('click', 'tr td a.view_detail_pengadaan', function (e) {
        e.preventDefault();
        var jenis = $(this).closest('tr').find('td .jenis_transaksi').val();
        var id = $(this).closest('tr').find('td .id_transaksi').val();

        $('#nmrLog').val('');
        $('#tgLog').val('');
        $('#ketLog').val('');

        var nomor = $(this).closest('tr').find('td').eq(1).text();
        var tanggal = $(this).closest('tr').find('td').eq(2).text();
        var keterangan = $(this).closest('tr').find('td').eq(4).text();

        $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>keuangan/homeunitperencanaan/view_detail_pengadaan/"+jenis+"/"+id,
          success: function (data) {
            console.log(data);
            $('#nmrLog').val(nomor);
            $('#tgLog').val(tanggal);
            $('#ketLog').val(keterangan);

            $('#tabel_detail_pengadaan tbody').empty();
            for (var i = 0; i < data.length; i++) {
              $('#tabel_detail_pengadaan tbody').append(
                '<tr>'+
                  '<td>'+data[i]['nama']+'</td>'+
                  '<td>'+data[i]['nama_penyedia']+'</td>'+
                  '<td align="right">'+data[i]['jumlah']+'</td>'+
                  '<td>'+data[i]['satuan']+'</td>'+
                  '<td align="right">'+data[i]['harga']+'</td>'+
                  '<td align="right">'+data[i]['total']+'</td>'+
                '</tr>'
                );
            };

          },
          error:function (data) {
            console.log(data);
          }
        })
    })

    /*logistik*/
    var this_io;
    $('#tbodyinventoribarang').on('click', 'tr td a.edBarang', function (e) {
      e.preventDefault();
      $('#id_barang_inoutprocess').val($(this).closest('tr').find('td .barang_detail_inout').val());
      var jlh = $(this).closest('tr').find('td').eq(4).text();
      this_io = $(this);
      $('#sisaInOut').val(jlh);

      $('#jmlInOut').on('change', function (e) {
        e.preventDefault();

        var is_in = $('#io').find('option:selected').val();
        var jmlInOut = $('#jmlInOut').val();
        var sisa = jlh;//$('#sisaInOut').val();
        var hasil ="";
        if (is_in == 'IN') {
          hasil = Number(jmlInOut) + Number(sisa);
        }else{      
          hasil = Number(sisa) - Number(jmlInOut);
        }

        if (jmlInOut == '') {
          hasil = Number(sisa);
        }
        $('#sisaInOut').val(hasil);     
      })

      $('#io').on('change', function () {
        var jumlah = Number($('#jmlInOut').val());
        var sisa = Number(jlh);//Number($('#sisaInOut').val());

        var isout = $('#io').find('option:selected').val();
        if (isout === 'IN') {
          $('#sisaInOut').val(jumlah + sisa);
        } else{
          $('#sisaInOut').val(sisa - jumlah);
        };
      })
    })
  
    $('#forminoutbarang').submit(function (e) {
      e.preventDefault();

      var item = {};
      item['barang_detail_id'] = $('#id_barang_inoutprocess').val();
      item['jumlah'] = $('#jmlInOut').val();
      item['sisa'] = $('#sisaInOut').val();
      item['is_out'] = $('#io').find('option:selected').val();
        item['tanggal'] = $('#tanggalinout').val();
        item['keterangan'] = $('#keteranganIO').val();
        //console.log(item);return false;
        if (item['jumlah'] != "") {
          $.ajax({
            type: "POST",
            data: item,
            url: "<?php echo base_url()?>keuangan/homeunitperencanaan/input_in_outbarang",
            success: function (data) {
              if (data == "true") {
                myAlert('data berhasil disimpan');
                $('#keteranganIO').val('');
                $('#jmlInOut').val('');
                this_io.closest('tr').find('td').eq(4).text(item['sisa']);
                $('#inoutbar').modal('hide'); 
              } else{
                myAlert('gagal, terdapat kesalahan');
              };
            },
            error: function (data) {
              myAlert('gagal');
            }
          })
      } else{
        myAlert('isi data dengan benar');
        $('#jmlInOut').focus();
      };      
    })

    $("#tbodyinventoribarang").on('click', 'tr td a.detailinvenbarang', function (e) {
      var id = $(this).closest('tr').find('td .barang_detail_inout').val();

       $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>keuangan/homeunitperencanaan/get_detail_inventori/" + id,
          success: function (data) {
            console.log(data);
            $('#tbodydetailbrginventori').empty();
            for(var i = 0; i < data.length ; i++){
              $('#tbodydetailbrginventori').append(
              '<tr>'+
                '<td align="center">'+format_date(data[i]['tanggal'])+'</td>'+
                '<td>'+data[i]['is_out']+'</td>'+
                '<td align="right">'+data[i]['jumlah']+'</td>'+
                '<td>'+data[i]['keterangan']+'</td>'+
              '</tr>'
              )
            }
          },
          error: function (data) {
            myAlert('gagal');
          }
        })
    })
    
    $('#formmintabarang').submit(function (e) {
      e.preventDefault();
      var item ={};
      item['katakunci'] = $('#katakuncimintabarang').val();
      $.ajax({
        type: "POST",
        data: item,
        url: '<?php echo base_url()?>keuangan/homeunitperencanaan/get_barang_gudang',
        success: function (data) {
          console.log(data);//return false;
          $('#tbodybarangpermintaan').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              $('#tbodybarangpermintaan').append(
                '<tr>'+
                  '<td>'+data[i]['nama']+'</td>'+
                  '<td>'+data[i]['satuan']+'</td>'+
                  '<td>'+data[i]['nama_merk']+'</td>'+
                  '<td align="center">'+data[i]['tahun_pengadaan']+'</td>'+
                  '<td align="right">'+data[i]['stok_gudang']+'</td>'+
                  '<td style="text-align:center"><a href="#" class="addnewpermintaanbarang"><i class="glyphicon glyphicon-check"></i></a></td>'+
                  '<td style="display:none">'+data[i]['barang_stok_id']+'</td>'+
                  '<td style="display:none">'+data[i]['barang_id']+'</td>'+
                '</tr>'
              )
            };
          }else{
            $('#tbodybarangpermintaan').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
          } 
        },
        error: function (data) {
          console.log(data);
        }
      })
    })

    $('#tbodybarangpermintaan').on('click', 'tr td a.addnewpermintaanbarang',function (e) {
      e.preventDefault();
      var cols = [];
          $(this).closest('tr').find('td').each(function (colIndex, c) {
              cols.push(c.textContent);
          });

          $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
      $('#addinputmintabarang').append(
        '<tr><td>'+cols[0]+'</td>'+//nama
        '<td>'+cols[1]+'</td>'+  //satuan
        '<td>'+cols[2]+'</td>'+ //merk
        '<td>'+cols[3]+'</td>'+ //tahun pengadaan
        '<td>'+cols[4]+'</td>'+ //stok gudang
        '<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah minta
        '<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
        '<td style="display:none">'+cols[6]+'</td>'+ //barang_stok_id
        '<td style="display:none">'+cols[7]+'</td></tr>' //barang_id
      )
    })

    $('#permintaanbarangunit').submit(function (e) {
      e.preventDefault();
      var item = {};
      item['no_permintaanbarang'] = $('#nomorpermintaanbarang').val();
      item['tanggal_request'] = $('#tglpermintaanbarang').val();
      item['keterangan_request'] = $('#keteranganpermintaanbarang').val();

      var data = [];
      $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
        $('#addinputmintabarang').find('tr').each(function (rowIndex, r) {
            var cols = [];
            $(this).find('td').each(function (colIndex, c) {
                cols.push(c.textContent);
            });
            $(this).find('td input[type=number]').each(function (colIndex, c) {
                cols.push(c.value);
            });
            data.push(cols);
        });
      if(data.length == 0){
        $('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
        myAlert('detail tidak ada, isi data dengan benar');
        return false;
      }

        item['data'] = data;

        $.ajax({
        type: "POST",
        data: item,
        url: '<?php echo base_url()?>keuangan/homeunitperencanaan/submit_permintaan_barangunit',
        success: function (data) {
          console.log(data);
          if (data['error'] == 'n'){
            $('#addinputmintabarang').empty();
            $('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
            $('#nomorpermintaanbarang').val('');
            $('#keteranganpermintaanbarang').val('');
          }
          myAlert(data['message']);
        },
        error: function (data) {
          console.log(data);
        }
      })
    })
    /*logistik unit*/

  }) //end of document ready

  function hitung_jumlah()
  {
    var vol = $('#volume').autoNumeric('get');
    var hrg = $('#hrgSat').autoNumeric('get');
    var jumlah = Number(vol) * Number(hrg);
    $('#jumlah').val(jumlah.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
  }

  function reset_rincian()
  {
    $('#add_no_formulir').val('');
    $('#add_tgl').val('<?php echo date("d/m/Y")?>');
    $('#add_thn_anggaran').val('<?php echo date("Y")?>');
    $('#add_urusanpemerintahan').val('');
    $('#add_organizer').val('');
    $('#add_program').val('');
    $('#add_kegiatan').val('');
    $('#add_lokasikegiatan').val('');
    $('#add_sumberdana').val('');

    $('#tbodyaddrincian').empty();
    $('#tbodyaddrincian').append('<tr class="kosong"><td colspan="8">Tambah Rincian Anggaran</td></tr>');
  }


</script>
