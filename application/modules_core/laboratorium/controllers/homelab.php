<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homelab extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_homelab");
		$this->load->model('logistik/m_gudangbarang');
		$this->load->model("rawatjalan/m_homerawatjalan");
		$this->load->model('bersalin/m_homebersalin');
		$this->load->model('rekammedis/m_olahrekammedis');
		$this->load->model('farmasi/m_obat');
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$data['page_title'] = 'LABORATORIUM';
		$this->session->set_userdata($data);
		$data['content'] = 'home';
		$data['javascript'] = 'javascript/j_home';
		$dept_id = $this->m_homelab->get_dept_id('LABORATORIUM');
		$data['listaps'] = $this->m_homelab->get_listaps($dept_id['dept_id']);
		$data['listrujuk'] = $this->m_homelab->get_listrujuk($dept_id['dept_id']);

		$data['listaps'] = $this->m_homelab->get_listaps($dept_id['dept_id']);
		$data['listrujuk'] = $this->m_homelab->get_listrujuk($dept_id['dept_id']);

		$data['dept_id'] = $dept_id['dept_id'];
		$data['jenisperiksa'] = $this->m_homelab->get_periksa($dept_id['dept_id']);
		$tindakan = $this->m_homelab->get_alltindakan($dept_id['dept_id']);
		$data['listperiksa'] = $this->m_homelab->get_listperiksa($dept_id['dept_id']);

		$i = 0;
		if(!empty($tindakan)){
			$tamp = $tindakan[0]['kategori'];
			foreach ($tindakan as $value) {
				if($tamp==$value['kategori']){
					$value['nomor'] = $i;
					$i++;
				}else{
					$i = 0;
					$value['nomor'] = $i;
				}			

				$tamp = $value['kategori'];
				$result[] = $value;
			}

			$data['tindakan'] = $result;
		}
		// print_r($data['tindakan']);die;
		$data['jasalayan'] = $this->m_homelab->get_jasapelayanan($dept_id['dept_id']);

		$this->load->view('base/operator/template', $data);
	}

	public function search_listaps(){
		$search = $_POST['search'];
		$dept_id = $this->m_homelab->get_dept_id('LABORATORIUM')['dept_id'];
		$result = $this->m_homelab->search_listaps($search, $dept_id);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function search_listrujuk(){
		$search = $_POST['search'];
		$dept_id = $this->m_homelab->get_dept_id('LABORATORIUM')['dept_id'];
		$result = $this->m_homelab->search_listrujuk($search, $dept_id);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function search_jasapelayanan(){
		$mulai = $this->date_db($_POST['mulai']);
		$sampai = $this->date_db($_POST['sampai']);
		$carabayar = $_POST['carabayar'];
		$paramedis = $_POST['paramedis'];
		$dept = $this->m_homelab->get_dept_id("LABORATORIUM")['dept_id'];

		$result = $this->m_homelab->search_jasapelayanan($dept, $mulai, $sampai, $carabayar, $paramedis);		

		//echo $result;
		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}

	public function submit_pemeriksaan(){
		//get data until id
		$insert['penunjang_id'] = $_POST['penunjang_id'];
		$kelas = $_POST['kelas_pelayanan'];
		$nama = $_POST['nama_tindakan'];
		$tindakan = $this->m_homelab->get_tindakpenunjang($nama, $kelas);
		$insert['tindakan_penunjang_id'] = $tindakan['tindakan_penunjang_id'];
		$insert['js'] = $tindakan['js'];
		$insert['jp'] = $tindakan['jp'];
		$insert['bakhp'] = $tindakan['bakhp'];
		$insert['penunjang_detail_id'] = $this->m_homelab->get_penunjang_detail_id($insert['penunjang_id']);
		
		// print_r($insert['penunjang_detail_id']);die;
		//submit penunjang detail
		$ins = $this->m_homelab->save_tindakan($insert);

		//edit visit penunjang status
		$update['status'] = 'PROSES';
		$upd = $this->m_homelab->update_status($insert['penunjang_id'], $update);

		header("Content-type:application/json");
		echo json_encode($insert);
	}

	public function get_detailperiksa($penunjang_id){
		$result = $this->m_homelab->get_detailperiksa($penunjang_id);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function get_paramedis(){
		$result = $this->m_homelab->get_paramedis();

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function get_allperiksa($id){
		$result = $this->m_homelab->get_allperiksa($id);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function update_hasilperiksa(){
		$up_detail['penunjang_detail_id'] = $_POST['penunjang_detail_id'];
		$up_detail['hasil'] = $_POST['hasil'];
		$up_detail['nilai_normal'] = $_POST['nilai_normal'];
		$up_detail['keterangan'] = $_POST['keterangan'];
		$up_detail['pemeriksa'] = $_POST['pemeriksa'];
		$up_detail['on_faktur'] = $_POST['on_faktur'];

		$update_detail = $this->m_homelab->update_detail($up_detail['penunjang_detail_id'], $up_detail);

		$update['status'] = $_POST['status'];
		$update['penunjang_id'] = $_POST['penunjang_id'];

		$update_status = $this->m_homelab->update_status($update['penunjang_id'], $update);		
	}

	public function get_listperiksa(){
		$dept_id = $this->m_homelab->get_dept_id('LABORATORIUM')['dept_id'];
		$result = $this->m_homelab->get_listperiksa($dept_id);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function get_listaps(){
		$dept_id = $this->m_homelab->get_dept_id('LABORATORIUM')['dept_id'];
		$result = $this->m_homelab->get_listaps($dept_id);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function get_listrujuk(){
		$dept_id = $this->m_homelab->get_dept_id('LABORATORIUM')['dept_id'];
		$result = $this->m_homelab->get_listrujuk($dept_id);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function search_tagihan(){
		$search = $_POST['search'];

		$result = $this->m_homelab->search_tagihan($search);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function submit_permintaan_bersalin($value='')
	{
		$this->form_validation->set_rules('no_permintaan', 'nomor permitaan', 'required|trim|xss_clean|is_unique[obat_permintaan.no_permintaan]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaan'] = $_POST['no_permintaan'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$dept = $this->m_homerawatjalan->get_dept_id("LABORATORIUM");
			$insert['dept_id'] = $dept['dept_id'];

			$val = $_POST['data'];
			$result = $this->m_homerawatjalan->insert_permintaan($insert);
			if($result){
				foreach ($val as $key) {
					$ins['obat_id'] = $key[1];
					$ins['obat_detail_id'] = $key[0];
					$ins['jumlah_request'] =  $key[9];
					$ins['obat_permintaan_id'] = $result;

					$elny = $this->m_homerawatjalan->insert_detail_permintaan($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function get_obat_retur()
	{
		$katakunci = $_POST['katakunci'];
		$dept = $this->m_homerawatjalan->get_dept_id("LABORATORIUM")['dept_id'];
		$result = $this->m_homebersalin->get_obat_farmasi_unit($katakunci, $dept);

		header('Content-Type: application/json');
	 	echo json_encode($result); 
	}

	public function submit_retur_bersalin()
	{
		$this->form_validation->set_rules('no_returdept', 'Nomor Retur', 'required|trim|xss_clean|is_unique[obat_retur_dept.no_returdept]');
		$this->form_validation->set_message('is_unique', 'Nomor Retur sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['status'] = 'belum diterima';
			$insert['no_returdept'] = $_POST['no_returdept'];
			$dept = $this->m_homerawatjalan->get_dept_id("LABORATORIUM");
			$insert['dept_id'] = $dept['dept_id'];
			$insert['petugas_input'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['keterangan'] = $_POST['keterangan'];
			$tgl =  DateTime::createFromFormat('d/m/Y H:i',$_POST['waktu']);
			$insert['waktu'] = $tgl->format('Y-m-d H:i');

			$val = $_POST['data'];

			$id = $this->m_homerawatjalan->submit_retur_dept($insert);
			if ($id) {
				foreach ($val as $key) {
					$ins['retur_dept_id'] = $id;
					$ins['obat_detail_id'] = $key[0];
					$ins['jumlah'] = $key[8];

					$res = $this->m_homerawatjalan->insert_detail_returdept($ins);
					//ubah stok di gudang dan unit, yang ubah gudang bukan unit :D
				}

				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_permintaan_barangunit($value='')
	{
		$this->form_validation->set_rules('no_permintaanbarang', 'nomor permitaan', 'required|trim|xss_clean|is_unique[barang_permintaan.no_permintaanbarang]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaanbarang'] = $_POST['no_permintaanbarang'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$dept = $this->m_homerawatjalan->get_dept_id("LABORATORIUM");
			$insert['dept_id'] = $dept['dept_id'];

			$val = $_POST['data'];
			$result = $this->m_homerawatjalan->insert_permintaanbarang($insert);
			if($result){
				foreach ($val as $key) {
					$ins['barang_id'] = $key[8];
					$ins['barang_stok_id'] = $key[7];
					$ins['jumlah_request'] =  $key[9];
					$ins['barang_permintaan_id'] = $result;

					$elny = $this->m_homerawatjalan->insert_detail_permintaanbarang($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function search_hasil(){
		$search = $_POST['search'];
		$dept_id = $this->m_homelab->get_dept_id('LABORATORIUM')['dept_id'];
		$result = $this->m_homelab->search_hasil($search, $dept_id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function save_tindakan_penunjang(){
		$dept_id = $this->m_homelab->get_dept_id('LABORATORIUM')['dept_id'];
		$kode = $this->m_homelab->get_kodetindakan('LAB');

		$invip['nama_tindakan'] = $_POST['nama_tindakan'];
		$invip['dept_id'] = $dept_id;
		$invip['jenis_tarif'] = 'VIP';
		$invip['kategori'] = $_POST['kategori'];
		$invip['js'] = $_POST['js_vip'];
		$invip['jp'] = $_POST['jp_vip'];
		$invip['bakhp'] = $_POST['bakhp_vip'];
		$invip['grup'] = $_POST['grup'];
		$invip['kode_tindakan'] = $kode;

		$inuta['nama_tindakan'] = $_POST['nama_tindakan'];
		$inuta['dept_id'] = $dept_id;
		$inuta['jenis_tarif'] = 'Utama';
		$inuta['kategori'] = $_POST['kategori'];
		$inuta['js'] = $_POST['js_utama'];
		$inuta['jp'] = $_POST['jp_utama'];
		$inuta['bakhp'] = $_POST['bakhp_utama'];
		$inuta['grup'] = $_POST['grup'];
		$inuta['kode_tindakan'] = $kode;

		$inki['nama_tindakan'] = $_POST['nama_tindakan'];
		$inki['dept_id'] = $dept_id;
		$inki['jenis_tarif'] = 'I';
		$inki['kategori'] = $_POST['kategori'];
		$inki['js'] = $_POST['js_I'];
		$inki['jp'] = $_POST['jp_I'];
		$inki['bakhp'] = $_POST['bakhp_I'];
		$inki['grup'] = $_POST['grup'];
		$inki['kode_tindakan'] = $kode;

		$inkii['nama_tindakan'] = $_POST['nama_tindakan'];
		$inkii['dept_id'] = $dept_id;
		$inkii['jenis_tarif'] = 'II';
		$inkii['kategori'] = $_POST['kategori'];
		$inkii['js'] = $_POST['js_II'];
		$inkii['jp'] = $_POST['jp_II'];
		$inkii['bakhp'] = $_POST['bakhp_II'];
		$inkii['grup'] = $_POST['grup'];
		$inkii['kode_tindakan'] = $kode;

		$inkiii['nama_tindakan'] = $_POST['nama_tindakan'];
		$inkiii['dept_id'] = $dept_id;
		$inkiii['jenis_tarif'] = 'III';
		$inkiii['kategori'] = $_POST['kategori'];
		$inkiii['js'] = $_POST['js_III'];
		$inkiii['jp'] = $_POST['jp_III'];
		$inkiii['bakhp'] = $_POST['bakhp_III'];
		$inkiii['grup'] = $_POST['grup'];
		$inkiii['kode_tindakan'] = $kode;

		$this->m_homelab->save_tindakan_penunjang($invip);
		$this->m_homelab->save_tindakan_penunjang($inuta);
		$this->m_homelab->save_tindakan_penunjang($inki);
		$this->m_homelab->save_tindakan_penunjang($inkii);
		$this->m_homelab->save_tindakan_penunjang($inkiii);

		header('Content-Type: application/json');
		echo json_encode($kode);
	}

	public function print_jaspel()
	{
		$mulai = $this->fdate_db($this->input->post('start'));
		$sampai = $this->fdate_db($this->input->post('end'));
		$carabayar = $this->input->post('cara_bayar');
		$paramedis = $this->input->post('id_petugas_jaspel');
		$dept = $this->m_homerawatjalan->get_dept_id("LABORATORIUM")['dept_id'];

		$result = $this->m_homelab->search_jasapelayanan($dept, $mulai, $sampai, $carabayar, $paramedis);
		
		$index=0;
		$final = [];
		foreach ($result as $data) {
			$data['paramedis_lain'] = "";
			$data['tanggal'] = substr($data['tanggal'],0,10);
			$final[$index] = $data;
			$index++;
		}

		$data['jaspel'] = $final;
		$data['nama_dept'] = 'LABORATORIUM';
		$data['awal'] = $this->input->post('start');
		$data['akhir'] = $this->input->post('end');

		$this->load->view('bersalin/excel_jaspel', $data);
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}

	public function get_detail_inventori($id)
	{
		$dept = $this->m_homerawatjalan->get_dept_id("LABORATORIUM");
		$dept_id = $dept['dept_id'];
		$res = $this->m_gudangbarang->get_detail_inventori($id, $dept_id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function input_in_outbarang($value='')
	{
		$insert['barang_detail_id'] = $_POST['barang_detail_id'];
		$tgl = DateTime::createFromFormat('d/m/Y H:i', $_POST['tanggal']);
		$insert['tanggal'] = $tgl->format('Y-m-d H:i');
		$insert['is_out'] = $_POST['is_out'];
		$insert['jumlah'] = $_POST['jumlah'];
		$insert['keterangan'] = $_POST['keterangan'];
		$dept = $this->m_homerawatjalan->get_dept_id("LABORATORIUM");
		$insert['barang_dept_id'] = $dept['dept_id'];

		$res = $this->m_gudangbarang->input_in_out($insert);
		if ($res) {
			$ins['barang_detail_id'] = $_POST['barang_detail_id'];
			$ins['dept_id'] = $dept['dept_id'];
			$ins['stok'] = $_POST['sisa'];
			$ins['tanggal_stok'] = date('Y-m-d H:i:s');
			$ins['keterangan_stok'] = "IN - OUT";

			$res = $this->m_gudangbarang->input_riwayat_out($ins);
			if ($res) {
				$message = "true";
			}else{
				$message = "false";
			}
		}else{
			$message = "false";
		}

		header('Content-Type: application/json');
	 	echo(json_encode($message));
	}

	public function excel_barang_unit()
	{
		$dept_id = $this->m_homerawatjalan->get_dept_id("LABORATORIUM")['dept_id'];
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($dept_id);
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];
		$data['awal'] = date('d F Y');

		$this->load->view('bersalin/excel_inventori',$data);
	}

	public function submit_filter_farmasi()
	{
		$dept = $this->m_homerawatjalan->get_dept_id("LABORATORIUM");

		if (isset($_POST['filterby'])) {
			$filterby = $_POST['filterby'];
			$filterval = $_POST['valfilter'];
			
			$result = $this->m_homerawatjalan->filter_farmasi($filterby,$filterval, $dept['dept_id']);			
		}else if (isset($_POST['expired'])) {
			$filterby = $_POST['expired'];
			$now = date('Y-m-d');
			$result = $this->m_homerawatjalan->filter_farmasi_expired($filterby,$now,$dept['dept_id']);
		}

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function search_icd9(){
		$search = $_POST['search'];
		$result = $this->m_homelab->search_icd9($search);

		header('Content-Type: application/json');
	 	echo json_encode($result);	
	}

	public function cetak_hasil($penunjang_id, $dept){
		$data['content'] = 'detailpenunjang';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		
		$data['dpasien'] = $this->m_homelab->get_detailperiksa($penunjang_id);
		$data['dperiksa'] = $result = $this->m_homelab->get_allperiksa($penunjang_id);
		$data['dept'] = $dept;

		// print_r($data['dperiksa']);die;

		$this->load->helper('pdf_helper');
	  	$this->load->view('cetak_penunjang', $data);

	}
	
}
