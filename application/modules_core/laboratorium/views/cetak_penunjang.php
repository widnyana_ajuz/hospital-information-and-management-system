<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->setPageOrientation('P');
$title = "RUMAH SAKIT DATU SANGGUL RANTAU";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData('logo-login-backup.png', '11px', $title, "Hasil ".$dept." \n".date('Y'));
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();

	//begin the content rendering
	$cetakhasil = '';
	$no = 1;
	if(!empty($dperiksa)){
		foreach ($dperiksa as $val) {
			$cetakhasil .= '
				<tr>
					<td width="5%">'.$no.'.</td>
					<td>'.$val['nama_tindakan'].'</td>
					<td>'.$val['hasilp'].'</td>
					<td>'.$val['nilaip'].'</td>
					<td>'.$val['keteranganp'].'</td>
				</tr>';

			$no++;
		}	
	}


    $content = '
    <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-top: solid 1px #000;
			border-left: solid 1px #000;
			border-right: solid 1px #000;
			border-bottom: solid 1px #000;
			font-size: 8pt;
			vertical-align:middle;
			line-height:20px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			font-weight: bold;
		}
		.center {
			text-align:center;
		}
		.italic {
			font-style:italic;
		}
    </style>
	<!-- Hasil Evaluasi Kelas -->
	<div class="hasil_kelas">
	<br>
	<table>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="50%" colspan="2">
			<h3>Info Pasien</h3>
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="50%" colspan="2">
			<h3>Info Visit</h3>
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			No Rekam Medis
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dpasien['rm_id'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Jenis Kelamin
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dpasien['jenis_kelamin'].'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Nama Pasien
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dpasien['nama'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Tanggal Lahir
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.date("d F Y", strtotime($dpasien['tanggal_lahir'])).'
			</td>
		</tr>
		<tr>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Alamat
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dpasien['alamat_skr'].'
			</td>
			<td style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			Pengirim
			</td>
			<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none" width="25%">
			'.$dpasien['nama_petugas'].'
			</td>
		</tr>
	</table>

	<br><br><br><br>
	<table border="1">
		<tr>
			<th width="5%">No.</th>
			<th width="30%">Jenis Pemeriksaan</th>
			<th width="25%">Hasil</th>
			<th>Nilai Normal</th>
			<th>Keterangan</th>
		</tr> '.$cetakhasil.'
	</table>

	<br><br><br><br><br><br><br><br>
	
	<table>
		<tbody>
			<tr>
				<td style="border-top:none;border-right:none;border-left:none;border-bottom:none">
				</td>
				<td  style="border-top:none;border-right:none;border-left:none;border-bottom:none;text-align:center">
					Petugas <br>
					<br><br>
					( .................................... )
				</td>
			</tr>
		</tbody>
	</table>
	';

ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('Laporan_Invoice.pdf', 'I');
?>

<!-- <table class="table" id="hasil-evaluasi-dosen">
			<tbody>
				<tr>
					<td width="40%" style="text-align:center"><b>Nama Tindakan</b></td>
					<td width="20%" style="text-align:center"><b>Unit</b></td>
					<td width="20%" style="text-align:center"><b>Waktu</b></td>
					<td width="20%" style="text-align:center"><b>Subtotal</b></td>
				</tr>
				'.$baris.'
				<tr>
					<td colspan="3" style="text-align:right"><b>Total Tagihan Pendukung</b></td>
					<td style="text-align:right">'.number_format($total,2,".",",").'</td>
				</tr>
			</tbody>
		</table> -->
