<?php

class m_login extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

     // login validtion
    function login_validation($params) {


        $sql = "SELECT a.user_id, a.user_name,a.petugas_id, d.role_id, d.role_default_url, e.portal_id, a.user_st
            FROM user a
            INNER JOIN petugas b ON b.petugas_id = a.petugas_id
            LEFT JOIN my_role_user c ON c.user_id = a.user_id
            LEFT JOIN my_role d ON d.role_id = c.role_id
            LEFT JOIN my_portal e ON e.portal_id = d.portal_id
            WHERE a.user_name like ? AND a.password = ? and b.status = 'Aktif'";

        $query = $this->db->query($sql, $params);

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return array();
        }
    }

}
