<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $this->session->userdata('page_title'); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>metronic/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>metronic/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>metronic/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>metronic/assets/css/style-responsive.css" rel="stylesheet">

</head>


<body class="login">

    <div class="logo">
        <a href="#">
            <img src="<?php echo base_url()?>metronic/assets/admin/pages/media/logo-login-backup.png" class="logo-defalt" alt="logo"style="width: 10%;"/>
        </a>
    </div>

    <div class="menu-toggler sidebar-toggler">
    </div>
    
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" action="<?php echo base_url()?>login/operator/login_validation" method="post">
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <!-- <label class="control-label visible-ie8 visible-ie9">Username</label> -->
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="user_name" required/>
                </div>
            </div>
            <div class="form-group">
                <!-- <label class="control-label visible-ie8 visible-ie9">Password</label> -->
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="user_pass" required/>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn blue pull-right">
                Login <i class="m-icon-swapright m-icon-white"></i>
                </button>
            </div>
        </form>
    </div>

    <div class="copyright">
         2015 &copy; Developed by MAHATALA MEDIS<br/>Themes by Metronic - Admin Dashboard Template.
    </div>


<!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>metronic/assets//js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>metronic/assets//js/bootstrap.min.js" type="text/javascript"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php echo base_url();?>metronic/assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch([
                "<?php echo base_url();?>metronic/assets/admin/pages/media/bg/1.jpg",
                "<?php echo base_url();?>metronic/assets/admin/pages/media/bg/2.jpg",
                "<?php echo base_url();?>metronic/assets/admin/pages/media/bg/3.jpg",
                "<?php echo base_url();?>metronic/assets/admin/pages/media/bg/4.jpg"
                ], {
                  fade: 1000,
                  duration: 5000
            }
            );
    </script>

   
</body>

<!-- END BODY -->
</html>
</div>
