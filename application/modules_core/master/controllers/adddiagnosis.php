<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adddiagnosis extends CI_Controller {
	public function index($page = 0)
	{
		// load template

		$data['content'] = 'adddiagnosis/add';
		$data['page_title'] = 'Tambah diagnosis';
		$this->session->set_userdata($data);
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}

}
