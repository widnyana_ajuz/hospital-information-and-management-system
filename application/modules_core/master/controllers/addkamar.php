<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

//class Homebersalin extends Application_Base {
class Addkamar extends Operator_base {
	protected $dept_id;
	function __construct() {
		parent:: __construct();

		$this->load->model("m_addkamar");
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'addkamar/add';
		$data['page_title']  = "Master Kamar";
		$this->session->set_userdata($data);
		$data['departemen'] = $this->m_addkamar->get_departemen();
		$this->load->view('base/operator/template', $data);
	}


	public function save_kamar()
	{
		$insert['nama_kamar'] =  $_POST['nama_kamar'];
		$insert['kelas_kamar'] =  $_POST['kelas_kamar'];
		$insert['tarif_kamar'] =  $_POST['tarif_kamar'];
		
		$insert['dept_id'] =  $_POST['unit'];	

		$hasil = $this->m_addkamar->insert_kamar($insert);

		$kamar_id = $this->m_addkamar->get_maxkamarid();
		$bed = Array();
		for ($i=0; $i < $_POST['counter']; $i++) { 

			$z = $i+1;
			$bed[$i]['kamar_id'] =  $kamar_id['kamar_id'];
			$bed[$i]['nama_bed'] =  $_POST['bed'.$z.''];
			$bed[$i]['deskripsi'] = $_POST['desk'.$z.''];
			$bed[$i]['is_dipakai'] = 0;
			$hasil1 = $this->m_addkamar->insert_bed($bed[$i]);
		}
		
		header('Content-Type:application/json');
		echo(json_encode($insert));
	}

}
