<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Addkaryawan extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_addkaryawan");
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'addkaryawan/add';
				$data['page_title'] = 'Tambah Karyawan';
		$this->session->set_userdata($data);
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$data['departemen'] = $this->m_addkaryawan->get_departemen();
		//$data['jabatan_master'] = $this->m_addkaryawan->get_jabatan();
		$data['role'] = $this->m_addkaryawan->get_role();
		$data['kualifikasi'] = $this->m_addkaryawan->get_kualifikasi();
		$this->load->view('base/operator/template', $data);
	}
	
	
	public function save_karyawan()
	{
		$insert['nip'] =  $_POST['nip'];
		$insert['nama_petugas'] =  $_POST['nama_petugas'];
		$insert['jabatan_id'] =  $_POST['jabatan_id'];
		$insert['kualifikasi_id'] = $_POST['kualifikasi_id'];
		$insert['dept_id'] =  $_POST['dept_id'];
		$insert['status'] =  'Aktif';

		$maxid =  $this->m_addkaryawan->get_maxid($insert['dept_id']);
		$maxidsb = intval(substr($maxid['petugas_id'], 4))+1;
		$yearnow = date('y');

        if (strlen($maxidsb) == '1') {
            $maxidsb = '000' . $maxidsb;
        } elseif (strlen($maxidsb) == '2') {
            $maxidsb = '00' . $maxidsb;
        } elseif (strlen($maxidsb) == '3') {
            $maxidsb = '0' . $maxidsb;
        } else {
            $maxidsb = strlen($maxidsb);
        }	

		$insert['petugas_id'] =  $_POST['dept_id'] . $yearnow . $maxidsb ;
		$hasil = $this->m_addkaryawan->insert_karyawan($insert);

		$status_akun = $_POST['status_akun'];
		if ($hasil and $status_akun === 'ya') {
			$akun['user_name'] =  $_POST['user_name'];
			$akun['password'] =  sha1($_POST['password']);
			$akun['petugas_id'] = $insert['petugas_id'];
			$hasil1 = $this->m_addkaryawan->insert_user($akun);
			$role = array(
				'role_id' => $_POST['role'],
				'user_id' => $hasil1
				);

			$this->m_addkaryawan->add_my_role_user($role);
		}

		header('Content-Type:application/json');
		echo(json_encode($insert));
	}

	public function get_jabatan($jenis)
	{
		$hasil = $this->m_addkaryawan->get_jabatan($jenis);
		header('Content-Type:application/json');
		echo(json_encode($hasil));
	}
}
