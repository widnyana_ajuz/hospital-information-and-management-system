<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );
class Addpenyedia extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_addpenyedia");
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'addpenyedia/add';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}
	public function save_penyedia()
	{
		
		foreach ($_POST as $value) 
		{
			$insert = $value;
		}

		$hasil = $this->m_addpenyedia->insert_penyedia($insert);
		
		header('Content-Type:application/json');
		echo(json_encode($insert));
	}

}
