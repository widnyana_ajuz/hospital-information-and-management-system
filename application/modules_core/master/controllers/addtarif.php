<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Addtarif extends Operator_base {
	protected $dept_id;

	function __construct(){
		parent:: __construct();
		$this->load->model('m_tarif');
		$data['page_title'] = "Data Tarif";
		$this->dept_id = $this->m_tarif->get_dept_id('UNIT PENDAPATAN')['dept_id'];
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$this->session->set_userdata($data);

		// load template
		$data['content'] = 'addtarif/add';
		$data['kategori'] = $this->m_tarif->get_kategori();
		$data['javascript'] = 'addtarif/j_add';
		$this->load->view('base/operator/template', $data);
	}

	public function search_icd()
	{
		$katakunci = $_POST['search'];
		$res = $this->m_tarif->search_icd($katakunci);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function add_tindakan()
	{
		$data = $_POST['data'];

		$ins = array(
			'nama_tindakan' => $_POST['nama_tindakan'],
			'jenis_unit' => $_POST['jenis_unit'],
			'kat_id' => $_POST['kat_id'],
			'status' => $_POST['status'],
			'icd9cm' => $_POST['icd9cm']
		);

		$id = $this->m_tarif->add_tindakan($ins);

		if ($id) {
			foreach ($data as $value) {
				$insdetail = array(
					'tindakan_id' => $id,
					'kode_tindakan' => $value[7],
					'kelas' => $value[0],
					'bakhp' => $value[12],
					'js' => $value[13],
					'jp' => $value[14]
				);
				$this->m_tarif->add_tindakan_detail($insdetail);
				$result = array(
					'message' => 'Data berhasil disimpan',
					'error' => 'n'
				);
			}
		}else {
			$result = array(
				'message' => 'Gagal menyimpan data',
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

}
