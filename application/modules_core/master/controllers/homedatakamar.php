<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

//class Homebersalin extends Application_Base {
class Homedatakamar extends Operator_base {
	protected $dept_id;
	function __construct() {
		parent:: __construct();
		$this->load->model("m_homedatakamar");
	}


	public function index($departemen = '')
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'datakamar/home';
		$data['page_title']  = "Master Kamar";
		$this->session->set_userdata($data);
		//$data['menu_view'] = $this->menu();
		if ($departemen == '') {
			$data['antrian'] = $this->m_homedatakamar->daftar_kamar();
			$data['filter'] = 'no';
		} else {
			//$departemen = 'Asisten Dokter';
			$departemen = str_replace('%20', ' ', $departemen);

			$data['antrian'] = $this->m_homedatakamar->get_all_kamar_by_departemen($departemen);
			$data['filter'] = 'yes';
		}

		//$data['antrian'] = $this->m_homedatakaryawan->daftar_karyawan();
		$data['departemen'] = $this->m_homedatakamar->get_departemen();

		$data['departemen_master'] = $this->m_homedatakamar->get_departemen();

		$this->load->view('base/operator/template', $data);

	}

	public function get_kamar(){
		$result = $this->m_homedatakamar->daftar_kamar();

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}


	public function hapus_kamar($id){
		//$this->m_homedatakamar->hapus_kamar($id);
		redirect('master/homedatakamar');
	}


	public function save_kamar(){
    	//get data
		$insert['nama_kamar'] =  $_POST['nama_kamar'];
		$insert['kelas_kamar'] =  $_POST['kelas_kamar'];
		$insert['tarif_kamar'] =  $_POST['tarif_kamar'];
		$kamar_id = $_POST['kamar_id'];

		$insert['dept_id'] =  $_POST['unit'];

		$hasil = $this->m_homedatakamar->update_kamar($kamar_id,$insert);


		$updetbed =Array();

		$bed = Array();

		for ($i=0; $i <= $_POST['counter']; $i++) {

			$z = $i+1;
			if ($i<$_POST['count']) {
				$updetbed[$i]['bed_id'] = $_POST['bedid'.$z.''];
				$updetbed[$i]['kamar_id'] =  $kamar_id;
				$updetbed[$i]['nama_bed'] =  $_POST['bed'.$z.''];
				$updetbed[$i]['deskripsi'] = $_POST['desk'.$z.''];
				$hasil1 = $this->m_homedatakamar->update_bed($updetbed[$i]);
			}else{
				if ($_POST['bed'.$z.''] == '') {
					continue;
				}
				$bed[$i]['kamar_id'] =  $kamar_id;
				$bed[$i]['nama_bed'] =  $_POST['bed'.$z.''];
				$bed[$i]['deskripsi'] = $_POST['desk'.$z.''];
				$bed[$i]['is_dipakai'] = 0;
				$hasil2 = $this->m_homedatakamar->insert_bed($bed[$i]);
			}
		}

		// for ($i=($_POST['count']+1); $i < $_POST['counter']; $i++) {

		// 	$z = $i+1;
		// 	$bed[$i]['kamar_id'] =  $kamar_id['kamar_id'];
		// 	$bed[$i]['nama_bed'] =  $_POST['bed'.$z.''];
		// 	$bed[$i]['deskripsi'] = $_POST['desk'.$z.''];
		// 	$bed[$i]['is_dipakai'] = 0;
		// 	$hasil2 = $this->m_addkamar->insert_bed($bed[$i]);
		// }

		header('Content-Type:application/json');
		echo(json_encode($insert));

	}

	public function ambil_bed($value){
		$bed = $this->m_homedatakamar->ambil_bed($value);
		header('Content-Type: application/json');
	 	echo json_encode($bed);
	}

	public function excelkamar($value='')
	{
		$data['antrian'] = $this->m_homedatakamar->daftar_kamar();
		$this->load->view('master/datakamar/excelkamar', $data);
	}



}
