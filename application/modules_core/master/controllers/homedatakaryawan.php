<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

//class Homebersalin extends Application_Base {
class Homedatakaryawan extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_homedatakaryawan");
		$this->load->model("m_addkaryawan");
	}

	
	public function index($jabatan = '')
	{

		//echo $this->uri->segment(1);die;
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['page_title'] = 'Data karyawan';
		$this->session->set_userdata($data);

		$data['content'] = 'datakaryawan/home';
		//$data['menu_view'] = $this->menu();	
		
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		if ($jabatan == '') {
			$data['antrian'] = $this->m_homedatakaryawan->daftar_karyawan();
			$data['filter'] = 'no';
		} else {
			//$jabatan = 'Asisten Dokter';
			$jabatan = str_replace('%20', ' ', $jabatan);
		
			$data['antrian'] = $this->m_homedatakaryawan->get_all_karyawan_by_position($jabatan);
			$data['filter'] = 'yes';
		}

		$data['role'] = $this->m_addkaryawan->get_role();
		$data['departemen'] = $this->m_homedatakaryawan->get_departemen();
		$data['jabatan_master'] = $this->m_homedatakaryawan->get_jabatan();
		$data['kualifikasi'] = $this->m_homedatakaryawan->get_kualifikasi();
		$this->load->view('base/operator/template', $data);
		
	}

	public function get_karyawan(){
		$result = $this->m_homedatakaryawan->daftar_karyawan();

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function excellkarwayan()
	{
		$jabatan = $this->uri->segment(4);
		//echo($jabatan);die;
		if ($jabatan == '') {
			$data['antrian'] = $this->m_homedatakaryawan->daftar_karyawan();
			$data['filter'] = 'no';
		} else {
			//$jabatan = 'Asisten Dokter';
			$jabatan = str_replace('%20', ' ', $jabatan);
		
			$data['antrian'] = $this->m_homedatakaryawan->get_all_karyawan_by_position($jabatan);
			$data['filter'] = 'yes';
		}

		$this->load->view('master/datakaryawan/datakaryawan', $data);
	}

	
	public function download_template() {
		$data['petugas'] 	= $this->m_homedatakaryawan->daftar_karyawan();
		$this->load->view('master/stok-karyawan-template',$data);		
	}


	public function import_mega_opname() {


		$this->load->library('excel_reader');
		$config['upload_path'] = './temp_upload/';
        $config['allowed_types'] = 'xls|xlsx';
 		ini_set('memory_limit', '-1');
        $this->load->library('upload', $config);
	 	//$this->form_validation->set_rules('tgl_pilih', 'Tgl Opname', 'required|trim|xss_clean');

 	 	if ( ! $this->upload->do_upload())
	        {
		        echo $error = $this->upload->display_errors();
	        }
	    else 
	        {
			
            $data = array('error' => false);
            $upload_data = $this->upload->data();
 			
            $this->load->library('excel_reader');
            $this->excel_reader->setOutputEncoding('CP1251');
 
            $file =  $upload_data['full_path'];
		    $this->excel_reader->read($file);

           
            error_reporting(E_ALL ^ E_NOTICE);
 
            // Sheet 1
            
	       
	        
            $data = $this->excel_reader->sheets[0] ;
                    $dataexcel = Array();
           	
            for ($i = 2; $i <= $data['numRows']; $i++) {
 				
                if($data['cells'][$i][1] != '') {
                	$dataexcel[$i-1]['nama_petugas'] 			= str_replace("\'", "", $data['cells'][$i][1]);
                	$data[$i-1]['id_jabt'] = str_replace("\'", "", $data['cells'][$i][2]);
                	$data[$i-1]['id_dept'] = str_replace("\'", "", $data['cells'][$i][4]);
		          			
		            $data[$i-1]['kualifikasi']	 		= str_replace("\'", "", $data['cells'][$i][3]);
		            //$dataexcel[$i-1]['dept_id']	 		= str_replace("\'", "", $data['cells'][$i][4]);
		            $dataexcel[$i-1]['nip']	 		= str_replace("\'", "", $data['cells'][$i][5]);
		            $dataexcel[$i-1]['status']	   = 'Aktif';


		            $jabtid =  $this->m_homedatakaryawan->get_jabatan_id($data[$i-1]['id_jabt']);
		            $dataexcel[$i-1]['jabatan_id'] = $jabtid['jabatan_id'];
		            $dept_id = $this->m_homedatakaryawan->get_departemen_id($data[$i-1]['id_dept']);
		            $dataexcel[$i-1]['dept_id']	 = $dept_id['dept_id'];
		            $kualifikasi = $this->m_homedatakaryawan->get_kualifikasi_id($data[$i-1]['kualifikasi']);
		            $dataexcel[$i-1]['kualifikasi_id'] = $kualifikasi['kualifikasi_id'];

		            $maxid =  $this->m_homedatakaryawan->get_maxid($dataexcel[$i-1]['dept_id']);
					$maxidsb = intval(substr($maxid['petugas_id'], 4))+1;
					$yearnow = date('y');

			        if (strlen($maxidsb) == '1') {
			            $maxidsb = '000' . $maxidsb;
			        } elseif (strlen($maxidsb) == '2') {
			            $maxidsb = '00' . $maxidsb;
			        } elseif (strlen($maxidsb) == '3') {
			            $maxidsb = '0' . $maxidsb;
			        } else {
			            $maxidsb = strlen($maxidsb);
			        }	

					$dataexcel[$i-1]['petugas_id']  =  $dataexcel[$i-1]['dept_id'] . $yearnow . $maxidsb ;
     
		            $this->m_homedatakaryawan->insert_karyawan($dataexcel[$i-1]);
                }         



            //echo '<pre>'; print_r($dataexcel[$i-1]); die; 
            }

            
            
           delete_files($upload_data['file_path']);
           redirect('master/homedatakaryawan');

       	}
        //updated last imported data

		// echo $data['message']; die;
        //$this->load->view('hasil', $data);		
	}

	public function hapus_karyawan($id){
		$this->m_homedatakaryawan->hapus_karyawan($id);
		redirect('master/homedatakaryawan');
		//header('Content-Type: application/json');
	 	//echo json_encode($result);
	}

	public function ubah_status(){

		foreach ($_POST as $value) {
			$insert =  $value;
		}
		$data = array('status'=>$insert['status'],'petugas_id'=>$insert['petugas_id']);
		
		$this->m_homedatakaryawan->ubah_status($data);
		
		//header('Content-Type: application/json');
	 	//echo json_encode($result);
	}

	public function save_karyawan(){
    	//get data
		

		$id = $_POST['petugas_id'];
		$insert['nama_petugas'] =  $_POST['nama_petugas'];
		$insert['jabatan_id'] =  $_POST['jabatan_id'];
		$insert['dept_id'] =  $_POST['dept_id'];
		$insert['kualifikasi_id'] = $_POST['kualifikasi_id'];
		//$insert['role_id'] = $_POST['role_id'];
		$passid =  $_POST['userid'];
		$akun['user_name'] =  $_POST['username'];
		$akun['petugas_id'] =  $id;
		$akunx['user_id'] =  $passid;
		$akunx['password_lama'] =  sha1($_POST['passwordlama']);
		$akun['password'] =  $_POST['passwordbarubuat'];
		$akunc['password_lama'] =  $_POST['passwordlama'];
		$password_reset = $_POST['passwordbaru_reset'];

		if ($akunx['password_lama']!='') {
			$cek = $this->m_homedatakaryawan->cek_password($akunx);
			if ($cek) {
				
				$akunbaru['password'] =  sha1($_POST['passwordbaru']);
				$akunbaru['user_name'] =  $akun['user_name'];
				$akunbaru['petugas_id'] =  $id;
				
				
				$password = $this->m_homedatakaryawan->update_password($passid, $akunbaru); 
				
				if($password){
					echo json_encode('true');
					$update = $this->m_homedatakaryawan->update_karyawan($id, $insert);	
					$update = $this->m_homedatakaryawan->update_role($passid, $_POST['role_id']);
					if($update)
						echo json_encode('true');					
					
				}else{
					
				};

			}else{
				
			};
		}


		if($akun['password'] !='') {
			$akun['password'] = sha1($akun['password']);
			$user = $this->m_homedatakaryawan->insert_user($akun);
			$newrole = array('user_id' => $passid, 'role_id' => $_POST['role_id'] );
			$update = $this->m_homedatakaryawan->insert_role($newrole);	
			if($user){
				$update = $this->m_homedatakaryawan->update_karyawan($id, $insert);	
				if($update){
					echo json_encode('true');
				}
				else{
					
				};
			}else{		
			};
		}

		if($akun['password'] == '' AND $akunc['password_lama'] == '' ){
			$akunbaru['user_name'] =  $akun['user_name'];

			$data = array('user_name'=>$akunbaru['user_name']);
			$user = $this->m_homedatakaryawan->update_password($passid, $data); 
			$asu = array('role_id' => $_POST['role_id']);
			$update = $this->m_homedatakaryawan->update_role($passid,$asu);
			if($user){

				$update = $this->m_homedatakaryawan->update_karyawan($id, $insert);	
					if($update)
						echo json_encode('true');
			}
			else{

			};
		};

		if ($password_reset != '') {
			$pass = sha1($password_reset);
			$asu = array('password' => $pass);
			$user = $this->m_homedatakaryawan->update_password($passid, $asu);
		}
    }

}
