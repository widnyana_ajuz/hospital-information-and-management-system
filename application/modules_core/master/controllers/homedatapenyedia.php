<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homedatapenyedia extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_homedatapenyedia");
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'datapenyedia/home';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$data['penyedia'] = $this->m_homedatapenyedia->get_penyedia();
		$this->load->view('base/operator/template', $data);
	}

	public function hapus_penyedia($id){
		$this->m_homedatapenyedia->hapus_penyedia($id);
		redirect('master/Homedatapenyedia');
		//header('Content-Type: application/json');
	 	//echo json_encode($result);
	}

	public function ubah_penyedia(){

		foreach ($_POST as $value) {
			$insert =  $value;
		}
		
		$this->m_homedatapenyedia->ubah_penyedia($insert);
		
		//header('Content-Type: application/json');
	 	//echo json_encode($result);
	}

	public function excelpenyedia()
	{
		$data['penyedia'] = $this->m_homedatapenyedia->get_penyedia();
		$this->load->view('master/datapenyedia/excelpenyedia', $data);
	}

}
