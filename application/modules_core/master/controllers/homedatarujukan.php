<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );
class Homedatarujukan extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_homedatarujukan");
	}
	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'datarujukan/home';
		$data['page_title']  = "Master Rujukan";
		$this->session->set_userdata($data);
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$data['rujukan'] = $this->m_homedatarujukan->get_rujukan();
		
		$this->load->view('base/operator/template', $data);
	}
	
	public function hapus_rujukan($id){
		$this->m_homedatarujukan->hapus_rujukan($id);
		redirect('master/Homedatarujukan');
		//header('Content-Type: application/json');
	 	//echo json_encode($result);
	}

	public function ubah_rujukan(){

		foreach ($_POST as $value) {
			$insert =  $value;
		}
		
		$this->m_homedatarujukan->ubah_rujukan($insert);
		
		//header('Content-Type: application/json');
	 	//echo json_encode($result);
	}

	public function excelrujukan($value='')
	{
		$data['rujukan'] = $this->m_homedatarujukan->get_rujukan();
		
		$this->load->view('master/datarujukan/excelrujukan', $data);
	}

}
