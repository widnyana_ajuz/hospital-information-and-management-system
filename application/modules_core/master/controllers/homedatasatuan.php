<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homedatasatuan extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_homedatasatuan");
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();

		$data['content'] = 'datasatuan/home';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$data['satuan'] = $this->m_homedatasatuan->get_satuan();
		$this->load->view('base/operator/template', $data);
		
	}

	public function hapus_satuan($id){
		$this->m_homedatasatuan->hapus_satuan($id);
		redirect('master/homedatasatuan');
		//header('Content-Type: application/json');
	 	//echo json_encode($result);
	}

	public function ubah_satuan(){

		foreach ($_POST as $value) {
			$insert =  $value;
		}
		
		$this->m_homedatasatuan->ubah_satuan($insert);
		
		//header('Content-Type: application/json');
	 	//echo json_encode($result);
	}

	public function excelsatuan($value='')
	{
		$data['satuan'] = $this->m_homedatasatuan->get_satuan();
		$this->load->view('master/datasatuan/excelsatuan', $data);
	}

}
