<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php');

class Homedatatarif extends Operator_base {
	protected $dept_id;

	function __construct(){
		parent:: __construct();
		$this->load->model('m_tarif');
		$data['page_title'] = "Data Tarif";
		$this->dept_id = $this->m_tarif->get_dept_id('UNIT PENDAPATAN')['dept_id'];
		$this->session->set_userdata($data);
	}

	public function index($jenis = '')
	{
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$this->session->set_userdata($data);

		$data['content'] = 'datatarif/home';
		$data['kategori'] = $this->m_tarif->get_kategori();

		if ($jenis == '') {
			$data['tarif'] = $this->m_tarif->get_all_tarif();
			$data['filter'] = 'no';
		} else {
			$jenis = str_replace('%20', ' ', $jenis);

			$data['tarif'] = $this->m_tarif->get_tarif_by_jenis($jenis);
			$data['filter'] = 'yes';
		}
		$data['jenis'] = $this->m_tarif->get_jenis_unit();
		$data['javascript'] = 'datatarif/j_tarif';

		$this->load->view('base/operator/template', $data);
	}

	//get tarif detail
	public function get_tarif_detail($id)
	{
		$result = $this->m_tarif->get_tarif_detail($id);

		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function search_icd()
	{
		$katakunci = $_POST['search'];
		$res = $this->m_tarif->search_icd($katakunci);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function update_status($id)
	{
		$data['status'] = $_POST['status'];
		$this->m_tarif->update_tindakan($id, $data);
	}

	public function update_tarif($id)
	{
		$data = $_POST['data'];
		$arr_ins = array(
			'nama_tindakan' => $_POST['nama_tindakan'],
			'jenis_unit' => $_POST['jenis_unit'],
			'kat_id' => $_POST['kat_id'],
			'status' => $_POST['status'],
			'icd9cm' => $_POST['icd9cm']
		);

		$res1 = $this->m_tarif->update_tindakan($id, $arr_ins);

		if ($res1) {
			foreach ($data as $value) {
				$id_detail = $value[15];
				$arr = array(
					'tindakan_id' => $id,
					'kode_tindakan' => $value[7],
					'bakhp' => $value[12],
					'js' => $value[13],
					'jp' => $value[14]
				);
				$res2 = $this->m_tarif->update_tindakan_detail($id_detail, $arr);
				$result = array(
					'message' => 'Data berhasil disimpan',
					'error' => 'n'
				);
			}
		}else {
			$result = array(
				'message' => 'Gagal menyimpan data',
				'error' => 'y'
			);
		}

		header('Content-Type:application/json');
		echo(json_encode($result));
	}

	public function exceltarif($value='')
	{
		$data['tarif'] = $this->m_tarif->get_all_tarif();
		$this->load->view('master/datatarif/exceltarif', $data);
	}


}
