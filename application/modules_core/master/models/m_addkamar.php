<?php
class m_addkamar extends CI_Model{
	public function insert_kamar($value)
    {
        //insert ke overview
        $query = $this->db->insert('master_kamar',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

    public function insert_bed($value)
    {
        //insert ke overview
        $query = $this->db->insert('master_bed',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

    public function get_departemen()
        {
            $sql = "SELECT * from master_dept";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->result_array();
            }else{
                return false;
            }
 		}
        
    

    public function get_maxkamarid()
        {
            $sql = "SELECT MAX(kamar_id) as kamar_id from master_kamar";
        
            $query = $this->db->query($sql);
            if ($query) {
                return $query->row_array();
            }else{
                return false;
            }
        }




 	

}
?>