<?php
class m_addkaryawan extends CI_Model{
	public function insert_karyawan($value)
    {
        //insert ke overview
        $query = $this->db->insert('petugas',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

    public function get_kualifikasi()
    {
        $sql = "SELECT * from master_kualifikasi";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->result_array();
            }else{
                return false;
            }
    }

    public function insert_user($value)
    {
        //insert ke overview
        $query = $this->db->insert('user',$value);
        if ($query) {
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    public function add_my_role_user($role)
    {
        $query = $this->db->insert('my_role_user',$role);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

    public function get_departemen()
        {
            $sql = "SELECT * from master_dept";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->result_array();
            }else{
                return false;
            }
 		}
        
    public function get_jabatan($jenis)
        {
            $sql = "SELECT * from master_jabatan where jenis LIKE '$jenis'";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->result_array();
            }else{
                return false;
            }
        }

    public function get_role()
    {
        $sql = "SELECT * from my_role";
        $query = $this->db->query($sql);
        if ($query) {
            return $query->result_array();
        }else{
            return false;
        }
    }

    public function get_maxid($value)
        {
            $sql = "SELECT MAX(petugas_id) as petugas_id from petugas where dept_id = '$value' ";
        
            $query = $this->db->query($sql);
            if ($query) {
                return $query->row_array();
            }else{
                return false;
            }
        }




 	

}
?>