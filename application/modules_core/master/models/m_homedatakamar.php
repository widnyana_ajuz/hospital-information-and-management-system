<?php
class m_homedatakamar extends CI_Model{
	
	 public function get_maxid($value)
        {
            $sql = "SELECT MAX(petugas_id) as petugas_id from petugas where dept_id = '$value' ";
        
            $query = $this->db->query($sql);
            if ($query) {
                return $query->row_array();
            }else{
                return false;
            }
        }
        
	public function get_all_kamar_by_departemen($departemen) {

		$new = strtolower($departemen);
				$sql = "SELECT nama_kamar, kelas_kamar, tarif_kamar, mk.kamar_id as kamar_id, nama_dept, mk.dept_id as dept_id, count(mb.bed_id) as jumlah_bed, IFNULL(sum(is_dipakai),0) as terpakai
                FROM master_dept md,  master_kamar mk LEFT JOIN master_bed mb on mb.kamar_id = mk.kamar_id
                WHERE md.dept_id = mk.dept_id AND md.nama_dept = '$new' GROUP BY mk.kamar_id ORDER BY mk.nama_kamar ASC"; //10 adalah dept_id poli umum

		//$query = $this->db->query($sql);
		
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function daftar_kamar(){
		$sql = "SELECT nama_kamar, kelas_kamar, tarif_kamar, mk.kamar_id as kamar_id, nama_dept, mk.dept_id as dept_id, count(mb.bed_id) as jumlah_bed, IFNULL(sum(is_dipakai),0) as terpakai
                FROM master_dept md,  master_kamar mk LEFT JOIN master_bed mb on mb.kamar_id = mk.kamar_id
                WHERE md.dept_id = mk.dept_id GROUP BY mk.kamar_id ORDER BY mk.nama_kamar ASC"; //10 adalah dept_id poli umum
				
				
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
	}

	public function hapus_kamar($id){
		$this->db->where('kamar_id',$id);
		$this->db->delete('master_kamar');
        $this->db->where('kamar_id',$id);
        $this->db->delete('master_bed');
	}

    public function ambil_bed($id){

        $sql = "SELECT bed_id, nama_bed, deskripsi from master_bed mb where kamar_id = '$id' "; //10 adalah dept_id poli umum
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;       
    }


	// public function get_all_kamar() {
	// 	$sql = "SELECT nama_petugas, p.petugas_id as petugas_id, spesialisasi_id, mj.nama_jabatan as jabatan, p.jabatan_id 
	// 			as jabt_id, nama_dept, p.dept_id as dept_id, nip, u.user_name as username, u.user_id as user_id, 
	// 			status, u.password as password  
	// 			FROM master_dept md, master_jabatan mj, petugas p LEFT JOIN user u on p.petugas_id = u.petugas_id
	// 			WHERE p.dept_id = md.dept_id
	// 			AND mj.jabatan_id = p.jabatan_id ORDER BY p.nama_petugas ASC"; $query = $this->db->query($sql);
	// 	if ($query->num_rows() > 0) {
	// 		return $query->result_array();
	// 	} else {
	// 		return array();
	// 	}
	// }
	
	public function get_departemen()
        {
            $sql = "SELECT * from master_dept where jenis like 'RAWAT INAP'";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->result_array();
            }else{
                return false;
            }
 		}


 	public function update_kamar($id, $data){
 		
        $this->db->where('kamar_id', $id);
        $update = $this->db->update('master_kamar', $data);

        if($update)
            return true;
        else
            return false;   
    }

    public function insert_kamar($value)
    {
        //insert ke overview
        $query = $this->db->insert('kamar_id',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

    public function update_bed($id){
 		
 		$this->db->where('bed_id', $id['bed_id']);
        $update = $this->db->update('master_bed', $id);

        if($update)
            return true;
        else
            return false;   
    }

     public function insert_bed($value)
    {
        //insert ke overview
        $query = $this->db->insert('master_bed',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }



}
?>