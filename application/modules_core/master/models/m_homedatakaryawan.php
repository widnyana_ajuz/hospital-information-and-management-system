<?php
class m_homedatakaryawan extends CI_Model{

    public function get_kualifikasi()
    {
        $sql = "SELECT * from master_kualifikasi";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->result_array();
            }else{
                return false;
            }
    }

	 public function get_maxid($value)
        {
            $sql = "SELECT MAX(petugas_id) as petugas_id from petugas where dept_id = '$value' ";
        
            $query = $this->db->query($sql);
            if ($query) {
                return $query->row_array();
            }else{
                return false;
            }
        }
        
	public function get_all_karyawan_by_position($jabatan) {

		$new = strtolower($jabatan);
				$sql = "SELECT nama_petugas, p.petugas_id as petugas_id, mj.nama_jabatan as jabatan, p.jabatan_id 
				as jabt_id, nama_dept, p.dept_id as dept_id, nip, u.user_name as username, u.user_id as user_id, 
				status, u.password as password, k.*, mj.jenis as jenis_karyawan, mr.role_id  
				FROM master_dept md, master_jabatan mj, master_kualifikasi k, petugas p 
                inner JOIN user u on p.petugas_id = u.petugas_id inner join my_role_user mr on mr.user_id = u.user_id
				WHERE p.dept_id = md.dept_id
				AND mj.jabatan_id = p.jabatan_id AND mj.nama_jabatan = '$jabatan' ORDER BY p.nama_petugas ASC"; //10 adalah dept_id poli umum
        $sql = "SELECT nama_petugas, p.petugas_id as petugas_id, mj.nama_jabatan as jabatan, p.jabatan_id 
                as jabt_id, nama_dept, p.dept_id as dept_id, nip, u.user_name as username, u.user_id as user_id, 
                status, u.password as password, mk.*, mj.jenis as jenis_karyawan, mr.role_id
                from petugas p left join user u on u.petugas_id = p.petugas_id left join my_role_user mr on mr.user_id = u.user_id
                left join master_jabatan mj on mj.jabatan_id = p.jabatan_id
                left join master_kualifikasi mk on mk.kualifikasi_id = p.kualifikasi_id
                left join master_dept mt on mt.dept_id = p.dept_id
                where mj.nama_jabatan like '$new' order by p.nama_petugas asc";
		$query = $this->db->query($sql);
		
		$result = $query->result_array();
		return $result;
	}

	public function daftar_karyawan(){
		$sql = "SELECT nama_petugas, p.petugas_id as petugas_id, mj.nama_jabatan as jabatan, p.jabatan_id 
				as jabt_id, nama_dept, p.dept_id as dept_id, nip, u.user_name as username, u.user_id as user_id, 
				status, u.password as password , k.*, mj.jenis as jenis_karyawan, mr.role_id 
				FROM master_dept md, master_jabatan mj, master_kualifikasi k, petugas p 
                LEFT JOIN user u on p.petugas_id = u.petugas_id
                left join my_role_user mr on mr.user_id = u.user_id
				WHERE p.dept_id = md.dept_id and k.kualifikasi_id = p.kualifikasi_id
				AND mj.jabatan_id = p.jabatan_id ORDER BY p.nama_petugas ASC"; //10 adalah dept_id poli umum
				
				
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
	}

	public function hapus_karyawan($id){
		$this->db->where('petugas_id',$id);
		$this->db->delete('petugas');
	}

	public function cek_password($value)
        {
        	$id = $value['user_id'];
        	$pass = $value['password_lama'];
        	
            $sql ="SELECT password FROM user where user_id ='$id' and password LIKE '$pass'";

            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                return true;
            }else{
                return false;
            }
 		}

 	public function get_jabatan_id($value)
        {
        	
            $sql ="SELECT jabatan_id FROM master_jabatan where nama_jabatan LIKE '$value'";
            $query = $this->db->query($sql);

            if ($query) {
                return $query->row_array();
            }else{
                return false;
            }
 		}

 	public function get_departemen_id($value)
        {
        	$sql ="SELECT dept_id FROM master_dept where nama_dept LIKE '$value'";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->row_array();
            }else{
                return false;
            }
 		}

	public function get_all_karyawan() {
		$sql = "SELECT nama_petugas, p.petugas_id as petugas_id, spesialisasi_id, mj.nama_jabatan as jabatan, p.jabatan_id 
				as jabt_id, nama_dept, p.dept_id as dept_id, nip, u.user_name as username, u.user_id as user_id, 
				status, u.password as password  
				FROM master_dept md, master_jabatan mj, petugas p LEFT JOIN user u on p.petugas_id = u.petugas_id
				WHERE p.dept_id = md.dept_id
				AND mj.jabatan_id = p.jabatan_id ORDER BY p.nama_petugas ASC"; 
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}
	
	public function get_departemen()
        {
            $sql = "SELECT * from master_dept";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->result_array();
            }else{
                return false;
            }
 		}

 	public function get_jabatan()
        {
            $sql = "SELECT * from master_jabatan";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->result_array();
            }else{
                return false;
            }
 		}

 	public function update_karyawan($id, $data){
 		
        $this->db->where('petugas_id', $id);
        $update = $this->db->update('petugas', $data);

        if($update)
            return true;
        else
            return false;   
    }

    public function insert_karyawan($value)
    {
        //insert ke overview
        $query = $this->db->insert('petugas',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

    public function update_password($id, $pass){
 		
 		$this->db->where('user_id', $id);
        $update = $this->db->update('user', $pass);

        if($update)
            return true;
        else
            return false;   
    }

    public function update_role($id, $role){
        
        $this->db->where('user_id', $id);
        $update = $this->db->update('my_role_user', $role);

        if($update)
            return true;
        else
            return false;   
    }

    public function ubah_status($value){
 		
 		$this->db->where('petugas_id', $value['petugas_id']);
        $update = $this->db->update('petugas', $value);

        if($update)
            return true;
        else
            return false;   
    }

     public function insert_user($value)
    {
        //insert ke overview
        $query = $this->db->insert('user',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

    public function insert_role($value)
    {
        //insert ke overview
        $query = $this->db->insert('my_role_user',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

     public function get_kualifikasi_id($value)
        {
            $sql ="SELECT kualifikasi_id FROM master_kualifikasi where kualifikasi LIKE '$value'";
            $query = $this->db->query($sql);
            if ($query) {
                return $query->row_array();
            }else{
                return false;
            }
        }



}
?>