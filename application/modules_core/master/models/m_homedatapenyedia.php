
<?php
class m_homedatapenyedia extends CI_Model{


	public function get_penyedia(){
		$sql = "SELECT penyedia_id, nama_penyedia, alamat from master_penyedia ORDER BY penyedia_id ASC"; //10 adalah dept_id poli umum
				
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function hapus_penyedia($id){
		$this->db->where('penyedia_id',$id);
		$this->db->delete('master_penyedia');
	}

	

    public function ubah_penyedia($value){
 		
 		$this->db->where('penyedia_id', $value['penyedia_id']);
        $update = $this->db->update('master_penyedia', $value);

        if($update)
            return true;
        else
            return false;   
    }


}
?>