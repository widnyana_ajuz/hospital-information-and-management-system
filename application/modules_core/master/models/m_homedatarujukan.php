
<?php
class m_homedatarujukan extends CI_Model{


	public function get_rujukan(){
		$sql = "SELECT rs_id, nama_rs, alamat, grup from master_rumah_sakit mrs ORDER BY nama_rs ASC"; //10 adalah dept_id poli umum
				
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function hapus_rujukan($id){
		$this->db->where('rs_id',$id);
		$this->db->delete('master_rumah_sakit');
	}

	

    public function ubah_rujukan($value){
 		
 		$this->db->where('rs_id', $value['rs_id']);
        $update = $this->db->update('master_rumah_sakit', $value);

        if($update)
            return true;
        else
            return false;   
    }

}
?>