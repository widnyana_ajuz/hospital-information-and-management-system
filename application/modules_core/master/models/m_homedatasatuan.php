<?php
class m_homedatasatuan extends CI_Model{


	public function get_satuan(){
		$sql = "SELECT satuan_id, satuan, keterangan from obat_satuan ORDER BY satuan ASC"; //10 adalah dept_id poli umum
				
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function hapus_satuan($id){
		$this->db->where('satuan_id',$id);
		$this->db->delete('obat_satuan');
	}

	

    public function ubah_satuan($value){
 		
 		$this->db->where('satuan_id', $value['satuan_id']);
        $update = $this->db->update('obat_satuan', $value);

        if($update)
            return true;
        else
            return false;   
    }


}
?>