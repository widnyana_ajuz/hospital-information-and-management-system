<?php
  class M_tarif extends CI_Model
  {
    function __construct(){}

    public function get_dept_id($nama)
    {
      $sql = "SELECT dept_id from master_dept WHERE nama_dept LIKE '$nama'";
      $res = $this->db->query($sql);
      if ($res){
        return $res->row_array();
      }
      else {
        return false;
      }
    }

    public function get_all_tarif()
    {
      $sql = "SELECT DISTINCT(mt.tindakan_id) AS id, nama_tindakan, jenis_unit, mtk.keterangan AS kategori, mt.status AS status, mt.icd9cm AS id_icd, mi9.keterangan AS icd9cm, mi9.kode AS kodeicd,
        k3.js+k3.jp+k3.bakhp AS tarif3,
        k2.js+k2.jp+k2.bakhp as tarif2,
        k1.js+k1.jp+k1.bakhp AS tarif1,
        ku.js+ku.jp+ku.bakhp AS tarif_utama,
        kv.js+kv.jp+ku.bakhp AS tarif_vip
        FROM master_tindakan mt LEFT JOIN master_tindakan_detail mtd ON mt.tindakan_id=mtd.tindakan_id
        LEFT JOIN master_icd9cm mi9 ON mt.icd9cm=mi9.id
          LEFT JOIN master_tindakan_kategori mtk ON mt.kat_id=mtk.kat_id
          LEFT JOIN master_tindakan_detail k3 ON k3.tindakan_id=mt.tindakan_id AND k3.kelas='Kelas III'
          LEFT JOIN master_tindakan_detail k2 ON k2.tindakan_id=mt.tindakan_id AND k2.kelas='Kelas II'
          LEFT JOIN master_tindakan_detail k1 ON k1.tindakan_id=mt.tindakan_id AND k1.kelas='Kelas I'
          LEFT JOIN master_tindakan_detail ku ON ku.tindakan_id=mt.tindakan_id AND ku.kelas='Kelas Utama'
          LEFT JOIN master_tindakan_detail kv ON kv.tindakan_id=mt.tindakan_id AND kv.kelas='Kelas VIP'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_tarif_by_jenis($jenis)
    {
      $sql = "SELECT DISTINCT(mt.tindakan_id) AS id, nama_tindakan, jenis_unit, mtk.keterangan AS kategori, mt.status AS status, mt.icd9cm AS id_icd, mi9.keterangan AS icd9cm, mi9.kode AS kodeicd,
        k3.js+k3.jp+k3.bakhp AS tarif3,
        k2.js+k2.jp+k2.bakhp as tarif2,
        k1.js+k1.jp+k1.bakhp AS tarif1,
        ku.js+ku.jp+ku.bakhp AS tarif_utama,
        kv.js+kv.jp+ku.bakhp AS tarif_vip
        FROM master_tindakan mt LEFT JOIN master_tindakan_detail mtd ON mt.tindakan_id=mtd.tindakan_id
        LEFT JOIN master_icd9cm mi9 ON mt.icd9cm=mi9.id
          LEFT JOIN master_tindakan_kategori mtk ON mt.kat_id=mtk.kat_id
          LEFT JOIN master_tindakan_detail k3 ON k3.tindakan_id=mt.tindakan_id AND k3.kelas='Kelas III'
          LEFT JOIN master_tindakan_detail k2 ON k2.tindakan_id=mt.tindakan_id AND k2.kelas='Kelas II'
          LEFT JOIN master_tindakan_detail k1 ON k1.tindakan_id=mt.tindakan_id AND k1.kelas='Kelas I'
          LEFT JOIN master_tindakan_detail ku ON ku.tindakan_id=mt.tindakan_id AND ku.kelas='Kelas Utama'
          LEFT JOIN master_tindakan_detail kv ON kv.tindakan_id=mt.tindakan_id AND kv.kelas='Kelas VIP'
        WHERE mt.jenis_unit='$jenis'";

      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_tarif_detail($id)
    {
      $sql = "SELECT * FROM master_tindakan_detail WHERE tindakan_id=$id";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_jenis_unit()
    {
      $sql = "SELECT DISTINCT(jenis_unit) FROM master_tindakan";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_kategori()
    {
      $sql = "SELECT * FROM master_tindakan_kategori";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function search_icd($katakunci)
    {
      $sql = "SELECT * FROM master_icd9cm WHERE keterangan LIKE '%$katakunci%' OR kode LIKE '%$katakunci%'";
      $res = $this->db->query($sql);
      if ($res->num_rows() >0) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function update_tindakan($id, $data)
    {
      $this->db->where('tindakan_id', $id);
      $res = $this->db->update('master_tindakan', $data);
      if ($res) {
        return true;
      }else {
        return false;
      }
    }

    public function update_tindakan_detail($id, $data)
    {
      $this->db->where('detail_id', $id);
      $res = $this->db->update('master_tindakan_detail', $data);
      if ($res) {
        return true;
      }else {
        return false;
      }
    }

    public function add_tindakan($insert)
    {
      $result =  $this->db->insert('master_tindakan', $insert);
			if ($result) {
				return $this->db->insert_id();
			}else{
				return false;
			}
    }

    public function add_tindakan_detail($insert)
    {
      $result = $this->db->insert('master_tindakan_detail', $insert);
      if ($result) {
        return $this->db->insert_id();
      }else {
        return false;
      }
    }

  }
?>
