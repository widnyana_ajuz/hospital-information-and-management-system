<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>master/homedatakamar">DATA KAMAR</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>master/addkamar">TAMBAH KAMAR</a>
		<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
			<a href="<?php echo base_url() ?>master/homedatakamar" class="btn btn-danger pull-right" style="border-radius:0px">Kembali</a>		

		</div>
	</li>
</div>

 
<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
		<div style="padding-top:10px"></div>
						
		<div class="dropdown" style="margin-left:10px;width:98.5%;">
	        <div id="titleInformasi" style=" color:white">Tambah Kamar</div>
	    </div>
		
		<br>
		
		
		<form class="form-horizontal" role="form" method="POST" id="submitKamar">
			<div class="informasi">
				<div class="form-group">
					<label class="control-label col-md-3">Nama Kamar</label>
					<div class="col-md-3">
						<input type="text" id="nama" class="form-control" name="nama" placeholder="Nama Kamar">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Kelas Kamar</label>
					<div class="col-md-3">
						<select class="form-control" id="klsKamar" name="klsKamar">
							<option selected value="">Pilih</option>
							<option value="KELAS I">Kelas I</option>
							<option value="KELAS II">Kelas II</option>
							<option value="KELAS III">Kelas III</option>
							<option value="KELAS VIP">Kelas VIP</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Unit</label>
					<div class="col-md-3">
						<select class="form-control" id="depKamar" name="depKamar">
							<option selected value="">Pilih</option>
							<?php
	    			 			foreach ($departemen as $d) {
									echo '<option value="'.$d['dept_id'].'">'.$d['nama_dept'].'</option>';
	    			 			}

    			 			?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Tarif Kamar</label>
					<div class="col-md-3">
						<input type="text" id="tarifKamar" class="form-control" name="tarifKamar" placeholder="Tarif Kamar">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Jumlah Bed</label>
					<div class="col-md-2">
						<input type="text" id="jmlhBed" class="form-control" name="jmlhBed" disabled>
					</div>
					<div class="col-md-1">
						<button class="btn btn-info addbedkamar" >Tambah</button>
					</div>
				</div>
			</div>
			<div class="form-group">
        		<div class="portlet-body" style="margin: 0px 30px 0px 30px">
				
					<table class="table table-striped table-bordered table-hover table-responsive" id="tabelBed">
						<thead>
							<tr class="info" >
								<th  style="text-align:center"> Nama Bed </th>
								<th  style="text-align:center"> Deskripsi Bed </th>
								<th  style="text-align:center" width="30"> Hapus </th>
							</tr>
						</thead>
						<tbody id="addbed">
							
						</tbody>
					</table>
				</div>
			</div>

			<br>
			<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
			<div style="margin-left:80%">
				<span style="padding:0px 10px 0px 10px;">
					<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
					<button class="btn btn-success">SIMPAN</button> 
				</span>
			</div>
			<br>
		</form>
		
	
</div>

							

<script type="text/javascript">
	$(document).ready( function(){
			var d = $('#addbed').size() - 1;
			$('#jmlhBed').val(0);

			$('.addbedkamar').on('click',function(e){
				e.preventDefault();
				d++;
				tambahBed('#addbed','.addbedkamar',d);
				$('#jmlhBed').val(d);

			});

			$('table').on('click', 'a[class="removeRow"]', function(e) {
				e.preventDefault();		
				d--;
				$('#jmlhBed').val(d);
		
			});




			$('#submitKamar').submit(function (e) {
				e.preventDefault();
				var item = {};
			    // var number = 1;
			    // item[number] = {};
			    var tbody = $('#addbed');

				item['nama_kamar'] = $('#nama').val();//localStorage.getItem('visit_id');//$('#visit_id').val();			
				item['kelas_kamar'] = $('#klsKamar').find('option:selected').val();
				//item[number]['jenis_karyawan'] = $('#jnsK').find('option:selected').val();
				item['unit'] = $('#depKamar').find('option:selected').val();
				//item[number]['kualifikasi'] = $('#kwal').val();
				item['tarif_kamar'] = $('#tarifKamar').val();


				for (var i = 1; i <= d; i++) {
					item['bed'+i+''] = $('#bed'+i+'').val();
					item['desk'+i+''] = $('#desk'+i+'').val();

				};
				item['counter'] = d;
				//item[number]['username'] = $('#username').val();
				//item[number]['password'] = $('#password').val();
				//item[number]['akses'] = $('#akses').find('option:selected').val();

				//item[number]['kat_id'] = $('#kategori').find('option:selected').val();
				// console.log(item);

				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/addkamar/save_kamar",
					success: function (data) {
						console.log(data);
						//$('#namaTindakan').find('option:selected').val();
						//$('#visit_id').val('');
						$('#nama').val('');
						$('#jmlhBed').val(0);
						$('#tarifKamar').val('');
						$("#klsKamar option[value='']").attr("selected", "selected");
						$("#depKamar option[value='']").attr("selected", "selected");
						for (var i = d; i > 0; i--) {
							$('#bed'+i+'').closest('tr').remove();
						};
						d=0;
						var notification = new NotificationFx({
							message : '<p>DATA BERHASIL DITAMBAHKAN</p>',
							layout : 'growl',
							effect : 'genie',
							type : 'notice', // notice, warning or error
							onClose : function() {
								bttn.disabled = false;
							}
						});

						// show the notification
						notification.show();
					},error:function(data){
						console.log(data);
					}
				});

		});
	});

</script