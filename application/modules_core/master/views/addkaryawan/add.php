<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>master/homedatakaryawan">DATA KARYAWAN</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>master/addkaryawan">TAMBAH KARYAWAN</a>
		<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
			<a href="<?php echo base_url() ?>master/homedatakaryawan" class="btn btn-danger pull-right" style="border-radius:0px">Kembali</a>		
		</div>
	</li>
	
</div>

 
<div class="navigation1" style="min-height:800px;border-radius:5px;" >
		<div style="padding-top:10px"></div>
						
		<div class="dropdown" id="bt1" style="margin-left:10px;width:98.5%;">
            <div id="titleInformasi" style=" color:white">Informasi Umum</div>
            <div class="btnBawah" style="color:white"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		</div>
		<br>
		<div class="informasi" id="ibt1">
			<form class="form-horizontal" role="form" method="POST" id="submitKaryawan">
					<div class="form-group">
						<label class="control-label col-md-3">NIP</label>
	    			 	<div class="col-md-2" style="margin-left:-40px;">
								<input type="text" class="form-control" id="nipPegawai" name="nipPegawai" placeholder="NIP">	
		        		</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Nama</label>
	    			 	<div class="col-md-2" style="margin-left:-40px;">
								<input type="text" class="form-control" id="namaPegawai" name="namaPegawai" placeholder="Nama Pegawai" >	
		        		</div>
					</div>

					<div class="form-group">
	    			 	<label class="control-label col-md-3">Jenis Karyawan</label>
	    			 	<div class="col-md-2" style="margin-left:-40px;">
	    			 		<select class="form-control" id="jnsK" name="jnsK">
	    			 			<option value='' selected>Pilih</option>	
	    			 			<option value="MEDIS">Tenaga Medis</option>
	    			 			<option value="NON_MEDIS">Tenaga Non Medis</option>
	    			 		</select>
	    			 	</div>
					</div>
					
					<div class="form-group">
	    			 	<label class="control-label col-md-3">Jabatan</label>
	    			 	<div class="col-md-2" style="margin-left:-40px;">
	    			 		<select class="form-control" id="jbtan" name="jbtan">
	    			 			<option value='' selected>Pilih</option>	
	    			 			
	    			 		</select>	
		        		</div>
		        	</div>

		        	<div class="form-group">
		        		<label class="control-label col-md-3">Kualifikasi Pendidikan </label>
		        		<div class="col-md-2" style="margin-left:-40px;">
	    			 		<select class="form-control" id="kwal" name="kwal">
	    			 			<option value='' selected>Pilih</option>	
	    			 			<?php  
	    			 				foreach ($kualifikasi as $data) {
	    			 					echo '<option value="'.$data['kualifikasi_id'].'">'.$data['kualifikasi'].'</option>';
	    			 				}
	    			 			?>
	    			 		</select>	
		        		</div>
	        		</div>

					<div class="form-group">
	    			 	<label class="control-label col-md-3">Unit</label>
	    			 	<div class="col-md-2" style="margin-left:-40px;">
	    			 		<select class="form-control" id="dep" name="dep">
	    			 			<option value='' selected>Pilih</option>	
	    			 			<?php
		    			 			foreach ($departemen as $d) {
										echo '<option value="'.$d['dept_id'].'">'.$d['nama_dept'].'</option>';
		    			 			}

	    			 			?>	
	    			 		</select>	
		        		</div>
					</div>
			
			<br>
		
		<div class="dropdown" id="bt2" style="margin-left:-60px;width:104%;">
            <div id="titleInformasi" style=" color:white">Informasi Akun</div>
            
		</div>
		
		<br>
					<div class="form-group">
						<label class="control-label col-md-3">Buat Akun ?</label>
						<div class="col-md-2" style="margin-left:-40px;">
							<select class="form-control" id="akun" name="akun">
						    		<option value='' selected>Pilih</option>	
	    			 				<option value="ya">YA</option>	
						    		<option value="tidak">TIDAK</option>	
						    </select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Username</label>
						<div class="col-md-2" style="margin-left:-40px;">
							<input type="text" id="username" name="username" placeholder="Username" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Password</label>
						<div class="col-md-2" style="margin-left:-40px;">
							<input type="password" id="password" name="password" placeholder="Password" class="form-control" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Jenis Hak Akses</label>
						<div class="col-md-2" style="margin-left:-40px;">
							<select class="form-control" id="akses" name="akses">
						    	<option value='' selected>Pilih</option>	
	    			 			<?php  
	    			 				foreach ($role as $rol) {
	    			 					echo '<option value="'.$rol['role_id'].'">'.$rol['role_name'].'</option>';
	    			 				}
	    			 			?>		
						    </select>
						</div>
					</div>
				
				<br>
				<hr style="margin-bottom:-17px; margin-left:-60px; margin-right:10px">
				<div style="margin-left:80%">
					<span style="padding:0px 10px 0px 10px;">
						<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
						<button type="submit" class="btn btn-success">SIMPAN</button> 
					</span>
				</div>
				<br>
			</form>
		</div>
</div>

<script type="text/javascript">
	$(document).ready( function(){
		
		$("#bt1").click(function(){
			$("#ibt1").slideToggle();
		});


	});

</script>


							
<script type="text/javascript">
	$(document).ready(function(){

		$('#submitKaryawan').submit(function (e) {
				e.preventDefault();
				var item = {};

				item['nip'] = $('#nipPegawai').val();
				item['nama_petugas'] = $('#namaPegawai').val();
				item['jabatan_id'] = $('#jbtan').find('option:selected').val();
				item['dept_id'] = $('#dep').find('option:selected').val();

				if ($('#akun').find('option:selected').val() == 'ya') {
					if ($('#username').val() == '') {myAlert('masukkan Username');return false};
					if ($('#password').val() == '') {myAlert('masukkan Password');return false};
					if ($('#akses').val() == '') {myAlert('masukkan hak akses');return false};
				}
				item['status_akun'] = $('#akun').find('option:selected').val();
				item['role'] = $('#akses').val();
				item['user_name'] = $('#username').val();
				item['password'] = $('#password').val();
				item['jenis'] = $('#jnsK').val();
				item['kualifikasi_id'] = $('#kwal').val();
				if ($('#namaPegawai').val() == '') {myAlert('masukkan nama Pegawai');return false};
				if ($('#jnsK').val() == '') {myAlert('pilih jenis Karyawan');return false;};
				if (item['jabatan_id'] == '') {myAlert('pilih jabatan');return false;};
				if (item['dept_id'] == '') {myAlert('pilih departemen');return false;};
				if (item['kualifikasi_id'] == '') {myAlert('pilih kualifikasi');return false;};
				//console.log(item);return false;
				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/addkaryawan/save_karyawan",
					success: function (data) {
						console.log(data);
						$('#nipPegawai').val('');
						$('#namaPegawai').val('');
						$('#username').val('');
						$('#password').val('');					
						$("#jbtan option[value='']").attr("selected", "selected");
						$("#kwal option[value='']").attr("selected", "selected");
						$("#akun option[value='']").attr("selected", "selected");
						$("#dep option[value='']").attr("selected", "selected");
						$("#jnsK option[value='']").attr("selected", "selected");
						$("#akses option[value='']").attr("selected", "selected");

						myAlert('Data berhasil ditambahkan');
					},error:function(data){
						console.log(data);
					}
				});
		});

		$('#jnsK').on('change', function () {
			var jenis = $(this).val();
			get_jabatan(jenis);
		})
	});

function get_jabatan (jenis) {
	if (jenis == '') {$("#jbtan option[value='']").attr("selected", "selected");return false;};
	$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>master/addkaryawan/get_jabatan/"+jenis,
		success: function (data) {
			//$()
			console.log(data);
			$('#jbtan').empty();
			$('#jbtan').append('<option selected value="'+data[0]['jabatan_id']+'">'+data[0]['nama_jabatan']+'</option>');
			for (var i = 1; i < data.length; i++) {
				$('#jbtan').append('<option value="'+data[i]['jabatan_id']+'">'+data[i]['nama_jabatan']+'</option>');
			};
		},
		error: function (data) {
			console.log(data);
		}
	})
}
</script>