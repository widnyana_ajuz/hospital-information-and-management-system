<br>
<div class="title">
		<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>master/homedatarujukan"><?php echo strtoupper('Data Instansi Rujukan') ?></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>master/addrujukan"><?php echo strtoupper('Tambah Instansi Rujukan') ?></a>
			<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
				<a href="<?php echo base_url() ?>master/homedatarujukan" class="btn btn-danger pull-right" style="border-radius:0px">Kembali</a>		

			</div>
		</li>
		
</div>

 
<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
		<div style="padding-top:10px"></div>
						
		<div class="dropdown" style="margin-left:10px;width:98.5%;">
            <div id="titleInformasi" style=" color:white">Tambah Instansi Rujukan</div>
		</div>
		
		<br>
		
		
		<form class="form-horizontal" role="form"  method="POST" id="tambahRujukan" role="form">
			<div class="informasi">
				<div class="form-group">
					<label class="control-label col-md-2">Nama Instansi</label>
					<div class="col-md-3">
						<input type="text" id="nama" class="form-control" name="nama" placeholder="Nama Instansi">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Alamat</label>
					<div class="col-md-3">
						<textarea class="form-control" id="alamat" name="alamat" placeholder="Alamat"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Group</label>
					<div class="col-md-3">
						<select class="form-control" id="group" name="group">
							<option value="" selected>Pilih</option>
							<option value="Puskesmas">Puskesmas</option>
							<option value="Rumah Sakit">Rumah Sakit</option>
						</select>
					</div>
				</div>
			</div>
			<br>
			<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
			<div style="margin-left:80%">
				<span style="padding:0px 10px 0px 10px;">
					<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
					<button type="submit "class="btn btn-success">SIMPAN</button> 
				</span>
			</div>
			<br>
		</form>
		
</div>


<script type="text/javascript">
	$(document).ready(function(){

		$('#tambahRujukan').submit(function (e) {
				e.preventDefault();
				var item = {};
			    var number = 1;
			    item[number] = {};

				item[number]['nama_rs'] = $('#nama').val();
				item[number]['alamat'] = $('#alamat').val();
				item[number]['grup'] = $('#group').find('option:selected').val();
								

				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/addrujukan/save_rujukan",
					success: function (data) {
						console.log(data);
						//$('#namaTindakan').find('option:selected').val();
						//$('#visit_id').val('');
						$('#nama').val('');
						$('#alamat').val('');
						$("#group option[value='']").attr("selected", "selected");
						
						var notification = new NotificationFx({
							message : '<p>DATA BERHASIL DITAMBAHKAN</p>',
							layout : 'growl',
							effect : 'genie',
							type : 'notice', // notice, warning or error
							onClose : function() {
								bttn.disabled = false;
							}
						});

						// show the notification
						notification.show();
					},error:function(data){
						console.log(data);
					}
				});

		});
	});
</script>

							
