
<br>
<div class="title">
		<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>master/homedatasatuan">DATA SATUAN</a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>master/addsatuan">TAMBAH SATUAN</a>
			<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
				<a href="<?php echo base_url() ?>master/homedatasatuan" class="btn btn-danger pull-right" style="border-radius:0px">Kembali</a>		

			</div>
		</li>
	
</div>

 
<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
		<div style="padding-top:10px"></div>
						
		<div class="dropdown" style="margin-left:10px;width:98.5%;">
            <div id="titleInformasi" style=" color:white">Tambah Satuan</div>
        </div>
		
		<br>
		
		
		<form class="form-horizontal" role="form" method="POST" id="submitTambah">
			<div class="informasi">
				<div class="form-group">
					<label class="control-label col-md-1">Satuan</label>
					<div class="col-md-3">
						<input type="text" id="sat" class="form-control" name="sat" placeholder="Satuan">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-1">Keterangan</label>
					<div class="col-md-4">
						<textarea id="ket" class="form-control" name="ket" placeholder="Keterangan"></textarea> 
					</div>
				</div>
			</div>
			<br>
			<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
			<div style="margin-left:80%">
				<span style="padding:0px 10px 0px 10px;">
					<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
					<button class="btn btn-success" type="submit">SIMPAN</button> 
				</span>
			</div>
			<br>
		</form>
	
</div>

							
<script type="text/javascript">
	$(document).ready(function(){

		$('#submitTambah').submit(function (e) {
				e.preventDefault();
				var item = {};
			    var number = 1;
			    item[number] = {};

			    if ($('#ket').val() == '') {
			    	var ket = 'Tidak ada';
			    	item[number]['keterangan'] = ket;
			    }else{
			    	item[number]['keterangan'] = $('#ket').val();
			    };

				item[number]['satuan'] = $('#sat').val();
				

				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/addsatuan/save_satuan",
					success: function (data) {
						console.log(data);
						//$('#namaTindakan').find('option:selected').val();
						//$('#visit_id').val('');
						$('#sat').val('');
						$('#ket').val('');
					
						var notification = new NotificationFx({
							message : '<p>DATA BERHASIL DITAMBAHKAN</p>',
							layout : 'growl',
							effect : 'genie',
							type : 'notice', // notice, warning or error
							onClose : function() {
								bttn.disabled = false;
							}
						});

						// show the notification
						notification.show();
					},error:function(data){
						console.log(data);
					}
				});

		});
	});
</script>