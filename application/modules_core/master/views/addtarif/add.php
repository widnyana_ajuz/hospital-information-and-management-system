<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>master/homedatatarif">DATA TARIF</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>master/addtarif">TAMBAH TARIF</a>
		<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
			<a href="<?php echo base_url() ?>master/homedatatarif" class="btn btn-danger pull-right" style="border-radius:0px">Kembali</a>
		</div>
	</li>
</div>

<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
	<div style="padding-top:10px"></div>

	<div class="dropdown" style="margin-left:10px;width:98.5%;">
    <div id="titleInformasi" style=" color:white">Tambah Tarif Tindakan</div>
	</div>

	<br>

	<form class="form-horizontal" role="form" method="post" id="frmTarif">
		<div class="informasi">
			<div class="form-group">
				<label class="control-label col-md-2">Nama Tindakan</label>
				<div class="col-md-6">
					<input type="text" id="addNama" class="form-control" placeholder="Nama Tindakan" required>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Jenis Unit</label>
				<div class="col-md-3">
					<select class="form-control" id="addUnit" required>
						<option value='' selected>Pilih</option>
						<option value='POLIKLINIK'>POLIKLINIK</option>
						<option value='IGD'>IGD</option>
						<option value='RAWAT INAP'>RAWAT INAP</option>
						<option value='BERSALIN'>BERSALIN</option>
						<option value='PENUNJANG'>PENUNJANG</option>
						<option value='PENDUKUNG'>PENDUKUNG</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Kategori</label>
				<div class="col-md-3">
					<select class="form-control" id="addKat">
						<option value='' selected>Pilih</option>
						<?php
							if (isset($kategori)) {
								if (!empty($kategori)) {
									foreach ($kategori as $value) {
										echo '<option value='.$value['kat_id'].'>'.$value['keterangan'].'</option>';
									}
								}
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2"> ICD-9CM</label>
				<div class="col-md-1">
					<input type="text" style="cursor:pointer;background-color:white" id="addKode9" class="form-control isian" placeholder="Kode" data-toggle="modal" data-target="#searchICD" readonly>
				</div>
				<div class="col-md-5">
					<input type="text" style="cursor:pointer;background-color:white" id="addIcd9" class="form-control" name="edicd9" placeholder="ICD-9CM" data-toggle="modal" data-target="#searchICD" readonly>
				</div>
				<input type="hidden" id="id_icd9">
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Status</label>
				<div class="col-md-2">
					<select class="form-control" id="addStatus">
						<option value='ACTIVE' selected>ACTIVE</option>
						<option value='INACTIVE'>INACTIVE</option>
					</select>
				</div>
			</div>
		</div>

		<div class="form-group">
    	<div class="portlet-body" style="margin: 0px 30px 0px 30px">
				<table class="table table-striped table-bordered table-hover table-responsive">
					<thead>
						<tr class="info" >
							<th  style="text-align:center"> Jenis Tarif </th>
							<th  style="text-align:center"> Kode </th>
							<th  style="text-align:center"> BAKHP </th>
							<th  style="text-align:center"> JS </th>
							<th  style="text-align:center"> JP </th>
							<th  style="text-align:center"> Total </th>
							<th  style="text-align:center"> Clear </th>
						</tr>
					</thead>
					<tbody id="tbodyadd">
						<tr>
							<td>Kelas III</td>
							<td><input type="text" class="form-control kode" id="kode1"></td>
							<td><input type="text" class="form-control text-right bakhp" id="bakhp1" placeholder="0"></td>
							<td><input type="text" class="form-control text-right js" id="js1" placeholder="0"></td>
							<td><input type="text" class="form-control text-right jp" id="jp1" placeholder="0"></td>
							<td><input type="text" class="form-control text-right total" id="total1" placeholder="0" disabled></td>
							<td align="center">
								<a href="#" class="clearRow" id="clear1"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
								<input type="hidden" value="0" class="nilai_bakhp" id="val_bakhp1">
								<input type="hidden" value="0" class="nilai_js" id="val_js1">
								<input type="hidden" value="0" class="nilai_jp" id="val_jp1">
							</td>
						</tr>
						<tr>
							<td>Kelas II</td>
							<td><input type="text" class="form-control kode" id="kode2"></td>
							<td><input type="text" class="form-control text-right bakhp" id="bakhp2" placeholder="0"></td>
							<td><input type="text" class="form-control text-right js" id="js2" placeholder="0"></td>
							<td><input type="text" class="form-control text-right jp" id="jp2" placeholder="0"></td>
							<td><input type="text" class="form-control text-right total" id="total2" placeholder="0" disabled></td>
							<td align="center">
								<a href="#" class="clearRow" id="clear2"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
								<input type="hidden" value="0" class="nilai_bakhp" id="val_bakhp2">
								<input type="hidden" value="0" class="nilai_js" id="val_js2">
								<input type="hidden" value="0" class="nilai_jp" id="val_jp2">
							</td>
						</tr>
						<tr>
							<td>Kelas I</td>
							<td><input type="text" class="form-control kode" id="kode3"></td>
							<td><input type="text" class="form-control text-right bakhp" id="bakhp3" placeholder="0"></td>
							<td><input type="text" class="form-control text-right js" id="js3" placeholder="0"></td>
							<td><input type="text" class="form-control text-right jp" id="jp3" placeholder="0"></td>
							<td><input type="text" class="form-control text-right total" id="total3" placeholder="0" disabled></td>
							<td align="center">
								<a href="#" class="clearRow" id="clear3"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
								<input type="hidden" value="0" class="nilai_bakhp" id="val_bakhp3">
								<input type="hidden" value="0" class="nilai_js" id="val_js3">
								<input type="hidden" value="0" class="nilai_jp" id="val_jp3">
							</td>
						</tr>
						<tr>
							<td>Kelas Utama</td>
							<td><input type="text" class="form-control kode" id="kode4"></td>
							<td><input type="text" class="form-control text-right bakhp" id="bakhp4" placeholder="0"></td>
							<td><input type="text" class="form-control text-right js" id="js4" placeholder="0"></td>
							<td><input type="text" class="form-control text-right jp" id="jp4" placeholder="0"></td>
							<td><input type="text" class="form-control text-right total" id="total4" placeholder="0" disabled></td>
							<td align="center">
								<a href="#" class="clearRow" id="clear4"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
								<input type="hidden" value="0" class="nilai_bakhp" id="val_bakhp4">
								<input type="hidden" value="0" class="nilai_js" id="val_js4">
								<input type="hidden" value="0" class="nilai_jp" id="val_jp4">
							</td>
						</tr>
						<tr>
							<td>Kelas VIP</td>
							<td><input type="text" class="form-control kode" id="kode5"></td>
							<td><input type="text" class="form-control text-right bakhp" id="bakhp5" placeholder="0"></td>
							<td><input type="text" class="form-control text-right js" id="js5" placeholder="0"></td>
							<td><input type="text" class="form-control text-right jp" id="jp5" placeholder="0"></td>
							<td><input type="text" class="form-control text-right total" id="total5" placeholder="0" disabled></td>
							<td align="center">
								<a href="#" class="clearRow" id="clear5"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
								<input type="hidden" value="0" class="nilai_bakhp" id="val_bakhp5">
								<input type="hidden" value="0" class="nilai_js" id="val_js5">
								<input type="hidden" value="0" class="nilai_jp" id="val_jp5">
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<br>
			<hr style="margin-bottom:-17px; margin-left:30px; margin-right:30px">
			<div style="margin-left:80%">
				<span style="padding:0px 10px 0px 10px;">
					<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
					<button class="btn btn-success">SIMPAN</button>
				</span>
			</div>
			<br>
  	</div>
	</form>

	<div class="modal fade" id="searchICD" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h3 class="modal-title" id="myModalLabel">Pilih ICD-9CM</h3>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<form class="form-horizontal" role="form" method="post" id="cari_icd">
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Kata kunci"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div>
						</form>
					</div>
					<br>
					<div style="margin-left:5px; margin-right:5px;"><hr></div>
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover" id="tabelSearchICD">
							<thead>
								<tr class="info">
									<th width="30%;">Kode</th>
									<th>Keterangan</th>
									<th width="10%">Pilih</th>
								</tr>
							</thead>
							<tbody id="tbody_icd">
								<tr>
									<td style="text-align:center;" class="kosong" colspan="4">Cari ICD-9CM</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				</div>
			</div>
		</div>
	</div>
	
</div>
