<script type="text/javascript">
  $(document).ready(function(){
    //autonumeric
    jQuery(function($) {
      $('#bakhp1').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js1').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp1').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#bakhp2').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js2').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp2').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#bakhp3').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js3').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp3').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#bakhp4').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js4').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp4').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#bakhp5').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js5').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp5').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
    });

    //search icd9cm di popup
    $('#cari_icd').submit(function(e){
      e.preventDefault();
      var katakunci = {};
      katakunci['search'] = $('#katakunci').val();
      $.ajax({
        type: "POST",
        data: katakunci,
        url: "<?php echo base_url()?>master/addtarif/search_icd",
        success: function(data){
          $('#tbody_icd').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              $('#tbody_icd').append(
                '<tr>'+
                  '<td>' + data[i]['kode']+'</td>'+
                  '<td>' + data[i]['keterangan']+'</td>'+
                  '<td style="text-align:center; cursor:pointer;">'+
                    '<input type="hidden" class="id_icd" value="'+data[i]['id']+'">'+
                    '<a href="#" class ="add_icd"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a>'+
                  '</td>'+
                '</tr>'
              )
            };
          }
        },
        error: function(data){
          console.log(data);
        }
      })
    });

    //pilih icd9cm
    $('#tbody_icd').on('click', 'tr td a.add_icd', function(e){
      e.preventDefault();
      $('#addKode9').val($(this).closest('tr').find('td').eq(0).text());
      $('#addIcd9').val($(this).closest('tr').find('td').eq(1).text());
      $('#id_icd9').val($(this).closest('tr').find('td .id_icd').val());

      $('#searchICD').modal('hide');
    });

    //reset
    $('#tbodyadd').on('click', 'tr td a.clearRow', function(e){
      e.preventDefault();
      $(this).closest('tr').find('td .kode').val('');
      $(this).closest('tr').find('td .bakhp').val('0');
      $(this).closest('tr').find('td .js').val('0');
      $(this).closest('tr').find('td .jp').val('0');
      $(this).closest('tr').find('td .total').val('0');
    })

    var tdtotal;
    $('#tbodyadd').on('change', ' tr td .bakhp', function(e){
      e.preventDefault();
      var valbakhp = $(this).autoNumeric('get');
      var valjs = $(this).closest('tr').find('td .js').autoNumeric('get');
      var valjp = $(this).closest('tr').find('td .jp').autoNumeric('get');
      $(this).closest('tr').find('td .nilai_bakhp').val(valbakhp);
      tdtotal = $(this);
      hitung_total(valbakhp, valjs, valjp, tdtotal);
    })

    $('#tbodyadd').on('change', ' tr td .js', function(e){
      e.preventDefault();
      var valbakhp = $(this).closest('tr').find('td .bakhp').autoNumeric('get');
      var valjs = $(this).autoNumeric('get');
      var valjp = $(this).closest('tr').find('td .jp').autoNumeric('get');
      $(this).closest('tr').find('td .nilai_js').val(valjs);
      tdtotal = $(this);
      hitung_total(valbakhp, valjs, valjp, tdtotal);
    })

    $('#tbodyadd').on('change', ' tr td .jp', function(e){
      e.preventDefault();
      var valbakhp = $(this).closest('tr').find('td .bakhp').autoNumeric('get');
      var valjs = $(this).closest('tr').find('td .js').autoNumeric('get');
      var valjp = $(this).autoNumeric('get');
      $(this).closest('tr').find('td .nilai_jp').val(valjp);
      tdtotal = $(this);
      hitung_total(valbakhp, valjs, valjp, tdtotal);
    })

    //simpan data baru
    $('#frmTarif').submit(function(e){
      e.preventDefault();
      var item = {};
      item['nama_tindakan'] = $('#addNama').val();
      item['jenis_unit'] = $('#addUnit').val();
      item['kat_id'] = $('#addKat').val();
      item['status'] = $('#addStatus').val();
      item['icd9cm'] = $('#id_icd9').val();

      var data=[];
      $('#tbodyadd').find('tr').each(function(rowIndex, r){
        var cols = [];
        $(this).find('td').each(function(colIndex, c){
          cols.push(c.textContent);
        });
        $(this).find('td input').each(function (colIndex, c) {
          cols.push(c.value);
        });
        data.push(cols);
      })
      console.log(data);

      item['data'] = data;

      console.log('data :' + data);
      $.ajax({
        type: "POST",
        data: item,
        url: "<?php echo base_url()?>master/addtarif/add_tindakan",
        success: function(data){
          myAlert('DATA BERHASIL DITAMBAH');
          console.log(data);

          $('#addNama').val('');
          $('#addUnit').val('');
          $('#addKat').val('');
          $('#addStatus').val('ACTIVE');
          $('#id_icd9').val('');
          $('#addKode9').val('');
          $('#addIcd9').val('');
          $('.kode').val('');
          $('.bakhp').val('');
          $('.js').val('');
          $('.jp').val('');
          $('.total').val('');
        },
        error: function(data){
          console.log('error: ' + data)
        }
      })
    })
  })//end of document ready


  function hitung_total(valbakhp, valjs, valjp, tdtotal)
  {
    var valtotal = Number(valbakhp) + Number(valjs) + Number(valjp);
    tdtotal.closest('tr').find('td .total').val(valtotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
  }
</script>
