<?php
$title = 'DATA KAMAR';

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>DATA KAMAR</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong> RS DATU SANGGUL RANTAU</strong></td>
		</tr>
	</table>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead >
				            <tr class="info">
				                <th width="20">No</th>
				                <th>Nama Kamar</th>
				                <th>Kelas</th>
				                <th>Unit</th>  
				                <th>Jumlah Bed</th>
				                <th>Terpakai</th>
				                <th>Tarif</th>
				                    
				            </tr>
				        </thead>
						<tbody id="tableKam">
						        	<?php
										$i = 0;
										foreach ($antrian as $data) {
											$i++;
											
											echo '<tr>';
												echo '<td style="text-align:center">'.$i.'</td>';
									 			echo'<td>'.$data['nama_kamar'].'</td>';
									 			echo'<td>'.$data['kelas_kamar'].'</td>';
									 			echo'<td>'.$data['nama_dept'].'</td>';
									 			echo'<td>'.$data['jumlah_bed'].'</td>';
									 			echo'<td>'.$data['terpakai'].'</td>';
									 			echo'<td>'.$data['tarif_kamar'].'</td>';
									 			
									 		echo'</tr>';
										}
									?>
									<!-- BUAT HAPUS DIBAWAH -->
									<!-- <a href="'.base_url().'master/homedatakaryawan/hapus_karyawan/'.$data['petugas_id'].'" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a><input type="hidden" class="petugas_id_edit" value="'.$data['petugas_id'].'"><input type="hidden" class="dept_id_edit" value="'.$data['dept_id'].'"><input type="hidden" class="nip_id_edit" value="'.$data['nip'].'"><input type="hidden" class="jabt_id_edit" value="'.$data['jabt_id'].'"> -->
						</tbody>
				 
				       
		</table>
	</div>
</body>
</html>