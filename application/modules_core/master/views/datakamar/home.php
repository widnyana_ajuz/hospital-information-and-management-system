<br>
<div class="title">
		<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>master/homedatakamar">DATA KAMAR</a>
			<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
				<a href="<?php echo base_url() ?>master/addkamar" style="border-radius:0px;" class="btn btn-success pull-right">Tambah</a>
				
				<div class="btn-group pull-right">
				<div class="btn-group pull-right">
					<?php if ($filter == 'yes') : ?>
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						<?php echo str_replace("%20"," ",$this->uri->segment(4)); ?>&nbsp;<i class="fa fa-angle-down"></i>
						</button>								
					<?php else : ?>
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						Semua Unit <i class="fa fa-angle-down"></i>
						</button>
					<?php endif; ?>								
					<ul class="dropdown-menu pull-right" role="menu" style="overflow-y:scroll; height:300px;">
						<li><a href="<?php echo base_url() ?>master/homedatakamar/">SEMUA UNIT</a></li>
			 			<?php

    			 			foreach ($departemen_master as $d) {
								echo '<li><a href="'.base_url().'master/homedatakamar/index/'.$d['nama_dept'].'" id="'.$d['dept_id'].'">'.$d['nama_dept'].'</a></li>';
    			 			}

			 			?>
					</ul>
				</div>		

				</div>


			</div>
		</li>
		
		
	
</div>

	<div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" role="form" method="POST" id="submitUbah">
					<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    				<h3 class="modal-title" id="myModalLabel">Ubah</h3>
    			</div>
    			<div class="modal-body">
    				
						<div class="informasi">
							<div class="form-group">
								<label class="control-label col-md-3">Nama Kamar</label>
								<div class="col-md-4">
									<input type="text" id="ednama" class="form-control" name="ednama" placeholder="Nama Kamar">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Kelas Kamar</label>
								<div class="col-md-4">
									<select class="form-control" id="edklsKamar" name="edklsKamar">
										<option selected value="">Pilih</option>
										<option value="KELAS I">Kelas I</option>
										<option value="KELAS II">Kelas II</option>
										<option value="KELAS III">Kelas III</option>
										<option value="KELAS VIP">Kelas VIP</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Departemen</label>
								<div class="col-md-4">
									<select class="form-control" id="eddepKamar" name="eddepKamar">
										<option selected>Pilih</option>
										<?php

								 			foreach ($departemen as $d) {
													echo '<option value="'.$d['dept_id'].'">'.$d['nama_dept'].'</option>';
					    			 			}

							 			?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Tarif Kamar</label>
								<div class="col-md-4">
									<input type="text" id="edtarifKamar" class="form-control" name="edtarifKamar" placeholder="Tarif Kamar">
								</div>
									<input type="hidden" id="kamarid" class="form-control" name="kamarid" placeholder="Tarif Kamar">
									<input type="hidden" id="asli" class="form-control" name="asli" placeholder="Tarif Kamar">
								
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Jumlah Bed</label>
								<div class="col-md-4">
									<input type="text" id="edjmlhBed" class="form-control" name="edjmlhBed" disabled>
								</div>
								<div class="col-md-1">
									<button class="btn btn-info addbedkamar" >Tambah</button>
								</div>
							</div>
						</div>
							<div class="form-group">
			            		<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								
									<table class="table table-striped table-bordered table-hover table-responsive" id="tableBed">
										<thead>
											<tr class="info" >
												<th  style="text-align:center"> Nama Bed </th>
												<th  style="text-align:center"> Deskripsi Bed </th>
												<th  style="text-align:center"> Hapus </th>
											</tr>
										</thead>
										<tbody id="addbed">
											
										</tbody>
									</table>
								</div>
			            	</div>
				       
					
					
    			</div>
    			<div class="modal-footer">

			       		<button type="button" id="batalUbah" class="btn btn-danger" data-dismiss="modal">Batal</button>
			       		<button type="submit" class="btn btn-success" >Simpan</button>
		      	</div>
		      	</form>
			</div>
		</div>
	</div>
<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
		<div style="padding-top:10px"></div>
						
		<div class="informasi" id="infoDataKaryawan" style="margin-left:30px;margin-right:30px;">
			<form class="form-horizontal" role="form" method="post" action="<?php echo base_url() ?>master/homedatakamar/excelkamar">
				<div class="form-group">
					<table id="example" class="table table-striped table-bordered tableDT" cellspacing="0" style="margin-left:0px;" width="99.9%">
				        <thead >
				            <tr class="info">
				                <th width="20">No</th>
				                <th>Nama Kamar</th>
				                <th>Kelas</th>
				                <th>Unit</th>  
				                <th>Jumlah Bed</th>
				                <th>Terpakai</th>
				                <th>Tarif</th>
				                <th width="80">Action</th>      
				            </tr>
				        </thead>
						<tbody id="tableKam">
						        	<?php
										$i = 0;
										foreach ($antrian as $data) {
											$i++;
											
											echo '<tr>';
												echo '<td style="text-align:center">'.$i.'</td>';
									 			echo'<td>'.$data['nama_kamar'].'</td>';
									 			echo'<td>'.$data['kelas_kamar'].'</td>';
									 			echo'<td>'.$data['nama_dept'].'</td>';
									 			echo'<td>'.$data['jumlah_bed'].'</td>';
									 			echo'<td>'.$data['terpakai'].'</td>';
									 			echo'<td>'.number_format($data['tarif_kamar'], 0, '', '.').'</td>';
									 			echo'<td style="text-align:center">';
									 				echo '<a href="#ubah" class="edStat" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
									 				<input type="hidden" class="dept_id_edit" value="'.$data['dept_id'].'"><input type="hidden" class="kamar_id_edit" value="'.$data['kamar_id'].'">';
									 			echo'</td>';
									 		echo'</tr>';
										}
									?>
									<!-- BUAT HAPUS DIBAWAH -->
									<!-- <a href="'.base_url().'master/homedatakaryawan/hapus_karyawan/'.$data['petugas_id'].'" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a><input type="hidden" class="petugas_id_edit" value="'.$data['petugas_id'].'"><input type="hidden" class="dept_id_edit" value="'.$data['dept_id'].'"><input type="hidden" class="nip_id_edit" value="'.$data['nip'].'"><input type="hidden" class="jabt_id_edit" value="'.$data['jabt_id'].'"> -->
						</tbody>
				 
				       
				    </table>

				</div>
				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
				<br>
			</form>
		</div>
</div>

<script type="text/javascript">
	$(document).ready( function(){
			var d;
			var abc;
			// $('.addbedkamar').on('click',function(e){
			// 	e.preventDefault();
			// 	d++;
			// 	tambahBed('#addbed','.addbedkamar',d);
			// 	$('#edjmlhBed').val(d);

			// 	number++;
			//   	$(tr).find("td:first").text(number);

			// });
			
			$('table').on('click', 'a[class="removeRow"]', function(e) {
				e.preventDefault();		
				abc--;
				$('#edjmlhBed').val(abc);
		
			});
			//var rowCount = $('#tableBed tr').length + 1;
			

			$("#tableKam").on('click', 'tr td a.edStat', function (e) {

			d = $(this).closest('tr').find("td").eq(4).text();
			abc = d;

			$('#edjmlhBed').val(d);
			e.preventDefault();
			var kamar_id = $(this).closest('tr').find('td .kamar_id_edit').val();
			var nama_kamar = $(this).closest('tr').find('td').eq(1).text();
			var kelas_kamar = $(this).closest('tr').find('td').eq(2).text();
			var tarif_kamar = $(this).closest('tr').find('td').eq(6).text().toString().replace(/[^\d\,\-\ ]/g, '');
			$('#asli').val($(this).closest('tr').find("td").eq(4).text());
			
			var dept_id = $(this).closest('tr').find('td .dept_id_edit').val();
			//var password_id = $(this).closest('tr').find('td .password_edit').val();
			$.ajax({
					type: "POST",
					url: "<?php echo base_url()?>master/homedatakamar/ambil_bed/"+kamar_id,
					success: function (data) {
						console.log(data);
						$("#addbed").empty();
						for (var i = 0; i < data.length; i++) {
							var z = i+1;
							$("#addbed").append('<tr><td><input type="text" class="form-control" id="bed'+z+'" name="bed'+z+'" value="'+data[i]['nama_bed']+'" ></td><td><input type="text" class="form-control" id="desk'+z+'" name="desk'+z+'" value="'+data[i]['deskripsi']+'"></td><td style="text-align:center"><input type="hidden" id="bedid'+z+'" ></td></tr>');
							$("#bedid"+z).val(data[i]['bed_id']);
						};
						
			
					},error:function(data){
						console.log(data);
					}
			});

			

			$('#ednama').val(nama_kamar);
			$('#edtarifKamar').val(tarif_kamar);
			$('#kamarid').val(kamar_id);
			
			//$('#stat').val(status_pegawai);
			//$("#jnsK option[value='"+new_satuan_id+"']").attr("selected", "selected");
			$("#edklsKamar option[value='"+kelas_kamar+"']").attr("selected", "selected");
			$("#eddepKamar option[value='"+dept_id+"']").attr("selected", "selected");
			// $('#username').val(username_edit);
			// $('#password').val(password_edit);
			//$('#akses').val(new_h_jual);
			
			});

			// var number = 0;
			// $("#expense_table tbody tr").each(function(tr) {
			//   number++;
			//   $(tr).find("td:first").text(number);
			// });

			// $("#add_ExpenseRow").click(function () {
			//     myTr = $("#expense_table tbody tr:first").clone(true);
			//     myTr.find("input[type='text']").val('');
			//     myTr.show();
			//     myTr.appendTo("#expense_table tbody");
			//     rowCount++;

			//     myTr.find("td:first").text(rowCount);
			// })

			//deleting the row
			// $('#expense_table').delegate(".delRow", 'click', function () {
			//     //don't allow to delete if the last row
			//     if (rowCount == 1) {
			//         return;
			//     }
			//     var $tr = $(this).closest("tr");
			//     $tr.nextAll().find('td:first').text(function (i, text) {
			//         return parseInt($.trim(text)) - 1;
			//     })
			//     $tr.remove();
			//     rowCount--;
			// });

			$('.addbedkamar').on('click',function(e){
				// myTr = $("#tableBed tbody tr:first").clone(true);
			 //    myTr.find("input[type='text']").val('');
			 //    myTr.show();
			 e.preventDefault();
			    d++;
			    abc++;
			    tambahBed('#addbed','.addbedkamar',d);
			    $('#edjmlhBed').val(abc);
			    // myTr.appendTo("#tableBed tbody");
			 

			    // myTr.find("td:first").text(rowCount);

				// e.preventDefault();
				// d++;
				// tambahBed('#addbed','.addbedkamar',d);
				// $('#edjmlhBed').val(d);

				// number++;
			 //  	$(tr).find("td:first").text(number);

			});

			


			$('#submitUbah').submit(function (e) {
				e.preventDefault();
				var item = {};
			    // var number = 1;
			    // item[number] = {};
			    var tbody = $('#addbed');
			    var count = $('#asli').val();
				item['nama_kamar'] = $('#ednama').val();//localStorage.getItem('visit_id');//$('#visit_id').val();			
				item['kelas_kamar'] = $('#edklsKamar').find('option:selected').val();
				//item[number]['jenis_karyawan'] = $('#jnsK').find('option:selected').val();
				item['unit'] = $('#eddepKamar').find('option:selected').val();
				//item[number]['kualifikasi'] = $('#kwal').val();
				item['tarif_kamar'] = $('#edtarifKamar').val();
				item['kamar_id'] = $('#kamarid').val();

				for (var i = 1; i <= count; i++) {
					item['bedid'+i+''] = $('#bedid'+i).val()  
					item['bed'+i+''] = $('#bed'+i).val();
					item['desk'+i+''] = $('#desk'+i).val();

				};
				for (var xx = parseInt(count)+1; xx <= parseInt($('#edjmlhBed').val())+1; xx++) {
					if ($('#bed'+xx).val()==null) {
						item['bed'+xx+''] = '';
						item['desk'+xx+''] = '';
					}else{
						item['bed'+xx+''] = $('#bed'+xx).val();
						item['desk'+xx+''] = $('#desk'+xx).val();
					};
					
				};

				item['count'] = count;
				item['counter'] = $('#edjmlhBed').val();

				//item[number]['username'] = $('#username').val();
				//item[number]['password'] = $('#password').val();
				//item[number]['akses'] = $('#akses').find('option:selected').val();

				//item[number]['kat_id'] = $('#kategori').find('option:selected').val();
				// console.log(item);

				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/homedatakamar/save_kamar",
					success: function (data) {
						console.log(data);
						// //$('#namaTindakan').find('option:selected').val();
						// //$('#visit_id').val('');
						// $('#ednama').val('');
						// $('#edjmlhBed').val(0);
						// $('#edtarifKamar').val('');
						// $("#edklsKamar option[value='']").attr("selected", "selected");
						// $("#eddepKamar option[value='']").attr("selected", "selected");
						
						alert('DATA BERHASIL DIUBAH');
						window.location.replace("<?php echo base_url()?>master/homedatakamar");

						// show the notification
						notification.show();
					},error:function(data){
						console.log(data);
					}
				});

		});
	});

</script>
			