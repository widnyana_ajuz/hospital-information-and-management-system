
<br>
<div class="title">
		<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>master/homedatakaryawan"><?php echo strtoupper('Data Karyawan') ?></a>
			<div class="pull-right" style="margin-top:-8px;margin-right:0px;">

				<div class="modal fade" id="importexel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<form class="form-horizontal" role="form" method="POST" id="submitExcel" action="<?php echo base_url()?>master/homedatakaryawan/import_mega_opname" enctype="multipart/form-data">
									<div class="modal-header">
						   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
						   				<h3 class="modal-title" id="myModalLabel">Import Data Karyawan</h3>
						   			</div>
									<div class="modal-body">
										<div class="alert alert-warning" style="font-size:10pt">
											<h3>Perhatian</h3>
											<ol>
												<li> Halaman ini berguna untuk melakukan penambahan data karyawan secara massal </li><br>
												<li> Gunakan template yang sudah disediakan oleh sistem </li> <br>
												<li> Jangan merubah tata letak dalam template yang diisi </li> <br>
												<li> Proses tidak dapat dibatalkan </li><br><br>
											</ol>
											
											
											
											<a href="<?php echo base_url()?>master/homedatakaryawan/download_template" class="btn btn-info">Download Template Disini</a>
										</div>

										<div class="form-group">
											<div class="col-md-9" style="padding:5px;margin-left:30px; font-size:10pt">

												<input type="file" class="form-input" name="userfile" placeholder="Pilih File" value="<?php echo $this->session->flashdata('nama_kegiatan') ?>" required>

											</div>
										</div>

				       				</div>
					        		<br>
					        		<div class="modal-footer">
					        			<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					 			     	<button type="submit" class="btn btn-success">Simpan</button>
								    </div>
								</form>
							</div>
						</div>
					
				</div>

				<a href="#importexel" data-toggle="modal" style="border-radius:0px;" class="btn btn-info pull-right">Import Masal</a>
				
				<a href="<?php echo base_url() ?>master/addkaryawan" style="border-radius:0px;" class="btn btn-success pull-right">Tambah</a>

				<div class="btn-group pull-right">
				<div class="btn-group pull-right">
					<?php if ($filter == 'yes') : ?>
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						<?php echo str_replace("%20"," ",$this->uri->segment(4)); ?>&nbsp;<i class="fa fa-angle-down"></i>
						</button>								
					<?php else : ?>
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						Semua Jabatan <i class="fa fa-angle-down"></i>
						</button>
					<?php endif; ?>								
					<ul class="dropdown-menu pull-right" role="menu">
						<li><a href="<?php echo base_url() ?>master/homedatakaryawan/">Semua Jabatan</a></li>
			 			<?php

    			 			foreach ($jabatan_master as $j) {
								echo '<li><a href="'.base_url().'master/homedatakaryawan/index/'.$j['nama_jabatan'].'" id="'.$j['jabatan_id'].'">'.$j['nama_jabatan'].'</a></li>';
    			 			}

			 			?>
					</ul>
				</div>		

			</div>
		</li>
		
		
	
</div>

	<div class="modal fade" id="edit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" role="form" method="POST" id="submitEdit">
									
					<div class="modal-header">
	    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	    				<h3 class="modal-title" id="myModalLabel">Edit</h3>
	    			</div>
	    			<div class="modal-body">
	    				<div class="informasi" id="infoInfoKaryawan" style="margin-left:40px;">
								<div class="form-group">
									<label class="control-label col-md-4">NIP</label>
				    			 	<div class="col-md-5" style="margin-left:-40px;">
											<input type="text" class="form-control" id="nipPegawai" name="nipPegawai" placeholder="NIP" disabled>	
					        		</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Nama</label>
				    			 	<div class="col-md-5" style="margin-left:-40px;">
											<input type="text" class="form-control" id="namaPegawai" name="namaPegawai" placeholder="Nama Pegawai" >	
					        		</div>
								</div>
								<!-- Select to -->
								<div class="form-group">
				    			 	<label class="control-label col-md-4">Jenis Karyawan</label>
				    			 	<div class="col-md-5" style="margin-left:-40px;">
				    			 		<select class="form-control" id="jnsK" name="jnsK">
				    			 			<option selected>Pilih</option>
				    			 			<option value="MEDIS">Tenaga Medis</option>
				    			 			<option value="NON_MEDIS">Tenaga Non Medis</option>
				    			 		</select>
				    			 	</div>
								</div>
								
								<div class="form-group">
				    			 	<label class="control-label col-md-4">Jabatan</label>
				    			 	<div class="col-md-5" style="margin-left:-40px;">
				    			 		<select class="form-control" id="jbtan" name="jbtan">
				    			 			<option value='' selected>Pilih</option>
				    			 			<?php
					    			 			foreach ($jabatan_master as $j) {
													echo '<option value="'.$j['jabatan_id'].'">'.$j['nama_jabatan'].'</option>';
					    			 			}

				    			 			?>
				    			 		</select>	
					        		</div>
									        		
								</div>
								
								<div class="form-group">

					        		<label class="control-label col-md-4">Kualifikasi Pendidikan </label>
					        		<div class="col-md-5" style="margin-left:-40px;" >
				    			 		<select class="form-control" id="kwal" name="kwal" style="margin-top:10px">
				    			 			<option selected>Pilih</option>
				    			 			<?php  
				    			 				foreach ($kualifikasi as $data) {
				    			 					echo '<option value="'.$data['kualifikasi_id'].'">'.$data['kualifikasi'].'</option>';
				    			 				}
				    			 			?>
				    			 		</select>	
					        		</div>
								</div>
								
								<div class="form-group">
				    			 	<label class="control-label col-md-4">Unit</label>
				    			 	<div class="col-md-5" style="margin-left:-40px;">
				    			 		<select class="form-control" id="dep" name="dep">
				    			 			<option value='' selected>Pilih</option>
				    			 			<?php
					    			 			foreach ($departemen as $d) {
													echo '<option value="'.$d['dept_id'].'">'.$d['nama_dept'].'</option>';
					    			 			}

				    			 			?>	
				    			 		</select>	
					        		</div>	
								</div>

								
							
					
								<div class="dropdown" style="margin-left:-30px;width:101.5%;">
						            <div id="titleInformasi" style=" color:white">Informasi Akun</div>
						        </div>
							
								<br>
								<div class="form-group" id="buatakun">
									<label class="control-label col-md-4">Buat Akun ?</label>
									<div class="col-md-5" style="margin-left:-40px;">
										<select class="form-control" id="akun" name="akun">
									    		<option value='' selected>Pilih</option>	
									    		<option value="ya">Ya</option>	
									    		<option value="tidak">Tidak</option>	
									    </select>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Username</label>
									<div class="col-md-5" style="margin-left:-40px;">
										<input type="text" id="username" name="username" placeholder="Username" class="form-control" >
									</div>
								</div>
								<div class="form-group" id="ubahPasdong">
									<label class="control-label col-md-4">Ubah Password ?</label>
									<div class="col-md-5" style="margin-left:-40px;">
										<select class="form-control" id="ubPass" name="ubPass">
											<option value="">Pilih</option>
											<option value="ya">Ya</option>
											<option value="tidak">Tidak</option>
										</select>
									</div>
								</div>
								<div class="form-group" id="barubuat">
									<label class="control-label col-md-4">Password</label>
									<div class="col-md-5" style="margin-left:-40px;">
										<input type="password" disabled id="passwordbarubuat" name="passwordbarubuat" placeholder="Password" class="form-control" >
									</div>
									
								</div>
								<div class="form-group" id="lama">
									<label class="control-label col-md-4">Password Lama</label>
									<div class="col-md-5" style="margin-left:-40px;">
										<input type="password" id="passwordlama" name="passwordlama" placeholder="Password Lama" class="form-control" >
									</div>
										<input type="hidden" id="userid" name="userid" class="form-control" >
										<input type="hidden" id="petugasid" name="petugasid" class="form-control" >
									
								</div>

								<div class="form-group" id="baru">
									<label class="control-label col-md-4">Password Baru</label>
									<div class="col-md-5" style="margin-left:-40px;">
										<input type="password" id="passwordbaru" name="passwordbaru" placeholder="Password" class="form-control" >
									</div>
								</div>

								<div class="form-group" id="resetPasdong">
									<label class="control-label col-md-4">Reset Password ?</label>
									<div class="col-md-5" style="margin-left:-40px;">
										<select class="form-control" id="resetPass" name="ubPass">
											<option value="">Pilih</option>
											<option value="ya">Ya</option>
											<option value="tidak">Tidak</option>
										</select>
									</div>
								</div>
								<div class="form-group" id="baru_reset">
									<label class="control-label col-md-4">Password Baru</label>
									<div class="col-md-5" style="margin-left:-40px;">
										<input type="password" id="passwordbaru_reset" name="passwordbaru" placeholder="Password" class="form-control" >
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Jenis Hak Akses</label>
									<div class="col-md-5" style="margin-left:-40px;">
										<select class="form-control" id="akses_edit" name="akses">
									    	<option selected>Pilih</option>
									    	<?php  
				    			 				foreach ($role as $rol) {
				    			 					echo '<option value="'.$rol['role_id'].'">'.$rol['role_name'].'</option>';
				    			 				}
				    			 			?>		
									    </select>
									</div>
								</div>
						
						</div>
					 
    			</div>
    			<div class="modal-footer">
    					<input type="hidden" id="role_user_login" value="<?php echo $user['role_id'] ?>">
    					<button type="button" id="bataledit" class="btn btn-danger" data-dismiss="modal">Batal</button>
			       	 	<button type="submit" class="btn btn-success"  >Simpan</button>
			       		
		      	</div>
		      </form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			<form class="form-horizontal" role="form" method="POST" id="sbmitStatus">
							
				<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    				<h3 class="modal-title" id="myModalLabel">Ubah Status</h3>
    			</div>
    			<div class="modal-body">
    				<div class="informasi" id="infoInfoKaryawan" style="margin-left:40px;">
								<div class="form-group">
									<label class="control-label col-md-4">Status</label>
				    			 	<div class="col-md-5" style="margin-left:-40px;">
				    			 		<select class="form-control" id="stat" name="stat">
				    			 			<option value="">Pilih</option>
								    		<option value="Aktif">Aktif</option>
				    			 			<option value="Non Aktif">Non Aktif</option>
				    			 		</select>	
					        		</div>
								</div>
									
								<input type="hidden" id="petugasidstat" name="petugasidstat" class="form-control" >
																			
							
						
					</div>

					
    			</div>
    			<div class="modal-footer">
			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
			       		<button type="submit" class="btn btn-success" >Simpan</button>
			    </div>
			    </form>
			</div>
		</div>
	</div>
 
<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
		<div style="padding-top:10px"></div>
		<div class="informasi" id="infoDataKaryawan" style="margin-left:30px;margin-right:30px;">
			<form class="form-horizontal" role="form" id="asukaryawan" method="post">
				<div class="form-group">
					<table id="example" class="table table-striped table-bordered tableDT" cellspacing="0" style="margin-left:0px;" width="99.9%">
				        <thead >
				            <tr class="info">
				            	<th width="20" style="text-align:center">No</th>
				                <th>Nama Lengkap</th>
				                <th>Jabatan</th>
				                <th>Departemen</th>
				                <th>Username</th>
				                <th>Status</th> 
				                <th width="120">Action</th>        
				            </tr>
				        </thead>
				 
				 
				       <tbody id="tableKar">
				        	<?php
								$i = 0;
								foreach ($antrian as $data) {
									$i++;
									// $tgl = strtotime($data['tanggal_lahir']);
									// $hasil = date('d F Y', $tgl); 
									if ($data['username'] == '') {
										$data['username'] = 'Tidak ada';
									}
									echo '<tr>';
										echo '<td style="text-align:center">'.$i.'</td>';
							 			echo'<td>'.$data['nama_petugas'].'</td>';
							 			echo'<td>'.$data['jabatan'].'</td>';
							 			echo'<td>'.$data['nama_dept'].'</td>';
							 			echo'<td>'.$data['username'].'</td>';
							 			echo'<td>'.$data['status'].'</td>';
							 			echo'<td style="text-align:center">';
							 				echo '<a href="#edit" class="edKar" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
												<a href="#ubah"  class="edStat" data-toggle="modal"><i class="glyphicon glyphicon-tasks" data-toggle="tooltip" data-placement="top" title="Ubah Status"></i></a>
												<input type="hidden" class="petugas_id_edit" value="'.$data['petugas_id'].'">
												<input type="hidden" class="dept_id_edit" value="'.$data['dept_id'].'">
												<input type="hidden" class="nip_id_edit" value="'.$data['nip'].'">
												<input type="hidden" class="jabt_id_edit" value="'.$data['jabt_id'].'">
												<input type="hidden" class="user_id_edit" value="'.$data['user_id'].'">
												<input type="hidden" class="jenis_karyawan_edit" value="'.$data['jenis_karyawan'].'">
												<input type="hidden" class="kualifikasi_edit" value="'.$data['kualifikasi_id'].'">
												<input type="hidden" class="password_edit" value="'.$data['password'].'">
												<input type="hidden" class="role_edit" value="'.$data['role_id'].'">';
							 			echo'</td>';
							 		echo'</tr>';
								}
							?>
				        </tbody>
				    </table>

				</div>
				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
				<br>
			</form>
		</div>
</div>

						

						
<script type="text/javascript">
	$(document).ready(function(){

		$('#asukaryawan').submit(function (e) {
			e.preventDefault();
			window.open('<?php echo base_url() ?>master/homedatakaryawan/excellkarwayan');
		})
		

		$("#lama").hide();
		$("#baru").hide();
		$("#resetPasdong").hide();
		$("#baru_reset").hide();
		$("#ubPass").change(function(){
			if (document.getElementById('ubPass').value=="ya") {
				$("#lama").show();
				$("#baru").show();
			}else{
				$("#lama").hide();
				$("#baru").hide();
			};
		});

		$("#akun").change(function(){
			if (document.getElementById('akun').value=="ya") {
				$("#passwordbarubuat").prop('disabled', false);
				$("#username").prop('disabled', false);
				
			}else{
				$("#passwordbarubuat").prop('disabled', true);
				$("#username").prop('disabled', true);
				
			};
		});
		$("#bataledit").on('click', function (e) {
			e.preventDefault();

			$('#nipPegawai').val('');
			$('#namaPegawai').val('');
			$('#petugasid').val('');
			$('#username').val('');
			$('#password').val('');
			$('#userid').val('');
			$("#akun option[value='']").attr("selected", "selected");
			$("#ubahPasdong option[value='']").attr("selected", "selected");
			
			$("#jbtan option[value='']").attr("selected", "selected");
			$("#dep option[value='']").attr("selected", "selected");
			$("#lama").hide();
			$("#baru").hide();
						

		});

		$("#resetPass").on('change', function () {
			var a = $(this).val();
			if (a == 'ya') {
				$('#baru_reset').show();
			}else{
				$('#baru_reset').hide();
			}
		})		

		$("#tableKar").on('click', 'tr td a.edKar', function (e) {
			e.preventDefault();
			var petugas_id = $(this).closest('tr').find('td .petugas_id_edit').val();
			var nip_pegawai = $(this).closest('tr').find('td .nip_id_edit').val();
			var nama_pegawai = $(this).closest('tr').find('td').eq(1).text();
			var status_pegawai = $(this).closest('tr').find('td').eq(5).text();
			var user_pegawai = $(this).closest('tr').find('td').eq(4).text();
			var dept_id = $(this).closest('tr').find('td .dept_id_edit').val();
			var jabt_id = $(this).closest('tr').find('td .jabt_id_edit').val();
			var user_id = $(this).closest('tr').find('td .user_id_edit').val();
			var jns = $(this).closest('tr').find('td .jenis_karyawan_edit').val();
			var kualifikasi = $(this).closest('tr').find('td .kualifikasi_edit').val();
			var role = $(this).closest('tr').find('td .role_edit').val();

			if ($('#role_user_login').val() == '2') {//admin
				$("#resetPasdong").show();
			};
			//var password_id = $(this).closest('tr').find('td .password_edit').val();
				
			if (document.getElementById('akun').value=="ya") {
				$("#passwordbarubuat").prop('disabled', false);
				$("#username").prop('disabled', false);
				
			}else{
				$("#passwordbarubuat").prop('disabled', true);
				$("#username").prop('disabled', true);
				
			};


			if (user_pegawai == 'Tidak ada') {
				$("#barubuat").show();
				$("#ubahPasdong").hide();
				$("#barubuat").show();
				$("#buatakun").show();
				
				$("#akun option[value='tidak']").attr("selected", "selected");
				$("#resetPass").prop('disabled', true);				
				
			}else{
				$("#buatakun").hide();
				$("#akun option[value='ya']").attr("selected", "selected");
				$("#barubuat").hide();
				$('#username').val(user_pegawai);
				$("#username").prop('disabled', false);
				$("#password").prop('disabled', false);
				$("#akses").prop('disabled', false);
				$("#ubahPasdong").show();
				$("#resetPass").prop('disabled', false);
				
			};

			$('#nipPegawai').val(nip_pegawai);
			$('#namaPegawai').val(nama_pegawai);
			$('#petugasid').val(petugas_id);
			$('#userid').val(user_id);
			
			//$('#stat').val(status_pegawai);
			//$("#jnsK option[value='"+new_satuan_id+"']").attr("selected", "selected");
			$("#jbtan option[value='"+jabt_id+"']").attr("selected", "selected");
			$("#dep option[value='"+dept_id+"']").attr("selected", "selected");
			$("#kwal option[value='"+kualifikasi+"']").attr("selected", "selected");
			$("#jnsK option[value='"+jns+"']").attr("selected", "selected");
			$("#akses_edit option[value='"+role+"']").attr("selected", "selected");
			// $('#username').val(username_edit);
			// $('#password').val(password_edit);
			//$('#akses').val(new_h_jual);
			
		});

		$('#jnsK').on('change', function () {
			var jenis = $(this).val();
			get_jabatan(jenis);
		})


		$("#tableKar").on('click', 'tr td a.edStat', function (e) {
			e.preventDefault();
			var petugas_id = $(this).closest('tr').find('td .petugas_id_edit').val();
			var status_pegawai = $(this).closest('tr').find('td').eq(5).text();
			
	
			$("#stat option[value='"+status_pegawai+"']").attr("selected", "selected");
			$('#petugasidstat').val(petugas_id);
			
			
			
		});
		
		// $('#passwordlama').on('change',function(){
		// 	var x = {};
		// 	x['id'] = $('#userid').val();
		// 	x['pass'] = $(this).val();

		// 	$.ajax({
		// 			type: "POST",
		// 			data : x,
		// 			url: "<?php echo base_url()?>master/homedatakaryawan/cek_password",
		// 			success: function (data) {
		// 				console.log(data);
		// 				if (data == true) {
		// 					alert('SUKSES');
		// 				}else{
		// 					alert('GAGAL');

		// 				};
		// 			},error:function(data){
		// 				console.log(data);
		// 			}
		// 		});
		// });		

		$('#submitEdit').submit(function (e) {
				e.preventDefault();
				var item = {};

				item['petugas_id'] = $('#petugasid').val();
				item['nama_petugas'] = $('#namaPegawai').val();
				item['jabatan_id'] = $('#jbtan').find('option:selected').val();
				item['kualifikasi_id'] = $('#kwal').find('option:selected').val();
				item['role_id'] =$('#akses_edit').val();
				item['dept_id'] = $('#dep').find('option:selected').val();
				item['username'] = $('#username').val();
				item['passwordbaru_reset'] = $('#passwordbaru_reset').val();

				if (item['role_id'] == '' && $('#akun').val() != 'ya') {myAlert('pilih role');return false;};
				if ($('#username').val()!='') {
					item['userid'] =  $('#userid').val();
				}else{
					item['userid'] =  '';
				};
					
				if ($('#passwordlama').val()!='') {
					item['passwordlama'] = $('#passwordlama').val();
					item['passwordbaru'] = $('#passwordbaru').val();
				}else{
					item['passwordlama'] = '';
					item['passwordbaru'] = '';

				};

				if ($('#passwordbarubuat').val()!='') {
					item['passwordbarubuat'] = $('#passwordbarubuat').val();
				}else{
					item['passwordbarubuat'] = '';
				};
				
				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/homedatakaryawan/save_karyawan",
					success: function (data) {
						console.log(data);
						if (data != '') {
							alert('DATA BERHASIL DIUBAH');
							window.location.replace("<?php echo base_url()?>master/homedatakaryawan");
						}else{
							alert('Password lama tidak cocok ! Cek lagi')
						};
					},error:function(data){
						console.log(data);
					}
				});

		});


		$('#sbmitStatus').submit(function (e) {
				e.preventDefault();
				var item = {};
			    var number = 1;
			   
			    item[number] = {};

				item[number]['petugas_id'] = $('#petugasidstat').val();//localStorage.getItem('visit_id');//$('#visit_id').val();			
				item[number]['status'] = $('#stat').find('option:selected').val();

				//item[number]['jenis_karyawan'] = $('#jnsK').find('option:selected').val();
				//item[number]['jabatan'] = $('#jbtan').find('option:selected').val();
				//item[number]['kualifikasi'] = $('#kwal').val();
				//item[number]['alasan'] = $('#asn').val();
				//item[number]['status'] = $('#stat').find('option:selected').val();
				
				
				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/homedatakaryawan/ubah_status",
					success: function (data) {
						console.log(data);
						alert('Status berhasil diubah !');
						window.location.replace("<?php echo base_url()?>master/homedatakaryawan");
					},error:function(data){
						console.log(data);
					}
				});

		});


		// $('#submitExcel').submit(function (e) {
		// 		e.preventDefault();
		// 		var item = {};
		// 	    var number = 1;
			   
		// 	    item[number] = {};

		// 		item[number]['petugas_id'] = $('#petugasidstat').val();//localStorage.getItem('visit_id');//$('#visit_id').val();			
		// 		item[number]['status'] = $('#stat').find('option:selected').val();

		// 		//item[number]['jenis_karyawan'] = $('#jnsK').find('option:selected').val();
		// 		//item[number]['jabatan'] = $('#jbtan').find('option:selected').val();
		// 		//item[number]['kualifikasi'] = $('#kwal').val();
		// 		item[number]['alasan'] = $('#asn').val();
		// 		//item[number]['status'] = $('#stat').find('option:selected').val();
				
				
		// 		$.ajax({
		// 			type: "POST",
		// 			data : item,
		// 			url: "<?php echo base_url()?>master/homedatakaryawan/ubah_status",
		// 			success: function (data) {
		// 				console.log(data);
		// 				alert('Status berhasil diubah !');
		// 				window.location.replace("<?php echo base_url()?>master/homedatakaryawan");
		// 			},error:function(data){
		// 				console.log(data);
		// 			}
		// 		});

		// });
	});

function get_jabatan (jenis) {
	if (jenis == '') {$("#jbtan option[value='']").attr("selected", "selected");return false;};
	$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>master/addkaryawan/get_jabatan/"+jenis,
		success: function (data) {
			//$()
			console.log(data);
			$('#jbtan').empty();
			$('#jbtan').append('<option selected value="'+data[0]['jabatan_id']+'">'+data[0]['nama_jabatan']+'</option>');
			for (var i = 1; i < data.length; i++) {
				$('#jbtan').append('<option value="'+data[i]['jabatan_id']+'">'+data[i]['nama_jabatan']+'</option>');
			};
		},
		error: function (data) {
			console.log(data);
		}
	})
}
</script>
