<br>
<div class="title">
		<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>master/homedatapenyedia"><?php echo strtoupper('Data Penyedia') ?></a>
			<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
				<a href="<?php echo base_url() ?>master/addpenyedia" style="border-radius:0px" class="btn btn-success pull-right">Tambah</a>
			</div>
		</li>

</div>

	<div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" role="form" method="POST" id="submitUbah">
						<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    				<h3 class="modal-title" id="myModalLabel">Ubah</h3>
    			</div>
    			<div class="modal-body">
    				<div class="informasi">
							<div class="form-group">
								<label class="control-label col-md-4">Nama Penyedia</label>
								<div class="col-md-5">
									<input type="text" id="edPenyedia" class="form-control" name="edPenyedia" placeholder="Nama Penyedia">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Alamat</label>
								<div class="col-md-6">
									<textarea class="form-control" id="edAlamat" name="edAlamat" placeholder="Alamat"></textarea>
									<input type="hidden" id="penyediaid">
								</div>
							</div>
						
					</div>
					 
    			</div>
    			<div class="modal-footer">
			       		<button type="button" class="btn btn-danger" id="batalUbah" data-dismiss="modal">Batal</button>
			       		<button type="submit" class="btn btn-success" >Simpan</button>
		      	</div>
		      	</form>
			</div>
		</div>
	</div>

<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
		<div style="padding-top:10px"></div>
						
		<div class="informasi" id="infoDataKaryawan" style="margin-left:30px;margin-right:30px;">
			<form class="form-horizontal" role="form" method="post" action="<?php echo base_url() ?>master/homedatapenyedia/excelpenyedia">
				<div class="form-group">
					<table id="example" class="table table-striped table-bordered tableDT" cellspacing="0" style="margin-left:0px;" width="99.9%">
				        <thead >
				            <tr class="info">
				                <th width="20">No</th>
				                <th>Nama Penyedia</th>
				                <th>Alamat</th>
				                <th width="80">Action</th>        
				            </tr>
				        </thead>
				 
				 		<tbody id="tablePen">
				        	<?php
								$i = 0;
								foreach ($penyedia as $data) {
									$i++;
									
									echo '<tr>';
										echo '<td style="text-align:center">'.$i.'</td>';
							 			echo'<td>'.$data['nama_penyedia'].'</td>';
							 			echo'<td>'.$data['alamat'].'</td>';
							 			echo'<td style="text-align:center">';
							 				echo '<a href="#ubah" class="edUbah" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
							 				<a href="'.base_url().'master/homedatapenyedia/hapus_penyedia/'.$data['penyedia_id'].'" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a><input type="hidden" class="penyedia_id_edit" value="'.$data['penyedia_id'].'">';
							 			echo'</td>';
							 		echo'</tr>';
								}
							?>
				        </tbody>
				    </table>

				</div>
				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
				<br>
			</form>
		</div>
</div>


<script type="text/javascript">
	$(document).ready(function(){

		$("#batalUbah").on('click', function (e) {
			e.preventDefault();

			$('#edPenyedia').val('');
			$('#edAlamat').val('');
			
						

		});	

		$("#tablePen").on('click', 'tr td a.edUbah', function (e) {
			e.preventDefault();
			var penyedia_id = $(this).closest('tr').find('td .penyedia_id_edit').val();
			var penyedia = $(this).closest('tr').find('td').eq(1).text();
			var alamat = $(this).closest('tr').find('td').eq(2).text();
			$('#edPenyedia').val(penyedia);
			$('#edAlamat').val(alamat);
			$('#penyediaid').val(penyedia_id);


		});

		$('#submitUbah').submit(function (e) {
				e.preventDefault();
				var item = {};
			    var number = 1;
			   
			    item[number] = {};

				item[number]['penyedia_id'] = $('#penyediaid').val();		
				item[number]['nama_penyedia'] = $('#edPenyedia').val();
				item[number]['alamat'] = $('#edAlamat').val();
				
				
				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/homedatapenyedia/ubah_penyedia",
					success: function (data) {
						console.log(data);
						alert('Penyedia berhasil diubah !');
						window.location.replace("<?php echo base_url()?>master/homedatapenyedia");
					},error:function(data){
						console.log(data);
					}
				});

		});


	});

</script>

							