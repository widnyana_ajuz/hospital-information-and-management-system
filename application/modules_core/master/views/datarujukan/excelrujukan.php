<?php
$title = 'DATA Rujukan';

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>DATA RUJUKAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong> RS DATU SANGGUL RANTAU</strong></td>
		</tr>
	</table>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead >
	            <tr class="info">
	                <th width="20">No</th>
	                <th>Nama Instansi</th>
	                <th>Alamat</th>
	                <th>Group</th>
	                   
	            </tr>
	        </thead>
	 
	        <tbody id="tableRuj">
	        	<?php
					$i = 0;
					foreach ($rujukan as $data) {
						$i++;
						
						echo '<tr>';
							echo '<td style="text-align:center">'.$i.'</td>';
				 			echo'<td>'.$data['nama_rs'].'</td>';
				 			echo'<td>'.$data['alamat'].'</td>';
				 			echo'<td>'.$data['grup'].'</td>';
				 			
				 		echo'</tr>';
					}
				?>
	        </tbody>
		</table>
	</div>
</body>
</html>