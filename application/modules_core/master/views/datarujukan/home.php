<br>
<div class="title">
		<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>master/homedatarujukan"><?php echo strtoupper('Data Instansi Rujukan') ?></a>
			<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
				<a href="<?php echo base_url() ?>master/addrujukan" class="btn btn-success pull-right" style="border-radius:0px">Tambah</a>
			</div>
		</li>
</div>

	<div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" role="form" method="POST" id="ubahRujukan">
					<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    				<h3 class="modal-title" id="myModalLabel">Ubah</h3>
    			</div>
    			<div class="modal-body">
    				<div class="informasi">
							<div class="form-group">
								<label class="control-label col-md-4">Nama Instansi</label>
								<div class="col-md-5">
									<input type="text" id="edNama" class="form-control" name="edNama" placeholder="Nama Instansi">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Alamat</label>
								<div class="col-md-5">
									<textarea class="form-control" id="edAlamat" name="edAlamat" placeholder="Alamat"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Group</label>
								<div class="col-md-4">
									<select class="form-control" id="edGroup" name="edGroup">
										<option value="" selected>Pilih</option>
										<option value="Puskesmas">Puskesmas</option>
										<option value="Rumah Sakit">Rumah Sakit</option>
									</select>
								</div>
							</div>
							<input type="hidden" id="rs_id">
						
					</div>
					 
    			</div>
    			<div class="modal-footer">
			       		<button type="button" id="batalUbah" class="btn btn-danger" data-dismiss="modal">Batal</button>
			       		<button type="submit" class="btn btn-success">Simpan</button>
		      	</div>
		      	</form>
			</div>
		</div>
	</div>

<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
		<div style="padding-top:10px"></div>
		
		<div class="informasi" id="infoDataKaryawan" style="margin-left:30px;margin-right:30px;">
			<form class="form-horizontal" role="form" method="post" action="<?php echo base_url() ?>master/homedatarujukan/excelrujukan">
				<div class="form-group">
					<table id="example" class="table table-striped table-bordered tableDT" cellspacing="0" style="margin-left:0px;" width="99.9%">
				        <thead >
				            <tr class="info">
				                <th width="20">No</th>
				                <th>Nama Instansi</th>
				                <th>Alamat</th>
				                <th>Group</th>
				                <th width="80">Action</th>        
				            </tr>
				        </thead>
				 
				        <tbody id="tableRuj">
				        	<?php
								$i = 0;
								foreach ($rujukan as $data) {
									$i++;
									
									echo '<tr>';
										echo '<td style="text-align:center">'.$i.'</td>';
							 			echo'<td>'.$data['nama_rs'].'</td>';
							 			echo'<td>'.$data['alamat'].'</td>';
							 			echo'<td>'.$data['grup'].'</td>';
							 			echo'<td style="text-align:center">';
							 				echo '<a href="#ubah" class="edUbah" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
							 				<a href="'.base_url().'master/homedatarujukan/hapus_rujukan/'.$data['rs_id'].'" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a><input type="hidden" class="rs_id_edit" value="'.$data['rs_id'].'">';
							 			echo'</td>';
							 		echo'</tr>';
								}
							?>
				        </tbody>

				    </table>

				</div>
				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
				<br>
			</form>
		</div>
</div>


<script type="text/javascript">
	$(document).ready(function(){

		$("#batalUbah").on('click', function (e) {
			e.preventDefault();

			$('#edAlamat').val('');
			$('#edNama').val('');
			$("#edGroup option[value='']").attr("selected", "selected");
						
						

		});	

		$("#tableRuj").on('click', 'tr td a.edUbah', function (e) {
			e.preventDefault();
			var rs_id = $(this).closest('tr').find('td .rs_id_edit').val();
			var nama = $(this).closest('tr').find('td').eq(1).text();
			var alamat = $(this).closest('tr').find('td').eq(2).text();
			var group = $(this).closest('tr').find('td').eq(3).text();
			$('#edNama').val(nama);
			$('#edAlamat').val(alamat);
			$('#edGroup').val(group);
			$('#rs_id').val(rs_id);


		});

		$('#ubahRujukan').submit(function (e) {
				e.preventDefault();
				var item = {};
			    var number = 1;
			   
			    item[number] = {};

				item[number]['rs_id'] = $('#rs_id').val();		
				item[number]['nama_rs'] = $('#edNama').val();
				item[number]['alamat'] = $('#edAlamat').val();
				item[number]['grup'] = $('#edGroup').val();
				
				
				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/homedatarujukan/ubah_rujukan",
					success: function (data) {
						console.log(data);
						alert('Rujukan berhasil diubah !');
						window.location.replace("<?php echo base_url()?>master/homedatarujukan");
					},error:function(data){
						console.log(data);
					}
				});

		});


	});

</script>