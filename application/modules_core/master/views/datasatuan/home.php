<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>master/homedatasatuan">DATA SATUAN</a>
		<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
			<a href="<?php echo base_url() ?>master/addsatuan" class="btn btn-success pull-right" style="border-radius:0px">Tambah</a>
		</div>
	</li>
</div>

<div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" role="form" method="POST" id="submitUbah">
					<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h3 class="modal-title" id="myModalLabel">Ubah</h3>
			</div>
			<div class="modal-body">
				<div class="informasi">
						<div class="form-group">
							<label class="control-label col-md-3">Satuan</label>
							<div class="col-md-4">
								<input type="text" id="edSat" class="form-control" name="edSat" placeholder="Satuan">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Keterangan</label>
							<div class="col-md-6">
								<textarea id="edKet" class="form-control" name="edKet" placeholder="Keterangan"></textarea>
							</div>
							<input type="hidden" id="satuanid" name="satuanid" class="form-control">
						</div>
					
				</div>
				 
			</div>
			<div class="modal-footer">
		       		
		       		<button type="button" id="batalUbah" class="btn btn-danger" data-dismiss="modal">Batal</button>
		       		<button type="submit" class="btn btn-success">Simpan</button>
	      	</div>
	      	</form>
		</div>
	</div>
</div>
	
<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
	<div style="padding-top:10px"></div>
						
		<div class="informasi" id="infoDataKaryawan" style="margin-left:30px;margin-right:30px;">
			<form class="form-horizontal" role="form" method="post" action="<?php echo base_url() ?>master/homedatasatuan/excelsatuan">
				<div class="form-group">
					<table id="example" class="table table-striped table-bordered tableDT" cellspacing="0" style="margin-left:0px;" width="99.9%">
				        <thead >
				            <tr class="info">
				                <th width="20">No</th>
				                <th>Satuan</th>
				                <th>Keterangan</th>
				                <th width="80">Action</th>        
				            </tr>
				        </thead>
				 
				 
				        <tbody id="tableSat">
				        	<?php
								$i = 0;
								foreach ($satuan as $data) {
									$i++;
									// $tgl = strtotime($data['tanggal_lahir']);
									// $hasil = date('d F Y', $tgl); 
									if ($data['keterangan'] == '') {
										$data['keterangan'] = 'Tidak ada';
									}
									echo '<tr>';
										echo '<td style="text-align:center">'.$i.'</td>';
							 			echo'<td>'.$data['satuan'].'</td>';
							 			echo'<td>'.$data['keterangan'].'</td>';
							 			echo'<td style="text-align:center">';
							 				echo '<a href="#ubah" class="edUbah" data-toggle="modal">
							 				<i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>';
							 				echo '<input type="hidden" class="satuan_id_edit" value="'.$data['satuan_id'].'"';
							 			echo '</td>';
							 		echo'</tr>';
							 		/*<a href="'.base_url().'master/homedatasatuan/hapus_satuan/'.$data['satuan_id'].'" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a><input type="hidden" class="satuan_id_edit" value="'.$data['satuan_id'].'">';*/
								}
							?>
				        </tbody>
				    </table>

				</div>
				<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
				<br>
			</form>
		</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$("#batalUbah").on('click', function (e) {
			e.preventDefault();

			$('#edSat').val('');
			$('#edKet').val('');
			
						

		});	

		$("#tableSat").on('click', 'tr td a.edUbah', function (e) {
			e.preventDefault();
			var satuan_id = $(this).closest('tr').find('td .satuan_id_edit').val();
			var satuan = $(this).closest('tr').find('td').eq(1).text();
			var keterangan = $(this).closest('tr').find('td').eq(2).text();
			$('#edSat').val(satuan);
			$('#edKet').val(keterangan);
			$('#satuanid').val(satuan_id);


		});

		$('#submitUbah').submit(function (e) {
				e.preventDefault();
				var item = {};
			    var number = 1;
			   
			    item[number] = {};

				item[number]['satuan_id'] = $('#satuanid').val();		
				item[number]['satuan'] = $('#edSat').val();
				item[number]['keterangan'] = $('#edKet').val();
				
				
				$.ajax({
					type: "POST",
					data : item,
					url: "<?php echo base_url()?>master/homedatasatuan/ubah_satuan",
					success: function (data) {
						console.log(data);
						alert('Satuan berhasil diubah !');
						window.location.replace("<?php echo base_url()?>master/homedatasatuan");
					},error:function(data){
						console.log(data);
					}
				});

		});


	});

</script>

							