<?php
$title = 'DATA Tarif';

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>DATA TARIF</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong> RS DATU SANGGUL RANTAU</strong></td>
		</tr>
	</table>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead >
			<tr class="info">
              <th>No</th>
              <th>Nama Tindakan</th>
              <th>Jenis Unit</th>
              <th>Kategori</th>
              <th>Kelas III</th>
              <th>Kelas II</th>
              <th>Kelas I</th>
              <th>Utama</th>
              <th>VIP</th>
			<th>ICD-9CM</th>
              <th>Status</th>
             
            </tr>
		    	</thead>

					<tbody id="tbodytarif">
						<?php
							if (isset($tarif)) {
								if (!empty($tarif)) {
									$i = 0;
									foreach ($tarif as $value) {
										echo '<tr>
											<td align="center">'.(++$i).'</td>
											<td>'.$value['nama_tindakan'].'</td>
											<td>'.$value['jenis_unit'].'</td>
											<td>'.$value['kategori'].'</td>
											<td class="text-right">'.$value['tarif3'].'</td>
											<td class="text-right">'.$value['tarif2'].'</td>
											<td class="text-right">'.$value['tarif1'].'</td>
											<td class="text-right">'.$value['tarif_utama'].'</td>
											<td class="text-right">'.$value['tarif_vip'].'</td>
											<td>'.$value['icd9cm'].'</td>
											<td>'.$value['status'].'</td>
							
										</tr>';
									}
								}
							}
						?>
					</tbody>
		</table>
	</div>
</body>
</html>