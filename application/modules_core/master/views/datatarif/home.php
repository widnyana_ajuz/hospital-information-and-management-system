<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>master/homedatatarif">DATA TARIF</a>
		<div class="pull-right" style="margin-top:-8px;margin-right:7px;">
			<a href="<?php echo base_url() ?>master/addtarif" style="border-radius:0px;" class="btn btn-success pull-right">Tambah</a>
			<div class="btn-group pull-right">

				<?php if ($filter == 'yes') : ?>
					<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						<?php echo str_replace("%20"," ",$this->uri->segment(4)); ?>&nbsp;<i class="fa fa-angle-down"></i>
					</button>
				<?php else : ?>
					<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						Semua Unit <i class="fa fa-angle-down"></i>
					</button>
				<?php endif; ?>

				<ul class="dropdown-menu pull-right" role="menu">
					<li><a href="<?php echo base_url() ?>master/homedatatarif/">SEMUA UNIT</a></li>
					<?php
						foreach ($jenis as $j) {
							echo '<li><a href="'.base_url().'master/homedatatarif/index/'.$j['jenis_unit'].'">'.$j['jenis_unit'].'</a></li>';
						}
					?>
				</ul>
				<!-- <button type="button" class="btn grey-salt dropdown-toggle"  style="border-radius:0px;"  data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
					DOKTER&nbsp;<i class="fa fa-angle-down"></i>
				</button>
				<ul class="dropdown-menu pull-right" role="menu">
					<li>
						<a href="#">SEMUA DEPARTEMEN </a>
					</li>
					<li>
						<a href="#">DOKTER </a>
					</li>
					<li>
						<a href="#">ASISTEN DOKTER </a>
					</li>
					<li>
						<a href="#">PERAWAT </a>
					</li>
					<li>
						<a href="#">LABORAT </a>
					</li>
					<li>
						<a href="#">APOTEKER </a>
					</li>
					<li>
						<a href="#">PETUGAS ADMINISTRASI </a>
					</li>
				</ul> -->
			</div>
		</div>
	</li>
</div>

<div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-400px;">
	<div class="modal-dialog">
		<div class="modal-content" style="width:1000px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h3 class="modal-title" id="myModalLabel">Ubah Tarif Tindakan</h3>
			</div>
			<form class="form-horizontal" role="form" method="post" id="frmUbah">
				<div class="modal-body">
					<div class="informasi">
						<div class="form-group">
							<label class="control-label col-md-2">Nama Tindakan</label>
							<div class="col-md-7">
								<textarea id="edNama" class="form-control" name="edNama" placeholder="Nama Tindakan" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Jenis Unit</label>
							<div class="col-md-3">
								<select class="form-control" id="edUnit" name="edUnit" required>
									<option value='' selected>Pilih</option>
									<option value='POLIKLINIK'>POLIKLINIK</option>
									<option value='IGD'>IGD</option>
									<option value='RAWAT INAP'>RAWAT INAP</option>
									<option value='BERSALIN'>BERSALIN</option>
									<option value='PENUNJANG'>PENUNJANG</option>
									<option value='PENDUKUNG'>PENDUKUNG</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Kategori</label>
							<div class="col-md-5">
								<select class="form-control" id="edKat" name="edKat">
									<?php
										if (isset($kategori)) {
											if (!empty($kategori)) {
												foreach ($kategori as $value) {
													echo '<option value='.$value['kat_id'].'>'.$value['keterangan'].'</option>';
												}
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">ICD-9CM</label>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" id="edKode9" class="form-control isisan" placeholder="Kode" data-toggle="modal" data-target="#searchICD" readonly>
							</div>
							<div class="col-md-5">
								<input type="text" style="cursor:pointer;background-color:white" id="edIcd9" class="form-control" name="edicd9" placeholder="ICD-9CM" data-toggle="modal" data-target="#searchICD" readonly>
							</div>
							<input type="hidden" id="id_icd9">
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Status</label>
							<div class="col-md-3">
								<select class="form-control" id="edStatus" name="edStatus">
									<option value='ACTIVE' selected>ACTIVE</option>
									<option value='INACTIVE'>INACTIVE</option>
								</select>
							</div>
						</div>
						<input type="hidden" id="idEdit">
						<div class="form-group">
							<div class="portlet-body" style="margin: 0px 20px 0px 0px">
								<table class="table table-striped table-bordered table-hover table-responsive">
									<thead>
										<tr class="info" >
											<th  style="text-align:center"> Jenis Tarif </th>
											<th  style="text-align:center"> Kode </th>
											<th  style="text-align:center"> BAKHP </th>
											<th  style="text-align:center"> JS </th>
											<th  style="text-align:center"> JP </th>
											<th  style="text-align:center"> Total </th>
											<th  style="text-align:center"> Clear </th>
										</tr>
									</thead>
									<tbody id="tbodyedit">
										<tr>
											<td>Kelas III</td>
											<td><input type="text" class="form-control kode" id="kode1"></td>
											<td><input type="text" class="form-control text-right bakhp" id="bakhp1" placeholder="0"></td>
											<td><input type="text" class="form-control text-right js" id="js1" placeholder="0"></td>
											<td><input type="text" class="form-control text-right jp" id="jp1" placeholder="0"></td>
											<td><input type="text" class="form-control text-right total" id="total1" placeholder="0" disabled></td>
											<td align="center">
												<a href="#" class="clearRow" id="clear1"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
												<input type="hidden" class="nilai_bakhp" id="val_bakhp1">
												<input type="hidden" class="nilai_js" id="val_js1">
												<input type="hidden" class="nilai_jp" id="val_jp1">
												<input type="hidden" class="nilai_id" id="id1">
											</td>
										</tr>
										<tr>
											<td>Kelas II</td>
											<td><input type="text" class="form-control kode" id="kode2"></td>
											<td><input type="text" class="form-control text-right bakhp" id="bakhp2" placeholder="0"></td>
											<td><input type="text" class="form-control text-right js" id="js2" placeholder="0"></td>
											<td><input type="text" class="form-control text-right jp" id="jp2" placeholder="0"></td>
											<td><input type="text" class="form-control text-right total" id="total2" placeholder="0" disabled></td>
											<td align="center">
												<a href="#" class="clearRow" id="clear2"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
												<input type="hidden" class="nilai_bakhp" id="val_bakhp2">
												<input type="hidden" class="nilai_js" id="val_js2">
												<input type="hidden" class="nilai_jp" id="val_jp2">
												<input type="hidden" class="nilai_id" id="id2">
											</td>
										</tr>
										<tr>
											<td>Kelas I</td>
											<td><input type="text" class="form-control kode" id="kode3"></td>
											<td><input type="text" class="form-control text-right bakhp" id="bakhp3" placeholder="0"></td>
											<td><input type="text" class="form-control text-right js" id="js3" placeholder="0"></td>
											<td><input type="text" class="form-control text-right jp" id="jp3" placeholder="0"></td>
											<td><input type="text" class="form-control text-right total" id="total3" placeholder="0" disabled></td>
											<td align="center">
												<a href="#" class="clearRow" id="clear3"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
												<input type="hidden" class="nilai_bakhp" id="val_bakhp3">
												<input type="hidden" class="nilai_js" id="val_js3">
												<input type="hidden" class="nilai_jp" id="val_jp3">
												<input type="hidden" class="nilai_id" id="id3">
											</td>
										</tr>
										<tr>
											<td>Kelas Utama</td>
											<td><input type="text" class="form-control kode" id="kode4"></td>
											<td><input type="text" class="form-control text-right bakhp" id="bakhp4" placeholder="0"></td>
											<td><input type="text" class="form-control text-right js" id="js4" placeholder="0"></td>
											<td><input type="text" class="form-control text-right jp" id="jp4" placeholder="0"></td>
											<td><input type="text" class="form-control text-right total" id="total4" placeholder="0" disabled></td>
											<td align="center">
												<a href="#" class="clearRow" id="clear4"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
												<input type="hidden" class="nilai_bakhp" id="val_bakhp4">
												<input type="hidden" class="nilai_js" id="val_js4">
												<input type="hidden" class="nilai_jp" id="val_jp4">
												<input type="hidden" class="nilai_id" id="id4">
											</td>
										</tr>
										<tr>
											<td>Kelas VIP</td>
											<td><input type="text" class="form-control kode" id="kode5"></td>
											<td><input type="text" class="form-control text-right bakhp" id="bakhp5" placeholder="0"></td>
											<td><input type="text" class="form-control text-right js" id="js5" placeholder="0"></td>
											<td><input type="text" class="form-control text-right jp" id="jp5" placeholder="0"></td>
											<td><input type="text" class="form-control text-right total" id="total5" placeholder="0" disabled></td>
											<td align="center">
												<a href="#" class="clearRow" id="clear5"><i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Reset"></i></a>
												<input type="hidden" class="nilai_bakhp" id="val_bakhp5">
												<input type="hidden" class="nilai_js" id="val_js5">
												<input type="hidden" class="nilai_jp" id="val_jp5">
												<input type="hidden" class="nilai_id" id="id5">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<br>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					<button type="submit" id="btnSimpanEdit" class="btn btn-success">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="searchICD" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h3 class="modal-title" id="myModalLabel">Pilih ICD-9CM</h3>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<form class="form-horizontal" role="form" method="post" id="cari_icd">
						<div class="col-md-5">
							<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Kata kunci"/>
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info">Cari</button>
						</div>
					</form>
				</div>
				<br>
				<div style="margin-left:5px; margin-right:5px;"><hr></div>
				<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover" id="tabelSearchICD">
						<thead>
							<tr class="info">
								<th width="30%;">Kode</th>
								<th>Keterangan</th>
								<th width="10%">Pilih</th>
							</tr>
						</thead>
						<tbody id="tbody_icd">
							<tr>
								<td style="text-align:center;" class="kosong" colspan="4">Cari ICD-9CM</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>

<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
	<div style="padding-top:10px"></div>
	<div class="informasi" id="infoDataKaryawan" style="margin-left:30px;margin-right:30px;">
		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url() ?>master/homedatatarif/exceltarif">
			<div class="form-group">
				<table id="example" class="table table-striped table-bordered tableDT" cellspacing="0" style="margin-left:0px;" width="99.9%">
	        <thead >
			<tr class="info">
              <th>No</th>
              <th>Nama Tindakan</th>
              <th>Jenis Unit</th>
              <th>Kategori</th>
              <th>Kelas III</th>
              <th>Kelas II</th>
              <th>Kelas I</th>
              <th>Utama</th>
              <th>VIP</th>
							<th>ICD-9CM</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
		    	</thead>

					<tbody id="tbodytarif">
						<?php
							if (isset($tarif)) {
								if (!empty($tarif)) {
									$i = 0;
									foreach ($tarif as $value) {
										echo '<tr>
											<td align="center">'.(++$i).'</td>
											<td>'.$value['nama_tindakan'].'</td>
											<td>'.$value['jenis_unit'].'</td>
											<td>'.$value['kategori'].'</td>
											<td class="text-right">'.number_format($value['tarif3'], 0, '', '.').'</td>
											<td class="text-right">'.number_format($value['tarif2'], 0, '', '.').'</td>
											<td class="text-right">'.number_format($value['tarif1'], 0, '', '.').'</td>
											<td class="text-right">'.number_format($value['tarif_utama'], 0, '', '.').'</td>
											<td class="text-right">'.number_format($value['tarif_vip'], 0, '', '.').'</td>
											<td>'.$value['icd9cm'].'</td>
											<td>'.$value['status'].'</td>
											<td style="text-align:center">
												<input type="hidden" id="idtarif" value="'.$value['id'].'">
												<input type="hidden" id="icd9_kode" value="'.$value['kodeicd'].'">
												<input type="hidden" id="icd9_id" value="'.$value['id_icd'].'">
												<a href="#ubah" class="btnedit" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
												<a href="#" class="changeStatus"><i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="Ubah Status"></i></a>
											</td>
										</tr>';
									}
								}
							}
						?>
					</tbody>
			  </table>
			</div>
			<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
				<br>
		</form>
	</div>
</div>
