<script type="text/javascript">
  $(document).ready(function(){
    //autonumeric
    jQuery(function($) {
      $('#bakhp1').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js1').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp1').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#bakhp2').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js2').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp2').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#bakhp3').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js3').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp3').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#bakhp4').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js4').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp4').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#bakhp5').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#js5').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
      $('#jp5').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0'});
    });

    //popup edit tarif
    $('#tbodytarif').on('click', 'tr td a.btnedit', function(e){
      e.preventDefault();

      var id = $(this).closest('tr').find('td #idtarif').val();
      $('#idEdit').val(id);

      $('#edNama').val($(this).closest('tr').find('td').eq(1).text());

      var unit = $(this).closest('tr').find('td').eq(2).text();
      $('#edUnit').val(unit);

      var kat = $(this).closest('tr').find('td').eq(3).text();
      $('#edKat option:contains(' + kat + ')').prop({selected: true});

      $('#edKode9').val($(this).closest('tr').find('td #icd9_kode').val());
      $('#edIcd9').val($(this).closest('tr').find('td').eq(9).text());
      $('#id_icd9').val($(this).closest('tr').find('td #icd9_id').val());

      var stat = $(this).closest('tr').find('td').eq(10).text();
      $('#edStatus').val(stat);

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>master/homedatatarif/get_tarif_detail/" + id,
        success: function(data){
          console.log(data);
          if (data.length > 0) {
            //$('#tbodyedit').empty();
            for (var i = 0; i < data.length; i++) {
              var c = i+1;
              var tariftotal = Number(data[i]['js'])+Number(data[i]['jp'])+Number(data[i]['bakhp']);
              $('#kode'+c+'').val(data[i]['kode_tindakan'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
              $('#bakhp'+c+'').val(data[i]['bakhp'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
              $('#js'+c+'').val(data[i]['js'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
              $('#jp'+c+'').val(data[i]['jp'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
              $('#total'+c+'').val(tariftotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

              $('#val_bakhp'+c+'').val(data[i]['bakhp']);
              $('#val_js'+c+'').val(data[i]['js']);
              $('#val_jp'+c+'').val(data[i]['jp']);
              $('#id'+c+'').val(data[i]['detail_id']);
            }
          }
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //ganti status active-inactive
    $('#tbodytarif').on('click', 'tr td a.changeStatus', function(e){
      e.preventDefault();
      var tdstatus = $(this);

      var id = $(this).closest('tr').find('td #idtarif').val();
      var stat = $(this).closest('tr').find('td').eq(10).text();
      var item = {};
      if (stat == 'ACTIVE') {
        item['status'] = 'INACTIVE';
      }else {
        item['status'] = 'ACTIVE';
      }

      $.ajax({
        type: "POST",
        data: item,
        url: "<?php echo base_url()?>master/homedatatarif/update_status/" + id,
        success: function(data){
          tdstatus.closest('tr').find('td').eq(10).text(item['status']);
          myAlert('DATA BERHASIL DIUBAH');
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //search icd9cm di popup
    $('#cari_icd').submit(function(e){
      e.preventDefault();
      var katakunci = {};
      katakunci['search'] = $('#katakunci').val();
      $.ajax({
        type: "POST",
        data:katakunci,
        url: "<?php echo base_url()?>master/homedatatarif/search_icd",
        success: function(data){
          $('#tbody_icd').empty();
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              $('#tbody_icd').append(
                '<tr>'+
                  '<td>' + data[i]['kode']+'</td>'+
                  '<td>' + data[i]['keterangan']+'</td>'+
                  '<td style="text-align:center; cursor:pointer;">'+
                    '<input type="hidden" class="id_icd" value="'+data[i]['id']+'">'+
                    '<a href="#" class ="add_icd"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a>'+
                  '</td>'+
                '</tr>'
              )
            };
          }
        },
        error: function(data){
          console.log(data);
        }
      })
    });

    //pilih icd9cm
    $('#tbody_icd').on('click', 'tr td a.add_icd', function(e){
      e.preventDefault();
      $('#edKode9').val($(this).closest('tr').find('td').eq(0).text());
      $('#edIcd9').val($(this).closest('tr').find('td').eq(1).text());
      $('#id_icd9').val($(this).closest('tr').find('td .id_icd').val());

      $('#searchICD').modal('hide');
    });

    var tdtotal;
    $('#tbodyedit').on('change', ' tr td .bakhp', function(e){
      e.preventDefault();
      var valbakhp = $(this).autoNumeric('get');
      var valjs = $(this).closest('tr').find('td .js').autoNumeric('get');
      var valjp = $(this).closest('tr').find('td .jp').autoNumeric('get');
      $(this).closest('tr').find('td .nilai_bakhp').val(valbakhp);
      tdtotal = $(this);
      hitung_total(valbakhp, valjs, valjp, tdtotal);
    })

    $('#tbodyedit').on('change', ' tr td .js', function(e){
      e.preventDefault();
      var valbakhp = $(this).closest('tr').find('td .bakhp').autoNumeric('get');
      var valjs = $(this).autoNumeric('get');
      var valjp = $(this).closest('tr').find('td .jp').autoNumeric('get');
      $(this).closest('tr').find('td .nilai_js').val(valjs);
      tdtotal = $(this);
      hitung_total(valbakhp, valjs, valjp, tdtotal);
    })

    $('#tbodyedit').on('change', ' tr td .jp', function(e){
      e.preventDefault();
      var valbakhp = $(this).closest('tr').find('td .bakhp').autoNumeric('get');
      var valjs = $(this).closest('tr').find('td .js').autoNumeric('get');
      var valjp = $(this).autoNumeric('get');
      $(this).closest('tr').find('td .nilai_jp').val(valjp);
      tdtotal = $(this);
      hitung_total(valbakhp, valjs, valjp, tdtotal);
    })

    //reset
    $('#tbodyedit').on('click', 'tr td a.clearRow', function(e){
      e.preventDefault();
      $(this).closest('tr').find('td .kode').val('');
      $(this).closest('tr').find('td .bakhp').val('0');
      $(this).closest('tr').find('td .js').val('0');
      $(this).closest('tr').find('td .jp').val('0');
      $(this).closest('tr').find('td .total').val('0');
      $(this).closest('tr').find('td .nilai_bakhp').val('0');
      $(this).closest('tr').find('td .nilai_js').val('0');
      $(this).closest('tr').find('td .nilai_jp').val('0');
      $(this).closest('tr').find('td .nilai_id').val('');
    })

    //simpan editan
    $('#frmUbah').submit(function(e){
      e.preventDefault();
      var item = {};
      var id = $('#idEdit').val();

      item['nama_tindakan'] = $('#edNama').val();
      item['jenis_unit'] = $('#edUnit').val();
      item['kat_id'] = $('#edKat').val();
      item['status'] = $('#edStatus').val();
      item['icd9cm'] = $('#id_icd9').val();

      var data=[];
      $('#tbodyedit').find('tr').each(function(rowIndex, r){
        var cols = [];
        $(this).find('td').each(function(colIndex,c){
          cols.push(c.textContent);
        });
        $(this).find('td input').each(function (colIndex, c) {
          cols.push(c.value);
        });
        data.push(cols);
      })
      console.log(data);
      item['data'] = data;

      $.ajax({
        type: "POST",
        data: item,
        url: "<?php echo base_url()?>master/homedatatarif/update_tarif/" + id,
        success: function(data){
          console.log(data);
          myAlert('DATA BERHASIL DIUBAH');
          $('#ubah').modal('hide');
          window.location.replace("<?php echo base_url()?>master/homedatatarif");
        },
        error: function(data){
          console.log('data: ' + data);
          console.log('item: ' + item);
        }
      })
    })

  }) //end of document ready

  function hitung_total(valbakhp, valjs, valjp, tdtotal)
  {
    var valtotal = Number(valbakhp) + Number(valjs) + Number(valjp);
    tdtotal.closest('tr').find('td .total').val(valtotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
  }
</script>
