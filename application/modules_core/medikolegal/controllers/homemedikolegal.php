<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php');

class Homemedikolegal extends Operator_base {
	protected $dept_id;

	function __construct(){
		parent:: __construct();
		$this->load->model('m_mediko');
		$data['page_title'] = "Mediko Legal";
		$this->dept_id = $this->m_mediko	->get_dept_id('MEDIKO LEGAL')['dept_id'];
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		// load template
		$data['content'] = 'home';
		$data['riwayat'] = $this->m_mediko->get_all_riwayat();
		$data['javascript'] = 'j_mediko';
		$this->load->view('base/operator/template', $data);
	}

	public function get_tindakan()
	{
		$result = $this->m_mediko->get_tindakan();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_paramedis()
	{
		$result = $this->m_mediko->get_paramedis();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_riwayat_detail($id)
	{
		$res = $this->m_mediko->get_riwayat_detail($id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function get_tarif($tindakan_id, $kelas)
	{
		$kelasbaru = str_replace("%20", " ", $kelas);
		$res = $this->m_mediko->get_tarif($tindakan_id, $kelasbaru);
		header('Content-Type: application/json');
		echo json_encode($res);
	}

	public function get_new_id()
	{
		//format: xx000yymmddzzz
		//xx: dept_id
		//zzz:autoincrement
		$id = $this->m_mediko->get_dept_id('MEDIKO LEGAL');
		$awal = $id['dept_id'].'000'.date('y').date('m').date('d');
		$cariin = $this->m_mediko->get_max_id($awal);
		if ($cariin['invoice']) {
			$id_invoice = $cariin['invoice'] + 1;
		}else{
			$id_invoice = $awal.'001';
		}
		header('Content-Type: application/json');
	 	echo json_encode($id_invoice);
	}

	public function insert_perawatan()
	{
		$data = $_POST['data'];
		$ins = array(
			'nama' => $_POST['nama'],
			'umur' => $_POST['umur'],
			'jenis_kelamin' => $_POST['jenis_kelamin'],
			'no_telp' => $_POST['no_telp'],
			'alamat' => $_POST['alamat'],
			'kelas_pelayanan' => $_POST['kelas_pelayanan'],
			'no_invoice' => $_POST['no_invoice'],
			'total' => $_POST['total']
		);

		$id = $this->m_mediko->add_mediko($ins);

		if ($id) {
			foreach ($data as $value) {
				$tgl = DateTime::createFromFormat('d/m/Y H:i:s', $value[2]);

				$insdetail = array(
					'perawatan_id' => $id,
					'detail_tindakan_id' => $value[20],
					'waktu' => $tgl->format('Y-m-d H:i:s'),
					'paramedis_id' => $value[21],
					'bakhp' => $value[22],
					'js' => $value[23],
					'jp' => $value[24],
					'on_faktur' => $value[25],
					'total' => $value[26]
				);
				$this->m_mediko->add_mediko_detail($insdetail);
				$result = array(
					'message' => 'Data berhasil disimpan',
					'error' => 'n'
				);
			}
		}else{
			$result = array(
				'message' => 'Gagal menyimpan data',
				'error' => 'y'
			 );
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}


}
