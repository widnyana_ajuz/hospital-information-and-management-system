<?php
  class M_mediko extends CI_Model
  {
    function __construct(){
    }

    public function get_dept_id($nama)
    {
      $sql = "SELECT dept_id from master_dept WHERE nama_dept LIKE '$nama'";
      $res = $this->db->query($sql);
      if ($res){
        return $res->row_array();
      }
      else {
        return false;
      }
    }

    public function get_all_riwayat()
    {
      $sql = "SELECT * FROM mediko_perawatan";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_riwayat_detail($id)
    {
      $sql = "SELECT mpd.id AS id, mpd.detail_tindakan_id AS detail_tindakan_id, mtd.tindakan_id AS tindakan_id, nama_tindakan, waktu, nama_petugas AS paramedis, IFNULL(paramedis_lain, '-') AS paramedis_lain, mpd.bakhp+mpd.js+mpd.jp AS tarif, mpd.on_faktur, mpd.total
              FROM mediko_perawatan mp LEFT JOIN mediko_perawatan_detail mpd on mpd.perawatan_id=mp.id
              LEFT JOIN master_tindakan_detail mtd ON mpd.detail_tindakan_id=mtd.detail_id
              LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
              LEFT JOIN petugas ON mpd.paramedis_id=petugas.petugas_id
              WHERE mpd.perawatan_id=$id";

      $res = $this->db->query($sql);
      if ($res) {
       return $res->result_array();
      }else {
       return false;
      }
    }

    public function get_tindakan()
    {
      $sql = "SELECT * FROM master_tindakan WHERE status='ACTIVE'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_paramedis()
    {
      $sql = "SELECT petugas_id, nama_petugas, jenis
              FROM petugas p, master_jabatan mj
              WHERE p.jabatan_id = mj.jabatan_id AND p.status='aktif' AND mj.jenis='medis'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_tarif($tindakan_id, $kelas)
    {
      $sql = "SELECT mtd.detail_id AS tarif_id, bakhp, js, jp
              FROM master_tindakan mt LEFT JOIN master_tindakan_detail mtd ON mtd.tindakan_id=mt.tindakan_id
              WHERE mtd.tindakan_id=$tindakan_id AND mtd.kelas='$kelas'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

    public function get_max_id($cari)
    {
      $sql = "SELECT MAX(no_invoice) AS invoice FROM mediko_perawatan WHERE no_invoice LIKE '$cari%'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

    public function add_mediko($data)
    {
      $result =  $this->db->insert('mediko_perawatan', $data);
			if ($result) {
				return $this->db->insert_id();
			}else{
				return false;
			}
    }

    public function add_mediko_detail($data)
    {
      $result =  $this->db->insert('mediko_perawatan_detail', $data);
      if ($result) {
        return $this->db->insert_id();
      }else{
        return false;
      }
    }

  }
?>
