<br><div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>medikolegal/home">MEDIKO LEGAL</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Tindakan Mediko Legal</a>
	</li>
</div>

<div class="navigation" style="margin-left: 10px" >
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
    <li class="active"><a href="#legal" class="cl" data-toggle="tab">Tindakan Mediko Legal</a></li>
    <li><a href="#riwayat" class="cl" data-toggle="tab">Riwayat Tindakan Mediko Legal</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
 		<div class="modal fade" id="tambahTindakan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
		   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		   				<h3 class="modal-title" id="myModalLabel">Tambah Tindakan</h3>
		   			</div>
						<div class="modal-body">
							<div class="informasi">
								<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-6">
										<input type="text" id="add_waktu" style="cursor:pointer;" class="form-control" readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii:ss" value="<?php echo date("d/m/Y H:i:s");?>"/>
									</div>
				  			</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tindakan</label>
									<div class="col-md-6">
										<input type="hidden" id="add_id_tindakan" >
										<input type="text" class="form-control" id="add_tindakan" autocomplete="off" spellcheck="off" placeholder="Pilih Tindakan" required>
										<br>
										<textarea class="form-control" id="add_view_tindakan" rows="4" placeholder="Tindakan" readonly></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input type="text" class="form-control text-right" id="add_tarif" placeholder="0" readonly >
											<input type="hidden" id="add_bakhp">
											<input type="hidden" id="add_js">
											<input type="hidden" id="add_jp">
											<input type="hidden" id="add_id_tarif">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input type="text" class="form-control text-right" id="add_onfaktur" placeholder="0">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Jumlah</label>
									<div class="col-md-5">
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input type="text" class="form-control text-right" id="add_jumlah" placeholder="0" readonly>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Paramedis</label>
									<div class="col-md-6">
										<input type="hidden" id="add_id_paramedis">
										<input type="text" class="form-control" id="add_paramedis" placeholder="Paramedis" required>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Paramedis Lain</label>
									<div class="col-md-6">
										<textarea class="form-control" id="add_paramedis_lain" placeholder="Paramedis Lain"></textarea>
									</div>
								</div>
			        </div>
		       	</div>
			      <br><br>
			      <div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
			 	     	<button type="submit" class="btn btn-success" id="addTindakan">Tambah</button>
						</div>
					</div>
				</div>
			</form>
		</div>

    <div class="tab-pane active" id="legal">
    	<div class="dropdown"  style="margin-left:10px;width:98.5%">
	      <div id="titleInformasi">Tambah Tindakan Mediko Legal</div>
	    
      </div>
      <br>
    
				<form class="form-horizontal" role="form" method="post" id="frmTambah">
				<div class="informasi">
					<div class="form-group">
						<label class="control-label col-md-3">Nama</label>
						<div class="col-md-3">
							<input type="text" class="form-control" placeholder="Nama" id="add_nama" required>
							<input type="hidden" id="add_invoice">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Umur</label>
						<div class="col-md-2">
							<div class="input-group">
								<input type="number" class="form-control" id="add_umur" placeholder="0" required>
								<span class="input-group-addon">tahun</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Jenis Kelamin</label>
						<div class="form-inline">
							<div class="radio-list">
								<div class="col-md-1" >
									<input type="radio" name="add_jenkel" id="add_lk" value="LAKI-LAKI" checked /><label class="control-label">Laki-laki</label>
								</div>
								<div class="col-md-2" style="margin-left:0px">
									<input type="radio" name="add_jenkel" value="PEREMPUAN" id="add_pr" /><label class="control-label">Perempuan</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Nomor Telepon</label>
						<div class="col-md-3">
							<input type="text" class="form-control" id="add_telp" placeholder="Nomor Telepon" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Alamat</label>
						<div class="col-md-3">
							<textarea class="form-control" id="add_alamat" placeholder="Alamat" required></textarea>
						</div>
	    		</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kelas Pelayanan</label>
						<div class="col-md-3">
							<select class="form-control" id="add_kelas">
								<option selected value=''>Pilih</option>
								<option value='Kelas III'>Kelas III</option>
								<option value='Kelas II'>Kelas II</option>
								<option value='Kelas I'>Kelas I</option>
								<option value='Kelas Utama'>Kelas Utama</option>
								<option value='Kelas VIP'>Kelas VIP</option>
							</select>
						</div>
					</div>
      	 	<div class="form-group">
    				<label class="control-label col-md-3"><a href="#tambahTindakan" data-toggle="modal" class="tmbhTndkan"><i class="fa fa-plus" >&nbsp;Tambah Tindakan</i></a></label>
    			</div>
    		</div>

    			<div class="tabelinformasi">
		        <div class="portlet-body" style="margin: 0px 10px 0px 10px">
	            <table class="table table-striped table-bordered table-hover tableDTUtamaScroll" style="font-size:90%" id="tableCare">
								<thead>
									<tr class="info">
										<th>Tindakan</th>
										<th>Waktu</th>
										<th>Paramedis</th>
										<th>Paramedis Lain</th>
										<th>BAKHP</th>
										<th>Jasa Sarana</th>
										<th>Jasa Pelayanan</th>
										<th>On Faktur</th>
										<th>Jumlah</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody id="tbodytindakan">
								</tbody>
							</table>
						</div>
						<div style="margin-right:40px;">
							<div class="form-group">
								<div class="col-md-2 pull-right">
									<label class="control-label pull-right" style="font-size:1.8em;margin-top:-10px;"><span id="total">0</span></label>
								</div>
								<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
									Total (Rp.) :
								</div>
							</div>
						</div>

						<br>

						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span style="padding:0px 10px 0px 10px;">
								<button type="reset" id="btnReset" class="btn btn-warning">RESET</button> &nbsp;
								<button type="submit" id="saveTindakan" class="btn btn-success">SIMPAN</button>
							</span>
						</div>
						<br>

	         <!--  <div class="form-group" style="margin-right:40px">
	          	<div class="pull-right">
								<button type="reset" id="btnReset" class="btn btn-warning">RESET</button>
								<button type="submit" id="saveTindakan" class="btn btn-success">SIMPAN</button>
		     			</div>
	    	 		</div> -->
	    	 	</div>
			  </form>
      
    </div>

		<div class="tab-pane" id="riwayat">
  		<div class="dropdown" style="margin-left:10px;width:98.5%">
	      <div id="titleInformasi">Riwayat Tindakan Mediko Legal</div>
	   
      </div>
      <br>
      <div class="modal fade" id="detailMediko" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-400px">
				<div class="modal-dialog">
					<div class="modal-content" style="width:1000px">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
							<h3 class="modal-title" id="myModalLabel">Detail</h3>
				    </div>
				    <div class="modal-body">
      			 <form class="form-horizontal" role="form">
							 <div class="form-group">
 								 <label class="control-label col-md-3">No. Invoice</label>
 								 <div class="col-md-5">
 									 <input type="text" class="form-control" placeholder="No. Invoice" id="pop_invoice" readonly>
 								 </div>
 							 </div>
							 <div class="form-group">
								 <label class="control-label col-md-3">Nama</label>
								 <div class="col-md-5">
									 <input type="text" class="form-control" placeholder="Nama" id="pop_nama" readonly>
								 </div>
							 </div>
							 <div class="form-group">
								 <label class="control-label col-md-3">Umur</label>
								 <div class="col-md-2">
									 <div class="input-group">
										 <input type="text" class="form-control" id="pop_umur" placeholder="0" readonly>
										 <span class="input-group-addon">tahun</span>
									 </div>
								 </div>
							 </div>
							 <div class="form-group">
								 <label class="control-label col-md-3">Jenis Kelamin</label>
								 <div class="col-md-3">
									 <input type="text" class="form-control" id="pop_jenkel" readonly>
								 </div>
							 </div>
							 <div class="form-group">
							 <label class="control-label col-md-3">Nomor Telepon</label>
							 <div class="col-md-5">
								 <input type="text" class="form-control" id="pop_telp" placeholder="Nomor Telepon" readonly>
							 </div>
						 </div>
						 <div class="form-group">
							 <label class="control-label col-md-3">Alamat</label>
							 <div class="col-md-5">
								 <textarea class="form-control" id="pop_alamat" readonly></textarea>
							 </div>
						 </div>
						 <div class="form-group">
							 <label class="control-label col-md-3">Kelas Pelayanan</label>
							 <div class="col-md-5">
								 <input class="form-control" id="pop_kelas" readonly>
							 </div>
						 </div>

						 <div class="form-group">
							 <div class="portlet-body" style="margin: 0px 10px 0px 10px">
								 <table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									 <thead>
											<tr class="info" >
												<th>No</th>
												<th>Tindakan</th>
												<th>Waktu Tindakan</th>
												<th>Paramedis</th>
												<th>Paramedis Lain</th>
												<th>Tarif</th>
												<th>On Faktur</th>
												<th>Total</th>
											</tr>
										</thead>
										<tbody id="tbodydetail">
											<tr class="kosong"><td colspan="8">Tidak ada detail</td></tr>
										</tbody>
									</table>
								</div>
								<div class="form-group pull-right" style="margin-right:20px">
									<div class="col-md-2" style="width:150px; margin-top:25px;">
										Sub Total :
									</div>
									<div class="col-md-2">
										<label class="control-label " style="font-size:3em;"><span id="pop_total">0</span></label>
									</div>
									
								</div>
							</div>
	 					</form>
				  </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					</div>
				</div>
			</div>
		</div>
 
    	<form class="form-horizontal" role="form">
	  		<div class="form-group">
			    <div class="portlet-body" style="margin: 0px 25px 0px 25px">
		        <table class="table table-striped table-bordered table-hover tableDT">
							<thead>
								<tr class="info">
									<th width="10">No.</th>
									<th>No. Invoice</th>
									<th>Nama</th>
									<th>Umur</th>
									<th>Jenis Kelamin</th>
									<th>Telepon</th>
									<th>Kelas</th>
									<th>Total Biaya</th>
									<th width="10">Action</th>
								</tr>
							</thead>
							<tbody id="tbodyriwayat">
								<?php
									if (isset($riwayat)) {
										if (!empty($riwayat)) {
											$i=0;
											foreach ($riwayat as $value) {
												echo '<tr>
													<td>'.(++$i).'</td>
													<td>'.$value['no_invoice'].'</td>
													<td>'.$value['nama'].'</td>
													<td>'.$value['umur'].'</td>
													<td>'.$value['jenis_kelamin'].'</td>
													<td>'.$value['no_telp'].'</td>
													<td>'.$value['kelas_pelayanan'].'</td>
													<td class="text-right">'.number_format($value['total'],0,'','.').'</td>
													<td style="text-align:center">
														<input type="hidden" id="val_alamat" value="'.$value['alamat'].'">
														<input type="hidden" id="val_id" value="'.$value['id'].'">
														<input type="hidden" id="val_invoice" value="'.$value['no_invoice'].'">

														<a href="#detailMediko" id="viewdetail" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>
													</td>
												</tr>';
											}
										}
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
	    </form>
  </div>
</div>
</div>


<!-- <script type="text/javascript">
	$(document).ready( function(){
			$('.tmbhTndkan').on('click',function(e){
				e.preventDefault();
				tambahTindakan('#tbhBaru','.tmbhTndkan','#tbhBaruTarif','#tbhBaruOnFaktur');
			});
	});

</script> -->
