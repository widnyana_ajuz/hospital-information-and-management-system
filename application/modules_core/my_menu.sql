-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2015 at 07:29 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lastfinalversion`
--

-- --------------------------------------------------------

--
-- Table structure for table `my_menu`
--

CREATE TABLE IF NOT EXISTS `my_menu` (
`menu_id` int(11) unsigned NOT NULL,
  `portal_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `menu_slug` varchar(255) DEFAULT NULL,
  `menu_desc` varchar(255) DEFAULT NULL,
  `menu_url` varchar(255) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `menu_st` varchar(255) NOT NULL DEFAULT 'show',
  `menu_display` varchar(255) NOT NULL DEFAULT 'yes',
  `menu_icon` varchar(255) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `dc` datetime DEFAULT NULL,
  `du` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1 COMMENT='INSERT INTO `my_menu` (`menu_id`, `portal_id`, `parent_id`, `menu_name`, `menu_slug`, `menu_desc`, `menu_url`, `menu_order`, `menu_st`, `menu_display`, `menu_icon`, `creator`, `dc`, `du`)\nVALUES\n	(50, 2, 41, ''Laporan Rawat Inap Anak'', ''laporan_rawat_inap_anak'', ''Laporan Rawat Inap Anak'', ''laporan/laporan_rawat_inap_anak'', 5, ''show'', ''yes'', ''dashboard'', 1, ''2012-07-01 00:01:00'', NULL);';

--
-- Dumping data for table `my_menu`
--

INSERT INTO `my_menu` (`menu_id`, `portal_id`, `parent_id`, `menu_name`, `menu_slug`, `menu_desc`, `menu_url`, `menu_order`, `menu_st`, `menu_display`, `menu_icon`, `creator`, `dc`, `du`) VALUES
(1, 1, 0, 'home', 'home', 'Dashboard Menu', 'dashboard/admin', 1, 'show', 'yes', 'home', 1, '2014-04-06 00:04:23', NULL),
(2, 1, 0, 'Portal', 'portal', 'Portal Management', 'setting/portal', 2, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:29:37', NULL),
(3, 1, 0, 'Menu', 'menu', 'Menu Management', 'setting/menu', 3, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:36:31', NULL),
(4, 1, 0, 'User', 'user', 'User Management', 'setting/user', 4, 'show', 'yes', 'user', 1, '2014-04-06 00:38:00', NULL),
(5, 1, 0, 'Role', 'role', 'Role Management', 'setting/role', 5, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:39:02', NULL),
(6, 1, 0, 'Permission', 'permission', 'Permission Management', 'setting/permission', 6, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:39:02', NULL),
(7, 2, 32, 'Data Karyawan', 'data_karyawan', 'Karyawan Management', 'master/homedatakaryawan', 1, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(8, 2, 32, 'Data Tarif', 'data_tarif', 'Tarif Management', 'master/homedatatarif', 2, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(9, 2, 32, 'Data Satuan', 'data_satuan', 'Satuan Management', 'master/homedatasatuan', 4, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(10, 2, 32, 'Data Kamar', 'data_kamar', 'Kamar Management', 'master/homedatakamar', 3, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(12, 2, 33, 'Logistik', 'registrasi', 'Pendaftaran Kunjungan', 'logistik/homegudangbarang', 1, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(13, 2, 33, 'Instalasi Gizi', 'rekam_medik', 'Daftar Pasien dan Riwayatnya', 'instalasigizi/homeinstalasigizi', 2, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(14, 2, 33, 'IPS-RS', 'igd', 'IGD Periksa', 'ipsrs/homeipsrs', 3, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(20, 2, 69, 'Pilih Kamar', 'pilih_kamar', 'Pilih Kamar', 'pilihkamar/pilih', 7, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(21, 2, 92, 'IGD', 'kasir', 'Kasir untuk Pembayaran', 'igd/homeigd', 1, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(22, 2, 33, 'UTD', 'pembelian_obat', 'Pembelian Obat', 'transfusidarah/hometransfusidarah', 4, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(23, 2, 33, 'Perawatan Jenazah', 'permintaan_apotik_utama', 'Permintaan', 'perawatanjenazah/homeperawatanjenazah', 5, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(24, 2, 33, 'Mediko Legal', 'kasir_obat', 'Kasir Obat', 'medikolegal/homemedikolegal', 6, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(25, 2, 92, 'Kasir', 'persetujuan_permintaan_obat', 'Persetujuan Permintaan Obat', 'kasirtindakan/homekasirtindakan', 2, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(26, 2, 32, 'Data Instansi Rujukan', 'data_instansi', 'Tarif Penunjang Management', 'master/homedatarujukan', 5, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(27, 2, 41, 'Poli Psikiatri Jiwa', 'stok_apotik_utama', 'Stok Apotik Managemen', 'polipsikiatri/homerawatjalan', 18, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(28, 2, 41, 'Poli Urologi', 'permintaan_igd', 'Permintaan Stok oleh IGD', 'poliurologi/homerawatjalan', 19, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(29, 2, 41, 'Poli Obsteri dan Ginekologi', 'stok_igd', 'Stok IGD Management', 'poliobsteri/homerawatjalan', 20, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(30, 2, 41, 'Poli Gizi', 'operasi', 'Operasi', 'poligizi/homerawatjalan', 21, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(31, 2, 72, 'Fisioterapi', 'registrasi_rawat_inap', 'Pendaftaran Rawat Inap', 'fisioterapi/homelab', 2, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(32, 2, 0, 'Master Data', 'master_data', 'Master Data Mega Menu', NULL, 1, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(33, 2, 0, 'Unit Pendukung', 'unit_utama', 'Unit Utama', NULL, 6, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(34, 2, 72, 'Radiologi', 'daftar_kamar', 'Daftar Kamar RS BLUD', 'radiologi/homelab', 3, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(35, 2, 72, 'EKG', 'persiapan_rawat_inap', 'Daftar Persiapan Rawat Inap di IGD', 'ekg/homelab', 4, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(36, 2, 0, 'Rawat Inap', 'rawat_inap', 'Rawat Inap Head Menu', '', 5, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(37, 2, 36, 'Kesehatan Anak', 'anak', 'Rawat Inap Anak', 'kia/homekia', 3, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(38, 2, 72, 'USG', 'pengambilan_obat', 'Pengambilan Obat', 'usg/homelab', 5, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(39, 2, 72, 'CT Scan', 'retur_obat', 'Retur Obat', 'ctscan/homelab', 6, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(40, 2, 72, 'MRI', 'kunjungan', 'Daftar_kunjungan', 'mri/homelab', 7, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(41, 2, 0, 'Rawat Jalan', 'rawat_jalan', 'Rawat Jalan', NULL, 4, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(42, 2, 41, 'Poli Umum', 'poli_umum', 'laporan gudang', 'rawatjalan/homerawatjalan', 1, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(43, 2, 41, 'Poli THT', 'laporan_apotik', 'Laporan Apotik', 'politht/homerawatjalan', 2, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(44, 2, 41, 'Poli Kulit Kelamin', 'laporan_operasi', 'Laporan Operasi', 'polikulitkelamin/homerawatjalan', 3, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(45, 2, 41, 'Poli Kandungan', 'laporan_igd', 'Laporan IGD', 'polikandungan/homerawatjalan', 4, 'show', 'yes', 'dashboard', 1, '2012-07-00 00:01:00', NULL),
(46, 2, 36, 'Bedah Umum', 'vip', 'Rawat Inap VIP', 'bedahumum/homebedahumum', 1, 'show', 'yes', 'dashboard', 1, '2012-07-00 00:01:00', NULL),
(47, 2, 72, 'Endoscopy', 'bedah', 'Rawat Inap Bedah', 'endoscopy/homelab', 8, 'show', 'yes', 'dashboard', 1, '2012-07-00 00:01:00', NULL),
(48, 2, 36, 'ICU', 'icu', 'Rawat Inap ICU', 'icu/homeicu', 6, 'show', 'yes', 'dashboard', 1, '2012-07-00 00:01:00', NULL),
(49, 2, 36, 'Penyakit Dalam', 'penyakit_dalam', 'Rawat Inap Penyakit Dalam', 'penyakitdalam/homepenyakitdalam', 2, 'show', 'yes', 'dashboard', 1, '2012-07-00 00:01:00', NULL),
(50, 2, 41, 'Poli KIA', 'laporan_rawat_inap_anak', 'Laporan Rawat Inap Anak', 'polikia/homerawatjalan', 5, 'show', 'yes', 'dashboard', 1, '2012-07-01 00:01:00', NULL),
(51, 2, 36, 'Bersalin', 'bersalin', 'Rawat Inap Bersalin', 'bersalin/homebersalin', 4, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(52, 2, 36, 'NICU', 'nicu', 'Rawat Inap NICU', 'nicu/homenicu', 7, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(53, 2, 41, 'Poli Mata', 'laporan_poliklinik', 'Laporan Poliklinik', 'polimata/homerawatjalan', 6, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(54, 2, 41, 'Poli Anak', 'laporan_rekam_medis', 'Laporan Rekam Medis', 'polianak/homerawatjalan', 7, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(55, 2, 41, 'Poli Penyakit Dalam', 'laporan_bersalin', 'Laporan Bersalin', '', 8, 'show', 'yes', 'dashboard', 1, NULL, NULL),
(56, 2, 0, 'Keuangan', 'keuangan', 'Keuangan', 'keuangan/homeunitperencanaan', 9, 'show', 'yes', 'dashboard', 1, NULL, NULL),
(57, 2, 56, 'Unit Pendapatan', 'unit_pendapatan', 'Unit Pendapatan', 'keuangan/homeunitpendapatan', 1, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(58, 2, 36, 'ICCU', 'deposit', 'Deposit', 'iccu/homeiccu', 8, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(59, 2, 56, 'Unit Realisasi', 'unit_realisasi', 'Unit Realisasi', 'keuangan/homeunitbendahara', 3, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(60, 2, 41, 'Poli Bedah', 'laporan_arus_kas', 'Laporan Arus Kas', 'polibedah/homerawatjalan', 9, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(61, 2, 41, 'Poli Gigi dan Mulut', 'laporan_pendapatan', 'Laporan Pendapatan', 'poligigi/homerawatjalan', 10, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(62, 2, 41, 'Poli PPT', 'riwayat_pembayaran', 'Riwayat Pembayaran', 'polippt/homerawatjalan', 12, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(63, 2, 41, 'Poli Tumbuh Kembang', 'laporan_rawat_inap_bersalin', 'Laporan Rawat Inap Bersalin', 'politumbuh/homerawatjalan', 13, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(64, 2, 41, 'Poli Laktasi', 'laporan_rawat_inap_nicu', 'Laporan Rawat Inap NICU', 'polilaktasi/homerawatjalan', 14, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(65, 2, 41, 'Poli Paru', 'laporan_rawat_inap_bedah', 'Laporan Rawat Inap Bedah', 'poliparu/homerawatjalan', 15, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(66, 2, 41, 'Poli Kardiologi', 'laporan_rawat_inap_penyakit_dalam', 'Laporan Rawat Inap Penyakit Dalam', 'polikardiologi/homerawatjalan', 16, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(67, 2, 41, 'Poli Syaraf', 'laporan_rawat_inap_icu', 'Laporan Rawat Inap ICU', 'polisyaraf/homerawatjalan', 17, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(68, 2, 41, 'Poli Bedah Orthopedi', 'laporan_rawat_inap_vip', 'Laporan Rawat Inap VIP', 'polibedaho/homerawatjalan', 11, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(69, 2, 0, 'Admisi', 'admisi', 'Admisi RJ, RI, Pilih Kamar', '', 2, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(70, 2, 69, 'Admisi Rawat Jalan', 'admisirawatjalan', 'Admisi RJ', 'pasien/daftarpasien', 5, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(71, 2, 69, 'Admisi Rawat Inap', 'admisirawatinap', 'Rawat Inap', 'pasien/rawatinap', 6, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(72, 2, 0, 'Unit Penunjang', 'unit_penunjang', 'Unit Penunjang', '', 6, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(73, 2, 72, 'Laboratorium', 'poli_umum', 'Poli Umum', 'laboratorium/homelab', 1, 'show', 'yes', NULL, NULL, NULL, NULL),
(74, 2, 32, 'Data Penyedia', 'data penyedia', 'penyedia', 'master/homedatapenyedia', 6, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(76, 2, 72, 'Hemodialisa', 'hemodialisa', 'Hemodialisa', 'hemodialisa/homelab', 9, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(77, 2, 0, 'Farmasi', 'farmasi', 'Seluruh Farmasi', '', 7, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(78, 2, 77, 'Gudang Obat', 'gudang_obat', 'Gudang Seluruh Obat', 'farmasi/homegudangobat', 1, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(79, 2, 77, 'Apotek Utama', 'apotek_utama', 'Apotek Utama', 'farmasi/homeapotikumum', 2, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(80, 2, 77, 'Apotek Depo', 'apotek_depo', 'Apotek Depo', 'farmasi/homeapotikdepo', 5, 'show', 'yes', NULL, NULL, NULL, NULL),
(81, 2, 77, 'Penjualan Obat (U)', 'penjualan_obat', 'Penjualan Obat', 'farmasi/homekasirobat', 3, 'show', 'yes', NULL, NULL, NULL, NULL),
(82, 2, 77, 'Retur Obat (U)', 'retur_obat', 'Retur Obat', 'farmasi/homereturobat', 4, 'show', 'yes', NULL, NULL, NULL, NULL),
(83, 2, 0, 'Rekam Medis', 'rekam_medik', 'Rekam Medis Pasien', NULL, 8, 'show', 'yes', NULL, NULL, NULL, NULL),
(84, 2, 56, 'Unit Perencanaan', 'perencanaan', 'perencanaan', 'keuangan/homeunitperencanaan', 2, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(85, 2, 36, 'Perinatologi', 'kardiologi', 'Kardiologi', 'perinatologi/homeperinatologi', 5, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(86, 2, 83, 'Olah Data Pasien', 'olahdatapasien', 'olahdatapasien', 'rekammedis/homeolahdatapasien', 1, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(87, 2, 83, 'Olah data Paramedis', 'olahdataparamedis', NULL, 'rekammedis/homeolahdataparamedis', 2, 'show', 'yes', NULL, NULL, NULL, NULL),
(88, 2, 83, 'Olah Data Diagnosa', 'olahdatadiagnosa', NULL, 'rekammedis/homeolahdatapenyakit', 3, 'show', 'yes', NULL, NULL, NULL, NULL),
(89, 2, 83, 'Olah Data Kamar', 'olahdatakamar', NULL, 'rekammedis/homeolahdatakamar', 4, 'show', 'yes', NULL, NULL, NULL, NULL),
(90, 2, 83, 'Laporan Rekam Medis', 'laporanrekammedis', '', 'rekammedis/homelaporanrekammedis', 5, 'show', 'yes', NULL, NULL, NULL, NULL),
(91, 2, 69, 'Pilih Kamar', 'pilihkamar', 'pilih kamar', 'pilihkamar/pilih', 7, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(92, 2, 0, 'Unit Utama', 'unit_utama', 'Unit Utama', NULL, 3, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(93, 2, 92, 'Kamar Operasi', 'persetujuan_permintaan_obat', 'Persetujuan Permintaan Obat', 'kamaroperasi/homeoperasi', 3, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(94, 2, 92, 'Ina CBG', 'kasir_obat', 'Kasir Obat', 'inacbg/homeinacbg', 4, 'show', 'yes', 'dashboard', 1, '2014-04-06 00:00:00', NULL),
(95, 2, 77, 'Retur Obat (D)', 'retur_obat', 'Retur Obat', 'farmasi/returobatdepo', 7, 'show', 'yes', NULL, NULL, NULL, NULL),
(96, 2, 77, 'Penjualan Obat (D)', 'penjualan_obat', 'Penjualan Obat', 'farmasi/kasirobatdepo', 6, 'show', 'yes', NULL, NULL, NULL, NULL),
(97, 2, 36, 'Isolasi', '', '', 'isolasi/homeisolasi', 9, 'show', 'yes', 'dashboard', NULL, NULL, NULL),
(98, 2, 36, 'Bedah Orthopedi', NULL, NULL, 'orthopedi/homeorthopedi', 10, 'show', 'yes', NULL, NULL, NULL, NULL),
(99, 2, 36, 'Bedah Syaraf', '', NULL, 'risyaraf/homerisyaraf', 11, 'show', 'yes', NULL, NULL, NULL, NULL),
(100, 2, 36, 'Psikologi', NULL, NULL, 'psikologi/homepsikologi', 12, 'show', 'yes', NULL, NULL, NULL, NULL),
(101, 2, 36, 'Jiwa', NULL, NULL, 'jiwa/homejiwa', 13, 'show', 'yes', NULL, NULL, NULL, NULL),
(103, 2, 36, 'Luka Bakar', NULL, NULL, 'lukabakar/homelukabakar', 15, 'show', 'yes', NULL, NULL, NULL, NULL),
(104, 2, 36, 'THT', NULL, NULL, 'rawattht/homerawattht', 16, 'show', 'yes', NULL, NULL, NULL, NULL),
(105, 2, 36, 'Mata', NULL, NULL, 'rawatmata/homerawatmata', 17, 'show', 'yes', NULL, NULL, NULL, NULL),
(106, 2, 36, 'Kulit & Kelamin', NULL, NULL, 'rawatkelamin/homerawatkelamin', 18, 'show', 'yes', NULL, NULL, NULL, NULL),
(107, 2, 36, 'Kardiologi', NULL, NULL, 'kardiologi/homekardiologi', 19, 'show', 'yes', NULL, NULL, NULL, NULL),
(108, 2, 36, 'Paru - Paru', NULL, NULL, 'rawatparu/homerawatparu', 20, 'show', 'yes', NULL, NULL, NULL, NULL),
(109, 2, 36, 'Geriatri', NULL, NULL, 'geriatri/homegeriatri', 21, 'show', 'yes', NULL, NULL, NULL, NULL),
(110, 2, 36, 'Kusta', NULL, NULL, 'kusta/homekusta', 22, 'show', 'yes', NULL, NULL, NULL, NULL),
(111, 2, 36, 'Kelas I', NULL, NULL, 'kelassatu/homekelassatu', 23, 'show', 'yes', NULL, NULL, NULL, NULL),
(112, 2, 36, 'Kelas II', NULL, NULL, 'kelasdua/homekelasdua', 24, 'show', 'yes', NULL, NULL, NULL, NULL),
(113, 2, 36, 'Kelas III', NULL, NULL, 'kelastiga/homekelastiga', 25, 'show', 'yes', NULL, NULL, NULL, NULL),
(114, 2, 36, 'Kelas Utama', NULL, '', 'kelasutama/homekelasutama', 26, 'show', 'yes', NULL, NULL, NULL, NULL),
(115, 2, 36, 'Kelas VIP', NULL, NULL, 'kelasvip/homekelasvip', 27, 'show', 'yes', NULL, NULL, NULL, NULL),
(116, 2, 41, 'Poli Geriatri', 'laporan_rawat_inap_vip', 'Laporan Rawat Inap VIP', 'rjgeriatri/homerjgeriatri', 11, 'show', 'yes', 'dashboard', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `my_menu`
--
ALTER TABLE `my_menu`
 ADD PRIMARY KEY (`menu_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `my_menu`
--
ALTER TABLE `my_menu`
MODIFY `menu_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=117;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
