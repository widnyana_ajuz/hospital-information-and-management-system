<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>pasien/daftarpasien">ADMISI RAWAT JALAN</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Pasien Rawat Jalan</a>
	</li>
</div>
<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
    	<li class="active"><a href="#lama"class="cl"  data-toggle="tab">Pasien Rawat Jalan</a></li>
    	<li><a href="#rujukan" class="cl" data-toggle="tab">Pasien Rujukan</a></li>
    	<li><a href="#kunjungan" class="cl" data-toggle="tab">Daftar Kunjungan</a></li>    	
	</ul>

	<div id="my-tab-content" class="tab-content">
		<div class="tab-pane active" id="lama">
    		<form class="form-horizontal" method="POST" id="search_submit">
		       	<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left: -400px">		
						<input type="text" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
					</div>
					<button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
					<a href="<?php echo base_url() ?>pasien/pendaftaran/registrasi/rawatjalan" class="btn btn-warning"> Daftar Pasien Baru</a>
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px; margin-left: -50px;">PASIEN RAWAT INAP</p>
			</div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">
				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
						<div class="teble-responsive">
							<table  class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tableSearch">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>#Rekam Medis</th>
										<th>Nama Lengkap</th>
										<th>Jenis Kelamin</th>
										<th>Tanggal Lahir</th>
										<th>Alamat Tinggal</th>
										<th>Identitas</th>
										<th width="70">Daftarkan</th>
									</tr>
								</thead>
								<tbody id="t_body">
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<br><br><br><br>  
	    </div>

	    <div class="tab-pane" id="rujukan">
       		<form class="form-horizontal" method="POST" id="search_rujuk">
				<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>

					<div class="col-md-4" style="margin-left:-400px">		
						<input type="text" class="form-control" id="text_rujuk" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
					</div>
					<button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
				</div>
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px; margin-left: -50px;">PASIEN RUJUKAN RAWAT JALAN</p>
			</div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px 0px 10px 0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -12px 0px -95px 0px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_Rujuk">
							<thead>
								<tr class="info">
									<th >No.</th>
									<th>#Rekam Medis</th>
									<th>Nama Pasien</th>
									<th>Poli Asal</th>
									<th>Poli Tujuan</th>
									<th>Dokter Pengirim</th>
									<th>Tanggal Lahir</th>
									<th>Alamat</th>
									<th>Jenis Kelamin</th>
									<th>Tanggal Daftar</th>
									<th>Action</th>
							</tr>
							</thead>
							<tbody id="tbody_rujuk_rj">
								
								<?php
									$i = 0;
									if(!empty($pasien_rujuk)){
										foreach ($pasien_rujuk as $val) {
											$data = $val;
											$i++;
											$tgl = strtotime($val['tanggal_lahir']);
											$hasil = date('d F Y', $tgl);

											$tgl = strtotime($val['tanggal_visit']);
											$tglvisit = date('d F Y', $tgl);

											echo"<tr>";
												echo"<td>".$i."</td>";
												echo"<td>".$val['rm_id']."</td>";
												echo"<td>".$val['nama']."</td>";
												echo"<td>".$val['nama_asal']."</td>";
												echo"<td>".$val['nama_rujuk']."</td>";
												echo"<td>".$val['nama_petugas']."</td>";
												echo"<td align='center'>".$hasil."</td>";
												echo"<td>".$val['alamat_skr']."</td>";
												echo"<td>".$val['jenis_kelamin']."</td>";									
												echo"<td>".$tglvisit."</td>";
												echo'<td style="text-align:center">';
													echo'<a href="#view" data-toggle="modal" data-original-title="View" onClick="visitRujuk(&quot;'.$val['rj_id'].'&quot;)">';
													echo'<i class="glyphicon glyphicon-edit"data-toggle="tooltip" data-placement="top" title="Edit"></i></a>';
													echo'<a href="#" data-toggle="modal" data-original-title="Delete" onClick="batalRujuk(&quot;'.$val['visit_id'].'&quot;)">';
													echo'<i class="glyphicon glyphicon-trash"data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>';
												echo"</td>";
											echo"</tr>";
										}
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<br><br><br><br>
        </div>

		<div class="tab-pane" id="kunjungan" style="padding: 0px 10px 0px 10px">
        	<!-- <form class="form-horizontal" method="POST" id="submitSearchKunjungan">
	       		<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px">		
						<input type="text" id="searchPasienKunjung" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
					</div>
					<button type="submit" class="btn btn-info">Cari</button>
				</div>	
			</form> 
			<br>	
			<hr class="garis"> -->

			<div role="form">
				<div class="portlet box red" >
					<div class="portlet-body" >
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" style="table-layout:fixed; color:black" id="table_kunjungan">
							<thead>
								<tr class="info">
									<th width="3%">No.</th>
									<th width="10%">#Rekam Medis</th>
									<th width="15%">Nama</th>
									<th width="10%">Poliklinik Tujuan</th>
									<th width="10%">Tanggal Lahir</th>
									<th>Alamat</th>
									<th width="10%">Jenis Kelamin</th>
									<th width="10%">Tanggal Daftar</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody id="tbody_rj">
								<?php
									$no = 0;
									foreach ($pasien_kunjungan as $data) {
										$tgl = strtotime($data['tanggal_lahir']);
										$hasil = date('d F Y', $tgl);

										$tgl_masuk = strtotime($data['waktu_masuk']);
										$hasil_masuk = date('d F Y', $tgl_masuk);
										$tgl_visit = date('d/m/Y', $tgl);

										echo '
											<tr>
												<td style="text-align:center">'.++$no.'</td>
												<td>'.$data['rm_id'].'</td>
												<td style="word-wrap: break-word;white-space: pre-wrap; ">'.$data['nama'].'</td>
												<td>'.$data['nama_dept'].'</td>
												<td align="center">'.$hasil.'</td>
												<td style="word-wrap: break-word;white-space: pre-wrap; ">'.$data['alamat_skr'].'</td>
												<td>'.$data['jenis_kelamin'].'</td>
												<td align="center">'.$hasil_masuk.'</td>
												<td style="text-align:center">
													<input type="hidden" class="tgl_visit" value="'.$tgl_visit.'">
													<input type="hidden" class="visit_id" value="'.$data['visit_id'].'">
													<input type="hidden" class="rj_id" value="'.$data['rj_id'].'">
													<input type="hidden" class="kj_cb" value="'.$data['cara_bayar'].'">
													<input type="hidden" class="kj_nama" value="'.$data['nama_asuransi'].'">
													<input type="hidden" class="kj_no" value="'.$data['no_asuransi'].'">
													<input type="hidden" class="kj_perusahaan" value="'.$data['nama_perusahaan'].'">
													<input type="hidden" class="kj_kelas" value="'.$data['kelas_pelayanan'].'">
													<a href="#editrj" class="viewico klikkunjung" data-toggle="modal" data-original-title="Edit Data Pasien"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
												</td>
											</tr>
										';
									}
								?>
								
							</tbody>
						</table>	
					</div>
				</div>
			</div>

			<br><br><br><br>
        </div>
	</div>
</div>
		<!-- modal edit rawat jalan -->
	<div class="modal fade" id="view" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	       	<div class="modal-content">
	        	<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        		<h3 class="modal-title" id="myModalLabel">View Data Pasien</h3>
	        	</div>	

	        	<div class="modal-body" style="padding:40px">
		        	<form class="form-horizontal" role="form" id="submit_tindakrujuk">
		        		<div class="form-group">
							<label class="control-label col-md-3" >Tanggal Daftar </label>
							<div class="col-md-3">	
								<input type="text" class="form-control" name="date" id="date_rujuk" value="<?php echo date("d/m/Y");?>" readonly />
							</div>				
						</div>	
							
						<div class="form-group">
							<label class="control-label col-md-3">No. Rekam Medis</label>
							<div class="col-md-7">
								<input type="hidden" id="visit_rujuk"/>
								<input type="text" class="form-control" id="rm_rujuk"  name="noRm" value="RJ100222" readonly/>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Nama Pasien</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="nama_rujuk" name="nama" value="Khrisna" readonly>
							</div>
						</div>
								
						<div class="form-group">
							<label class="control-label col-md-3">Poli Asal</label>
							<div class="col-md-7">
								<input type="hidden" id="idasal_rujuk">
								<input type="text" class="form-control" id="asal_rujuk"  name="alas" value="Poli Gigi" readonly>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Poli Tujuan</label>
							<div class="col-md-5">
								<select class="form-control select" name="poli" id="poli_rujuk">
									<option value="" selected>Pilih Poliklinik</option>
									<?php foreach( $poliklinik as $poli ) { ?>
										<option value="<?php echo $poli['dept_id']; ?>" >
											<?php echo $poli['nama_dept']; ?>
										</option>
									<?php } ?>	
								</select>												
							</div>
						</div>
									
						<div class="form-group">
							<label class="control-label col-md-3">Cara Bayar</label>
							<div class="col-md-5">
								<select class="form-control select" name="carabayar" id="carabayar_rujuk">
									<option value="" selected>Pilih Cara Bayar</option>
									<option value="Umum">Umum</option>
									<option value="BPJS" id="op-bpjs">BPJS</option>
									<option value="Jamkesmas" >Jamkesmas</option>
									<option value="Asuransi" id="op-asuransi">Asuransi</option>
									<option value="Kontrak" id="op-kontrak">Kontrak</option>
									<option value="Gratis" >Gratis</option>
									<option value="Lain">Lain-lain</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group" id="asuransi_rujuk">
							<label class="control-label col-md-3">Nama Asuransi</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="namaAsuransi_rujuk" name="namaAsuransi" placeholder="Nama Asuransi">
							</div>
						</div>
								
						<div class="form-group" id="kontrak_rujuk">
							<label class="control-label col-md-3">Nama Perusahaan</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="perusahaan_rujuk" name="namaPerusahaan" placeholder="Nama Perusahaan">
							</div>
						</div>

						<div class="form-group" id="kelasP_rujuk">
							<label class="control-label col-md-3">Kelas Pelayanan </label>
							<div class="col-md-5">
								<select class="form-control select" name="kelas_pelayanan" id="kelas_rujuk">
									<option value="" selected>Pilih Kelas BPJS</option>
									<option value="III">III</option>
									<option value="II">II</option>
									<option value="I"  >I</option>
									<option value="Utama" >Utama</option>
									<option value="VIP">VIP</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group" id="noAsuransi_rujuk">
							<label class="control-label col-md-3">Nomor Asuransi</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="nomorAsuransi" id="nomorAsuransi_rujuk" placeholder="Nomor Asuransi">
							</div>
						</div>

	    		</div>
	    	
		    	<br>
		    	<div class="modal-footer">
		    		<button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>	
		 	   		<button type="submit" class="btn btn-success">Simpan</button>
			   	</div>

			   	</form>
			</div>
	    </div>
	</div>

	<div class="modal fade" id="editrj" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	       	<div class="modal-content">
	       	<form method="POST" id="submitEditKunjungan">
	        	<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        		<h3 class="modal-title" id="myModalLabel">Edit Data Pasien Rawat Jalan</h3>
	        	</div>	

	        	<div class="modal-body">
	        		<div class="form-group">
						<label class="control-label col-md-3" >Tanggal Daftar </label>
						<div class="col-md-3">	
							<input type="hidden" id="kunjung_rj_id">
							<input type="hidden" id="kunjung_visit_id">
							<input type="hidden" id="kunjung_cb">
							<input type="hidden" id="kunjung_nama">
							<input type="hidden" id="kunjung_no">
							<input type="hidden" id="kunjung_perusahaan">
							<input type="hidden" id="kunjung_kelas">
							<input type="text" id="kunjung_date" class="form-control" name="date"  value="<?php echo date("d/m/Y");?>" readonly />
						</div>				
					</div>	
						
					<div class="form-group">
						<br><br>
						<label class="control-label col-md-3">No. Rekam Medis</label>
						<div class="col-md-7">
							<input type="text" id="kunjung_rm" class="form-control"  name="rm_rujuk" value="" readonly/>
						</div>
					</div>

					<div class="form-group">
						<br><br>
						<label class="control-label col-md-3">Nama Pasien</label>
						<div class="col-md-7">
							<input type="text" id="kunjung_namapasien" class="form-control"  name="nama" value="" readonly>
						</div>
					</div>
							
					<div class="form-group"><br><br>
						<label class="control-label col-md-3">Poliklinik</label>
						<div class="col-md-5">
							<select class="form-control select" name="poli" id="poli_kunjung" required>
								<option value="" selected>Pilih Poliklinik</option>
									<?php foreach( $poliklinik as $poli ) { ?>
										<option value="<?php echo $poli['dept_id']; ?>" >
											<?php echo $poli['nama_dept']; ?>
										</option>
									<?php } ?>	
							</select>												
						</div>
					</div>
								
					<div class="form-group"><br><br>
						<label class="control-label col-md-3">Tgl. Lahir</label>
						<div class="col-md-7">
							<input type="text" id="kunjung_lahir" class="form-control" name="tglLahir" value="" readonly>
						</div>
					</div>
								
					<div class="form-group"><br><br>
						<label class="control-label col-md-3">Alamat</label>
						<div class="col-md-7">
							<input type="text" id="kunjung_alamat" class="form-control" name="alamat" value="" readonly>
						</div>
					</div>

					<div class="form-group"><br><br>
						<label class="control-label col-md-3">Jenis Kelamin</label>
						<div class="col-md-7">
							<input type="text" id="kunjung_jk" class="form-control" id="jk_rujuk" name="jk" value="" readonly>											
						</div>
					</div>
	    		</div>
	    	
		    	<br><br>
		    	<div class="modal-footer">
		    		<button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>	
		 	   		<button type="submit" class="btn btn-success">Simpan</button>
			   	</div>
			</form>
			</div>
	    </div>
	</div>

	<div class="modal fade" id="tambahPemeriksaan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	   	<form method="POST" id="submitPemeriksaan">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Pemeriksaan</h3>
        			</div>	
        			<div class="modal-body">
        				<div class="form-group">
							<label class="control-label col-md-3" >Tanggal Periksa </label>
							<div class="col-md-3">	
								<input date-date-format="dd/mm/yyyy" value="<?php echo date("d/m/Y");?>" type="text" class="form-control" name="date" id="inputdate" disabled/>
							</div>				
						</div>	
						
						<div class="form-group">
						<br><br>
							<label class="control-label col-md-3">No. Rekam Medis</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="noRm" placeholder="No Rekam Medis" id="modal_no_rm" disabled>
							</div>
						</div>

						<div class="form-group">
						<br><br>
							<label class="control-label col-md-3">Nama Pasien</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="nama" placeholder="Nama Pasien" id="modal_nama" disabled>
							</div>
						</div>
						
						<div class="form-group"><br><br>
							<label class="control-label col-md-3">Poliklinik</label>
							<div class="col-md-5">
								<input type="hidden" id="status_visit">
								<select class="form-control select" name="poli" id="poli">
									<option value="" selected>Pilih Poliklinik</option>
									<?php foreach( $poliklinik as $poli ) { ?>
										<option value="<?php echo $poli['dept_id']; ?>" >
											<?php echo $poli['nama_dept']; ?>
										</option>
									<?php } ?>
								</select>												
							</div>
						</div>
						
						<div class="form-group" id="dcarabayar"><br><br>
							<label class="control-label col-md-3">Cara Bayar</label>
							<div class="col-md-5">
								<select class="form-control select" name="carabayar" id="carabayar">
									<option value="" selected>Pilih Cara Bayar</option>
									<option value="Umum">Umum</option>
									<option value="BPJS" id="op-bpjs">BPJS</option>
									<option value="Jamkesmas" >Jamkesmas</option>
									<option value="Asuransi" id="op-asuransi">Asuransi</option>
									<option value="Kontrak" id="op-kontrak">Kontrak</option>
									<option value="Gratis" >Gratis</option>
									<option value="Lain-laun">Lain-lain</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group" id="asuransi"><br><br>
							<label class="control-label col-md-3">Nama Asuransi</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="namaAsuransi" name="namaAsuransi" placeholder="Nama Asuransi">
							</div>
						</div>
								
						<div class="form-group" id="kontrak"><br><br>
							<label class="control-label col-md-3">Nama Perusahaan</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="namaPerusahaan" name="namaPerusahaan" placeholder="Nama Perusahaan">
							</div>
						</div>

						<div class="form-group" id="kelas"><br><br>
							<label class="control-label col-md-3">Kelas Pelayanan </label>
							<div class="col-md-5">
								<select class="form-control select" name="kelas_pelayanan" id="kelas_pelayanan">
									<option value="" selected>Pilih Kelas BPJS</option>
									<option value="III">III</option>
									<option value="II">II</option>
									<option value="I"  >I</option>
									<option value="Utama" >Utama</option>
									<option value="VIP">VIP</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group" id="noasuransi"><br><br>
							<label class="control-label col-md-3">Nomor Asuransi</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="nomorAsuransi" id="nomorAsuransi" placeholder="Nomor Asuransi">
							</div>
						</div>
						
						<div class="form-group"><br><br>
							<label class="control-label col-md-3">Cara Masuk</label>
							<div class="col-md-5">
								<select class="form-control select" name="caramasuk" id="caramasuk">
									<option value="" selected>Pilih Cara Masuk</option>
									<option value="Datang sendiri">Datang sendiri</option>
									<option value="Puskesmas"  >Puskesmas</option>
									<option value="Rujuk RS lain" >Rujuk RS lain</option>
									<option value="Instansi" >Instansi</option>
									<option value="Kasus Polisi" >Kasus Polisi</option>
									<option value="Rujukan Dokter" >Rujukan Dokter</option>
									<option value="Lain-laun">Lain-lain</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group"><br><br>
							<label class="control-label col-md-3">Detail Cara Masuk</label>
							<div class="col-md-7">
								<textarea class="form-control" name="detailMasuk" id="detailmasuk" placeholder="Detail cara masuk .."></textarea> 
							</div>
						</div>
						<div class="form-group" id="djenis_periksa"><br><br><br>
							<label class="control-label col-md-3">Jenis Pemeriksaan</label>
							<div class="col-md-7">
								<textarea class="form-control" name="detailMasuk" id="jenis_pemeriksaan" placeholder="Detail cara masuk .."></textarea> 
							</div>
						</div>
						<br><br>
						<div class="form-group"><br>
							<label class="control-label col-md-3">Adminitrasi</label>
							<div class="col-md-5">
								<select class="form-control select" name="caramasuk" id="adminitrasi">
									<option value="" selected>Pilih Adminitrasi</option>
									<option value="1">Pasien Lama</option><!-- tarif 3000 -->
									<option value="0">Pasien Baru</option><!-- tarif 5000 -->
									<option value="NULL" >Pasien Lanjutan</option>
								</select>												
							</div>
						</div>
      				</div>
      				<br><br>
      				<div class="modal-footer">
      					<button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
 			       		<button type="submit" class="btn btn-success">Simpan</button>
			      	</div>

        		</div>
        	</div>        	
        </form>
    </div>