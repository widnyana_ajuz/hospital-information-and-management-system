<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>pasien/rawatinap/">ADMISI RAWAT INAP</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Pasien Bersalin & NICU</a>
	</li>
</div>

<style type="text/css">
.cinemabox{
	
	color: white;
	cursor: pointer;
	width: 100px;
	height: 100px;
	float: left;
	margin-right: 15px;
	margin-bottom: 15px;
}	
.cinemabox label{
	margin-top: 50px;
	cursor: pointer;
}	
.cinemabox .rusak label{
	margin-top: 50px;
	cursor: default;
}	

.cinemabox .terpakai{
	background: #D9534F;
	width: 100px;
	height: 100px;
	cursor: default;
	
}

.cinemabox .rusak{
	background: #C0C0C0;
	width: 100px;
	height: 100px;
	cursor: default;
}

.cinemabox .kosong{
	background: #A7FFAE;
	width: 100px;
	height: 100px;
	cursor: pointer;
	
}
.cinemabox .kosong:hover{
	background: #F7FB86;
	width: 100px;
	height: 100px;
	cursor: pointer;
	
}

	
</style>


<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
    	<li class="active"><a href="#lama" class="cl" data-toggle="tab">Pasien Bersalin & NICU</a></li>
    	<li><a href="#rujukan" class="cl" data-toggle="tab">Pasien Rujukan</a></li>
    	<li><a href="#kunjungan" class="cl" data-toggle="tab">Daftar Kunjungan</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
		<div class="tab-pane active" id="lama">
			<form class="form-horizontal" method="POST" id="search_submit">
	       		<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left: -400px">		
						<input type="text" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
					</div>
					<button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
					<a href="<?php echo base_url() ?>pasien/pendaftaran/registrasi/rawatinap" class="btn btn-warning"> Daftar Pasien Baru</a>
				</div>
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">PASIEN RAWAT INAP</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">

					<div class="teble-responsive">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" style="table-layout:fixed" id="tableSearch">
							<thead>
								<tr class="info">
									<th width="3%">No.</th>
									<th width="10%">#Rekam Medis</th>
									<th width="15%">Nama Lengkap</th>
									<th width="10%">Jenis Kelamin</th>
									<th width="15%">Tanggal Lahir</th>
									<th>Alamat Tinggal</th>
									<th width="10%">Identitas</th>
									<th width="7%">Daftarkan</th>
								</tr>
							</thead>
							<tbody id="t_body">
							</tbody>
						</table>
					</div>
				</div>
			</div>     
		</div>
		<br> 
		<br><br><br><br>    
		</div>

		<div class="tab-pane" id="rujukan"> <!-- rujukan is here -->
       		<form class="form-horizontal" method="POST" id="search_rujukan">
	       		<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left: -400px">		
						<input type="text" id="rujuk_input" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
					</div>
					<button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">PASIEN RUJUKAN</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

			<div class="portlet box red">
				<div class="portlet-body" style="margin: -11px 0px -85px 0px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabel_rujuk">
							<thead>
								<tr class="info">
									<th style="width:20px;">No.</th>
									<th>#Rekam Medis</th>
									<th>Nama Pasien</th>
									<th>Jenis Kelamin</th>
									<th>Tanggal Lahir</th>
									<th>Rujukan dari Unit</th>
									<th width="80">Daftarkan</th>
								</tr>
							</thead>
							<tbody id="tbody_rujuk">
								<?php
									$i = 0;
									foreach ($pasien_rujuk as $key) {
										if(!empty($pasien_rujuk)){
											$i++;
											$tgl = strtotime($key['tanggal_lahir']);
											$hasil = date('d F Y', $tgl); 
											echo '
												<tr>
													<td>'.$i.'</td>
													<td>'.$key['rm_id'].'</td>
													<td>'.$key['nama'].'</td>
													<td>'.$key['jenis_kelamin'].'</td>										
													<td align="center">'.$hasil.'</td>
													<td>'.$key['nama_dept'].'</td>
													<td style="text-align:center">
														<a href="#daftarkanrujukan" data-toggle="modal" data-original-title="Tambah Pemeriksaan" onClick="rujuk(&quot;'.$key['nama'].'&quot;,&quot;'.$key['rm_id'].'&quot;,&quot;'.$key['visit_id'].'&quot;)">
														<i class="fa fa-plus"data-toggle="tooltip" data-placement="top" title="Tambah Pemeriksaan"></i></a>
													</td>										
												</tr>
											';
										}
									}
								?>
							</tbody>
						</table>
				</div>
			</div>
			</div>


			<br><br><br><br>
        </div>

        <div class="tab-pane" id="kunjungan">
        	<!-- <form class="form-horizontal" class="form-horizontal" method="POST" id="search_kunjungan">
	       		<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">		
						<input type="text" id="input_kunjungan" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
					</div>
					<button type="submit" class="btn btn-info">Cari</button>
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;"> -->
			
			<div role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" style="table-layout:fixed; color:black" id="table_kunjungan">
							<thead>
								<tr class="info" >
									<th width="3%">No.</th>
									<th width="10%">#Rekam Medis</th>
									<th width="15%">Nama</th>
									<th width="10%">Kamar Inap</th>
									<th width="13%">Unit</th>
									<th width="12%">Tanggal Lahir</th>
									<th>Alamat</th>
									<th width="10%">Jenis Kelamin</th>
									<th width="10%">Tanggal Daftar</th>
									
								</tr>
							</thead>
							<tbody>
								<?php
									$no = 0;
									foreach ($pasien_kunjungan as $data) {
										$tgl = strtotime($data['tanggal_lahir']);
										$hasil = date('d F Y', $tgl);

										$tgl_masuk = strtotime($data['waktu_masuk']);
										$hasil_masuk = date('d F Y', $tgl_masuk);

										echo '
											<tr>
												<td align="center">'.++$no.'</td>
												<td>'.$data['rm_id'].'</td>
												<td style="word-wrap: break-word;white-space: pre-wrap; ">'.$data['nama'].'</td>
												<td>'.$data['nama_kamar'].'</td>										
												<td>'.$data['nama_dept'].'</td>
												<td align="center">'.$hasil.'</td>
												<td style="word-wrap: break-word;white-space: pre-wrap; ">'.$data['alamat_skr'].'</td>
												<td>'.$data['jenis_kelamin'].'</td>
												<td align="center">'.$hasil_masuk.'</td>
											</tr>
										';
									}
								?>
								
							</tbody>
						</table>	
						
					</div>
				</div>
			</div>

			<br><br><br><br>
        </div>

        <div class="modal fade" id="viewri" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		       	<div class="modal-content">
		        	<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        		<h3 class="modal-title" id="myModalLabel">View Data Pasien Rawat Inap</h3>
		        	</div>	

		        	<div class="modal-body">
		        		<div class="form-group">
							<label class="control-label col-md-3" >Tanggal Daftar </label>
							<div class="col-md-3">	
								<input type="text" class="form-control" name="date" id="inputdate" value="<?php echo date("d/m/Y");?>" readonly />
							</div>				
						</div>	
							
						<div class="form-group">
							<br><br>
							<label class="control-label col-md-3">No. Rekam Medis</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="noRm" value="RJ1002" readonly/>
							</div>
						</div>

						<div class="form-group">
							<br><br>
							<label class="control-label col-md-3">Nama Pasien</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="nama" value="Khrisna" readonly>
							</div>
						</div>

						<div class="form-group">
							<br><br>
							<label class="control-label col-md-3">Kamar Pasien</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="kamar" value="B21" readonly>
							</div>
						</div>
								
						<div class="form-group">
							<br><br>
							<label class="control-label col-md-3">Department/Unit</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="department" value="Kandungan" readonly>
							</div>
						</div>
									
						<div class="form-group"><br><br>
							<label class="control-label col-md-3">Dokter Pemeriksa</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="namaDokter" value="dr. Jems" readonly>												
							</div>
						</div>
									
						<div class="form-group"><br><br>
							<label class="control-label col-md-3">Tgl. Lahir</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="tglLahir" name="tglLahir" value="20-10-1992" readonly>
							</div>
						</div>
									
						<div class="form-group"><br><br>
							<label class="control-label col-md-3">Alamat</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="alamat" name="alamat" value="Rumahnya" readonly>
							</div>
						</div>

						<div class="form-group"><br><br>
							<label class="control-label col-md-3">Jenis Kelamin</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="jk" name="jk" value="Laki-laki" readonly>											
							</div>
						</div>
		    		</div>
		    	
			    	<br><br>
			    	<div class="modal-footer">
			 	   		<button type="button" class="btn btn-warning" data-dismiss="modal">Kembali</button>
				   	</div>
				</div>
		    </div>
		</div>
        
        <div class="modal fade" id="daftarkan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Tindakan Rawat Inap</h3>
        			</div>	
        			<div class="modal-body">
        			<form id="submitDaftarkan" method="POST" class="form-horizontal" role="form">
        				<div class="form-group">
							<label class="control-label col-md-3" >Tanggal Periksa </label>
							<div class="col-md-3">	
								<input date-date-format="dd/mm/yyyy H:i:s" value="<?php echo date("d/m/Y H:i:s");?>" type="text" class="form-control" name="date" id="inputdate" placeholder="Date Now" disabled/>
							</div>				
						</div>	
						
						<div class="form-group">
							<label class="control-label col-md-3">No. Rekam Medis</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="modal_no_rm" name="noRm" placeholder="No Rekam Medis" disabled>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Nama Pasien</label>
							<div class="col-md-7">
								<input type="text" id="modal_nama" class="form-control" name="nama" placeholder="Nama Pasien" disabled>
							</div>
						</div>
													
						<div class="form-group">
							<label class="control-label col-md-3">Cara Bayar</label>
							<div class="col-md-5">
								<select class="form-control select" name="carabayar" id="carabayar" required>
									<option value="" selected>--Pilih Cara Bayar--</option>
									<option value="Umum">Umum</option>
									<option value="BPJS" id="op-bpjs">BPJS</option>
									<option value="Jamkesmas" >Jamkesmas</option>
									<option value="Asuransi" id="op-asuransi">Asuransi</option>
									<option value="Kontrak" id="op-kontrak">Kontrak</option>
									<option value="Gratis" >Gratis</option>
									<option value="Lain-laun">Lain-lain</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group" id="asuransi">
							<label class="control-label col-md-3">Nama Asuransi</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="namaAsuransi" name="namaAsuransi" placeholder="Nama Asuransi">
							</div>
						</div>
								
						<div class="form-group" id="kontrak">
							<label class="control-label col-md-3">Nama Perusahaan</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="namaPerusahaan" name="namaPerusahaan" placeholder="Nama Perusahaan">
							</div>
						</div>

						<div class="form-group" id="kelas">
							<label class="control-label col-md-3">Kelas Pelayanan </label>
							<div class="col-md-5">
								<select class="form-control select" name="kelasBpjs" id="kelas_pelayanan">
									<option value="" selected>--Pilih Kelas--</option>
									<option value="III">III</option>
									<option value="II">II</option>
									<option value="I"  >I</option>
									<option value="Utama" >Utama</option>
									<option value="VIP">VIP</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group" id="noasuransi">
							<label class="control-label col-md-3">Nomor Asuransi</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="nomorAsuransi" name="nomorAsuransi" placeholder="Nomor Asuransi">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Cara Masuk</label>
							<div class="col-md-5">
								<select class="form-control select" name="caramasuk" id="caramasuk" required>
									<option value="" selected>--Pilih Cara Masuk--</option>
									<option value="Datang sendiri">Datang sendiri</option>
									<option value="Puskesmas"  >Puskesmas</option>
									<option value="Rujuk RS lain" >Rujuk RS lain</option>
									<option value="Instansi" >Instansi</option>
									<option value="Kasus Polisi" >Kasus Polisi</option>
									<option value="Rujukan Dokter" >Rujukan Dokter</option>
									<option value="Lain-laun">Lain-lain</option>
								</select>									
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Detail Cara Masuk</label>
							<div class="col-md-7">
								<textarea class="form-control" name="detailMasuk" id="detailmasuk" placeholder="Detail cara masuk .."></textarea> 
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Departemen Tujuan</label>
							<div class="col-md-6">
								<select class="form-control select" id="deptTujuan" required>
									<option value="" selected>--Pilih Departemen--</option>
									<?php foreach( $departemen as $dep ) { ?>
										<option value="<?php echo $dep['dept_id']; ?>" >
											<?php echo $dep['nama_dept']; ?>
										</option>
										<?php } ?>
								</select>												
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Pilih Kamar & Kelas Kamar</label>
							<div class="col-md-4">
								<input type="hidden" id="kamar_id" name="kamar_id">
								<input type="hidden" id="bed_id" name="bed_id">
								<input type="text" class="form-control" id="kamar" placeholder="Search Kamar" disabled="true" data-toggle="modal" data-target="#pilkamar" required>
							</div>
						</div>				

						<div class="form-group">
						<label class="control-label col-md-3">Adminitrasi</label>
						<div class="col-md-5">
							<select class="form-control select" name="caramasuk" id="adminitrasi" required>
								<option value="" selected>Pilih Adminitrasi</option>
								<option value="1">Pasien Lama</option><!-- tarif 3000 -->
								<option value="0">Pasien Baru</option><!-- tarif 5000 -->
								<option value="NULL" >Pasien Lanjutan</option>
							</select>												
						</div>
					</div>
      				</div>
      				<div class="modal-footer">
	      				<button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
 			       		<button type="submit" class="btn btn-success">Simpan</button>
			      	</div>
			    </form>
        		</div>
	        </div>


	    <!-- <div class="modal fade" id="pilkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-300px">
        	<div class="modal-dialog">
        		<div class="modal-content" style="width:900px">
        			<div class="modal-header">
        				<button type="button" class="close" id="close-kamar" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Kamar</h3>
        			</div>	
        			<div class="modal-body">

        				<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchPengirim">
								<thead>
									<tr class="success">
										<td>Kamar</td>
										<td>Kelas</td>
										<td>Jumlah Bed</td>
										<td>Terpakai</td>
										<td width="10%" style="text-align:center;">Pilih</td>
									</tr>
								</thead>
								<tbody id="tbody_kamar">
									
								</tbody>
							</table>												
						</div>
	        			
      				</div>
      				<br>
      				<div class="modal-footer">
 			       		<button type="button" id="modal-kamar" class="btn btn-warning">Keluar</button>	
 			       	</div>

        		</div>
        	</div>        	
	    </div>  -->   	
	    </div>

	    <div class="modal fade" id="pilkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-300px">
        	<div class="modal-dialog">
        		<div class="modal-content" style="width:900px">
        		<form class="form-horizontal" method="POST">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Kamar</h3>
        			</div>	
        			<div class="modal-body">
							<div class="form-group">
								<label class="col-md-3">Pilih Kamar</label>
								<div class="col-md-5">
									<select class="form-control" id="kmrPilih" name="kmrPilih">
										<option value="" selected>Pilih</option>
									</select>
								</div>
							</div>
							<!-- <div class="form-group">
								<label class="col-md-3">Pilih Kelas</label>
								<div class="col-md-5">
									<select class="form-control" id="klsPilih" name="klsPilih" disabled>
										<option value="" selected>Pilih</option>
										<option value="KELAS VIP" selected>Kelas VIP</option>
										<option value="KELAS UTAMA" selected>Kelas Utama</option>
										<option value="KELAS I" selected>Kelas I</option>
										<option value="KELAS II" selected>Kelas II</option>
										<option value="KELAS III" selected>Kelas III</option>
									</select>
								</div>
							</div> -->
							<br>
							<br>
							<p style="text-align:center;font-size:20px;">PILIH BED</p>
							<hr class="garis">

						<div id="boxshit">

						<!-- 	<div class="cinemabox">
								<div class="kosong" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>
							<div class="cinemabox">
								<div class="kosong" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>	
							<div class="cinemabox">
								<div class="kosong" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>	
							<div class="cinemabox">
								<div class="kosong" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>	
							<div class="cinemabox">
								<div class="rusak" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>	
							<div class="cinemabox">
								<div class="kosong" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>
							<div class="cinemabox">
								<div class="kosong" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>	
							<div class="cinemabox">
								<div class="kosong" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>	
							<div class="cinemabox">
								<div class="rusak" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>	
							<div class="cinemabox">
								<div class="kosong" style="text-align:center">
									<label>NAMA BED</label>
								</div>
							</div>	 -->

						</div>
					</div>
					<div class="clear"></div>	        			
      				<div class="modal-footer">
 			       		<button type="reset" id="btlKamar" class="btn btn-danger" data-dismiss="modal">Batal</button>
 			       		<button type="submit" class="btn btn-success">Simpan</button>	
 			       	</div>
				</form>
        		</div>
        	</div>        	
	    </div>	

	    <div class="modal fade" id="daftarkanrujukan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Daftarkan Rawat Inap</h3>
        			</div>	
        			<div class="modal-body">

	        			<form class="form-horizontal" role="form" id="submit_rujukan">
	        				<div class="form-group">
								<label class="control-label col-md-3" >Tanggal Periksa </label>
								<div class="col-md-4" >
									<div class="input-icon">
										<i class="fa fa-calendar"></i>
										<input type="text" style="cursor:pointer;background-color:white" id="modalrujuk_tgl" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
									</div>
								</div>				
							</div>	
							
							<div class="form-group">
								<label class="control-label col-md-3">No. Rekam Medis</label>
								<div class="col-md-7">
									<input type="hidden" id="modalrujuk_visit">
									<input type="text" class="form-control" id="modalrujuk_rm" name="noRm" placeholder="No Rekam Medis" disabled>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Nama Pasien</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="modalrujuk_nama" name="nama" placeholder="Nama Pasien" disabled>
								</div>
							</div>
														
							<div class="form-group">
								<label class="control-label col-md-3">Cara Bayar</label>
								<div class="col-md-5">
									<select class="form-control select" name="carabayar" id="carabayarruj">
										<option value="" selected>--Pilih Cara Bayar--</option>
										<option value="Umum">Umum</option>
										<option value="BPJS" id="op-bpjs">BPJS</option>
										<option value="Jamkesmas" >Jamkesmas</option>
										<option value="Asuransi" id="op-asuransi">Asuransi</option>
										<option value="Kontrak" id="op-kontrak">Kontrak</option>
										<option value="Gratis" >Gratis</option>
										<option value="Lain-laun">Lain-lain</option>
									</select>												
								</div>
							</div>
							
							<div class="form-group" id="asuransiruj">
								<label class="control-label col-md-3">Nama Asuransi</label>
								<div class="col-md-7">
									<input type="text " class="form-control modalrujuk_nasur" name="namaAsuransi" placeholder="Nama Asuransi">
								</div>
							</div>
									
							<div class="form-group" id="kontrakruj">
								<label class="control-label col-md-3">Nama Perusahaan</label>
								<div class="col-md-7">
									<input type="text" class="form-control modalrujuk_nperus" name="namaPerusahaan" placeholder="Nama Perusahaan">
								</div>
							</div>

							<div class="form-group" id="kelasruj">
								<label class="control-label col-md-3">Kelas Pelayanan </label>
								<div class="col-md-5">
									<select class="form-control select" name="kelasBpjs" id="kelasBpjs">
										<option value="" selected>Pilih Kelas Pelayanan</option>
										<option value="III">III</option>
										<option value="II">II</option>
										<option value="I"  >I</option>
										<option value="Utama" >Utama</option>
										<option value="VIP">VIP</option>
									</select>												
								</div>
							</div>
							
							<div class="form-group" id="noasuransiruj">
								<label class="control-label col-md-3">Nomor Asuransi</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="modalrujuk_noasur" name="nomorAsuransi" placeholder="Nomor Asuransi">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-3">Cara Masuk</label>
								<div class="col-md-5">
									<select class="form-control select" name="caramasuk" id="modalrujuk_cara">
										<option value="" selected>--Pilih Cara Masuk--</option>
										<option value="Datang sendiri">Datang sendiri</option>
										<option value="Puskesmas"  >Puskesmas</option>
										<option value="Rujuk RS lain" >Rujuk RS lain</option>
										<option value="Instansi" >Instansi</option>
										<option value="Kasus Polisi" >Kasus Polisi</option>
										<option value="Rujukan Dokter" >Rujukan Dokter</option>
										<option value="Lain-laun">Lain-lain</option>
									</select>												
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-3">Detail Cara Masuk</label>
								<div class="col-md-7">
									<textarea class="form-control" name="detailMasuk" placeholder="Detail cara masuk .." id="modalrujuk_detail"></textarea> 
								</div>
							</div>
      				</div>
      				<br>
      				<div class="modal-footer">
 			       		<button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>	
 			       		<button type="submit" class="btn btn-success" >Simpan</button>
			      	</div>
			      	</form>	
        		</div>
        	</div>        	
	    </div>
    </div>

</div>
