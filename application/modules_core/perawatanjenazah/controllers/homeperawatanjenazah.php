<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php');

class Homeperawatanjenazah extends Operator_base {
	protected  $dept_id;

	function __construct(){
		parent:: __construct();
		$this->load->model('m_jenazah');
		$data['page_title'] = "Perawatan Jenazah";
		$this->dept_id = $this->m_jenazah->get_dept_id('PERAWATAN JENAZAH')['dept_id'];
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$this->session->set_userdata($data);

		// load template
		$data['content'] = 'home';
		$data['riwayat'] = $this->m_jenazah->get_all_riwayat();
		$data['javascript'] = 'j_jenazah';

		$this->load->view('base/operator/template', $data);
	}

	public function get_tindakan()
	{
		$result = $this->m_jenazah->get_tindakan();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_paramedis()
	{
		$result = $this->m_jenazah->get_paramedis();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_riwayat_detail($id)
	{
		$res = $this->m_jenazah->get_riwayat_detail($id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function get_tarif($tindakan_id, $kelas)
	{
		$kelasbaru = str_replace("%20", " ", $kelas);
		$res = $this->m_jenazah->get_tarif($tindakan_id, $kelasbaru);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function get_new_id()
	{
		//format: xx000yymmddzzz
		//xx: dept_id
		//zzz:autoincrement
		$id = $this->m_jenazah->get_dept_id('PERAWATAN JENAZAH');
		$awal = $id['dept_id'].'000'.date('y').date('m').date('d');
		$cariin = $this->m_jenazah->get_max_id($awal);
		if ($cariin['invoice']) {
			$id_invoice = $cariin['invoice'] + 1;
		}else{
			$id_invoice = $awal.'001';
		}
		header('Content-Type: application/json');
	 	echo json_encode($id_invoice);
	}

	public function insert_perawatan()
	{
		$data = $_POST['data'];
		$ins = array(
			'nama' => $_POST['nama'],
			'umur' => $_POST['umur'],
			'jenis_kelamin' => $_POST['jenis_kelamin'],
			'sebab_kematian' => $_POST['sebab_kematian'],
			'nama_pj' => $_POST['nama_pj'],
			'no_telp' => $_POST['no_telp'],
			'alamat' => $_POST['alamat'],
			'kelas_pelayanan' => $_POST['kelas_pelayanan'],
			'no_invoice' => $_POST['no_invoice'],
			'total' => $_POST['total']
		);

		$id = $this->m_jenazah->add_jenazah($ins);

		if ($id) {
			foreach ($data as $value) {
				$tgl = DateTime::createFromFormat('d/m/Y H:i:s', $value[2]);

				$insdetail = array(
					'perawatan_id' => $id,
					'detail_tindakan_id' => $value[20],
					'waktu' => $tgl->format('Y-m-d H:i:s'),
					'paramedis_id' => $value[21],
					'paramedis_lain' => $value[6],
					'bakhp' => $value[22],
					'js' => $value[23],
					'jp' => $value[24],
					'on_faktur' => $value[25],
					'total' => $value[26]
				);
				$this->m_jenazah->add_jenazah_detail($insdetail);
				$result = array(
					'message' => 'Data berhasil disimpan',
					'error' => 'n'
				);
			}
		}else{
			$result = array(
				'message' => 'Gagal menyimpan data',
				'error' => 'y'
			 );
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

}
