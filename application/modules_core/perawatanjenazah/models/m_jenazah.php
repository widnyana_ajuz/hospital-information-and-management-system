<?php
  class M_jenazah extends CI_Model
  {
    function __construct(){}

    public function get_dept_id($nama)
    {
      $sql = "SELECT dept_id from master_dept WHERE nama_dept LIKE '$nama'";
      $res = $this->db->query($sql);
      if ($res){
        return $res->row_array();
      }
      else {
        return false;
      }
    }

    public function get_all_riwayat()
    {
      $sql = "SELECT * FROM jenazah_perawatan";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_riwayat_detail($id)
    {
      $sql = "SELECT jpd.id AS id, jpd.detail_tindakan_id AS detail_tindakan_id, mtd.tindakan_id AS tindakan_id, nama_tindakan, waktu, nama_petugas AS paramedis, IFNULL(paramedis_lain, '-') AS paramedis_lain, jpd.bakhp+jpd.js+jpd.jp AS tarif, jpd.on_faktur, jpd.total
              FROM jenazah_perawatan jp LEFT JOIN jenazah_perawatan_detail jpd on jpd.perawatan_id=jp.id
              LEFT JOIN master_tindakan_detail mtd ON jpd.detail_tindakan_id=mtd.detail_id
              LEFT JOIN master_tindakan mt ON mtd.tindakan_id=mt.tindakan_id
              LEFT JOIN petugas ON jpd.paramedis_id=petugas.petugas_id
              WHERE jpd.perawatan_id=$id";

      $res = $this->db->query($sql);
      if ($res) {
       return $res->result_array();
      }else {
       return false;
      }
    }

    public function get_tindakan()
    {
      $sql = "SELECT * FROM master_tindakan WHERE status='ACTIVE'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_paramedis()
    {
      $sql = "SELECT petugas_id, nama_petugas, jenis
              FROM petugas p, master_jabatan mj
              WHERE p.jabatan_id = mj.jabatan_id AND p.status='aktif' AND mj.jenis='medis'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->result_array();
      }else {
        return false;
      }
    }

    public function get_tarif($tindakan_id, $kelas)
    {
      $sql = "SELECT mtd.detail_id AS tarif_id, bakhp, js, jp
              FROM master_tindakan mt LEFT JOIN master_tindakan_detail mtd ON mtd.tindakan_id=mt.tindakan_id
              WHERE mtd.tindakan_id=$tindakan_id AND mtd.kelas='$kelas'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

    public function get_max_id($cari)
    {
      $sql = "SELECT MAX(no_invoice) AS invoice FROM jenazah_perawatan WHERE no_invoice LIKE '$cari%'";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

    public function add_jenazah($data)
    {
      $result =  $this->db->insert('jenazah_perawatan', $data);
			if ($result) {
				return $this->db->insert_id();
			}else{
				return false;
			}
    }

    public function add_jenazah_detail($data)
    {
      $result =  $this->db->insert('jenazah_perawatan_detail', $data);
      if ($result) {
        return $this->db->insert_id();
      }else{
        return false;
      }
    }

  }
?>
