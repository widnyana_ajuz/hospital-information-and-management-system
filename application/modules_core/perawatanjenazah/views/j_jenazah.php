<script type="text/javascript">
  $(document).ready(function(){
    //autonumeric
    jQuery(function($) {
      $('#add_onfaktur').autoNumeric('init',{
        aSep: '.',
        aDec: ',',
        mDec: '0'
      });
    });

    //data buat autocomplete tindakan
    $('#add_tindakan').focus(function(){
      var kelas = $('#add_kelas').val(); //buat cari tarif berdasarkan kelas
      var $input = $('#add_tindakan');

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>perawatanjenazah/homeperawatanjenazah/get_tindakan",
        success: function(data){
          var autodata = [];
          var iddata = [];
          for (var i = 0; i < data.length; i++) {
            autodata.push(data[i]['nama_tindakan']);
            iddata.push(data[i]['tindakan_id']);
          }
          $input.typeahead({
            autoSelect: true,
            source: autodata
          });
          $input.change(function(){
            var current = $input.typeahead("getActive");
            var index = autodata.indexOf(current);

            $('#add_id_tindakan').val(iddata[index]);
            $.ajax({
              type: "POST",
              url: "<?php echo base_url()?>perawatanjenazah/homeperawatanjenazah/get_tarif/" + iddata[index] + "/" + kelas,
              success: function(data){
                var total = Number(data['bakhp']) + Number(data['js']) + Number(data['jp']);
                var onf = $('#add_onfaktur').autoNumeric('get');
                if (onf == '') {
                  onf='0';
                }
                $('#add_tarif').val(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                $('#add_jumlah').val((total + Number(onf)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                $('#add_id_tarif').val(data['tarif_id']);
                $('#add_bakhp').val(data['bakhp']);
                $('#add_js').val(data['js']);
                $('#add_jp').val(data['jp']);
                $('#add_view_tindakan').val($('#add_tindakan').val());
              },
              error: function(data){
                console.log(data);
              }
            })

            if (current) {
                // Some item from your model is active!
                if (current.name == $input.val()) {
                    // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
                } else {
                    // This means it is only a partial match, you can either add a new item
                    // or take the active if you don't want new items
                }
            } else {
                // Nothing is active so it is a new value (or maybe empty value)
            }
          });
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //data buat autocomplete paramedis
    $('#add_paramedis').focus(function(){
      var $input = $('#add_paramedis');

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>perawatanjenazah/homeperawatanjenazah/get_paramedis",
        success: function(data){
          var autodata = [];
          var iddata = [];
          for (var i = 0; i < data.length; i++) {
            autodata.push(data[i]['nama_petugas']);
            iddata.push(data[i]['petugas_id']);
          }
          $input.typeahead({
            autoSelect: true,
            source: autodata
          });

          $input.change(function(){
            var current = $input.typeahead("getActive");
            var index = autodata.indexOf(current);

            $('#add_id_paramedis').val(iddata[index]);

            if (current) {
                // Some item from your model is active!
                if (current.name == $input.val()) {
                    // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
                } else {
                    // This means it is only a partial match, you can either add a new item
                    // or take the active if you don't want new items
                }
            } else {
                // Nothing is active so it is a new value (or maybe empty value)
            }
          });
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //detail riwayat perawatan Jenazah
    $('#tbodyriwayat').on('click', 'tr td a#viewdetail', function(e){
      e.preventDefault();
      var id = $(this).closest('tr').find('td #val_id').val();

      $('#pop_invoice').val($(this).closest('tr').find('td').eq(1).text());
      $('#pop_nama').val($(this).closest('tr').find('td').eq(2).text());
      $('#pop_umur').val($(this).closest('tr').find('td').eq(3).text());
      $('#pop_jenkel').val($(this).closest('tr').find('td').eq(4).text());
      $('#pop_sebab').val($(this).closest('tr').find('td #val_sebab').val());
      $('#pop_pj').val($(this).closest('tr').find('td').eq(5).text());
      $('#pop_telp').val($(this).closest('tr').find('td').eq(6).text());
      $('#pop_alamat').val($(this).closest('tr').find('td #val_alamat').val());
      $('#pop_kelas').val($(this).closest('tr').find('td').eq(7).text());
      $('#pop_total').html($(this).closest('tr').find('td').eq(8).text().toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>perawatanjenazah/homeperawatanjenazah/get_riwayat_detail/" + id,
        success: function(data){
          console.log('data: ' + data);
          if (data.length > 0) {
            $('#tbodydetail').empty();
            for (var i = 0; i < data.length; i++) {
              $('#tbodydetail').append(
                '<tr>' +
                  '<td>'+(i+1)+'</td>' +
                  '<td>'+data[i]['nama_tindakan']+'</td>' +
                  '<td>'+data[i]['waktu']+'</td>' +
                  '<td>'+data[i]['paramedis']+'</td>' +
                  '<td>'+data[i]['paramedis_lain']+'</td>' +
                  '<td class="text-right">'+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>' +
                  '<td class="text-right">'+data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>' +
                  '<td class="text-right">'+data[i]['total'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>' +
                '</tr>'
              );
            }
          }
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //klik tambah tindakan
    $('.tmbhTndkan').on('click', function(e){
      e.preventDefault();

      if ($('#add_kelas').val() == '') {
        alert('Pilih kelas pelayanan terlebih dulu.');
        return false;
      }
    })

    //on fakturnya diganti
    $('#add_onfaktur').on('change', function(e){
      var tarif = $('#add_tarif').val();
      if (tarif == '') {
        tarif="0"
      }
      var total = Number($(this).autoNumeric('get')) + Number(tarif.toString().replace(/[^\d\,\-\ ]/g, ''));
      $('#add_jumlah').val(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
    })

    //masukkan tindakan ke tabel
    $('#submitTindakan').submit(function(e){
      e.preventDefault();
      var tindakan = $('#add_tindakan').val();
      var waktu = $('#add_waktu').val();
      var paramedis = $('#add_paramedis').val();
      var paramedis_lain = $('#add_paramedis_lain').val();
      var bakhp = $('#add_bakhp').val();
      var js = $('#add_js').val();
      var jp = $('#add_jp').val();
      var on_faktur = $('#add_onfaktur').autoNumeric('get');
      if (on_faktur == '') {
        on_faktur = 0;
      }
      var jumlah = $('#add_jumlah').val();
      var tindakan_id = $('#add_id_tarif').val();
      var paramedis_id = $('#add_id_paramedis').val();

      var tabel = $('#tableCare').DataTable();
      var act = '<center><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></center>'+
                '<input type="hidden" id="val_id_tindakan" value='+ tindakan_id +'>'+
                '<input type="hidden" id="val_id_paramedis" value='+ paramedis_id +'>'+
                '<input type="hidden" id="val_bakhp" value='+ bakhp.toString().replace(/[^\d\,\-\ ]/g, '') +'>'+
                '<input type="hidden" id="val_js" value='+ js.toString().replace(/[^\d\,\-\ ]/g, '') +'>'+
                '<input type="hidden" id="val_jp" value='+ jp.toString().replace(/[^\d\,\-\ ]/g, '') +'>'+
                '<input type="hidden" id="val_onfaktur" value='+ on_faktur.toString().replace(/[^\d\,\-\ ]/g, '') +'>'+
                '<input type="hidden" id="val_jumlah" value='+ jumlah.toString().replace(/[^\d\,\-\ ]/g, '') +'>';

      tabel.row.add([
        tindakan,
        waktu,
        paramedis,
        paramedis_lain,
        bakhp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
        js.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
        jp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
        on_faktur.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
        jumlah.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
        act
      ]).draw();
      hitung_total();

      $('#add_id_tindakan').val('');
      $('#add_tindakan').val('');
      $('#add_view_tindakan').val('');
      $('#add_waktu').val('<?php echo date("d/m/Y H:i:s")?>');
      $('#add_id_paramedis').val('');
      $('#add_paramedis').val('');
      $('#add_paramedis_lain').val('');
      $('#add_tarif').val('');
      $('#add_bakhp').val('');
      $('#add_js').val('');
      $('#add_jp').val('');
      $('#add_onfaktur').val('');
      $('#add_jumlah').val('');

      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>perawatanjenazah/homeperawatanjenazah/get_new_id",
        success: function(data){
        $('#add_invoice').val(data);
        },
        error: function(data){
          console.log(data);
        }
      })
    })

    //remove tindakan di tabel
    $('#tbodytindakan').on('click', 'tr td .removeRow', function(e){
      e.preventDefault();

      var tabel = $('#tableCare').DataTable();
      tabel.row($(this).closest('tr')).remove().draw();
      console.log(tabel.data().length);

      if (tabel.data().length == 0) {
        $('#total').text(0); //biar gak NaN
      }else {
        hitung_total();
      }
    })

    //simpan tindakan & detail2nya
    $('#frmTambah').submit(function(e){
      e.preventDefault();

      var x = confirm("Yakin akan menambahkan data? Data yang sudah ditambahkan tidak dapat dihapus.");
      if (x) {
        var item = {};
        item['nama'] = $('#add_nama').val();
        item['umur'] = $('#add_umur').val();
        item['jenis_kelamin'] = $('input[name="add_jenkel"]:checked').val();
        item['sebab_kematian'] = $('#add_sebab').val();
        item['nama_pj'] = $('#add_pj').val();
        item['no_telp'] = $('#add_telp').val();
        item['alamat'] = $('#add_alamat').val();
        item['kelas_pelayanan'] = $('#add_kelas').val();
        item['no_invoice'] = $('#add_invoice').val();
        item['total'] = $('#total').text().toString().replace(/[^\d\,\-\ ]/g, '');

        var data = [];
        $('#tbodytindakan').find('tr').each(function (rowIndex, r) {
            var cols = [];
            $(this).find('td').each(function (colIndex, c) {
              cols.push(c.textContent);
              cols.push(c.value);
            });
            $(this).find('td input').each(function (colIndex, c) {
  	          cols.push(c.value);
  	        });
            data.push(cols);
        });

        var tabel = $('#tableCare').DataTable();
        if (tabel.data().length == 0) { //kalau gak ada detail
          alert('Isi detail penerimaan!');
          return false;
        }

        console.log(data);
        item['data'] = data;

        $.ajax({
          type: "POST",
          data: item,
          url: "<?php echo base_url()?>perawatanjenazah/homeperawatanjenazah/insert_perawatan",
          success: function(data){
            console.log(data);
            reset_perawatan();
            myAlert('DATA BERHASIL DITAMBAH');
            window.location.replace("<?php echo base_url()?>perawatanjenazah/homeperawatanjenazah");
          },
          error: function(data){
            console.log(data);
          }
        })

      }else {
        return false;
      }
    })

    //reset
    $('#btnReset').on('click', function(e){
      reset_perawatan();
    })

  }) //end of document ready

  function hitung_total()
  {
    var data = [];
    var total = 0;

    $('#tbodytindakan').find('tr').each(function (rowIndex, r) {
        var cols = [];
        $(this).find('td').each(function (colIndex, c) {
            cols.push(c.textContent);
            cols.push(c.value);
        });
        data.push(cols);
    });

    console.log(data);
    for (var i = data.length - 1; i >= 0; i--) {
			total += Number(data[i][16].toString().replace(/[^\d\,\-\ ]/g, ''));
		};

    $('#total').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
  }

  function reset_perawatan()
  {
    $('#add_invoice').val('');
    $('#add_nama').val('');
    $('#add_umur').val('');
    $('#add_jenkel').val('LAKI-LAKI');
    $('#add_sebab').val('');
    $('#add_pj').val('');
    $('#add_telp').val('');
    $('#add_alamat').val('');
    $('#add_kelas').val('');
    $('#total').text('0');

    var table = $('#tableCare').DataTable();
    table.clear().draw();
  }
</script>
