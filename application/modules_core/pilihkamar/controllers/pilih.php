<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Pilih extends Operator_base {

	function __construct(){

		parent:: __construct();
		$this->load->model("m_pilihkamar");
		$data['page_title'] = "Pilih Kamar";
		$this->session->set_userdata($data);

	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();

		$data['content'] = 'pilihkamar';
		$data['datapasien'] = $this->m_pilihkamar->get_data_pasien();
		$data['departemen'] = $this->m_pilihkamar->get_dept();
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}

	public function get_order_pasien(){
		$result = $this->m_pilihkamar->get_data_pasien();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_pasien(){
		$keyword = $_POST['search'];
		$result = $this->m_pilihkamar->get_search_pasien($keyword);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_kelas_kamar($query){
		$result = $this->m_pilihkamar->get_kelas_kamar($query);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function submit_pilih_kamar(){
		//ambil data
		$year_now = date('y');
		$month_now = date('m');
		$date_now = date('d');
		$insert['inap_id']=$this->m_pilihkamar->create_inap_id($year_now,$month_now,$date_now);
		$insert['kamar_id']=$_POST['kamar_id'];
		$insert['bed_id'] = $_POST['bed_id'];
		$insert['waktu_masuk'] = $this->get_now();
		$insert['ri_id'] = $this->m_pilihkamar->create_visit_ri_id($_POST['dept_id'],$year_now,$month_now,$date_now);
		
		//masukan kedalam table visit_inap_kamar
		$in_inap = $this->m_pilihkamar->add_visit_inap_kamar($insert);

		//edit status_visit pada table visit
		$visit_id = $this->m_pilihkamar->get_visit_id($_POST['ri_id']);
		$status['status_visit'] = "REGISTRASI INAP";
		$status['tipe_kunjungan'] = "RAWAT INAP";

		$update_visit = $this->m_pilihkamar->update_visit($visit_id, $status);

		//update unit tujuan di visit_ri
		$update['unit_tujuan'] = $_POST['dept_id'];
		$riid = $_POST['ri_id'];
		$update['ri_id'] = $insert['ri_id'];
		//$update['ri_id'] = $_POST['dept_id'].''.substr($insert['ri_id'],2);
		$update = $this->m_pilihkamar->update_visit_ri($riid, $update);

		//update bad
		$bed['is_dipakai'] = 1;
		$update_bed = $this->m_pilihkamar->update_bed($insert['bed_id'], $bed);

		if($update && $update_visit)
			return true;
		return false;

		header('Content-Type: application/json');
		echo json_encode($insert);

	}

	public function get_now() {
	    $this->load->helper('date');
        $datestring = '%Y-%m-%d %H:%i:%s';
        $time = time();
        $now = mdate($datestring, $time);
        return $now;
	}


	public function check_bed($query){
		$result = $this->m_pilihkamar->check_bed($query);

		header('Content-Type: application/json');
		echo json_encode($result);		
	}

	public function get_kamar($query){
		$result = $this->m_pilihkamar->get_kamar($query);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_bed($query){
		$result = $this->m_pilihkamar->get_bed($query);

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

};