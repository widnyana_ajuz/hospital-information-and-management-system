<?php
class m_pilihkamar extends CI_Model{

	public function get_data_pasien(){
		$sql = "SELECT p.rm_id, p.nama, p.jenis_kelamin, p.tanggal_lahir, p.alamat_skr, p.jenis_id, v.visit_id, vr.ri_id, vr.cara_bayar FROM pasien p, visit v, visit_ri vr
		WHERE p.rm_id = v.rm_id AND v.visit_id = vr.visit_id AND v.status_visit = 'REGIS RUJUK INAP' ORDER BY vr.waktu_masuk ASC";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function get_search_pasien($keyword){
		$sql = "SELECT * FROM pasien p, visit v, visit_ri vr
		WHERE p.rm_id = v.rm_id AND v.visit_id = vr.visit_id AND v.status_visit = 'REGIS RUJUK INAP' AND (p.nama LIKE '%$keyword%' OR p.rm_id LIKE '%$keyword%' ) ORDER BY vr.waktu_masuk ASC";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function get_dept(){
		$sql = "SELECT * FROM master_dept WHERE jenis = 'RAWAT INAP'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	
	public function get_kamar($dept_id){
		$sql = "SELECT * FROM master_kamar mk, master_bed mb, (SELECT kamar_id ,count(kamar_id) as jumlah FROM master_bed GROUP BY kamar_id) v, (SELECT kamar_id ,sum(is_dipakai) as terpakai FROM master_bed GROUP BY kamar_id) x WHERE mk.kamar_id = v.kamar_id AND mk.kamar_id = x.kamar_id AND mb.kamar_id = mk.kamar_id AND mk.dept_id = $dept_id";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function get_kelas_kamar($insert){
		$sql = "SELECT * FROM master_kamar WHERE nama_kamar = '$insert'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function submit_pilih_kamar($item, $inap_id){
		$this->db->where('inap_id', $inap_id);
		$update = $this->db->update('visit_inap_kamar',$item);
		if($update){
			return true;
		}else{
			return false;
		}
	}

	public function get_bed($inap_id){
		$sql="SELECT bed_id FROM master_bed WHERE kamar_id = $inap_id ORDER BY nama_bed ASC LIMIT 1";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}

	public function check_bed($query){
		$sql="SELECT * FROM master_bed WHERE kamar_id = $query";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}

	public function create_inap_id($year,$month,$date) {
        $sql = "SELECT SUBSTR(inap_id, 9, 54)'inap_id' FROM visit_inap_kamar
        			WHERE SUBSTR(inap_id, 3, 2) = '$year' 
        			AND SUBSTR(inap_id, 5, 2) = '$month' 
                    AND SUBSTR(inap_id, 7, 2) = '$date'
        			ORDER BY inap_id 
        			DESC LIMIT 1";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $inap_id = $query->row_array();
            $inap_id = intval($inap_id['inap_id']) + 1;

            if (strlen($inap_id) == '1') {
                $inap_id = '000' . $inap_id;
            } elseif (strlen($inap_id) == '2') {
                $inap_id = '00' . $inap_id;
            } elseif (strlen($inap_id) == '3') {
                $inap_id = '0' . $inap_id;
            } else {
                $inap_id = strlen($inap_id);
            }	
            return "KM" . $year . $month . $date . $inap_id;
        } else {
            return "KM" . $year . $month . $date .'0001';
        }
	}

	public function add_visit_inap_kamar($input){
		$insert = $this->db->insert('visit_inap_kamar',$input);
		if ($insert) {
			return true;
		} else {
			return false;
		}
	}	

	public function get_visit_id($ri_id){
		$sql="SELECT visit_id FROM visit_ri WHERE ri_id = '$ri_id'";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result['visit_id'];
	}

	public function update_visit($id, $data){
        $this->db->where('visit_id', $id);
        $update = $this->db->update('visit', $data);

        if($update)
            return true;
        else
            return false;   
    }

    public function update_visit_ri($id, $data){
    	$this->db->where('ri_id', $id);
        $update = $this->db->update('visit_ri', $data);

        if($update)
            return true;
        else
            return false;   	
    }

    public function update_bed($id_bed, $data){
		$this->db->where('bed_id', $id_bed);
		$update = $this->db->update('master_bed', $data);

		if($update)
			return true;
		else
			return false;	
	}

	public function create_visit_ri_id($id, $year,$month,$date) {
        $sql = "SELECT SUBSTR(ri_id, 9, 54)'ri_id' FROM visit_ri
        			WHERE SUBSTR(ri_id, 1, 2) = '$id'  
        			AND SUBSTR(ri_id, 3, 2) = '$year' 
        			AND SUBSTR(ri_id, 5, 2) = '$month' 
                    AND SUBSTR(ri_id, 7, 2) = '$date'
        			ORDER BY ri_id 
        			DESC LIMIT 1";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $ri_id = $query->row_array();
            $ri_id = intval($ri_id['ri_id']) + 1;

            if (strlen($ri_id) == '1') {
                $ri_id = '000' . $ri_id;
            } elseif (strlen($ri_id) == '2') {
                $ri_id = '00' . $ri_id;
            } elseif (strlen($ri_id) == '3') {
                $ri_id = '0' . $ri_id;
            } else {
                $ri_id = strlen($ri_id);
            }
            return $id . $year . $month . $date . $ri_id;
        } else {
            return $id . $year . $month . $date .'0001';
        }
	}
}
?>