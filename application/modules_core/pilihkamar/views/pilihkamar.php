<br>
<div class="title">
	<li style="list-style: none">		
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>pilihkamar/pilih">PILIH KAMAR</a>

	</li>
</div>


<style type="text/css">
.cinemabox{
	
	color: white;
	cursor: pointer;
	width: 100px;
	height: 100px;
	float: left;
	margin-right: 15px;
	margin-bottom: 15px;
}	
.cinemabox label{
	margin-top: 50px;
	cursor: pointer;
}	
.cinemabox .rusak label{
	margin-top: 50px;
	cursor: default;
}	

.cinemabox .terpakai{
	background: #D9534F;
	width: 100px;
	height: 100px;
	cursor: default;
	
}

.cinemabox .rusak{
	background: #C0C0C0;
	width: 100px;
	height: 100px;
	cursor: default;
}

.cinemabox .kosong{
	background: #A7FFAE;
	width: 100px;
	height: 100px;
	cursor: pointer;
	
}
.cinemabox .kosong:hover{
	background: #F7FB86;
	width: 100px;
	height: 100px;
	cursor: pointer;
	
}

	
</style>


<div class="backregis"  style="margin-top:50px;">
	<div id="my-tab-content" class="tab-content">
		
		<form class="form-horizontal" method="POST" id="search_submit">
	   		<div class="search">
				<label class="control-label col-md-3">
					<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
				</label>
				<div class="col-md-4" style="margin-left:-400px">		
					<input type="text" id="searchPasien" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
				</div>
				<button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			</div>	
		</form>
		<br>
		<hr class="garis">

		<div id="titleInformasi" style="margin-bottom:-40px;">
		<p style="text-align:center;margin-top:-30px; margin-left: -50px;">PASIEN RAWAT JALAN</p></div>
		<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

			<div class="portlet box red">
				<div class="portlet-body" style="margin: -11px 0px -85px 0px">
				
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" style="table-layout:fixed">
						<thead>
							<tr class="info">
								<th width="3%">No.</th>
								<th width="10%">#Rekam Medis</th>
								<th width="15%">Nama Lengkap</th>
								<th width="10%">Jenis Kelamin</th>
								<th width="13%">Tanggal Lahir</th>
								<th>Alamat Tinggal</th>
								<th width="10%">Identitas</th>
								<th width="5%">Action</th>
							</tr>
						</thead>
						<tbody id="t_body_pilih">
							<?php
								if(isset($datapasien)){
									if(!empty($datapasien)){
									$i = 0;
									foreach($datapasien as $data){
										$tgl = strtotime($data['tanggal_lahir']);
										$hasil = date('d F Y', $tgl); 
										echo"
											<tr>
											<td align='center'>".++$i."</td>
											<td>".$data['rm_id']."</td>
											<td  style='word-wrap: break-word;white-space: pre-wrap; '>".$data['nama']."</td>
											<td>".$data['jenis_kelamin']."</td>
											<td align='center'>".$hasil."</td>
											<td  style='word-wrap: break-word;white-space: pre-wrap;' >".$data['alamat_skr']."</td>
											<td>".$data['jenis_id']."</td>
											<td style='text-align:center'>
												<a href='#pilihkamar' data-toggle='modal' data-original-title='Tambah Pemeriksaan' onclick='visit(".$data['rm_id'].",&quot;".$data['nama']."&quot;,&quot;".$data['ri_id']."&quot;,&quot;".$data['cara_bayar']."&quot;)'>
												<i class='fa fa-plus' data-toggle='tooltip' data-placement='top' title='Tambah Pemeriksaan'></i></a>
											</td>		
											</tr>
										";
									}
									}
								}
							?>
						</tbody>
					</table>
			</div>
			</div>
			</div>
			<br><br><br><br>
		</div>       
		<br>

	    <div class="modal fade" id="pilihkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        	<div class="modal-dialog">
	        	<form method="POST" id="submit_pilih_kamar">
	        		<div class="modal-content">
	        			<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Kamar Rawat Inap</h3>
	        			</div>	
	        			<div class="modal-body">
	        				
							<div class="form-group">
							<br><br>
								<label class="control-label col-md-3">No. Rekam Medis</label>
								<div class="col-md-7">
									<input type="hidden" id="m_visit_id" name="m_visit">
									<input type="text" class="form-control" id="modal_no_rm" name="noRm" placeholder="No Rekam Medis" readonly>
								</div>
							</div>

							<div class="form-group">
							<br><br>
								<label class="control-label col-md-3">Nama Pasien</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="modal_nama" name="nama" placeholder="Nama Pasien" readonly>
								</div>
							</div>
														
							<div class="form-group"><br><br>
								<label class="control-label col-md-3">Cara Bayar</label>
								<div class="col-md-5">
									<select class="form-control select" name="carabayar" id="carabayar" readonly>
										<option value="">--Pilih Cara Bayar--</option>
										<option value="Umum">Umum</option>
										<option value="BPJS" id="op-bpjs">BPJS</option>
										<option value="Jamkesmas" >Jamkesmas</option>
										<option value="Asuransi" id="op-asuransi">Asuransi</option>
										<option value="Kontrak" id="op-kontrak">Kontrak</option>
										<option value="Gratis" >Gratis</option>
										<option value="Lain-laun">Lain-lain</option>
									</select>												
								</div>
							</div>
							
							<div class="form-group" id="asuransi"><br><br>
								<label class="control-label col-md-3">Nama Asuransi</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="namaAsuransi" name="namaAsuransi" placeholder="Nama Asuransi" readonly>
								</div>
							</div>
									
							<div class="form-group" id="kontrak"><br><br>
								<label class="control-label col-md-3">Nama Perusahaan</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="namaPerusahaan" name="namaPerusahaan" placeholder="Nama Perusahaan" readonly>
								</div>
							</div>

							<div class="form-group" id="kelas"><br><br>
								<label class="control-label col-md-3">Kelas Pelayanan </label>
								<div class="col-md-5">
									<select class="form-control select" name="kelasBpjs" id="kelasBpjs" readonly>
										<option value="">--Pilih Kelas--</option>
										<option value="III">III</option>
										<option value="II">II</option>
										<option value="I"  >I</option>
										<option value="Utama" >Utama</option>
										<option value="VIP">VIP</option>
									</select>												
								</div>
							</div>
							
							<div class="form-group" id="noAsuransi"><br><br>
								<label class="control-label col-md-3">Nomor Asuransi</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="noAsuran" name="nomorAsuransi" placeholder="Nomor Asuransi" readonly>
								</div>
							</div>

							<div class="form-group"><br><br>
								<label class="control-label col-md-3">Departemen Tujuan</label>
								<div class="col-md-6">
									<select class="form-control select" id="deptTujuan">
										<option value="">--Pilih Departemen--</option>
										<?php
											foreach ($departemen as $dept) {
												echo "<option value='".$dept['dept_id']."'>";
												echo $dept['nama_dept'];
												echo "</option>";
											}
										?>
									</select>												
								</div>
							</div>
							
							<div class="form-group"><br><br>
								<label class="control-label col-md-3">Pilih Kamar & Kelas Kamar</label>
								<div class="col-md-4">
									<input type="hidden" id="kamar_id" name="kamar_id">
									<input type="hidden" id="bed_id" name="bed_id">
									<input type="text" class="form-control" id="kamar" placeholder="Search Kamar" data-toggle="modal" data-target="#pilkamar">
								</div>
							</div>	

	      				</div>
	      				<br><br>
	      				<div class="modal-footer">
	 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
	 			       		<button type="submit" class="btn btn-success">Simpan</button>
				      	</div>

	        		</div>
	        	</form>
	        	</div>        	
	    </div>

	    <div class="modal fade" id="pilkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-300px">
        	<div class="modal-dialog">
        		<div class="modal-content" style="width:900px">
        		<form class="form-horizontal" method="POST">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Kamar</h3>
        			</div>	
        			<div class="modal-body">
							<div class="form-group">
								<label class="col-md-3">Pilih Kamar</label>
								<div class="col-md-5">
									<select class="form-control" id="kmrPilih" name="kmrPilih">
										<option value="" selected>Pilih</option>
									</select>
								</div>
							</div>
							<br>
							<br>
							<p style="text-align:center;font-size:20px;">PILIH BED</p>
							<hr class="garis">

						<div id="boxshit">
						</div>
					</div>
					<div class="clear"></div>	        			
      				<div class="modal-footer">
 			       		<button type="reset" id="btlKamar" class="btn btn-danger" data-dismiss="modal">Batal</button>
 			       		<button type="submit" class="btn btn-success">Simpan</button>	
 			       	</div>
				</form>
        		</div>
        	</div>        	
	    </div>	
	    </div>
	</div>
</div>

<script type="text/javascript">
	$(window).ready(function(){
		$('#modal-kamar').click(function(){
			$('#pilkamar').modal('hide');
		});

		$('#close-kamar').click(function(){
			$('#pilkamar').modal('hide');
		});

		$('#search_submit').submit(function(e){
			var query = $('#searchPasien').val();
			var item = {};
			item['search'] = query;
			e.preventDefault();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url()?>pilihkamar/pilih/search_pasien',
				success:function(data){
					console.log(query);
					console.log(data);
					$('#t_body_pilih').empty();
					if(data.length>0){
						for(var i = 0; i<data.length;i++){
							$('#t_body_pilih').append(
								'<tr>'+
								'<td align="center">'+(i+1)+'</td>'+
								'<td>'+data[i]['rm_id']+'</td>'+
								'<td style="word-wrap: break-word;white-space: pre-wrap; ">'+data[i]['nama']+'</td>'+
								'<td>'+data[i]['jenis_kelamin']+'</td>'+
								'<td align="center">'+changeDate(data[i]['tanggal_lahir'])+'</td>'+
								'<td  style="word-wrap: break-word;white-space: pre-wrap; ">'+data[i]['alamat_skr']+'</td>'+
								'<td>'+data[i]['jenis_id']+'</td>'+
								'<td style="text-align:center">'+
									'<a href="#pilihkamar" data-toggle="modal" data-original-title="Tambah Pemeriksaan" onclick="visit('+data[i]['rm_id']+',&quot;'+data[i]['nama']+'&quot;,&quot;'+data[i]['ri_id']+'&quot;,&quot;'+data[i]['cara_bayar']+'&quot;)">'+
									'<i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Pemeriksaan"></i></a>'+
								'</td>'+		
								'</tr>'
							);
						}
					}else{
						$('#t_body_pilih').empty();
						$('#t_body_pilih').append(
							'<tr>'+
							'<td colspan="8" style="text-align:center;">Data tidak ditemukan</td>'+
							'</tr>'
						);
					}
				},
				error:function(data){
					alert('no');
					console.log(data);
				}
			})
		});

		$('#kelas_k').change(function(e){
			e.preventDefault();

			var query = $(this).val();
			$.ajax({
				type:'POST',
				url:'<?php echo base_url()?>pilihkamar/pilih/check_bed/'+query,
				success:function(data){
					if(data.length>0){
						$('#bed_tersedia').show();
					}else{
						$('#bed_nonsedia').show();
					}
				}
			});
		});

		var item = {};
		$("#submit_pilih_kamar").submit(function(e){
			item['dept_id'] = $('#deptTujuan').val();
			item['ri_id'] = $('#m_visit_id').val();
			item['kamar_id'] = $('#kamar_id').val();
			item['bed_id'] = $('#bed_id').val();
			
			e.preventDefault();
			// console.log(item);return false;
			
			$.ajax({
				type:"POST",
				data:item,
				url:"<?php echo base_url()?>pilihkamar/pilih/submit_pilih_kamar",
				success:function(data){
					console.log(data);
					alert('Data berhasilkan ditambahkan');
					$('#pilihkamar').modal('hide');
					window.location.href="<?php echo base_url()?>pilihkamar/pilih";
				},error:function(data){
					console.log(data);
				}
			});
		});

		$('#kamar').click(function(){
			var dept = $('#deptTujuan').val();
			var dataKamar = '';

			$('#tbody_kamar').empty();
			$.ajax({
				type:"POST",
				url:"<?php echo base_url() ?>pasien/rawatinap/get_nama_kamar/"+dept,
				success:function(data){
					var kamarSkr = '',
						kamarBaru = '';				
					console.log(data);

					$('#kmrPilih').empty();

					$('#kmrPilih').append('<option value="" selected>Pilih</option>');

					for(var i=0; i<data.length; i++){
						$('#kmrPilih').append(
							'<option value="'+data[i]['kamar_id']+'">'+data[i]['nama_kamar']+'</option>'
						);
					}
				}
			});
		});

		$('#kmrPilih').change(function(){
			var item = {};
			item['nama_kamar'] = $(this).val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?=base_url()?>pasien/rawatinap/get_databed',
				success:function(data){
					console.log(data);
					$('#boxshit').empty();

					for(var i = 0; i<data.length; i++){
						var status = '';
						if(data[i]['is_dipakai']=='0')
							status = 'kosong';
						else
							status = 'rusak';

						$('#boxshit').append(
							'<div class="cinemabox">'+
								'<div class="'+status+'" style="text-align:center">'+
									'<input type="hidden" class="bed_id" value="'+data[i]['bed_id']+'">'+
									'<input type="hidden" class="bed_name" value="'+data[i]['nama_bed']+'">'+
									'<label>'+data[i]['nama_bed']+'</label>'+
								'</div>'+
							'</div>'
						);
					}
				},error:function(data){
					console.log(data);
				}
			});
		})

		$('#boxshit').on('click','.kosong', function(){
			var bed_id = $(this).find('.bed_id').val();
			var kamar_id = $('#kmrPilih').val();
			var bed_name = $(this).find('.bed_name').val();

			$('#kamar_id').val(kamar_id);
			$('#bed_id').val(bed_id);
			$('#kamar').val(bed_name);

			$('#pilkamar').modal('hide');
		})
	});

	function visit(rm_id,nama,visit_id,carabayar){
		$('#modal_no_rm').val(rm_id);
		$('#m_visit_id').val(visit_id);
		$('#modal_nama').val(nama);
		$('#carabayar').val(carabayar);
		// $('modal_dept_first').val(dept);
		// $('#deptuju').val(dept);

		$('#bed_tersedia').hide();
		$('#bed_nonsedia').hide();

		var item = {};
		item['search'] = rm_id;

		if(carabayar=="BPJS"){
			$("#asuransi").hide();
			$("#kontrak").hide();
			$("#kelas").show();
			$("#noAsuransi").show();
			$('#namaPerusahaan').hide();
			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url()?>pilihkamar/pilih/search_pasien',
				success:function(data){
					console.log(data);
					$('#kelasBpjs').val(data[0]['kelas_pelayanan']);
					$('#noAsuran').val(data[0]['no_asuransi']);
				}
			});
		}else if(carabayar=="Jamkesmas"){
			$("#asuransi").hide();
			$("#kontrak").hide();
			$("#kelas").hide();
			$("#noAsuransi").show();
			$('#namaPerusahaan').hide();
			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url()?>pilihkamar/pilih/search_pasien',
				success:function(data){
					console.log(data);
					$('#noAsuran').val(data[0]['no_asuransi']);
				}
			});
		}else if(carabayar=="Asuransi"){
			$("#asuransi").show();
			$("#kontrak").hide();
			$("#kelas").hide();
			$("#noAsuransi").show();
			$('#namaPerusahaan').hide();
			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url()?>pilihkamar/pilih/search_pasien',
				success:function(data){
					console.log(data);
					$('#namaAsuransi').val(data[0]['nama_asuransi']);
					$('#noAsuran').val(data[0]['no_asuransi']);
				}
			});
		}else if(carabayar=="Kontrak"){
			$("#asuransi").hide();
			$("#kontrak").show();
			$("#kelas").hide();
			$("#noAsuransi").hide();
			$('#namaPerusahaan').show();
			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url()?>pilihkamar/pilih/search_pasien',
				success:function(data){
					console.log(data);
					$('#namaPerusahaan').val(data[0]['nama_perusahaan']);
				}
			});
		}else{
			$("#asuransi").hide();
			$("#kontrak").hide();
			$("#kelas").hide();
			$("#noAsuransi").hide();
			$('#namaPerusahaan').hide();
		}

		// $.ajax({
		// 	type:'POST',
		// 	url:'<?php echo base_url()?>pilihkamar/pilih/get_kamar/'+dept,
		// 	success:function(data){
		// 		console.log(data);
		// 		$('#kamar').empty();
		// 		$('#kamar').append('<option value="" selected>--Pilih Kamar--</option>');
		// 		for(var i = 0; i<data.length; i++){
		// 			$('#kamar').append(
		// 				'<option value="'+data[i]['nama_kamar']+'">'+data[i]['nama_kamar']+'</option>'
		// 			);
		// 		}
		// 	}
		// });

		$('#kelas_k').empty();
		$('#kelas_k').append('<option value="" selected>--Pilih Kelas--</option>');
	}

	function pilih_bed(kamar_id, bed_id, nama_bed){
		$('#kamar_id').val(kamar_id);
		$('#bed_id').val(bed_id);
		$('#kamar').val(nama_bed);
		$('#pilkamar').modal('hide');
	}

	function changeDate(tgl_lahir){
		var remove = tgl_lahir.split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0];

		return tgl;
	}
</script>