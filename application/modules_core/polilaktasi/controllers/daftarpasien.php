<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );


class Daftarpasien extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_daftarpasien");
		$data['page_title'] = "Detail Poli Laktasi";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load template
		$data['content'] = 'daftarpasien/list';
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();

		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}	

	public function periksa($rj_id, $visit_id){
		$data['content'] = 'daftarpasien/list';
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$rm = $this->m_daftarpasien->get_rm_id($visit_id);
		$data['sub_visit'] = $rj_id;
		$rm_id = $rm['rm_id'];
		$dept = $this->m_daftarpasien->get_dept_id("POLI LAKTASI");
		$data['dept_id'] = $dept['dept_id'];
		$data['menu_view'] = $this->menu();
		$pasien = $this->m_daftarpasien->get_pasien($visit_id, $rj_id);
		$data['pasien'] = $pasien;
		$data['visit_id'] = $visit_id;
		$data['visit_rj'] = $this->m_daftarpasien->get_ri_id($visit_id);
		$history = $this->get_overview_history($visit_id,$rj_id);
		$data['overview_history'] = $history;
		$data['visit_resep'] = $this->m_daftarpasien->get_visit_resep($rj_id);
		$data['kategori'] = $this->m_daftarpasien->get_tindakan_kategori();
		$data['dokter'] = $this->m_daftarpasien->get_dokter();
		$data['order_operasi'] = $this->m_daftarpasien->get_order_kamar_operasi($rm_id);
		$data['visit_care'] = $this->m_daftarpasien->get_visit_care($visit_id);
		$data['gizi']= $this->m_daftarpasien->get_visit_gizi($visit_id);
		$data['poliklinik'] = $this->m_daftarpasien->get_dept_rj();
		$data['riwayat_klinik'] = $this->m_daftarpasien->get_riwayat_klinik($rm_id);
		$data['riwayat_igd'] = $this->m_daftarpasien->get_riwayat_igd($rm_id);

		$data['penunjang'] = $this->m_daftarpasien->get_unit_penunjang();
		$data['visit_penunjang'] = $this->m_daftarpasien->get_visit_penunjang($rm_id);
		$data['riwayat_perawatan'] = $this->m_daftarpasien->get_riwayat_perawatan($pasien['rm_id']);

		$this->load->view('base/operator/template', $data);	
	}

	public function get_master_tindakan(){
    	$result = $this->m_daftarpasien->get_master_tindakan();

    	header('Content-Type: application/json');
    	echo json_encode($result);	
    }

    public function get_kelas_tindakan(){
    	$search = $_POST['tindakan'];

    	$result = $this->m_daftarpasien->get_kelas_tindakan($search);

    	header('Content-Type: application/json');
    	echo json_encode($result);	
    }

	public function get_overview_history($v_id, $rj_id)
	{
		$result = $this->m_daftarpasien->get_overview_history($v_id, $rj_id);
		return $result;
	}

	public function get_tindakan($id)
	{
		$result = $this->m_daftarpasien->get_tindakan($id);

		header('Content-Type:application/json');
		echo(json_encode($result));
	}

	public function save_overview()
	{
		
		foreach ($_POST as $value) 
		{
			$insert = $value;
		}

		$insert['id'] = $this->m_daftarpasien->get_overview_id($insert['visit_id']);

		$tgl = $this->sdate_db($insert['waktu']);
		$insert['waktu'] = $tgl;

		$hasil = $this->m_daftarpasien->insert_overview($insert);

		header('Content-Type:application/json');
		echo(json_encode($insert));
	}

	public function save_tindakan()
	{
		foreach ($_POST as $value) 
		{
			$insert = $value;
		}

		$visit_id = $insert['visit_id'];
		
		$id = $this->m_daftarpasien->get_last_visit_care($visit_id);
		if($id){
			$vid = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vid) == "1") {
				$vid = '000'. $vid;
			}else if(strlen($vid) == "2"){
				$vid = '00' . $vid;
			}else if (strlen($vid) == "3") {
				$vid = '0' . $vid;
			}
			$insert['care_id'] = "CA".$visit_id."".($vid);
		}else{
			$insert['care_id'] = "CA".$visit_id."0001";
		}
		
		$tgl = $this->sdate_db($insert['waktu_tindakan']);
		$insert['waktu_tindakan'] = $tgl;
		$hasil = $this->m_daftarpasien->save_tindakan($insert);
		
		$ins = $this->m_daftarpasien->get_inserted_visit_care($insert['care_id']);
		header('Content-Type:application/json');
		echo(json_encode($ins));
	}


	public function save_visit_resep()
	{
		foreach ($_POST as $value) 
		{
			$insert = $value;
		}

		$visit_id = $insert['visit_id'];
		$id = $this->m_daftarpasien->get_last_visit_resep();
		if($id){
			$vir = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vir) == "1") {
				$vir = '000'. $vir;
			}else if(strlen($vir) == "2"){
				$vir = '00' . $vir;
			}else if (strlen($vir) == "3") {
				$vir = '0' . $vir;
			}
			$insert['resep_id'] = "RE".$visit_id."".($vir);
		}else{
			$insert['resep_id'] = "RE".$visit_id."0001";
		}

		$tgl = $this->fdate_db($insert['tanggal']);
		$insert['tanggal'] = $tgl;
		$hasil = $this->m_daftarpasien->save_visit_resep($insert);
		
		$ins = $this->m_daftarpasien->get_inserted_visit_resep($insert['resep_id']);
		header('Content-Type:application/json');
		echo(json_encode($ins));

	}

	public function hapus_tindakan($id, $v_id){
		$input = $this->m_daftarpasien->hapus_tindakan($id);

		$result = $this->m_daftarpasien->get_visit_care($v_id);
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}


	//-------- pemberian resep ---------------//

	public function hapus_resep($id, $v_id){
		$input = $this->m_daftarpasien->hapus_resep($id);

		$result = $this->m_daftarpasien->get_visit_resep($v_id);
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_dokter(){
		$query = $_POST['dokter'];
		$result = $this->m_daftarpasien->search_dokter($query);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_dokter(){
		$result = $this->m_daftarpasien->get_dokter();

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	//-------- Order Operasi ---------------//
	public function save_order_operasi(){
		foreach ($_POST as $value) {
			$insert = $value;
		}

		// $visit_id = $insert['visit_id'];
		// $id = $this->m_daftarpasien->get_last_order_operasi($visit_id);

		// if($id){
		// 	$vir = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
		// 	if (strlen($vir) == "1") {
		// 		$vir = '0'. $vir;
		// 	}
		// 	$insert['order_operasi_id'] = "OR".$visit_id."".($vir);
		// }else{
		// 	$insert['order_operasi_id'] = "OR".$visit_id."01";
		// }

		$d_id = $this->m_daftarpasien->get_dept_id($insert['dept_id']);
		$insert['dept_id'] = $d_id['dept_id'];

		$tujuan = $this->m_daftarpasien->get_dept_id($insert['dept_tujuan']);
		$insert['dept_tujuan'] = $tujuan['dept_id'];

		$tgl = $this->sdate_db($insert['waktu_mulai']);
		$insert['waktu_mulai'] = $tgl;
		$hasil = $this->m_daftarpasien->save_order_operasi($insert);

		$ins = $this->m_daftarpasien->get_inserted_order_operasi();
		header('Content-Type:application/json');
		echo(json_encode($ins));
	}

	public function hapus_order($id, $sub_visit){
		$input = $this->m_daftarpasien->hapus_order_operasi($id);

		$result = $this->m_daftarpasien->get_order_kamar_operasi($sub_visit);
		header('Content-type: application/json');
		echo json_encode($result);
	}

    public function search_diagnosa($value){
    	$result = $this->m_daftarpasien->search_diagnosa($value);

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function get_detail_over($value){
    	$result = $this->m_daftarpasien->get_detail_over($value);

		header('Content-Type: application/json');
		echo json_encode($result);
    }

     public function get_diag_name($value){
    	$result = $this->m_daftarpasien->get_diag_name($value);

    	header('Content-Type: application/json');
    	echo json_encode($result);
    }

    public function save_gizi(){
    	foreach ($_POST as $value) 
		{
			$insert = $value;
		}

		$visit_id = $insert['visit_id'];

		$id = $this->m_daftarpasien->get_last_visit_gizi($visit_id);
		if($id){
			$vid = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if(strlen($vid) == "1"){
				$vid = '00' . $vid;
			}else if (strlen($vid) == "2") {
				$vid = '0' . $vid;
			}
			$insert['gizi_id'] = "GI".$visit_id."".($vid);
		}else{
			$insert['gizi_id'] = "GI".$visit_id."001";
		}

		$tgl= $this->fdate_db($insert['tanggal']);
		$insert['tanggal'] = $tgl;

		$hasil = $this->m_daftarpasien->save_gizi($insert);

		header('Content-Type:application/json');
		echo(json_encode($insert));
    }

    public function hapus_gizi($id, $v_id){
    	$input = $this->m_daftarpasien->hapus_gizi($id);

		$result = $this->m_daftarpasien->get_visit_gizi($v_id);
		
		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function save_resume(){
    	//get data
		foreach ($_POST as $value) {
			$insert =  $value;
		}
		$tgl = $insert['waktu_keluar'];
		$insert['waktu_keluar'] = $this->sdate_db($tgl);
		$id = $insert['rj_id'];
    	//update table_rj
		$update = $this->m_daftarpasien->save_resume($id, $insert);

		//update table visit
		if($insert['alasan_keluar'] == "Pasien Dipindahkan"){
			$data['status_visit'] = "RUJUK RJ";
		}
		else if($insert['alasan_keluar'] == "Rujuk IGD"){
			$year_now = date('y');
			$month_now = date('m');
			$date_now = date('d');
			$inidata = $this->m_daftarpasien->get_data_rj($insert['rj_id']);

			$insert_igd['visit_id'] = $insert['visit_id'];
			$insert_igd['unit_asal'] = $inidata['unit_tujuan'];
			$insert_igd['unit_tujuan'] = '9';
			$insert_igd['cara_bayar'] = $inidata['cara_bayar'];
			$insert_igd['nama_asuransi'] = $inidata['nama_asuransi'];
			$insert_igd['nama_perusahaan'] = $inidata['nama_perusahaan'];
			$insert_igd['kelas_pelayanan'] = $inidata['kelas_pelayanan'];
			$insert_igd['no_asuransi'] = $inidata['no_asuransi'];
			$insert_igd['unit_tujuan'] = '9';
			$insert_igd['waktu_masuk'] = $this->get_now();
			$data['status_visit'] = "REGISTRASI";

			$insert_igd['igd_id'] = $this->m_daftarpasien->create_igd_id($insert_igd['unit_tujuan'],$year_now,$month_now,$date_now);
			$input = $this->m_daftarpasien->add_visit_igd($insert_igd);
		}
		else if($insert['alasan_keluar'] == "Rujuk Rawat Inap"){
			$in['visit_id'] = $insert['visit_id'];
			$in['dept_id'] = substr($insert['rj_id'], 0, 2);
			$tanggal = $this->m_daftarpasien->get_date_in($insert['rj_id']);
			$in['waktu'] = $tanggal['waktu_masuk'];
			$in['is_checkout'] = "1";
			$go = $this->m_daftarpasien->save_visit_dept($in);

			$data['status_visit'] = "RUJUK INAP";
		}else{
			$data['status_visit'] = "CHECKOUT";
		}
		$data['visit_id'] = $insert['visit_id'];
		$update_v = $this->m_daftarpasien->update_visit($insert['visit_id'], $data);

		if($update)
			return true;
		return false;
    }

    public function get_now() {
	    $this->load->helper('date');
        $datestring = '%Y-%m-%d %H:%m:%s';
        $time = time();
        $now = mdate($datestring, $time);
        return $now;
	}

    public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i:s',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}

	public function sdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function save_penunjang(){
		foreach ($_POST as $value) {
			$insert = $value;
		}

		$tgl = $this->fdate_db($insert['waktu']);
		$insert['waktu'] = $tgl;

		$year_now = date('y');
		$month_now = date('m');
		$date_now = date('d');

		$p_id = $insert['dept_tujuan'].$year_now.$month_now.$date_now;

		$id = $this->m_daftarpasien->get_last_visit_penunjang($p_id);
		if($id){
			$vid = intval(substr($id['value'], strlen($p_id))) + 1;
			if (strlen($vid) == "1") {
				$vid = '000'. $vid;
			}else if(strlen($vid) == "2"){
				$vid = '00' . $vid;
			}else if (strlen($vid) == "3") {
				$vid = '0' . $vid;
			}
			$insert['penunjang_id'] = $p_id."".($vid);
		}else{
			$insert['penunjang_id'] = $p_id."0001";
		}

		$dept_id = $this->m_daftarpasien->get_dept_id("POLI LAKTASI");
		$insert['dept_asal'] = $dept_id['dept_id'];

		$save = $this->m_daftarpasien->save_penunjang($insert);

		header("Content-Type: application/json");
		echo json_encode($insert);
	}

	public function get_detailpenunjang($penunjang){
		$result = $this->m_daftarpasien->get_detailpenunjang($penunjang);

		header("Content-Type: application/json");
		echo json_encode($result);	
	}
}
