<script type="text/javascript">
	$(document).ready(function () {
		var controler_link = 'psikologi/homepsikologi';
		$('#sebabkematian').attr('disabled', true);
		$('#statuslahir').on('change', function (e) {
			var a = $(this).val();
			if (a == 'HIDUP') {$('#srtLahir').text('No Surat Kelahiran');$('#surat').attr('placeholder','surat kelahiran');$('#sebabkematian').attr('disabled', true);return false;};
			$('#srtLahir').text('No Surat Kematian');$('#surat').attr('placeholder','surat kematian');$('#sebabkematian').attr('disabled', false);return false;

		})

		$('#namaibu').focus(function(){
			var $input = $('#namaibu');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>psikologi/daftarkelahiran/get_patient_on_bed',
				success:function(data){
					//console.log(data);
					var nama = [];var kabupaten = [];
					var visit = [];var kelurahan = [];
					var kecamatan = [];var telpon = [];
					var idkab = [];var alamat = [];
					var idkec = [];var idkel = [];
					var alamat = [];

					for(var i = 0; i<data.length; i++){
						nama.push(data[i]['nama']);
						visit.push(data[i]['visit_id']);
						idkab.push(data[i]['kab_id_skr']);
						idkec.push(data[i]['kec_id_skr']);
						idkel.push(data[i]['kel_id_skr']);
						kabupaten.push(data[i]['nama_kab']);
						kecamatan.push(data[i]['nama_kec']);
						kelurahan.push(data[i]['nama_kel']);
						telpon.push(data[i]['no_telp']);
						alamat.push(data[i]['alamat_skr']);
					}
					console.log(nama);

					$input.typeahead({source:nama, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = nama.indexOf(current);

					    $('#visit_id_ibu').val(visit[index]);
					    $('#telp').val(telpon[index]);
					    $('#kelurahan').val(kelurahan[index]);
					    $('#kelurahan_id').val(idkel[index]);
					    $('#kabupaten').val(kabupaten[index]);
					    $('#kabupaten_id').val(idkab[index]);
					    $('#kecamatan').val(kecamatan[index]);
					    $('#kecamatan_id').val(idkec[index]);
					    $('#alamat_ibu').val(alamat[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				},
				error: function (data) {
					console.log(data);
				}
			});
		});

		$('#textkamarnicu').click(function(){
			var dept = $('#deptTujuan').val();
			var dataKamar = '';

			$('#tbody_kamar_nicu').empty();
			$.ajax({
				type:"POST",
				url:"<?php echo base_url() ?>pasien/rawatinap/get_kamar/"+dept, //ambil dari admisi
				success:function(data){
					var kamarSkr = '',
						kamarBaru = '';				
					console.log(data);

					for(var i = 0; i<data.length;i++){
						var nama_kamar = data[i]['nama_kamar'];
						var kamar_id = data[i]['kamar_id'];
						var kelas_kamar = data[i]['kelas_kamar'];
						var jumlah = data[i]['jumlah'];
						var terpakai = data[i]['terpakai'];
						var nama_bed = data[i]['nama_bed'];
						var bed_id = data[i]['bed_id'];
						var is_dipakai = data[i]['is_dipakai'];

						kamarSkr = kamar_id;

						if(kamarSkr!=kamarBaru){
							if(i!=0){
								dataKamar+='<tr><td colspan="5"></td></tr>';
							}

							dataKamar+='<tr>'+
									'<td>'+nama_kamar+'</td>'+
									'<td>'+kelas_kamar+'</td>'+
									'<td>'+jumlah+'</td>'+
									'<td>'+terpakai+'</td>'+
									'<td></td>'+
								'</tr>';

							dataKamar+='<tr>'+
									'<td><input type="hidden" value="'+bed_id+'"></td>'+
									'<td></td>'+
									'<td></td>'+
									'<td>'+nama_bed+'</td>';
							if(is_dipakai==0){
								dataKamar+='<td style="text-align:center;"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"  style="cursor:pointer;" onClick="pilih_bed_nicu('+kamar_id+','+bed_id+',&quot;'+nama_bed+'&quot;)"></i></td>'+
								'</tr>';
							}else{
								dataKamar+='<td></td></tr>';
							}
						}else{
							dataKamar+='<tr>'+
								'<td><input type="hidden" value="'+bed_id+'"></td>'+
								'<td></td>'+
								'<td></td>'+
								'<td>'+nama_bed+'</td>';
							
							if(is_dipakai==0){
								dataKamar+='<td style="text-align:center;"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"  style="cursor:pointer;" onClick="pilih_bed_nicu('+kamar_id+','+bed_id+',&quot;'+nama_bed+'&quot;)"></i></td>'+
								'</tr>';
							}else{
								dataKamar+='<td></td></tr>';
							}
						}
						kamarBaru = kamarSkr;
					}
					$('#tbody_kamar_nicu').append(dataKamar);

				}
			});
		});

		$('#search_submit').submit(function(e){
			var data = {};
			data['search'] = $('#search_value').val();
			var admisi = 'Admisi';

			e.preventDefault();
			$.ajax({
				type:'POST',
				data:data,
				url:'<?php echo base_url(); ?>psikologi/homepsikologi/search_pasien',
				success:function(data){
					console.log(data);
					var t = $('#tabelutamapasienunit').DataTable();

					t.clear().draw();

					for(var i=0; i<data.length; i++){
						var rm = data[i]['rm_id'],
							nama = data[i]['nama'],
							jk = data[i]['jenis_kelamin'],
							tgl = data[i]['tanggal_lahir'],
							alamat = data[i]['alamat_skr'];
							asal = data[i]['dept_asal'];
						var last = '<center><a href="#" class="pindahpasien" data-toggle="modal" data-target="#pindahkan"><i class="fa fa-external-link" data-toggle="tooltip" data-placement="top" title="Pindah"></i></a>'+
									'<a href="<?php echo base_url() ?>psikologi/psikologidetail/daftar/'+data[i]['ri_id']+'/'+data[i]['visit_id']+'" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a></center>'+
									'<input type="hidden" class="ri_id" value="'+data[i]['ri_id']+'"><input type="hidden" class="v_id" value="'+data[i]['visit_id']+'">'+
									'<input type="hidden" class="b_id" value="'+data[i]['bed_id']+'">';
						t.row.add([
							i+1,
							rm,
							nama,
							jk,
							format_date(tgl),
							alamat,
							asal,
							last,
							i
						]).draw();
					}
					t.on( 'order.dt search.dt', function () {
				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
				    } ).draw();
					$('[data-toggle="tooltip"]').tooltip();
				},
				error:function (data) {
					console.log(data);
				}
			});
		});

		$('#submitpindahkan').submit(function (e) {
			e.preventDefault();
			pindah_pasien('psikologi/homepsikologi');
		})


		/*farmasi unit*/
		$('#submitfilterfarmasiunit').submit(function (e) {
			e.preventDefault();
			var filter = {};
			filter['filterby'] = $('#filterInv').find('option:selected').val();
			filter['valfilter'] = $('#filterby').val();
			submit_filter(filter);
		})

		$('#expired').on('click', function (e) {
			e.preventDefault();
			var filter = {};
			filter['expired'] = '0';
			submit_filter(filter)
		})

		$('#expired3').on('click', function (e) {
			e.preventDefault();
			var filter = {};
			filter['expired'] = '3';
			submit_filter(filter)
		})

		$('#expired6').on('click', function (e) {
			e.preventDefault();
			var filter = {};
			filter['expired'] = '6';
			submit_filter(filter);
		})

		$('#formobatfarmasibersalin').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['katakunci'] = $('#katakuncifarmasibersalin').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>bersalin/homebersalin/get_obat_gudang',
				success: function (data) {
					console.log(data);
					$('#tbodyobatpermintaanfarmasi').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodyobatpermintaanfarmasi').append(
								'<tr>'+
									'<td style="display:none">'+data[i]['obat_detail_id']+'</td>'+
									'<td style="display:none">'+data[i]['tgl_kadaluarsa']+'</td>'+
									'<td style="display:none">'+data[i]['obat_id']+'</td>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['total_stok']+'</td>'+
									'<td>'+format_date(data[i]['tgl_kadaluarsa'])+'</td>'+
									'<td style="text-align:center"><a href="#" class="addNewMintaFar"><i class="glyphicon glyphicon-check"></i></a></td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodyobatpermintaanfarmasi').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tbodyobatpermintaanfarmasi').on('click','tr td a.addNewMintaFar', function (e) {
			e.preventDefault();

			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputMintaFar').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputMintaFar').append(
				'<tr><td style="display:none">'+cols[0]+'</td>'+
				'<td style="display:none">'+cols[2]+'</td>'+
				'<td>'+cols[3]+'</td>'+
				'<td>'+format_date(cols[1])+'</td>'+
				'<td>'+cols[4]+'</td>'+
				'<td>'+cols[5]+'</td>'+
				'<td>'+cols[6]+'</td>'+
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>'
			)
		})

		$('#permintaanfarmasibersalin').submit(function (e) {
			e.preventDefault();
			$('#addinputMintaFar').find('tr td.dataKosong').closest('tr').remove();
			var item = {};
			item['no_permintaan'] = $('#noPermFarmBers').val();
			item['tanggal_request'] = $('#tglpermintaanfarmasi').val();
			item['keterangan_request'] = $('#ketObatFarBers').val();

			//jlh = 9, obat_id = 1, obat_detail_id = 0
			var data = [];
		    $('#addinputMintaFar').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				$('#addinputMintaFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;
		    var a = confirm('yakin disimpan ?');
		    if (a == true) {
			    $.ajax({
					type: "POST",
					data: item,
					url: '<?php echo base_url()?>psikologi/homepsikologi/submit_permintaan_bersalin',
					success: function (data) {
						//console.log(data);
						if (data['error'] == 'n'){
							$('#addinputMintaFar').empty();
							$('#noPermFarmBers').val('');
							$('#ketObatFarBers').val('');
							$('#addinputMintaFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
						}
						myAlert(data['message']);
					},
					error: function (data) {
						console.log(data);
					}
				})
			};
		})

		$('#batalpermintaanfarmasi').on('click',function (e) {
			e.preventDefault();
			$('#addinputMintaFar').empty();
			$('#addinputMintaFar').append('<tr><td colspan="6" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
		})

		//retur obat (ya ke gudang lah)
		$('#formsearchobatretur').submit(function (e) {
			e.preventDefault();
			var item ={};
			item['katakunci'] = $('#katakuncireturbersalin').val();

			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>psikologi/homepsikologi/get_obat_retur',
				success: function (data) {
					//console.log(data);
					$('#tbodyreturbersalin').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodyreturbersalin').append(
								'<tr>'+
									'<td style="display:none">'+data[i]['obat_detail_id']+'</td>'+
									'<td style="display:none">'+data[i]['tgl_kadaluarsa']+'</td>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['total_stok']+'</td>'+
									'<td>'+format_date(data[i]['tgl_kadaluarsa'])+'</td>'+
									'<td style="text-align:center"><a href="#" class="addNewReturFar"><i class="glyphicon glyphicon-check"></i></a></td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodyreturbersalin').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		
		$('#tbodyreturbersalin').on('click', 'tr td a.addNewReturFar', function (e) {
			e.preventDefault();
			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputRetFar').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputRetFar').append(
				'<tr><td style="display:none">'+cols[0]+'</td>'+//obat detail id
				'<td>'+cols[2]+'</td>'+  //nama
				'<td>'+format_date(cols[1])+'</td>'+ //tanggal kadaluarsa
				'<td>'+cols[3]+'</td>'+ //satuan
				'<td>'+cols[4]+'</td>'+ //merk
				'<td>'+cols[5]+'</td>'+ //stok unit
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah retur
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>'
			)
		})

		$('#formsubmitreturbersalin').submit(function (e) {
			e.preventDefault();
			$('#addinputRetFar').find('tr td.dataKosong').closest('tr').remove();
			var item = {};
			item['no_returdept'] = $('#noRetFarBers').val();
			item['waktu'] = $('#waktureturbersalin').val();
			item['keterangan'] = $('#ketObatRetFarBers').val();

			//jlh = 8, obat_id = 1, obat_detail_id = 0
			var data = [];
		    $('#addinputRetFar').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				 $('#addinputRetFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;
		    var a = confirm('yakin diproses ?');
		    if (a == true) {
			    $.ajax({
					type: "POST",
					data: item,
					url: '<?php echo base_url()?>psikologi/homepsikologi/submit_retur_bersalin',
					success: function (data) {
						console.log(data);
						if (data['error'] == 'n'){
							$('#addinputRetFar').empty();
							$('#addinputRetFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
							$('#noRetFarBers').val('');
							$('#ketObatRetFarBers').val('');
						}
						myAlert(data['message']);
					},
					error: function (data) {
						console.log(data);
					}
				})
			};
		})

		$('#batalreturfarmasi').on('click',function (e) {
			e.preventDefault();
			$('#addinputRetFar').empty();
			$('#addinputRetFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
		})

		var this_io_obat;
		$('#tbodyinventoriunit').on('click','tr td a.inoutobat', function (e) {
			e.preventDefault();
			this_io_obat = $(this);
			var obat_dept_id = $(this).closest('tr').find('td .barangobat_dept_id').val();
			var jlh = $(this).closest('tr').find('td').eq(5).text();

			$('#inout_obat_dept_id').val(obat_dept_id);
			$('#sisaInOutBer').val(jlh);

			$('#jmlInOutBer').on('change', function (e) {
				e.preventDefault();

				var is_in = $('#iober').find('option:selected').val();
				var jmlInOut = $('#jmlInOutBer').val();
				var sisa = jlh;//$('#sisaInOut').val();
				var hasil ="";
				if (is_in == 'IN') {
					hasil = Number(jmlInOut) + Number(sisa);
				}else{			
					hasil = Number(sisa) - Number(jmlInOut);
				}

				if (jmlInOut == '') {
					hasil = Number(sisa);
				}
				$('#sisaInOutBer').val(hasil);			
			})

			$('#iober').on('change', function () {
				var jumlah = Number($('#jmlInOutBer').val());
				var sisa = Number(jlh);//Number($('#sisaInOut').val());

				var isout = $('#iober').find('option:selected').val();
				if (isout === 'IN') {
					$('#sisaInOutBer').val(jumlah + sisa);
				} else{
					$('#sisaInOutBer').val(sisa - jumlah);
				};
			})
		})

		$('#submitinoutunit').submit(function (e) {
			e.preventDefault();

			var item = {};
			item['obat_dept_id'] = $('#inout_obat_dept_id').val();
			item['jumlah'] = $('#jmlInOutBer').val();
			item['sisa'] = $('#sisaInOutBer').val();
			item['is_out'] = $('#iober').find('option:selected').val();
			item['tanggal'] = $('#tglInOut').val();
		    item['keterangan'] = $('#keteranganIO').val();

		    if (item['jumlah'] != "") {
			    $.ajax({
			    	type: "POST",
			    	data: item,
			    	url: "<?php echo base_url()?>bersalin/homebersalin/input_in_out",
			    	success: function (data) {

			    		if (data == "true") {
			    			myAlert('data berhasil disimpan');
			    			$('#keteranganIO').val('');
			    			$('#jmlInOutBer').val('');
			    			this_io_obat.closest('tr').find('td').eq(5).text(item['sisa']);
			    			$('#inout').modal('hide');	
			    		} else{
			    			myAlert('gagal, terdapat kesalahan');
			    		};
			    		
			    	},
			    	error: function (data) {
			    		myAlert('gagal');
			    	}
			    })
			} else{
				myAlert('isi data dengan benar');
				$('#jmlInOutBer').focus();
			};		
		})

		$("#tbodyinventoriunit").on('click', 'tr td a.printobat', function (e) {
			var obat_dept_id = $(this).closest('tr').find('td .barangobat_dept_id').val();

			 $.ajax({
		    	type: "POST",
		    	url: "<?php echo base_url()?>farmasi/homegudangobat/get_detail_obat_bydeptid/" + obat_dept_id, //benar
		    	success: function (data) {
		    		console.log(data);
		    		$('#tbodydetailobatinventori').empty();
		    		for(var i = 0; i < data.length ; i++){
		    			var a = "";
		    			var jlh = "";
		    			if(data[i]['masuk'] == 0) {a = "OUT"} else a = "IN";
		    			if(data[i]['masuk'] == 0)  {jlh = data[i]['keluar']} else jlh = data[i]['masuk'];
		    			$('#tbodydetailobatinventori').append(
							'<tr>'+
								'<td style="text-align:center">'+format_date(data[i]['tanggal'])+'</td>'+
								'<td>'+a+'</td>'+
								'<td>'+jlh+'</td>'+
								'<td>'+data[i]['total_stok']+'</td>'+
							'</tr>'
		    			)
		    		}
		    	},
		    	error: function (data) {
		    		myAlert('gagal');
		    	}
		    })
		})
		/*akhir farmasi unit*/

		/*logistik*/
		var this_io;
		$('#tbodyinventoribarang').on('click', 'tr td a.edBarang', function (e) {
			e.preventDefault();
			$('#id_barang_inoutprocess').val($(this).closest('tr').find('td .barang_detail_inout').val());
			var jlh = $(this).closest('tr').find('td').eq(4).text();
			this_io = $(this);
			$('#sisaInOut').val(jlh);

			$('#jmlInOut').on('change', function (e) {
				e.preventDefault();

				var is_in = $('#io').find('option:selected').val();
				var jmlInOut = $('#jmlInOut').val();
				var sisa = jlh;//$('#sisaInOut').val();
				var hasil ="";
				if (is_in == 'IN') {
					hasil = Number(jmlInOut) + Number(sisa);
				}else{			
					hasil = Number(sisa) - Number(jmlInOut);
				}

				if (jmlInOut == '') {
					hasil = Number(sisa);
				}
				$('#sisaInOut').val(hasil);			
			})

			$('#io').on('change', function () {
				var jumlah = Number($('#jmlInOut').val());
				var sisa = Number(jlh);//Number($('#sisaInOut').val());

				var isout = $('#io').find('option:selected').val();
				if (isout === 'IN') {
					$('#sisaInOut').val(jumlah + sisa);
				} else{
					$('#sisaInOut').val(sisa - jumlah);
				};
			})
		})
	
		$('#forminoutbarang').submit(function (e) {
			e.preventDefault();

			var item = {};
			item['barang_detail_id'] = $('#id_barang_inoutprocess').val();
			item['jumlah'] = $('#jmlInOut').val();
			item['sisa'] = $('#sisaInOut').val();
			item['is_out'] = $('#io').find('option:selected').val();
		    item['tanggal'] = $('#tanggalinout').val();
		    item['keterangan'] = $('#keteranganIObar').val();
		    //console.log(item);return false;
		    if (item['jumlah'] != "") {
			    $.ajax({
			    	type: "POST",
			    	data: item,
			    	url: "<?php echo base_url()?>psikologi/homepsikologi/input_in_outbarang",
			    	success: function (data) {
			    		if (data == "true") {
			    			myAlert('data berhasil disimpan');
			    			$('#keteranganIObar').val('');
			    			$('#jmlInOut').val('');
			    			this_io.closest('tr').find('td').eq(4).text(item['sisa']);
			    			$('#inoutbar').modal('hide');	
			    		} else{
			    			myAlert('gagal, terdapat kesalahan');
			    		};
			    	},
			    	error: function (data) {
			    		myAlert('gagal');
			    	}
			    })
			} else{
				myAlert('isi data dengan benar');
				$('#jmlInOut').focus();
			};			
		})

		$("#tbodyinventoribarang").on('click', 'tr td a.detailinvenbarang', function (e) {
			var id = $(this).closest('tr').find('td .barang_detail_inout').val();

			 $.ajax({
		    	type: "POST",
		    	url: "<?php echo base_url()?>psikologi/homepsikologi/get_detail_inventori/" + id,
		    	success: function (data) {
		    		console.log(data);
		    		$('#tbodydetailbrginventori').empty();
		    		for(var i = 0; i < data.length ; i++){
		    			$('#tbodydetailbrginventori').append(
							'<tr>'+
								'<td>'+format_date(data[i]['tanggal'])+'</td>'+
								'<td>'+data[i]['is_out']+'</td>'+
								'<td>'+data[i]['jumlah']+'</td>'+
								'<td>'+data[i]['keterangan']+'</td>'+
							'</tr>'
		    			)
		    		}
		    	},
		    	error: function (data) {
		    		myAlert('gagal');
		    	}
		    })
		})
		
		$('#formmintabarang').submit(function (e) {
			e.preventDefault();
			var item ={};
			item['katakunci'] = $('#katakuncimintabarang').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>psikologi/homepsikologi/get_barang_gudang',
				success: function (data) {
					console.log(data);//return false;
					$('#tbodybarangpermintaan').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodybarangpermintaan').append(
								'<tr>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['tahun_pengadaan']+'</td>'+
									'<td>'+data[i]['stok_gudang']+'</td>'+
									'<td style="text-align:center"><a href="#" class="addnewpermintaanbarang"><i class="glyphicon glyphicon-check"></i></a></td>'+
									'<td style="display:none">'+data[i]['barang_stok_id']+'</td>'+
									'<td style="display:none">'+data[i]['barang_id']+'</td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodybarangpermintaan').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tbodybarangpermintaan').on('click', 'tr td a.addnewpermintaanbarang',function (e) {
			e.preventDefault();
			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputmintabarang').append(
				'<tr><td>'+cols[0]+'</td>'+//nama
				'<td>'+cols[1]+'</td>'+  //satuan
				'<td>'+cols[2]+'</td>'+ //merk
				'<td>'+cols[3]+'</td>'+ //tahun pengadaan
				'<td>'+cols[4]+'</td>'+ //stok gudang
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah minta
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
				'<td style="display:none">'+cols[6]+'</td>'+ //barang_stok_id
				'<td style="display:none">'+cols[7]+'</td></tr>' //barang_id
			)
		})

		$('#permintaanbarangunit').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['no_permintaanbarang'] = $('#nomorpermintaanbarang').val();
			item['tanggal_request'] = $('#tglpermintaanbarang').val();
			item['keterangan_request'] = $('#keteranganpermintaanbarang').val();

			var data = [];
			$('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
		    $('#addinputmintabarang').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;

		    $.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>psikologi/homepsikologi/submit_permintaan_barangunit',
				success: function (data) {
					console.log(data);
					if (data['error'] == 'n'){
						$('#addinputmintabarang').empty();
						$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
						$('#nomorpermintaanbarang').val('');
						$('#keteranganpermintaanbarang').val('');
					}
					myAlert(data['message']);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*logistik unit*/

		/*data kamar*/
		$('#tabelkamarunit tbody').on('click', 'tr td a.viewdetailkamar',function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .kamar_id_detail').val();
			var nama = $(this).closest('tr').find('td').eq(1).text();
			$('.titlekamar').text(nama);
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>bersalin/homebersalin/get_detail_kamar/" +id,
				success: function (data) {
					console.log(data);
					if (data.length > 0) {
						$('#tabeldetailkamar tbody').empty();
						for (var i = 0; i < data.length; i++) {
							$('#tabeldetailkamar tbody').append(
								'<tr>'+
									'<td>'+(Number(i+1))+'</td>'+
									'<td>'+data[i]['nama_bed']+'</td>'+
									'<td>'+data[i]['status']+'</td>'+
									'<td>'+data[i]['nama_pasien']+'</td>'+
								'</tr>'
							);
						};
					};
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*akhir data kamar*/

		/*master*/
		$('#nama_petugas_jaspel').focus(function(){
			var $input = $('#nama_petugas_jaspel');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>bersalin/homebersalin/get_all_dokter',//sama di igd :D
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(data);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#id_petugas_jaspel').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		$('#btn_filter_jaspel').on('click',function (e) {
			e.preventDefault();
			var item ={};
			item['mulai'] = format_date3($('#mulai_jaspel').val());
			item['akhir'] = format_date3($('#akhir_jaspel').val());
			item['cara_bayar'] = $('#carabayar_jaspel').val();
			item['petugas_id'] = $('#id_petugas_jaspel').val();
			console.log(item);
			$.ajax({
				type: "POST",
				data: item,
				url: "<?php echo base_url()?>psikologi/homepsikologi/filter_jaspel",
				success: function  (data) {
					console.log(data);
					var t = $('#tabelJpPoliinap').DataTable();
					t.clear().draw();
					for (var i = 0; i < data.length; i++) {
						t.row.add([
							Number(i+1),
							format_date(data[i]['tanggal']),
							data[i]['nama_tindakan'],
							data[i]['cara_bayar'],
							data[i]['nama_petugas'],
							data[i]['paramedis_lain'],
							data[i]['jp'],
						]).draw();
					};
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*master akhir*/
		
		/*belum pulang*/
		var this_pulang;
		$('#listygblmpulang tbody').on('click', 'tr td a.pulangkan_pasien', function (e) {
			e.preventDefault();
			this_pulang = $(this);
			var item = {};
			item['visit_id'] = $(this).closest('tr').find('td .v_id_pulang').val();
			item['bed_id'] = $(this).closest('tr').find('td .bed_id_pulang').val();
			item['ri_id'] = $(this).closest('tr').find('td .ri_id_pulang').val();
			var nama = $(this).closest('tr').find('td').eq(5).text();
			var a = confirm('pasien dengan nama ' + nama + ' akan dipulangkan ?');
			if (a == false) {return false;};
			$.ajax({
				type: "POST",
				data: item,
				url: "<?php echo base_url()?>bersalin/homebersalin/pulangkan_belum_pulang",
				success: function (data) {
					console.log(data);
					var table = $('#listygblmpulang').DataTable();
					table.row(this_pulang.parents('tr') ).remove().draw();
					table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			            cell.innerHTML = i+1;
			        } );
				},
				error:function (data) {
					console.log(data);
				}
			})
		})

		/*pindah pasien*/
		$('#submitpindahkan').submit(function (e) {
			e.preventDefault();
			pindah_pasien('psikologi/homepsikologi');
		})
		/*akhir pindah pasien*/

		$('#submitTagihanSearch').submit(function(event){
			event.preventDefault();

			var item = {};
			item['search'] = $('#search_tagihan').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo  base_url(); ?>psikologi/homepsikologi/search_tagihan',
				success:function(data){
					console.log(data);
					var t = $('#table_tagihan').DataTable();
					var no = 0;
					var action;
					t.clear().draw();

					for(var i=0; i<data.length; i++){
						no++;
						if(data[i]['carapembayaran'] == "BPJS"){
							action = '<a href="<?php echo base_url() ?>psikologi/invoicebpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}else{
							action = '<a href="<?php echo base_url() ?>psikologi/invoicenonbpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}

						t.row.add([
							no,
							data[i]['nama_dept'],
							data[i]['no_invoice'],
							data[i]['visit_id'],
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['alamat_skr'],
							data[i]['carapembayaran'],
							action,
							i
						]).draw();
					}

				}
			});
		});
	})

	function pilih_bed_nicu(kamar_id, bed_id, nama_bed){
		$('#kamar_id_nicu').val(kamar_id);
		$('#bed_id_nicu').val(bed_id);
		$('#textkamarnicu').val(nama_bed);
		$('#pilkamar').modal('hide');
	}

	function submit_filter (filter) {
		$.ajax({
			type: "POST",
			data: filter,
			url: "<?php echo base_url()?>psikologi/homepsikologi/submit_filter_farmasi",
			success:function (data) {
				console.log(data);
				$('#tbodyinventoriunit').empty();
				var t = $('#tabelinventoriunit').DataTable();

				t.clear().draw();
				for (var i =  0; i < data.length; i++) {
					var last = '<a href="#" class="inoutobat" data-toggle="modal" data-target="#inout"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
								'<a href="#edInvenBer" data-toggle="modal" class="printobat"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>'+
								'<input type="hidden" class="barangmerk_id" value="'+data[i]['merk_id']+'">'+
								'<input type="hidden" class="barangjenis_obat_id" value="'+data[i]['jenis_obat_id']+'">'+
								'<input type="hidden" class="barangsatuan_id" value="'+data[i]['satuan_id']+'">'+
								'<input type="hidden" class="barangobat_dept_id" value="'+data[i]['obat_dept_id']+'">';
					var tgl_kadaluarsa = format_date(data[i]['tgl_kadaluarsa']);
					t.row.add([
						(Number(i+1)),
						data[i]['nama'],
						data[i]['no_batch'],
						data[i]['harga_jual'],
						data[i]['nama_merk'],
						data[i]['total_stok'],
						data[i]['satuan'],								
						tgl_kadaluarsa,
						last
					]).draw();
				}

				t.on( 'order.dt search.dt', function () {
			        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			            cell.innerHTML = i+1;
			        } );
			    } ).draw();

				$('[data-toggle="tooltip"]').tooltip();
					
			},
			error:function (data) {
				console.log(data);
			}
		})
	}

	function setStatus(departmen){
		var u = "<?php echo base_url() ?>psikologi/";
		localStorage.setItem('department', departmen);
		localStorage.setItem('url', u);
		window.location.href="<?php echo base_url() ?>invoice/tambahinvoice";
	}
</script>