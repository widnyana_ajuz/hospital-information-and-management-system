<br>
<div class="title" id="rowfix" style="position:fixed; z-index:10; width:98.5%">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>rawatmata/homerawatmata">PASIEN MATA</a>
		<i class="fa fa-angle-right"></i>
		<a href="#"><?php echo $pasien['alias']; ?>. <?php echo $pasien['nama'] ?></a>
		<span class="nama">
			( <?php echo $pasien['nama_kamar'] ?> / <?php echo $pasien['nama_bed'] ?>
			&nbsp;-&nbsp;<?php echo $pasien['kelas_kamar'] ?>&nbsp;-&nbsp;<?php echo $pasien['cara_bayar'] ." ". $pasien['kelas_pelayanan'] ?> )
		</span>
		<i class="fa fa-angle-right"></i>
		<a href="#"><label class="control-label" id="dasbod" style="background:transparent;border: 0px;">Identitas Pasien</label></a>
	</li>
</div>

<input type="hidden" class="visit_id" value="<?php echo($pasien['visit_id']) ?>">
<input type="hidden" class="ri_id" value="<?php echo($pasien['ri_id']) ?>">
<input type="hidden" class="bed_id" value="<?php echo($pasien['bed_id']) ?>">
<input type="hidden" class="kamar_id" value="<?php echo($pasien['kamar_id']) ?>">
<input type="hidden" id="dept_id" value="<?php echo($dept_id) ?>">

<div class="navigation" style="margin-left: 10px; margin-top:100px;" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	    <li class="active"><a href="#identitas" data-toggle="tab">Identitas Pasien</a></li>
		<li><a href="#rmklinik" data-toggle="tab">Overview Klinik</a></li>
		<li><a href="#rmigd" data-toggle="tab">Overview IGD</a></li>
	    <li><a href="#rm" data-toggle="tab">Overview Perawatan</a></li>
		<li><a href="#resep" data-toggle="tab">Pemberian Resep</a></li>
	    <li><a href="#penunjang" data-toggle="tab">Pemeriksaan Penunjang</a></li>
	    <li><a href="#orderkamar" data-toggle="tab">Order Kamar Operasi</a></li>
	    <li><a href="#konsul" data-toggle="tab">Order Konsultasi Gizi</a></li>
	    <li><a href="#makan" data-toggle="tab">Daftar Permintaan Makan</a></li>
	    <!-- <li><a href="#pindah" data-toggle="tab">Pindah Pasien</a></li> -->
	    <!-- <li><a href="#catatan" data-toggle="tab">Catatan Persalinan</a></li> -->
	    <li><a href="#riwayat" data-toggle="tab">Riwayat Penyakit</a></li>
	    <li><a href="#resume" data-toggle="tab">Resume Pulang</a></li>
	    
	</ul>

	<div id="my-tab-content" class="tab-content">
    	
		<div class="modal fade" id="riwkklin" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="riwkondok">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Klinik</h3>
			   			</div>
						<div class="modal-body" style="padding-left:80px;">

			   				<div class="form-group">
								<label class="control-label col-md-4">Waktu Tindakan</label>
								<div class="col-md-5">	
									<input type="text" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
								</div>
		        			</div>	
		        			<div class="form-group">
								<label class="control-label col-md-4">Anamnesa</label>
								<div class="col-md-7">
									<textarea class="form-control" id="anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
								</div>
							</div>

							<fieldset class="fsStyle">
								<legend>
					                Tanda Vital
								</legend>
								<div class="form-group">
									<label class="control-label col-md-4" >Tekanan Darah</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="tekanandarah" name="takanandarah" placeholder="Tekanan Darah" readonly>
									</div>
									<label class="control-label col-md-2">mmHg</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Temperatur</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="temperatur" name="temperatur" placeholder="Temperatur" readonly>
									</div>
									<label class="control-label col-md-2">&deg;C</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Nadi</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="nadi" name="nadi" placeholder="Nadi" readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Pernapasan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Berat Badan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="berat" name="berat" placeholder="Berat Badan" readonly>
									</div>
									<label class="control-label col-md-2">Kg</label>
								</div>
					  		</fieldset>

					  		<fieldset class="fsStyle">
								<legend>
					                Diagnosa & Terapi
								</legend>
								<div class="form-group">
									<label class="control-label col-md-4" >Dokter Pemeriksa</label>
									<div class="col-md-7">
										<input type="text" style="background-color:white" class="form-control" id="dokter" placeholder="Search Dokter" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Utama</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_utama" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Sekunder</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek1" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" readonly>
									</div>
									<label class="control-label">1</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek2" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" readonly>
									</div>
									<label class="control-label">2</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek3" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" readonly>
									</div>
									<label  class="control-label">3</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek4" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" readonly>
									</div>
									<label class="control-label">4</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Detail Diagnosa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="detailDiagnosa" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Terapi</label>
									<div class="col-md-7">
										<textarea class="form-control" id="terapi" name="terapi" placeholder="Terapi" readonly></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Alergi</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="alergi" name="alergi" placeholder="Alergi" readonly>
									</div>
								</div>
					  		</fieldset>
			        	</div>
		        		
		        		<div class="modal-footer">
		        			
		 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

		<div class="modal fade" id="riwigd" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="riwkondok">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Penanganan IGD</h3>
			   			</div>
						<div class="modal-body" style="padding-left:80px;">

			   				<div class="form-group">
								<label class="control-label col-md-4">Waktu Tindakan</label>
								<div class="col-md-5">	
									<input type="text" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
								</div>
		        			</div>	
		        			<div class="form-group">
								<label class="control-label col-md-4">Anamnesa</label>
								<div class="col-md-7">
									<textarea class="form-control" id="anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
								</div>
							</div>

							<fieldset class="fsStyle">
								<legend>
					                Tanda Vital
								</legend>
								<div class="form-group">
									<label class="control-label col-md-4" >Tekanan Darah</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="tekanandarah" name="takanandarah" placeholder="Tekanan Darah" readonly>
									</div>
									<label class="control-label col-md-2">mmHg</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Temperatur</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="temperatur" name="temperatur" placeholder="Temperatur" readonly>
									</div>
									<label class="control-label col-md-2">&deg;C</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Nadi</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="nadi" name="nadi" placeholder="Nadi" readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Pernapasan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Berat Badan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="berat" name="berat" placeholder="Berat Badan" readonly>
									</div>
									<label class="control-label col-md-2">Kg</label>
								</div>
					  		</fieldset>

					  		<fieldset class="fsStyle">
								<legend>
					                Pemeriksaan Fisik
								</legend>
								<div class="form-group">
									<label class="control-label col-md-4" >Kepala & Leher</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="kepalaleher" name="kepalaleher" placeholder="Kepala & Leher" readonly>
									</div>
									
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Thorax & ABD</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="thorax" name="thorax" placeholder="Thorax & ABD" readonly>
									</div>
									
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Extremitas</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="Extremitas" name="Extremitas" placeholder="Extremitas" readonly>
									</div>
									
								</div>
								
					  		</fieldset>

					  		<fieldset class="fsStyle">
								<legend>
					                Diagnosa & Terapi
								</legend>
								<div class="form-group">
									<label class="control-label col-md-4" >Dokter Jaga</label>
									<div class="col-md-7">
										<input type="text" style="background-color:white" class="form-control" id="dokter" placeholder="Search Dokter" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Perawat Jaga</label>
									<div class="col-md-7">
										<input type="text" style="background-color:white" class="form-control" id="dokter" placeholder="Search Dokter" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Utama</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_utama" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Sekunder</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek1" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="control-label">1</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek2" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="control-label">2</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek3" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="control-label">3</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_sek4" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="control-label">4</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Detail Diagnosa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="detailDiagnosa" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Terapi</label>
									<div class="col-md-7">
										<textarea class="form-control" id="terapi" name="terapi" placeholder="Terapi" readonly></textarea>
									</div>
								</div>
								
					  		</fieldset>
			        	</div>
		        		
		        		<div class="modal-footer">
		        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
		 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

		<div class="modal fade" id="riwperawatan_" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="riwkondok">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Perawatan</h3>
			   			</div>
						<div class="modal-body" style="padding-left:80px;">

			   				<div class="form-group">
								<label class="control-label col-md-4">Waktu Tindakan</label>
								<div class="col-md-5">	
									<input type="text" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
								</div>
		        			</div>	

		        			<div class="form-group">
								<label class="control-label col-md-4">Dokter Visit</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="dokterv" name="dokterv" placeholder="Dokter" readonly></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">Petugas</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="petugas" name="petugas" placeholder="Petugas" readonly></textarea>
								</div>
							</div>

		        			<div class="form-group">
								<label class="control-label col-md-4">Anamnesa</label>
								<div class="col-md-7">
									<textarea class="form-control" id="anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" >Diagnosa Utama</label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_utama" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Keterangan" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" >Diagnosa Sekunder</label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_sek1" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Keterangan" readonly>
								</div>
								<label class="label-cntrol">1</label>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" ></label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_sek2" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Keterangan" readonly>
								</div>
								<label class="label-cntrol">2</label>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" ></label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_sek3" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Keterangan" readonly>
								</div>
								<label class="label-cntrol">3</label>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" ></label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_sek4" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Keterangan" readonly>
								</div>
								<label class="label-cntrol">4</label>
							</div>


							<div class="form-group">
								<label class="control-label col-md-4">Perbangan Penyakit</label>
								<div class="col-md-7">
									<textarea class="form-control" id="perkembangan" name="perkembangan" placeholder="Perkembangan Penyakit" readonly></textarea>
								</div>
							</div>

							<fieldset class="fsStyle">
								<legend>
					                Tanda Vital
								</legend>
								<div class="form-group">
									<label class="control-label col-md-4" >Tekanan Darah</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="tekanandarah" name="takanandarah" placeholder="Tekanan Darah" readonly>
									</div>
									<label class="control-label col-md-2">mmHg</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Temperatur</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="temperatur" name="temperatur" placeholder="Temperatur" readonly>
									</div>
									<label class="control-label col-md-2">&deg;C</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Nadi</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="nadi" name="nadi" placeholder="Nadi" readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Pernapasan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Berat Badan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="berat" name="berat" placeholder="Berat Badan" readonly>
									</div>
									<label class="control-label col-md-2">Kg</label>
								</div>
					  		</fieldset>

			        	</div>
		        		
		        		<div class="modal-footer">
		        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
		 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

		<div class="modal fade" id="tambahTindakan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tindakan</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" style="cursor:pointer;background-color:white" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy - hh:ii" placeholder="<?php echo date("d/m/Y - H:i");?>"/>
									</div>
			        			</div>							
			        			<div class="form-group">
									<label class="control-label col-md-4">Tindakan</label>
									<div class="col-md-5">
										<input type="text" class="typeahead form-control" autocomplete="off" spellcheck="false">												
									</div>
								</div>
			        			<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="tarif" name="tarif" placeholder="Tarif" readonly > 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="onfaktur" name="onfaktur" placeholder="On Faktur" >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Jumlah</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" readonly>
									</div>
			        			</div>

								<div class="form-group">
									<label class="control-label col-md-4">Paramedis</label>
									<div class="col-md-5">	
										<input type="text" class="typeahead form-control" autocomplete="off" spellcheck="false" id="paramedis" name="paramedis" placeholder="Paramedis">
										
									</div>
			        			</div>
			        			
		        			</div>
	       				</div>
		        		<br><br>
		        		<div class="modal-footer">
		        		 	<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>
	
    	<div class="tab-pane active" id="identitas">
    		<div class="dropdown">
        		<div id="titleInformasi">Identitas Pasien</div>
 			</div>
          	
            <div class="informasi" id="info1">
	            <form class="form-horizontal" role="form">
	            	
	           		<div class="form-group">
						<label class="control-label col-md-3" >Jenis Identitas Pasien</label>
						<div class="col-md-1">
							<input  id="jnsIdentitas" name="jenis_identitas" value="<?php echo $pasien['jenis_id']; ?>" style="border:0px;background-color:transparent;font-weight:bold" disabled />
						</div>					
					</div>	
					
					<div class="form-group">
						<label class="control-label col-md-3" >Nomor Identitas Pasien</label>
						<div class="col-md-3">
							<input  id="NomorID" name="nomor_identitas" value="<?php echo $pasien['no_id']; ?>" style="border:0px;background-color:transparent;font-weight:bold" disabled />
						</div>
					</div>	
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					<br>

					<div class="form-group">
						<label class="control-label col-md-3">No RM</label>
						<div class="col-md-4">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="rm_id" name="rm_id" value="<?php echo $pasien['rm_id']; ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					<br>

					<div class="form-group">
						<label class="control-label col-md-3">Nama Lengkap </label>
						<div class="col-md-4">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="NamaLengkap" name="NamaLengkap" value="<?php echo $pasien['nama']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Alias </label>
						<div class="col-md-1">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="alias" name="alias" value="<?php echo $pasien['alias'] ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Jenis Kelamin</label>
						<div class="col-md-1">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="jk" name="jk" value="<?php echo $pasien['jenis_kelamin'] ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Golongan Darah </label>
						<div class="col-md-1">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="goldarah" name="goldarah" value="<?php echo $pasien['gol_darah']; ?>" disabled />												
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Agama </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="agama" name="agama" value="<?php echo $pasien['agama']; ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					<br>

					<div class="form-group">
						<label class="control-label col-md-3">Tempat Lahir </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="newTempatLahir" name="tempat_lahir" value="<?php echo $pasien['tempat_lahir']; ?>" disabled/>
						</div>
																		
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Tanggal Lahir </label>
						<div class="col-md-2">
						<?php 
							$tgl = strtotime($pasien['tanggal_lahir']);
							$hasil = date('d F Y', $tgl); 
						?>
							<input style="border:0px;background-color:transparent;font-weight:bold" class="input-medium date-picker" maxlength="12" type="text" data-date-format="dd/mm/yyyy" id="TanggalLahir" value="<?php echo $hasil; ?>" disabled />
						</div>												
					</div>			
					
					<div class="form-group">
						<label class="control-label col-md-3">Umur</label>
						<div class="col-md-2">
						<?php  
							$datetime1 = new DateTime();
							$datetime2 = new DateTime($pasien['tanggal_lahir']);
							$interval = $datetime1->diff($datetime2);
							$umur = ''						;
							if($interval->y > 0)
								$umur .= $interval->y ." tahun ";
							if($interval->m > 0)
								$umur .= $interval->m." bulan ";
							if($interval->d > 0)
								$umur .= $interval->d ." hari";
						?>
							<input style="border:0px;background-color:transparent;font-weight:bold" id="umur" name="umur" 
							value="<?php echo($umur) ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					<br>

					<div class="form-group">
						<label class="control-label col-md-3">Status Kawin</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="status kawin" name="statuskawin" value="<?php echo $pasien['status_perkawinan']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Pendidikan Terakhir</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="pendidikan" name="pendidikan" value="<?php echo $pasien['pendidikan']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Pekerjaan </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="Pekerjaan" name="pekerjaan" value="<?php echo $pasien['pendidikan']; ?>" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Nomor Telepon</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="nomorPasien" name="nomor_pasien" value="<?php echo $pasien['no_telp']; ?>" disabled />
						</div>						
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					<br>

					<div class="form-group">
						<label class="control-label col-md-3">Alamat </label>
						<div class="col-md-5">
							<input style="border:0px;background-color:transparent;width:900px;font-weight:bold" id="Alamat" name="alamat" value="<?php echo $pasien['alamat_skr']; ?>" disabled />
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Alamat KTP</label>
						<div class="col-md-5">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="AlamatKTP" name="alamatKTP" value="<?php echo $pasien['alamat_ktp'] ?>" disabled />
						</div>						
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Wilayah </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="provinsi" name="provinsi" value="<?php echo $pasien['nama_prov']; ?>" disabled />
						</div>											
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kabupaten </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="kabupaten" name="kabupaten" value="<?php echo $pasien['nama_kab']; ?>" disabled />
						</div>												
						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kecamatan </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="kecamatan" name="kecamatan" value="<?php echo $pasien['nama_kec']; ?>" disabled />
						</div>
						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kelurahan </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="kelurahan" name="kelurahan" value="<?php echo $pasien['nama_kel']; ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					<br>
						
					<div class="form-group">
						<label class="control-label col-md-3">Cara Pembayaran</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="cara_bayar" name="cara_bayar" value="<?php echo $pasien['cara_bayar'] ?>" disabled />
						</div>						
					</div>
				</form>
			</div>
			<br><br>
		</div>

		<div class="tab-pane" id="rmklinik"> 
			<div class="dropdown"  id="btkonsudokter">
		        <div id="titleInformasi">Konsultasi Dokter </div>
		        <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i> </div>
		    </div>
	        <div class="tabelinformasi" id="ibtkonsudokter">				
				<div class="portlet-body" style="margin: 20px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabelhistoryoverklinis">
						<thead>
							<tr class="info" style="text_align: center;">
								<th width="20">No.</th>
								<th>Unit</th>
								<th>Anamnesa</th>
								<th>Dokter Pemeriksa</th>
								<th width="20">Action</th>
							</tr>
						</thead>
						<tbody id="tbody_overview">
							<?php
								if (isset($overview_history)) {
									$i = 0;
									if(!empty($overview_history)){
										foreach ($overview_history as $over) {
											echo'<tr>';
												echo'<td>'.(++$i).'</td>';
												echo'<td>'.$over['nama_dept'].'</td>';
												echo'<td>'.$over['anamnesa'].'</td>';
												echo'<td>'.$over['nama_petugas'].'</td>';
												echo'<td style="text-align:center;">
													<a href="#riwayatoverviewklinis" class="viewdetailoverviewklinis" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a>
													<input type="hidden" class="overviewid_detail" value="'.$over['id'].'">
													</td>';
											echo'</tr>';
										}
									}
								}
							?>
							
						</tbody>
					</table>												
				</div>
			</div>
			<br>
			<!-- modal -->
		 	<div class="modal fade" id="riwayatoverviewklinis" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="riwayatkonsultasidokter">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Overview Klinis</h3>
				   			</div>
							<div class="modal-body" style="padding-left:80px;">

				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" readonly id="waktutindakanklinis" />
									</div>
			        			</div>	
			        			<div class="form-group">
									<label class="control-label col-md-4">Anamnesa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="anamnesaklinis" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
									</div>
								</div>

								<fieldset class="fsStyle">
									<legend>
						                Tanda Vital
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Tekanan Darah</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="tekanandarahklinis" name="takanandarah" placeholder="Tekanan Darah" readonly>
										</div>
										<label class="control-label col-md-2">mmHg</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Temperatur</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="temperaturklinis" name="temperatur" placeholder="Temperatur" readonly>
										</div>
										<label class="control-label col-md-2">&deg;C</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Nadi</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="nadiklinis" name="nadi" placeholder="Nadi" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Pernapasan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="pernapasanklinis" name="pernapasan" placeholder="Pernapasan" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Berat Badan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="beratklinis" name="beratklinis" placeholder="Berat Badan" readonly>
										</div>
										<label class="control-label col-md-2">Kg</label>
									</div>
						  		</fieldset>

						  		<fieldset class="fsStyle">
									<legend>
						                Diagnosa & Terapi
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Dokter Pemeriksa</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="dokterklinis" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Diagnosa Utama</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_utamaklinis"  readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="diagutamaklinis" placeholder="Keterangan" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Diagnosa Sekunder</label>
										<div class="col-md-3">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d2" id="sekunderklinis1" placeholder="Kode"  readonly>
										</div>
										<div class="col-md-4">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d2" id="sekunderklinis1_1" placeholder="Keterangan"  readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" ></label>
										<div class="col-md-3">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d3" id="sekunderklinis2" placeholder="Kode"  readonly>
										</div>
										<div class="col-md-4">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d3" id="sekunderklinis2_2" placeholder="Keterangan" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" ></label>
										<div class="col-md-3">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d4" id="sekunderklinis3" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d4" id="sekunderklinis3_3" placeholder="Keterangan"  readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" ></label>
										<div class="col-md-3">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d5" id="sekunderklinis4" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d5" id="sekunderklinis4_4" placeholder="Keterangan" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Detail Diagnosa</label>
										<div class="col-md-7">
											<textarea class="form-control" id="detailDiagnosaklinis" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Terapi</label>
										<div class="col-md-7">
											<textarea class="form-control" id="terapiklinis" name="terapi" placeholder="Terapi" readonly></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Alergi</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="alergiklinis" name="alergi" placeholder="Alergi" readonly>
										</div>
									</div>
						  		</fieldset>
				        	</div>
			        		
			        		<div class="modal-footer">
			        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						    </div>
						</div>
					</div>
				</form>
			</div>
			<!-- modal -->
		 	<div class="dropdown" id="bturaianklinik">
		 	  	<div id="titleInformasi" >Uraian Tindakan Klinik</div>
		        <div id="btnBawahCare" class="btnBawah" ><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>
	        <br>
	        <div class="tabelinformasi" id="ibturaianklinik">
				<form class="form-horizontal" role="form" method="POST" style="margin-left:20px;margin-right:20px;">					
				    <div class="form-group">
				        <div class="portlet-body" style="margin: 0px 10px 0px 10px">
				            <table class="table table-striped table-bordered table-hover tableDT" id="tableCare">
								<thead>
									<tr class="info">
										<th style="width:10px;">No.</th>
										<th>Waktu</th>
										<th>Tindakan</th>
										<th>Jasa Sarana</th>
										<th>Jasa Pelayanan</th>
										<th>BAKHP</th>
										<th>On faktur</th>
										<th>Paramedis</th>
										<th>Jumlah</th>
										<th width="80">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php  
										if (!empty($visit_care_klinik)) {
											$i = 0;
											foreach($visit_care_klinik as $value){
												$i++;
												echo "<tr>";
													echo "<td>".$i."</td>";										
													echo "<td>".$value['waktu_tindakan']."</td>";									
													echo "<td>".$value['nama_tindakan']."</td>";												
													echo "<td>".$value['j_sarana']."</td>";										
													echo "<td>".$value['j_pelayanan']."</td>";
													echo "<td>".$value['bakhp_this']."</td>";										
													echo "<td>".$value['on_faktur']."</td>";
													echo "<td>".$value['nama_petugas']."</td>";										
													echo "<td>".$value['jumlah']."</td>";
													echo "<td style='text-align:center'>-</td>";
												echo "</tr>";
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>

	        <br>
		</div>

		<div class="tab-pane" id="rmigd"> 
			<div class="dropdown" id="btnBawahTambahCare">
		        <div id="titleInformasi">Penanganan IGD  </div>
		        <div class="btnBawah floatright">	<i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i>  </div>
		   	</div>
		   	<div id="tbhCare" style="padding-left:5px;">
				<div class="portlet-body" style="margin: 20px 10px 0px 10px">
					<table id="tableoverviewigd" class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabeloverigd">
						<thead>
							<tr class="info" style="text_align: center;">
								<th width="20">No.</th>
								<th>Waktu</th>
								<th>Anamnesa</th>
								<th>Dokter Jaga</th>
								<th>Perawat Jaga</th>
								<th style="width:20px;">Action</th>
							</tr>
						</thead>
						<tbody id="tbody_overviewigd">
							<?php
								if (isset($overviewigd_history)) {
									$i = 0;
									if(!empty($overviewigd_history)){
										foreach ($overviewigd_history as $over) {
											echo'<tr>';
												echo'<td>'.(++$i).'</td>';
												echo'<td>'.$over['waktu'].'</td>';
												echo'<td>'.$over['anamnesa'].'</td>';
												echo'<td>'.$over['dokter_jaga'].'</td>';
												echo'<td>'.$over['perawat_jaga'].'</td>';
												echo'<td style="text-align:center;">
													<a href="#riwayatpenangananigd" class="viewdetailoverviewigd" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a>
													<input type="hidden" class="overviewigdid_detail" value="'.$over['id'].'">
													</td>';
											echo'</tr>';
										}
									}
								}
							?>
							
						</tbody>
					</table>																								
				</div>
			</div>
			<br>

		 	<div class="dropdown" id="bwuraianigd">
		 	  	<div id="titleInformasi" >Uraian Tindakan IGD</div>
		        <div id="btnBawahCare" class="btnBawah floatright"  style="margin-top:-25px;"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>
	        <div class="tabelinformasi" id="ibwuraianigd">
				<form class="form-horizontal" role="form" method="POST" style="margin-left:20px;margin-right:20px;">
					<br>					
				    <div class="form-group">
				        <div class="portlet-body" style="margin: 10px 10px 0px 10px">
				            <table class="table table-striped table-bordered table-hover" id="tableCare">
								<thead>
									<tr class="info">
										<th style="width:10px;">No.</th>
										<th>Waktu</th>
										<th>Tindakan</th>
										<th>Jasa Sarana</th>
										<th>Jasa Pelayanan</th>
										<th>BAKHP</th>
										<th>On faktur</th>
										<th>Paramedis</th>
										<th>Jumlah</th>
										<th>Total</th>
										<th width="80">Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr><td colspan='10' style='text-align:center'>Data Kosong</td>
									<td style="text-align:center">
									<a href="#hapusResep">
									<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
								</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>
			<br>
	   	</div> 

		<div class="tab-pane" id="rm"> 
			<div class="dropdown" id="btkunjungandokter">
		        <div id="titleInformasi">Kunjungan dan Penanganan Dokter  
		        </div>
		        <div class="btnBawah floatright" style="margin-top:-25px;">
		           	<i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i>
		        </div>
		   	</div>
		   	<div id="ibtkunjungandokter">
				
	 			<form class="form-horizontal" role="form" method="POST" id="submitoverviewperawatan">
		 			<div class="informasi" >
				       	<br>
		 				<div class="form-group">
							<label class="control-label col-md-3">Waktu Tindakan</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="waktukunjungandokter" data-date-autoclose="true" class="form-control" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>	

	        			<div class="form-group">
							<label class="control-label col-md-3">Dokter Visit</label>
							<div class="col-md-3">
								<input type="text" class="form-control" style="cursor:pointer;background-color:white" readonly id="dokteroverperawatan" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" required>
								<input type="hidden" id="id_dokteroverperawatan">
							</div>
						</div>

	        			<div class="form-group">
							<label class="control-label col-md-3">Anamnesa</label>
							<div class="col-md-4">
								<textarea class="form-control isian" id="anamnesaoverperawatan" name="anamnesaoverperawatan" placeholder="Anamnesa"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Diagnosa Utama</label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov" id="kode_utamaoverperawatan" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov" id="diagutamaoverperawatan" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Diagnosa Sekunder</label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov1" id="kode_sek1overperawatan" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov1" id="diagsek1overperawatan" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<label class="control-label col-md-2">1</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov2" id="kode_sek2overperawatan" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov2" id="diagsek2overperawatan" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<label class="control-label col-md-2">2</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov3" id="kode_sek3overperawatan" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov3" id="diagsek3overperawatan" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<label class="control-label col-md-2">3</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov4" id="kode_sek4overperawatan" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian ov4" id="diagsek4overperawatan" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<label class="control-label col-md-2">4</label>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Perkembangan Penyakit</label>
							<div class="col-md-5">
								<textarea class="form-control" id="perkembanganoverperawatan" name="perkembanganoverperawatan" placeholder="Perkembangan Penyakit"></textarea>
							</div>
						</div>

						<fieldset class="fsStyle">
							<legend>
				                Tanda Vital
							</legend>
							<div class="form-group">
								<label class="control-label col-md-3"  style="width:310px;">Tekanan Darah</label>
								<div class="col-md-2">
									<input type="text" class="form-control" id="tekanandarahoverperawatan" name="takanandarahoverperawatan" placeholder="Tekanan Darah">
								</div>
								<label class="control-label col-md-2">mmHg</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" style="width:310px;">Temperatur</label>
								<div class="col-md-2">
									<input type="number" class="form-control numberrequired" id="temperaturoverperawatan" name="temperaturoverperawatan" placeholder="Temperatur" >
								</div>
								<label class="control-label col-md-2">&deg;C</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" style="width:310px;">Nadi</label>
								<div class="col-md-2">
									<input type="number" class="form-control numberrequired" id="nadioverperawatan" name="nadioverperawatan" placeholder="Nadi" >
								</div>
								<label class="control-label col-md-2">x/menit</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" style="width:310px;">Pernapasan</label>
								<div class="col-md-2">
									<input type="number" class="form-control numberrequired" id="pernapasanoverperawatan" name="pernapasanoverperawatan" placeholder="Pernapasan">
								</div>
								<label class="control-label col-md-2">x/menit</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" style="width:310px;">Berat Badan</label>
								<div class="col-md-2">
									<input type="number" class="form-control numberrequired" id="beratoverperawatan" name="beratoverperawatan" placeholder="Berat Badan">
								</div>
								<label class="control-label col-md-2">Kg</label>
							</div>
						</fieldset>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<input type="hidden" id="v_id_perawatan" value="<?php echo($pasien['visit_id']) ?>">
							<input type="hidden" id="ri_id_perawatan" value="<?php echo($pasien['ri_id']) ?>">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
					<br>			        
			   	</form>

	   			<div class="tabelinformasi">
		   			<br>
	        		<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:20px;margin-left:20px;">
					
		        	<div class="portlet-body" style="margin: 10px 25px 0px 20px">
		        	<?php echo "<input type='hidden' id='jml_overkunjungan' value='".count($overview_kunjungandokter)."'>"; ?>
						<table id="tableoverviewperawatan" class="table table-striped table-bordered table-hover table-responsive tableDTUtama">
							<thead>
								<tr class="info" style="text_align: center;">
									<th width="20">No.</th>
									<th>Waktu</th>
									<th>Dokter Visit</th>
									<th>Diagnosa Utama</th>
									<th>Unit</th>
									<th style="width:20px;">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php  
									if (isset($overview_kunjungandokter)) {
										if (!empty($overview_kunjungandokter)) {
											$i = 0;
											foreach ($overview_kunjungandokter as $over) {
												$tgl = DateTime::createFromFormat('Y-m-d H:i:s', $over['waktu_visit']);
												echo '<tr>
														<td>'.(++$i).'</td>
														<td>'.$tgl->format('d F Y H:i:s').'</td>
														<td>'.$over['dokter'].'</td>
														<td>'.$over['diagnosa_utama'].'</td>
														<td>'.$over['nama_dept'].'</td>
														<td style="text-align:center;">
															<a href="#riwperawatan" class="viewdetailriwperawatan" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a>
															<input type="hidden" class="id_detailriwperawatan" value="'.$over['kunjungan_dok_id'].'"> 
														</td>
													</tr>'	;
											}
										}
									}
								?>
							</tbody>
						</table>												
					</div>
		        
			        <div class="modal fade" id="riwkondok" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<form class="form-horizontal" role="form" method="POST" id="riwkondok">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
						   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
						   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Penanganan IGD</h3>
						   			</div>
									<div class="modal-body" style="padding-left:80px;">

						   				<div class="form-group">
											<label class="control-label col-md-4">Waktu Tindakan</label>
											<div class="col-md-5">	
												<input type="text" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
											</div>
					        			</div>	
					        			<div class="form-group">
											<label class="control-label col-md-4">Anamnesa</label>
											<div class="col-md-7">
												<textarea class="form-control" id="anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
											</div>
										</div>

										<fieldset class="fsStyle">
											<legend>
								                Tanda Vital
											</legend>
											<div class="form-group">
												<label class="control-label col-md-4" >Tekanan Darah</label>
												<div class="col-md-5">
													<input type="text" class="form-control" id="tekanandarah" name="takanandarah" placeholder="Tekanan Darah" readonly>
												</div>
												<label class="control-label col-md-2">mmHg</label>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4">Temperatur</label>
												<div class="col-md-5">
													<input type="text" class="form-control" id="temperatur" name="temperatur" placeholder="Temperatur" readonly>
												</div>
												<label class="control-label col-md-2">&deg;C</label>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4">Nadi</label>
												<div class="col-md-5">
													<input type="text" class="form-control" id="nadi" name="nadi" placeholder="Nadi" readonly>
												</div>
												<label class="control-label col-md-2">x/menit</label>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4">Pernapasan</label>
												<div class="col-md-5">
													<input type="text" class="form-control" id="pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
												</div>
												<label class="control-label col-md-2">x/menit</label>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4" >Berat Badan</label>
												<div class="col-md-5">
													<input type="text" class="form-control" id="berat" name="berat" placeholder="Berat Badan" readonly>
												</div>
												<label class="control-label col-md-2">Kg</label>
											</div>
								  		</fieldset>

								  		<fieldset class="fsStyle">
											<legend>
								                Pemeriksaan Fisik
											</legend>
											<div class="form-group">
												<label class="control-label col-md-4">Kepala & Leher</label>
												<div class="col-md-2">
													<input type="text" class="form-control" id="kepalaleher" name="kepalaleher" placeholder="Hasil Pemeriksaan" >
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4">Thorax & ABD</label>
												<div class="col-md-2">
													<input type="text" class="form-control" id="thorax" name="thorax" placeholder="Hasil Pemeriksaan" >
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4">Extremitas</label>
												<div class="col-md-2">
													<input type="text" class="form-control" id="extremitas" name="extremitas" placeholder="Hasil Pemeriksaan" >
												</div>
											</div>
										</fieldset>

								  		<fieldset class="fsStyle">
											<legend>
								                Diagnosa & Terapi
											</legend>
											<div class="form-group">
												<label class="control-label col-md-4" >Dokter Pemeriksa</label>
												<div class="col-md-7">
													<input type="text" style="background-color:white" class="form-control" id="dokter" placeholder="Search Dokter" readonly>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4">Diagnosa Utama</label>
												<div class="col-md-3">
													<input type="text" class="form-control" id="kode_utama" placeholder="Kode" readonly>
												</div>
												<div class="col-md-4">
													<input type="text" class="form-control" placeholder=" Diagnosa" readonly>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4">Diagnosa Sekunder</label>
												<div class="col-md-3">
													<input type="text" class="form-control" id="kode_sek1" placeholder="Kode" readonly>
												</div>
												<div class="col-md-4">
													<input type="text" class="form-control" placeholder=" Diagnosa" readonly>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4"></label>
												<div class="col-md-3">
													<input type="text" class="form-control" id="kode_sek2" placeholder="Kode" readonly>
												</div>
												<div class="col-md-4">
													<input type="text" class="form-control" placeholder=" Diagnosa" readonly>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4"></label>
												<div class="col-md-3">
													<input type="text" class="form-control" id="kode_sek3" placeholder="Kode" readonly>
												</div>
												<div class="col-md-4">
													<input type="text" class="form-control" placeholder=" Diagnosa" readonly>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4"></label>
												<div class="col-md-3">
													<input type="text" class="form-control" id="kode_sek4" placeholder="Kode" readonly>
												</div>
												<div class="col-md-4">
													<input type="text" class="form-control" placeholder=" Diagnosa" readonly>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4" >Detail Diagnosa</label>
												<div class="col-md-7">
													<textarea class="form-control" id="detailDiagnosa" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4" >Terapi</label>
												<div class="col-md-7">
													<textarea class="form-control" id="terapi" name="terapi" placeholder="Terapi" readonly></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4" >Alergi</label>
												<div class="col-md-7">
													<input type="text" class="form-control" id="alergi" name="alergi" placeholder="Alergi" readonly>
												</div>
											</div>
								  		</fieldset>
						        	</div>
					        		
					        		<div class="modal-footer">
					        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
								    </div>
								</div>
							</div>
						</form>
					</div>
				</div>
		         
			</div>
			<br> 

		 	<div class="dropdown" id="bwasuhan">
		 	  	<div id="titleInformasi" >Asuhan Keperawatan</div>
		        <div id="btnBawahCare" class="btnBawah floatright"  style="margin-top:-25px;"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>
	       
			<div id="ibwasuhan">
				
				<form class="form-horizontal" role="form" method="POST" id="submitasuhankeperawatan">
					<div class="informasi">
				       	<br>
		 				<div class="form-group">
							<label class="control-label col-md-3">Waktu Tindakan</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="waktuasuhan" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
							<label class="control-label col-md-3">Perawat 1 </label>
							<div class="col-md-3">
								<input type="text" class="form-control" style="cursor:pointer;background-color:white" readonly id="perawatasuhan1" placeholder="Search Perawat" data-toggle="modal" data-target="#searchPerawat">
								<input type="hidden" id="idperawatasuh1">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Perawat 2 </label>
							<div class="col-md-3">
								<input type="text" class="form-control" style="cursor:pointer;background-color:white" readonly id="perawatasuhan2" placeholder="Search Perawat" data-toggle="modal" data-target="#searchPerawat">
								<input type="hidden" id="idperawatasuh2">
							</div>
						</div>

	        			<div class="form-group">
							<label class="control-label col-md-3">Perjalanan Penyakit</label>
							<div class="col-md-4">
								<textarea class="form-control" id="perjalananpenyakitasuhan" name="perjalanan" placeholder="Keterangan"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Pemberian Obat</label>
							<div class="col-md-4">
								<textarea class="form-control" id="pemberianobatasuhan" name="pemberianobat" placeholder="Keterangan"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Diet</label>
							<div class="col-md-4">
								<textarea class="form-control" id="dietasuhan" name="diet" placeholder="Keterangan"></textarea>
							</div>
						</div>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
							<input type="hidden" id="v_id_asuhan" value="<?php echo($pasien['visit_id']) ?>">
							<input type="hidden" id="ri_id_asuhan" value="<?php echo($pasien['ri_id']) ?>">
						</span>
					</div>
					<br>
	        	</form>	
        		<br>
	        	
        		<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:20px;margin-left:20px;">
				
	        	<div class="tabelinformasi">
	        		<div class="portlet box red">
	        		<?php echo "<input type='hidden' id='jml_overasuhan' value='".count($overview_asuhan)."'>"; ?>
						<div class="portlet-body" style="margin: 10px 20px 0px 20px">
							<table id="tabelasuhan" class="table table-striped table-bordered table-hover table-responsive tableDTUtama">
								<thead>
									<tr class="info" >
										<th style="width:30px"> No.</th>
										<th > Waktu </th>
										<th > Perawat 1 </th>
										<th > Perawat 2 </th>
										<th > Unit </th>
										<th width="80"> Action</th>
									</tr>
								</thead>
								<tbody>
									<?php  
										if (isset($overview_asuhan)) {
											if (!empty($overview_asuhan)) {
												$i = 0;
												foreach ($overview_asuhan as $value) {
													$tgl = DateTime::createFromFormat('Y-m-d H:i:s', $value['waktu_tindakan']);
													echo '<tr>
															<td>'.(++$i).'</td>
															<td>'.$tgl->format('d F Y H:i:s').'</td>
															<td>'.$value['perawat1'].'</td>
															<td>'.$value['perawat2'].'</td>
															<td>'.$value['nama_dept'].'</td>
															<td style="text-align:center">
																<input type="hidden" class="asuhan_id" value="'.$value['asuhan_id'].'">
																<a href="#datasuh" class="lihat_asuh" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>
																<a href="" class="hapus_asuh"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
															</td>
														</tr>';
												}
											}
										}
									?>
								</tbody>
							</table>
						</div>
						
					</div>
	        	</div>
				<div class="modal fade" id="datasuh" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<form class="form-horizontal" role="form" method="POST" id="detail_asuh">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
						   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
						   				<h3 class="modal-title" id="myModalLabel">Detail Asuhan Keperawatan</h3>
						   			</div>
									<div class="modal-body">
										<div class="informasi">
											<div class="form-group">
												<label class="control-label col-md-4">Waktu Tindakan</label>
												<div class="col-md-5" >
													<div class="input-icon">
														<i class="fa fa-calendar"></i>
														<input type="text" disabled data-date-autoclose="true" class="form-control" readonly id="waktu_asuh">
													</div>
												</div>
						        			</div>
						        			<div class="form-group">
												<label class="control-label col-md-4">Perawat 1 </label>
												<div class="col-md-5">
													<input type="text" readonly class="form-control"  readonly id="perawat1_asuh" >
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-4">Perawat 2 </label>
												<div class="col-md-5">
													<input type="text" readonly class="form-control"  readonly id="perawat2_asuh" >
												</div>
											</div>

						        			<div class="form-group">
												<label class="control-label col-md-4">Perjalanan Penyakit</label>
												<div class="col-md-6">
													<textarea class="form-control" readonly id="perjalanan_asuh" name="perjalanan_asuh"></textarea>
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-md-4">Pemberian Obat</label>
												<div class="col-md-6">
													<textarea class="form-control" readonly id="pemberianobat_asuh" name="pemberianobat" ></textarea>
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-md-4">Diet</label>
												<div class="col-md-6">
													<textarea class="form-control" readonly id="diet_asuh" name="diet" ></textarea>
												</div>
											</div>
										</div>
				       				</div>
					        		<br>
					        		<div class="modal-footer">
					        			<button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
								    </div>
								</div>
							</div>
						</form>
					</div>
	        </div>
			<br>

			<div class="dropdown" id="bturtin">
		 	  	<div id="titleInformasi" >Uraian Tindakan </div>
		        <div id="btnBawahCare" class="btnBawah floatright"  style="margin-top:-25px;"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>
	        <div class="tabelinformasi" id="ibturtin">
				<form class="form-horizontal" role="form" method="POST" style="margin-left:20px;margin-right:20px;">
					<div class="form-group" style="margin-bottom:-25px;">
						<a href="#tambahTindakanok" data-toggle="modal"  style="margin-left:15px;font-size:11pt;"	><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah">&nbsp;Tambah Tindakan</i></a>
						<div class="clearfix"></div>        
					</div>
				    <div class="form-group">
				    	<?php
				        	echo '<input type="hidden" id="jml_tindak_rawat" value="'.count($visit_care_unit).'" >';
				        ?>
				        <div class="portlet-body" style="margin: 0px 20px 0px 15px">
				            <table class="table table-striped table-bordered table-hover tableDT" id="tableCareRawat">
								<thead>
									<tr class="info">
										<th style="width:10px;">No.</th>
										<th>Waktu</th>
										<th>Tindakan</th>
										<th>Jasa Sarana</th>
										<th>Jasa Pelayanan</th>
										<th>BAKHP</th>
										<th>On faktur</th>
										<th>Paramedis</th>
										<th>Jumlah</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php  
										if (!empty($visit_care_unit)) {
											$i = 0;
											foreach($visit_care_unit as $value){
												$tgl = DateTime::createFromFormat('Y-m-d H:i:s', $value['waktu_tindakan']);
												$i++;
												echo "<tr>";
													echo "<td>".$i."</td>";										
													echo "<td>".$tgl->format('d F Y H:i:s')."</td>";									
													echo "<td>".$value['nama_tindakan']."</td>";												
													echo "<td>".$value['j_sarana']."</td>";										
													echo "<td>".$value['j_pelayanan']."</td>";
													echo "<td>".$value['bakhp_this']."</td>";										
													echo "<td>".$value['on_faktur']."</td>";
													echo "<td>".$value['nama_petugas']."</td>";										
													echo "<td>".$value['jumlah']."</td>";
													echo "<td style='text-align:center'><a style='cursor:pointer;' class='hapusTindakan'><input type='hidden' class='getid' value='".$value['care_id']."''><i class='glyphicon glyphicon-trash'></i></a></td>";
												echo "</tr>";
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>
			
	        <br>
	        <div class="modal fade" id="tambahTindakanok" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="submitTindakanPerawatan">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Tindakan Perawatan</h3>
				   			</div>
							<div class="modal-body">
								<div class="informasi">
					   				<div class="form-group">
										<label class="control-label col-md-4">Waktu Tindakan</label>
										<div class="col-md-5">	
											<input type="text" id="tin_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
										</div>
				        			</div>
				        			<div class="form-group">
										<label class="control-label col-md-4">Tindakan</label>
										<div class="col-md-6">
											<input type="hidden" id="idtindakan_klinik">
											<input type="text" class="form-control" id="namatindakan" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Tindakan" />
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-4"></label>
										<div class="col-md-6">
											<textarea class="form-control" id="preview_tindakan" placeholder="Tindakan" readonly=""></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-4">Kelas Pelayanan</label>
										<div class="col-md-5">	
											<input type="hidden" id="idtindakdetail">
											<select class="form-control" name="kelas_tindakan" id="kelas_klinik">
												<option value="">Pilih Kelas</option>
												<option value="Kelas VIP">VIP</option>
												<option value="Kelas Utama">Utama</option>
												<option value="Kelas I">Kelas I</option>
												<option value="Kelas II">Kelas II</option>
												<option value="Kelas III">Kelas III</option>
											</select>
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Tarif</label>
										<div class="col-md-5">	
											<input type="hidden" id="js_klinik">
											<input type="hidden" id="jp_klinik">
											<input type="hidden" id="bakhp_klinik">
											<input type="text" class="form-control" id="js_tarif" name="tarif" placeholder="Tarif" readonly > 
										</div>
				        			</div>
				        			
				        			<div class="form-group">
										<label class="control-label col-md-4">On Faktur</label>
										<div class="col-md-5">	
											<input type="number" class="form-control" id="js_onfaktur" name="onfaktur" placeholder="On Faktur" >
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Jumlah</label>
										<div class="col-md-5">	
											<input type="text" class="form-control" id="js_jumlah" name="jumlah" placeholder="Jumlah" readonly>
										</div>
				        			</div>

									<div class="form-group">
										<label class="control-label col-md-4">Paramedis</label>
										<div class="col-md-5">	
											<input type="hidden" id="paramedis_id">
											<input type="text" class="form-control" id="js_paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
										</div>
				        			</div>

				        			<div class="form-group">
										<label class="control-label col-md-4">Paramedis Lain</label>
										<div class="col-md-6">	
											<textarea class="form-control" id="js_paramedis_lain" name="paramedis" placeholder="Paramedis, dipisah dengan koma (,)" ></textarea>
										</div>
				        			</div>
			        			</div>
		       				</div>
			        		<br><br>
			        		<div class="modal-footer">
			        			<input type="hidden" class="visit_id_t" value="<?php echo($pasien['visit_id']) ?>">
								<input type="hidden" class="ri_id_t" value="<?php echo($pasien['ri_id']) ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
						    </div>
						</div>
					</div>
				</form>
			</div>
		</div>  

        <div class="tab-pane" id="resep">
	 		<div class="dropdown" id="btnBawahTambahResep">
    		    <div id="titleInformasi">Tambah Resep</div>
        		<div id="btnBawahTambahResep" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
        	</div>
            <br>
        	<div id="tambahResep">
        		<form class="form-horizontal" role="form" method="POST" id="submitresep">
	        		<div class="informasi" >
						<div class="form-group">
							<label class="control-label col-md-3">Dokter</label>
							<div class="col-md-3">
								<input type="hidden" id="resep_id_dokter">
								<input type="text" class="form-control" readonly="" style="cursor:pointer" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" id="resep_namadokter">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Tanggal</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="resep_date" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Deskripsi Resep</label>
							<div class="col-md-5">
								<textarea class="form-control" name="deskripsiResep" placeholder="Deskripsi Resep" id="resep_deskripsi"></textarea>							
							</div>
						</div>
					</div>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
							<input type="hidden" id="r_id_resep" value="<?php echo $pasien['ri_id']; ?>">
							<input type="hidden" id="v_id_resep" value="<?php echo $pasien['visit_id']; ?>">
						</span>
					</div>
					<br>
				</form>		
			</div>

	 		<div class="dropdown" id="btnBawahTabelResep">
		        <div id="titleInformasi">Riwayat Tabel Resep</div>
		        <div id="btnBawahTabelResep" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
            <div id="tblResep">
	        	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
	        		<input type="hidden" id="jml_resep" value="<?php echo count($visit_resep); ?>">
					<table class="table table-striped table-bordered table-hover tableDT" id="tableResep">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Dokter</th>
								<th>Tanggal</th>
								<th>Deskripsi Resep</th>
								<th>Status Bayar</th>
								<th>Status Ambil</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<?php  
							if (!empty($visit_resep)) {
								$i = 0;
								foreach ($visit_resep as $value) {
									$i++;
									$tgl = strtotime($value['tanggal']);
									$hasil = date('d F Y', $tgl); 
									echo '<tr>';
									echo '<td>'.$i.'</td>';
									echo '<td>'.$value['nama_petugas'].'</td>';										
									echo '<td>'.$hasil.'</td>';										
									echo '<td>'.$value['resep'].'</td>';										
									echo '<td>'.$value['status_bayar'].'</td>';										
									echo '<td>'.$value['status_ambil'].'</td>';										
									echo '<td style="text-align:center">';
										echo '<a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value="'.$value['resep_id'].'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>';
									echo '</td>';
									echo '</tr>';
								}
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
			<br>
        </div>

         <div class="tab-pane" id="penunjang">
	        <div class="dropdown" id="btnBawahPenunjang">
		        <div id="titleInformasi">Pemeriksaan Penunjang</div>
		        <div class="btnBawah" id="btnBawahPenunjang"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>
		    <br>

            <div id="infoPenunjang">
	            <form class="form-horizontal" id="submit_penunjang">
	            	<div class="informasi" >
		          		<div class="form-group">
							<label class="control-label col-md-3">Tanggal</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tun_date" class="form-control isian calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
								</div>
							</div>
						</div>	
						<div class="form-group">
							<label class="control-label col-md-3" >Tujuan Penunjang</label>
							<div class="col-md-2">			
								<select class="form-control select" name="depTujuan" id="tun_tujuan" required>
									<option value="" selected>Pilih Unit Penunjang</option>
									<?php
										foreach ($penunjang as $data) {
											echo '<option value="'.$data['dept_id'].'">'.$data['nama_dept'].'</option>';
										}
									?>
								</select>		
							</div>							
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" >Pengirim</label>
							<div class="col-md-3">
								<input type="hidden" id="tun_iddokter">
								<input type="text" class="form-control" id="tun_namadokter" placeholder="Search Pengirim" data-toggle="modal" data-target="#searchDokter" required>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Jenis Pemeriksaan</label>
							<div class="col-md-5">
								<textarea class="form-control" rows="5" id="tun_jenis" placeholder="Jenis Penunjang" required></textarea>
							</div>
						</div>
					</div>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="submit" class="btn btn-success">Tambah</button>
						</span>
					</div>
				</form>		
				<br>
	        </div>

	        <div class="dropdown" id="btnBawahTabelRiwayat">
		        <div id="titleInformasi">Tabel Riwayat Pemeriksaan</div>
		        <div id="btnBawahTabelRiwayat" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
            <br>

        	<div class="tabelinformasi" id="tblRiwayat">
	        	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
	        		<?php echo '<input type="hidden" id="tun_jumlah" value="'.count($visit_penunjang).'">'?>
					<table class="table table-striped table-bordered tableDTUtama table-hover" id="table_penunjang">
						<thead>
							<tr class="info">
								<th width="3%"> No. </th>
								<th> Tanggal Tindakan</th>
								<th> Departemen Penunjang</th>
								<th> Unit Rujukan</th>
								<th> Status </th>
								<th style="width:20px;"> Details</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 0;
								foreach ($visit_penunjang as $data) {
									$tgl = strtotime($data['waktu']);
									$hasil = date('d F Y', $tgl); 
									
									$no++;
									echo '
										<tr>
											<td>'.$no.'</td>
											<td align="center">'.$hasil.'</td>								
											<td>'.$data['unit_tujuan'].'</td>										
											<td>'.$data['unit_asal'].'</td>										
											<td>'.$data['status_penunjang'].'</td>											
											<td style="text-align:center">
												<input type="hidden" class="idpenunjang" value="'.$data['penunjang_id'].'">
												<a href="#viewRiwayat" class="detailpenunjang" data-toggle="modal" data-placement="top"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Details"></i></a>
											</td>												
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="modal fade" id="viewRiwayat" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-dialog" style="width:1300px;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        	<h3 class="modal-title" id="myModalLabel">Hasil Pemeriksaan</h3>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-3 nama goright">Order ID:</label>
										<div class="col-md-9 nama" id="dp_penunjang">	0001 </div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-3 goright">Tanggal Tindakan:</label>
										<div class="col-md-8" id="dp_date">
											12 Desember 2012
										</div>
									</div>
								</div>
								
								
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-4 goright">Departemen Penunjang:</label>
										<div class="col-md-8" id="dp_dept"> Labolatorium	</div>
									</div>
								</div>
								
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-3 goright">Pemeriksa:</label>
										<div class="col-md-8" id="dp_periksa" >terserah</div>
									</div>
								</div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-4 goright">Status Hasil:</label>
										<div class="col-md-8" id="dp_status">SELESAI</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<hr/>
							<table class="table table-striped table-bordered table-hover" id="tabelHasilPenunjang">
								<thead>
									<tr class="info">
										<th>Jenis Pemeriksaan</th>
										<th>Hasil Pemeriksaan</th>
										<th>Nilai Normal</th>
										<th>Keterangan/Rujukan</th>
									</tr>
								</thead>
								<tbody id="tbody_detail">
									
								</tbody>
							</table>
						</div>
						<div class="modal-footer">
				 			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
        </div>

        <div class="tab-pane" id="orderkamar">
        	<div class="dropdown" id="btnBawahOrder">
	            <div id="titleInformasi">Order Kamar Operasi</div>
	            <div class="btnBawah" id="btnBawahOrder"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>

	        <div id="infoKamar">
		        <form class="form-horizontal" method="POST" id="submit_order_operasi">
		        	<div class="informasi">
		          		<div class="form-group">
							<label class="control-label col-md-3">Waktu Pelaksanaan</label>
							<div class="col-md-3" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="operasi_date" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii:ss" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i:s");?>">
								</div>
							</div>
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3">Dokter</label>
							<div class="col-md-3">
								<input type="hidden" id="iddokter_o">
								<input type="text" class="form-control" style="cursor:pointer" readonly="" id="namadokter_o" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Jenis Operasi</label>
							<div class="col-md-3">			
								<select class="form-control select" name="operasi_jenis" id="operasi_jenis">
									<option value="" selected>Pilih Jenis Operasi</option>
									<option value="Kecil">Kecil</option>
									<option value="Sedang">Sedang</option>
									<option value="Besar">Besar</option>
									<option value="Khusus">Khusus</option>
								</select>
					 		</div>
						</div>
								
						<div class="form-group">
							<label class="control-label col-md-3" >Detail Operasi</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="5" id="operasi_detail" placeholder="Detail Operasi"></textarea>
					 		</div>
						</div>
					</div>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
							<input type="hidden" id="r_id_operasi" value="<?php echo $pasien['ri_id']; ?>">
							<input type="hidden" id="v_id_operasi" value="<?php echo $pasien['visit_id']; ?>">	
						</span>
					</div>
					<br>	
							
				</form>
				<br>
			</div>	

			<div class="dropdown" id="btnTableKamarOperasi">
	            <div id="titleInformasi">Riwayat Operasi</div>
	            <div class="btnBawah" id="btnTableKamarOperasi"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <div class="tabelinformasi" id="tabelKamar">
			<input type="hidden" id="jml_data_order" value="<?php echo count($order_operasi); ?>">
	           	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tableOpeasi" >
						<thead>
							<tr class="info">
								<th width="20">No. </th>
								<th width="200">Waktu Tindakan</th>
								<th>Dokter</th>
								<th>Status</th>
								<th>Keterangan Order</th>
								<th width="20">Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i = 1;
							if(!empty($order_operasi)){
								foreach ($order_operasi as $value) {
									echo"
										<tr>
											<td>".$i."</td>
											<td>".$value['waktu_mulai']."</td>
											<td>".$value['nama_petugas']."</td>										
											<td>Kamar Operasi</td>
											<td>".$value['det_operasi']."</td>
											<td style='text-align:center'>
												<i class='glyphicon glyphicon-trash hapusorder' data-toggle='tooltip' data-placement='top' style='cursor:pointer;' title='Hapus'><input type='hidden' class='getid' value='".$value['order_operasi_id']."'></i>
											</td>										
										</tr>
									";
									$i++;
								}
							}
						?>
						</tbody>
					</table>
				</div>	<br><br>			</div>	
			<br>
        </div>

        <div class="tab-pane" id="konsul">
        	<div class="dropdown" id="btnBawahOrderKonsul">
	            <div id="titleInformasi">Order Konsultasi Gizi</div>
	            <div class="btnBawah" id="btnBawahOrderKonsul"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>
	        <div id="infoKonsul">
		      	<form class="form-horizontal" role="form" id="submit_gizi">
		      		<div class="informasi">
		          		<div class="form-group">
							<label class="control-label col-md-3">Tanggal Konsultasi</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" id="konsul_date" style="cursor:pointer;background-color:white;" class="form-control isian calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
								</div>
							</div>
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3" >Konsultan Gizi</label>
							<div class="col-md-3">
								<input type="hidden" id="konsul_idDokter">
								<input type="text" class="form-control" id="konsul_dokter" placeholder="Search Konsultan" data-toggle="modal" data-target="#searchDokter" style="cursor:pointer" readonly="">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Kajian Gizi</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="konsul_kajiangizi" placeholder="Kajian Gizi"></textarea>
								
						 	</div>				
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Anamnesa Diet</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="konsul_anemdiet" placeholder="Anamnesa Diet"></textarea>
								
						 	</div>		
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Kajian Diet</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="konsul_kajiandiet"  placeholder="Kajian Diet"></textarea>
								
						 	</div>		
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Detail Menu Sehari-hari</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="konsul_detail"  placeholder="Detail Menu"></textarea>
								
						 	</div>		
						</div>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
							<input type="hidden" id="r_id_gizi" value="<?php echo $pasien['ri_id']; ?>">
							<input type="hidden" id="v_id_gizi" value="<?php echo $pasien['visit_id']; ?>">	
						</span>
					</div>
					<br>
				</form>
			</div>	

			<div class="dropdown" id="btnTabelKonsultasi">
	            <div id="titleInformasi">Riwayat Konsultasi Gizi</div>
	           	<div class="btnBawah" id="btnTabelKonsultasi"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>
	        <div class="tabelinformasi" id="tabelKonsultasi" >
	           	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
	           		<input type="hidden" id="jml_gizi" value="<?php echo count($gizi); ?>">
					<table class="table table-striped table-bordered table-hover tableDT" id="tableKonsultasi">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal Konsultasi </th>
								<th>Konsultan</th>
								<th>Kajian Gizi</th>
								<th>Anamnesa Diet</th>
								<th>Kajian Diet</th>
								<th>Detail Menu Sehari-hari</th>
								<th width="100">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!empty($gizi)){
								$no = 0;
								foreach ($gizi as $data) {
									$tgl = strtotime($data['tanggal']);
									$hasil = date('d F Y', $tgl);

									$no++;
									echo'
										<tr>
											<td>'.$no.'</td>
											<td>'.$hasil.'</td>
											<td>'.$data['nama_petugas'].'</td>
											<td>'.$data['kajian_gizi'].'</td>										
											<td>'.$data['anamnesa_diet'].'</td>
											<td>'.$data['kajian_diet'].'</td>
											<td>'.$data['detail_menu'].'</td>
											<td style="text-align:center">
												<a style="cursor:pointer;" class="hapusgizi"><input type="hidden" class="getid" value="'.$data['gizi_id'].'"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
												<a href="#print"><i class="glyphicon glyphicon-print"  data-toggle="tooltip" data-placement="top" title="Print"></i></a>
											</td>										
										</tr>
									';
									$i++;
								}
							}
							?>
						</tbody>
					</table>
				</div>	<br>
			</div>	
			<br>
        </div>     

        <div class="tab-pane" id="riwayat">
         	<div class="dropdown" id="rwp1">
            	<div id="titleInformasi">Riwayat Klinik</div>
            	<div class="btnBawah" id="btnBawahRiwayat"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div class="portlet-body" id="tblrwp1" style="margin: 20px 10px 0px 10px">
            	
				<table class="table table-striped table-bordered table-hover table-responsive tableDT">
					<thead>
						<tr class="info" style="text_align: center;">
							<th width="20">No.</th>
							<th>Tanggal</th>
							<th>Unit</th>
							<th>Anamnesa</th>
							<th>Dokter Pemeriksa</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($riwayat_klinik as $data) {
								$i++;
								$tgl = strtotime($data['waktu']);
								$hasil = date('d F Y H:i:s', $tgl);

								echo '
									<tr>
										<td>'.$i.'</td>
										<td style="text-align:center">'.$hasil.'</td>
										<td>'.$data['nama_dept'].'</td>
										<td>'.$data['anamnesa'].'</td>
										<td>'.$data['nama_petugas'].'</td>
										<td style="text-align:center;"><a href="#riwayat_over" data-toggle="modal" onClick="detailOver(&quot;'.$data['id'].'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>
									</tr>
								';
							}
						?>
					</tbody>
				</table>												
			</div>
			<br>	

            <div class="dropdown" id="rwp2">
            	<div id="titleInformasi">Riwayat IGD</div>
            	<div class="btnBawah" id="btnBawahRiwayat"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div class="portlet-body" id="tblrwp2" style="margin: 20px 10px 0px 10px">
            	
				<table class="table table-striped table-bordered table-hover table-responsive tableDT">
					<thead>
						<tr class="info" style="text_align: center;">
							<th width="20">No.</th>
							<th>Tanggal</th>
							<th>Anamnesa</th>
							<th>Dokter Jaga</th>
							<th>Perawat Jaga</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($riwayat_igd as $data) {
								$i++;
								$tgl = strtotime($data['waktu']);
								$hasil = date('d F Y H:i:s', $tgl);

								echo '
									<tr>
										<td>'.$i.'</td>
										<td style="text-align:center">'.$hasil.'</td>										
										<td>'.$data['anamnesa'].'</td>
										<td>'.$data['rdokter'].'</td>
										<td>'.$data['rperawat'].'</td>
										<td style="text-align:center;"><a href="#riwayatpenangananigd" data-toggle="modal" onClick="detailOverIgd(&quot;'.$data['id'].'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>
									</tr>
								';
							}
						?>
					</tbody>
				</table>												
			</div>
			<br>

			<div class="dropdown" id="rwp3">
            	<div id="titleInformasi">Riwayat Perawatan</div>
            	<div class="btnBawah" id="btnBawahRiwayat"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div class="portlet-body" id="tblrwp3" style="margin: 20px 10px 0px 10px">
            	
            	<table class="table table-striped table-bordered table-hover table-responsive tableDT">
					<thead>
						<tr class="info" style="text_align: center;">
							<th style="width:10px;">No.</th>
							<th>Tanggal</th>
							<th>Dokter Visit</th>
							<th>Diagnosa Utama</th>
							<th>Perkembangan Penyakit</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php  
							if (isset($riwayat_perawatan)) {
								if (!empty($riwayat_perawatan)) {
									$i = 0;
									foreach ($riwayat_perawatan as $over) {
										$tgl = DateTime::createFromFormat('Y-m-d H:i:s', $over['waktu_visit']);
										echo '<tr>
												<td>'.(++$i).'</td>
												<td>'.$tgl->format('d F Y H:i:s').'</td>
												<td>'.$over['rdokter'].'</td>
												<td>'.$over['diagnosa_utama'].'</td>
												<td>'.$over['perkembangan_penyakit'].'</td>
												<td style="text-align:center;">
													<a href="#riwperawatan" class="viewdetailriwperawatan" onClick="detailOverPerawatan(&quot;'.$over['kunjungan_dok_id'].'&quot;)" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a>
												</td>
											</tr>'	;
									}
								}
							}
						?>
					</tbody>
				</table>												
			</div>
			<br>
        </div>

        <div class="tab-pane" id="makan">
        	<div class="dropdown" id="btpermintaanmakan">
	            <div id="titleInformasi">Permintaan Makan</div>
		        <div class="btnBawah" id="btnBawahOrderMakan"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		   	</div>
		    <br>

            <div id="infoMakan">
            	<form class="form-horizontal" role="form" method="post" id="sumbitpermintaanmakan">
	            	<div class="informasi">
		          		<div class="form-group">
							<label class="control-label col-md-3">Tanggal</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control isian calder" readonly data-date-format="dd/mm/yyyy H:i:s" data-provide="datetimepicker" id="tanggalmakan" value="<?php echo date("d/m/Y H:i:s");?>">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Tipe Diet Penyakit</label>
							<div class="col-md-2">
								<select class="form-control select" name="tipediet" id="tipediet" >
									<option value="" selected>Pilih</option>
									<?php  
										if (isset($tipediet)) {
											foreach ($tipediet as $value) {
												echo '<option value="'.$value['id'].'">'.$value['tipe_diet'].'</option>';
											}
										}
									?>															
								</select>			
							</div>							
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Paket Makanan</label>
							<div class="col-md-2">			
								<select class="form-control select" name="paketmakan" id="paketmakan">
									<option value="" selected>Pilih</option>
								</select>	
							</div>							
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Keterangan</label>
							<div class="col-md-4">			
								<textarea class="form-control" id="keteranganmakan"></textarea>
						 	</div>
						</div>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
							<input type="hidden" class="visit_id_makan" value="<?php echo($pasien['visit_id']) ?>">
							<input type="hidden" class="ri_id_makan" value="<?php echo($pasien['ri_id']) ?>">
							<input type="hidden" class="bed_id_makan" value="<?php echo($pasien['bed_id']) ?>">
							<input type="hidden" class="kamar_id_makan" value="<?php echo($pasien['kamar_id']) ?>">
						</span>
					</div>
					<br>
				</form>
			</div>	

			<div class="dropdown" id="btriwpermintaanmakan">
	            <div id="titleInformasi">Riwayat Permintaan Makan </div>
	          	<div class="btnBawah" id="btnDaftarmakan"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>

            <div id="ibtriwpermintaanmakan" >
              	<input type="hidden" id="jml_makan" value="<?php echo count($permintaan_makan); ?>">
              	<div class="portlet-body" style="margin: 0px 20px 0px 20px">
					<table class="table table-striped table-bordered table-hover tableDT" id="tabelriwayatmakan" >
						<thead>
							<tr class="info">
								<th width="20">No</th>
								<th>Tanggal</th>
								<th>Tipe Diet Penyakit</th>
								<th>Paket Makanan</th>
								<th>Keterangan</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php  
								if (isset($permintaan_makan)) {
									$i = 0;
									foreach ($permintaan_makan as $value) {
										$tgl = DateTime::createFromFormat('Y-m-d H:i:s', $value['waktu_permintaan']);
										echo '<tr>
												<td>'.(++$i).'</td>
												<td style="text-align:center">'.$tgl->format('d F Y H:i:s').'</td>
												<td>'.$value['nama_diet'].'</td>
												<td>'.$value['nama_paket'].'</td>
												<td>'.$value['keterangan'].'</td>
												<td>'.$value['status'].'</td>
												<td style="text-align:center">
													<a href="#editpaketmakan" data-toggle="modal" class="editmakan"><i class="glyphicon glyphicon-edit"  data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
													<a href="#" class="hapusmakan"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
													<input type="hidden" class="makan_id" value="'.$value['makan_id'].'">
												</td>										
											</tr>';
									}
								}
							?>
							
						</tbody>
					</table>
				</div>	<br>
				<div class="modal fade" id="editpaketmakan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<form class="form-horizontal informasi" role="form" method="post" id="submit_edit_makan">
							<div class="modal-content" >
								<div class="modal-header">
			        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			        				<h3 class="modal-title" id="myModalLabel">Edit Permintaan Makan</h3>
			        			</div>
			        			<div class="modal-body">
			        			
									<div class="form-group">
										<label class="control-label col-md-4">Tanggal</label>
										<div class="col-md-5" >
											<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input type="text" style="cursor:pointer;background-color:white" class="form-control isian calder" id="editwaktumakan" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
											</div>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-4" >Tipe Diet Penyakit</label>
										<div class="col-md-5">
											<select class="form-control select" name="edittipediet" id="edittipediet">
												<option value="" selected>Pilih</option>
												<?php  
													if (isset($tipediet)) {
														foreach ($tipediet as $value) {
															echo '<option value="'.$value['id'].'">'.$value['tipe_diet'].'</option>';
														}
													}
												?>							
											</select>			
										</div>							
									</div>

									<div class="form-group">
										<label class="control-label col-md-4" >Paket Makanan</label>
										<div class="col-md-5">			
											<select class="form-control select" name="paketmakanedit" id="paketmakanedit">
												<option value="" selected>Pilih</option>
											</select>	
										</div>							
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Keterangan</label>
										<div class="col-md-5">			
											<textarea class="form-control" id="editketeranganmakan"></textarea>
									 	</div>
									</div>
			        			</div>
			        			<div class="modal-footer">
			        				<input type="hidden" id="edit_makan_id">
			        				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
			 			       		<button type="submit" class="btn btn-success">Simpan</button>
						      	</div>
							</div>
						</form>
					</div>
				</div>
			</div>	
        </div>

		<div class="tab-pane" id="pindah"  id="btnBawahPindah">
			<div class="dropdown" id="btpindah">
	            <div id="titleInformasi">Pindah Pasien</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="ibtpindah">
            	<form class="form-horizontal" role="form">
		        	<div class="informasi">
		            	<div class="form-group">
							<label class="control-label col-md-3">Tanggal Pindah Kamar</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control" readonly data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
								</div>
							</div>
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3">Departemen Tujuan</label>
							<div class="col-md-2">
								<select class="form-control select" id="kamar">
									<option value="" selected>--Pilih Departement--</option>
									<option value="Kamar Bersalin">Kamar Bersalin</option>
									<option value="UGD"  >UGD</option>
									<option value="ICU" >ICU</option>
									<option value="Kamar Anak" >Kamar Anak</option>
									<option value="Umum" >Umum</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Pilih Kamar & Kelas Kamar</label>
							<div class="col-md-3">
								<input type="text" class="form-control" id="kamar" placeholder="Search Kamar" data-toggle="modal" data-target="#pilkamar">											
							</div>
						</div>	
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button class="btn btn-success" type="submit">SIMPAN</button> 
						</span>
					</div>
					<br>
				</form>	
				<br>
            </div>

            <div class="modal fade" id="pilkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-300px">
	        	<div class="modal-dialog">
	        		<div class="modal-content" style="width:900px">
	        			<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Kamar</h3>
	        			</div>	
	        			<div class="modal-body">

	        				<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchPengirim">
									<thead>
										<tr class="success">
											<td>Kamar</td>
											<td>Kelas</td>
											<td>Jumlah Bed</td>
											<td>Terpakai</td>
											<td width="10%" style="text-align:center;">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Melati</td>
											<td>Kelas III</td>
											<td>2</td>
											<td>0</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td>Bed 1</td>
											<td style="text-align:center;"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td>Bed 2</td>
											<td style="text-align:center;"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td>Mawar</td>
											<td>Kelas III</td>
											<td>2</td>
											<td>0</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td>Bed 1</td>
											<td style="text-align:center;"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td>Bed 2</td>
											<td style="text-align:center;"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></td>
										</tr>

									</tbody>
								</table>												
							</div>
		        			
	      				</div>
	      				<br>
	      				<div class="modal-footer">
	 			       		<button type="button" data-dismiss="modal" class="btn btn-warning">Keluar</button>	
	 			       	</div>

	        		</div>
	        	</div>        	
		    </div>	

            <div class="dropdown" id="btriwpindah">
	            <div id="titleInformasi">Status Pindah Pasien</div>
	            	<div class="btnBawah" id="btnTableKamarOperasi"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	          	</div>
	           	<br>

	            <div id="ibtriwpindah">
	              	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" >
								<thead>
									<tr class="info">
										<th width="20">No. </th>
										<th>Tanggal Tindakan</th>
										<th>Dokter</th>
										<th>Tujuan Order</th>
										<th>Keterangan Order</th>
										<th>Status Hasil</th>
										<th width="80">Delete </th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="center">11</td>
										<td align="center">4/18/2015</td>
										<td>Labolatorium</td>										
										<td>APS</td>
										<td>Bebas </td>
										<td>BELUM</td>
										<td style="text-align:center">
											<a href="#hapusResep">
											<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
										</td>										
									</tr>
									<tr>
										<td align="center">11</td>
										<td align="center">4/18/2015</td>
										<td>Labolatorium</td>										
										<td>APS</td>
										<td>Bebas </td>
										<td>BELUM</td>
										<td style="text-align:center">
											<a href="#hapusResep">
											<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
										</td>										
									</tr>
									<tr>
										<td align="center">11</td>
										<td align="center">4/18/2015</td>
										<td>Labolatorium</td>										
										<td>APS</td>
										<td>Bebas </td>
										<td>BELUM</td>
										<td style="text-align:center">
											<a href="#hapusResep">
											<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"	></i></a>
										</td>										
									</tr>
								</tbody>
							</table>
			
					</div>	<br><br>
			</div>	
		</div>

		<div class="tab-pane" id="catatan">
        	<div class="dropdown" id="btnBawahBersalin">
            	<div id="titleInformasi">Tambah Kegiatan Bersalin</div>
            	<div class="btnBawah" ><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="infoBersalin">
            	<form class="form-horizontal" role="form" method="post" id="submitkegiatanbersalin">
	            	<div class="informasi">
		            	<div class="form-group">
							<label class="control-label col-md-3" >Jenis Kegiatan
							</label>
							<div class="col-md-2">			
								<select class="form-control select" name="pilJnsKegiatan" id="pilJnsKegiatan">
									<option value="" selected>Pilih</option>
									<option value="Persalinan Normal">Persalinan Normal</option>
									<option value="Sectio Caesaria">Sectio Caesaria</option>
									<option value="komplikasi">Pers. dg Komplikasi</option>
									<option value="Abortus">Abortus</option>
									<option value="Imunisasi TT-1">Imunisasi TT-1</option>
									<option value="Imunisasi TT-2">Imunisasi TT-2</option>
								</select>
							</div>					
						</div>	

						<div class="form-group" id="komp">
							<label class="control-label col-md-3" >Komplikasi
							</label>
							<div class="col-md-2">			
								<select class="form-control select" name="pilKegiatan" id="pilkomplikasi">
									<option value="" selected>Pilih</option>
									<option value="Perd seblm Persalinan">Perd seblm Persalinan</option>
									<option value="Perd sesdh Persalin">Perd sesdh Persalinan</option>
									<option value="Pre Eclampsi">Pre Eclampsi</option>
									<option value="Eclampsi">Eclampsi</option>
									<option value="Infeksi">Infeksi</option>
									<option value="Lain - lain">Lain - lain</option>
								</select>
							</div>							
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Rujukan
							</label>
							<div class="col-md-2">			
								<select class="form-control select" name="rujukan" id="rujukanbresalin">
									<option value="" selected>Pilih</option>
									<option value="Rumah Sakit">Rumah Sakit </option>
									<option value="Bidan">Bidan</option>
									<option value="Puskesmas">Puskesmas</option>
									<option value="Faskes Lainnya">Faskes Lainnya </option>
									<option value="Non Medis">Non Medis</option>
								</select>
							</div>							
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3" >Status
							</label>
							<div class="col-md-2">			
								<select class="form-control select" name="rujukan" id="statusbersalin">
									<option value="" selected>Pilih</option>
									<option value="hidup">Hidup</option>
									<option value="mati">Mati</option>
									
								</select>
							</div>							
						</div>


						<div class="form-group">
							<label class="control-label col-md-3">Tanggal Pelaksanaan</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tgl_pelaksanaanbersalin" class="form-control isian calder" readonly data-date-format="dd/mm/yyyy H:i:s" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i:s");?>">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Dirujuk
							</label>
							<div class="col-md-2">			
								<select class="form-control select" name="statusRujukan" id="statusRujukan">
									<option value="Tidak" selected>Tidak</option>
									<option value="Ya">Ya</option>
								</select>
							</div>							
						</div>

						<div class="form-group" id="tujuanRujuk">
							<label class="control-label col-md-3" >Tujuan Rujukan
							</label>
							<div class="col-md-2">			
								<select class="form-control select" name="tujuanRujuk" id="tujuanRujuk">
									<option value="" selected>Pilih</option>
									<?php  
										foreach ($dept_rujukan as $value) {
											echo '<option value="'.$value['dept_id'].'">'.$value['nama_dept'].'</option>';
										}
									?>
								</select>
							</div>							
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3" >Dokter Penolong
							</label>
							<div class="col-md-3">
								<input type="text" style="cursor:pointer;background-color:white"  id="dokterpenolongbersalin" class="form-control" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" readonly>
								<input type="hidden" id="id_dokterpenolongbersalin">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Asisten Penolong
							</label>
							<div class="col-md-3">
								<input type="text" style="cursor:pointer;background-color:white" id="penolongbersalin" class="form-control" placeholder="Search Asisten" data-toggle="modal" data-target="#searchAsisten" readonly>
								<input type="hidden" id="id_penolongbersalin">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Keterangan Kegiatan
							</label>
							<div class="col-md-4">
								<textarea class="form-control" id="ketKegiatan"></textarea>
							</div>							
						</div>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<input type="hidden" id="v_id_bersalin" value="<?php echo($pasien['visit_id']) ?>">
							<input type="hidden" id="ri_id_bersalin" value="<?php echo($pasien['ri_id']) ?>">
							<button type="submit" class="btn btn-success">Tambah </button></a> 
						</span>
					</div>
					<br>
				</form>
				<br>
            </div>        

            <div class="dropdown" id="btnBawahMintaBarang">
            	<div id="titleInformasi">Riwayat Kegiatan Bersalin</div>
            	<div class="btnBawah" ><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div> 
            <div class="tabelinformasi" id="infoMintaBarang">
				<div class="portlet box red">
				<?php echo "<input type='hidden' id='jml_kegbersalin' value='".count($riwayat_kegiatanbers)."'>"; ?>
					<div class="portlet-body" style="margin: 25px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabelhistorybersalin">
							<thead>
								<tr class="info" >
									<th style="width:20px"> No.</th>
									<th > Jenis Kegiatan </th>
									<th > Rujukan </th>
									<th > Status </th>
									<th > Dirujuk </th>
									<th > Dokter Penolong</th>
									<th > Asisten</th>
									<th > Tanggal Pelaksanaan </th>
									<th width="30"> Action</th>
								</tr>
							</thead>
							<tbody>
								<?php  
									if (isset($riwayat_kegiatanbers)) {
										$i = 0;
										foreach ($riwayat_kegiatanbers as $value) {
											$tgl = DateTime::createFromFormat('Y-m-d H:i:s', $value['waktu']);
											echo '<tr>	
													<td>'.(++$i).'</td>
													<td>'.$value['jenis_kegiatan'].'</td>
													<td>'.$value['rujukan_dari'].'</td>
													<td>'.$value['status'].'</td>
													<td>'.$value['dirujuk_ke'].'</td>
													<td>'.$value['dokter'].'</td>
													<td>'.$value['asisten'].'</td>
													<td>'.$tgl->format('d F Y H:i:s').'</td>
													<td style="text-align:center" >
														<a href="#" class="hapuskegiatanbersalin"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
														<input type="hidden" class="bersalin_id" value="'.$value['bersalin_id'].'">
													</td>			
												</tr>';
										}
									}
								?>
							</tbody>
						</table>
					</div>
					
				</div>            	
            </div>  
            <br>
		</div>

        <div class="tab-pane" id="resume">
        	<div class="dropdown">
	            <div id="titleInformasi">Resume Medis <span style="color:red; margin-left:30px;font-style:italic;">WAJIB DIISI!</span> </div>
	        </div>

            <div id="infoResumePulang">
            	<form class="form-horizontal" role="form" method="post" id="submitresume">
            		<div class="informasi" >
	            		<div class="form-group">
							<label class="control-label col-md-3">Waktu Keluar <span class="required">* </span></label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="res_date" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
						</div>	
						
						<div class="form-group">
							<label class="control-label col-md-3">Diagnosa Masuk <span class="required">* </span></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian resume6" id="kode_utama_masuk" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian resume6" id="res_utama_masuk" placeholder="Nama Diagnosa" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Dokter Pengirim</label>
							<div class="col-md-3">
								<input type="text" class="form-control" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" id="namadokter_resume1" readonly="" style="cursor:pointer;background-color:white">
								<input type="hidden" id="iddokter_resume1">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Dokter Penanggung Jawab</label>
							<div class="col-md-3">
								<input type="text" class="form-control" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" id="namadokter_resume2" readonly="" style="cursor:pointer;background-color:white">
								<input type="hidden" id="iddokter_resume2">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Anamnesa & Pemeriksaan Fisik</label>
							<div class="col-md-4">
								<textarea class="form-control isian" id="res_anamnesa" name="anamnesa" placeholder="Anamnesa"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Diagnosa Akhir <span class="required">* </span></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian resume1" id="res_kode_utama" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian resume1" id="res_diag_utama" placeholder="Nama Diagnosa" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Diagnosa Sekunder</label>
							<div class="col-md-1">
								<input type="text" class="form-control resume2" style="cursor:pointer;background-color:white" data-toggle="modal" data-target="#searchDiagnosa" id="res_kode_sek1" placeholder="Kode" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control resume2" style="cursor:pointer;background-color:white" data-toggle="modal" data-target="#searchDiagnosa" id="res_sek1" placeholder=" Diagnosa" readonly>
							</div>
							<label class="control-label">1</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-1">
								<input type="text" class="form-control resume3" style="cursor:pointer;background-color:white" data-toggle="modal" data-target="#searchDiagnosa" id="res_kode_sek2" placeholder="Kode" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control resume3" style="cursor:pointer;background-color:white" data-toggle="modal" data-target="#searchDiagnosa" id="res_sek2" placeholder=" Diagnosa" readonly>
							</div>
							<label class="control-label">2</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-1">
								<input type="text" class="form-control resume4" style="cursor:pointer;background-color:white" data-toggle="modal" data-target="#searchDiagnosa" id="res_kode_sek3" placeholder="Kode" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control resume4" style="cursor:pointer;background-color:white" data-toggle="modal" data-target="#searchDiagnosa" id="res_sek3" placeholder=" Diagnosa" readonly>
							</div>
							<label class="control-label">3</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-1">
								<input type="text" class="form-control resume5" style="cursor:pointer;background-color:white" data-toggle="modal" data-target="#searchDiagnosa" id="res_kode_sek4" placeholder="Kode" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control resume5" style="cursor:pointer;background-color:white" data-toggle="modal" data-target="#searchDiagnosa" id="res_sek4" placeholder=" Diagnosa" readonly>
							</div>
							<label class="control-label">4</label>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Detail Diagnosa</label>
							<div class="col-md-4">
								<textarea class="form-control" id="detailDiagnosa" name="detailDiagnosa" placeholder="Detail Diagnosa"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Alasan Keluar<span class="required">* </span>
							</label>
							<div class="col-md-4">			
								<select class="form-control select" name="alasanKeluarPasien" id="alasanKeluarPasien">
									<option value="" selected>Pilih</option>									
									<option value="Pasien Dipulangkan">Pasien Dipulangkan</option>									
									<option value="Atas Permintaan Sendiri">Atas Permintaan Sendiri</option>
									<option value="Rujuk Rumah Sakit Lain"  >Rujuk ke Rumah Sakit Lain</option>
									<option value="Pasien Meninggal" >Pasien Meninggal</option>
								</select>		
							</div>							
						</div>	
						
						<div class="form-group" id="alasanPlg">
							<label class="control-label col-md-3" >Alasan Pulang
							</label>
							<div class="col-md-4">			
								<textarea class="form-control" rows="3" id="alasanPulang"></textarea>
						 	</div>
						</div>
						
						<div class="form-group" id="isiRujuk">
							<label class="control-label col-md-3" >Isian Rumah Sakit Rujukan<span class="required">* </span>
							</label>
							<div class="col-md-4">			
								<input type="text" class="form-control" id="rumasakitlain" name="isianRSRujuk" placeholder="Rumah Sakit Rujukan">
						 	</div>
						</div>				
						<div class="form-group" id="detPasienMeninggal">
							<label class="control-label col-md-3" >Detail Pasien Meninggal<span class="required">* </span>
							</label>
							<div class="col-md-4">			
								<select class="form-control select" name="detPasDie" id="res_dmeninggal">
									<option value="sebelum dirawat" selected>Meninggal sebelum dirawat</option>
									<option value="sesudah dirawat > 48">Meninggal sesudah dirawat > 48 jam</option>
									<option value="sesudah dirawat < 48">Meninggal sesudah dirawat < 48 jam</option>
								</select>
						 	</div>							
						</div>

						<div class="form-group" id="pasienMeninggal">
							<label class="control-label col-md-3">Waktu Pasien Meninggal</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="res_datemeninggal" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii:ss" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i:s");?>">
								</div>
							</div>
						</div>
						
						<div class="form-group" id="ketMati">
							<label class="control-label col-md-3"> Keterangan Kematian
							</label>
							<div class="col-md-4">			
							<textarea class="form-control" rows="3" id="res_ketmeninggal"></textarea>
						 	</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Golongan Sebab Penyakit Luar</label>
							<div class="col-md-4">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian" id="res_sebab" placeholder="Golongan Sebab Penyakit" data-toggle="modal" data-target="#searchGolongan" readonly>
								<input type="hidden" id="res_idsebab">

							</div>
						</div>
						<div class="form-group">
							<div class="form-inline">
								<label class="control-label col-md-3">Jenis Kasus <span class="required">* </span></label>
								<div class="col-md-4">
									<div class="radio-list">
										<label>
											<input type="radio"  name="res_jk" id="klama" checked="checked" value="0"/><span style="margin-left:15px;font-size:10pt;font-width:normal">Kasus Lama </span> 
										</label>
										<label style="margin-left:40px;">
											<input type="radio"  name="res_jk" id="kbaru" value="1"/><span style="margin-left:15px;font-size:10pt;font-width:normal">Kasus Baru </span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button class="btn btn-danger">BATAL</button> &nbsp;
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button>
							<input type="hidden" id="v_id_resume" value="<?php echo($pasien['visit_id']) ?>">
							<input type="hidden" id="ri_id_resume" value="<?php echo($pasien['ri_id']) ?>"> 
						</span>
					</div>
					<br>	
				</form>	
				<br><br>
            </div>
            <br>
        </div>

		<div class="modal fade" id="searchDiagnosa" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Diagnosa</h3>
        			</div>
        			<div class="modal-body">
        				<form class="form-horizontal" role="form" method="post" id="search_diagnosa">
							<div class="form-group">	
								<div class="col-md-5">
									<input type="text" class="form-control" name="katakunci" id="katakunci_diag" placeholder="Kata kunci"/>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-info">Cari</button>
								</div>	
							</div>	
						</form>
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" style="table-layout:fixed" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th width="25%;">Kode Diagnosa</th>
										<th>Keterangan</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_diagnosa">
									<tr>
										<td style="text-align:center;" colspan="3">Cari Diagnosa</td>
									</tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="searchDokter" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Dokter</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">	
							<form method="post" id="form_dokter"><div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="inputDokter" placeholder="Nama dokter"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div></form>	
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th>Nama Dokter</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_dokter">
									<tr>
										<td style="text-align:center;" colspan="2">Cari dokter</td>
									</tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="searchPerawat" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Perawat</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">	
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="katakunciperawat" placeholder="Nama Perawat"/>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-info">Cari</button>
							</div>	
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelperawat">
								<thead>
									<tr class="info">
										<th>Nama Perawat</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_perawat">
									<tr>										
										<td style="text-align:center; cursor:pointer;" colspan="2">Cari perawat</td>
									</tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="riwayatpenangananigd" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form class="form-horizontal" role="form" method="POST" id="riwayatpenangananigd">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
		   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Penanganan IGD</h3>
		   			</div>
					<div class="modal-body" style="padding-left:80px;">

		   				<div class="form-group">
							<label class="control-label col-md-4">Waktu Tindakan</label>
							<div class="col-md-5">	
								<input type="text" id="waktutindakanigd" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
							</div>
	        			</div>	
	        			<div class="form-group">
							<label class="control-label col-md-4">Anamnesa</label>
							<div class="col-md-7">
								<textarea class="form-control" id="anamnesaigd" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
							</div>
						</div>

						<fieldset class="fsStyle">
							<legend>
				                Tanda Vital
							</legend>
							<div class="form-group">
								<label class="control-label col-md-4" >Tekanan Darah</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="tekanandarahigd" name="takanandarah" placeholder="Tekanan Darah" readonly>
								</div>
								<label class="control-label col-md-2">mmHg</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Temperatur</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="temperaturigd" name="temperatur" placeholder="Temperatur" readonly>
								</div>
								<label class="control-label col-md-2">&deg;C</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Nadi</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="nadiigd" name="nadiigd" placeholder="Nadi" readonly>
								</div>
								<label class="control-label col-md-2">x/menit</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Pernapasan</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="pernapasanigd" name="pernapasan" placeholder="Pernapasan" readonly>
								</div>
								<label class="control-label col-md-2">x/menit</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4" >Berat Badan</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="beratigd" name="berat" placeholder="Berat Badan" readonly>
								</div>
								<label class="control-label col-md-2">Kg</label>
							</div>
				  		</fieldset>

				  		<fieldset class="fsStyle">
							<legend>
				                Pemeriksaan Fisik
							</legend>
							<div class="form-group">
								<label class="control-label col-md-4">Kepala & Leher</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="kepalaleherigd" name="kepalaleher" placeholder="Hasil Pemeriksaan" >
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Thorax & ABD</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="thoraxigd" name="thorax" placeholder="Hasil Pemeriksaan" >
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Extremitas</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="extremitasigd" name="extremitas" placeholder="Hasil Pemeriksaan" >
								</div>
							</div>
						</fieldset>

				  		<fieldset class="fsStyle">
							<legend>
				                Diagnosa & Terapi
							</legend>
							<div class="form-group">
								<label class="control-label col-md-4" >Dokter Pemeriksa</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="dokterigd" placeholder="Search Dokter" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Diagnosa Utama</label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_utamaigd" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="diagutamaigd" placeholder=" Diagnosa" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Diagnosa Sekunder</label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_sek1igd" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="igd1igd" placeholder=" Diagnosa" readonly>
								</div>
								<label class="control-label">1</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4"></label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_sek2igd" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="igd2igd" placeholder=" Diagnosa" readonly>
								</div>
								<label class="control-label">2</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4"></label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_sek3igd" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="igd3igd" placeholder=" Diagnosa" readonly>
								</div>
								<label class="control-label">3</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4"></label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="kode_sek4igd" placeholder="Kode" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="igd4igd" placeholder=" Diagnosa" readonly>
								</div>
								<label class="control-label">4</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4" >Detail Diagnosa</label>
								<div class="col-md-7">
									<textarea class="form-control" id="detailDiagnosaigd" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4" >Terapi</label>
								<div class="col-md-7">
									<textarea class="form-control" id="terapiigd" name="terapi" placeholder="Terapi" readonly></textarea>
								</div>
							</div>
				  		</fieldset>
		        	</div>
	        		
	        		<div class="modal-footer">
	        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				    </div>
				</div>
			</div>
		</form>
	</div>
		<div class="modal fade" id="searchAsisten" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Asisten </h3>
        			</div>
        			<div class="modal-body">
	        			<div class="form-group">
	        				<form class="form-horizontal" role="form" method="post" id="formcariasisten">
								<div class="form-group">	
									<div class="col-md-5" style="margin-left:35px;">
										<input type="text" class="form-control" name="katakunci" id="keyasisten" placeholder="Nama Dokter"/>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-info">Cari</button>
									</div>
									<br><br>	
								</div>	
							</form>	
							<div style="margin-left:10px; margin-right:10px;"><hr></div>
							<div class="portlet-body" style="margin: 0px 30px 0px 20px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabledokter">
									<thead>
										<tr class="info">
											<th>Nama Asisten</th>
											<th width="10%">Pilih</th>
										</tr>
									</thead>
									<tbody id="tbodyasisten">
										<tr>
											<td style="text-align:center;" colspan="2">Cari data asisten</td>
										</tr>
									</tbody>
								</table>												
							</div>
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="riwayat_over" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="riwkondok">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Konsultasi Dokter</h3>
			   			</div>
						<div class="modal-body" style="padding-left:80px;">

			   				<div class="form-group">
								<label class="control-label col-md-4">Waktu Tindakan</label>
								<div class="col-md-5">	
									<input type="text" id="detail_waktu" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
								</div>
		        			</div>	
		        			<div class="form-group">
								<label class="control-label col-md-4">Anamnesa</label>
								<div class="col-md-7">
									<textarea class="form-control" id="detail_anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
								</div>
							</div>

							<fieldset class="fsStyle">
								<legend>
					                Tanda Vital
								</legend>
								<div class="form-group">
									<label class="control-label col-md-4" >Tekanan Darah</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="detail_tekanan" name="takanandarah" placeholder="Tekanan Darah" readonly>
									</div>
									<label class="control-label col-md-2">mmHg</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Temperatur</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="detail_temperatur" name="temperatur" placeholder="Temperatur" readonly>
									</div>
									<label class="control-label col-md-2">&deg;C</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Nadi</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="detail_nadi" name="nadi" placeholder="Nadi" readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Pernapasan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="detail_pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Berat Badan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="detail_berat" name="berat" placeholder="Berat Badan" readonly>
									</div>
									<label class="control-label col-md-2">Kg</label>
								</div>
					  		</fieldset>

					  		<fieldset class="fsStyle">
								<legend>
					                Diagnosa & Terapi
								</legend>
								<div class="form-group">
									<label class="control-label col-md-4" >Dokter Pemeriksa</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="detail_dokter" placeholder="Search Dokter" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Utama</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="detail_kutama" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" id="detail_dutama" placeholder="Keterangan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Sekunder</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="detail_ksek1" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" id="detail_dsek1" placeholder="Keterangan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" > </label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="detail_ksek2" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" id="detail_dsek2" placeholder="Keterangan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" > </label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="detail_ksek3" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" id="detail_dsek3" placeholder="Keterangan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" > </label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="detail_ksek4" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" id="detail_dsek4" placeholder="Keterangan" readonly>
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-4" >Detail Diagnosa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="detail_detail" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Terapi</label>
									<div class="col-md-7">
										<textarea class="form-control" id="detail_terapi" name="terapi" placeholder="Terapi" readonly></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Alergi</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="detail_alergi" name="alergi" placeholder="Alergi" readonly>
									</div>
								</div>
					  		</fieldset>
			        	</div>
		        		
		        		<div class="modal-footer">
		        			<input type="hidden" id="visit_id_riwayat" value="<?php echo $this->session->userdata('visit_id'); ?>">
		 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					    </div>
					</div>
				</div>
			</form>
		</div>
		<div class="modal fade" id="riwperawatan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="riwperawatan">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Perawatan</h3>
			   			</div>
						<div class="modal-body" style="padding-left:80px;">

			   				<div class="form-group">
								<label class="control-label col-md-4">Waktu Tindakan</label>
								<div class="col-md-5">	
									<input type="text" class="form-control" readonly id="waktutindakanrawat" />
								</div>
		        			</div>	

		        			<div class="form-group">
								<label class="control-label col-md-4">Dokter Visit</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="dokterrawat" name="dokterv" readonly></textarea>
								</div>
							</div>

		        			<div class="form-group">
								<label class="control-label col-md-4">Anamnesa</label>
								<div class="col-md-6">
									<textarea class="form-control" id="anamnesarawat" name="anamnesa" readonly></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" >Diagnosa Utama</label>
								<div class="col-md-2">
									<input type="text" class="form-control" id="kode_utamarawat" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="diagnosautamarawat"  readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" >Diagnosa Sekunder</label>
								<div class="col-md-2">
									<input type="text" class="form-control" id="kode_sek1rawat"  readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="sek1rawat"  readonly>
								</div>
								<label class="control-label col-md-1" >1</label>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" ></label>
								<div class="col-md-2">
									<input type="text" class="form-control" id="kode_sek2rawat" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="sek2rawat"  readonly>
								</div>
								<label class="control-label col-md-1" >2</label>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" ></label>
								<div class="col-md-2">
									<input type="text" class="form-control" id="kode_sek3rawat" readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="sek3rawat" readonly>
								</div>
								<label class="control-label col-md-1" >3</label>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" ></label>
								<div class="col-md-2">
									<input type="text" class="form-control" id="kode_sek4rawat"  readonly>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" id="sek4rawat" readonly>
								</div>
								<label class="control-label col-md-1" >4</label>
							</div>


							<div class="form-group">
								<label class="control-label col-md-4">Perkembangan Penyakit</label>
								<div class="col-md-6">
									<textarea class="form-control" id="perkembanganrawat" name="perkembangan"  readonly></textarea>
								</div>
							</div>

							<fieldset class="fsStyle">
								<legend>
					                Tanda Vital
								</legend>
								<div class="form-group">
									<label class="control-label col-md-4" >Tekanan Darah</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="tekanandarahrawat" name="takanandarah"  readonly>
									</div>
									<label class="control-label col-md-2">mmHg</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Temperatur</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="temperaturrawat" name="temperatur"  readonly>
									</div>
									<label class="control-label col-md-2">&deg;C</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Nadi</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="nadirawat" name="nadi"  readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Pernapasan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="pernapasanrawat" name="pernapasan" readonly>
									</div>
									<label class="control-label col-md-2">x/menit</label>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Berat Badan</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="beratrawat" name="berat"  readonly>
									</div>
									<label class="control-label col-md-2">Kg</label>
								</div>
					  		</fieldset>

			        	</div>
		        		
		        		<div class="modal-footer">
		        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
		 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					    </div>
					</div>
				</div>
			</form>
		</div>
		<div class="modal fade" id="searchGolongan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Golongan Sebab Penyakit Luar</h3>
        			</div>
        			<div class="modal-body">
        				<form method="POST" id="submit_sebab">
							<div class="form-group">	
								<div class="col-md-5">
									<input type="text" class="form-control" name="katakunci" id="sebab_katakunci" placeholder="Kata kunci"/>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-info">Cari</button>
								</div>	
							</div>
						</form>
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<td width="30%;">No Daftar Terperinci</td>
										<td>Golongan Sebab Penyakit Luar</td>
										<td width="10%">Pilih</td>
									</tr>
								</thead>
								<tbody id="tbody_sebab">
									<tr>
										<td colspan="3" style="text-align:center;">Cari Data Golongan Sebab</td>
									</tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>
</div>

																						
<script type="text/javascript">
	$(document).ready( function(){

		$("#btkonsudokter").click(function(){
			$("#ibtkonsudokter").slideToggle();
		});

		$("#ibturaianklinik").hide();
		$("#bturaianklinik").click(function(){
			$("#ibturaianklinik").slideToggle();
		});

		$("#infoMintaBarang").hide();
		$("#ibtriwpermintaanmakan").hide();
		$("#btriwpermintaanmakan").click(function(){
			$("#ibtriwpermintaanmakan").slideToggle();
		});

		$("#ibwuraianigd").hide();
		$("#bwuraianigd").click(function(){
			$("#ibwuraianigd").slideToggle();
		});

		$("#inrovih").hide();
		$("#btpermintaanmakan").click(function(){
			$("#ibtpermintaanmakan").slideToggle();
		});

		$("#ibwasuhan").hide();
		$("#bwasuhan").click(function(){
			$("#ibwasuhan").slideToggle();
		});

		$("#btkunjungandokter").click(function(){
			$("#ibtkunjungandokter").slideToggle();
		});


		$("#ibturtin").hide();
		$("#bturtin").click(function(){
			$("#ibturtin").slideToggle();
		});

		$("#btpindah").click(function(){
			$("#ibtpindah").slideToggle();
		});

		$("#ibtriwpindah").hide();
		$("#btriwpindah").click(function(){
			$("#ibtriwpindah").slideToggle();
		});

	});

</script>
											
<script type="text/javascript">
	
	$(document).ready(function(){


		$("#dasbod").val('Identitas Pasien');
		$('.cl').on('click',function (e) {
			e.preventDefault();
			var a = $(this).text();
			$('#dasbod').val(a);
		})



	});
</script>