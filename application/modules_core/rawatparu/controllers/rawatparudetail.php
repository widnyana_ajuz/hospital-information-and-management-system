<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Rawatparudetail extends Operator_base {
	protected $ses;
	protected $dept_id;
	function __construct() {
		parent:: __construct();

		$this->load->model('logistik/m_gudangbarang');
		$this->load->model('bersalin/m_bersalin');
		$this->dept_id = $this->m_gudangbarang->get_dept_id('PARU - PARU')['dept_id'];
	}

	public function index($page = 0)
	{
		redirect('rawatparu/homerawatparu');
	}

	public function daftar($ri_id='',$visit_id='')
	{
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$data['page_title'] = 'Detail PARU - PARU';
		$this->session->set_userdata($data);

		$data['content'] = 'rawatparudetail/list';

		$data['javascript'] = "rawatparudetail/j_list";	

		//load semua data pasien dengan no rm tertentu
		$pasien = $this->m_bersalin->get_pasien($visit_id, $ri_id);	
		if (!$pasien) {
			redirect('rawatparu/homerawatparu');
		}	
		$data['pasien'] = $pasien;

		$data['overview_history'] = $this->m_bersalin->get_overview_history($visit_id);
		$data['overviewigd_history'] = $this->m_bersalin->get_overviewigd_history($visit_id);
		//$data['overviewhamil_history'] = $this->m_bersalin->get_overviewhamil_history($visit_id);
		$data['overview_kunjungandokter'] = $this->m_bersalin->getinfo_kunjungan_dokter($visit_id);
		$data['overview_asuhan'] = $this->m_bersalin->get_asuhan_dokter($visit_id);
		$data['dept_rujukan'] = $this->m_bersalin->get_dept_rujukan();
		//$data['riwayat_kegiatanbers'] = $this->m_bersalin->get_kegiatan_bersalin($visit_id);
		$data['visit_resep'] = $this->m_bersalin->get_visit_resep($visit_id);
		$data['order_operasi'] = $this->m_bersalin->get_order_operasi($visit_id);
		$data['riwayat_klinik'] = $this->m_bersalin->get_riwayat_klinik($pasien['rm_id']);
		$data['riwayat_igd'] = $this->m_bersalin->get_riwayat_igd($pasien['rm_id']);
		$data['riwayat_perawatan'] = $this->m_bersalin->get_riwayat_perawatan($pasien['rm_id']);
		$data['gizi'] = $this->m_bersalin->get_visit_gizi($visit_id);
		$data['tipediet'] = $this->m_bersalin->get_tipe_diet();
		$data['permintaan_makan'] = $this->m_bersalin->get_permintaan_makan($visit_id);
		$dept= $this->m_gudangbarang->get_dept_id('IGD')['dept_id'];
		$data['visit_care_igd'] = $this->m_bersalin->get_visit_care_unit($visit_id, $dept);
		$data['visit_care_klinik'] = $this->m_bersalin->get_visit_care_klinik($visit_id);
		$data['visit_care_unit'] = $this->m_bersalin->get_visit_care_unit($visit_id, $this->dept_id);
		$data['penunjang'] = $this->m_bersalin->get_unit_penunjang();
		$data['visit_penunjang'] = $this->m_bersalin->get_visit_penunjang($pasien['rm_id']);
		$data['dept_id'] = $this->dept_id;
		$this->load->view('base/operator/template', $data);
	}

	public function save_order_operasi(){
		foreach ($_POST as $value) {
			$insert = $value;
		}

		$insert['dept_id'] = $this->dept_id;

		$visit_id = $insert['visit_id'];
		$id = $this->m_bersalin->get_last_order_operasi($visit_id);

		if($id){
			$vir = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vir) == "1") {
				$vir = '0'. $vir;
			}
			$insert['order_operasi_id'] = "OR".$visit_id."".($vir);
		}else{
			$insert['order_operasi_id'] = "OR".$visit_id."01";
		}

		$tujuan = $this->m_bersalin->get_dept_id($insert['dept_tujuan']);
		$insert['dept_tujuan'] = $tujuan['dept_id'];

		$waktu = $this->date_db($insert['waktu_mulai']);
		$insert['waktu_mulai'] = $waktu;

		$hasil = $this->m_bersalin->save_order_operasi($insert);

		$ins = $this->m_bersalin->get_inserted_order_operasi($insert['order_operasi_id']);
		header('Content-Type:application/json');
		echo(json_encode($ins));
	}


	/*penunjang*/
	public function save_penunjang(){
		foreach ($_POST as $value) {
			$insert = $value;
		}

		$tgl = $this->fdate_db($insert['waktu']);
		$insert['waktu'] = $tgl;

		$year_now = date('y');
		$month_now = date('m');
		$date_now = date('d');

		$p_id = $insert['dept_tujuan'].$year_now.$month_now.$date_now;

		$id = $this->m_bersalin->get_last_visit_penunjang($p_id);
		if($id){
			$vid = intval(substr($id['value'], strlen($p_id))) + 1;
			if (strlen($vid) == "1") {
				$vid = '000'. $vid;
			}else if(strlen($vid) == "2"){
				$vid = '00' . $vid;
			}else if (strlen($vid) == "3") {
				$vid = '0' . $vid;
			}
			$insert['penunjang_id'] = $p_id."".($vid);
		}else{
			$insert['penunjang_id'] = $p_id."0001";
		}

		/*$dept_id = $this->m_bersalin->get_dept_id("IGD");
		$insert['dept_asal'] = $dept_id['dept_id'];*/
		$insert['dept_asal'] = $this->dept_id;

		$save = $this->m_bersalin->save_penunjang($insert);

		header("Content-Type: application/json");
		echo json_encode($insert);
	}
	/*akhir penunjang*/

	/*tindakan*/
	public function save_tindakan()
	{
		$this->load->model('igd/m_igddetail');
		foreach ($_POST as $value) 
		{
			$insert = $value;
		}

		$visit_id = $insert['visit_id'];
		
		$id = $this->m_igddetail->get_last_visit_care($visit_id);
		if($id){
			$vid = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vid) == "1") {
				$vid = '000'. $vid;
			}else if(strlen($vid) == "2"){
				$vid = '00' . $vid;
			}else if (strlen($vid) == "3") {
				$vid = '0' . $vid;
			}
			$insert['care_id'] = "CA".$visit_id."".($vid);
		}else{
			$insert['care_id'] = "CA".$visit_id."0001";
		}
		
		$insert['dept_id'] = $this->dept_id;
		$hasil = $this->m_igddetail->save_tindakan($insert);
		
		$ins = $this->m_igddetail->get_inserted_visit_care($insert['care_id']);
		header('Content-Type:application/json');
		echo(json_encode($ins));
	}
	/*akhir tindakan*/

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i:s',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}
}
