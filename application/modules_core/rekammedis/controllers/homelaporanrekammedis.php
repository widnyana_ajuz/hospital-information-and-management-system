<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homelaporanrekammedis extends Operator_base {
	protected $dept_id;
	function __construct() {
		parent:: __construct();
		$this->load->model('m_olahrekammedis');
		$this->load->model('logistik/m_gudangbarang');
		$this->load->model("pasien/m_pendaftaran");
		$this->dept_id = $this->m_gudangbarang->get_dept_id('REKAM MEDIS')['dept_id'];
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['page_title'] = 'Rekam Medis';
		$this->session->set_userdata($data);
		// load template
		$data['content'] = 'laporanrekammedis/home';
		$data['javascript'] = 'laporanrekammedis/j_rekammedis';
		$data['unit'] = $this->m_olahrekammedis->get_unit_rekammedis();
		$data['klinik'] = $this->m_olahrekammedis->get_klinik();
		$data['rawat_inap'] =  $this->m_olahrekammedis->get_ri();
		$this->load->view('base/operator/template', $data);
	}

	public function penyakit_10_besar()
	{
		$unit = $this->input->post('dept_rekam');
		$periode = $this->input->post('start');
		$temp = explode('/', $periode);
		$result = $this->m_olahrekammedis->get_10_besar_penyakit($unit,$temp[0],$temp[1]);
		$data['result'] = $result;
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($unit)['nama_dept'];
		//echo $data['nama_dept'];
		$data['periode'] = $periode;//die;
		$this->load->view('rekammedis/laporanrekammedis/laporan/sepuluh_penyakit',$data);
	}

	public function get_sensus_harian_poli()
	{
		$dept = $this->input->post();
		$start = $this->fdate_db($this->input->post('start'));
		$end = $this->fdate_db($this->input->post('end'));
		$start .= ' 00:00:00';
		$end .= ' 00:00:00';
		$result = $this->m_olahrekammedis->get_sensus_harian_poli($dept['dept_sensus'], $start, $end);

		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept['dept_sensus'])['nama_dept'];
		$data['result'] = $result;
		$data['start'] = $this->fdate_db($this->input->post('start'));
		$data['end'] =$this->fdate_db($this->input->post('end'));
		
		$this->load->view('rekammedis/laporanrekammedis/laporan/sensus_harian_poli',$data);
	}

	public function get_sensus_harian_igd()
	{
		$start = $this->fdate_db($this->input->post('start'));
		$end = $this->fdate_db($this->input->post('end'));
		$start .= ' 00:00:00';
		$end .= ' 00:00:00';
		$result = $this->m_olahrekammedis->get_sensus_harian_igd($start, $end);

		$data['result'] = $result;
		$data['start'] = $this->fdate_db($this->input->post('start'));
		$data['end'] =$this->fdate_db($this->input->post('end'));
		
		$this->load->view('rekammedis/laporanrekammedis/laporan/sensus_harian_igd',$data);
	}

	public function get_rekap_bulanan_igd($value='')
	{
		$filter = $this->input->post('start');
		//echo $filter;die;
		$temp = explode('/', $filter);
		$kunjungan = $this->m_olahrekammedis->get_rekap_kunjungan_by_month_igd($temp[0],$temp[1]);
		$rujukan = $this->m_olahrekammedis->get_rekap_rujukan_by_month_igd($temp[0],$temp[1]);
		$cara_bayar = $this->m_olahrekammedis->get_rekap_cara_bayar_by_month_igd($temp[0],$temp[1]);
		$resume_pulang = $this->m_olahrekammedis->get_rekap_alasan_by_month_igd($temp[0],$temp[1]);
		//print_r($resume_pulang);

		$d =implode('/', $temp);
		$bln = DateTime::createFromFormat('m/Y',$d);
		$data['periode'] = $bln->format('F Y');
		$data['cara_bayar'] = $cara_bayar;
		$data['rujukan'] = $rujukan;
		$data['kunjungan'] = $kunjungan;
		$data['resume_pulang'] = $resume_pulang;

		$this->load->view('rekammedis/laporanrekammedis/laporan/sensus_bulanan_igd',$data);
	}

	public function get_rekap_bulanan_poli($value='')
	{
		$filter = $this->input->post();
		//echo $filter;die;
		$temp = explode('/', $filter['start']);
		$dept_id = $filter['dept_sensus'];
		$kunjungan = $this->m_olahrekammedis->get_rekap_kunjungan_by_month_poli($dept_id,$temp[0],$temp[1]);
		$rujukan = $this->m_olahrekammedis->get_rekap_rujukan_by_month_poli($dept_id,$temp[0],$temp[1]);
		$cara_bayar = $this->m_olahrekammedis->get_rekap_cara_bayar_by_month_poli($dept_id,$temp[0],$temp[1]);
		$resume_pulang = $this->m_olahrekammedis->get_rekap_alasan_by_month_poli($dept_id,$temp[0],$temp[1]);
		//print_r($resume_pulang);

		$d =implode('/', $temp);
		$bln = DateTime::createFromFormat('m/Y',$d);
		$data['periode'] = $bln->format('F Y');
		$data['cara_bayar'] = $cara_bayar;
		$data['rujukan'] = $rujukan;
		$data['kunjungan'] = $kunjungan;
		$data['resume_pulang'] = $resume_pulang;
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];

		$this->load->view('rekammedis/laporanrekammedis/laporan/sensus_bulanan_poli',$data);
	}

	public function get_rekap_tahunan_igd()
	{
		$filter = $this->input->post();
		$thn = $filter['start'];
		$kunjungan = $this->m_olahrekammedis->get_rekap_kunjungan_by_year_igd($thn);
		$pelayanan = $this->m_olahrekammedis->get_rekap_pelayanan_by_year_igd($thn);
		$rujukan = $this->m_olahrekammedis->get_rekap_rujukan_by_year_igd($thn);
		$cara_bayar = $this->m_olahrekammedis->get_rekap_carabayar_by_year_igd($thn);
		$resume_pulang = $this->m_olahrekammedis->get_rekap_resume_pulang_by_year_igd($thn);
		
		$data['tahun'] = $thn;
		$data['cara_bayar'] = $cara_bayar;
		$data['kunjungan'] = $kunjungan;
		$data['pelayanan'] = $pelayanan;
		$data['resume_pulang'] = $resume_pulang;
		$data['rujukan'] = $rujukan;

		$this->load->view('rekammedis/laporanrekammedis/laporan/sensus_tahunan_igd',$data);
	}

	public function get_rekap_tahunan_poli()
	{
		$filter = $this->input->post();
		$thn = $filter['start'];
		$dept_id =  $filter['dept_sensus'];
		$kunjungan = $this->m_olahrekammedis->get_rekap_kunjungan_by_year_poli($dept_id,$thn);
		$rujukan = $this->m_olahrekammedis->get_rekap_rujukan_by_year_poli($dept_id,$thn);
		$cara_bayar = $this->m_olahrekammedis->get_rekap_carabayar_by_year_poli($dept_id,$thn);
		$resume_pulang = $this->m_olahrekammedis->get_rekap_resume_pulang_by_year_poli($dept_id,$thn);
		
		$data['tahun'] = $thn;
		$data['cara_bayar'] = $cara_bayar;
		$data['kunjungan'] = $kunjungan;
		$data['resume_pulang'] = $resume_pulang;
		$data['rujukan'] = $rujukan;
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];

		$this->load->view('rekammedis/laporanrekammedis/laporan/sensus_tahunan_poli',$data);
	}

	public function get_rekap_persebaran()
	{
		$input = $this->input->post();
		$dept_id = $input['dept_rekap'];
		$waktu = explode('/', $input['start']);

		$result = $this->m_olahrekammedis->get_rekap_persebaran($dept_id, $waktu[1], $waktu[0]);
		$d =implode('/', $waktu);
		$bln = DateTime::createFromFormat('m/Y',$d);
		$data['periode'] = $bln->format('F Y');
		$data['result'] = $result;
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];
		//print_r($result);die;
		$this->load->view('rekammedis/laporanrekammedis/laporan/rekapitulasi_per_kecamatan',$data);
	}

	public function get_laporan_bulanan_rawat_inap()
	{
		$input = $this->input->post();
		$dept_id = $input['dept_rekap'];
		$waktu = explode('/', $input['start']);

		$pasien_masuk = $this->m_olahrekammedis->get_pasien_masuk_by_cara_bayar($dept_id, $waktu[1], $waktu[0]);
		$rujukan = $this->m_olahrekammedis->get_pasien_rujukan_by_cara_bayar($dept_id, $waktu[1], $waktu[0]);
		$pulang = $this->m_olahrekammedis->get_pasien_pulang_by_cara_bayar($dept_id, $waktu[1], $waktu[0]);
		$mati = $this->m_olahrekammedis->get_pasien_mati_by_cara_bayar($dept_id, $waktu[1], $waktu[0]);
		$sisa = $this->m_olahrekammedis->get_pasien_sisa_by_cara_bayar($dept_id, $waktu[1], $waktu[0]);

		$d =implode('/', $waktu);
		$bln = DateTime::createFromFormat('m/Y',$d);
		$data['periode'] = $bln->format('F Y');
		$data['pasien_masuk'] = $pasien_masuk;
		$data['rujukan'] = $rujukan;
		$data['pulang'] = $pulang;
		$data['mati'] = $mati;
		$data['sisa'] = $sisa;
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];

		$this->load->view('rekammedis/laporanrekammedis/laporan/laporan_bulanan',$data);
	}

	public function get_sensus_bulanan_ri()
	{
		$data='';
		$this->load->view('rekammedis/laporanrekammedis/laporan/rekapitulasi_harian_ri',$data);
	}

	public function get_laporan_jenis_penyakit()
	{
		$input = $this->input->post();
		$dept_id = $input['dept_rekap'];
		$waktu = explode('/', $input['start']);

		$result = $this->m_olahrekammedis->get_laporan_jenis_penyakit($dept_id, $waktu[1], $waktu[0]);

		$d =implode('/', $waktu);
		$bln = DateTime::createFromFormat('m/Y',$d);
		$data['periode'] = $bln->format('F Y');
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];
		$data['result'] = $result;
		$this->load->view('rekammedis/laporanrekammedis/laporan/laporan_jenis_penyakit',$data);
	}

	public function get_sensus_harian_ri()
	{
		$dept = $this->input->post();
		$start = $this->fdate_db($this->input->post('start'));

		$result = $this->m_olahrekammedis->get_sensus_harian_ri($dept['dept_rekap'], $start);
		$trans_masuk = $this->m_olahrekammedis->get_transfer_masuk_ri($dept['dept_rekap'], $start);
		$trans_keluar = $this->m_olahrekammedis->get_transfer_keluar_ri($dept['dept_rekap'], $start);
		$pulang = $this->m_olahrekammedis->get_pasien_keluar_rs($dept['dept_rekap'], $start);

		$data['start'] = $this->input->post('start');
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept['dept_rekap'])['nama_dept'];
		$data['result'] = $result;
		$data['trans_masuk'] = $trans_masuk;
		$data['trans_keluar'] = $trans_keluar;
		$data['pulang'] = $pulang;
		$this->load->view('rekammedis/laporanrekammedis/laporan/sensus_harian_ri',$data);
	}

	public function get_kegiatan_kebinanan()
	{
		$input = $this->input->post();
		$waktu = $input['start'];

		$result = $this->m_olahrekammedis->get_kegiatan_kebinanan($waktu);
		$data['result'] = $result;
		$this->load->view('rekammedis/laporanrekammedis/laporan/laporan_kegiatan_kebidanan',$data);
	}

	public function get_kegiatan_perinatalogi()
	{
		$input = $this->input->post();
		$waktu = $input['start'];

		$bayi_hidup = $this->m_olahrekammedis->get_bayi_hidup($waktu);
		$sebab_mati = $this->m_olahrekammedis->get_kegiatan_perinatologi($waktu);
		$mati = $this->m_olahrekammedis->get_mati_perinatologi($waktu);
		$data['sebab_mati'] = $sebab_mati;
		$data['bayi_hidup'] = $bayi_hidup;
		$data['mati'] = $mati;
		$this->load->view('rekammedis/laporanrekammedis/laporan/laporan_perinatalogi',$data);
	}

	public function get_register_ri()
	{
		$input = $this->input->post();
		$dept_id = $input['dept_rekap'];
		$awal = $this->fdate_db($input['start']);
		$akhir = $this->fdate_db($input['end']);
		$result = $this->m_olahrekammedis->get_register_rawat_inap($dept_id, $awal, $akhir);
		//echo json_encode($result);die;
		$data['start'] = $input['start'];
		$data['end'] = $input['end'];
		$data['result'] = $result;
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];
		$this->load->view('rekammedis/laporanrekammedis/laporan/register_rawat_inap',$data);
	}

	public function get_ketenagaan()
	{
		$data['periode'] = $this->input->post('start');
		$data['result'] = $this->m_olahrekammedis->get_ketenagaan();

		$this->load->view('rekammedis/laporanrekammedis/laporan/laporan_ketenagaan',$data);
	}


	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i:s',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}
}
