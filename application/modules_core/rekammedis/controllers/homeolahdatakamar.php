<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

//class Homebersalin extends Application_Base {
class Homeolahdatakamar extends Operator_base {
	protected $dept_id;
	function __construct() {
		parent:: __construct();
		$this->load->model('m_olahkamar');
		$this->load->model('m_olahrekammedis');
		$this->load->model('logistik/m_gudangbarang');
		$this->load->model("pasien/m_pendaftaran");
		$this->dept_id = $this->m_gudangbarang->get_dept_id('REKAM MEDIS')['dept_id'];
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['page_title'] = 'Data Kamar';
		$this->session->set_userdata($data);
		// load template
		$now = date('Y-m-d 23:59:59');
		$m_now = date('Y-m');
		$data['kamar_harian'] = $this->m_olahkamar->get_kamar_harian($now);
		//echo json_encode($data['kamar_harian']);die;
		$data['kamar_bulanan'] = $this->m_olahkamar->get_kamar_bulanan($m_now);
		$data['all_unit_inap'] = $this->m_olahkamar->get_all_unit_inap();
		$data['content'] = 'olahdatakamar/home';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}

	public function search_harian()
	{
		$tgl = $_POST['tanggal'];
		$dept = $_POST['unit'];
		$tgl .= ' 23:59:59';
		if ($dept == '') {
			$result = $this->m_olahkamar->get_kamar_harian($tgl);
		}else{
			$result = $this->m_olahkamar->get_search_harian($tgl, $dept);
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_bulanan()
	{
		$bln = $_POST['bulan'];
		$dept = $_POST['unit'];
		if ($dept == '') {
			$result = $this->m_olahkamar->get_kamar_bulanan($bln);
		}else{
			$result = $this->m_olahkamar->get_search_bulanan($bln, $dept);	
		}
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function excel_harian()
	{
		$tgl = $this->fdate_db($this->input->post('start'));;
		$dept =$this->input->post('selectunitharian');
		$tgl .= ' 23:59:59';
		
		if($dept_id == '') {
			$result = $this->m_olahkamar->get_kamar_harian($tgl);
			$data['nama_dept'] = 'SEMUA UNIT';
		}else{
			$result = $this->m_olahkamar->get_search_harian($tgl, $dept);
			$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];	
		}
		$data['periode'] = $this->input->post('start');
		$data['kamar_harian'] =  $result;
		$this->load->view('rekammedis/olahdatakamar/excel_harian',$data);
	}

	public function excel_bulanan()
	{
		$tgl = $this->input->post('start');
		$w = explode('/', $tgl);
		$dept_id = $this->input->post('selectunitbulanan');
		$tgl .= ' 23:59:59';
		
		if($dept_id == '') {
			$result = $this->m_olahkamar->get_kamar_bulanan($tgl);
			$data['nama_dept'] = 'SEMUA UNIT';
		}else{
			$result = $this->m_olahkamar->get_search_bulanan($w[1].'-'.$w[0], $dept_id);
			$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept_id)['nama_dept'];	
		}
		$data['kamar_bulanan'] =  $result;

		$data['periode'] = $this->input->post('start');
		//print_r($data['kamar_bulanan']);die;
		$this->load->view('rekammedis/olahdatakamar/excel_bulanan',$data);
	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i:s',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}

}
