<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

//class Homebersalin extends Application_Base {
class Homeolahdataparamedis extends Operator_base {
	protected $dept_id;
	function __construct() {
		parent:: __construct();
		$this->load->model('m_olahparamedis');
		$this->load->model('logistik/m_gudangbarang');
		$this->load->model("pasien/m_pendaftaran");
		$this->dept_id = $this->m_gudangbarang->get_dept_id('REKAM MEDIS')['dept_id'];
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['page_title'] = 'Data Paramedis';
		$this->session->set_userdata($data);
		// load template
		$data['content'] = 'olahdataparamedis/home';
		$data['dokter'] = $this->m_olahparamedis->get_dokter();
		$data['perawat'] = $this->m_olahparamedis->get_perawat();
		$data['tenagamedis'] = $this->m_olahparamedis->get_tenagamedis();
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}

	public function search_dokter()
	{
		$key = $_POST['katakunci'];
		$result = $this->m_olahparamedis->get_search_dokter($key);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_perawat()
	{
		$key = $_POST['katakunci'];
		$result = $this->m_olahparamedis->get_search_perawat($key);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_tenagamedis()
	{
		$key = $_POST['katakunci'];
		$result = $this->m_olahparamedis->get_search_tenagamedis($key);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function excellsatu($val='')
	{
		$result = $this->m_olahparamedis->get_search_dokter($val);
		$data['dokter'] = $result;
		$data['jenis']= 'DOKTER';
		$this->load->view('rekammedis/olahdataparamedis/excel_dokter', $data);
	}

	public function excelldua($val='')
	{
		$result = $this->m_olahparamedis->get_search_perawat($val);
		$data['dokter'] = $result;
		$data['jenis']= 'PERAWAT';
		$this->load->view('rekammedis/olahdataparamedis/excel_dokter', $data);
	}

	public function excelltiga($val='')
	{
		$result = $this->m_olahparamedis->get_search_tenagamedis($val);
		$data['dokter'] = $result;
		$data['jenis']= 'TENAGA MEDIS';
		$this->load->view('rekammedis/olahdataparamedis/excel_dokter', $data);
	}

}
