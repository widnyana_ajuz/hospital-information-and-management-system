<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

//class Homebersalin extends Application_Base {
class Homeolahdatapasien extends Operator_base {
	protected $dept_id;
	function __construct() {
		parent:: __construct();
		$this->load->model('m_olahpasien');
		$this->load->model('m_olahrekammedis');
		$this->load->model('logistik/m_gudangbarang');
		$this->load->model("pasien/m_pendaftaran");
		$this->load->model('bersalin/m_homebersalin');
		$this->dept_id = $this->m_gudangbarang->get_dept_id('REKAM MEDIS')['dept_id'];
	}

	public function index($page = 0)
	{

		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['page_title'] = 'Data Pasien';
		$this->session->set_userdata($data);
		// load template
		$data['javascript'] = "olahdatapasien/j_pasien";
		$data['content'] = 'olahdatapasien/home';
		/*$res = $this->m_olahpasien->get_rekap_rj();
		echo json_encode($res);die;*/
		$data['allpoli'] = $this->m_olahpasien->get_all_poli();
		$data['allunit'] = $this->m_olahpasien->get_all_unit();
		$data['rekap_rj'] = $this->get_rekap_rj();
		$data['rekap_ri'] = $this->get_rekap_ri();
		//echo json_encode($data['rekap_ri']);die();
		$data['provinsi'] = $this->m_pendaftaran->get_provinsi();
		$data['all_pasienactive'] = $this->m_olahpasien->get_all_pasien_active();
		$data['all_pasieninactive'] = $this->m_olahpasien->get_all_pasien_inactive();
		$data['all_pasienmeninggal'] = $this->m_olahpasien->get_all_pasien_meninggal();
		$data['all_pasienrj'] = $this->m_olahpasien->get_all_pasien_rj();
		$data['all_pasienri'] = $this->m_olahpasien->get_all_pasien_ri();
		$data['in_patient'] = $this->m_olahpasien->in_patient()['jumlah'];
		$data['out_patient'] = $this->m_olahpasien->out_patient()['jumlah'];
		$data['emergency'] = $this->m_olahpasien->emergency_patient()['jumlah'];
		$data['rj_per_cara_bayar'] = $this->get_pasien_rj_per_cara_bayar();
		$data['ri_per_cara_bayar'] = $this->get_pasien_ri_per_cara_bayar();
		$data['sensus_rj'] = $this->get_rekap_rawat_jalan();//print_r($data['sensus_rj']);die();
		$data['sensus_ri'] = $this->get_rekap_rawat_inap();//print_r($data['sensus_ri']);
		$data['pasien_pulang'] = $this->m_olahpasien->pasien_pulang();
		$data['iso_rm'] = $this->m_olahpasien->get_iso_rekam_medis();
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($this->dept_id);
		$data['dept_id'] = $this->dept_id;
		//print_r($data['iso_rm']);die;
		$data['rj_per_kecamatan'] = $this->get_pasien_rj_per_kecamatan();
		$data['ri_per_kecamatan'] = $this->get_pasien_ri_per_kecamatan();
		//print_r($data['rj_per_kecamatan']);
		//die;
		$this->load->view('base/operator/template', $data);
	}

	public function get_rekap_rj()
	{
		//$poli = $this->m_olahpasien->get_all_poli();
		$jlh_pasien = array();
		for ($i=30; $i >= 0; $i--) {
			$date =  date('Y-m-d',strtotime("-".$i." day"));
			$date .= ' 00:00:00';
			$jlh_pasien[$i]['waktu_masuk'] = $date;
			$jlh_pasien[$i]['tgl'] = $this->m_olahpasien->get_jlh_per_poli($date);
		}

		return $jlh_pasien;
		//echo json_encode($jlh_pasien);
	}

	public function get_sensus($value='')
	{
		# code...
	}

	public function get_rekap_ri()
	{
		//$poli = $this->m_olahpasien->get_all_poli();
		$jlh_pasien = array();
		for ($i=30; $i >= 0; $i--) {
			$date =  date('Y-m-d',strtotime("-".$i." day"));
			$jlh_pasien[$i]['waktu_masuk'] = $date;
			$jlh_pasien[$i]['tgl'] = $this->m_olahpasien->get_jlh_per_unit($date);
		}

		return $jlh_pasien;
		//echo json_encode($jlh_pasien);
	}

	public function get_detail_pasien($rm_id)
	{
		$result = $this->m_olahpasien->get_detail_pasien($rm_id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_detail_pasienmeninggal($rm_id)
	{
		$result = $this->m_olahpasien->get_detail_pasienmeninggal($rm_id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function selectProvinsi($prov){
		$kabupaten = $this->m_daftarpasien->get_kabupaten_prov($prov);
		echo"<option value='' selected>Pilih Kabupaten</option>";
		foreach ($kabupaten as $kab) {
			echo "<option value='".$kab['kab_id']."'>". $kab['nama_kab']." </option>";
		}
	}

	public function selectKabupaten($kab){
		$kecamatan = $this->m_daftarpasien->get_kecamatan_kab($kab);
		echo"<option value='' selected>Pilih Kecamatan</option>";
		foreach ($kecamatan as $kec) {
			echo "<option value='".$kec['kec_id']."'>". $kec['nama_kec']." </option>";
		}
	}

	public function selectKecamatan($kec){
		$kelurahan = $this->m_pendaftaran->get_kelurahan_kec($kec);
		echo"<option value='' selected>Pilih Kelurahan</option>";
		foreach ($kelurahan as $kel) {
			echo "<option value='".$kel['kel_id']."'>". $kel['nama_kel']." </option>";
		}
	}

	public function get_kab($id='')
	{
		$result = $this->m_olahpasien->get_kab($id);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_kec($id='')
	{
		$result = $this->m_olahpasien->get_kec($id);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_kel($id='')
	{
		$result = $this->m_olahpasien->get_kel($id);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_umur($tanggal_lahir)
	{
		$datetime1 = new DateTime();
		$datetime2 = new DateTime($tanggal_lahir);
		$interval = $datetime1->diff($datetime2);
		$umur = '';
		if($interval->y > 0)
			$umur .= $interval->y ." tahun ";
		if($interval->m > 0)
			$umur .= $interval->m." bulan ";
		if($interval->d > 0)
			$umur .= $interval->d ." hari";

		header('Content-Type: application/json');
		echo json_encode(array('umur' => $umur));
	}

	public function edit_pasien()
	{
		$insert['nama']=$_POST['nama'];
		$insert['alias']=$_POST['alias'];		
		$insert['tempat_lahir']=$_POST['tempat_lahir'];
		$insert['tanggal_lahir']=$this->date_db($_POST['tanggal_lahir']);
		$insert['jenis_kelamin']=$_POST['jenis_kelamin'];
		$insert['gol_darah']=$_POST['gol_darah'];
		$insert['pekerjaan']=$_POST['pekerjaan'];
		$insert['jenis_id']=$_POST['jenis_id'];
		$insert['no_id']=$_POST['no_id'];
		$insert['pendidikan']=$_POST['pendidikan'];
		$insert['agama']=$_POST['agama'];
		$insert['status_perkawinan']=$_POST['status_kawin'];
		$insert['alamat_skr']=$_POST['alamat_skr'];
		$insert['prov_id_skr']=$_POST['prov_id_skr'];
		$insert['kab_id_skr']=$_POST['kab_id_skr'];
		$insert['kec_id_skr']=$_POST['kec_id_skr'];
		$insert['kel_id_skr']=$_POST['kel_id_skr'];
	 	$insert['alamat_ktp']=$_POST['alamat_ktp'];
	 	$insert['prov_id']=$_POST['prov_id'];
		$insert['kab_id']=$_POST['kab_id'];
		$insert['kec_id']=$_POST['kec_id'];
		$insert['kel_id']=$_POST['kel_id'];
		$insert['no_telp']=$_POST['no_telp'];
		$insert['nama_wali']=$_POST['nama_wali'];
		$insert['hubungan_wali']=$_POST['hubungan_wali'];
		$insert['alamat_wali']=$_POST['alamat_wali'];
		$insert['no_telp_wali']=$_POST['no_telp_wali'];
		$insert['pekerjaan_wali']=$_POST['pekerjaan_wali'];
		$insert['alergi']=$_POST['alergi'];

		$rm_id = $_POST['rm_lama'];

		$result = $this->m_olahpasien->update_info_pasien($insert, $rm_id);
		header('Content-Type: application/json');
		echo json_encode($insert);
	}

	public function inactive_pasien()
	{
		$insert['status_pasien'] = $_POST['status_pasien'];
		if ($insert['status_pasien'] == 'meninggal') {
			$insert['tgl_meninggal'] = $this->date_db($_POST['tgl_meninggal']);
			$insert['sebab'] = $_POST['sebab'];
		}

		$result = $this->m_olahpasien->inactive_pasien($_POST['rm_id'], $insert);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function delete_pasien($rm_id)
	{
		$result = $this->m_olahpasien->delete_pasien($rm_id);
	}

	public function submit_search_active()
	{
		$katakunci = $_POST['katakunci'];

		$result = $this->m_olahpasien->search_active_pasien($katakunci);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function submit_search_inactive()
	{
		$katakunci = $_POST['katakunci'];

		$result = $this->m_olahpasien->search_inactive_pasien($katakunci);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function submit_search_died()
	{
		$katakunci = $_POST['katakunci'];

		$result = $this->m_olahpasien->search_died_pasien($katakunci);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	/*pasien rawat jalan*/
	public function get_riwayat_klinik($rm_id)
	{
		$result = $this->m_olahpasien->get_riwayatklinik($rm_id);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_riwayat_igd($rm_id)
	{
		$result = $this->m_olahpasien->get_riwayatigd($rm_id);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_riwayat_ri($rm_id)
	{
		$result = $this->m_olahpasien->get_riwayatri($rm_id);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_detail_riwayatklinik($id)
	{
		$result = $this->m_olahpasien->get_detail_overview_klinis($id);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_detail_riwayatigd($id)
	{
		$result = $this->m_olahpasien->get_detail_overview_igd($id);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_detail_riwayatperawatan($id)
	{
		$result = $this->m_olahpasien->get_detail_over_perawatan($id);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	//rekap
	public function filter_rekap_rj()
	{
		$s = $_POST['start'];
		$e = $_POST['end'];
		$days = (strtotime($e) - strtotime($s)) / (60 * 60 * 24);
		
		$jlh_pasien = array();
		for ($i=$days; $i >= 0; $i--) {
			$d = strtotime("-".$i." day", strtotime($e));
			$date =  date('Y-m-d',$d);
			$date .= ' 00:00:00';
			$jlh_pasien[$i]['waktu_masuk'] = $date;
			$jlh_pasien[$i]['tgl'] = $this->m_olahpasien->get_jlh_per_poli($date);
		}
		
		$r['j'] = $days;
		$r['d'] = $jlh_pasien;
		header('Content-Type: application/json');
		echo json_encode($r);
	}

	public function excel_rekap_rj()
	{
		$input = $this->input->post();
		$a = explode('/', $input['start']);
		$b = explode('/', $input['end']);
		$s = $a[2] .'-'.$a[1] .'-'. $a[0];
		$e = $b[2] .'-'.$b[1] .'-'. $b[0];
		$days = (strtotime($e) - strtotime($s)) / (60 * 60 * 24);
		
		$jlh_pasien = array();
		for ($i=$days; $i >= 0; $i--) {
			$d = strtotime("-".$i." day", strtotime($e));
			$date =  date('Y-m-d',$d);
			$date .= ' 00:00:00';
			$jlh_pasien[$i]['waktu_masuk'] = $date;
			$jlh_pasien[$i]['tgl'] = $this->m_olahpasien->get_jlh_per_poli($date);
		}
		
		//$r['j'] = $days;
		$r['rekap_rj'] = $jlh_pasien;
		$r['mulai'] = $input['start'];
		$r['akhir'] = $input['end'];
		$r['poli'] = $this->m_olahpasien->get_all_poli();
		$this->load->view('rekammedis/olahdatapasien/excel_rekap_rj',$r);
	}

	public function filter_rekap_ri()
	{
		$s = $_POST['start'];
		$e = $_POST['end'];
		$days = (strtotime($e) - strtotime($s)) / (60 * 60 * 24);
		
		$jlh_pasien = array();
		for ($i=$days; $i >= 0; $i--) {
			$d = strtotime("-".$i." day", strtotime($e));
			$date =  date('Y-m-d',$d);
			$jlh_pasien[$i]['waktu_masuk'] = $date;
			$jlh_pasien[$i]['tgl'] = $this->m_olahpasien->get_jlh_per_unit($date);
		}
		
		$r['j'] = $days;
		$r['d'] = $jlh_pasien;
		header('Content-Type: application/json');
		echo json_encode($r);
	}

	public function excel_rekap_ri()
	{
		$input = $this->input->post();
		$a = explode('/', $input['start']);
		$b = explode('/', $input['end']);
		$s = $a[2] .'-'.$a[1] .'-'. $a[0];
		$e = $b[2] .'-'.$b[1] .'-'. $b[0];
		$days = (strtotime($e) - strtotime($s)) / (60 * 60 * 24);
		
		$jlh_pasien = array();
		for ($i=$days; $i >= 0; $i--) {
			$d = strtotime("-".$i." day", strtotime($e));
			$date =  date('Y-m-d',$d);
			$jlh_pasien[$i]['waktu_masuk'] = $date;
			$jlh_pasien[$i]['tgl'] = $this->m_olahpasien->get_jlh_per_unit($date);
		}
		
		//$r['j'] = $days;
		$r['rekap_ri'] = $jlh_pasien;
		$r['mulai'] = $input['start'];
		$r['akhir'] = $input['end'];
		$r['unit'] = $this->m_olahpasien->get_all_unit();
		$this->load->view('rekammedis/olahdatapasien/excel_rekap_ri',$r);
	}

	public function get_pasien_rj_per_cara_bayar()
	{
		$bulan = date('m');
		$tahun = date('Y');
		$result = $this->m_olahpasien->get_pasien_rj_per_cara_bayar($bulan, $tahun);
		return $result;
	}

	public function get_pasien_rj_per_kecamatan()
	{
		$bulan = date('m');
		$tahun = date('Y');
		$result = $this->m_olahpasien->get_pasien_rj_per_kecamatan($bulan, $tahun);
		return $result;
	}

	public function filter_pasien_rj_per_kecamatan()
	{
		$bulan = $_POST['bulan'];
		$tahun = $_POST['tahun'];
		$result = $this->m_olahpasien->get_pasien_rj_per_kecamatan($bulan, $tahun);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function excel_pasien_rj_per_kecamatan()
	{
		$waktu = $this->input->post('start');
		$w = explode('/', $waktu);
		$bulan = $w[0];
		$tahun = $w[1];
		$data['result'] = $this->m_olahpasien->get_pasien_rj_per_kecamatan($bulan, $tahun);
		$data['periode'] = $waktu;

		$this->load->view('rekammedis/olahdatapasien/excel_rj_kecamatan',$data);
	}

	public function filter_pasien_rj_per_cara_bayar()
	{
		$bulan = $_POST['bulan'];
		$tahun = $_POST['tahun'];
		$result = $this->m_olahpasien->get_pasien_rj_per_cara_bayar($bulan, $tahun);
		$object = array();
		if (!empty($result)) {
			foreach ($result as $key) {
				$object[]=$key;
			}
		}
		
		header('Content-Type: application/json');
		echo json_encode($object);
	}

	public function excel_rj_per_carabayar()
	{
		$waktu = $this->input->post('start');
		$w = explode('/', $waktu);
		$bulan = $w[0];
		$tahun = $w[1];
		$data['result'] = $this->m_olahpasien->get_pasien_rj_per_cara_bayar($bulan, $tahun);
		$data['periode'] = $waktu;
		$this->load->view('rekammedis/olahdatapasien/excel_rj_carabayar',$data);
	}

	public function get_pasien_ri_per_kecamatan()
	{
		$bulan = date('m');
		$tahun = date('Y');
		$result = $this->m_olahpasien->get_pasien_ri_per_kecamatan($bulan, $tahun);
		return $result;
	}

	public function filter_pasien_ri_per_kecamatan()
	{
		$bulan = $_POST['bulan'];
		$tahun = $_POST['tahun'];
		$result = $this->m_olahpasien->get_pasien_ri_per_kecamatan($bulan, $tahun);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function excel_pasien_ri_per_kecamatan()
	{
		$waktu = $this->input->post('start');
		$w = explode('/', $waktu);
		$bulan = $w[0];
		$tahun = $w[1];
		$data['result'] = $this->m_olahpasien->get_pasien_ri_per_kecamatan($bulan, $tahun);
		$data['periode'] = $waktu;

		$this->load->view('rekammedis/olahdatapasien/excel_ri_kecamatan',$data);
	}

	public function get_pasien_ri_per_cara_bayar()
	{
		$bulan = date('m');
		$tahun = date('Y');
		$result = $this->m_olahpasien->get_pasien_ri_per_cara_bayar($bulan, $tahun);
		return $result;
	}

	public function excel_ri_per_carabayar()
	{
		$waktu = $this->input->post('start');
		$w = explode('/', $waktu);
		$bulan = $w[0];
		$tahun = $w[1];
		$data['result'] = $this->m_olahpasien->get_pasien_ri_per_cara_bayar($bulan, $tahun);
		$data['periode'] = $waktu;
		$this->load->view('rekammedis/olahdatapasien/excel_ri_carabayar',$data);
	}

	public function filter_pasien_ri_per_cara_bayar()
	{
		$bulan = $_POST['bulan'];
		$tahun = $_POST['tahun'];
		$result = $this->m_olahpasien->get_pasien_ri_per_cara_bayar($bulan, $tahun);
		$object = array();
		if (!empty($result)) {
			foreach ($result as $key) {
				$object[]=$key;
			}
		}
		header('Content-Type: application/json');
		echo json_encode($object);
	}

	public function get_rekap_rawat_jalan()
	{
		$bulan = date('m');
		$tahun = date('Y');
		$result = $this->m_olahpasien->get_rekap_rawat_jalan($bulan, $tahun);
		if (!empty($result)) {
			$result['bulan'] = $bulan ."/". $tahun;
		}
		
		return $result;
	}

	public function excel_rekap_rawat_jalan()
	{
		$waktu = $this->input->post('start');
		$w = explode('/', $waktu);
		$bulan = $w[0];
		$tahun = $w[1];
		$result = $this->m_olahpasien->get_rekap_rawat_jalan($bulan, $tahun);
		$result['bulan'] = $bulan ."/". $tahun;

		$data['result'] = $result;
		$data['periode'] = $waktu;
		$data['allpoli'] = $this->m_olahpasien->get_all_poli();
		
		$this->load->view('rekammedis/olahdatapasien/excel_rekap_rj_all',$data);
	}

	public function filter_rekap_rawat_jalan()
	{
		$waktu = $_POST['tanggal'];
		$w = explode('/', $waktu);
		$bulan = $w[0];
		$tahun = $w[1];
		$result = $this->m_olahpasien->get_rekap_rawat_jalan($bulan, $tahun);
		$result['bulan'] = $bulan ."/". $tahun;
		$i = 0;
		$object = array();
		if (!empty($result)) {
			$result['atas']['bulan'] = $waktu;
			foreach ($result['bawah'] as $key) {
				$result['atas'][$i] = $key;
				$i++;
			}

			$object = $result['atas'];
		}

		header('Content-Type: application/json');
		echo json_encode($object);
	}


	/*akhir pasien rawat jalan*/

	public function get_status_pulang_rj()
	{
		$tanggal = $this->fdate_db($_POST['tanggal']);
		$dept = $_POST['dept_id'];
		$result = $this->m_olahpasien->status_pulang_rj($tanggal, $dept);
		if (!empty($result)) {
			$result['tanggal'] = $_POST['tanggal'];
		}		
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function excel_status_pulang_rj()
	{
		$input= $this->input->post();
		$f = explode('/', $input['start']);
		$tanggal = $f[2].'-'.$f[1].'-'.$f[0];
		$dept = $input['unit_rj'];
		$result = $this->m_olahpasien->status_pulang_rj($tanggal, $dept);
		if ($result) {
			$result['tanggal'] = $input['start'];
		}
		$data['key'] = $result;
		$data['periode'] = $input['start'];
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept['dept_sensus'])['nama_dept'];
		$this->load->view('rekammedis/olahdatapasien/status_pulang_rj',$data);
	}

	public function get_status_pulang_ri()
	{
		$tanggal = $this->fdate_db($_POST['tanggal']);
		$dept = $_POST['dept_id'];
		$result = $this->m_olahpasien->status_pulang_ri($tanggal, $dept);
		if (!empty($result)) {
			$result['tanggal'] = $_POST['tanggal'];
		}
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function excel_status_pulang_ri()
	{
		$input= $this->input->post();
		$f = explode('/', $input['start']);
		$tanggal = $f[2].'-'.$f[1].'-'.$f[0];
		$dept = $input['unit_ri'];
		$result = $this->m_olahpasien->status_pulang_ri($tanggal, $dept);		
		if ($result) {
			$result['tanggal'] = $input['start'];
		}

		$data['key'] = $result;
		$data['periode'] = $input['start'];
		$data['nama_dept'] = $this->m_olahrekammedis->get_dept_nama($dept['dept_sensus'])['nama_dept'];
		$this->load->view('rekammedis/olahdatapasien/status_pulang_ri',$data);	}

	public function get_rekap_rawat_inap()
	{
		$bulan = date('m');
		$tahun = date('Y');
		$result = $this->m_olahpasien->get_rekap_rawat_inap($bulan, $tahun);
		
		return $result;
	}

	public function filter_rekap_rawat_inap()
	{
		$waktu = $_POST['bulan'];
		$w = explode('/', $waktu);
		$bulan = $w[0];
		$tahun = $w[1];
		$result = $this->m_olahpasien->get_rekap_rawat_inap($bulan, $tahun);
		$result['bulan'] = $bulan ."/". $tahun;
		
		$object = array();
		if (!empty($result)) {
			$i = 0;
			foreach ($result['bawah'] as $key) {
				$result['atas']['poli_'.$i] = $key;
				$i++;
			}
			$i = 0;
			foreach ($result['ri'] as $key) {
				$result['atas']['unit_'.$i] = $key;
				$i++;
			}

			$object = $result['atas'];
		}

		header('Content-Type: application/json');
		echo json_encode($object);
	}

	public function excel_rekap_rawat_inap()
	{
		$waktu = $this->input->post('start');
		$w = explode('/', $waktu);
		$bulan = $w[0];
		$tahun = $w[1];
		$result = $this->m_olahpasien->get_rekap_rawat_inap($bulan, $tahun);

		$data['sensus_ri'] = $result;
		$data['periode'] = $waktu;
		$data['allpoli'] = $this->m_olahpasien->get_all_poli();
		$data['allunit'] = $this->m_olahpasien->get_all_unit();
		
		$this->load->view('rekammedis/olahdatapasien/excel_rekap_ri_all',$data);
	}

	/*iso*/
	public function excel_iso_rm()
	{
		$awal = DateTime::createFromFormat('d/m/Y',$this->input->post('start'))->format('Y-m-d');
		$akhir = DateTime::createFromFormat('d/m/Y',$this->input->post('end'))->format('Y-m-d');
		$rm = $this->input->post('noRMISO');

		$result = $this->m_olahpasien->filter_iso_rekam_medis($rm, $awal, $akhir);
		$data['mulai'] = $awal;
		$data['akhir'] = $akhir;
		$data['iso_rm'] = $result;
		$this->load->view('rekammedis/olahdatapasien/excel_iso_rm',$data);
	}

	public function filter_iso_rm()
	{
		$awal = $_POST['awal'];
		$akhir = $_POST['akhir'];
		$rm = $_POST['rm'];

		$result = $this->m_olahpasien->filter_iso_rekam_medis($rm, $awal, $akhir);
		header('Content-Type: application/json');
		echo json_encode($result);
	}



	/*iso akhir*/

	/*logistik*/
	public function input_in_outbarang($value='')
	{
		$insert['barang_detail_id'] = $_POST['barang_detail_id'];
		$tgl = DateTime::createFromFormat('d/m/Y H:i', $_POST['tanggal']);
		$insert['tanggal'] = $tgl->format('Y-m-d H:i');
		$insert['is_out'] = $_POST['is_out'];
		$insert['jumlah'] = $_POST['jumlah'];
		$insert['keterangan'] = $_POST['keterangan'];
		$insert['barang_dept_id'] = $this->dept_id;

		$res = $this->m_gudangbarang->input_in_out($insert);
		if ($res) {
			$ins['barang_detail_id'] = $_POST['barang_detail_id'];
			$ins['dept_id'] = $this->dept_id;
			$ins['stok'] = $_POST['sisa'];
			$ins['tanggal_stok'] = date('Y-m-d H:i:s');
			$ins['keterangan_stok'] = "IN - OUT";

			$res = $this->m_gudangbarang->input_riwayat_out($ins);
			if ($res) {
				$message = "true";
			}else{
				$message = "false";
			}
		}else{
			$message = "false";
		}

		header('Content-Type: application/json');
	 	echo(json_encode($message));
	}

	public function get_detail_inventori($id)
	{
		$res = $this->m_gudangbarang->get_detail_inventori($id, $this->dept_id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function get_barang_gudang()
	{
		$katakunci = $_POST['katakunci'];
		$elny2 = $this->m_homebersalin->get_barang_gudang($katakunci,$this->dept_id,'24');
		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_permintaan_barangunit($value='')
	{
		$this->form_validation->set_rules('no_permintaanbarang', 'nomor permitaan', 'required|trim|xss_clean|is_unique[barang_permintaan.no_permintaanbarang]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaanbarang'] = $_POST['no_permintaanbarang'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$insert['dept_id'] = $this->dept_id;

			$val = $_POST['data'];
			$result = $this->m_homebersalin->insert_permintaanbarang($insert);
			if($result){
				foreach ($val as $key) {
					$ins['barang_id'] = $key[8];
					$ins['barang_stok_id'] = $key[7];
					$ins['jumlah_request'] =  $key[9];
					$ins['barang_permintaan_id'] = $result;

					$elny = $this->m_homebersalin->insert_detail_permintaanbarang($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}


	public function date_db($date){
		$dateTime = date_create($date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function fdate_db($date){
		$dateTime = date_create($date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}

}
