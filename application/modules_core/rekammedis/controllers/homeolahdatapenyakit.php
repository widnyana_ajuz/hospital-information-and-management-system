<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homeolahdatapenyakit extends Operator_base {
	protected $dept_id;
	function __construct() {
		parent:: __construct();
		$this->load->model('m_olahpenyakit');
		$this->load->model('logistik/m_gudangbarang');
		$this->load->model("pasien/m_pendaftaran");
		$this->load->model('bersalin/m_homebersalin');
		$this->dept_id = $this->m_gudangbarang->get_dept_id('REKAM MEDIS')['dept_id'];
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['page_title'] = 'Data Diagnosa';
		$this->session->set_userdata($data);
		// load template
		$data['content'] = 'olahdatapenyakit/home';
		$data['javascript'] = 'olahdatapenyakit/j_penyakit';
		$data['icdx'] = $this->m_olahpenyakit->get_icdx();
		$data['inacbg'] = $this->m_olahpenyakit->get_inacbg();
		$data['icd9cm'] = $this->m_olahpenyakit->get_icd9cm();
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($this->dept_id);
		$data['dept_id'] = $this->dept_id;
		$this->load->view('base/operator/template', $data);
	}

	public function download_template_icdx()
	{
		$this->load->view('rekammedis/olahdatapenyakit/template_icdx');
	}

	public function download_template_icd9cm()
	{
		$this->load->view('rekammedis/olahdatapenyakit/template_icd9cm');
	}

	public function download_template_inacbg()
	{
		$this->load->view('rekammedis/olahdatapenyakit/template_inacbg');
	}

	public function import_icdx()
	{
		$this->load->library('excel_reader');		
		$config['upload_path'] = './temp_upload/';
        $config['allowed_types'] = 'xls|xlsx';
 		ini_set('memory_limit', '-1');
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload())
        {
	        $data = array(
			    'message' => $this->upload->display_errors()
		     );
        }
        else
        {
            $data = array('error' => false);
            $upload_data = $this->upload->data();
 
            $this->load->library('excel_reader');
            $this->excel_reader->setOutputEncoding('UTF-8');
 
            $file =  $upload_data['full_path'];
            $this->excel_reader->read($file);

            error_reporting(E_ALL ^ E_NOTICE);
            ini_set('display_errors', 1);
 
            // Sheet 1
            $data = $this->excel_reader->sheets[0] ;
            $dataexcel = Array();

            for ($i = 6; $i <= $data['numRows']; $i++) {
                if($data['cells'][$i][1] != '') {
		            $dataexcel[$i-1]['diagnosis_id'] = str_replace("\[]()'", "", $data['cells'][$i][1]);
		            $dataexcel[$i-1]['diagnosis_nama'] = str_replace("\[]()'", "", $data['cells'][$i][2]);
		            $dataexcel[$i-1]['diagnosis_local'] = str_replace("\[]()'", "", $data['cells'][$i][3]);
		            $dataexcel[$i-1]['diagnosis_dtd'] = str_replace("\[]()'", "", $data['cells'][$i][4]);
		            $dataexcel[$i-1]['diagnosis_sebab'] = str_replace("\[]()'", "", $data['cells'][$i][5]);
		            $dataexcel[$i-1]['diagnosis_group'] = str_replace("\[]()'", "", $data['cells'][$i][6]);
                }     
            }

            //echo count($dataexcel);die();

            foreach ($dataexcel as $data) {
            	$insert['diagnosis_id'] = $data['diagnosis_id'];
            	$insert['diagnosis_nama'] = $data['diagnosis_nama'];
            	$insert['diagnosis_local'] = $data['diagnosis_local'];
            	$insert['diagnosis_dtd'] = $data['diagnosis_dtd'];
            	$insert['diagnosis_sebab'] = $data['diagnosis_sebab'];
            	$insert['diagnosis_group'] = $data['diagnosis_group'];

            	$res = $this->m_olahpenyakit->insert_icdx($insert);
            	$data = array(
				     'message' => $res
			     );
            }

            delete_files($upload_data['file_path']);
        }
        //updated last imported data

        /*$data = array(
		     'message' => 'Data berhasil di import'
	     );*/
		//print_r($data); die;
		/*$this->session->set_flashdata($data);*/
		redirect('rekammedis/homeolahdatapenyakit');
	}

	public function import_icd_ix_cm()
	{
		$this->load->library('excel_reader');		
		$config['upload_path'] = './temp_upload/';
        $config['allowed_types'] = 'xls|xlsx';
 		ini_set('memory_limit', '-1');
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload())
        {
	        $data = array(
			    'message' => $this->upload->display_errors()
		     );
        }
        else
        {
            $data = array('error' => false);
            $upload_data = $this->upload->data();
 
            $this->load->library('excel_reader');
            $this->excel_reader->setOutputEncoding('UTF-8');
 
            $file =  $upload_data['full_path'];
            $this->excel_reader->read($file);

            error_reporting(E_ALL ^ E_NOTICE);
            ini_set('display_errors', 1);
 
            // Sheet 1
            $data = $this->excel_reader->sheets[0] ;
            $dataexcel = Array();

            for ($i = 6; $i <= $data['numRows']; $i++) {
                if($data['cells'][$i][1] != '') {
		            $dataexcel[$i-1]['kode'] = str_replace("\[]()'", "", $data['cells'][$i][1]);
		            $dataexcel[$i-1]['prosedur'] = str_replace("\[]()'", "", $data['cells'][$i][2]);
		            $dataexcel[$i-1]['keterangan'] = str_replace("\[]()'", "", $data['cells'][$i][3]);
                }     
            }

            //echo count($dataexcel);die();

            foreach ($dataexcel as $data) {
            	$insert['kode'] = $data['kode'];
            	$insert['prosedur'] = $data['prosedur'];
            	$insert['keterangan'] = $data['keterangan'];

            	$res = $this->m_olahpenyakit->insert_icd_ix_cm($insert);
            	$data = array(
				     'message' => $res
			     );
            }

            delete_files($upload_data['file_path']);
        }
        //updated last imported data

        /*$data = array(
		     'message' => 'Data berhasil di import'
	     );*/
		//print_r($data); die;
		/*$this->session->set_flashdata($data);*/
		redirect('rekammedis/homeolahdatapenyakit');
	}

	public function import_ina_cbg_s($value='')
	{
		$this->load->library('excel_reader');		
		$config['upload_path'] = './temp_upload/';
        $config['allowed_types'] = 'xls|xlsx';
 		ini_set('memory_limit', '-1');
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload())
        {
	        $data = array(
			    'message' => $this->upload->display_errors()
		     );
        }
        else
        {
            $data = array('error' => false);
            $upload_data = $this->upload->data();
 
            $this->load->library('excel_reader');
            $this->excel_reader->setOutputEncoding('UTF-8');
 
            $file =  $upload_data['full_path'];
            $this->excel_reader->read($file);

            error_reporting(E_ALL ^ E_NOTICE);
            ini_set('display_errors', 1);
 
            // Sheet 1
            $data = $this->excel_reader->sheets[0] ;
            $dataexcel = Array();

            for ($i = 6; $i <= $data['numRows']; $i++) {
                if($data['cells'][$i][1] != '') {
		            $dataexcel[$i-1]['kode'] = str_replace("\[]()'", "", $data['cells'][$i][1]);
		            $dataexcel[$i-1]['deskripsi'] = str_replace("\[]()'", "", $data['cells'][$i][2]);
		            $dataexcel[$i-1]['tarif_kelas_1'] = str_replace("\[]()'", "", $data['cells'][$i][3]);
		            $dataexcel[$i-1]['tarif_kelas_2'] = str_replace("\[]()'", "", $data['cells'][$i][4]);
		            $dataexcel[$i-1]['tarif_kelas_3'] = str_replace("\[]()'", "", $data['cells'][$i][5]);
		            $dataexcel[$i-1]['tarif_kelas_utama'] = str_replace("\[]()'", "", $data['cells'][$i][6]);
		            $dataexcel[$i-1]['tarif_kelas_vip'] = str_replace("\[]()'", "", $data['cells'][$i][7]);
                }     
            }

            //echo count($dataexcel);die();

            foreach ($dataexcel as $data) {
            	$insert['kode'] = $data['kode'];
            	$insert['deskripsi'] = $data['deskripsi'];
            	$insert['tarif_kelas_1'] = $data['tarif_kelas_1'];
            	$insert['tarif_kelas_2'] = $data['tarif_kelas_2'];
            	$insert['tarif_kelas_3'] = $data['tarif_kelas_3'];
            	$insert['tarif_kelas_utama'] = $data['tarif_kelas_utama'];
            	$insert['tarif_kelas_vip'] = $data['tarif_kelas_vip'];

            	$res = $this->m_olahpenyakit->insert_inacbg_s($insert);
            	$data = array(
				     'message' => $res
			     );
            }

            delete_files($upload_data['file_path']);
        }
        //updated last imported data

        /*$data = array(
		     'message' => 'Data berhasil di import'
	     );*/
		//print_r($data); die;
		/*$this->session->set_flashdata($data);*/
		redirect('rekammedis/homeolahdatapenyakit');
	}

	public function save_inacbg()
	{
		$insert['kode'] = $_POST['kode'];
		$insert['deskripsi'] = $_POST['deskripsi'];
		$insert['tarif_kelas_1'] = $_POST['tarif_kelas_1'];
		$insert['tarif_kelas_2'] = $_POST['tarif_kelas_2'];
		$insert['tarif_kelas_3'] = $_POST['tarif_kelas_3'];
		$insert['tarif_kelas_utama'] = $_POST['tarif_kelas_utama'];
		$insert['tarif_kelas_vip'] = $_POST['tarif_kelas_vip'];

		$id = $this->m_olahpenyakit->insert_new_inacbg($insert);
		$insert['id'] = $id;
		header('Content-Type: application/json');
		echo json_encode($insert);
	}

	public function save_icd9cm()
	{
		$insert['kode'] = $_POST['kode'];
    	$insert['prosedur'] = $_POST['prosedur'];
    	$insert['keterangan'] = $_POST['keterangan'];

		$id = $this->m_olahpenyakit->insert_new_icd9cm($insert);
		$insert['id'] = $id;
		header('Content-Type: application/json');
		echo json_encode($insert);
	}

	public function save_icdx()
	{
		$insert['diagnosis_id'] = $_POST['diagnosis_id'];
    	$insert['diagnosis_nama'] = $_POST['diagnosis_nama'];
    	$insert['diagnosis_local'] = $_POST['diagnosis_local'];
    	$insert['diagnosis_dtd'] = $_POST['diagnosis_dtd'];
    	$insert['diagnosis_sebab'] = $_POST['diagnosis_sebab'];
    	$insert['diagnosis_group'] = $_POST['diagnosis_group'];

		$id = $this->m_olahpenyakit->insert_new_icdx($insert);
		$insert['id'] = $id;
		$inser['deskripsi'] = $_POST['deskripsi'];
		header('Content-Type: application/json');
		echo json_encode($insert);
	}

	public function delete_icdx($id)
	{
		$id = $this->m_olahpenyakit->delete_icdx($id);
		header('Content-Type: application/json');
		echo json_encode($id);
	}

	public function delete_inacbg($id)
	{
		$id = $this->m_olahpenyakit->delete_inacbg($id);
		header('Content-Type: application/json');
		echo json_encode($id);
	}

	public function delete_icd9cm($id)
	{
		$id = $this->m_olahpenyakit->delete_icd9cm($id);
		header('Content-Type: application/json');
		echo json_encode($id);
	}

	public function edit_icdx($value='')
	{
		$insert['diagnosis_id'] = $_POST['diagnosis_id'];
    	$insert['diagnosis_nama'] = $_POST['diagnosis_nama'];
    	$insert['diagnosis_local'] = $_POST['diagnosis_local'];
    	$insert['diagnosis_dtd'] = $_POST['diagnosis_dtd'];
    	$insert['diagnosis_sebab'] = $_POST['diagnosis_sebab'];
    	$insert['diagnosis_group'] = $_POST['diagnosis_group'];
    	$id = $_POST['id'];
		$this->m_olahpenyakit->edit_icdx($id, $insert);
		$insert['deskripsi'] = $_POST['deskripsi'];
		header('Content-Type: application/json');
		echo json_encode($insert);
	}

	public function edit_inacbg($value='')
	{
		$insert['kode'] = $_POST['kode'];
		$insert['deskripsi'] = $_POST['deskripsi'];
		$insert['tarif_kelas_1'] = $_POST['tarif_kelas_1'];
		$insert['tarif_kelas_2'] = $_POST['tarif_kelas_2'];
		$insert['tarif_kelas_3'] = $_POST['tarif_kelas_3'];
		$insert['tarif_kelas_utama'] = $_POST['tarif_kelas_utama'];
		$insert['tarif_kelas_vip'] = $_POST['tarif_kelas_vip'];
		$id = $_POST['id'];
		$this->m_olahpenyakit->edit_inacbg($id, $insert);
		header('Content-Type: application/json');
		echo json_encode($insert);
	}

	public function edit_icd9cm($value='')
	{
		$insert['kode'] = $_POST['kode'];
    	$insert['prosedur'] = $_POST['prosedur'];
    	$insert['keterangan'] = $_POST['keterangan'];
    	$id = $_POST['id'];
		$this->m_olahpenyakit->edit_icd9cm($id, $insert);
		header('Content-Type: application/json');
		echo json_encode($insert);
	}

	public function get_group_icdX()
	{
		$result = $this->m_olahpenyakit->get_group_icdX();
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_icdx()
	{
		$key = $_POST['katakunci'];
		$result = $this->m_olahpenyakit->search_icdx($key);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_icd9cm()
	{
		$key = $_POST['katakunci'];
		$result = $this->m_olahpenyakit->search_icd9cm($key);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_inacbg()
	{
		$key = $_POST['katakunci'];
		$result = $this->m_olahpenyakit->search_inacbg($key);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	/*logistik*/
	public function input_in_outbarang($value='')
	{
		$insert['barang_detail_id'] = $_POST['barang_detail_id'];
		$tgl = DateTime::createFromFormat('d/m/Y H:i', $_POST['tanggal']);
		$insert['tanggal'] = $tgl->format('Y-m-d H:i');
		$insert['is_out'] = $_POST['is_out'];
		$insert['jumlah'] = $_POST['jumlah'];
		$insert['keterangan'] = $_POST['keterangan'];
		$insert['barang_dept_id'] = $this->dept_id;

		$res = $this->m_gudangbarang->input_in_out($insert);
		if ($res) {
			$ins['barang_detail_id'] = $_POST['barang_detail_id'];
			$ins['dept_id'] = $this->dept_id;
			$ins['stok'] = $_POST['sisa'];
			$ins['tanggal_stok'] = date('Y-m-d H:i:s');
			$ins['keterangan_stok'] = "IN - OUT";

			$res = $this->m_gudangbarang->input_riwayat_out($ins);
			if ($res) {
				$message = "true";
			}else{
				$message = "false";
			}
		}else{
			$message = "false";
		}

		header('Content-Type: application/json');
	 	echo(json_encode($message));
	}

	public function get_detail_inventori($id)
	{
		$res = $this->m_gudangbarang->get_detail_inventori($id, $this->dept_id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function get_barang_gudang()
	{
		$katakunci = $_POST['katakunci'];
		$elny2 = $this->m_homebersalin->get_barang_gudang($katakunci,$this->dept_id,'24');
		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}

	public function submit_permintaan_barangunit($value='')
	{
		$this->form_validation->set_rules('no_permintaanbarang', 'nomor permitaan', 'required|trim|xss_clean|is_unique[barang_permintaan.no_permintaanbarang]');
		$this->form_validation->set_message('is_unique', 'Nomor permintaan sudah ada');
		$this->form_validation->set_message('required', 'Data tidak boleh kosong');

		if ($this->form_validation->run() == TRUE) {
			$insert['no_permintaanbarang'] = $_POST['no_permintaanbarang'];
			$tgl = DateTime::createFromFormat('d/m/Y H:i',$_POST['tanggal_request']);
			$insert['tanggal_request'] = $tgl->format('Y-m-d H:i');
			$insert['keterangan_request'] = $_POST['keterangan_request'];
			$insert['petugas_request'] = $this->session->userdata('session_operator')['petugas_id'];
			$insert['is_responded'] = '0';
			$insert['dept_id'] = $this->dept_id;

			$val = $_POST['data'];
			$result = $this->m_homebersalin->insert_permintaanbarang($insert);
			if($result){
				foreach ($val as $key) {
					$ins['barang_id'] = $key[8];
					$ins['barang_stok_id'] = $key[7];
					$ins['jumlah_request'] =  $key[9];
					$ins['barang_permintaan_id'] = $result;

					$elny = $this->m_homebersalin->insert_detail_permintaanbarang($ins);
				}
				$elny2 = array(
					'message'		=> "Data berhasil disimpan",
					'error' => 'n'
				);
			}
		}else{
			$elny2 = array(
				'message'		=> strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			);
		}

		header('Content-Type: application/json');
	 	echo json_encode($elny2);
	}
}
