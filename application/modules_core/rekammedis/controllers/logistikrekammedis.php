<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

//class Homebersalin extends Application_Base {
class Logistikrekammedis extends Operator_base {
	protected $dept_id;
	function __construct() {
		parent:: __construct();
		$this->load->model('m_olahkamar');
		$this->load->model('m_olahrekammedis');
		$this->load->model('logistik/m_gudangbarang');
		$this->load->model("pasien/m_pendaftaran");
		$this->dept_id = $this->m_gudangbarang->get_dept_id('REKAM MEDIS')['dept_id'];
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['page_title'] = 'Data Kamar';
		$this->session->set_userdata($data);
		// load template
		$now = date('Y-m-d 23:59:59');
		$m_now = date('Y-m');

		$data['content'] = 'logistikrm/home';
		$data['javascript'] = 'logistikrm/j_list';
		$data['inventoribarang'] = $this->m_gudangbarang->get_inventori_barang($this->dept_id);
		$data['dept_id'] = $this->dept_id;
		$this->load->view('base/operator/template', $data);
	}

	
	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i:s',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}

}
