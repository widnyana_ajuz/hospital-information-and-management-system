<?php  
	/**
	* 
	*/
	class m_olahkamar extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function get_kamar_harian($now)
		{
			$sql = "SELECT * from master_kamar a left join 
					master_dept b on a.dept_id = b.dept_id order by a.dept_id desc";
			$res = $this->db->query($sql);
			$i = 0;
			$result = array();
			foreach ($res->result_array() as $kamar) {
				$id = $kamar['kamar_id'];
				$subquery = "SELECT count(c.bed_id) as jlh from visit_inap_kamar c 
							left join visit_ri ri on c.ri_id = ri.ri_id
							left join visit v on v.visit_id = ri.visit_id
							where c.kamar_id = $id and (date(c.waktu_masuk)) <= date('$now') 
								and c.waktu_keluar is NULL and v.status_visit NOT LIKE 'CHECKOUT'";
				$jlh = $this->db->query($subquery)->row_array();
				$sub = "SELECT count(c.kamar_id) as jlh_bed from master_bed c where c.kamar_id = $id";
				$jlh_bed = $this->db->query($sub)->row_array();

				$result[$i]['nama_dept'] = $kamar['nama_dept'];
				$result[$i]['nama_kamar'] = $kamar['nama_kamar'];
				$result[$i]['terisi'] = $jlh['jlh'];
				$result[$i]['jlh_bed'] = $jlh_bed['jlh_bed'];
				$i++;
			}

			return $result;
		}

		public function get_kamar_bulanan($m_now)
		{
			$sql = "SELECT * from master_kamar a left join master_dept b on a.dept_id = b.dept_id order by a.dept_id desc";
			$res = $this->db->query($sql);
			$i = 0;
			$result = array();
			foreach ($res->result_array() as $kamar) {
				$id = $kamar['kamar_id'];
				$subquery = "SELECT count(c.bed_id) as jlh from visit_inap_kamar c 
							left join visit_ri ri on c.ri_id = ri.ri_id
							left join visit v on v.visit_id = ri.visit_id
							where c.kamar_id = $id and (substr(c.waktu_masuk,1,7) <= '$m_now' 
								and c.waktu_keluar is NULL) and v.status_visit NOT LIKE 'CHECKOUT'";
				$jlh = $this->db->query($subquery)->row_array();
				$sub = "SELECT count(c.kamar_id) as jlh_bed from master_bed c where c.kamar_id = $id";
				$jlh_bed = $this->db->query($sub)->row_array();

				$result[$i]['nama_dept'] = $kamar['nama_dept'];
				$result[$i]['nama_kamar'] = $kamar['nama_kamar'];
				$result[$i]['terisi'] = $jlh['jlh'];
				$result[$i]['jlh_bed'] = $jlh_bed['jlh_bed'];
				$i++;
			}

			return $result;
		}

		public function get_all_unit_inap()
		{
			$sql = "SELECT * from master_dept where jenis LIKE 'RAWAT INAP'";
			$res = $this->db->query($sql);
			return $res->result_array();
		}

		public function get_search_harian($tgl, $dept)
		{
			$sql = "SELECT * from master_kamar a left join master_dept b on a.dept_id = b.dept_id where a.dept_id = '$dept' order by a.dept_id desc";
			$res = $this->db->query($sql);
			$i = 0;
			$result = array();
			foreach ($res->result_array() as $kamar) {
				$id = $kamar['kamar_id'];
				$subquery = "SELECT count(c.bed_id) as jlh from visit_inap_kamar c 
							left join visit_ri ri on c.ri_id = ri.ri_id
							left join visit v on v.visit_id = ri.visit_id
							where c.kamar_id = $id and (date(c.waktu_masuk) <= date('$tgl') and c.waktu_keluar is NULL)
							and v.status_visit NOT LIKE 'CHECKOUT'";
				$jlh = $this->db->query($subquery)->row_array();
				$sub = "SELECT count(c.kamar_id) as jlh_bed from master_bed c where c.kamar_id = $id";
				$jlh_bed = $this->db->query($sub)->row_array();

				$result[$i]['s'] = $tgl;
				$result[$i]['nama_dept'] = $kamar['nama_dept'];
				$result[$i]['nama_kamar'] = $kamar['nama_kamar'];
				$result[$i]['terisi'] = $jlh['jlh'];
				$result[$i]['jlh_bed'] = $jlh_bed['jlh_bed'];
				$i++;
			}

			return $result;
		}

		public function get_search_bulanan($bln, $dept)
		{
			$sql = "SELECT * from master_kamar a left join master_dept b on a.dept_id = b.dept_id where a.dept_id = '$dept' order by a.dept_id desc";
			$res = $this->db->query($sql);
			$i = 0;
			$result = array();
			foreach ($res->result_array() as $kamar) {
				$id = $kamar['kamar_id'];
				$subquery = "SELECT count(c.bed_id) as jlh from visit_inap_kamar c
							left join visit_ri ri on c.ri_id = ri.ri_id
							left join visit v on v.visit_id = ri.visit_id 
							where c.kamar_id = $id and (substr(c.waktu_masuk,1,7) <= '$bln' and c.waktu_keluar is NULL)
							and v.status_visit NOT LIKE 'CHECKOUT'";
				$jlh = $this->db->query($subquery)->row_array();
				$sub = "SELECT count(c.kamar_id) as jlh_bed from master_bed c where c.kamar_id = $id";
				$jlh_bed = $this->db->query($sub)->row_array();

				$result[$i]['nama_dept'] = $kamar['nama_dept'];
				$result[$i]['nama_kamar'] = $kamar['nama_kamar'];
				$result[$i]['terisi'] = $jlh['jlh'];
				$result[$i]['jlh_bed'] = $jlh_bed['jlh_bed'];
				$i++;
			}

			return $result;
		}
	}
?>