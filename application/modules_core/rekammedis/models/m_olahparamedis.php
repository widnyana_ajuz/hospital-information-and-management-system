<?php  
	/**
	* 
	*/
	class m_olahparamedis extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function get_dokter()
		{
			$sql = "SELECT * from petugas p inner join master_jabatan j on p.jabatan_id = j.jabatan_id 
					inner join master_dept q on p.dept_id = q.dept_id 
					inner join master_kualifikasi i on i.kualifikasi_id = p.kualifikasi_id
					where j.nama_jabatan LIKE 'Dokter'";
			$result = $this->db->query($sql);
			return $result->result_array();
		}

		public function get_perawat()
		{
			$sql = "SELECT * from petugas p inner join master_jabatan j on p.jabatan_id = j.jabatan_id 
					inner join master_dept q on p.dept_id = q.dept_id 
					inner join master_kualifikasi i on i.kualifikasi_id = p.kualifikasi_id
					where (j.nama_jabatan LIKE 'Perawat' or j.nama_jabatan LIKE 'Bidan')";
			$result = $this->db->query($sql);
			return $result->result_array();
		}

		public function get_tenagamedis()
		{
			$sql = "SELECT * from petugas p inner join master_jabatan j on p.jabatan_id = j.jabatan_id 
					inner join master_dept q on p.dept_id = q.dept_id 
					inner join master_kualifikasi i on i.kualifikasi_id = p.kualifikasi_id
					where (j.jenis LIKE 'MEDIS')";
			$result = $this->db->query($sql);
			return $result->result_array();
		}

		public function get_search_dokter($key)
		{
			$sql = "SELECT * from petugas p inner join master_jabatan j on p.jabatan_id = j.jabatan_id 
					inner join master_dept q on p.dept_id = q.dept_id 
					inner join master_kualifikasi i on i.kualifikasi_id = p.kualifikasi_id
					where j.nama_jabatan LIKE 'Dokter' and p.nama_petugas LIKE '%$key%'";
			$result = $this->db->query($sql);
			return $result->result_array();
		}

		public function get_search_perawat($key)
		{
			$sql = "SELECT * from petugas p inner join master_jabatan j on p.jabatan_id = j.jabatan_id 
					inner join master_dept q on p.dept_id = q.dept_id 
					inner join master_kualifikasi i on i.kualifikasi_id = p.kualifikasi_id
					where (j.nama_jabatan LIKE 'Perawat' or j.nama_jabatan LIKE 'Bidan') and p.nama_petugas LIKE '%$key%'";
			$result = $this->db->query($sql);
			return $result->result_array();
		}

		public function get_search_tenagamedis($key)
		{
			$sql = "SELECT * from petugas p inner join master_jabatan j on p.jabatan_id = j.jabatan_id 
					inner join master_dept q on p.dept_id = q.dept_id 
					inner join master_kualifikasi i on i.kualifikasi_id = p.kualifikasi_id
					where (j.jenis LIKE 'MEDIS')
					and p.nama_petugas LIKE '%$key%'";
			$result = $this->db->query($sql);
			return $result->result_array();
		}

	}
?>