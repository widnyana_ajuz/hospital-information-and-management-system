<?php  
	/**
	* 
	*/
	class m_olahpasien extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function in_patient()
		{
			$sql = "SELECT count(distinct(visit_id))'jumlah' from visit_ri where waktu_keluar IS NULL";
			return $this->db->query($sql)->row_array();
		}

		public function out_patient()
		{
			$sql = "SELECT count(distinct(visit_id))'jumlah' from visit_rj where waktu_keluar IS NULL";
			return $this->db->query($sql)->row_array();
		}

		public function emergency_patient()
		{
			$sql = "SELECT count(distinct(visit_id))'jumlah' from visit_igd where waktu_keluar IS NULL";
			return $this->db->query($sql)->row_array();
		}

		public function get_all_pasien_active()
		{
			$sql = "SELECT p.*, IFNULL(v.tanggal_visit,'-')'tanggal_visit' 
					FROM pasien p left join (select * from visit order by tanggal_visit desc) v 
					on p.rm_id = v.rm_id where p.status_pasien LIKE 'active' group by p.rm_id LIMIT 100";
			$result = $this->db->query($sql);
			if ($result->num_rows() > 0) {
				return $result->result_array();
			}else{
				return false;
			}
		}

		public function get_all_pasien_inactive()
		{
			$sql = "SELECT p.*, IFNULL(v.tanggal_visit,'-')'tanggal_visit' 
					FROM pasien p left join (select * from visit order by tanggal_visit desc) v 
					on p.rm_id = v.rm_id where p.status_pasien LIKE 'inactive' group by p.rm_id LIMIT 100";
			$result = $this->db->query($sql);
			if ($result->num_rows() > 0) {
				return $result->result_array();
			}else{
				return false;
			}
		}

		public function get_all_pasien_meninggal()
		{
			$sql = "SELECT * FROM pasien p inner join (select * from visit order by tanggal_visit desc) v 
					on p.rm_id = v.rm_id where p.status_pasien LIKE 'meninggal' group by p.rm_id";
			$result = $this->db->query($sql);
			if ($result->num_rows() > 0) {
				return $result->result_array();
			}else{
				return false;
			}
		}

		public function search_active_pasien($key='')
		{
			$sql = "SELECT p.*, v.tanggal_visit FROM pasien p inner join (select * from visit order by tanggal_visit desc) v 
					on p.rm_id = v.rm_id where p.status_pasien LIKE 'active' and p.nama LIKE '%$key%' group by p.rm_id";
			$result = $this->db->query($sql);
			if ($result->num_rows() > 0) {
				return $result->result_array();
			}else{
				return false;
			}
		}

		public function search_inactive_pasien($key='')
		{
			$sql = "SELECT p.*, v.tanggal_visit FROM pasien p inner join (select * from visit order by tanggal_visit desc) v 
					on p.rm_id = v.rm_id where p.status_pasien LIKE 'inactive' and p.nama LIKE '%$key%' group by p.rm_id";
			$result = $this->db->query($sql);
			if ($result->num_rows() > 0) {
				return $result->result_array();
			}else{
				return false;
			}
		}

		public function search_died_pasien($key='')
		{
			$sql = "SELECT * FROM pasien p inner join (select * from visit order by tanggal_visit desc) v 
					on p.rm_id = v.rm_id where p.status_pasien LIKE 'meninggal' and p.nama LIKE '%$key%' group by p.rm_id";
			$result = $this->db->query($sql);
			if ($result->num_rows() > 0) {
				return $result->result_array();
			}else{
				return false;
			}
		}

		public function get_detail_pasien($rm_id)
		{
			$sql = "SELECT *
					FROM pasien p /*left join (select * from master_provinsi) mp on mp.prov_id = p.prov_id_skr
					left join (select * from master_kabupaten) mk on mk.kab_id = p.kab_id_skr
					left join (SELECT * from master_kecamatan) mkc on mkc.kec_id = p.kec_id_skr
					left join (select * from master_desa) md on md.kel_id = p.kel_id_skr*/
					where p.rm_id = '$rm_id'";
			//$this->db->where($rm_id);
			$query = $this->db->query($sql);
			if ($query) {
				return $query->row_array();
			}else{
				return false;
			}
		}

		public function get_detail_pasienmeninggal($rm_id)
		{
			$sql = "SELECT *
					FROM pasien p left join (select *, nama_prov as prov_skr  from master_provinsi) mp on mp.prov_id = p.prov_id_skr
					left join (select *, nama_kab as kab_skr from master_kabupaten) mk on mk.kab_id = p.kab_id_skr
					left join (SELECT *, nama_kec as kec_skr from master_kecamatan) mkc on mkc.kec_id = p.kec_id_skr
					
					left join (select *, nama_prov as prov  from master_provinsi) mpa on mpa.prov_id = p.prov_id
					left join (select *, nama_kab as kab from master_kabupaten) mka on mka.kab_id = p.kab_id
					left join (SELECT *, nama_kec as kec from master_kecamatan) mkca on mkca.kec_id = p.kec_id
					
					where p.rm_id = '$rm_id'";
			//$this->db->where($rm_id);
			$query = $this->db->query($sql);
			if ($query) {
				return $query->row_array();
			}else{
				return false;
			}
		}

		public function update_info_pasien($insert, $rm_id)
		{
			$this->db->where('rm_id', $rm_id);
			$result = $this->db->update('pasien', $insert);
			return $result;
		}

		public function get_kab($id='')
		{
			$sql = "SELECT * from master_kabupaten
					where prov_id = '$id'";
			//$this->db->where($rm_id);
			$query = $this->db->query($sql);
			if ($query) {
				return $query->result_array();
			}else{
				return false;
			}
		}

		public function get_kec($id='')
		{
			$sql = "SELECT * from master_kecamatan
					where kab_id = '$id'";
			//$this->db->where($rm_id);
			$query = $this->db->query($sql);
			if ($query) {
				return $query->result_array();
			}else{
				return false;
			}
		}

		public function get_kel($id='')
		{
			$sql = "SELECT * from master_desa
					where kec_id = '$id'";
			//$this->db->where($rm_id);
			$query = $this->db->query($sql);
			if ($query) {
				return $query->result_array();
			}else{
				return false;
			}
		}

		public function inactive_pasien($rm_id, $params)
		{
			$this->db->where('rm_id', $rm_id);
			$result = $this->db->update('pasien', $params);
			return $result;
		}

		public function delete_pasien($rm_id='')
		{
			$this->db->where('rm_id', $rm_id);
			$result = $this->db->delete('pasien');
			return $result;
		}

		public function get_all_pasien_rj()
		{
			$sql = "SELECT * from visit_rj vr inner join visit v on v.visit_id = vr.visit_id 
					inner join pasien p on p.rm_id = v.rm_id 
					left join master_dept mp on mp.dept_id = vr.unit_tujuan 
					where vr.waktu_keluar IS NULL";
			
			$query = $this->db->query($sql);
			if ($query) {
				return $query->result_array();
			}else{
				return false;
			}	
		}
		
		public function get_all_pasien_ri()
		{
			$sql = "SELECT * from pasien p inner join visit v on p.rm_id = v.rm_id 
					inner join visit_ri vr on v.visit_id = vr.visit_id 
					left join master_dept mp on mp.dept_id = vr.unit_tujuan 
					where vr.waktu_keluar IS NULL";
			$query = $this->db->query($sql);
			if ($query) {
				return $query->result_array();
			}else{
				return false;
			}	
		}

		public function get_riwayatklinik($rm_id)
		{
			$sql = "SELECT * FROM visit vd 
					inner join visit_rj vr on vd.visit_id = vr.visit_id inner join overview_klinik o on o.rj_id = vr.rj_id
					left join master_dept m on m.dept_id = vr.unit_tujuan left join petugas p on o.dokter = p.petugas_id
					where vd.rm_id = '$rm_id'
					ORDER BY o.waktu DESC";
	        $query = $this->db->query($sql);
	        $result = $query->result_array();
	        return $result;
		}

		public function get_riwayatigd($rm_id)
		{
			$sql = "SELECT * FROM visit vd
					inner join visit_igd vr on vd.visit_id = vr.visit_id inner join overview_igd o on o.sub_visit = vr.igd_id
					left join master_dept m on m.dept_id = vr.unit_tujuan left join (select *, nama_petugas as r_dokter from petugas ) p on o.dokter = p.petugas_id
					left join (select *, nama_petugas as rperawat from petugas ) pe on o.perawat = pe.petugas_id
					where vd.rm_id = '$rm_id'
					ORDER BY o.waktu DESC";
	        $query = $this->db->query($sql);
	        $result = $query->result_array();
	        return $result;
		}

		public function get_riwayatri($rm_id)
		{	
			 $sql = "SELECT * FROM visit vd
					inner join visit_ri vr on vd.visit_id = vr.visit_id inner join visit_perawatan_dokter o on o.sub_visit = vr.ri_id
					left join master_dept m on m.dept_id = vr.unit_tujuan left join petugas p on o.dokter_visit = p.petugas_id
					left join (select diagnosis_id, diagnosis_nama as diag_utama from master_diagnosa ) ma on ma.diagnosis_id = o.diagnosa_utama
					left join (select diagnosis_id, diagnosis_nama as diag_sek from master_diagnosa ) mb on mb.diagnosis_id = o.sekunder1
					where vd.rm_id = '$rm_id'
					ORDER BY o.waktu_visit DESC";
	        $query = $this->db->query($sql);
	        $result = $query->result_array();
	        return $result;
		}

		public function get_detail_overview_klinis($id)
	    {
	    	$sql = "SELECT * FROM overview_klinik v left join petugas p on v.dokter = p.petugas_id 
	    			left join (select diagnosis_id as diag_u, diagnosis_nama as diagnosa_utama from master_diagnosa) a on v.diagnosa1 = a.diag_u
	    			left join (select diagnosis_id as diag_1, diagnosis_nama as diagnosa_1 from master_diagnosa) b on v.diagnosa2 = b.diag_1
	    			left join (select diagnosis_id as diag_2, diagnosis_nama as diagnosa_2 from master_diagnosa) c on v.diagnosa3 = c.diag_2
	    			left join (select diagnosis_id as diag_3, diagnosis_nama as diagnosa_3 from master_diagnosa) d on v.diagnosa4 = d.diag_3
	    			left join (select diagnosis_id as diag_4, diagnosis_nama as diagnosa_4 from master_diagnosa) e on v.diagnosa5 = e.diag_4
	        		where v.id = '$id'";
	        $result = $this->db->query($sql);
	        if ($result) {
	            return $result->row_array();
	        }else{
	            return false;
	        }
	    }

	    public function get_detail_overview_igd($id)
	    {
	    	$sql = "SELECT * FROM overview_igd v 
	    			left join (select petugas_id, nama_petugas as dokter from petugas) p on v.dokter = p.petugas_id
	    			left join (select petugas_id, nama_petugas as perawat from petugas) z on v.perawat = z.petugas_id
	    			left join (select diagnosis_id as diag_u, diagnosis_nama as diagnosa_utama from master_diagnosa) a on v.diagnosa1 = a.diag_u
	    			left join (select diagnosis_id as diag_1, diagnosis_nama as diagnosa_1 from master_diagnosa) b on v.diagnosa2 = b.diag_1
	    			left join (select diagnosis_id as diag_2, diagnosis_nama as diagnosa_2 from master_diagnosa) c on v.diagnosa3 = c.diag_2
	    			left join (select diagnosis_id as diag_3, diagnosis_nama as diagnosa_3 from master_diagnosa) d on v.diagnosa4 = d.diag_3
	    			left join (select diagnosis_id as diag_4, diagnosis_nama as diagnosa_4 from master_diagnosa) e on v.diagnosa5 = e.diag_4
	        		where v.id = '$id'";
	        $result = $this->db->query($sql);
	        if ($result) {
	            return $result->row_array();
	        }else{
	            return false;
	        }
	    }

	    public function get_detail_over_perawatan($id)
	    {
	    	$sql = "SELECT * FROM visit_perawatan_dokter v 
	    			left join (select petugas_id, nama_petugas as dokter from petugas) p on v.dokter_visit = p.petugas_id
	    			left join (select diagnosis_id as diag_u, diagnosis_nama as diagnosa_u from master_diagnosa) a on v.diagnosa_utama = a.diag_u
	    			left join (select diagnosis_id as diag_1, diagnosis_nama as diagnosa_1 from master_diagnosa) b on v.sekunder1 = b.diag_1
	    			left join (select diagnosis_id as diag_2, diagnosis_nama as diagnosa_2 from master_diagnosa) c on v.sekunder2 = c.diag_2
	    			left join (select diagnosis_id as diag_3, diagnosis_nama as diagnosa_3 from master_diagnosa) d on v.sekunder3 = d.diag_3
	    			left join (select diagnosis_id as diag_4, diagnosis_nama as diagnosa_4 from master_diagnosa) e on v.sekunder4 = e.diag_4
	    			where v.kunjungan_dok_id = '$id'";
	        $result = $this->db->query($sql);
	        if ($result) {
	            return $result->row_array();
	        }else{
	            return false;
	        }
	    }

	    /*rekap*/
	    public function get_all_poli()
	    {
	    	$sql = "SELECT dept_id, nama_dept from master_dept where jenis LIKE 'POLIKLINIK' order by dept_id asc";
	    	$res = $this->db->query($sql);
	    	return $res->result_array();
	    }
	    public function get_all_unit()
	    {
	    	$sql = "SELECT dept_id, nama_dept from master_dept where jenis LIKE 'RAWAT INAP' order by dept_id asc";
	    	$res = $this->db->query($sql);
	    	return $res->result_array();
	    }

	    public function get_jlh_per_poli($date)
	    {
	    	$sql = "SELECT dept_id, nama_dept from master_dept where jenis LIKE 'POLIKLINIK'";
	    	$res = $this->db->query($sql);
	    	$i = 0;
	    	foreach ($res->result_array() as $poli) {
	    		$id = $poli['dept_id'];
	    		$sql = "SELECT count(unit_tujuan) as jlh from visit_rj where unit_tujuan = $id and waktu_masuk = '$date'";
	    		$r = $this->db->query($sql);
	    		$result[$i] = $r->row_array();
	    		$i++;
	    	}

	    	return $result;
	    }

	    public function get_jlh_per_unit($date)
	    {
	    	$sql = "SELECT dept_id, nama_dept from master_dept where jenis LIKE 'RAWAT INAP'";
	    	$res = $this->db->query($sql);
	    	$i = 0;
	    	foreach ($res->result_array() as $poli) {
	    		$id = $poli['dept_id'];
	    		$sql = "SELECT count(unit_tujuan) as jlh from visit_ri where unit_tujuan = $id and substr(waktu_masuk,1,10) = '$date' and waktu_keluar is NULL";
	    		$r = $this->db->query($sql);
	    		$result[$i] = $r->row_array();
	    		$i++;
	    	}

	    	return $result;
	    }

	   /*akhir rekap*/

	   /*iso rekam medis*/
	   public function get_iso_rekam_medis()
	   {
	   		/*inconsistent query result - fakkkkkkk*/
	   		$sql = "SELECT ff.*,p.nama,p.rm_id, mt.nama_dept as dept_in, mk.nama_dept as dept_out 
	   				from (select * from (select a.rm_id, a.status_visit, IF(a.is_pasien_lama=1,'LAMA','BARU')'is_lama', a.visit_id,a.dept_id, a.tanggal_visit,i.cara_bayar,i.ri_id as id, i.waktu_masuk,
	   					i.waktu_keluar from visit a left join visit_ri i on i.visit_id = a.visit_id union
						select b.rm_id, b.status_visit,IF(b.is_pasien_lama=1,'LAMA','BARU')'is_lama', b.visit_id, b.dept_id, b.tanggal_visit,r.cara_bayar,r.rj_id as id, r.waktu_masuk,r.waktu_keluar 
						from visit b left join visit_rj r on r.visit_id = b.visit_id union
						select c.rm_id, c.status_visit,IF(c.is_pasien_lama=1,'LAMA','BARU')'is_lama', c.visit_id,c.dept_id, c.tanggal_visit,g.cara_bayar,g.igd_id as id, g.waktu_masuk,g.waktu_keluar 
						from visit c left join visit_igd g on g.visit_id = c.visit_id) e 
						order by e.waktu_masuk desc) ff 
					left join pasien p on p.rm_id = ff.rm_id  
					left join (select * from master_dept) mt on mt.dept_id = ff.dept_id 
					left join (select * from master_dept) mk on mk.dept_id = substr(ff.id,1,2) 
					where ff.waktu_keluar is not null and ff.status_visit = 'CHECKOUT'
					group by ff.visit_id";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result_array();
			}else{
				return false;
			}
			
	   }

	   public function filter_iso_rekam_medis($rm, $awal, $akhir)
	   {
	   		/*inconsistent query result - fakkkkkkk*/
	   		$sql = "SELECT ff.*,p.nama,p.rm_id, mt.nama_dept as dept_in, mk.nama_dept as dept_out 
	   				from (select * from (select a.rm_id, a.status_visit,IF(a.is_pasien_lama=1,'LAMA','BARU')'is_lama', a.visit_id,a.dept_id, a.tanggal_visit,i.cara_bayar,i.ri_id as id, i.waktu_masuk,
	   					i.waktu_keluar from visit a left join visit_ri i on i.visit_id = a.visit_id union
						select b.rm_id, b.status_visit,IF(b.is_pasien_lama=1,'LAMA','BARU')'is_lama', b.visit_id, b.dept_id, b.tanggal_visit,r.cara_bayar,r.rj_id as id, r.waktu_masuk,r.waktu_keluar 
						from visit b left join visit_rj r on r.visit_id = b.visit_id union
						select c.rm_id, c.status_visit,IF(c.is_pasien_lama=1,'LAMA','BARU')'is_lama', c.visit_id,c.dept_id, c.tanggal_visit,g.cara_bayar,g.igd_id as id, g.waktu_masuk,g.waktu_keluar 
						from visit c left join visit_igd g on g.visit_id = c.visit_id) e 
						order by e.waktu_masuk desc) ff 
					left join pasien p on p.rm_id = ff.rm_id  
					left join (select * from master_dept) mt on mt.dept_id = ff.dept_id 
					left join (select * from master_dept) mk on mk.dept_id = substr(ff.id,1,2) 
					where ff.waktu_keluar is not null and ff.status_visit = 'CHECKOUT' 
					and p.rm_id LIKE '%$rm%' 
					and substr(ff.tanggal_visit, 1,10) >= '$awal' and substr(ff.tanggal_visit, 1,10) <= '$akhir'
					group by ff.visit_id";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result_array();
			}else{
				return false;
			}
			
	   }
	   /*akhir iso*/

	   public function get_pasien_rj_per_kecamatan($bulan='', $tahun)
	   {
	   		$sql = "SELECT m.nama_dept,
	   				SUM(IF(p.jenis_kelamin='LAKI-LAKI',1,0))'laki',
	   				SUM(IF(p.jenis_kelamin='PEREMPUAN',1,0))'perempuan',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Bakarangan',1,0))'bakarangan',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Binuang',1,0))'binuang',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Bungur',1,0))'bungur',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Candi Laras Selatan',1,0))'candilarasselatan',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Candi Laras Utara',1,0))'candilarasutara',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Hatungun',1,0))'hatungun',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Lokpaikat',1,0))'lokpaikat',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Piani',1,0))'piani',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Salam Babaris',1,0))'salambabaris',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Tapin Selatan',1,0))'tapinselatan',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Tapin Tengah',1,0))'tapintengah',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Tapin Utara',1,0))'tapinutara',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= ('Bakarangan' OR 'Binuang' OR 'Bungur'
	   										OR 'Candi Laras Selatan' OR 'Candi Laras Utara'
	   										OR 'Hatungun' OR 'Lokpaikat' OR 'Piani'
	   										OR 'Salam Babaris' OR 'Tapin Selatan' OR 'Tapin Tengah' OR 'Tapin Utara'),0,1))'lain',
					count(r.visit_id) 'total'
	   				from visit_rj r left join visit v on v.visit_id = r.visit_id
	   				left join pasien p on p.rm_id = v.rm_id 
	   				left join master_dept m on m.dept_id = substr(r.rj_id, 1,2)
	   				where MONTH(r.waktu_masuk) = '$bulan' and YEAR(r.waktu_masuk) = '$tahun'
	   				group by substr(r.rj_id, 1,2)";
	   		$query = $this->db->query($sql);
	   		return $query->result_array();
	   }

	   public function get_pasien_ri_per_kecamatan($bulan='', $tahun)
	   {
	   		$sql = "SELECT m.nama_dept,
	   				SUM(IF(p.jenis_kelamin='LAKI-LAKI',1,0))'laki',
	   				SUM(IF(p.jenis_kelamin='PEREMPUAN',1,0))'perempuan',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Bakarangan',1,0))'bakarangan',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Binuang',1,0))'binuang',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Bungur',1,0))'bungur',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Candi Laras Selatan',1,0))'candilarasselatan',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Candi Laras Utara',1,0))'candilarasutara',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Hatungun',1,0))'hatungun',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Lokpaikat',1,0))'lokpaikat',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Piani',1,0))'piani',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Salam Babaris',1,0))'salambabaris',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Tapin Selatan',1,0))'tapinselatan',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Tapin Tengah',1,0))'tapintengah',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= 'Tapin Utara',1,0))'tapinutara',
	   				SUM(IF((select nama_kec from master_kecamatan where kec_id = p.kec_id_skr )= ('Bakarangan' OR 'Binuang' OR 'Bungur'
	   										OR 'Candi Laras Selatan' OR 'Candi Laras Utara'
	   										OR 'Hatungun' OR 'Lokpaikat' OR 'Piani'
	   										OR 'Salam Babaris' OR 'Tapin Selatan' OR 'Tapin Tengah' OR 'Tapin Utara'),0,1))'lain',
					count(r.visit_id) 'total'
	   				from visit_ri r left join visit v on v.visit_id = r.visit_id
	   				left join pasien p on p.rm_id = v.rm_id 
	   				left join master_dept m on m.dept_id = substr(r.ri_id, 1,2)
	   				where MONTH(r.waktu_masuk) = '$bulan' and YEAR(r.waktu_masuk) = '$tahun' 
	   				group by substr(r.ri_id, 1,2)";
	   		$query = $this->db->query($sql);
	   		return $query->result_array();
	   }

	   public function get_pasien_rj_per_cara_bayar($bulan='', $tahun)
	   {
	   		$sql = "SELECT m.nama_dept,
	   				SUM(IF(p.jenis_kelamin='LAKI-LAKI',1,0))'laki',
	   				SUM(IF(p.jenis_kelamin='PEREMPUAN',1,0))'perempuan', 
					SUM(IF(v.cara_bayar='Umum',1,0))'umum',
					SUM(IF(v.cara_bayar='BPJS',1,0))'bpjs',
					SUM(IF(v.cara_bayar='JamKesmas',1,0))'jamkesmas',
					SUM(IF(v.cara_bayar='Asuransi',1,0))'asuransi',
					SUM(IF(v.cara_bayar='Kontrak',1,0))'kontrak', 
					SUM(IF(v.cara_bayar='Gratis',1,0))'gratis', 
					SUM(IF(v.cara_bayar='Lain-lain',1,0))'lain_lain',
					count(distinct(v.visit_id)) 'total'
					from visit_rj v left join visit vi on vi.visit_id = v.visit_id
					left join pasien p on p.rm_id = vi.rm_id
					left join master_dept m on m.dept_id = substr(v.rj_id, 1,2)
					where MONTH(v.waktu_masuk) = '$bulan' and YEAR(v.waktu_masuk) = '$tahun' 
					group by substr(v.rj_id, 1,2)";
			$query = $this->db->query($sql);

			$poli = $this->db->query("SELECT nama_dept from master_dept where jenis LIKE 'POLIKLINIK'")->result_array();
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($poli as $value) {
						if ($key['nama_dept'] == $value['nama_dept']) {
							$rekap[$key['nama_dept']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}	
	   }

	   public function get_pasien_ri_per_cara_bayar($bulan='', $tahun)
	   {
	   		$sql = "SELECT m.nama_dept,
	   				SUM(IF(p.jenis_kelamin='LAKI-LAKI',1,0))'laki',
	   				SUM(IF(p.jenis_kelamin='PEREMPUAN',1,0))'perempuan', 
					SUM(IF(v.cara_bayar='Umum',1,0))'umum',
					SUM(IF(v.cara_bayar='BPJS',1,0))'bpjs',
					SUM(IF(v.cara_bayar='JamKesmas',1,0))'jamkesmas',
					SUM(IF(v.cara_bayar='Asuransi',1,0))'asuransi',
					SUM(IF(v.cara_bayar='Kontrak',1,0))'kontrak', 
					SUM(IF(v.cara_bayar='Gratis',1,0))'gratis', 
					SUM(IF(v.cara_bayar='Lain-lain',1,0))'lain_lain',
					count(v.visit_id) 'total'
					from visit_ri v left join visit vi on vi.visit_id = v.visit_id
					left join pasien p on p.rm_id = vi.rm_id
					left join master_dept m on m.dept_id = substr(v.ri_id, 1,2)
					where MONTH(v.waktu_masuk) = '$bulan' and YEAR(v.waktu_masuk) = '$tahun'
					/*and (v.alasan_keluar != 'Pasien pindah%' or v.ala*/
					group by substr(v.ri_id, 1,2)";
			$query = $this->db->query($sql);

			$poli = $this->db->query("SELECT nama_dept from master_dept where jenis LIKE 'RAWAT INAP'")->result_array();
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($poli as $value) {
						if ($key['nama_dept'] == $value['nama_dept']) {
							$rekap[$key['nama_dept']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}	
	   }

	   public function get_klinik()
	   {
	   		$sql = "SELECT dept_id, nama_dept from master_dept where jenis LIKE 'POLIKLINIK' order by dept_id asc";
			$query = $this->db->query($sql);
			return $query->result_array();
	   }

	   public function get_rekap_rawat_jalan($bulan, $tahun)
	   {
	   		$sql = "SELECT 
	   				SUM(IF(p.jenis_kelamin = 'LAKI-LAKI' and v.is_pasien_lama = '1', 1,0))'co_lama',
	   				SUM(IF(p.jenis_kelamin = 'LAKI-LAKI' and (v.is_pasien_lama = '0' or v.is_pasien_lama IS NULL), 1,0))'co_baru',
	   				SUM(IF(p.jenis_kelamin = 'PEREMPUAN' and v.is_pasien_lama = '1', 1,0))'ce_lama',
	   				SUM(IF(p.jenis_kelamin = 'PEREMPUAN' and (v.is_pasien_lama = '0' or v.is_pasien_lama IS NULL), 1,0))'ce_baru',
	   				SUM(IF(vr.cara_bayar='Umum',1,0))'umum',
					SUM(IF(vr.cara_bayar='BPJS',1,0))'bpjs',
					SUM(IF(vr.cara_bayar='JamKesmas',1,0))'jamkesmas',
					SUM(IF(vr.cara_bayar='Asuransi',1,0))'asuransi',
					SUM(IF(vr.cara_bayar='Kontrak',1,0))'kontrak', 
					SUM(IF(vr.cara_bayar='Gratis',1,0))'gratis', 
					SUM(IF(vr.cara_bayar='Lain-lain',1,0))'lain_lain',
					count(vr.cara_bayar)'total'
					FROM visit_rj vr left join visit v on v.visit_id = vr.visit_id
					left join pasien p on p.rm_id = v.rm_id
					where MONTH(vr.waktu_masuk) = '$bulan' and YEAR(vr.waktu_masuk) = '$tahun'
					group by substr(vr.rj_id, 1,2)";
			$query = $this->db->query($sql);
			$rekap['atas'] = $query->row_array();
			$sql = "SELECT dept_id, nama_dept from master_dept where jenis LIKE 'POLIKLINIK' order by dept_id asc";
			$query = $this->db->query($sql);

			foreach ($query->result_array() as $key) {
				$dept_id = $key['dept_id'];
				$dept_nama = $key['nama_dept'];

				$q = "SELECT SUM(IF(substr(vr.rj_id, 1,2) = '$dept_id', 1,0))'jumlah'
					FROM visit_rj vr 
					where MONTH(vr.waktu_masuk) = '$bulan' and YEAR(vr.waktu_masuk) = '$tahun'";
				$qy = $this->db->query($q);
				$res[$dept_nama] = $qy->row_array()['jumlah'];
			}

			$rekap['bawah'] = $res;
			if (empty($rekap['atas']) or empty($rekap['bawah'])) {
				return array();
			}
			return $rekap;
	   }

	   public function status_pulang_rj($tanggal, $dept_id)
	   {
	   		$sql = "SELECT
	   				IFNULL(SUM(IF(vr.waktu_keluar IS NULL,1,0)),0)'belum_pulang',
	   				IFNULL(SUM(IF(vr.waktu_keluar IS NOT NULL,1,0)),0)'sudah_pulang',
	   				IFNULL(SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Rujuk IGD',1,0)),0)'rujuk_igd',
	   				IFNULL(SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Rujuk Rawat Inap',1,0)),0)'rujuk_ri',
	   				IFNULL(SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Pasien Meninggal',1,0)),0)'mati',
	   				IFNULL(SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Rujuk Rumah Sakit lain',1,0)),0)'rujuk_rs',
	   				IFNULL(SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Atas Permintaan Sendiri',1,0)),0)'aps',
	   				IFNULL(SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Pasien Dipindahkan',1,0)),0)'pindah_poli',
	   				IFNULL(SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Pasien Dipulangkan',1,0)),0)'dipulangkan',
	   				count(distinct(vr.visit_id))
	   				from visit_rj vr where date(vr.waktu_masuk) = date('$tanggal')";
	   		if ($dept_id != '') {
	   			$sql .= " and substr(vr.rj_id, 1,2) = '$dept_id' ";
	   		}
	   		$sql .= "group by substr(vr.rj_id, 1,2)";
	   		$query = $this->db->query($sql);
			return $query->row_array();
	   }

	   public function get_rekap_rawat_inap($bulan, $tahun)
	   {
	   		$sql = "SELECT 
	   				SUM(IF(p.jenis_kelamin = 'LAKI-LAKI' and v.is_pasien_lama = '1', 1,0))'co_lama',
	   				SUM(IF(p.jenis_kelamin = 'LAKI-LAKI' and (v.is_pasien_lama = '0' or v.is_pasien_lama IS NULL) , 1,0))'co_baru',
	   				SUM(IF(p.jenis_kelamin = 'PEREMPUAN' and v.is_pasien_lama = '1', 1,0))'ce_lama',
	   				SUM(IF(p.jenis_kelamin = 'PEREMPUAN' and (v.is_pasien_lama = '0' or v.is_pasien_lama IS NULL), 1,0))'ce_baru',
	   				IFNULL(count(v.visit_id),0)'total'
					FROM visit_ri vr left join visit v on v.visit_id = vr.visit_id
					left join pasien p on p.rm_id = v.rm_id
					where MONTH(vr.waktu_masuk) = '$bulan' and YEAR(vr.waktu_masuk) = '$tahun'";
			$query = $this->db->query($sql);
			$rekap['atas'] = $query->row_array();
			$rekap['atas']['tanggal'] = $bulan ."/".$tahun;

			$sql = "SELECT dept_id, nama_dept from master_dept where jenis LIKE 'POLIKLINIK' order by dept_id asc";
			$query = $this->db->query($sql);

			foreach ($query->result_array() as $key) {
				$dept_id = $key['dept_id'];
				$dept_nama = $key['nama_dept'];

				$q = "SELECT SUM(IF(vr.unit_asal = '$dept_id', 1,0))'jumlah'
					FROM visit_ri vr 
					where MONTH(vr.waktu_masuk) = '$bulan' and YEAR(vr.waktu_masuk) = '$tahun'";
				$qy = $this->db->query($q);
				$res[$dept_nama] = $qy->row_array()['jumlah'];
			}

			$rekap['bawah'] = $res;

			$rawat = "SELECT dept_id, nama_dept from master_dept where jenis LIKE 'RAWAT INAP' order by dept_id asc";
			$que = $this->db->query($rawat);

			foreach ($que->result_array() as $key) {
				$dept_id = $key['dept_id'];
				$dept_nama = $key['nama_dept'];

				$q = "SELECT SUM(IF(vr.unit_tujuan = '$dept_id', 1,0))'jumlah'
					FROM visit_ri vr 
					where MONTH(vr.waktu_masuk) = '$bulan' and YEAR(vr.waktu_masuk) = '$tahun'";
				$qy = $this->db->query($q);
				$r[$dept_nama] = $qy->row_array()['jumlah'];
			}

			$rekap['ri'] = $r;

			return $rekap;
	   }

	   public function status_pulang_ri($tanggal, $dept_id)
	   {
	   		$sql = "SELECT
	   				SUM(IF(vr.waktu_keluar IS NULL,1,0))'belum_pulang',
	   				SUM(IF(vr.waktu_keluar IS NOT NULL,1,0))'sudah_pulang',
	   				SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Pasien Meninggal',1,0))'mati',
	   				SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Rujuk Rumah Sakit Lain',1,0))'rujuk_rs',
	   				SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Atas Permintaan Sendiri',1,0))'aps',
	   				SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Pasien Pindah%',1,0))'pindah_poli',
	   				SUM(IF(vr.waktu_keluar IS NOT NULL and vr.alasan_keluar = 'Pasien Dipulangkan',1,0))'dipulangkan',
	   				count(distinct(vr.visit_id))
	   				from visit_ri vr where date(vr.waktu_masuk) = date('$tanggal')";
	   		if ($dept_id != '') {
	   			$sql .= " and substr(vr.ri_id, 1,2) = '$dept_id' ";
	   		}
	   		$sql .= "group by substr(vr.ri_id, 1,2)";
	   		$query = $this->db->query($sql);
			return $query->row_array();
	   }

	   public function pasien_pulang()
	   {
	   		$sql = "SELECT p.*, kr.*,md.nama_dept
	   				from visit_ri r left join visit_inap_kamar vk on vk.ri_id = r.ri_id
	   				left join master_kamar kr on kr.kamar_id = vk.kamar_id
	   				left join visit v on v.visit_id = r.visit_id 
	   				left join pasien p on p.rm_id = v.rm_id
	   				left join master_dept md on r.unit_tujuan = md.dept_id
	   				where r.alasan_keluar LIKE '%Dipulangkan%' and v.status_visit LIKE 'CHECKOUT'";
	   		$query = $this->db->query($sql);
			return $query->result_array();
	   }
	}
?>