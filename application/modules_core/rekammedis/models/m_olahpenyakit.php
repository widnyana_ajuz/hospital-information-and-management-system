<?php  
	/**
	* 
	*/
	class m_olahpenyakit extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function insert_icdx($params)
		{
			$res = $this->db->insert('master_diagnosa', $params);
			return $res;
		}

		public function insert_icd_ix_cm($params)
		{
			$res = $this->db->insert('master_icd9cm', $params);
			return $res;
		}

		public function insert_inacbg_s($params)
		{
			$res = $this->db->insert('master_inacbg', $params);
			return $res;

		}

		public function get_icdx()
		{
			$sql = "SELECT a.*, b.kode, b.deskripsi from master_diagnosa a left join master_inacbg b on a.diagnosis_group = b.kode";
			return $this->db->query($sql)->result_array();
		}

		public function get_icd9cm()
		{
			$sql = "SELECT * from master_icd9cm";
			return $this->db->query($sql)->result_array();
		}

		public function get_inacbg()
		{
			$sql = "SELECT * from master_inacbg";
			return $this->db->query($sql)->result_array();
		}

		public function insert_new_icdx($params)
		{
			$res = $this->db->insert('master_diagnosa', $params);
			return $this->db->insert_id();
		}

		public function insert_new_inacbg($params)
		{
			$res = $this->db->insert('master_inacbg', $params);
			return $this->db->insert_id();
		}

		public function insert_new_icd9cm($params)
		{
			$res = $this->db->insert('master_icd9cm', $params);
			return $this->db->insert_id();
		}

		public function delete_icdx($id)
		{
			$this->db->where('id', $id);
			$res = $this->db->delete('master_diagnosa');
			return $res;
		}

		public function delete_inacbg($id)
		{
			$this->db->where('id', $id);
			$res = $this->db->delete('master_inacbg');
			return $res;
		}

		public function delete_icd9cm($id)
		{
			$this->db->where('id', $id);
			$res = $this->db->delete('master_icd9cm');
			return $res;
		}

		public function edit_icdx($id, $insert)
		{
			$this->db->where('id', $id);
			$res = $this->db->update('master_diagnosa', $insert);
			return $res;
		}

		public function edit_inacbg($id, $insert)
		{
			$this->db->where('id', $id);
			$res = $this->db->update('master_inacbg', $insert);
			return $res;
		}

		public function edit_icd9cm($id, $insert)
		{
			$this->db->where('id', $id);
			$res = $this->db->update('master_icd9cm', $insert);
			return $res;
		}

		public function get_group_icdX()
		{
			return $this->db->query("SELECT kode,deskripsi from master_inacbg")->result_array();
		}

		public function search_icdx($key)
		{
			$sql = "SELECT a.*, b.kode, b.deskripsi 
					from master_diagnosa a left join master_inacbg b on a.diagnosis_group = b.kode
					where (a.diagnosis_id = '$key' or a.diagnosis_nama LIKE '%$key%')";
			return $this->db->query($sql)->result_array();
		}

		public function search_icd9cm($key)
		{
			$sql = "SELECT * from master_icd9cm where kode = '$key' or prosedur LIKE '%$key%'";
			return $this->db->query($sql)->result_array();
		}

		public function search_inacbg($key)
		{
			$sql = "SELECT * from master_inacbg where kode = '$key' or deskripsi LIKE '%$key%'";
			return $this->db->query($sql)->result_array();
		}
	}
?>