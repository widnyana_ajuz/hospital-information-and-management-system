<?php  
	/**
	* 
	*/
	class m_olahrekammedis extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function get_dept_nama($dept)
		{
			$sql = "SELECT nama_dept from master_dept where dept_id = '$dept'";
			return $this->db->query($sql)->row_array();
		}

		public function get_unit_rekammedis()
		{
			$sql = "SELECT * from master_dept where (jenis LIKE '%poli%' or jenis LIKE '%rawat%' or nama_dept like 'IGD')";
			return $this->db->query($sql)->result_array();
		}

		public function get_klinik()
		{
			$sql = "SELECT * from master_dept where (jenis LIKE '%poli%')";
			return $this->db->query($sql)->result_array();
		}

		public function get_ri()
		{
			$sql = "SELECT * from master_dept where (jenis LIKE '%INAP%')";
			return $this->db->query($sql)->result_array();
		}

		public function get_10_besar_penyakit($dept_id,$bulan,$tahun)
		{
			$jenis = $this->db->query("SELECT jenis from master_dept where dept_id = '$dept_id'")->row_array()['jenis'];

			switch ($jenis) {
				case 'RAWAT INAP':
					{
						$sql = "SELECT b.diagnosis_nama, count(a.diagnosa_utama)'jumlah' from visit_perawatan_dokter a
								left join master_diagnosa b on a.diagnosa_utama = b.diagnosis_id 
								where substr(a.sub_visit, 1,2) = '$dept_id' 
								and MONTH(a.waktu_visit) = '$bulan' and YEAR(a.waktu_visit) = '$tahun'
								group by b.diagnosis_id order by jumlah desc limit 10";
						return $this->db->query($sql)->result_array();
					}
					break;
				case 'POLIKLINIK':
					{
						$sql = "SELECT b.diagnosis_nama, count(a.diagnosa1)'jumlah' from overview_klinik a
								left join master_diagnosa b on a.diagnosa1 = b.diagnosis_id 
								where substr(a.rj_id, 1,2) = '$dept_id' 
								and MONTH(a.waktu) = '$bulan' and YEAR(a.waktu) = '$tahun'
								group by b.diagnosis_id order by jumlah desc limit 10";
						return $this->db->query($sql)->result_array();
					}
					break;
				case 'IGD':
					{
						$sql = "SELECT b.diagnosis_nama, count(a.diagnosa1)'jumlah' from overview_igd a
								left join master_diagnosa b on a.diagnosa1 = b.diagnosis_id 
								where substr(a.sub_visit, 1,2) = '$dept_id' 
								and MONTH(a.waktu) = '$bulan' and YEAR(a.waktu) = '$tahun'
								group by b.diagnosis_id order by jumlah desc limit 10";
						return $this->db->query($sql)->result_array();
					}
					break;
				default:
					
					break;
			}

		}

		public function get_sensus_harian_poli($dept_id='', $start, $end)
		{
			$sql = "SELECT p.rm_id, p.nama, p.alamat_skr, p.jenis_kelamin,a.cara_bayar, 
					IF((a.is_kasus_baru=1 or a.is_kasus_baru=NULL),'Lama', 'Baru') as 'jenis_kasus', x.nama_dept as 'asal_rujukan',
					z.nama_dept as 'tujuan_rujuk', b.diagnosa1, p.tanggal_lahir
					FROM visit_rj a inner join overview_klinik b on a.rj_id = b.rj_id 
					left join visit v on v.visit_id = a.visit_id 
					left join pasien p on p.rm_id = v.rm_id left join master_dept x on x.dept_id = a.unit_asal 
					left join master_dept z on z.dept_id = a.unit_rujukan 
					where substr(a.rj_id,1,2) = '$dept_id' and (a.waktu_masuk >= date('$start') and a.waktu_masuk <= date('$end'))";
			return $this->db->query($sql)->result_array();
		}	

		public function get_sensus_harian_igd($start, $end)
		{
			$sql = "SELECT p.rm_id, p.nama, p.alamat_skr, p.jenis_kelamin,a.cara_bayar, a.jenis,
					IF(DATEDIFF('a.waktu_masuk','a.waktu_keluar') > 1,'Rawat Inap', 'Rawat Jalan') as 'jenis_kasus', x.nama_dept as 'asal_rujukan',
					z.nama_dept as 'tujuan_rujuk', b.diagnosa1, p.tanggal_lahir,
                    IF(a.is_kunjungan_baru=1, 'Baru','Lama') 'jenis_kunjungan', a.detail_kematian, u.sebab_penyakit
					FROM visit_igd a inner join overview_igd b on a.igd_id = b.sub_visit 
					left join visit v on v.visit_id = a.visit_id 
					left join pasien p on p.rm_id = v.rm_id left join master_dept x on x.dept_id = a.unit_asal 
					left join master_dept z on z.dept_id = a.unit_rujukan left join master_golongan_sebab_penyakit u
                    on u.kode_sebab = a.sebab
					where a.waktu_keluar IS NOT NULL and (a.waktu_masuk >= date('$start') and a.waktu_masuk <= date('$end'))";
			return $this->db->query($sql)->result_array();
		}	

		public function get_rekap_kunjungan_by_month_igd($bulan,$tahun)
		{
			$sql = "SELECT SUM(IF(a.is_kunjungan_baru='1',1,0))'baru',
					SUM(IF(a.is_kunjungan_baru='1',0,1))'lama',count(a.visit_id)'total' 
					from visit_igd a where MONTH(a.waktu_masuk) = '$bulan' and YEAR(a.waktu_masuk) = '$tahun'";
			$query = $this->db->query($sql);
			if ($query->num_rows > 0) {
				return $query->result_array();
			} else {
				return false;
			}		
		}


		public function get_rekap_rujukan_by_month_igd($bulan,$tahun){
			$sql = "SELECT i.jenis, 
					SUM(IF(v.cara_masuk='Datang sendiri',1,0))'tanpa_rujukan',
					SUM(IF(v.cara_masuk='Puskesmas',1,0))'puskesmas',
					SUM(IF(v.cara_masuk='Rujuk RS lain',1,0))'rujuk_rs',
					SUM(IF(v.cara_masuk='Instansi',1,0))'instansi',
					SUM(IF(v.cara_masuk='Rujukan Dokter',1,0))'rujuk_dokter',
					count(v.cara_masuk) 'total'
					from visit_igd i left join visit v on v.visit_id = i.visit_id 
					where MONTH(i.waktu_masuk) = '$bulan' and YEAR(i.waktu_masuk) = '$tahun' group by i.jenis";
			$query = $this->db->query($sql);
			$pelayanan = array('Bedah', 'Non Bedah', 'Kecelakaan Lalulintas', 'Kebidanan', 'Psikiatrik', 'Anak');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($pelayanan as $value) {
						if ($key['jenis'] == $value) {
							$rekap[$key['jenis']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}	
		}

		public function get_rekap_cara_bayar_by_month_igd($bulan,$tahun){
			$sql = "SELECT v.jenis, 
					SUM(IF(v.cara_bayar='Umum',1,0))'umum',
					SUM(IF(v.cara_bayar='BPJS',1,0))'bpjs',
					SUM(IF(v.cara_bayar='JamKesmas',1,0))'jamkesmas',
					SUM(IF(v.cara_bayar='Asuransi',1,0))'asuransi',
					SUM(IF(v.cara_bayar='Kontrak',1,0))'kontrak', 
					SUM(IF(v.cara_bayar='Gratis',1,0))'gratis', 
					SUM(IF(v.cara_bayar='Lain-lain',1,0))'lain_lain',
					count(v.cara_bayar) 'total'
					from visit_igd v 
					where MONTH(v.waktu_masuk) = '$bulan' and YEAR(v.waktu_masuk) = '$tahun' group by v.jenis";
			$query = $this->db->query($sql);
			$pelayanan = array('Bedah', 'Non Bedah', 'Kecelakaan Lalulintas', 'Kebidanan', 'Psikiatrik', 'Anak');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($pelayanan as $value) {
						if ($key['jenis'] == $value) {
							$rekap[$key['jenis']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}	
		}

		public function get_rekap_alasan_by_month_igd($bulan,$tahun){
			$sql = "SELECT v.jenis, 
					SUM(IF(v.alasan_keluar='Pasien Dipulangkan',1,0))'dipulangkan',
					SUM(IF(v.alasan_keluar='Atas Permintaan Sendiri',1,0))'aps',
					SUM(IF(v.alasan_keluar='Rujuk Rumah Sakit Lain',1,0))'rujuk',
					SUM(IF(v.alasan_keluar='Rujuk Rawat Inap',1,0))'rawat',
					SUM(IF((v.detail_kematian='sebelum dirawat' and v.alasan_keluar='Pasien Meninggal'),1,0))'sebelum_rawat', 
					SUM(IF((v.detail_kematian!='sebelum dirawat' and v.alasan_keluar='Pasien Meninggal'),1,0))'setelah_rawat',
					count(v.alasan_keluar) 'total'
					from visit_igd v 
					where MONTH(v.waktu_masuk) = '$bulan' and YEAR(v.waktu_masuk) = '$tahun' group by v.jenis";
			$query = $this->db->query($sql);
			$pelayanan = array('Bedah', 'Non Bedah', 'Kecelakaan Lalulintas', 'Kebidanan', 'Psikiatrik', 'Anak');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($pelayanan as $value) {
						if ($key['jenis'] == $value) {
							$rekap[$key['jenis']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}	
		}

		/*rekap per tahun IGD*/
		public function get_rekap_kunjungan_by_year_igd($tahun)
		{
			$sql = "SELECT IF(a.is_kunjungan_baru = 1,'BARU','LAMA') 'jenis_kunjungan', 
					SUM(IF(MONTH(a.waktu_masuk)='01',1,0))'jan',
					SUM(IF(MONTH(a.waktu_masuk)='02',1,0))'feb',
					SUM(IF(MONTH(a.waktu_masuk)='03',1,0))'mar',
					SUM(IF(MONTH(a.waktu_masuk)='04',1,0))'apr',
					SUM(IF(MONTH(a.waktu_masuk)='05',1,0))'mei',
					SUM(IF(MONTH(a.waktu_masuk)='06',1,0))'jun',
					SUM(IF(MONTH(a.waktu_masuk)='07',1,0))'jul',
					SUM(IF(MONTH(a.waktu_masuk)='08',1,0))'ags',
					SUM(IF(MONTH(a.waktu_masuk)='09',1,0))'sep',
					SUM(IF(MONTH(a.waktu_masuk)='10',1,0))'okt',
					SUM(IF(MONTH(a.waktu_masuk)='11',1,0))'nov',
					SUM(IF(MONTH(a.waktu_masuk)='12',1,0))'des',
					count(a.visit_id) 'total'
					from visit_igd a where YEAR(a.waktu_masuk) = '$tahun' 
					group by a.is_kunjungan_baru";
			$query = $this->db->query($sql);
			$rekap = array();
			$tindakan = array('BARU','LAMA');
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($tindakan as $value) {
						if ($key['jenis_kunjungan'] == $value) {
							$rekap[$key['jenis_kunjungan']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}
		}

		public function get_rekap_pelayanan_by_year_igd($tahun)
		{
			$sql = "SELECT a.jenis, 
					SUM(IF(MONTH(a.waktu_masuk)='01',1,0))'jan',
					SUM(IF(MONTH(a.waktu_masuk)='02',1,0))'feb',
					SUM(IF(MONTH(a.waktu_masuk)='03',1,0))'mar',
					SUM(IF(MONTH(a.waktu_masuk)='04',1,0))'apr',
					SUM(IF(MONTH(a.waktu_masuk)='05',1,0))'mei',
					SUM(IF(MONTH(a.waktu_masuk)='06',1,0))'jun',
					SUM(IF(MONTH(a.waktu_masuk)='07',1,0))'jul',
					SUM(IF(MONTH(a.waktu_masuk)='08',1,0))'ags',
					SUM(IF(MONTH(a.waktu_masuk)='09',1,0))'sep',
					SUM(IF(MONTH(a.waktu_masuk)='10',1,0))'okt',
					SUM(IF(MONTH(a.waktu_masuk)='11',1,0))'nov',
					SUM(IF(MONTH(a.waktu_masuk)='12',1,0))'des',
					count(a.jenis) 'total'
					from visit_igd a where YEAR(a.waktu_masuk) = '$tahun' 
					group by a.jenis";
			$query = $this->db->query($sql);
			$pelayanan = array('Bedah', 'Non Bedah', 'Kecelakaan Lalulintas', 'Kebidanan', 'Psikiatrik', 'Anak');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($pelayanan as $value) {
						if ($key['jenis'] == $value) {
							$rekap[$key['jenis']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}
		}

		public function get_rekap_carabayar_by_year_igd($tahun)
		{
			$sql = "SELECT a.cara_bayar, 
					SUM(IF(MONTH(a.waktu_masuk)='01',1,0))'jan',
					SUM(IF(MONTH(a.waktu_masuk)='02',1,0))'feb',
					SUM(IF(MONTH(a.waktu_masuk)='03',1,0))'mar',
					SUM(IF(MONTH(a.waktu_masuk)='04',1,0))'apr',
					SUM(IF(MONTH(a.waktu_masuk)='05',1,0))'mei',
					SUM(IF(MONTH(a.waktu_masuk)='06',1,0))'jun',
					SUM(IF(MONTH(a.waktu_masuk)='07',1,0))'jul',
					SUM(IF(MONTH(a.waktu_masuk)='08',1,0))'ags',
					SUM(IF(MONTH(a.waktu_masuk)='09',1,0))'sep',
					SUM(IF(MONTH(a.waktu_masuk)='10',1,0))'okt',
					SUM(IF(MONTH(a.waktu_masuk)='11',1,0))'nov',
					SUM(IF(MONTH(a.waktu_masuk)='12',1,0))'des',
					count(a.cara_bayar) 'total'
					from visit_igd a where YEAR(a.waktu_masuk) = '$tahun' 
					group by a.cara_bayar";
			$query = $this->db->query($sql);
			$pelayanan = array('Umum', 'BPJS', 'JamKesmas', 'Asuransi', 'Kontrak', 'Gratis', 'Lain-lain');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($pelayanan as $value) {
						if ($key['cara_bayar'] == $value) {
							$rekap[$key['cara_bayar']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}
		}

		public function get_rekap_rujukan_by_year_igd($tahun)
		{
			$sql = "SELECT v.cara_masuk, 
					SUM(IF(MONTH(a.waktu_masuk)='01',1,0))'jan',
					SUM(IF(MONTH(a.waktu_masuk)='02',1,0))'feb',
					SUM(IF(MONTH(a.waktu_masuk)='03',1,0))'mar',
					SUM(IF(MONTH(a.waktu_masuk)='04',1,0))'apr',
					SUM(IF(MONTH(a.waktu_masuk)='05',1,0))'mei',
					SUM(IF(MONTH(a.waktu_masuk)='06',1,0))'jun',
					SUM(IF(MONTH(a.waktu_masuk)='07',1,0))'jul',
					SUM(IF(MONTH(a.waktu_masuk)='08',1,0))'ags',
					SUM(IF(MONTH(a.waktu_masuk)='09',1,0))'sep',
					SUM(IF(MONTH(a.waktu_masuk)='10',1,0))'okt',
					SUM(IF(MONTH(a.waktu_masuk)='11',1,0))'nov',
					SUM(IF(MONTH(a.waktu_masuk)='12',1,0))'des',
					count(v.cara_masuk) 'total'
					from visit_igd a left join visit v on v.visit_id = a.visit_id where YEAR(a.waktu_masuk) = '$tahun' 
					group by v.cara_masuk";
			$query = $this->db->query($sql);
			$cara = array('Datang sendiri', 'Puskesmas', 'Rujuk RS lain', 'Instansi', 'Rujukan Dokter');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($cara as $value) {
						if ($key['cara_masuk'] == $value) {
							$rekap[$key['cara_masuk']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}
		}

		public function get_rekap_resume_pulang_by_year_igd($tahun)
		{
			$sql = "SELECT IF(a.alasan_keluar='Pasien Meninggal',
					(IF(a.detail_kematian='sebelum dirawat',a.detail_kematian,'sesudah dirawat')),a.alasan_keluar) 'alasan', 
					SUM(IF(MONTH(a.waktu_masuk)='01',1,0))'jan',
					SUM(IF(MONTH(a.waktu_masuk)='02',1,0))'feb',
					SUM(IF(MONTH(a.waktu_masuk)='03',1,0))'mar',
					SUM(IF(MONTH(a.waktu_masuk)='04',1,0))'apr',
					SUM(IF(MONTH(a.waktu_masuk)='05',1,0))'mei',
					SUM(IF(MONTH(a.waktu_masuk)='06',1,0))'jun',
					SUM(IF(MONTH(a.waktu_masuk)='07',1,0))'jul',
					SUM(IF(MONTH(a.waktu_masuk)='08',1,0))'ags',
					SUM(IF(MONTH(a.waktu_masuk)='09',1,0))'sep',
					SUM(IF(MONTH(a.waktu_masuk)='10',1,0))'okt',
					SUM(IF(MONTH(a.waktu_masuk)='11',1,0))'nov',
					SUM(IF(MONTH(a.waktu_masuk)='12',1,0))'des',
					count(a.alasan_keluar) 'total'
					from visit_igd a where YEAR(a.waktu_masuk) = '$tahun' 
					group by alasan";
			$query = $this->db->query($sql);
			$alasan = array('Pasien Dipulangkan', 
							'Atas Permintaan Sendiri', 'Rujuk Rumah Sakit Lain', 
							'Rujuk Rawat Inap', 'sebelum dirawat', 'sesudah dirawat');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($alasan as $value) {
						if ($key['alasan'] == $value) {
							$rekap[$key['alasan']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}
		}

		/*akhir rekap tahunan igd*/

		/*rekap bulanan poli*/
		public function get_rekap_kunjungan_by_month_poli($dept_id,$bulan,$tahun)
		{
			$sql = "SELECT SUM(IF(a.is_kunjungan_baru='1',1,0))'baru',
					SUM(IF(a.is_kunjungan_baru='1',0,1))'lama',count(a.visit_id)'total' 
					from visit_rj a 
					where MONTH(a.waktu_masuk) = '$bulan' and YEAR(a.waktu_masuk) = '$tahun'
					and substr(a.rj_id, 1,2) = '$dept_id'";
			$query = $this->db->query($sql);
			if ($query->num_rows > 0) {
				return $query->result_array();
			} else {
				return false;
			}		
		}


		public function get_rekap_rujukan_by_month_poli($dept_id,$bulan,$tahun){
			$sql = "SELECT 
					SUM(IF(v.cara_masuk='Datang sendiri',1,0))'tanpa_rujukan',
					SUM(IF(v.cara_masuk='Puskesmas',1,0))'puskesmas',
					SUM(IF(v.cara_masuk='Rujuk RS lain',1,0))'rujuk_rs',
					SUM(IF(v.cara_masuk='Instansi',1,0))'instansi',
					SUM(IF(v.cara_masuk='Rujukan Dokter',1,0))'rujuk_dokter',
					count(v.cara_masuk) 'total'
					from visit_rj i left join visit v on v.visit_id = i.visit_id 
					where MONTH(i.waktu_masuk) = '$bulan' and YEAR(i.waktu_masuk) = '$tahun'
					and substr(i.rj_id, 1,2) = '$dept_id'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}

		public function get_rekap_cara_bayar_by_month_poli($dept_id,$bulan,$tahun){
			$sql = "SELECT  
					SUM(IF(v.cara_bayar='Umum',1,0))'umum',
					SUM(IF(v.cara_bayar='BPJS',1,0))'bpjs',
					SUM(IF(v.cara_bayar='JamKesmas',1,0))'jamkesmas',
					SUM(IF(v.cara_bayar='Asuransi',1,0))'asuransi',
					SUM(IF(v.cara_bayar='Kontrak',1,0))'kontrak', 
					SUM(IF(v.cara_bayar='Gratis',1,0))'gratis', 
					SUM(IF(v.cara_bayar='Lain-lain',1,0))'lain_lain',
					count(v.cara_bayar) 'total'
					from visit_rj v 
					where MONTH(v.waktu_masuk) = '$bulan' and YEAR(v.waktu_masuk) = '$tahun'
					and substr(v.rj_id, 1,2) = '$dept_id'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}

		public function get_rekap_alasan_by_month_poli($dept_id,$bulan,$tahun){
			$sql = "SELECT
					SUM(IF(v.alasan_keluar='Pasien Dipulangkan',1,0))'dipulangkan',
					SUM(IF(v.alasan_keluar='Atas Permintaan Sendiri',1,0))'aps',
					SUM(IF(v.alasan_keluar='Rujuk Rumah Sakit Lain',1,0))'rujuk',
					SUM(IF(v.alasan_keluar='Rujuk Rawat Inap',1,0))'rawat',
					SUM(IF((v.detail_alasan_keluar='sebelum dirawat' and v.alasan_keluar='Pasien Meninggal'),1,0))'sebelum_rawat', 
					SUM(IF((v.detail_alasan_keluar!='sebelum dirawat' and v.alasan_keluar='Pasien Meninggal'),1,0))'setelah_rawat',
					count(v.alasan_keluar) 'total'
					from visit_rj v 
					where MONTH(v.waktu_masuk) = '$bulan' and YEAR(v.waktu_masuk) = '$tahun'
					and substr(v.rj_id, 1,2) = '$dept_id'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		/*akhir rekap bulanan poli*/

		/*rekap tahunan poli*/
		public function get_rekap_kunjungan_by_year_poli($dept_id,$tahun)
		{
			$sql = "SELECT IF(a.is_kunjungan_baru = 1,'BARU','LAMA') 'jenis_kunjungan', 
					SUM(IF(MONTH(a.waktu_masuk)='01',1,0))'jan',
					SUM(IF(MONTH(a.waktu_masuk)='02',1,0))'feb',
					SUM(IF(MONTH(a.waktu_masuk)='03',1,0))'mar',
					SUM(IF(MONTH(a.waktu_masuk)='04',1,0))'apr',
					SUM(IF(MONTH(a.waktu_masuk)='05',1,0))'mei',
					SUM(IF(MONTH(a.waktu_masuk)='06',1,0))'jun',
					SUM(IF(MONTH(a.waktu_masuk)='07',1,0))'jul',
					SUM(IF(MONTH(a.waktu_masuk)='08',1,0))'ags',
					SUM(IF(MONTH(a.waktu_masuk)='09',1,0))'sep',
					SUM(IF(MONTH(a.waktu_masuk)='10',1,0))'okt',
					SUM(IF(MONTH(a.waktu_masuk)='11',1,0))'nov',
					SUM(IF(MONTH(a.waktu_masuk)='12',1,0))'des',
					count(a.visit_id) 'total'
					from visit_rj a where YEAR(a.waktu_masuk) = '$tahun' 
					and substr(a.rj_id, 1,2) = '$dept_id'
					group by a.is_kunjungan_baru";
			$query = $this->db->query($sql);
			$rekap = array();
			$tindakan = array('BARU','LAMA');
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($tindakan as $value) {
						if ($key['jenis_kunjungan'] == $value) {
							$rekap[$key['jenis_kunjungan']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}
		}

		public function get_rekap_rujukan_by_year_poli($dept_id,$tahun)
		{
			$sql = "SELECT v.cara_masuk, 
					SUM(IF(MONTH(a.waktu_masuk)='01',1,0))'jan',
					SUM(IF(MONTH(a.waktu_masuk)='02',1,0))'feb',
					SUM(IF(MONTH(a.waktu_masuk)='03',1,0))'mar',
					SUM(IF(MONTH(a.waktu_masuk)='04',1,0))'apr',
					SUM(IF(MONTH(a.waktu_masuk)='05',1,0))'mei',
					SUM(IF(MONTH(a.waktu_masuk)='06',1,0))'jun',
					SUM(IF(MONTH(a.waktu_masuk)='07',1,0))'jul',
					SUM(IF(MONTH(a.waktu_masuk)='08',1,0))'ags',
					SUM(IF(MONTH(a.waktu_masuk)='09',1,0))'sep',
					SUM(IF(MONTH(a.waktu_masuk)='10',1,0))'okt',
					SUM(IF(MONTH(a.waktu_masuk)='11',1,0))'nov',
					SUM(IF(MONTH(a.waktu_masuk)='12',1,0))'des',
					count(v.cara_masuk) 'total'
					from visit_rj a left join visit v on v.visit_id = a.visit_id where YEAR(a.waktu_masuk) = '$tahun' 
					and substr(a.rj_id, 1,2) = '$dept_id' 
					group by v.cara_masuk";
			$query = $this->db->query($sql);
			$cara = array('Datang sendiri', 'Puskesmas', 'Rujuk RS lain', 'Instansi', 'Rujukan Dokter');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($cara as $value) {
						if ($key['cara_masuk'] == $value) {
							$rekap[$key['cara_masuk']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}
		}

		public function get_rekap_resume_pulang_by_year_poli($dept_id,$tahun)
		{
			$sql = "SELECT IF(a.alasan_keluar='Pasien Meninggal',
					(IF(a.detail_alasan_keluar='sebelum dirawat',a.detail_alasan_keluar,'sesudah dirawat')),a.alasan_keluar) 'alasan', 
					SUM(IF(MONTH(a.waktu_masuk)='01',1,0))'jan',
					SUM(IF(MONTH(a.waktu_masuk)='02',1,0))'feb',
					SUM(IF(MONTH(a.waktu_masuk)='03',1,0))'mar',
					SUM(IF(MONTH(a.waktu_masuk)='04',1,0))'apr',
					SUM(IF(MONTH(a.waktu_masuk)='05',1,0))'mei',
					SUM(IF(MONTH(a.waktu_masuk)='06',1,0))'jun',
					SUM(IF(MONTH(a.waktu_masuk)='07',1,0))'jul',
					SUM(IF(MONTH(a.waktu_masuk)='08',1,0))'ags',
					SUM(IF(MONTH(a.waktu_masuk)='09',1,0))'sep',
					SUM(IF(MONTH(a.waktu_masuk)='10',1,0))'okt',
					SUM(IF(MONTH(a.waktu_masuk)='11',1,0))'nov',
					SUM(IF(MONTH(a.waktu_masuk)='12',1,0))'des',
					count(a.alasan_keluar) 'total'
					from visit_rj a where YEAR(a.waktu_masuk) = '$tahun'
					and substr(a.rj_id, 1,2) = '$dept_id' 
					group by alasan";
			$query = $this->db->query($sql);
			$alasan = array('Pasien Dipulangkan', 
							'Atas Permintaan Sendiri', 'Rujuk Rumah Sakit Lain', 
							'Rujuk Rawat Inap', 'sebelum dirawat', 'sesudah dirawat');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($alasan as $value) {
						if ($key['alasan'] == $value) {
							$rekap[$key['alasan']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}
		}

		public function get_rekap_carabayar_by_year_poli($dept_id,$tahun)
		{
			$sql = "SELECT a.cara_bayar, 
					SUM(IF(MONTH(a.waktu_masuk)='01',1,0))'jan',
					SUM(IF(MONTH(a.waktu_masuk)='02',1,0))'feb',
					SUM(IF(MONTH(a.waktu_masuk)='03',1,0))'mar',
					SUM(IF(MONTH(a.waktu_masuk)='04',1,0))'apr',
					SUM(IF(MONTH(a.waktu_masuk)='05',1,0))'mei',
					SUM(IF(MONTH(a.waktu_masuk)='06',1,0))'jun',
					SUM(IF(MONTH(a.waktu_masuk)='07',1,0))'jul',
					SUM(IF(MONTH(a.waktu_masuk)='08',1,0))'ags',
					SUM(IF(MONTH(a.waktu_masuk)='09',1,0))'sep',
					SUM(IF(MONTH(a.waktu_masuk)='10',1,0))'okt',
					SUM(IF(MONTH(a.waktu_masuk)='11',1,0))'nov',
					SUM(IF(MONTH(a.waktu_masuk)='12',1,0))'des',
					count(a.cara_bayar) 'total'
					from visit_rj a where YEAR(a.waktu_masuk) = '$tahun' 
					and substr(a.rj_id, 1,2) = '$dept_id'
					group by a.cara_bayar";
			$query = $this->db->query($sql);
			$pelayanan = array('Umum', 'BPJS', 'JamKesmas', 'Asuransi', 'Kontrak', 'Gratis', 'Lain-lain');
			$rekap = array();
			if ($query->num_rows > 0) {
				foreach ($query->result_array() as $key) {
					foreach ($pelayanan as $value) {
						if ($key['cara_bayar'] == $value) {
							$rekap[$key['cara_bayar']] = $key;
						}
					}
				}
				return $rekap;
				//return $query->result_array(); 
			} else {
				return false;
			}
		}

		/*akhir rekap tahunann poli*/

		public function get_rekap_persebaran($dept_id, $tahun, $bulan)
		{
			$sql = "SELECT nama_kec from master_kecamatan";
			$dept = $this->db->query($sql)->result_array();

			$query = "SELECT p.kec_id_skr, mk.nama_kec,
						SUM(IF(v.cara_bayar='Umum',1,0))'umum',
						SUM(IF(v.cara_bayar='BPJS',1,0))'bpjs',
						SUM(IF(v.cara_bayar='JamKesmas',1,0))'jamkesmas',
						SUM(IF(v.cara_bayar='Asuransi',1,0))'asuransi',
						SUM(IF(v.cara_bayar='Kontrak',1,0))'kontrak', 
						SUM(IF(v.cara_bayar='Gratis',1,0))'gratis', 
						SUM(IF(v.cara_bayar='Lain-lain',1,0))'lain_lain',
						count(v.cara_bayar) 'total'
						from visit_ri v left join visit vt on v.visit_id = vt.visit_id
						left join pasien p on p.rm_id = vt.rm_id
						left join master_kecamatan mk on mk.kec_id = p.kec_id_skr
						where MONTH(v.waktu_masuk) = '$bulan' and YEAR(v.waktu_masuk) = '$tahun'
						and substr(v.ri_id, 1,2) = '$dept_id'
						group by p.kec_id_skr";
			$res = $this->db->query($query);
			$rekap = array();
			foreach ($res->result_array() as $pp) {
				foreach ($dept as $key) {
					if ($key['nama_kec'] = $pp['nama_kec']) {
						$rekap[$key['nama_kec']] = $pp;
					}
				}
			}

			return $rekap;
		}

		public function get_pasien_masuk_by_cara_bayar($dept_id, $tahun, $bulan)
		{
			$sql = "SELECT mk.kelas_kamar, 
						sum(IF(ri.cara_bayar='Umum',1,0))'umum',
						sum(IF(ri.cara_bayar='BPJS',1,0))'bpjs',
						sum(IF(ri.cara_bayar='JamKesmas',1,0))'jamkesmas',
						sum(IF(ri.cara_bayar='Asuransi',1,0))'asuransi',
						SUM(IF(ri.cara_bayar='Kontrak',1,0))'kontrak', 
						sum(IF(ri.cara_bayar='Gratis',1,0))'gratis',
						SUM(IF(ri.cara_bayar='Lain-lain',1,0))'lain_lain',
						count(ri.cara_bayar) 'total' 
						from master_kamar mk left join 
							(select * from visit_inap_kamar where MONTH(waktu_masuk) = '$bulan' and YEAR(waktu_masuk) = '$tahun')vk 
							on vk.kamar_id = mk.kamar_id 
						left join visit_ri ri on ri.ri_id = vk.ri_id left join visit v on v.visit_id = ri.visit_id
						where mk.dept_id = '$dept_id' group by mk.kelas_kamar order by mk.kelas_kamar desc";
			$query = $this->db->query($sql);
	        if ($query->num_rows() > 0) {
	            return $query->result_array();
	        } else {
	            return array();
	        }  
		}

		public function get_pasien_rujukan_by_cara_bayar($dept_id, $tahun, $bulan)
		{
			$sql = "SELECT mk.kelas_kamar, 
						sum(IF(ri.cara_bayar='Umum',1,0))'umum',
						sum(IF(ri.cara_bayar='BPJS',1,0))'bpjs',
						sum(IF(ri.cara_bayar='JamKesmas',1,0))'jamkesmas',
						sum(IF(ri.cara_bayar='Asuransi',1,0))'asuransi',
						SUM(IF(ri.cara_bayar='Kontrak',1,0))'kontrak', 
						sum(IF(ri.cara_bayar='Gratis',1,0))'gratis',
						SUM(IF(ri.cara_bayar='Lain-lain',1,0))'lain_lain',
						count(ri.cara_bayar) 'total' 
						from master_kamar mk left join 
							(select * from visit_inap_kamar where MONTH(waktu_masuk) = '$bulan' and YEAR(waktu_masuk) = '$tahun')vk 
							on vk.kamar_id = mk.kamar_id 
						left join visit_ri ri on ri.ri_id = vk.ri_id left join visit v on v.visit_id = ri.visit_id
						where mk.dept_id = '$dept_id' and v.cara_masuk != 'Datang sendiri' group by mk.kelas_kamar order by mk.kelas_kamar desc";
			$query = $this->db->query($sql);
	        if ($query->num_rows() > 0) {
	            return $query->result_array();
	        } else {
	            return array();
	        }  
		}

		public function get_pasien_sisa_by_cara_bayar($dept_id, $tahun, $bulan)
		{
			$sql = "SELECT mk.kelas_kamar, 
						sum(IF(ri.cara_bayar='Umum',1,0))'umum',
						sum(IF(ri.cara_bayar='BPJS',1,0))'bpjs',
						sum(IF(ri.cara_bayar='JamKesmas',1,0))'jamkesmas',
						sum(IF(ri.cara_bayar='Asuransi',1,0))'asuransi',
						SUM(IF(ri.cara_bayar='Kontrak',1,0))'kontrak', 
						sum(IF(ri.cara_bayar='Gratis',1,0))'gratis',
						SUM(IF(ri.cara_bayar='Lain-lain',1,0))'lain_lain',
						count(ri.cara_bayar) 'total' 
						from master_kamar mk left join 
							(select * from visit_inap_kamar where MONTH(waktu_masuk) = '$bulan' and YEAR(waktu_masuk) = '$tahun')vk 
							on vk.kamar_id = mk.kamar_id 
						left join visit_ri ri on ri.ri_id = vk.ri_id left join visit v on v.visit_id = ri.visit_id
						where mk.dept_id = '$dept_id' and ri.waktu_keluar != NULL group by mk.kelas_kamar order by mk.kelas_kamar desc";
			$query = $this->db->query($sql);
	        if ($query->num_rows() > 0) {
	            return $query->result_array();
	        } else {
	            return array();
	        }  
		}

		public function get_pasien_pulang_by_cara_bayar($dept_id, $tahun, $bulan)
		{
			$sql = "SELECT mk.kelas_kamar, 
						sum(IF(ri.cara_bayar='Umum',1,0))'umum',
						sum(IF(ri.cara_bayar='BPJS',1,0))'bpjs',
						sum(IF(ri.cara_bayar='JamKesmas',1,0))'jamkesmas',
						sum(IF(ri.cara_bayar='Asuransi',1,0))'asuransi',
						SUM(IF(ri.cara_bayar='Kontrak',1,0))'kontrak', 
						sum(IF(ri.cara_bayar='Gratis',1,0))'gratis',
						SUM(IF(ri.cara_bayar='Lain-lain',1,0))'lain_lain',
						count(ri.cara_bayar) 'total' 
						from master_kamar mk left join 
							(select * from visit_inap_kamar where MONTH(waktu_masuk) = '$bulan' and YEAR(waktu_masuk) = '$tahun')vk 
							on vk.kamar_id = mk.kamar_id 
						left join visit_ri ri on ri.ri_id = vk.ri_id left join visit v on v.visit_id = ri.visit_id";
						
			$pindah = $sql ." where mk.dept_id = '$dept_id' and ri.alasan_keluar LIKE 'Pasien Pindah%' 
						group by mk.kelas_kamar order by mk.kelas_kamar desc";
			$query = $this->db->query($pindah);
			$rekap['pindah'] = $query->result_array();

			$dipulangkan = $sql ." where mk.dept_id = '$dept_id' and ri.alasan_keluar LIKE 'Pasien Dipulangkan%' 
						group by mk.kelas_kamar order by mk.kelas_kamar desc";
			$query = $this->db->query($dipulangkan);
			$rekap['pulang'] = $query->result_array();

			$aps = $sql ." where mk.dept_id = '$dept_id' and ri.alasan_keluar LIKE 'Atas%' 
						group by mk.kelas_kamar order by mk.kelas_kamar desc";
			$query = $this->db->query($aps);
			$rekap['aps'] = $query->result_array();

			$rujuk = $sql ." where mk.dept_id = '$dept_id' and ri.alasan_keluar LIKE 'Rujuk Rumah%' 
						group by mk.kelas_kamar order by mk.kelas_kamar desc";
			$query = $this->db->query($rujuk);
			$rekap['rujuk'] = $query->result_array();

	        return $rekap; 
		}

		public function get_pasien_mati_by_cara_bayar($dept_id, $tahun, $bulan)
		{
			$sql = "SELECT mk.kelas_kamar, 
						sum(IF(ri.cara_bayar='Umum',1,0))'umum',
						sum(IF(ri.cara_bayar='BPJS',1,0))'bpjs',
						sum(IF(ri.cara_bayar='JamKesmas',1,0))'jamkesmas',
						sum(IF(ri.cara_bayar='Asuransi',1,0))'asuransi',
						SUM(IF(ri.cara_bayar='Kontrak',1,0))'kontrak', 
						sum(IF(ri.cara_bayar='Gratis',1,0))'gratis',
						SUM(IF(ri.cara_bayar='Lain-lain',1,0))'lain_lain',
						count(ri.cara_bayar) 'total' 
						from master_kamar mk left join 
							(select * from visit_inap_kamar where MONTH(waktu_masuk) = '$bulan' and YEAR(waktu_masuk) = '$tahun')vk 
							on vk.kamar_id = mk.kamar_id 
						left join visit_ri ri on ri.ri_id = vk.ri_id left join visit v on v.visit_id = ri.visit_id ";
			$kurang = $sql. "where mk.dept_id = '$dept_id' and ri.alasan_keluar = '%Meninggal%' 
						and (HOUR(TIMEDIFF(ri.waktu_kematian, ri.waktu_masuk))) < 2
						group by mk.kelas_kamar order by mk.kelas_kamar desc";
			$lebih = $sql. "where mk.dept_id = '$dept_id' and ri.alasan_keluar = '%Meninggal%' 
						and (HOUR(TIMEDIFF(ri.waktu_kematian, ri.waktu_masuk))) > 2
						group by mk.kelas_kamar order by mk.kelas_kamar desc";
			$rekap['kurang'] = $this->db->query($kurang)->result_array();
			$rekap['lebih'] = $this->db->query($lebih)->result_array();
			return $rekap;
		}

		public function get_laporan_jenis_penyakit($dept_id, $tahun, $bulan)
		{
			$sql = "SELECT 
					ri.diagnosa_akhir,d.diagnosis_nama,
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) <= 6 ),1,0))'0_6_TH_PRIA',
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) > 6 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) <= 28 ),1,0))'6_28_HR_PRIA',
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) > 28 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) <= 365 ),1,0))'28_HR_1_TH_PRIA',
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 1 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 4 ),1,0))'1_4_TH_PRIA',
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 4 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 14 ),1,0))'4_14_TH_PRIA',
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 14 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 24 ),1,0))'14_24_TH_PRIA',
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 24 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 44 ),1,0))'24_44_TH_PRIA',
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 44 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 64 ),1,0))'44_64_TH_PRIA',
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 64 ),1,0))'64_ATAS_TH_PRIA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) <= 6 ),1,0))'0_6_TH_WANITA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) > 6 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) <= 28 ),1,0))'6_28_HR_WANITA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) > 28 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir))) <= 365 ),1,0))'28_HR_1_TH_WANITA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 1 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 4 ),1,0))'1_4_TH_WANITA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 4 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 14 ),1,0))'4_14_TH_WANITA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 14 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 24 ),1,0))'14_24_TH_WANITA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 24 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 44 ),1,0))'24_44_TH_WANITA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 44 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) <= 64 ),1,0))'44_64_TH_WANITA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN' 
							AND (DATEDIFF(DATE(ri.waktu_masuk), DATE(rm.tanggal_lahir)) / 365) > 64 ),1,0))'64_ATAS_TH_WANITA',
					SUM(IF( (rm.jenis_kelamin ='PEREMPUAN'),1,0))'JK_WANITA',
					SUM(IF( (rm.jenis_kelamin ='LAKI-LAKI'),1,0))'JK_PRIA'
				from visit_ri ri
				left join visit v on v.visit_id = ri.visit_id
				left join pasien rm on rm.rm_id = v.rm_id
				left join master_diagnosa d on d.diagnosis_id = ri.diagnosa_akhir
				where  MONTH(ri.waktu_masuk) = '$bulan' AND YEAR(ri.waktu_masuk) = '$tahun'
					and substr(ri.ri_id, 1,2) = '$dept_id' and ri.waktu_keluar != NULL
				group by ri.diagnosa_akhir";
			$query = $this->db->query($sql);
	        if ($query->num_rows() > 0) {
	            return $query->result_array();
	        } else {
	            return array();
	        }  
		}

		public function get_sensus_harian_ri($dept, $tanggal)
		{
			$sql = "SELECT p.*,ri.*,mk.kelas_kamar,v.cara_masuk, pt.nama_petugas 
					from visit_inap_kamar vk left join visit_ri ri on vk.ri_id = ri.ri_id 
					left join visit v on ri.visit_id = v.visit_id
					left join pasien p on p.rm_id = v.rm_id 
					left join visit_perawatan_dokter vt on vt.sub_visit = ri.ri_id
					left join petugas pt on pt.petugas_id = vt.dokter_visit
					left join master_kamar mk on mk.kamar_id = vk.kamar_id
					where ri.unit_tujuan = '$dept' and substr(ri.waktu_masuk, 1,10) = '$tanggal' 
					and (v.status_visit != 'PINDAH' AND v.status_visit != 'CHECKOUT')";
			$query = $this->db->query($sql);		
			// echo "<pre>"; print_r($query->result());die;
			if ($query->num_rows > 0) {
				return $query->result_array();
			} else {
				return array();
			}
		}


		public function get_transfer_masuk_ri($dept, $tanggal)
		{
			$sql = "SELECT p.*,ri.*,mk.kelas_kamar,v.cara_masuk, pt.nama_petugas 
					from visit_inap_kamar vk left join visit_ri ri on vk.ri_id = ri.ri_id 
					left join visit v on ri.visit_id = v.visit_id
					left join pasien p on p.rm_id = v.rm_id 
					left join visit_perawatan_dokter vt on vt.sub_visit = ri.ri_id
					left join petugas pt on pt.petugas_id = vt.dokter_visit
					left join master_kamar mk on mk.kamar_id = vk.kamar_id
					where ri.unit_tujuan = '$dept' and substr(ri.waktu_masuk, 1,10) = '$tanggal' 
					and (v.status_visit = 'PINDAH' AND v.status_visit != 'CHECKOUT') and ri.unit_asal != '$dept'";
			$query = $this->db->query($sql);		
			// echo "<pre>"; print_r($query->result());die;
			if ($query->num_rows > 0) {
				return $query->result_array();
			} else {
				return array();
			}
		}

		public function get_transfer_keluar_ri($dept, $tanggal)
		{
			$sql = "SELECT p.*,ri.*,mk.kelas_kamar, pt.nama_petugas 
					from visit_inap_kamar vk left join visit_ri ri on vk.ri_id = ri.ri_id 
					left join visit v on ri.visit_id = v.visit_id
					left join pasien p on p.rm_id = v.rm_id 
					left join visit_perawatan_dokter vt on vt.sub_visit = ri.ri_id
					left join petugas pt on pt.petugas_id = vt.dokter_visit
					left join master_kamar mk on mk.kamar_id = vk.kamar_id
					where ri.unit_tujuan != '$dept' and substr(ri.waktu_masuk, 1,10) = '$tanggal' 
					and (v.status_visit = 'PINDAH' AND v.status_visit != 'CHECKOUT') and ri.unit_asal = '$dept'";
			$query = $this->db->query($sql);		
			// echo "<pre>"; print_r($query->result());die;
			if ($query->num_rows > 0) {
				return $query->result_array();
			} else {
				return array();
			}
		}

		public function get_pasien_keluar_rs($dept, $tanggal)
		{
			$sql = "SELECT p.*,ri.*,mk.kelas_kamar,v.cara_masuk, pt.nama_petugas,
					IF((ri.alasan_keluar = 'Pasien Dipulangkan'),'&#10004;','')'pulang_sembuh', 
					IF((ri.alasan_keluar = 'Rujuk Rumah Sakit Lain'),'&#10004;','')'rujuk_rs',
					IF((ri.alasan_keluar = 'Atas Permintaan Sendiri'),'&#10004;','')'aps',
					IF((ri.alasan_keluar = 'Pasien Pindah %'),'&#10004;','')'pindah',
					IF((ri.alasan_keluar = 'Pasien Meninggal' 
						and (HOUR(TIMEDIFF(ri.waktu_kematian,ri.waktu_masuk))) < 2),'&#10004;','')'mati_kurang_48',
					IF((ri.alasan_keluar = 'Pasien Meninggal' 
						and (HOUR(TIMEDIFF(ri.waktu_kematian,ri.waktu_masuk))) >= 2),'&#10004;','')'mati_lebih_48'
					from visit_inap_kamar vk left join visit_ri ri on vk.ri_id = ri.ri_id 
					left join visit v on ri.visit_id = v.visit_id
					left join pasien p on p.rm_id = v.rm_id 
					left join visit_perawatan_dokter vt on vt.sub_visit = ri.ri_id
					left join petugas pt on pt.petugas_id = vt.dokter_visit
					left join master_kamar mk on mk.kamar_id = vk.kamar_id
					where ri.unit_tujuan = '$dept' and substr(ri.waktu_masuk, 1,10) = '$tanggal' 
					and v.status_visit = 'CHECKOUT'";
			$query = $this->db->query($sql);		
			// echo "<pre>"; print_r($query->result());die;
			if ($query->num_rows > 0) {
				return $query->result_array();
			} else {
				return array();
			}
		}

		public function get_kegiatan_kebinanan($tahun)
		{
			$sql = "SELECT 
					jenis_kegiatan,
					SUM(IF(rujukan_dari ='RUMAH SAKIT',1,0))'RS',
					SUM(IF(rujukan_dari ='BIDAN',1,0))'BIDAN',
					SUM(IF(rujukan_dari ='PUSKESMAS',1,0))'PUSKESMAS',
					SUM(IF(rujukan_dari ='FASKES LAINNYA',1,0))'FASKES',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='HIDUP',1,0))'MDS_HIDUP',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='MATI',1,0))'MDS_MATI',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='HIDUP',1,0))'NON_MDK_HIDUP',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='MATI',1,0))'NON_MDK_MATI',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='HIDUP',1,0))'NON_RJ_HIDUP',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='MATI',1,0))'NON_RJ_MATI',
					SUM(IF(dirujuk_ke ='-',0,1))'DIRUJUK'
				FROM visit_kegiatan_bersalin ri
				WHERE YEAR(waktu) = '$tahun' 
				GROUP BY jenis_kegiatan";
			$query = $this->db->query($sql);
			// echo "<pre>"; print_r($query->result_array());die;		
			$rekap['PERSALINAN NORMAL'] = array('nama_kegiatan'=>'PERSALINAN NORMAL','no'=>'1');
			$rekap['SECTIO CAESARIA'] = array('nama_kegiatan'=>'SECTIO CAESARIA','no'=>'2');
			$rekap['PERS. DGN KOMPLIKASI'] = array('nama_kegiatan'=>'PERS. DGN KOMPLIKASI','no'=>'3');
			$rekap['PEND. SEBELUM PERSALINAN'] = array('nama_kegiatan'=>'PEND. SEBELUM PERSALINAN','no'=>'3.1');
			$rekap['PEND. SESUDAH PERSALINAN'] = array('nama_kegiatan'=>'PEND. SESUDAH PERSALINAN','no'=>'3.2');
			$rekap['PRE ECLAMPSI'] = array('nama_kegiatan'=>'PRE ECLAMPSI','no'=>'3.3');
			$rekap['ECLAMPSI'] = array('nama_kegiatan'=>'ECLAMPSI','no'=>'3.4');
			$rekap['INFEKSI'] = array('nama_kegiatan'=>'INFEKSI','no'=>'3.5');
			$rekap['LAIN-LAIN'] = array('nama_kegiatan'=>'LAIN-LAIN','no'=>'3.6');
			$rekap['ABORTUS'] = array('nama_kegiatan'=>'ABORTUS','no'=>'4');
			$rekap['IMUNISASI TT-1'] = array('nama_kegiatan'=>'IMUNISASI TT-1','no'=>'5.0');
			$rekap['IMUNISASI TT-2'] = array('nama_kegiatan'=>'IMUNISASI TT-2','no'=>'5.1');
			// echo "<pre>"; print_r($rekap);die;		
			$pelayanan = array('PERSALINAN NORMAL','SECTIO CAESARIA','PERS. DGN KOMPLIKASI','PEND. SEBELUM PERSALINAN','PEND. SESUDAH PERSALINAN',
				'PRE ECLAMPSI','ECLAMPSI','INFEKSI','LAIN-LAIN','ABORTUS','IMUNISASI TT-1','IMUNISASI TT-2');
			foreach ($query->result_array() as $key) {
				foreach ($pelayanan as $pel) {
					$ketemu = false;
					if ($key['nama_kegiatan'] == $pel) {
						$rekap[$key['nama_kegiatan']] = $key;
						$ketemu = true;
					}
					else{
						$rekap[$pel] = array();
					}
				}
			}
			// echo "<pre>"; print_r($rekap);die;		
			return $rekap;			
		}

		public function get_kegiatan_perinatologi($tahun)
		{
			$sql = "SELECT 
					sebab_kematian,
					SUM(IF(rujukan_dari ='RUMAH SAKIT',1,0))'RS',
					SUM(IF(rujukan_dari ='BIDAN',1,0))'BIDAN',
					SUM(IF(rujukan_dari ='PUSKESMAS',1,0))'PUSKESMAS',
					SUM(IF(rujukan_dari ='FASKES LAINNYA',1,0))'FASKES',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='HIDUP',1,0))'MDS_HIDUP',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='MATI',1,0))'MDS_MATI',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='HIDUP',1,0))'NON_MDK_HIDUP',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='MATI',1,0))'NON_MDK_MATI',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='HIDUP',1,0))'NON_RJ_HIDUP',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='MATI',1,0))'NON_RJ_MATI'
				FROM visit_kelahiran ri
				WHERE YEAR(waktu_lahir) = '$tahun' AND sebab_kematian != ''
				GROUP BY sebab_kematian";
			$query = $this->db->query($sql);
			// echo "<pre>"; print_r($query->result_array());die;		
			$rekap['ASPHYXIA'] = array('sebab_kematian'=>'ASPHYXIA', 'no'=>'3.1');
			$rekap['TRAUMA KELAHIRAN'] = array('sebab_kematian'=>'TRAUMA KELAHIRAN', 'no'=>'3.2');
			$rekap['BBLR'] = array('sebab_kematian'=>'BBLR', 'no'=>'3.3');
			$rekap['TETANUS NEONATORUM'] = array('sebab_kematian'=>'TETANUS NEONATORUM', 'no'=>'3.4');
			$rekap['KELAHIRAN CONGENITAL'] = array('sebab_kematian'=>'KELAHIRAN CONGENITAL', 'no'=>'3.5');
			$rekap['ISPA'] = array('sebab_kematian'=>'ISPA', 'no'=>'3.6');
			$rekap['DIARE'] = array('sebab_kematian'=>'DIARE', 'no'=>'3.7');
			$rekap['LAIN-LAIN'] = array('sebab_kematian'=>'LAIN-LAIN', 'no'=>'3.8');
			// echo "<pre>"; print_r($rekap);die;		
			$pelayanan = array('ASPHYXIA','TRAUMA KELAHIRAN','BBLR','TETANUS NEONATORUM','KELAHIRAN CONGENITAL','ISPA',
				'DIARE','LAIN-LAIN');
			foreach ($query->result_array() as $key) {
				foreach ($pelayanan as $pel) {
					$ketemu = false;
					if ($key['sebab_kematian'] == $pel) {
						$rekap[$key['sebab_kematian']] = $key;
						$ketemu = true;
					}
				}
			}
			// echo "<pre>"; print_r($rekap);die;		
			return $rekap;	
		}

		public function get_bayi_hidup($thn)
		{
			//lebih dari 2500
			$sql = "SELECT 
					status,
					SUM(IF(rujukan_dari ='RUMAH SAKIT',1,0))'RS',
					SUM(IF(rujukan_dari ='BIDAN',1,0))'BIDAN',
					SUM(IF(rujukan_dari ='PUSKESMAS',1,0))'PUSKESMAS',
					SUM(IF(rujukan_dari ='FASKES LAINNYA',1,0))'FASKES',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='HIDUP',1,0))'MDS_HIDUP',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='MATI',1,0))'MDS_MATI',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='HIDUP',1,0))'NON_MDK_HIDUP',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='MATI',1,0))'NON_MDK_MATI',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='HIDUP',1,0))'NON_RJ_HIDUP',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='MATI',1,0))'NON_RJ_MATI'
				FROM visit_kelahiran
				WHERE YEAR(waktu_lahir) = '$thn' AND berat_badan > 2500 AND status = 'HIDUP'
				GROUP BY status";
			$query = $this->db->query($sql);
			$rekap['lebih'] = $query->row_array();
			$rekap['lebih']['keterangan'] = '> 2500 gram';
			$rekap['lebih']['no'] = '1.1';

			$sql = "SELECT 
					status,
					SUM(IF(rujukan_dari ='RUMAH SAKIT',1,0))'RS',
					SUM(IF(rujukan_dari ='BIDAN',1,0))'BIDAN',
					SUM(IF(rujukan_dari ='PUSKESMAS',1,0))'PUSKESMAS',
					SUM(IF(rujukan_dari ='FASKES LAINNYA',1,0))'FASKES',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='HIDUP',1,0))'MDS_HIDUP',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='MATI',1,0))'MDS_MATI',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='HIDUP',1,0))'NON_MDK_HIDUP',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='MATI',1,0))'NON_MDK_MATI',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='HIDUP',1,0))'NON_RJ_HIDUP',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='MATI',1,0))'NON_RJ_MATI'
				FROM visit_kelahiran
				WHERE YEAR(waktu_lahir) = '$thn' AND berat_badan < 2500 AND status = 'HIDUP'
				GROUP BY status";
			$query = $this->db->query($sql);
			$rekap['kurang'] = $query->row_array();
			$rekap['kurang']['keterangan'] = '< 2500 gram';
			$rekap['kurang']['no'] = '1.2';

			return $rekap;
		}

		public function get_mati_perinatologi($thn)
		{
			//lahir mati
			$sql = "SELECT 
					status,
					SUM(IF(rujukan_dari ='RUMAH SAKIT',1,0))'RS',
					SUM(IF(rujukan_dari ='BIDAN',1,0))'BIDAN',
					SUM(IF(rujukan_dari ='PUSKESMAS',1,0))'PUSKESMAS',
					SUM(IF(rujukan_dari ='FASKES LAINNYA',1,0))'FASKES',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='HIDUP',1,0))'MDS_HIDUP',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='MATI',1,0))'MDS_MATI',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='HIDUP',1,0))'NON_MDK_HIDUP',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='MATI',1,0))'NON_MDK_MATI',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='HIDUP',1,0))'NON_RJ_HIDUP',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='MATI',1,0))'NON_RJ_MATI'
				FROM visit_kelahiran
				WHERE YEAR(waktu_lahir) = '$thn' AND status = 'MATI'
				GROUP BY status";
			$query = $this->db->query($sql);
			$rekap['mati'] = $query->row_array();
			$rekap['mati']['keterangan'] = 'Kelahiran Mati';
			$rekap['mati']['no'] = '2.1';

			$sql = "SELECT 
					status,
					SUM(IF(rujukan_dari ='RUMAH SAKIT',1,0))'RS',
					SUM(IF(rujukan_dari ='BIDAN',1,0))'BIDAN',
					SUM(IF(rujukan_dari ='PUSKESMAS',1,0))'PUSKESMAS',
					SUM(IF(rujukan_dari ='FASKES LAINNYA',1,0))'FASKES',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='HIDUP',1,0))'MDS_HIDUP',
					SUM(IF(rujukan_dari !='NON MEDIS' AND rujukan_dari !='NON RUJUKAN' AND status ='MATI',1,0))'MDS_MATI',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='HIDUP',1,0))'NON_MDK_HIDUP',
					SUM(IF(rujukan_dari ='NON MEDIS' AND status ='MATI',1,0))'NON_MDK_MATI',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='HIDUP',1,0))'NON_RJ_HIDUP',
					SUM(IF(rujukan_dari ='NON RUJUKAN' AND status ='MATI',1,0))'NON_RJ_MATI'
				FROM visit_kelahiran v left join visit_ri i on i.visit_id = v.visit_id
				WHERE YEAR(waktu_lahir) = '$thn' AND status = 'HIDUP'
				AND ( DATEDIFF(DATE(i.waktu_kematian), DATE(v.waktu_lahir)) < 7 )
					AND i.alasan_keluar = '%Meninggal%'				
				GROUP BY status";
			$query = $this->db->query($sql);
			$rekap['mati_7_hari'] = $query->row_array();
			$rekap['mati_7_hari']['keterangan'] = 'Mati Neonatal < 7 hari';
			$rekap['mati_7_hari']['no'] = '2.2';

			return $rekap;

		}

		public function get_register_rawat_inap($dept_id, $bulan, $tahun)
		{
			$sql = "SELECT i.waktu_masuk,p.nama,p.rm_id,p.alamat_skr, p.jenis_kelamin,p.tanggal_lahir,i.waktu_keluar,
					IF(i.unit_asal = '9', '&#10004;','')'dari_igd',
					IF(i.unit_asal != '9' and ((SELECT jenis from master_dept where dept_id = i.unit_asal) = 'POLIKLINIK'), '&#10004;', '') 'dari_rj',
					IF(i.unit_asal != '9' and ((SELECT jenis from master_dept where dept_id like i.unit_asal) = 'RAWAT INAP'), '&#10004;', '') 'dari_ri',
					IF((i.unit_asal = '8' or i.unit_asal IS NULL),'&#10004;','')'langsung_ri',
					i.waktu_keluar, vp.diagnosa_utama, mp.diagnosis_nama, pe.nama_petugas 'dokter',
					IF(v.status_visit='PINDAH' and (i.unit_asal != '' or i.unit_asal IS NOT NULL), drx.nama_dept,'')'dept_asal',
					IF(v.status_visit='PINDAH' and (i.unit_asal != '' or i.unit_asal IS NOT NULL), kmrx.kelas_kamar,'')'kelas_asal',
					IF(v.status_visit='PINDAH', dr.nama_dept,'')'dept_tujuan',
					IF(v.status_visit='PINDAH', kmr.kelas_kamar,'')'kelas_pindah',
					IF(v.status_visit='PINDAH', kr.waktu_masuk,'')'waktu_pindah',
					IF(i.alasan_keluar='Pasien Dipulangkan','&#10004;','')'pulang',
					IF(i.alasan_keluar='Rujuk Rumah Sakit Lain','&#10004;','')'dirujuk',
					IF(i.alasan_keluar='Atas Permintaan Sendiri','&#10004;','')'aps',
					IF(i.alasan_keluar='Pasien Dipulangkan','&#10004;','')'sembuh',
					IF((i.alasan_keluar!='Pasien Dipulangkan' AND i.alasan_keluar!='Pasien Meninggal') or i.alasan_keluar IS NULL,'&#10004;','')'belum_sembuh',
						IF((i.alasan_keluar = 'Pasien Meninggal' 
							AND (HOUR(TIMEDIFF(i.waktu_kematian, i.waktu_masuk))) < 2 ),'&#10004;','')'MATI_KR_48_JAM',
						IF((i.alasan_keluar = 'Pasien Meninggal' 
							AND (HOUR(TIMEDIFF(i.waktu_kematian, i.waktu_masuk))) >= 2 ),'&#10004;','')'MATI_LBH_48_JAM',
					IF(i.cara_bayar='Umum','&#10004','')'umum',
					IF(i.cara_bayar='BPJS','&#10004','')'bpjs',
					IF(i.cara_bayar='Gratis','&#10004','')'gratis',
					IF(i.cara_bayar='JamKesmas','&#10004','')'jamkesmas',
					IF(i.cara_bayar='Kontrak','&#10004','')'kontrak',
					IF(i.cara_bayar='Asuransi','&#10004','')'asuransi',
					IF(i.cara_bayar='Lain-lain','&#10004','')'lain_lain'
					from visit_inap_kamar vk left join master_kamar kmrx on vk.kamar_id = kmrx.kamar_id
					left join visit_inap_kamar kr on vk.inap_id = kr.inap_id
					left join master_kamar kmr on kmr.kamar_id = kr.kamar_id
					left join visit_ri i on kr.ri_id = i.ri_id 
					left join visit_perawatan_dokter vp on vp.sub_visit = i.ri_id
					left join master_diagnosa mp on mp.diagnosis_id = vp.diagnosa_utama
					left join visit v on v.visit_id = i.visit_id
					left join pasien p on p.rm_id = v.rm_id
					left join master_dept drx on drx.dept_id = i.unit_asal
					left join master_dept dr on dr.dept_id = i.unit_tujuan
					left join petugas pe on pe.petugas_id = vp.dokter_visit
					where date(substr(vk.waktu_masuk,1,10)) >= '$bulan' and date(substr(vk.waktu_masuk,1,10)) <= '$tahun' and substr(i.ri_id, 1,2) = '$dept_id'
					order by vk.waktu_masuk ASC";
			$query = $this->db->query($sql);		
			// echo "<pre>"; print_r($query->result());die;
			if ($query->num_rows > 0) {
				return $query->result_array();
			} else {
				return array();
			}
		}

		public function get_ketenagaan()
		{
			$sql = "SELECT m.kualifikasi, count(p.kualifikasi_id) as jumlah from petugas p 
					left join master_kualifikasi m on m.kualifikasi_id = p.kualifikasi_id 
					group by p.kualifikasi_id";

			$query = $this->db->query($sql);		
			// echo "<pre>"; print_r($query->result());die;
			if ($query->num_rows > 0) {
				return $query->result_array();
			} else {
				return array();
			}
		}

	}
?>