<br>
<div class="title">
<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>laporanrekammedis/home">LAPORAN REKAM MEDIS</a>
			
		</li> 
</div>
<div class="navigation1" style=" min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
 			<div style="padding-top:10px"></div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">10 Besar Penyakit</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" 
	        		role="form" method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/penyakit_10_besar">
			        <div class="tabelinformasi" >
				        <div class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_rekam">
		        					<option value="" selected>Pilih</option>
		        					<?php  
		        						foreach ($unit as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
		        			<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
							
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        			<?php  
		        				if (isset($message) and $message != '') {
		        					echo $message;
		        				}
		        			?>
		        		</div>
	        		</div>
				</form>
			</div>			
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Harian Poliklinik</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" 
	        		role="form" method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_sensus_harian_poli">
			        <div class="tabelinformasi" >
				        <div class="form-group">
		        			<label class="control-label col-md-2"> Tanggal</label>
		        			<div class="col-md-3">
								<div class="input-daterange input-group" id="datepicker">
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="start"  data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
								    <span class="input-group-addon">to</span>
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
								</div>
							</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_sensus">
		        					<option value="" selected>Pilih</option>
		        					<?php  
		        						foreach ($klinik as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        			<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
		        		
	        		</div>
				</form>
			</div>

			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Harian IGD</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_sensus_harian_igd">
			        <div class="tabelinformasi" >
				        <div class="form-group">
		        			<label class="control-label col-md-2"> Tanggal</label>
		        			<div class="col-md-3">
								<div class="input-daterange input-group" id="datepicker">
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="start"  data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
								    <span class="input-group-addon">to</span>
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
								</div>
							</div>

		        			<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
		        		
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Bulanan Poliklinik</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_rekap_bulanan_poli">
			        <div class="tabelinformasi" >
				        
		        		<div  class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_sensus">
		        					<option value="" selected>Pilih</option>
		        					<?php  
		        						foreach ($klinik as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        			
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
							<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
							
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Bulanan IGD</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_rekap_bulanan_igd">
			        <div class="tabelinformasi" >
				        
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
		        			<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Tahunan Poli</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_rekap_tahunan_poli">
			        <div class="tabelinformasi" >
			        	<div class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_sensus">
		        					<option value="" selected>Pilih</option>
		        					<?php  
		        						foreach ($klinik as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        		</div>
			        	<div class="form-group">
							<label class="control-label col-md-2">Tahun</label>
							<div class="col-md-2">
								<input type="text" data-date-format="yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="2" data-provide="datepicker" readonly value="<?php echo date("Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
		        	</div>
	        		
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Tahunan IGD</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_rekap_tahunan_igd">
			        <div class="tabelinformasi" >

		        		<div class="form-group">
		        			
							<label class="control-label col-md-2">Tahun</label>
							<div class="col-md-1">
								<input type="text" data-date-format="yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="2" data-provide="datepicker" readonly value="<?php echo date("Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Rekapitulasi Pasien Rawat Inap per Kecamatan</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" 
	        		role="form" method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_rekap_persebaran">
			        <div class="tabelinformasi" >
				        <div class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_rekap">
		        					<option value="" selected>Pilih</option>
		        					<?php  
		        						foreach ($rawat_inap as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        			
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
		        			<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Bulanan Pasien Rawat Inap</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_sensus_bulanan_ri">
			        <div class="tabelinformasi" >
				        
		        		<div class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_rekap">
		        					<option value="" selected>Pilih</option>
		        					<?php  
		        						foreach ($rawat_inap as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        			
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
		        			<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Bulanan Pasien Rawat Inap</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" 
	        		role="form" method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_laporan_bulanan_rawat_inap">
			        <div class="tabelinformasi" >
				        
		        		<div class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_rekap">
		        					<option selected>Pilih</option>
		        					<?php  
		        						foreach ($rawat_inap as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        			
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
		        			<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>

		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Jenis Penyakit</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_laporan_jenis_penyakit">
			        <div class="tabelinformasi" >
				        
		        		<div class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_rekap">
		        					<option selected>Pilih</option>
		        					<?php  
		        						foreach ($rawat_inap as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        			
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
		        			<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Harian Pasien Rawat Inap</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" 
	        		role="form" method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_sensus_harian_ri">
			        <div class="tabelinformasi" >
				        <div class="form-group">
		        			<label class="control-label col-md-2"> Tanggal</label>
		        			<div class="col-md-3">
								<div class="input-daterange input-group" id="datepicker">
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="start"  data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
								   <!--  <span class="input-group-addon">to</span>
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" /> -->
								</div>
							</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_rekap">
		        					<option selected>Pilih</option>
		        					<?php  
		        						foreach ($rawat_inap as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        			<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
		        		
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Kegiatan Kebidanan</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_kegiatan_kebinanan">
			        <div class="tabelinformasi" >
		        		<div class="form-group">
							<label class="control-label col-md-2">Tahun</label>
							<div class="col-md-1">
								<input type="text" data-date-format="yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="2" data-provide="datepicker" readonly value="<?php echo date("Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Kegiatan Perinatalogi</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" 
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_kegiatan_perinatalogi">
			        <div class="tabelinformasi" >
				        
		        		<div class="form-group">
		        			
							<label class="control-label col-md-2">Tahun</label>
							<div class="col-md-1">
								<input type="text" data-date-format="yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="2" data-provide="datepicker" readonly value="<?php echo date("Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Buku Register Pasien Rawat Inap</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_register_ri">
			        <div class="tabelinformasi" >
				        <div class="form-group">
		        			<label class="control-label col-md-2"> Tanggal</label>
		        			<div class="col-md-3">
								<div class="input-daterange input-group" id="datepicker">
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="start"  data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
								    <span class="input-group-addon">to</span>
								    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
								</div>
							</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-md-2">Unit</label>
		        			<div class="col-md-2">
		        				<select class="form-control" name="dept_rekap">
		        					<option selected>Pilih</option>
		        					<?php  
		        						foreach ($rawat_inap as $key) {
		        							echo '<option value="'.$key['dept_id'].'">'.$key['nama_dept'].'</option>';
		        						}
		        					?>
		        				</select>
		        			</div>
		        			<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
		        		
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Ketenagaan</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_ketenagaan">
			        <div class="tabelinformasi" >
				        
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
		        			<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button type="submit" class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Radiologi</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form">
			        <div class="tabelinformasi" >
				       
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
		        			<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly placeholder="<?php echo date("m/Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
			<div class="informasi" style="margin-left:30px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Laboratorium</div>
	        	<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form">
			        <div class="tabelinformasi" >
				        
		        		<div class="form-group">
		        			<label class="control-label col-md-2"> Bulan / Tahun</label>
		        			<div class="col-md-2">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly placeholder="<?php echo date("m/Y");?>" />
							</div>
							<div class="pull-right" style="margin-right:20px">
			        			<div class="col-md-3">
			        				<button class="btn btn-info">Simpan ke Excel(.xls)</button>
			        			</div>
		        			</div>
		        		</div>
	        		</div>
				</form>
			</div>
	        <br>
</div>
			