<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Laporan_Bulanan'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>LAPORAN BULANAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong><?php echo strtoupper($nama_dept) ?> RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Laporan per </strong> 
	<?php echo $periode; ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px" rowspan="2">No.</th>
					<th style="text-align:center;width:300px" rowspan="2">Uraian</th>
					<th style="text-align:center;width:300px" colspan="7">KELAS III</th>
					<th style="text-align:center;width:300px" colspan="7">KELAS II</th>
					<th style="text-align:center;width:300px" colspan="7">KELAS I</th>
					<th style="text-align:center;width:300px" colspan="7">KELAS UTAMA</th>
					<th style="text-align:center;width:300px" colspan="7">KELAS VIP</th>
					<th style="text-align:center;width:300px" rowspan="2">total</th>
				</tr>
				<tr class="header">
					<?php for ($i=0; $i < 5; $i++) { 
					echo '<th style="text-align:center;width:300px">Umum</th>
					<th style="text-align:center;width:300px">BPJS</th>
					<th style="text-align:center;width:300px">Jamkesmas</th>
					<th style="text-align:center;width:300px">Asuransi</th>
					<th style="text-align:center;width:300px">Kontrak</th>
					<th style="text-align:center;width:300px">Gratis</th>
					<th style="text-align:center;width:300px">Lainnya</th>';
					} ?>
				</tr>
			</thead>
			<tbody>
				<?php  
					echo '<tr><td>Pasien Masuk</td>';
					$tot = 0;
					foreach ($pasien_masuk as $key) {
						echo '<td>'.$key['umum'].'</td>';
						echo '<td>'.$key['bpjs'].'</td>';
						echo '<td>'.$key['jamkesmas'].'</td>';
						echo '<td>'.$key['asuransi'].'</td>';
						echo '<td>'.$key['kontrak'].'</td>';
						echo '<td>'.$key['gratis'].'</td>';
						echo '<td>'.$key['lain_lain'].'</td>';
						$tot += intval($key['total']);
					}
					echo '<td>'.$tot.'</td>';

				?>
			</tbody>
		</table>
	</div>

</body>
</html>
