<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'LAPORAN_JENIS_PENYAKIT'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>LAPORAN JENIS PENYAKIT</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong><?php echo strtoupper($nama_dept) ?> RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>laporan per </strong> 
	<?php echo $periode; ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px" rowspan="2">No.</th>
					<th style="text-align:center;width:300px" rowspan="2">jenis penyakit</th>
					<th style="text-align:center;width:300px" colspan="2">0 -6 hr</th>
					<th style="text-align:center;width:300px" colspan="2">7-28 hr</th>
					<th style="text-align:center;width:300px" colspan="2">28 hr - 1 th</th>
					<th style="text-align:center;width:300px" colspan="2">2 - 4 th</th>
					<th style="text-align:center;width:300px" colspan="2">5 - 14 th</th>
					<th style="text-align:center;width:300px" colspan="2">15 - 24 th</th>
					<th style="text-align:center;width:300px" colspan="2">25 - 44 th</th>
					<th style="text-align:center;width:300px" colspan="2">45 - 64 th</th>
					<th style="text-align:center;width:300px" rowspan="2">L</th>
					<th style="text-align:center;width:300px" rowspan="2">P</th>
					<th style="text-align:center;width:300px" rowspan="2">Jumlah</th>
				</tr>
				<tr>
					<?php  
						for ($i=0; $i < 8; $i++) { 
							echo '<th style="text-align:center;">L</th>
								<th style="text-align:center;">P</th>';
						}
					?>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($result as $value) {
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px">'.$value['diagnosis_nama'].'</td>'.
								'<td style="text-align:center;>'.$value['0_6_TH_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['0_6_TH_WANITA'].'</td>'.
								'<td style="text-align:center;>'.$value['6_28_HR_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['6_28_HR_WANITA'].'</td>'.
								'<td style="text-align:center;>'.$value['28_HR_1_TH_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['28_HR_1_TH_WANITA'].'</td>'.
								'<td style="text-align:center;>'.$value['1_4_TH_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['1_4_TH_WANITA'].'</td>'.
								'<td style="text-align:center;>'.$value['4_14_TH_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['4_14_TH_WANITA'].'</td>'.
								'<td style="text-align:center;>'.$value['14_24_TH_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['14_24_TH_WANITA'].'</td>'.
								'<td style="text-align:center;>'.$value['24_44_TH_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['24_44_TH_WANITA'].'</td>'.
								'<td style="text-align:center;>'.$value['44_64_TH_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['44_64_TH_WANITA'].'</td>'.
								'<td style="text-align:center;>'.$value['64_ATAS_TH_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['64_ATAS_TH_WANITA'].'</td>'.
								'<td style="text-align:center;>'.$value['JK_PRIA'].'</td>'.
								'<td style="text-align:center;>'.$value['JK_WANITA'].'</td>'.
								'<td style="text-align:center;>'.intval($value['JK_WANITA']) + intval($value['JK_PRIA']).'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>

</body>
</html>