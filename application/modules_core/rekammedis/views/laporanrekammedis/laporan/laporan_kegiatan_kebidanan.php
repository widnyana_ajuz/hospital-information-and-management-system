<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Laporan_Keg_Kebidanan'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>LAPORAN KEGIATAN KEBIDANAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Laporan per </strong> 
	<?php echo date('d F Y'); ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px" rowspan="3">No.</th>
					<th style="text-align:center;width:300px" rowspan="3">Jenis Kegiatan</th>
					<th style="text-align:center;width:300px" colspan="10">Rujukan</th>
					<th style="text-align:center;width:300px" colspan="3" rowspan="2">Non Rujukan</th>
					<th style="text-align:center;width:300px" colspan="2" rowspan="3">Dirujuk</th>
				</tr>
				<tr class="header">
					<th colspan="7" style="text-align:center;">Medis</th>
					<th colspan="3" style="text-align:center;">Non Medis</th>
				</tr>
				<tr class="header">
					<th style="text-align:center;">Rumah Sakit</th>
					<th style="text-align:center;">Bidan</th>
					<th style="text-align:center;">Puskesmas</th>
					<th style="text-align:center;">Faskes lainnya</th>
					<th style="text-align:center;">Hidup</th>
					<th style="text-align:center;">Mati</th>
					<th style="text-align:center;">Total</th>
					<th style="text-align:center;">Hidup</th>
					<th style="text-align:center;">Mati</th>
					<th style="text-align:center;">Total</th>
					<th style="text-align:center;">Hidup</th>
					<th style="text-align:center;">Mati</th>
					<th style="text-align:center;">Total</th>
				</tr>
			</thead>
			<tbody>

				<?php  
				//print_r($result);die;
					foreach ($result as $key) {
						echo '<tr>
							<td>'.$key['no'].'</td>
							<td>'.$key['nama_kegiatan'].'</td>
							<td>'.(!isset($key['RS']) ? '' : $key['RS']).'</td>
							<td>'.(!isset($key['BIDAN']) ? '' : $key['BIDAN']).'</td>
							<td>'.(!isset($key['PUSKESMAS']) ? '' : $key['PUSKESMAS']).'</td>
							<td>'.(!isset($key['FASKES']) ? '' : $key['FASKES']).'</td>
							<td>'.(!isset($key['MDS_HIDUP']) ? '' : $key['MDS_HIDUP']).'</td>
							<td>'.(!isset($key['MDS_MATI']) ? '' : $key['MDS_MATI']).'</td>
							<td></td>
							<td>'.(!isset($key['NON_MDK_HIDUP']) ? '' : $key['NON_MDK_HIDUP']).'</td>
							<td>'.(!isset($key['NON_MDK_MATI']) ? '' : $key['NON_MDK_MATI']).'</td>
							<td></td>
							<td>'.(!isset($key['NON_RJ_HIDUP']) ? '' : $key['NON_RJ_HIDUP']).'</td>
							<td>'.(!isset($key['NON_RJ_MATI']) ? '' : $key['NON_RJ_MATI']).'</td>
							<td></td>
							<td colspan="2">'.(!isset($key['DIRUJUK']) ? '' : $key['DIRUJUK']).'</td>
							</tr>';
					}
				?>
			</tbody>
		</table>
	</div>

</body>
</html>
