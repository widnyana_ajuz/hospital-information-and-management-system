<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Register_Rawat_Inap'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>REGISTER RAWAT INAP</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong><?php echo strtoupper($nama_dept) ?> RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Laporan per </strong> 
	<?php echo $start; ?> sampai <?php echo $end ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px" rowspan="3">No.</th>
					<th style="text-align:center;width:300px" rowspan="3">Tanggal</th>
					<th style="text-align:center;width:300px" rowspan="3">No RM</th>
					<th style="text-align:center;width:300px" rowspan="3">Nama</th>
					<th style="text-align:center;width:300px" rowspan="3">Alamat</th>
					<th style="text-align:center;width:300px" rowspan="3">Umur</th>
					<th style="text-align:center;width:300px" rowspan="3">Jenis Kelamin</th>
					<th style="text-align:center;width:300px" rowspan="3">Kelas Perawatan</th>
					<th style="text-align:center;width:300px" colspan="4">Cara Masuk</th>
					<th style="text-align:center;width:300px" colspan="2">Pindah Intern dari</th>
					<th style="text-align:center;width:300px" colspan="3">Pindah Intern ke</th>
					<th style="text-align:center;width:300px" rowspan="3">Waktu Keluar RS</th>
					<th style="text-align:center;width:300px" rowspan="3">Diagnosa Utama</th>
					<th style="text-align:center;width:300px" rowspan="3">Komplikasi</th>
					<th style="text-align:center;width:300px" rowspan="3">Sebab Luar Kecelakaan</th>
					<th style="text-align:center;width:300px" rowspan="3">Kode ICD X</th>
					<th style="text-align:center;width:300px" rowspan="3">Tindakan Operasi</th>
					<th style="text-align:center;width:300px" rowspan="3">Tanggal Tindakan Operasi</th>
					<th style="text-align:center;width:300px" rowspan="3">Dokter</th>
					<th style="text-align:center;width:300px" colspan="5">Cara Pasien Keluar</th>
					<th style="text-align:center;width:300px" colspan="4">Keadaan Pasien Keluar</th>
					<th style="text-align:center;width:300px" colspan="7">Cara bayar</th>
				</tr>
				<tr class="header">
					<th rowspan="2">IGD</th>
					<th rowspan="2">Dari RJ</th>
					<th rowspan="2">Dari RI</th>
					<th rowspan="2">Langsung RI</th>
					<th rowspan="2">Ruangan</th>
					<th rowspan="2">Kelas</th>
					<th rowspan="2">Ruangan</th>
					<th rowspan="2">Kelas</th>
					<th rowspan="2">Tanggal</th>
					<th rowspan="2">Pulang</th>
					<th colspan="2">Dirujuk</th>
					<th rowspan="2">Pulang Paksa</th>
					<th rowspan="2">Lain-lain</th>
					<th colspan="2">Hidup</th>
					<th colspan="2">Mati</th>
					<th rowspan="2">Umum</th>
					<th rowspan="2">BPJS</th>
					<th rowspan="2">Jamkesmas</th>
					<th rowspan="2">Asuransi</th>
					<th rowspan="2">Kontrak</th>
					<th rowspan="2">Gratis</th>
					<th rowspan="2">Lainnya</th>
				</tr>
				<tr class="header">
					<th>Ke Rs yg lebih tinggi</th>
					<th>Pindah RS Lain</th>
					<th>Sembuh</th>
					<th>Blm Sembuh</th>
					<th> < 48 jam</th>
					<th> > 48 jam</th>
				</tr>
				
			</thead>
			<tbody>
				<?php  
				$i = 0;
					foreach ($result as $key) {
						echo '<tr><td>'.(++$i).'</td>'.
								'<td>'.$key['waktu_masuk'].'</td>'.
								'<td>'.$key['rm_id'].'</td>'.
								'<td>'.$key['nama'].'</td>'.
								'<td>'.$key['alamat_skr'].'</td>'.
								'<td>'.$key['tanggal_lahir'].'</td>'.
								'<td>'.$key['jenis_kelamin'].'</td>'.
								'<td></td>'.
								'<td>'.$key['dari_igd'].'</td>'.
								'<td>'.$key['dari_rj'].'</td>'.
								'<td>'.$key['dari_ri'].'</td>'.
								'<td>'.$key['langsung_ri'].'</td>'.
								'<td>'.$key['dept_asal'].'</td>'.
								'<td>'.$key['kelas_asal'].'</td>'.
								'<td>'.$key['dept_tujuan'].'</td>'.
								'<td>'.$key['kelas_pindah'].'</td>'.
								'<td>'.$key['waktu_pindah'].'</td>'.
								'<td>'.$key['waktu_keluar'].'</td>'.
								'<td>'.$key['diagnosis_nama'].'</td>'.
								'<td></td>'.
								'<td></td>'.
								'<td>'.$key['diagnosa_utama'].'</td>'.
								'<td></td>'.
								'<td></td>'.
								'<td>'.$key['dokter'].'</td>'.
								'<td>'.$key['pulang'].'</td>'.
								'<td></td>'.
								'<td>'.$key['dirujuk'].'</td>'.
								'<td>'.$key['aps'].'</td>'.
								'<td>'.$key['sembuh'].'</td>'.
								'<td></td>'.
								'<td>'.$key['belum_sembuh'].'</td>'.
								'<td>'.$key['MATI_KR_48_JAM'].'</td>'.
								'<td>'.$key['MATI_LBH_48_JAM'].'</td>'.
								'<td>'.$key['umum'].'</td>'.
								'<td>'.$key['bpjs'].'</td>'.
								'<td>'.$key['jamkesmas'].'</td>'.
								'<td>'.$key['asuransi'].'</td>'.
								'<td>'.$key['kontrak'].'</td>'.
								'<td>'.$key['gratis'].'</td>'.
								'<td>'.$key['lain_lain'].'</td></tr>';

					}
				?>
			</tbody>
		</table>
	</div>

</body>
</html>
