<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'REKAPITULASI_HARIAN RAWAT_INAP'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>REKAPITULASI HARIAN PASIEN RAWAT INAP</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong><?php //echo strtoupper($nama_dept) ?> RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>laporan per </strong> 
	<?php //echo $periode; ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px" rowspan="2">No.</th>
					<th style="text-align:center;width:300px" rowspan="2">Pasien Awal</th>
					<th style="text-align:center;width:300px" rowspan="2">Pasien Masuk</th>
					<th style="text-align:center;width:300px" rowspan="2">Pasien Pindahan</th>
					<th style="text-align:center;width:300px" rowspan="2">Jumlah</th>
					<th style="text-align:center;width:300px" rowspan="2">Pasien Dipindahkan</th>
					<th style="text-align:center;width:300px" rowspan="2">Pasien Keluar Hidup</th>
					<th style="text-align:center;width:300px" rowspan="2">Jumlah</th>
					<th style="text-align:center;width:300px" colspan="2">Perincian Pasien Mati</th>
					<th style="text-align:center;width:300px" rowspan="2">Pasien Mati</th>
					<th style="text-align:center;width:300px" rowspan="2">Pasien Sisa</th>
					<th style="text-align:center;width:300px" colspan="5">Perincian Pasien Sisa</th>
					<th style="text-align:center;width:300px" colspan="7">Pembiayaan</th>
				</tr>
				<tr class="header">
					<th style="text-align:center;"> < 48 jam</th>
					<th style="text-align:center;"> > 48 jam</th>
					<th style="text-align:center;"> Kelas III</th>
					<th style="text-align:center;"> Kelas II</th>
					<th style="text-align:center;"> Kelas I</th>
					<th style="text-align:center;"> Utama</th>
					<th style="text-align:center;"> VIP</th>
					<th style="text-align:center;"> Umum</th>
					<th style="text-align:center;"> BPJS</th>
					<th style="text-align:center;"> JamKesmas</th>
					<th style="text-align:center;"> Asuransi</th>
					<th style="text-align:center;"> Kontrak</th>
					<th style="text-align:center;"> Gratis</th>
					<th style="text-align:center;"> Lainnya</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>

</body>
</html>