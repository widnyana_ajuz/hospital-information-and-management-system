<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Sensus_Bulanan_POLI_'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>SENSUS BULANAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong> <?php echo $nama_dept; ?> RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>sensus BULANAN</strong> 
	<?php echo $periode; ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px">Uraian</th>
					<th style="text-align:center;width:300px" colspan="2">Baru</th>
					<th style="text-align:center;width:300px" colspan="2">Lama</th>
					<th style="text-align:center;width:300px" colspan="2">Total</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($kunjungan as $value) {
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px">Kunjungan</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['baru'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['lama'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['total'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>

	<br>
	<b>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px" colspan="2">Tanpa Rujukan</th>
					<th style="text-align:center;width:300px" colspan="2">Rujukan Puskesmas</th>
					<th style="text-align:center;width:300px" colspan="2">Rujukan Rs</th>
					<th style="text-align:center;width:300px" colspan="2">Rujukan Instansi Lain</th>
					<th style="text-align:center;width:300px" colspan="2">Rujukan Dokter</th>
					<th style="text-align:center;width:300px" colspan="2">Total</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					$value = $rujukan;
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['tanpa_rujukan'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['puskesmas'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['rujuk_rs'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['instansi'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['rujuk_dokter'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['total'].'</td>'.
							'</tr>';
					
				?>	
			</tbody>
		</table>
	</div>

	<br>
	<b>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px" colspan="2">Dirawat</th>
					<th style="text-align:center;width:300px" colspan="2">Dirujuk</th>
					<th style="text-align:center;width:300px" colspan="2">Dipulangkan</th>
					<th style="text-align:center;width:300px" colspan="2">Pulang atas Permintaan Sendiri</th>
					<th style="text-align:center;width:300px" colspan="2">Meninggal sblm dirawat</th>
					<th style="text-align:center;width:300px" colspan="2">Meninggal sesdh dirawat</th>
					<th style="text-align:center;width:300px" colspan="2">Total</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					$value = $resume_pulang;
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['rawat'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['rujuk'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['dipulangkan'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['aps'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['sebelum_rawat'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['setelah_rawat'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['total'].'</td>'.
							'</tr>';
					
				?>	
			</tbody>
		</table>
	</div>

	<br>
	<b>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px" colspan="2">Umum</th>
					<th style="text-align:center;width:300px" colspan="2">BPJS</th>
					<th style="text-align:center;width:300px" colspan="2">JamKesmas</th>
					<th style="text-align:center;width:300px" colspan="2">Asuransi</th>
					<th style="text-align:center;width:300px" colspan="2">Kontrak</th>
					<th style="text-align:center;width:300px" colspan="2">Gratis</th>
					<th style="text-align:center;width:300px" colspan="2">Lainnya</th>
					<th style="text-align:center;width:300px" colspan="2">Total</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					$value = $cara_bayar;
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['umum'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['bpjs'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jamkesmas'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['asuransi'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['kontrak'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['gratis'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['lain_lain'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['total'].'</td>'.
							'</tr>';
					
				?>	
			</tbody>
		</table>
	</div>

	<br>
	<b>
</body>
</html>