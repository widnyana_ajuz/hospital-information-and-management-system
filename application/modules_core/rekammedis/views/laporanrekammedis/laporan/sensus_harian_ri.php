<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'SENSUS_HARIAN_RAWAT_INAP'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>SENSUS HARIAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong><?php echo strtoupper($nama_dept) ?> RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>laporan per </strong> 
	<?php echo $start; ?>
	<br/>
	<br/>

	<?php echo "<center>Pasien Masuk</center>"; ?>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px" >No.</th>
					<th style="text-align:center;width:300px" colspan="2">Nama Pasien</th>
					<th style="text-align:center;width:300px" colspan="2">Umur</th>
					<th style="text-align:center;width:300px" colspan="2">Jenis Kelamin</th>
					<th style="text-align:center;width:300px" colspan="2">Kelas</th>
					<th style="text-align:center;width:300px" colspan="2">Rujukan dari</th>
					<th style="text-align:center;width:300px" colspan="2">Cara Bayar</th>
					<th style="text-align:center;width:300px" colspan="2">Dokter yang merawat</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($result as $value) {
						$datetime1 = new DateTime();
						$datetime2 = new DateTime($value['tanggal_lahir']);
						$interval = $datetime1->diff($datetime2);
						$umur = ''						;
						if($interval->y > 0)
							$umur = $interval->y ." tahun ";
						else if($interval->m > 0)
							$umur = $interval->m." bulan ";
						else if($interval->d > 0)
							$umur = $interval->d ." hari";

						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nama'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$umur.'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jenis_kelamin'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['kelas_kamar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['cara_masuk'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['cara_bayar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nama_petugas'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>
	<br><br>

	<?php echo "<center>Pasien Pindahan dari ruangan lain</center>"; ?>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px" >No.</th>
					<th style="text-align:center;width:300px" colspan="2">Nama Pasien</th>
					<th style="text-align:center;width:300px" colspan="2">Umur</th>
					<th style="text-align:center;width:300px" colspan="2">Jenis Kelamin</th>
					<th style="text-align:center;width:300px" colspan="2">Kelas</th>
					<th style="text-align:center;width:300px" colspan="2">Rujukan dari</th>
					<th style="text-align:center;width:300px" colspan="2">Cara Bayar</th>
					<th style="text-align:center;width:300px" colspan="2">Dokter yang merawat</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($trans_masuk as $value) {
						$datetime1 = new DateTime();
						$datetime2 = new DateTime($value['tanggal_lahir']);
						$interval = $datetime1->diff($datetime2);
						$umur = ''						;
						if($interval->y > 0)
							$umur = $interval->y ." tahun ";
						else if($interval->m > 0)
							$umur = $interval->m." bulan ";
						else if($interval->d > 0)
							$umur = $interval->d ." hari";

						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nama'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$umur.'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jenis_kelamin'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['kelas_kamar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['cara_masuk'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['cara_bayar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nama_petugas'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>
	<br><br>

	<?php echo "<center>Pasien Pindahan Ke ruangan lain</center>"; ?>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px" >No.</th>
					<th style="text-align:center;width:300px" colspan="2">Nama Pasien</th>
					<th style="text-align:center;width:300px" colspan="2">Umur</th>
					<th style="text-align:center;width:300px" colspan="2">Jenis Kelamin</th>
					<th style="text-align:center;width:300px" colspan="2">Kelas</th>
					<th style="text-align:center;width:300px" colspan="2">Dokter yang merawat</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($trans_keluar as $value) {
						$datetime1 = new DateTime();
						$datetime2 = new DateTime($value['tanggal_lahir']);
						$interval = $datetime1->diff($datetime2);
						$umur = ''						;
						if($interval->y > 0)
							$umur = $interval->y ." tahun ";
						else if($interval->m > 0)
							$umur = $interval->m." bulan ";
						else if($interval->d > 0)
							$umur = $interval->d ." hari";

						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nama'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$umur.'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jenis_kelamin'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['kelas_kamar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nama_petugas'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>
	<br><br>

	<?php echo "<center>Pasien Keluar Rumah Sakit</center>"; ?>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px" rowspan="2">No.</th>
					<th style="text-align:center;width:300px" colspan="2" rowspan="2">Nama Pasien</th>
					<th style="text-align:center;width:300px" rowspan="2">Tanggal Masuk</th>
					<th style="text-align:center;width:300px" colspan="4">Cara Keluar Hidup</th>
					<th style="text-align:center;width:300px" colspan="2">Mati</th>
				</tr>
				<tr>
					<th style="text-align:center;">Pulang Sembuh</th>
					<th style="text-align:center;">Dirujuk ke Atas</th>
					<th style="text-align:center;">APS</th>
					<th style="text-align:center;">Rujuk RS Lain</th>
					<th style="text-align:center;">Sebelum 48 jam</th>
					<th style="text-align:center;">Setelah 48 jam</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($pulang as $value) {
						$tgl = date_create($value['waktu_masuk']);
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nama'].'</td>'.
								'<td style="text-align:center;width:300px">'.$tgl->format('d F Y').'</td>'.
								'<td style="text-align:center;width:300px">'.$value['pulang_sembuh'].'</td>'.
								'<td style="text-align:center;width:300px">'.$value['pindah'].'</td>'.
								'<td style="text-align:center;width:300px">'.$value['rujuk_rs'].'</td>'.
								'<td style="text-align:center;width:300px">'.$value['aps'].'</td>'.
								'<td style="text-align:center;width:300px">'.$value['mati_kurang_48'].'</td>'.
								'<td style="text-align:center;width:300px">'.$value['mati_lebih_48'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>
	<br><br>
</body>
</html>