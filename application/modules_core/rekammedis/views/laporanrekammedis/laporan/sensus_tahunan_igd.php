<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Sensus_Tahunan_IGD_'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>SENSUS TAHUNAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong> IGD RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>sensus Tahunan igd tahun </strong> 
	<?php echo $tahun; ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px">Variabel Kegiatan</th>
					<th style="text-align:center;width:300px" colspan="2">Januari</th>
					<th style="text-align:center;width:300px" colspan="2">Februari</th>
					<th style="text-align:center;width:300px" colspan="2">Maret</th>
					<th style="text-align:center;width:300px" colspan="2">April</th>
					<th style="text-align:center;width:300px" colspan="2">Mei</th>
					<th style="text-align:center;width:300px" colspan="2">Juni</th>
					<th style="text-align:center;width:300px" colspan="2">Juli</th>
					<th style="text-align:center;width:300px" colspan="2">Agustus</th>
					<th style="text-align:center;width:300px" colspan="2">September</th>
					<th style="text-align:center;width:300px" colspan="2">Oktober</th>
					<th style="text-align:center;width:300px" colspan="2">November</th>
					<th style="text-align:center;width:300px" colspan="2">Desember</th>
					<th style="text-align:center;width:300px" colspan="2">Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($kunjungan as $value) {
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px">KUNJUNGAN '.$value['jenis_kunjungan'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jan'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['feb'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['apr'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mei'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jun'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jul'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['ags'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['sep'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['okt'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nov'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['des'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['total'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>

	<br>
	<b>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px">Variabel Kegiatan</th>
					<th style="text-align:center;width:300px" colspan="2">Januari</th>
					<th style="text-align:center;width:300px" colspan="2">Februari</th>
					<th style="text-align:center;width:300px" colspan="2">Maret</th>
					<th style="text-align:center;width:300px" colspan="2">April</th>
					<th style="text-align:center;width:300px" colspan="2">Mei</th>
					<th style="text-align:center;width:300px" colspan="2">Juni</th>
					<th style="text-align:center;width:300px" colspan="2">Juli</th>
					<th style="text-align:center;width:300px" colspan="2">Agustus</th>
					<th style="text-align:center;width:300px" colspan="2">September</th>
					<th style="text-align:center;width:300px" colspan="2">Oktober</th>
					<th style="text-align:center;width:300px" colspan="2">November</th>
					<th style="text-align:center;width:300px" colspan="2">Desember</th>
					<th style="text-align:center;width:300px" colspan="2">Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($pelayanan as $value) {
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px">'.$value['jenis'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jan'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['feb'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['apr'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mei'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jun'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jul'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['ags'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['sep'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['okt'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nov'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['des'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['total'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>

	<br>
	<b>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px">Variabel Kegiatan</th>
					<th style="text-align:center;width:300px" colspan="2">Januari</th>
					<th style="text-align:center;width:300px" colspan="2">Februari</th>
					<th style="text-align:center;width:300px" colspan="2">Maret</th>
					<th style="text-align:center;width:300px" colspan="2">April</th>
					<th style="text-align:center;width:300px" colspan="2">Mei</th>
					<th style="text-align:center;width:300px" colspan="2">Juni</th>
					<th style="text-align:center;width:300px" colspan="2">Juli</th>
					<th style="text-align:center;width:300px" colspan="2">Agustus</th>
					<th style="text-align:center;width:300px" colspan="2">September</th>
					<th style="text-align:center;width:300px" colspan="2">Oktober</th>
					<th style="text-align:center;width:300px" colspan="2">November</th>
					<th style="text-align:center;width:300px" colspan="2">Desember</th>
					<th style="text-align:center;width:300px" colspan="2">Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($cara_bayar as $value) {
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px">'.$value['cara_bayar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jan'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['feb'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['apr'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mei'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jun'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jul'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['ags'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['sep'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['okt'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nov'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['des'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['total'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>

	<br>
	<b>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px">Variabel Kegiatan</th>
					<th style="text-align:center;width:300px" colspan="2">Januari</th>
					<th style="text-align:center;width:300px" colspan="2">Februari</th>
					<th style="text-align:center;width:300px" colspan="2">Maret</th>
					<th style="text-align:center;width:300px" colspan="2">April</th>
					<th style="text-align:center;width:300px" colspan="2">Mei</th>
					<th style="text-align:center;width:300px" colspan="2">Juni</th>
					<th style="text-align:center;width:300px" colspan="2">Juli</th>
					<th style="text-align:center;width:300px" colspan="2">Agustus</th>
					<th style="text-align:center;width:300px" colspan="2">September</th>
					<th style="text-align:center;width:300px" colspan="2">Oktober</th>
					<th style="text-align:center;width:300px" colspan="2">November</th>
					<th style="text-align:center;width:300px" colspan="2">Desember</th>
					<th style="text-align:center;width:300px" colspan="2">Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($resume_pulang as $value) {
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px">'.$value['alasan'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jan'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['feb'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['apr'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mei'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jun'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jul'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['ags'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['sep'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['okt'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nov'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['des'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['total'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>

	<br>
	<b>
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px">Variabel Kegiatan</th>
					<th style="text-align:center;width:300px" colspan="2">Januari</th>
					<th style="text-align:center;width:300px" colspan="2">Februari</th>
					<th style="text-align:center;width:300px" colspan="2">Maret</th>
					<th style="text-align:center;width:300px" colspan="2">April</th>
					<th style="text-align:center;width:300px" colspan="2">Mei</th>
					<th style="text-align:center;width:300px" colspan="2">Juni</th>
					<th style="text-align:center;width:300px" colspan="2">Juli</th>
					<th style="text-align:center;width:300px" colspan="2">Agustus</th>
					<th style="text-align:center;width:300px" colspan="2">September</th>
					<th style="text-align:center;width:300px" colspan="2">Oktober</th>
					<th style="text-align:center;width:300px" colspan="2">November</th>
					<th style="text-align:center;width:300px" colspan="2">Desember</th>
					<th style="text-align:center;width:300px" colspan="2">Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					foreach ($rujukan as $value) {
						echo '<tr class="body">'.
								'<td style="text-align:center;width:30px">'.(++$i).'</td>'.
								'<td style="text-align:center;width:300px">'.$value['cara_masuk'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jan'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['feb'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mar'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['apr'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['mei'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jun'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['jul'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['ags'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['sep'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['okt'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['nov'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['des'].'</td>'.
								'<td style="text-align:center;width:300px" colspan="2">'.$value['total'].'</td>'.
							'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>
</body>
</html>