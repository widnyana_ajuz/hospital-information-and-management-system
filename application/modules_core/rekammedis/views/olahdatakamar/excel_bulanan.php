<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Laporan_Harian'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>LAPORAN KAMAR BULANAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong><?php echo strtoupper($nama_dept) ?> RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Laporan per </strong> 
	<?php echo $periode; ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="width:3%" colspan="2">No</th>
					<th style="text-align:center;width:30px" colspan="2">Unit</th>
					<th style="text-align:center;width:30px" colspan="2">Nama Kamar</th>
					<th style="text-align:center;width:30px" colspan="2">Jumlah Bed</th>
					<th style="text-align:center;width:30px" colspan="2">Tersedia</th>
					<th style="text-align:center;width:30px" colspan="2">Terisi</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					if (isset($kamar_bulanan) and !empty($kamar_bulanan)) {
						$i = 0;
						foreach ($kamar_bulanan as $kamar) {
							echo '<tr>
									<td style="text-align:center;width:3%" colspan="2">'.(++$i).'</td>
									<td style="text-align:center;width:30px" colspan="2">'.$kamar['nama_dept'].'</td>
									<td style="text-align:center;width:30px" colspan="2">'.$kamar['nama_kamar'].'</td>
									<td style="text-align:center;width:30px" colspan="2">'.$kamar['jlh_bed'].'</td>
									<td style="text-align:center;width:30px" colspan="2">'.(intval($kamar['jlh_bed']) - intval($kamar['terisi'])).'</td>
									<td style="text-align:center;width:30px" colspan="2">'.$kamar['terisi'].'</td>
								</tr>';
						}
					}
				?>
			</tbody>
		</table>
	</div>

</body>
</html>
