<br>
<div class="title">
<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>olahdatakamar/home">REKAM MEDIS OLAH DATA KAMAR</a>
			
		</li>
</div>
<div class="navigation1" style="min-height:800px;border-radius:5px; margin-left: 10px;margin-right: 10px;" >
 			<div style="padding-top:10px"></div>
						
			<div class="dropdown" style="margin-left:10px;width:98.5%;">
	            <div id="titleInformasi" style=" color:white">Data Kamar Harian</div>
	            <div id="btnBawahDataKaryawan" class="btnBawah" style="color:white"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			</div>
			<form class="form-horizontal" role="form" method="post" action="<?php echo base_url() ?>rekammedis/homeolahdatakamar/excel_harian">
				<div class="informasi">
	        		<div class="form-group">
	        			<label class="control-label col-md-1"> Tanggal</label>
	        			<div class="col-md-3">
							<div class="input-daterange input-group" id="datepicker">
							    <input type="text" id="tanggal" style="cursor:pointer;" class="form-control" name="start"  data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
							</div>
						</div>
	        		</div>
	        		<div class="form-group">
	        			<label class="control-label col-md-1">Unit</label>
	        			<div class="col-md-3">
	        				<select class="form-control" name="selectunitharian" id="selectunitharian">
	        					<option value="" selected>SEMUA</option>
	        					<?php  
	        						foreach ($all_unit_inap as $value) {
	        							echo '<option value="'.$value['dept_id'].'">'.$value['nama_dept'].'</option>';
	        						}
	        					?>
	        				</select>
	        			</div>
	        			<div class="col-md-5">
	        				<button type="button" id="filter_harian" class="btn btn-info">Cari</button>
	        			</div>
	        		</div>
		        <br>
		        </div>
				<hr class="garis"><br>
	            <div class="tabelinformasi">
			       	<div class="portlet box red">
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama display" cellspacing="0" width="100%" id="tabelkamarharian">
								<thead>
									<tr class="info">
										<th style="width:3%">No</th>
										<th >Unit</th>
										<th >Nama Kamar</th>
										<th >Jumlah Bed</th>
										<th >Tersedia</th>
										<th >Terisi</th>
									</tr>
								</thead>
								<tbody >
									<?php  
										if (isset($kamar_harian) and !empty($kamar_harian)) {
											$i = 0;
											foreach ($kamar_harian as $kamar) {
												echo '<tr>
														<td>'.(++$i).'</td>
														<td>'.$kamar['nama_dept'].'</td>
														<td>'.$kamar['nama_kamar'].'</td>
														<td>'.$kamar['jlh_bed'].'</td>
														<td>'.(intval($kamar['jlh_bed']) - intval($kamar['terisi'])).'</td>
														<td>'.$kamar['terisi'].'</td>
													</tr>';
											}
										}
									?>
								</tbody>
							</table>
						</div>		

						<button class="btn btn-info" type="submit" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button> 	
					</div>  
				</div>
			</form>
			<div class="dropdown" style="margin-left:10px;width:98.5%;">
	            <div id="titleInformasi" style=" color:white">Data Kamar Bulanan</div>
	            <div id="btnBawahDataKaryawan" class="btnBawah" style="color:white"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			</div>
			<form class="form-horizontal" role="form" method="post" action="<?php echo base_url() ?>rekammedis/homeolahdatakamar/excel_bulanan">
				<div class="informasi">
	        		<div class="form-group">
	        			<label class="control-label col-md-2"> Bulan / Tahun</label>
	        			<div class="col-md-2">
	        				<input type="text" id="bulan" data-date-format="mm/yyyy" style="cursor:pointer;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1"  data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label class="control-label col-md-2">Unit</label>
	        			<div class="col-md-3">
	        				<select class="form-control" name="selectunitbulanan" id="selectunitbulanan">
	        					<option value="" selected>SEMUA</option>
	        					<?php  
	        						foreach ($all_unit_inap as $value) {
	        							echo '<option value="'.$value['dept_id'].'">'.$value['nama_dept'].'</option>';
	        						}
	        					?>
	        				</select>
	        			</div>
	        			<div class="col-md-5">
	        				<button type="button" id="filter_bulanan" class="btn btn-info">Cari</button>
	        			</div>
	        		</div>
		        <br>
		        </div>
				<hr class="garis"><br>
	            <div class="tabelinformasi">
			       	<div class="portlet box red">
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama display" cellspacing="0" width="100%" id="tabelkamarbulanan">
								<thead>
									<tr class="info">
										<th style="width:3%">No</th>
										<th >Unit</th>
										<th >Nama Kamar</th>
										<th >Jumlah Bed</th>
										<th >Tersedia</th>
										<th >Terisi</th>

																			
									</tr>
									
								</thead>
								<tbody >
									<?php  
										if (isset($kamar_bulanan) and !empty($kamar_bulanan)) {
											$i = 0;
											foreach ($kamar_bulanan as $kamar) {
												echo '<tr>
														<td>'.(++$i).'</td>
														<td>'.$kamar['nama_dept'].'</td>
														<td>'.$kamar['nama_kamar'].'</td>
														<td>'.$kamar['jlh_bed'].'</td>
														<td>'.(intval($kamar['jlh_bed']) - intval($kamar['terisi'])).'</td>
														<td>'.$kamar['terisi'].'</td>
													</tr>';
											}
										}
									?>
								</tbody>
							</table>
						</div>		

						<button class="btn btn-info" type="submit" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button> 	
					</div>  
				</div>
			</form>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$('#filter_harian').on('click', 
			function (e) {
			e.preventDefault();
			var item = {};
			item['tanggal'] = format_date3($('#tanggal').val());
			item['unit'] = $('#selectunitharian').val();
			//if ($('#selectunitharian').val() == '') {return false;};
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url() ?>rekammedis/homeolahdatakamar/search_harian',
				success: function (data) {
					console.log(data);
					var t = $('#tabelkamarharian').DataTable();
					t.clear().draw();
					for (var i = 0; i < data.length; i++) {
						t.row.add([
							Number(i+1),
							data[i]['nama_dept'],
							data[i]['nama_kamar'],
							data[i]['jlh_bed'],
							(Number(data[i]['jlh_bed']) - Number(data[i]['terisi'])),
							data[i]['terisi']
						]).draw();
					};

				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#filter_bulanan').on('click', function (e) {
			e.preventDefault();
			var item = {};
			var bln = $('#bulan').val().split('/')
			item['bulan'] = bln[1] + "-" + bln[0] ;
			item['unit'] = $('#selectunitbulanan').val();
			//if ($('#selectunitbulanan').val() == '') {return false;};
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url() ?>rekammedis/homeolahdatakamar/search_bulanan',
				success: function (data) {
					console.log(data);
					var t = $('#tabelkamarbulanan').DataTable();
					t.clear().draw();
					for (var i = 0; i < data.length; i++) {
						t.row.add([
							Number(i+1),
							data[i]['nama_dept'],
							data[i]['nama_kamar'],
							data[i]['jlh_bed'],
							(Number(data[i]['jlh_bed']) - Number(data[i]['terisi'])),
							data[i]['terisi']
						]).draw();
					};
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
	})
</script>
			