<?php
$title = 'paramedis';

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="10" style="text-align:center;border-bottom:none;"><strong>DATA <?php echo $jenis; ?></strong></td>
		</tr>
		<tr>
			<td colspan="10" style="text-align:center;border-bottom:none;"><strong> RS DATU SANGGUL RANTAU</strong></td>
		</tr>
	</table>

	<br/>
	<br/>

	<!-- Hasil Evaluasi Kelas -->
	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="info">
					<th style="text-align:center;width:20px;">No</th>
					<th>Nama Lengkap</th>
					<th>NIP</th>
					<th>Kategori</th>
					<th>Kualifikasi Pendidikan</th>
					<th>Unit</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					if (isset($dokter) and !empty($dokter)) {
						$i = 0;
						foreach ($dokter as $value) {
							echo '<tr><td>'.(++$i).'</td>
								<td>'.$value['nama_petugas'].'</td>
								<td>'.$value['nip'].'</td>
								<td>'.$value['jenis'].'</td>
								<td>'.$value['kualifikasi'].'</td>
								<td>'.$value['nama_dept'].'</td></tr>';
						}
					}
				?>
			</tbody>
		</table>
	</div>
</body>
</html>