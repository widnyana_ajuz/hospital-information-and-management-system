<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'ISO_RekamMedis'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>ISO REKAM MEDIK</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Laporan per </strong> 
	<?php echo $mulai; ?> sampai <?php echo $akhir ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:20px;">No.</th>
					<th style="text-align:center;width:300px" colspan="2">Waktu Masuk</th>
					<th style="text-align:center;width:300px" colspan="2">No Rekam Medis</th>
					<th style="text-align:center;width:300px" colspan="2">Nama Pasien</th>
					<th style="text-align:center;width:300px" colspan="2">Waktu Bayar</th>
					<th style="text-align:center;width:300px" colspan="2">Cara Bayar</th>
					<th style="text-align:center;width:300px" colspan="2">Pasien Baru/Lama</th>
					<!-- <th>Diagnosa</th> -->
					<th style="text-align:center;width:300px" colspan="2">Unit Masuk</th>
					<th style="text-align:center;width:300px" colspan="2">Unit Keluar</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					$i = 0;
					if (!empty($iso_rm)) {
						foreach ($iso_rm as $key) {
							echo '<tr>
									<td style="text-align:center;width:20px;">'.(++$i).'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['tanggal_visit'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['rm_id'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['nama'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['waktu_keluar'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['cara_bayar'].'</td>
									<td style="text-align:center;width:300px" colspan="2"></td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['dept_in'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['dept_out'].'</td>
								</tr>';
						}
					}
				?>
			</tbody>
		</table>
	</div>

</body>
</html>
