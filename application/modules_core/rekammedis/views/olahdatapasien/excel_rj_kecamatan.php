<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Laporan_Per_Kecamatan'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>REKAP POLI PER KECAMATAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Laporan per </strong> 
	<?php echo $periode; ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px" colspan="2">Poliklinik</th>
					<th style="text-align:center;width:300px" colspan="2">L</th>
					<th style="text-align:center;width:300px" colspan="2">P</th>
					<th style="text-align:center;width:300px" colspan="2">Bakarangan</th>
					<th style="text-align:center;width:300px" colspan="2">Binuang</th>
					<th style="text-align:center;width:300px" colspan="2">Bungur</th>
					<th style="text-align:center;width:300px" colspan="2">Candi LRS Selatan</th>
					<th style="text-align:center;width:300px" colspan="2">Candi LRS Utara</th>
					<th style="text-align:center;width:300px" colspan="2">Hatungun</th>
					<th style="text-align:center;width:300px" colspan="2">Lokpaikat</th>
					<th style="text-align:center;width:300px" colspan="2">Piani</th>
					<th style="text-align:center;width:300px" colspan="2">Salam babaris</th>
					<th style="text-align:center;width:300px" colspan="2">Tapin Selatan</th>
					<th style="text-align:center;width:300px" colspan="2">Tapin Utara</th>
					<th style="text-align:center;width:300px" colspan="2">Tapin Tengah</th>
					<th style="text-align:center;width:300px" colspan="2">Lain - lain</th>
					<th style="text-align:center;width:300px" colspan="2">Total</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					if (!empty($result)) {
						$i = 0;
						foreach ($result as $key) {
							echo '<tr>
									<td style="text-align:center;width:30px">'.(++$i).'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['nama_dept'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['laki'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['perempuan'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['bakarangan'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['binuang'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['bungur'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['candilarasselatan'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['candilarasutara'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['hatungun'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['lokpaikat'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['piani'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['salambabaris'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['tapinselatan'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['tapinutara'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['tapintengah'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['lain'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['total'].'</td>
								</tr>';
						}
					}
				?>
			</tbody>
		</table>
	</div>

</body>
</html>
