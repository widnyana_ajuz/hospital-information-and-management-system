<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Laporan_Status_Pulang'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>REKAP STATUS PULANG</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong><?php echo $nama_dept ?> RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Laporan per </strong> 
	<?php echo $periode; ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:30px">No.</th>
					<th style="text-align:center;width:300px" colspan="2">Tanggal</th>
					<th style="text-align:center;width:300px" colspan="2">Belum Pulang</th>
					<th style="text-align:center;width:300px" colspan="2">Pulang</th>
					<th style="text-align:center;width:300px" colspan="2">Rujuk IGD</th>
					<th style="text-align:center;width:300px" colspan="2">Rujuk Ranap</th>
					<th style="text-align:center;width:300px" colspan="2">Pasien Dipulangkan</th>
					<th style="text-align:center;width:300px" colspan="2">Pasien Pindah Poli Lain</th>
					<th style="text-align:center;width:300px" colspan="2">APS</th>
					<th style="text-align:center;width:300px" colspan="2">Rujuk Rumah Sakit Lain</th>
					<th style="text-align:center;width:300px" colspan="2">Pasien Meninggal</th>
				</tr>
			</thead>
			<tbody>
				<?php  
						if (!empty($key)) {
							echo '<tr>
									<td style="text-align:center;width:300px">1</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['tanggal'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['belum_pulang'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['sudah_pulang'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['rujuk_igd'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['rujuk_ri'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['dipulangkan'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['pindah_poli'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['aps'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['rujuk_rs'].'</td>
									<td style="text-align:center;width:300px" colspan="2">'.$key['mati'].'</td>
								</tr>';
						}
				?>
			</tbody>
		</table>
	</div>

</body>
</html>
