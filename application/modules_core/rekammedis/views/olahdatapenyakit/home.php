<br>
<div class="title">
<li style="list-style: none">
			<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
			<i class="fa fa-angle-right"></i>
			<a href="<?php echo base_url() ?>olahdatapenyakit/home">REKAM MEDIS OLAH DATA DIAGNOSA</a>
			
		</li>
</div>
<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	   	<li class="active"><a href="#list" data-toggle="tab">Master ICD-X</a></li>
	    <li><a href="#ina" data-toggle="tab">Master INA-CBG's</a></li>
	    <li><a href="#icd" data-toggle="tab">Master ICD-9CM</a></li>
	    <!-- <li><a href="#logistik" data-toggle="tab">Logistik</a></li> -->

	</ul>

	<div id="my-tab-content" class="tab-content">
		<div class="modal fade" id="tambahICDx" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:100px">
			<div class="modal-dialog">
				<form class="form-horizontal" role="form" id="tambahicdx" method="post">
					<div class="modal-content" style="width:500px">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Tambah ICD-X</h3>
	        			</div>
	        			<div class="modal-body">
			            	<div class="informasi" id="info1">

								<div class="form-group">
									<label class="control-label col-md-4">Kode ICD-X</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="kodeicdx" name="kodeicd_x" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Diagnosa</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="diagnosaicd" name="diagnosaicd" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Diagnosa Lokal</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="diagnosaicdlok" name="diagnosaicdlok" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">DTD</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="dtd" name="dtd" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Sebab Penyakit</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="sbb" name="sbb" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Group </label>
									<div class="col-md-6">
										<textarea class="form-control" autocomplete="off" spellcheck="false" name="grupicd" id="grupicd" ></textarea>
										<input type="hidden" id="id_grupicd">
									</div>
								</div>
							</div>								
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="submit" class="btn btn-success" >Tambah</button>
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</form>
			</div>
		</div>

		<div class="modal fade" id="edICDX" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:100px">
			<div class="modal-dialog">
				<form class="form-horizontal" role="form" id="submitedit_icdx" method="post">
					<div class="modal-content" style="width:500px">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Edit ICD-X</h3>
	        			</div>
	        			<div class="modal-body">
			            	<div class="informasi" id="info1">
								<div class="form-group">
									<label class="control-label col-md-4">Kode ICD-X</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="edkodeicdx" name="edkodeicd_x" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Diagnosa</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="eddiagnosaicd" name="eddiagnosaicd" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Diagnosa Lokal</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="eddiagnosaicdlok" name="eddiagnosaicdlok" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">DTD</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="eddtd" name="eddtd" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Sebab Penyakit</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="edsbb" name="edsbb" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Group </label>
									<div class="col-md-6">
										<textarea class="form-control" autocomplete="off" spellcheck="false" name="edgrupicd" id="edgrupicd" ></textarea>
										<input type="hidden" id="id_edgrupicd">
									</div>
								</div>
							</div>		
	        			</div>
	        			<div class="modal-footer">
	        				<input type="hidden" id="id_icdx_edit">
	 			       		<button type="submit" class="btn btn-success" >Ubah</button>
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
	 			       		
				      	</div>
					</div>
				</form>
			</div>
		</div>


		<div class="modal fade" id="tambahINACBG" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:100px">
			<div class="modal-dialog">
				<form class="form-horizontal" role="form" id="tambahinacbg" method="post">
					<div class="modal-content" style="width:500px">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Tambah INA-CBG</h3>
	        			</div>
	        			<div class="modal-body">
			            	<div class="informasi" id="info1">

								<div class="form-group">
									<label class="control-label col-md-4">Kode INA-CBG</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="kodeinacbg" name="kodeinacbg" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Deskripsi</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="deskripsiinacbg" name="deskripsiinacbg" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas I</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="kelasI" name="kelasI" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas II</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="kelasII" name="kelasII" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas III</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="kelasIII" name="kelasIII" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas Utama</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="kelasUtama" name="kelasUtama" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas VIP</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="kelasVIP" name="kelasVIP" />
									</div>
								</div>															
							</div>		
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="submit" class="btn btn-success" >Tambah</button>
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</form>
			</div>
		</div>

		<div class="modal fade" id="edINA" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:100px">
			<div class="modal-dialog">
				<form class="form-horizontal" role="form" id="submitedit_inacbg">
					<div class="modal-content" style="width:500px">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Edit INA-CBG</h3>
	        			</div>
	        			<div class="modal-body">
			            	<div class="informasi" id="info1">

								<div class="form-group">
									<label class="control-label col-md-4">Kode INA-CBG</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="edkodeinacbg" name="edkodeinacbg" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Deskripsi</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="eddeskripsiinacbg" name="deskripsiinacbg" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas I</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="edkelasI" name="edkelasI" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas II</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="edkelasII" name="edkelasII" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas III</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="edkelasIII" name="edkelasIII" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas Utama</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="edkelasUtama" name="edkelasUtama" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tarif Kelas VIP</label>
									<div class="col-md-6">
										<input type="number" class="form-control" id="edkelasVIP" name="edkelasVIP" />
									</div>
								</div>
							</div>		
	        			</div>
	        			<div class="modal-footer">
	        				<input type="hidden" id="id_inacbg_edit">
	 			       		<button type="submit" class="btn btn-success" >Ubah</button>
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</form>
			</div>
		</div>
	
		<div class="modal fade" id="tambahICDCM" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:100px">
			<div class="modal-dialog">
				<form class="form-horizontal" role="form" id="tambahicd9cm">
					<div class="modal-content" style="width:500px">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Tambah ICD-9CM</h3>
	        			</div>
	        			<div class="modal-body">
			            	<div class="informasi" id="info1">
								<div class="form-group">
									<label class="control-label col-md-4">Kode ICD-9CM</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="kodeicd9cm" name="kodeicd9cm" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Prosedur</label>
									<div class="col-md-6">
										<textarea class="form-control" id="pros" name="pros"></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Keterangan</label>
									<div class="col-md-6">
										<textarea class="form-control" id="eks" name="eks"></textarea>
									</div>
								</div>
							</div>		
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="submit" class="btn btn-success" >Tambah</button>
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
	 			       		
				      	</div>
					</div>
				</form>
			</div>
		</div>
	
		<div class="modal fade" id="edCM" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:100px">
			<div class="modal-dialog">
				<form class="form-horizontal" role="form" id="submitedit_icd9cm" method="post">
					<div class="modal-content" style="width:500px">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Edit ICD-9CM</h3>
	        			</div>
	        			<div class="modal-body">
			            	<div class="informasi" id="info1">

								<div class="form-group">
									<label class="control-label col-md-4">Kode IC-9CM</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="edkodeicd9cm" name="edkodeicd9cm" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Prosedur</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="edpros" name="edpros" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Keterangan</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="edeks" name="edeks" />
									</div>
								</div>
							</div>		
	        			</div>
	        			<div class="modal-footer">
	        				<input type="hidden" id="id_icd9cm_edit">
	 			       		<button type="submit" class="btn btn-success" >Ubah</button>
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</form>
			</div>
		</div>

		<div class="modal fade" id="importexelicdx" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/import_icdx" enctype="multipart/form-data">
				<div class="modal-dialog">
					<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Import Data ICD-X</h3>
				   			</div>
							<div class="modal-body">
								<div class="alert alert-warning">
									<h4>Perhatian</h4>
									<label>1. Halaman ini berguna untuk melakukan penambahan data ICD-X secara massal </label><br>
									<label>2. Gunakan template yang sudah disediakan oleh sistem </label><br>
									<label>3. Jangan merubah tata letak dalam template yang diisi </label><br>
									<label>4. Proses tidak dapat dibatalkan </label><br>
									<a href="<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/download_template_icdx" class="btn btn-info">Download Template Disini</a>
								</div>

								<div class="form-group">
									<div class="col-md-9" style="padding:5px;margin-left:30px">
										<input type="file" class="form-input" name="userfile" placeholder="Pilih File" value required>
									</div>
								</div>

		       				</div>
			        		<br>
			        		<div class="modal-footer">
			        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	<button type="submit" class="btn btn-success">Simpan</button>
						    </div>
						
					</div>
				</div>
			</form>
		</div>

		<div class="modal fade" id="importexelinacbg" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/import_ina_cbg_s" enctype="multipart/form-data">
				<div class="modal-dialog">
					<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Import Data INA-CBG's</h3>
				   			</div>
							<div class="modal-body">
								<div class="alert alert-warning">
									<h4>Perhatian</h4>
									<label>1. Halaman ini berguna untuk melakukan penambahan data INA-CBG's secara massal </label><br>
									<label>2. Gunakan template yang sudah disediakan oleh sistem </label><br>
									<label>3. Jangan merubah tata letak dalam template yang diisi </label><br>
									<label>4. Proses tidak dapat dibatalkan </label><br>
									<a href="<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/download_template_inacbg" class="btn btn-info">Download Template Disini</a>
								</div>

								<div class="form-group">
									<div class="col-md-9" style="padding:5px;margin-left:30px">

										<input type="file" class="form-input" name="userfile" placeholder="Pilih File" value required>
									</div>
								</div>

		       				</div>
			        		<br>
			        		<div class="modal-footer">
			        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	<button type="submit" class="btn btn-success">Simpan</button>
						    </div>
						
					</div>
				</div>
			</form>
		</div>

		<div class="modal fade" id="importexelicdcm9" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/import_icd_ix_cm" enctype="multipart/form-data">
				<div class="modal-dialog">
					<div class="modal-content">
						<form class="form-horizontal" role="form">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Import Data ICD-9CM</h3>
				   			</div>
							<div class="modal-body">
								<div class="alert alert-warning">
									<h4>Perhatian</h4>
									<label>1. Halaman ini berguna untuk melakukan penambahan data ICD-9CM secara massal </label><br>
									<label>2. Gunakan template yang sudah disediakan oleh sistem </label><br>
									<label>3. Jangan merubah tata letak dalam template yang diisi </label><br>
									<label>4. Proses tidak dapat dibatalkan </label><br>
									<a href="<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/download_template_icd9cm" class="btn btn-info">Download Template Disini</a>
								</div>

								<div class="form-group">
									<div class="col-md-9" style="padding:5px;margin-left:30px">

										<input type="file" class="form-input" name="userfile" placeholder="Pilih File" value required>
									</div>
								</div>

		       				</div>
			        		<br>
			        		<div class="modal-footer">
			        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	<button type="submit" class="btn btn-success">Simpan</button>
						    </div>
						</form>
					</div>
				</div>
			</form>
		</div>
	
	
		<div class="tab-pane active" id="list">
			<div class="informasi" style="margin-right:60px">
		       	<form class="form-horizontal" method="POST" id="search_submit_icdx">
			       	<div class="search">
						<label class="control-label col-md-3">
							<i class="fa fa-search">&nbsp;&nbsp;</i>Kode ICD-X <span class="required" style="color : red">* </span>
						</label>
						<div class="col-md-4">		
							<input type="text" class="form-control" id="key_icdx" placeholder="Masukkan Kode ICD-X" autofocus>
				        </div>
				        <button type="submit" class="btn btn-info">Cari</button>&nbsp;
				        <a href="#importexelicdx" data-toggle="modal" class="btn btn-info">Import Masal</a>
					</div>	
				</form>
			</div>

			<hr class="garis">
			<div class="tabelinformasi">

				<a href="#tambahICDx" data-toggle="modal" class="tmbhTndkan" style="margin-left:5px"><i class="fa fa-plus" >&nbsp;Tambah ICD-X</i></a>
				<div class="portlet box red">
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<input type="hidden" id="jlh_icdx" value="<?php echo count($icdx) ?>">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabelutamaicdx">
							<thead>
								<tr class="info">
									<th style="text-align:center;width:20px;">No</th>
									<th>ICD-X</th>
									<th>Diagnosa</th>
									<th>Diagnosa(Local)</th>
									<th>DTD</th>
									<th>Sebab Penyakit</th>
									<th>Group</th>
									<th width="90">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php  
									if (isset($icdx) and !empty($icdx)) {
										$i = 0;
										foreach ($icdx as $value) {
											echo '<tr>
													<td>'.(++$i).'</td>
													<td>'.$value['diagnosis_id'].'</td>
													<td>'.$value['diagnosis_nama'].'</td>
													<td>'.$value['diagnosis_local'].'</td>
													<td>'.$value['diagnosis_dtd'].'</td>
													<td>'.$value['diagnosis_sebab'].'</td>
													<td>'.$value['deskripsi'].'</td>
													<td style="text-align:center"><a class="edit_diagnosa" href="#edICDX" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
														<a href="#" class="remove_diagnosa"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
														<input type="hidden" class="id_icdx" value="'.$value['id'].'">
														<input type="hidden" class="id_grupicdX" value="'.$value['kode'].'">
													</td>								
												</tr>';
										}
									}
								?>
								
							</tbody>
						</table>
					</div>			
				</div>
			</div>  
			
	    </div>

        <div class="tab-pane" id="ina">
	     	<div class="informasi" style="margin-right:60px">
		       	<form class="form-horizontal" method="POST" id="search_submit_inacbg">
			       	<div class="search">
						<label class="control-label col-md-3">
							<i class="fa fa-search">&nbsp;&nbsp;</i>Kode INA-CBG's <span class="required" style="color : red">* </span>
						</label>
						<div class="col-md-4">		
							<input type="text" id="key_inacbg" class="form-control" placeholder="Masukkan Kode INA-CBG's" autofocus>
				        </div>
				        <button type="submit" class="btn btn-info">Cari</button>&nbsp;
				        <a href="#importexelinacbg" data-toggle="modal" class="btn btn-info">Import Masal</a>
					</div>	
				</form>
			</div>
			<hr class="garis">
			<div class="tabelinformasi">

				<a href="#tambahINACBG" data-toggle="modal" class="tmbhTndkan" style="margin-left:5px"><i class="fa fa-plus" >&nbsp;Tambah INA-CBG</i></a>
				<div class="portlet box red">
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<input type="hidden" id="jlh_inacbg" value="<?php echo count($inacbg) ?>">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabelutamainacbg">
							<thead>
								<tr class="info">
									<th style="text-align:center;width:20px;">No</th>
									<th>Kode</th>
									<th>Deskripsi</th>
									<th>Tarif Kelas I</th>
									<th>Tarif Kelas II</th>
									<th>Tarif Kelas III</th>
									<th>Tarif Kelas Utama</th>
									<th>Tarif Kelas VIP</th>
									<th width="90">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php  
									if (isset($inacbg) and !empty($inacbg)) {
										$i = 0;
										foreach ($inacbg as $value) {
											echo '<tr>
													<td>'.(++$i).'</td>
													<td>'.$value['kode'].'</td>
													<td>'.$value['deskripsi'].'</td>
													<td>'.$value['tarif_kelas_1'].'</td>
													<td>'.$value['tarif_kelas_2'].'</td>
													<td>'.$value['tarif_kelas_3'].'</td>
													<td>'.$value['tarif_kelas_utama'].'</td>
													<td>'.$value['tarif_kelas_vip'].'</td>
													<td style="text-align:center"><a class="edit_diagnosa" href="#edINA" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
														<a href="#" class="remove_diagnosa"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
														<input type="hidden" class="id_inacbg" value="'.$value['id'].'">
													</td>
																						
												</tr>';
										}
									}
								?>
							</tbody>
						</table>
					</div>			
				</div>
			</div>
        </div>

        <div class="tab-pane" id="icd">   
        	<div class="informasi" style="margin-right:60px">
		       	<form class="form-horizontal" method="POST" id="search_submit_icd9cm">
			       	<div class="search">
						<label class="control-label col-md-3">
							<i class="fa fa-search">&nbsp;&nbsp;</i>Kode ICD-9CM <span class="required" style="color : red">* </span>
						</label>
						<div class="col-md-4">		
							<input type="text" id="key_icd9cm" class="form-control" placeholder="Masukkan Kode ICD-9CM" autofocus>
				        </div>
				        <button type="submit" class="btn btn-info">Cari</button>&nbsp;
				        <a href="#importexelicdcm9" data-toggle="modal" class="btn btn-info">Import Masal</a>
					</div>	
				</form>
			</div>
			
			<hr class="garis">
			<div class="tabelinformasi">

				<a href="#tambahICDCM" data-toggle="modal" class="tmbhTndkan" style="margin-left:5px"><i class="fa fa-plus" >&nbsp;Tambah ICD-9CM</i></a>
				<div class="portlet box red">
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<input type="hidden" id="jlh_icd9cm" value="<?php echo count($icd9cm) ?>">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabelutamaicd9cm">
							<thead>
								<tr class="info">
									<th style="text-align:center;width:20px;">No</th>
									<th>Kode ICD-9CM</th>
									<th>Prosedur</th>
									<th>Keterangan</th>
									<th width="90">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php  
									if (isset($icd9cm) and !empty($icd9cm)) {
										$i = 0;
										foreach ($icd9cm as $value) {
											echo '<tr>
													<td>'.(++$i).'</td>
													<td>'.$value['kode'].'</td>
													<td>'.$value['prosedur'].'</td>
													<td>'.$value['keterangan'].'</td>
													<td style="text-align:center"><a class="edit_diagnosa" href="#edCM" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
														<a href="#" class="remove_diagnosa"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>							
														<input type="hidden" class="id_icd9cm" value="'.$value['id'].'">
													</td>
												</tr>';
										}
									}
								?>
							</tbody>
						</table>
					</div>			
				</div>
			</div>
        </div>

        <div class="tab-pane" id="logistik">
        	<div class="modal fade" id="modalbarang" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
	        			</div>
	        			<div class="modal-body">

		        			<div class="form-group">
		        				<form method="post" class="form-horizontal" role="form" id="formmintabarang">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncimintabarang" placeholder="Nama barang"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
								</form>
								<div style="margin-right:10px;margin-left:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%">
										<thead>
											<tr class="info">
												<th>Nama Barang</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Tahun Pengadaan</th>
												<th>Stok Gudang</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodybarangpermintaan">
											<tr>
												<td colspan="6" style="text-align:center">Cari data Barang</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
	       	<div class="dropdown" id="btnBawahInventoriBarang">
	            <div id="titleInformasi">Inventori</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoInventoriBarang">
				
				<div class="form-group" >
					<div class="portlet-body" style="margin: 30px 10px 20px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tblinventorigudangunit">
							<thead>
								<tr class="info" >
									<th width="20">No.</th>
									<th > Nama Barang </th>
									<th > Merek </th>
									<th > Harga </th>
									<th > Stok </th>
									<th > Satuan </th>
									<th > Tahun Pengadaan</th>
									<th > Sumber Dana</th>
									<th width="100"> Action </th>

								</tr>
							</thead>
							<tbody id="tbodyinventoribarang">
								<?php 
									if (isset($inventoribarang)) {
										if (!empty($inventoribarang)) {
											$i = 1;
											foreach ($inventoribarang as $value) {
												echo '<tr>
														<td>'.($i++).'</td>
														<td>'.$value['nama'].'</td>
														<td>'.$value['nama_merk'].'</td>
														<td>'.$value['harga'].'</td>
														<td>'.$value['stok'].'</td>
														<td>'.$value['satuan'].'</td>
														<td>'.$value['tahun_pengadaan'].'</td>
														<td>'.$value['sumber_dana'].'</td>
														<td style="text-align:center">
															<input type="hidden" class="barang_detail_inout" value="'.$value['barang_detail_id'].'">
															<a href="#inoutbar" data-toggle="modal" class="edBarang" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
															<a href="#edInvenBerBar" data-toggle="modal" class="detailinvenbarang"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
														</td>
													</tr>';
											}
										}
									}
								?>
									
							</tbody>
						</table>
					</div>
					<form method="post" action="<?php echo base_url() ?>bersalin/homebersalin/excel_barang_unit">
						<button class="btn btn-info" type="submit" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
						<input type="hidden" class="my_dept_id" name="my_dept_id" value="<?php echo $dept_id ?>">
					</form>
	        	</div>
	        </div>
			<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<form class="form-horizontal" role="form" style="margin-left:30px;" id="forminoutbarang">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<div class="form-group">
			        				<label class="control-label col-md-3" >Tanggal </label>
									<div class="col-md-6" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" id="tanggalinout" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
									</div>
								</div>
										
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" >In / Out </label>
									<div class="col-md-6">
						         		<select class="form-control select" name="io" id="io">
											<option value="IN" selected>IN</option>
											<option value="OUT">OUT</option>					
										</select>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Jumlah in/out</label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="jmlInOut" name="jmlInOut" placeholder="Jumlah">
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Sisa Stok </label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="sisaInOut" name="sisaInOut" placeholder="Sisa Stok" readonly>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Keterangan </label>
									<div class="col-md-6" >
										<textarea class="form-control" id="keteranganIObar" placeholder="Keterangan"></textarea>
									</div>
								</div>										
		        			</div>
		        			<div class="modal-footer">
		        				<input type="hidden" id="id_barang_inoutprocess">
		 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			       		<button type="submit" class="btn btn-success">Simpan</button>
					      	</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<form class="form-horizontal" role="form">
				            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th> Waktu </th>
											<th> IN / OUT </th>
											<th> Jumlah </th>
											<th> Keterangan </th>
										</tr>
									</thead>
									<tbody id="tbodydetailbrginventori">
										<tr>
											<td colspan="4" style="text-align:center">Tidak ada detail in-out</td>
										</tr>
											
									</tbody>
								</table>
							</form>
							
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
			<br>

			<div class="dropdown" id="btnBawahPermintaanBarang" style="margin-left:10px;width:98.5%">
	            <div id="titleInformasi">Permintaan Logistik</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoPermintaanBarang">
            	<form class="form-horizontal" role="form" method="post" id="permintaanbarangunit">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="nomorpermintaanbarang" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanbarang" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="keteranganpermintaanbarang" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>
					<a href="#modalbarang" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Barang</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<th> Nama Barang </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Tahun Pengadaan </th>
										<th> Stok Gudang </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputmintabarang">
									<?php echo '<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>'; ?>
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<button class="btn btn-warning" type="reset" id="batalpermintaanfarmasi">RESET</button>
							<button class="btn btn-success" type="submit">SIMPAN</button>
						</div>
						<br>
					</div>	
				</form>
			</div>	    
			<br>
	    </div>
        
    </div>

</div>

			
<script type="text/javascript">
	$(document).ready(function(){
		$("#bwpermintaanlogistik").click(function(){
			$("#ibwpermintaanlogistik").slideToggle();
			
		});
		$("#bwinlogistik").click(function(){
			$("#ibwinlogistik").slideToggle();
			
		});
	});
</script>