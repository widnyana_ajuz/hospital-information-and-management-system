<script type="text/javascript">
	$(document).ready(function () {
		$('#tambahicdx').submit(function (e) {
			e.preventDefault();
			var item =  {};
			item['diagnosis_id'] = $('#kodeicdx').val();
			item['diagnosis_nama'] = $('#diagnosaicd').val();
			item['diagnosis_local'] = $('#diagnosaicdlok').val();
			item['diagnosis_dtd'] = $('#dtd').val();
			item['diagnosis_sebab'] = $('#sbb').val();
			item['diagnosis_group'] = $('#id_grupicd').val();
			item['deskripsi'] = $('#grupicd').val();
			//console.log(item);return false;
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/save_icdx',
				success: function (data) {
					console.log(data);
					var jlh = $('#jlh_icdx').val();
					var no = Number(jlh) + 1;
					var t = $('#tabelutamaicdx').DataTable();
					var last = '<center><a class="edit_diagnosa" href="#edICDX" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
								'<a href="#" class="remove_diagnosa"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></center>'+
								'<input type="hidden" class="id_icdx" value="'+data['id']+'">'+
								'<input type="hidden" class="id_grupicdX" value="'+data['diagnosis_group']+'">';
					t.row.add([
						no,
						data['diagnosis_id'],
						data['diagnosis_nama'],
						data['diagnosis_local'],
						data['diagnosis_dtd'],
						data['diagnosis_sebab'],
						data['deskripsi'],
						last,
						''
					]).draw();
					$('#jlh_icdx').val();
					$('#tambahICDx').modal('hide');
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		
		var this_icd;
		$('#tabelutamaicdx tbody').on('click', 'tr td a.remove_diagnosa', function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .id_icdx').val();
			this_icd = $(this);
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/delete_icdx/' + id,
				success: function (data) {
					console.log(data);
					var jlh = $('#jlh_icdx').val();
					var no = Number(jlh) - 1;
					$('#jlh_icdx').val(no);
					var t = $('#tabelutamaicdx').DataTable();
					t.row(this_icd.parents('tr') ).remove().draw();
					t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			            cell.innerHTML = i+1;
			        } );
			        t.on( 'order.dt search.dt', function () {
				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
				    } ).draw();
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tambahinacbg').submit(function (e) {
			e.preventDefault();
			var item =  {};
			item['kode'] = $('#kodeinacbg').val();
			item['deskripsi'] = $('#deskripsiinacbg').val();
			item['tarif_kelas_1'] = $('#kelasI').val();
			item['tarif_kelas_2'] = $('#kelasII').val();
			item['tarif_kelas_3'] = $('#kelasIII').val();
			item['tarif_kelas_utama'] = $('#kelasUtama').val();
			item['tarif_kelas_vip'] = $('#kelasVIP').val();

			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/save_inacbg',
				success: function (data) {
					console.log(data);
					var jlh = $('#jlh_inacbg').val();
					var no = Number(jlh) + 1;
					var t = $('#tabelutamainacbg').DataTable();
					var last = '<center><a class="edit_diagnosa" href="#edINA" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
								'<a href="#" class="remove_diagnosa"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></center>'+
								'<input type="hidden" class="id_inacbg" value="'+data['id']+'">';
					t.row.add([
						no,
						data['kode'],
						data['deskripsi'],
						data['tarif_kelas_1'],
						data['tarif_kelas_2'],
						data['tarif_kelas_3'],
						data['tarif_kelas_utama'],
						data['tarif_kelas_vip'],
						last,
						''
					]).draw();
					$('#jlh_inacbg').val(no);
					$('#tambahINACBG').modal('hide');
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		var this_inacbg;
		$('#tabelutamainacbg tbody').on('click', 'tr td a.remove_diagnosa', function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .id_inacbg').val();
			this_inacbg = $(this);
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/delete_inacbg/' + id,
				success: function (data) {
					console.log(data);
					var jlh = $('#jlh_inacbg').val();
					var no = Number(jlh) - 1;
					$('#jlh_inacbg').val(no);
					var t = $('#tabelutamainacbg').DataTable();
					t.row(this_inacbg.parents('tr') ).remove().draw();
					t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			            cell.innerHTML = i+1;
			        } );
			        t.on( 'order.dt search.dt', function () {
				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
				    } ).draw();
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tambahicd9cm').submit(function (e) {
			e.preventDefault();
			var item =  {};
			item['kode'] = $('#kodeicd9cm').val();
			item['prosedur'] = $('#pros').val();
			item['keterangan'] = $('#eks').val();

			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/save_icd9cm',
				success: function (data) {
					console.log(data);
					var jlh = $('#jlh_icd9cm').val();
					var no = Number(jlh) + 1;
					var t = $('#tabelutamaicd9cm').DataTable();
					var last = '<center><a class="edit_diagnosa" href="#edCM" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
								'<a href="#" class="remove_diagnosa"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></center>'+
								'<input type="hidden" class="id_icd9cm" value="'+data['id']+'">';
					t.row.add([
						no,
						data['kode'],
						data['prosedur'],
						data['keterangan'],
						last,
						''
					]).draw();
					 $('#jlh_icd9cm').val(no);
					 $('#tambahICDCM').modal('hide');
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		var this_icd9cm;
		$('#tabelutamaicd9cm tbody').on('click', 'tr td a.remove_diagnosa', function (e) {
			e.preventDefault();
			var id = $(this).closest('tr').find('td .id_icd9cm').val();
			this_icd9cm = $(this);
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/delete_icd9cm/' + id,
				success: function (data) {
					console.log(data);
					var jlh = $('#jlh_icd9cm').val();
					var no = Number(jlh) - 1;
					$('#jlh_icd9cm').val(no);
					var t = $('#tabelutamaicd9cm').DataTable();
					t.row(this_icd9cm.parents('tr') ).remove().draw();
					t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			            cell.innerHTML = i+1;
			        } );
			        t.on( 'order.dt search.dt', function () {
				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
				    } ).draw();
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		//edit
		$('#tabelutamaicdx tbody').on('click', 'tr td a.edit_diagnosa', function (e) {
			$(':input, #submitedit_icdx').not(':button, :hidden').val('');
			this_icd = $(this);
			e.preventDefault();
			var id = $(this).closest('tr').find('td .id_icdx').val();
			var id_grup = $(this).closest('tr').find('td .id_grupicdX').val();
			$('#id_icdx_edit').val(id);
			$('#id_edgrupicd').val(id_grup);
			var close = $(this).closest('tr').find('td');
			$('#edkodeicdx').val(close.eq(1).text());
			$('#eddiagnosaicd').val(close.eq(2).text());
			$('#eddiagnosaicdlok').val(close.eq(3).text());
			$('#eddtd').val(close.eq(4).text());
			$('#edsbb').val(close.eq(5).text());
			$('#edgrupicd').val(close.eq(6).text());
		})

		$('#submitedit_icdx').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['diagnosis_id'] = $('#edkodeicdx').val();
			item['diagnosis_nama'] = $('#eddiagnosaicd').val();
			item['diagnosis_local'] = $('#eddiagnosaicdlok').val();
			item['diagnosis_dtd'] = $('#eddtd').val();
			item['diagnosis_sebab'] = $('#edsbb').val();
			item['diagnosis_group'] = $('#id_edgrupicd').val();
			item['deskripsi'] = $('#edgrupicd').val();
			item['id'] = $('#id_icdx_edit').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/edit_icdx',
				success: function (data) {
					console.log(data);
					this_icd.closest('tr').find('td').eq(1).text(data['diagnosis_id']);
					this_icd.closest('tr').find('td').eq(2).text(data['diagnosis_nama']);
					this_icd.closest('tr').find('td').eq(3).text(data['diagnosis_local']);
					this_icd.closest('tr').find('td').eq(4).text(data['diagnosis_dtd']);
					this_icd.closest('tr').find('td').eq(5).text(data['diagnosis_sebab']);
					this_icd.closest('tr').find('td').eq(6).text(data['deskripsi']);
					this_icd.closest('tr').find('td .id_grupicdX').val(data['diagnosis_group']);
					$('#edICDX').modal('hide');
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tabelutamainacbg tbody').on('click', 'tr td a.edit_diagnosa', function (e) {
			$(':input, #submitedit_inacbg').not(':button, :hidden').val('');
			this_inacbg = $(this);
			e.preventDefault();
			var id = $(this).closest('tr').find('td .id_inacbg').val();
			$('#id_inacbg_edit').val(id);
			var close = $(this).closest('tr').find('td');
			$('#edkodeinacbg').val(close.eq(1).text());
			$('#eddeskripsiinacbg').val(close.eq(2).text());
			$('#edkelasI').val(close.eq(3).text());
			$('#edkelasII').val(close.eq(4).text());
			$('#edkelasIII').val(close.eq(5).text());
			$('#edkelasUtama').val(close.eq(6).text());
			$('#edkelasVIP').val(close.eq(7).text());
		})

		$('#submitedit_inacbg').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['kode'] = $('#edkodeinacbg').val();
			item['deskripsi'] = $('#eddeskripsiinacbg').val();
			item['tarif_kelas_1'] = $('#edkelasI').val();
			item['tarif_kelas_2'] = $('#edkelasII').val();
			item['tarif_kelas_3'] = $('#edkelasIII').val();
			item['tarif_kelas_utama'] = $('#edkelasUtama').val();
			item['tarif_kelas_vip'] = $('#edkelasVIP').val();
			item['id'] = $('#id_inacbg_edit').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/edit_inacbg',
				success: function (data) {
					console.log(data);
					this_inacbg.closest('tr').find('td').eq(1).text(data['kode']);
					this_inacbg.closest('tr').find('td').eq(2).text(data['deskripsi']);
					this_inacbg.closest('tr').find('td').eq(3).text(data['tarif_kelas_1']);
					this_inacbg.closest('tr').find('td').eq(4).text(data['tarif_kelas_2']);
					this_inacbg.closest('tr').find('td').eq(5).text(data['tarif_kelas_3']);
					this_inacbg.closest('tr').find('td').eq(6).text(data['tarif_kelas_utama']);
					this_inacbg.closest('tr').find('td').eq(7).text(data['tarif_kelas_vip']);
					
					$('#edINA').modal('hide');
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tabelutamaicd9cm tbody').on('click', 'tr td a.edit_diagnosa', function (e) {
			$(':input, #submitedit_icd9cm').not(':button, :hidden').val('');
			this_icd9cm = $(this);
			e.preventDefault();
			var id = $(this).closest('tr').find('td .id_icd9cm').val();
			$('#id_icd9cm_edit').val(id);
			var close = $(this).closest('tr').find('td');
			$('#edkodeicd9cm').val(close.eq(1).text());
			$('#edpros').val(close.eq(2).text());
			$('#edeks').val(close.eq(3).text());
		})

		$('#submitedit_icd9cm').submit(function (e) {
			e.preventDefault();
			var item =  {};
			item['kode'] = $('#edkodeicd9cm').val();
			item['prosedur'] = $('#edpros').val();
			item['keterangan'] = $('#edeks').val();
			item['id'] = $('#id_icd9cm_edit').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url() ?>rekammedis/homeolahdatapenyakit/edit_icd9cm',
				success: function (data) {
					console.log(data);
					this_icd9cm.closest('tr').find('td').eq(1).text(data['kode']);
					this_icd9cm.closest('tr').find('td').eq(2).text(data['prosedur']);
					this_icd9cm.closest('tr').find('td').eq(3).text(data['keterangan']);
					
					$('#edCM').modal('hide');
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		//autocomplete
		$('#grupicd').focus(function(){
			var $input = $('#grupicd');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rekammedis/homeolahdatapenyakit/get_group_icdX',
				success:function(data){
					//console.log(data);return false;
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['deskripsi']);
						iddata.push(data[i]['kode']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#id_grupicd').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		$('#edgrupicd').focus(function(){
			var $input = $('#edgrupicd');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rekammedis/homeolahdatapenyakit/get_group_icdX',
				success:function(data){
					//console.log(data);return false;
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['deskripsi']);
						iddata.push(data[i]['kode']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#id_edgrupicd').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		/*logistik*/
		var this_io;
		$('#tbodyinventoribarang').on('click', 'tr td a.edBarang', function (e) {
			e.preventDefault();
			$('#id_barang_inoutprocess').val($(this).closest('tr').find('td .barang_detail_inout').val());
			var jlh = $(this).closest('tr').find('td').eq(4).text();
			this_io = $(this);
			$('#sisaInOut').val(jlh);

			$('#jmlInOut').on('change', function (e) {
				e.preventDefault();

				var is_in = $('#io').find('option:selected').val();
				var jmlInOut = $('#jmlInOut').val();
				var sisa = jlh;//$('#sisaInOut').val();
				var hasil ="";
				if (is_in == 'IN') {
					hasil = Number(jmlInOut) + Number(sisa);
				}else{			
					hasil = Number(sisa) - Number(jmlInOut);
				}

				if (jmlInOut == '') {
					hasil = Number(sisa);
				}
				$('#sisaInOut').val(hasil);			
			})

			$('#io').on('change', function () {
				var jumlah = Number($('#jmlInOut').val());
				var sisa = Number(jlh);//Number($('#sisaInOut').val());

				var isout = $('#io').find('option:selected').val();
				if (isout === 'IN') {
					$('#sisaInOut').val(jumlah + sisa);
				} else{
					$('#sisaInOut').val(sisa - jumlah);
				};
			})
		})
	
		$('#forminoutbarang').submit(function (e) {
			e.preventDefault();

			var item = {};
			item['barang_detail_id'] = $('#id_barang_inoutprocess').val();
			item['jumlah'] = $('#jmlInOut').val();
			item['sisa'] = $('#sisaInOut').val();
			item['is_out'] = $('#io').find('option:selected').val();
		    item['tanggal'] = $('#tanggalinout').val();
		    item['keterangan'] = $('#keteranganIObar').val();
		    //console.log(item);return false;
		    if (item['jumlah'] != "") {
			    $.ajax({
			    	type: "POST",
			    	data: item,
			    	url: "<?php echo base_url()?>rekammedis/homeolahdatapenyakit/input_in_outbarang",
			    	success: function (data) {
			    		if (data == "true") {
			    			myAlert('data berhasil disimpan');
			    			$('#keteranganIObar').val('');
			    			$('#jmlInOut').val('');
			    			this_io.closest('tr').find('td').eq(4).text(item['sisa']);
			    			$('#inoutbar').modal('hide');	
			    		} else{
			    			myAlert('gagal, terdapat kesalahan');
			    		};
			    	},
			    	error: function (data) {
			    		myAlert('gagal');
			    	}
			    })
			} else{
				myAlert('isi data dengan benar');
				$('#jmlInOut').focus();
			};			
		})

		$("#tbodyinventoribarang").on('click', 'tr td a.detailinvenbarang', function (e) {
			var id = $(this).closest('tr').find('td .barang_detail_inout').val();

			 $.ajax({
		    	type: "POST",
		    	url: "<?php echo base_url()?>rekammedis/homeolahdatapenyakit/get_detail_inventori/" + id,
		    	success: function (data) {
		    		console.log(data);
		    		$('#tbodydetailbrginventori').empty();
		    		for(var i = 0; i < data.length ; i++){
		    			$('#tbodydetailbrginventori').append(
							'<tr>'+
								'<td>'+format_date(data[i]['tanggal'])+'</td>'+
								'<td>'+data[i]['is_out']+'</td>'+
								'<td>'+data[i]['jumlah']+'</td>'+
								'<td>'+data[i]['keterangan']+'</td>'+
							'</tr>'
		    			)
		    		}
		    	},
		    	error: function (data) {
		    		myAlert('gagal');
		    	}
		    })
		})
		
		$('#formmintabarang').submit(function (e) {
			e.preventDefault();
			var item ={};
			item['katakunci'] = $('#katakuncimintabarang').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>rekammedis/homeolahdatapenyakit/get_barang_gudang',
				success: function (data) {
					console.log(data);//return false;
					$('#tbodybarangpermintaan').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodybarangpermintaan').append(
								'<tr>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['tahun_pengadaan']+'</td>'+
									'<td>'+data[i]['stok_gudang']+'</td>'+
									'<td style="text-align:center"><a href="#" class="addnewpermintaanbarang"><i class="glyphicon glyphicon-check"></i></a></td>'+
									'<td style="display:none">'+data[i]['barang_stok_id']+'</td>'+
									'<td style="display:none">'+data[i]['barang_id']+'</td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodybarangpermintaan').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tbodybarangpermintaan').on('click', 'tr td a.addnewpermintaanbarang',function (e) {
			e.preventDefault();
			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputmintabarang').append(
				'<tr><td>'+cols[0]+'</td>'+//nama
				'<td>'+cols[1]+'</td>'+  //satuan
				'<td>'+cols[2]+'</td>'+ //merk
				'<td>'+cols[3]+'</td>'+ //tahun pengadaan
				'<td>'+cols[4]+'</td>'+ //stok gudang
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah minta
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
				'<td style="display:none">'+cols[6]+'</td>'+ //barang_stok_id
				'<td style="display:none">'+cols[7]+'</td></tr>' //barang_id
			)
		})

		$('#permintaanbarangunit').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['no_permintaanbarang'] = $('#nomorpermintaanbarang').val();
			item['tanggal_request'] = $('#tglpermintaanbarang').val();
			item['keterangan_request'] = $('#keteranganpermintaanbarang').val();

			var data = [];
			$('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
		    $('#addinputmintabarang').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;

		    $.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>rekammedis/homeolahdatapenyakit/submit_permintaan_barangunit',
				success: function (data) {
					console.log(data);
					if (data['error'] == 'n'){
						$('#addinputmintabarang').empty();
						$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
						$('#nomorpermintaanbarang').val('');
						$('#keteranganpermintaanbarang').val('');
					}
					myAlert(data['message']);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
		/*logistik unit*/

		/*search: nanti saja lah*/
		$('#search_submit_icdx').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['katakunci'] = $('#key_icdx').val();

			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url();?>rekammedis/homeolahdatapenyakit/search_icdx',
				success: function (data) {
					console.log(data);
					var t = $('#tabelutamaicdx').DataTable();
					t.clear().draw();

					for (var i = 0; i < data.length; i++) {
						var last = '<center><a class="edit_diagnosa" href="#edICDX" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
									'<a href="#" class="remove_diagnosa"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>'+
									'<input type="hidden" class="id_icdx" value="'+data[i]['id']+'">'+
									'<input type="hidden" class="id_grupicdX" value="'+data[i]['kode']+'"></center>';
						t.row.add([
							Number(i+1),
							data[i]['diagnosis_id'],
							data[i]['diagnosis_nama'],
							data[i]['diagnosis_local'],
							data[i]['diagnosis_dtd'],
							data[i]['diagnosis_sebab'],
							data[i]['deskripsi'],
							last,
							''
							]).draw();
					};

					t.on( 'order.dt search.dt', function () {
				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
				    } ).draw();
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#search_submit_icd9cm').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['katakunci'] = $('#key_icd9cm').val();

			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url();?>rekammedis/homeolahdatapenyakit/search_icd9cm',
				success: function (data) {
					console.log(data);
					var t = $('#tabelutamaicd9cm').DataTable();
					t.clear().draw();

					for (var i = 0; i < data.length; i++) {
						var last = '<center><a class="edit_diagnosa" href="#edCM" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
									'<a href="#" class="remove_diagnosa"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>'+	
									'<input type="hidden" class="id_icd9cm" value="'+data[i]['id']+'"></center>';
						t.row.add([
							Number(i+1),
							data[i]['kode'],
							data[i]['prosedur'],
							data[i]['keterangan'],
							last,
							''
							]).draw();
						
					};

					t.on( 'order.dt search.dt', function () {
				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
				    } ).draw();
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#search_submit_inacbg').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['katakunci'] = $('#key_inacbg').val();

			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url();?>rekammedis/homeolahdatapenyakit/search_inacbg',
				success: function (data) {
					console.log(data);
					var t = $('#tabelutamainacbg').DataTable();
					t.clear().draw();

					for (var i = 0; i < data.length; i++) {
						var last = '<center><a class="edit_diagnosa" href="#edINA" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
									'<a href="#" class="remove_diagnosa"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>'+
									'<input type="hidden" class="id_inacbg" value="'+data[i]['id']+'"></center>';
						t.row.add([
							Number(i+1),
							data[i]['kode'],
							data[i]['deskripsi'],
							data[i]['tarif_kelas_1'],
							data[i]['tarif_kelas_2'],
							data[i]['tarif_kelas_3'],
							data[i]['tarif_kelas_utama'],
							data[i]['tarif_kelas_vip'],
							last,
							''
							]).draw();
					};

					t.on( 'order.dt search.dt', function () {
				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
				    } ).draw();
				},
				error: function (data) {
					console.log(data);
				}
			})
		})
	})
</script>