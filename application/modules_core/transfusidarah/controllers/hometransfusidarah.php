<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Hometransfusidarah extends Operator_base {
	protected $dept_id;

	function __construct()
	{
		parent:: __construct();
		$this->load->model('m_darah');
		$data['page_title'] = "Unit Transfusi Darah";
		$this->dept_id = $this->m_darah->get_dept_id('UNIT TRANSFUSI DARAH')['dept_id'];
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$this->session->set_userdata($data);
		// load template
		$data['content'] = 'home';
		$data['alldarah'] = $this->m_darah->get_alldarah();
		$data['riwayatdarah'] = $this->m_darah->getriwayatpenerimaan();
		$data['totalriwayat'] = $this->m_darah->get_riwayat_total();
		$data['javascript'] = 'j_penerimaan';
		$this->load->view('base/operator/template', $data);
	}

	public function adddarah()
	{
		$insert['nama'] = $_POST['nama'];
		$insert['gol_darah'] = $_POST['gol_darah'];
		$insert['rhesus'] = $_POST['rhesus'];
		$insert['stok'] = $_POST['stok'];

		$res = $this->m_darah->adddarah($insert);

		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function addpenerimaandarah()
	{
		$this->form_validation->set_rules('tgl_penerimaan', 'Tanggal Penerimaan', 'required|trim|xss_clean');
		$this->form_validation->set_rules('penyedia', 'Penyedia Penerimaan', 'required|trim|xss_clean');
		$this->form_validation->set_message('required', 'isi semua data dengan benar');

		if ($this->form_validation->run() == TRUE) {
			$tgl = DateTime::createFromFormat('d/m/Y', $_POST['tgl_penerimaan']);
			$data = $_POST['data'];
			$insert = array(
					'no_penerimaan' => $_POST['no_penerimaan'],
			 		'tgl_penerimaan' => $tgl->format('Y-m-d'),
					'penyedia' => $_POST['penyedia'],
					'subtotal' => $_POST['subtotal'],
					'potongan' => $_POST['potongan'],
					'grandtotal' => $_POST['grandtotal'],
					'petugas_input' => $this->session->userdata('session_operator')['petugas_id']
			);
			$id = $this->m_darah->add_penerimaan($insert);
			if($id){
				foreach ($data as $value) {
					$insdetail = array(
						'stok_id' => $value[7],
						'jumlah' => $value[9],
						'harga' => $value[11],
						'penerimaan_id' => $id
					);
					$res = $this->m_darah->add_penerimaan_detail($insdetail);
					//ubah stok darah
					$stok = $this->m_darah->get_stok($value[7]);
					if ($stok) {
						$stokbaru['stok'] = ($stok['stok'] + intval($value[9]));
						$this->m_darah->inoutdarah($value[7], $stokbaru);
						$result = $id;
					}
				}
			}else{
				$result = array(
					'message' => 'Gagal menyimpan data',
					'error' => 'y'
				);
			}
		}else{
			$result = array(
				'message' => strip_tags(str_replace("\n ", "", validation_errors())),
				'error' => 'y'
			 );
		}
		header('Content-Type: application/json');
	 	echo json_encode($result);
	}

	public function deletedarah($id)
	{
		$this->m_darah->delete($id);
		redirect('transfusidarah/hometransfusidarah');
	}

	public function inoutdarah()
	{
		$id = $_POST['id'];
		$data['stok'] = $_POST['stok'];
		$this->m_darah->inoutdarah($id, $data);
	}

	public function searchdarah()
	{
		$katakunci = $_POST['search'];
		$res = $this->m_darah->searchdarah($katakunci);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

	public function getdetailriwayat($id)
	{
		$res = $this->m_darah->getriwayatpenerimaandetail($id);
		header('Content-Type: application/json');
	 	echo json_encode($res);
	}

}
