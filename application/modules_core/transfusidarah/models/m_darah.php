<?php
  class M_darah extends CI_Model
  {
    function __construct(){}

    public function get_dept_id($nama)
    {
      $sql = "SELECT dept_id from master_dept WHERE nama_dept LIKE '$nama'";
      $res = $this->db->query($sql);
      if ($res){
        return $res->row_array();
      }
      else {
        return false;
      }
    }

    public function get_alldarah()
    {
      $sql = "SELECT * FROM darah_stok WHERE stok > 0";
      $result = $this->db->query($sql);
      if ($result) {
          return $result->result_array();
      }
      else {
        return false;
      }
    }

    public function get_riwayat_total()
    {
      $sql = "SELECT COUNT(id) AS total FROM darah_penerimaan";
      $res = $this->db->query($sql);
      if ($res) {
        return $res->row_array();
      }else {
        return false;
      }
    }

    public function getriwayatpenerimaan()
    {
      $sql = "SELECT * FROM darah_penerimaan";
      $result   = $this->db->query($sql);
      if($result){
        return $result->result_array();
      }
      else {
        return false;
      }
    }

    public function getriwayatpenerimaandetail($id)
    {
        $sql = "SELECT gol_darah, rhesus, jumlah, harga FROM darah_stok ds, darah_penerimaan_detail dpd WHERE dpd.stok_id = ds.id AND dpd.penerimaan_id = $id";
        $result = $this->db->query($sql);
        if($result){
          return $result->result_array();
        }else {
          return false;
        }
    }

    public function get_stok($id)
    {
      $sql = "SELECT stok FROM darah_stok WHERE id = $id";
      $result = $this->db->query($sql);
      if($result){
        return $result->row_array();
      }else {
        return false;
      }
    }

    public function adddarah($value)
    {
      $res = $this->db->insert('darah_stok', $value);
      if ($res) {
        return $this->db->insert_id();
      }
      else {
        return false;
      }
    }

    public function add_penerimaan($insert)
    {
      $result =  $this->db->insert('darah_penerimaan', $insert);
			if ($result) {
				return $this->db->insert_id();
			}else{
				return false;
			}
    }

    public function add_penerimaan_detail($insert)
    {
      $result =  $this->db->insert('darah_penerimaan_detail', $insert);
      if ($result) {
        return $this->db->insert_id();
      }else{
        return false;
      }
    }

    public function inoutdarah($id, $data) //update stok darah (dipakai di in-out & penerimaan)
    {
      $this->db->where('id', $id);
      $res = $this->db->update('darah_stok', $data);
      if ($res) {
        return true;
      }else {
        return false;
      }
    }

    public function delete($id)
    {
      $this->db->where('id', $id);
      $this->db->delete('darah_stok');
    }

    public function searchdarah($katakunci)
    {
      $sql = "SELECT * FROM darah_stok WHERE nama LIKE '%$katakunci%' OR gol_darah LIKE '%$katakunci%' OR rhesus LIKE '%$katakunci%'";
      $res = $this->db->query($sql);
      if ($res->num_rows() >0) {
        return $res->result_array();
      }else {
        return false;
      }
    }


  }
 ?>
