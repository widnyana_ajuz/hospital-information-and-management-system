<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>transfusidarah/home">UNIT TRANSFUSI DARAH</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">Inventori Darah</a>
	</li>
</div>

<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	    <li class="active"><a href="#inventori" data-toggle="tab">Inventori Darah</a></li>
	    <li><a href="#penerimaan" data-toggle="tab">Penerimaan Darah</a></li>
	    <li><a href="#riwayat" data-toggle="tab">Riwayat Penerimaan Darah</a></li>
	</ul>
	<div class="modal fade" id="inoutUTD" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:200px">
			<div class="modal-dialog">
				<div class="modal-content" >
					<div class="modal-header">
	  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	  				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
	  			</div>
					<form class="form-horizontal" role="form">
	  				<div class="modal-body">
							<div class="form-group">
								<label class="control-label col-md-3" >In / Out</label>
								<div class="col-md-4">
						    	<select class="form-control select" name="ioUTD" id="ioUTD">
										<option value="IN" selected>IN</option>
										<option value="OUT">OUT</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" >Jumlah</label>
								<div class="col-md-4" >
									<input type="number" class="form-control" id="jmlInOutUTD" name="jmlInOutUTD" placeholder="Jumlah">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" >Sisa Stok</label>
								<div class="col-md-4" >
									<input type="text" readonly class="form-control" id="sisaInOutUTD" name="sisaInOutUTD" placeholder="Sisa Stok">
								</div>
							</div>
	        	</div>
	        	<div class="modal-footer">
							<input type="hidden" id="id_darah_inout">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
							<button type="submit" id="simpaninout" class="btn btn-success" data-dismiss="modal">Simpan</button>
				    </div>
					</form>
				</div>
			</div>
		</div>
		<div id="my-tab-content" class="tab-content">
      <div class="tab-pane active" id="inventori">
      	<div class="dropdown"  style="margin-left:10px;width:98.5%">
	        <div id="titleInformasi">Tambah Inventori</div>
	        
        </div>
        <br>
        
        	 <form class="form-horizontal" role="form" method="post" id="frminventori">
        	 		<div class="informasi">
						 <div class="form-group">
	 						<label class="control-label col-md-2">Nama Barang</label>
	 						<div class="col-md-4">
	 							<input type="text" class="form-control" name="namaTD" id="namaTD" placeholder="Nama Barang" required>
	 						</div>
	 					</div>
						<div class="form-group">
							<label class="control-label col-md-2">Golongan Darah</label>
							<div class="col-md-1" style="width:150px;">
								<select class="form-control select" name="golDarahTD" id="golDarahTD" required>
									<option selected value="">Pilih</option>
									<option value="A">A</option>
									<option value="AB">AB</option>
									<option value="B">B</option>
									<option value="O">O</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Rhesus</label>
							<div class="col-md-2">
								<select class="form-control select" name="rhesusTD" id="rhesusTD" required>
									<option selected value="">Pilih</option>
									<option value="Positif">Positif</option>
									<option value="Negatif">Negatif</option>
								</select>
							</div>
						</div>
						<div class="form-group">
	    	 			<label class="control-label col-md-2">Jumlah</label>
	    	 			<div class="col-md-2">
								<div class="input-group">
						    	<input type="text" class="form-control text-right" name="jmlhTransfusi" id="jmlhTransfusi" placeholder="0" required>
						      <div class="input-group-addon">kantong</div>
						    </div>
	    	 			</div>
	    	 		</div>
	    	 	</div>
       			<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span style="padding:0px 10px 0px 10px;">
								<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
								<button type="submit" class="btn btn-success">SIMPAN</button>
							</span>
						</div>
						<br>
						<br>
        	 	<div class="form-group">
			        <div class="portlet-body" style="margin: 0px 23px 0px 23px">
		            <table class="table table-striped table-bordered table-hover tableDTUtama">
									<thead>
										<tr class="info">
											<th width="10">No.</th>
											<th>Nama</th>
											<th>Golongan Darah</th>
											<th>Rhesus</th>
											<th>Stok</th>
											<th width="80">Action</th>
										</tr>
									</thead>
									<tbody id="tbodyinventoridarah">
										<?php
											if (isset($alldarah))
											{
												if(!empty($alldarah))
												{
													$i=0;
													foreach ($alldarah as $value) {
														echo '<tr>
														<td align="center">' .(++$i).'</td>
														<td>' .$value['nama'].'</td>
														<td>' .$value['gol_darah'].'</td>
														<td>' .$value['rhesus'].'</td>
														<td class="stokval text-right">' .$value['stok'].'</td>
														<td style="text-align:center;">
															<input type="hidden" class="darah_inout" value="'.$value['id'].'">
															<a href="#inoutUTD" class="inoutdarah" data-toggle="modal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
														</td>
														</tr>';
													}
												}
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
        	 </form>
        
      </div>

      <div class="modal fade" id="detailTD" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
							<h3 class="modal-title" id="myModalLabel">Detail Riwayat Penerimaan</h3>
						</div>
						<div class="modal-body">
      			 <form class="form-horizontal" role="form">
							 <div class="form-group">
  						 		<label class="control-label col-md-3">No Penerimaan</label>
  								<div class="col-md-5">
  						 			<input type="text" class="form-control" id="noPenTD" name="noPenTD"  placeholder="Nomor" readonly>
  								</div>
  		    	 		</div>
  		    	 		<div class="form-group">
  		      	 		<label class="control-label col-md-3">Penyedia</label>
  				     		<div class="col-md-5">
  				     			<input type="text" class="form-control" id="pnyediaTD" name="pnyediaTD" placeholder="Penyedia" readonly>
  				     		</div>
  		    	 		</div>
								<div class="form-group">
									<label class="control-label col-md-3">Tanggal</label>
									<div class="col-md-5">
										<input type="text" id="tglTD" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy" placeholder="<?php echo date("d/m/Y");?>" disabled />
									</div>
								</div>

								<div class="form-group">
						      <div class="portlet-body" style="margin: 0px 10px 0px 10px">
										<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
											<thead>
												<tr class="info" >
													<th>No</th>
													<th>Golongan Darah</th>
													<th>Rhesus</th>
													<th>Jumlah</th>
													<th>Harga Satuan</th>
													<th>Total</th>
												</tr>
											</thead>
											<tbody id="detailriwayatpenerimaan">
												<tr><td colspan="6" style="text-align:center">Tidak ada detail</td></tr>
											</tbody>
										</table>
									</div>
									<div class="pull-right" style="margin-right:20px">
										<div class="form-group">
											<div class="col-md-2" style="width:150px; margin-top:15px;">
												Sub Total :
											</div>
											<div class="col-md-2">
												<label class="control-label pull-right subtotalriwayat" style="font-size:2em;">0</label>
											</div>	
										</div>
										<div class="form-group">
											<div class="col-md-2" style="width:150px; margin-top:15px;">
												Potongan :
											</div>
											<div class="col-md-2">
												<label class="control-label pull-right potonganriwayat" style="font-size:2em;">0</label>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-2" style="width:150px; margin-top:15px;">
												Grand Total :
											</div>
											<div class="col-md-2">
												<label class="control-label pull-right grandtotalriwayat" style="font-size:2em;">0</label>
											</div>
										</div>
						  			</div>
						  	</div>
			     		</form>
				  	</div>
				  	<div class="modal-footer">
				 	 		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
		      	</div>
					</div>
				</div>
			</div>

      <div class="tab-pane" id="penerimaan">
    		<div class="dropdown"  style="margin-left:10px;width:98.5%">
          <div id="titleInformasi">Penerimaan Darah</div>
         
        </div>
        <br>
        
        	<form class="form-horizontal" role="form" method="post" id="frmpenerimaandarah">
        		<div class="informasi">
						<div class="form-group">
			  			<label class="control-label col-md-3">No Penerimaan</label>
			  			<div class="col-md-3">
			  				<input type="text" class="form-control" placeholder="Nomor Penerimaan" id="nmrTrimaTD" name="nmrTrimaTD">
			  			</div>
			  		</div>
						<div class="form-group">
		    			<label class="control-label col-md-3">Tanggal</label>
		    			<div class="col-md-3">
		    				<input type="text" style="cursor:pointer;background-color:white" id="tglTrimaTD" name="tglTrimaTD" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
		    			</div>
		    		</div>
		    		<div class="form-group">
		    			<label class="control-label col-md-3">Penyedia</label>
		    			<div class="col-md-3">
								<input type="text" class="form-control" id="penyediaTD" name="penyediaTD" placeholder="Penyedia" required />
		    			</div>
		    		</div>

          				<a href="#modalTerimaTD" data-toggle="modal"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Transfusi Darah" style="margin-left : -30px">&nbsp;Tambah Darah</i></a>
				</div>
						<div class="clearfix"></div>

						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
								<thead>
									<tr class="info" >
										<th>Nama</th>
										<th>Golongan Darah</th>
										<th>Rhesus</th>
										<th width="200">Jumlah</th>
										<th width="200">Harga Satuan</th>
										<th>Total</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="addinputTransfusi">
									<tr class="kosong"><td colspan="7" style="text-align:center">Tambah Penerimaan Darah</td></tr>
								</tbody>
							</table>

							<div class="form-group">
								<div class="col-md-2 pull-right">
									<label class="control-label pull-right" style="font-size:1.8em;margin-top:-10px;"><span id="subtotalpenerimaan"></span></label>
								</div>
								<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; margin-right:19px; text-align:right;">
									Sub Total(Rp.) :
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-2 pull-right" style="width:140px;">
									<input type="text" class="form-control text-right" id="potongan" name="potongan" placeholder="0" />
								</div>
								<div class="col-md-2 pull-right" style="width:100px;">
									<select class="form-control select" name="jenispotongan" id="selectpotongan" >
										<option value="persen" selected>%</option>
										<option value="rp">Rp. </option>
									</select>
								</div>
								<div class="col-md-2 pull-right" style="width:150px; margin-top:5px; text-align:right;">
									Potongan :
								</div>
							</div>

							<div class="form-group" style="margin-top:-10px;">
								<div class="col-md-2 pull-right" style="width:240px;">
									<label class="control-label pull-right" style="font-size:2em;color:red;"><span id="grandtotal"></span></label>
								</div>
								<div class="col-md-2 pull-right" style="width:150px; margin-top:15px; text-align:right;">
									Grand Total :
								</div>
							</div>

							<br>
							<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
							<div style="margin-left:80%">
								<span style="padding:0px 10px 0px 10px;">
									<button type="reset" id="resetpenerimaan" class="btn btn-warning">RESET</button> &nbsp;
									<button type="submit" class="btn btn-success">SIMPAN</button>
								</span>
							</div>
							<br>
						</div>
          </form>
      </div>

			<div class="modal fade" id="modalTerimaTD" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
							<h3 class="modal-title" id="myModalLabel">Pilih Darah</h3>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<form class="form-horizontal" role="form" method="post" id="caridarahpenerimaan">
									<div class="form-group">
										<div class="col-md-4" style="margin-left:10px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncicaridarah" placeholder="Nama"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>
									</div>
								</form>
								<div style="margin-left:10px; margin-right:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover" style="table-layout:fixed" id="tabelsearchdarah">
										<thead>
											<tr class="info">
												<th>Nama</th>
												<th width="15%">Gol. Darah</th>
												<th width="15%">Rhesus</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodydarahpenerimaan">
											<tr>
												<td style="text-align:center;" class="kosong" colspan="4">Cari Darah</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						</div>
					</div>
				</div>
			</div>

      <div class="tab-pane" id="riwayat">
    		<div class="dropdown"  style="margin-left:10px;width:98.5%">
	        <div id="titleInformasi">Riwayat Penerimaan Darah</div>
	        
        </div>
        <br>
        	<form class="form-horizontal" role="form">
        		<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<?php
								if(isset($totalriwayat))
								{
									echo '<input type="hidden" id="totalriwayat" value="'.$totalriwayat['total'].'">';
								}else{
									echo '<input type="hidden" id="totalriwayat" value="0">';
								}
							?>
							<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tabelriwayat">
								<thead>
									<tr class="info" >
										<th width="10">No</th>
										<th>Nomor Penerimaan</th>
										<th>Tanggal</th>
										<th>Penyedia</th>
										<th>Subtotal</th>
										<th>Potongan</th>
										<th>Grand Total</th>
										<th style="width:10px ">Action</th>
									</tr>
								</thead>
								<tbody id="tbodyriwayatpenerimaan">
									<?php
										if(isset($riwayatdarah)){
											if(!empty($riwayatdarah)){
												$i = 0;
												foreach ($riwayatdarah as $value) {
													$tgl = DateTime::createFromFormat('Y-m-d', $value['tgl_penerimaan']);
													echo '<tr>
														<td align="center">'.(++$i).'</td>
														<td>'.$value['no_penerimaan'].'</td>
														<td>'.$tgl->format('d F Y').'</td>
														<td>'.$value['penyedia'].'</td>
														<td class="text-right">'.number_format($value['subtotal'], 0, '', '.').'</td>
														<td class="text-right">'.number_format($value['potongan'], 0, '', '.').'</td>
														<td class="text-right">'.number_format($value['grandtotal'], 0, '', '.').'</td>
														<td class="text-center">
															<input type="hidden" class="riwayat_id" value="'.$value['id'].'">
															<a href="#detailTD" class="viewriwayat" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>
														</td>
													</tr>';
												}
											}
										}
									?>
								</tbody>
							</table>
						</div>
        	</form>
      </div>
    </div>
	</div>
