<script type="text/javascript">
  $(document).ready(function(){
      jQuery(function($) {
        $('#jmlhTransfusi').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0', vMin:'0'});
        $('#potongan').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0', vMin:'0'});
      });

      //tambah inventori
      $('#frminventori').submit(function(e){
        e.preventDefault();
        var x = confirm('Yakin akan menambahkan data? Data yang sudah ditambahkan tidak dapat dihapus.');
        if (x) {
          var item={};
          item['nama'] = $('#namaTD').val();
          item['gol_darah'] = $('#golDarahTD').val();
          item['rhesus'] = $('#rhesusTD').val();
          item['stok'] = $('#jmlhTransfusi').autoNumeric('get');

          $.ajax({
            type: "POST",
            data: item,
            url: "<?php echo base_url()?>transfusidarah/hometransfusidarah/adddarah",
            success: function(data){
              console.log(data);
              myAlert('DATA BERHASIL DITAMBAHKAN');
              window.location.replace("<?php echo base_url()?>transfusidarah/hometransfusidarah");
            },
            error: function(data){
              console.log(data);
            }
          })
        }else {
          return false;
        }
      })

      //search darah di popup
      $('#caridarahpenerimaan').submit(function(e){
        e.preventDefault();
        var katakunci = {};
        katakunci['search'] = $('#katakuncicaridarah').val();
        $.ajax({
          type: "POST",
          data:katakunci,
          url: "<?php echo base_url()?>transfusidarah/hometransfusidarah/searchdarah",
          success: function(data){
            $('#tbodydarahpenerimaan').empty();
            if (data.length > 0) {
              for (var i = 0; i < data.length; i++) {
                $('#tbodydarahpenerimaan').append(
                  '<tr>'+
                    '<td  style="word-wrap: break-word;white-space: pre-wrap;">' + data[i]['nama']+'</td>'+
                    '<td>' + data[i]['gol_darah']+'</td>'+
                    '<td>' + data[i]['rhesus']+'</td>'+
                    '<td style="text-align:center">'+
                      '<input type="hidden" class="iddarahstok" value="'+data[i]['id']+'">'+
                      '<a href="#" class ="addNewDarah"><i class="glyphicon glyphicon-check"></i></a>'+
                    '</td>'+
                  '</tr>'
                )
              };
            }
          },
          error: function(data){
            console.log(data);
          }
        })
      });

      //pilih darah
      var counter = 0;
      $('#tbodydarahpenerimaan').on('click', 'tr td a.addNewDarah', function(e){
        e.preventDefault();
        counter += 1;
        var nama = $(this).closest('tr').find('td').eq(0).text();
        var gol_darah = $(this).closest('tr').find('td').eq(1).text();
        var rhesus = $(this).closest('tr').find('td').eq(2).text();
        var idstok = $(this).closest('tr').find('td .iddarahstok').val();

         $('#addinputTransfusi').find('tr.kosong').remove();
         var newtabel =  '<tr><td>'+nama+'</td>'+
                           '<td>'+gol_darah+'</td>'+
                           '<td>'+rhesus+'</td>'+
                           '<td align="center">'+
                            '<div class="input-group" style="width:200px;">'+
                              '<input type="text" class="form-control text-right qtyjumlah" id="qtyjumlah'+counter+'" placeholder="0">'+
                              '<div class="input-group-addon">kantong</div>'+
                            '</div>'+
                            '<input type="hidden" class="val_qtyjumlah" value="0">'+
                           '</td>'+
                           '<td align="center">'+
                            '<div class="input-group" style="width:230px;">'+
                              '<div class="input-group-addon">Rp</div>'+
                              '<input type="text" class="form-control text-right hargasatuan" id="hargasatuan'+counter+'" placeholder="0">'+
                            '</div>'+
                            '<input type="hidden" class="val_hargasatuan" value="0">'+
                           '</td>'+
                           '<td align="right">0</td>'+
                           '<td align="center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
                           '<td style="display:none">'+idstok+'</td>'+
                         '</tr>';
        $('#addinputTransfusi').append(newtabel);
        $('#qtyjumlah'+counter+'').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0', vMin:'0'});
        $('#hargasatuan'+counter+'').autoNumeric('init',{aSep: '.', aDec: ',', mDec: '0', vMin:'0'});
      });

      $('#addinputTransfusi').on('change', 'tr td .qtyjumlah', function (e) {
        var harga = $(this).closest('tr').find('td .hargasatuan').autoNumeric('get');
  			var jumlah = $(this).autoNumeric('get');
  			var total = Number(jumlah) * Number(harga);
        $(this).closest('tr').find('td').eq(5).html(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        $(this).closest('tr').find('.val_qtyjumlah').val(jumlah);

        hitung_penerimaan();
      });

      $('#addinputTransfusi').on('change', 'tr td .hargasatuan', function (e) {
        var harga = $(this).autoNumeric('get');
        var jumlah = $(this).closest('tr').find('td .qtyjumlah').autoNumeric('get');
        var total = Number(jumlah) * Number(harga);
        $(this).closest('tr').find('td').eq(5).html(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        $(this).closest('tr').find('.val_hargasatuan').val(harga);

        hitung_penerimaan();
      });

      $('#selectpotongan').on('change', function(e){
        e.preventDefault();
        hitung_penerimaan();
      })

      $('#potongan').on('change', function(e){
        e.preventDefault();
        hitung_penerimaan();
      })

      $('#addinputTransfusi').on('click', 'tr td a.removeRow', function(e){
        e.preventDefault();
        $(this).closest('tr').remove();
        hitung_penerimaan();
      })

      $('#resetpenerimaan').on('click', function(e){
        $('#addinputTransfusi').empty('');
  			$('#addinputTransfusi').append('<tr class="kosong"><td colspan="7" style="text-align:center">tambah penerimaan barang</td></tr>');
      })

      //submit penerimaan darah
      $('#frmpenerimaandarah').submit(function(e){
        e.preventDefault();

        var x = confirm("Yakin akan menambahkan data? Data yang sudah ditambahkan tidak dapat dihapus.");
        if (x) {
          var item = {};
          item['no_penerimaan'] = $('#nmrTrimaTD').val();
          item['tgl_penerimaan'] = $('#tglTrimaTD').val();
          item['penyedia'] = $('#penyediaTD').val();

          $('#addinputTransfusi').find('tr.kosong').remove();
          var data = hitung_penerimaan();
          if (data.length == 0) {
            alert('Isi detail penerimaan!');
            $('#addinputTransfusi').append('<tr class="kosong"><td colspan="7" style="text-align:center">tambah penerimaan barang</td></tr>');
    				return false;
          }
          item['data'] = data;
          item['subtotal'] = $('#subtotalpenerimaan').text().toString().replace(/[^\d\,\-\ ]/g, '');
          item['potongan'] = potong;
          item['grandtotal'] = $('#grandtotal').text().toString().replace(/[^\d\,\-\ ]/g, '');
          
          $.ajax({
            type: "POST",
            data: item,
            url: "<?php echo base_url()?>transfusidarah/hometransfusidarah/addpenerimaandarah",
            success: function(data){
              console.log(data);
              resetpenerimaan();
              myAlert('DATA BERHASIL DITAMBAHKAN');

              //update tabel
              var tot = Number($('#totalriwayat').val()) + 1;
              $('#totalriwayat').val(tot);
              var tabel = $('#tabelriwayat').DataTable();
              var act = '<input type="hidden" class="riwayat_id" value="' + data + '">'+
              '<a href="#detailTD" class="viewriwayat" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>';

              tabel.row.add([
                tot,
                item['no_penerimaan'],
                item['tgl_penerimaan'],
                item['penyedia'],
                item['subtotal'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
                item['potongan'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
                item['grandtotal'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
                act
              ]).draw();
              $('#tabs a[href="#riwayat"]').tab('show');
            },
            error: function(data){
              console.log(data);
            }
          });
        }else{
          resetpenerimaan;
          return false;
        }

      })

      //detail riwayat penerimaan darah
      $('#tbodyriwayatpenerimaan').on('click', 'tr td .viewriwayat', function(e){
        e.preventDefault();
        var id = $(this).closest('tr').find('td .riwayat_id').val();
        $('#noPenTD').val($(this).closest('tr').find('td').eq(1).text());
        $('#pnyediaTD').val($(this).closest('tr').find('td').eq(3).text());
        $('#tglTD').val($(this).closest('tr').find('td').eq(2).text());
        $('.subtotalriwayat').html($(this).closest('tr').find('td').eq(4).text());
        $('.potonganriwayat').html($(this).closest('tr').find('td').eq(5).text());
        $('.grandtotalriwayat').html($(this).closest('tr').find('td').eq(6).text());

        $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>transfusidarah/hometransfusidarah/getdetailriwayat/" + id,
          success:  function (data) {
            console.log(data);
            if (data.length > 0) {
		    			$('#detailriwayatpenerimaan').empty();
		    			for (var i = 0; i < data.length; i++) {
                var total = Number(data[i]['jumlah']) * Number(data[i]['harga']);
		    				$('#detailriwayatpenerimaan').append(
			    				'<tr><td>'+(i+1)+'</td>'+
			    				'<td>'+data[i]['gol_darah']+'</td>'+
			    				'<td>'+data[i]['rhesus']+'</td>'+
			    				'<td class="text-right">'+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
			    				'<td class="text-right">'+data[i]['harga'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
			    				'<td class="text-right">'+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
			    				'</tr>'
			    			)
		    			};
		    		};
		    	},
		    	error: function (data) {

		    	}
        })
      });

      //popup in out stok darah
      var tdstok ='';
      $('#tbodyinventoridarah').on('click', 'tr td a.inoutdarah', function(e){
        e.preventDefault();
        tdstok = $(this);
        $('#id_darah_inout').val($(this).closest('tr').find('td .darah_inout').val());
        var stok = $(this).closest('tr').find('td').eq(4).text();

        $('#sisaInOutUTD').val(stok);

        $('#jmlInOutUTD').on('change', function(e){
            e.preventDefault();

            var is_in = $('#ioUTD').find('option:selected').val();
            var jmlhInOut = $('#jmlInOutUTD').val();
            var sisa = stok;
            var hasil = "";
            if (is_in == 'IN'){
              hasil = Number(sisa) + Number(jmlhInOut);
            }else {
              hasil = Number(sisa) - Number(jmlhInOut);
            };

            if (jmlhInOut == ''){
              hasil = Number(sisa);
            }
            $('#sisaInOutUTD').val(hasil);
        })

        $('#ioUTD').on('change', function(){
          var jumlah = Number($('#jmlInOutUTD').val());
          var sisa = Number(stok);
          var is_out = $('#ioUTD').find('option:selected').val();
          if (is_out === 'IN') {
            $('#sisaInOutUTD').val(sisa + jumlah);
          }
          else {
            $('#sisaInOutUTD').val(sisa - jumlah);
          };
        })
      });

      $('#simpaninout').on('click', function(e){
        e.preventDefault();
        var item = {};
        item['id'] = $('#id_darah_inout').val();
        item['stok'] = $('#sisaInOutUTD').val();
        $.ajax({
          type: "POST",
          data: item,
          url: "<?php echo base_url()?>transfusidarah/hometransfusidarah/inoutdarah",
          success: function(data){
            console.log(data);
            tdstok.closest('tr').find('td.stokval').text($('#sisaInOutUTD').val());
            $('#sisaInOutUTD').val('');
            myAlert('DATA BERHASIL DIUBAH');
          },
          error: function(data){
            console.log(data);
            $('#sisaInOutUTD').val('');
          }
        })
      });

  }); //end of document ready

  var potong = 0;
  function hitung_penerimaan(){
    var data = [];
	    $('#addinputTransfusi').find('tr').each(function (rowIndex, r) {
	        var cols = [];
	        $(this).find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });
	       	$(this).find('td input').each(function (colIndex, c) {
	            cols.push(c.value);
	        });
	        data.push(cols);
	    });
    var jumlah = 0;
console.log(data);
    for (var i = data.length - 1; i >= 0; i--) {
			jumlah += Number(data[i][5].toString().replace(/[^\d\,\-\ ]/g, ''));
		};

    var jenispotongan = $('#selectpotongan').find('option:selected').val();
    var potongan = Number($('#potongan').autoNumeric('get'));
    var grandtotal = 0;

    if (jenispotongan === 'persen') {
      potong = (jumlah * (potongan / 100));
      grandtotal = (jumlah - potong);
    }else {
      potong = potongan;
      grandtotal = (jumlah - potong);
    };

    $('#subtotalpenerimaan').text(jumlah.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
    $('#grandtotal').text(grandtotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

    return data;
  };

  function resetpenerimaan()
  {
    $('#nmrTrimaTD').val('');
    $('#penyediaTD').val('');
    $('tglTrimaTD').val('<?php echo date("d/m/Y");?>');
    $('#addinputTransfusi').empty('');
    $('#addinputTransfusi').append('<tr class="kosong"><td colspan="7" style="text-align:center">tambah penerimaan barang</td></tr>');
    $('#subtotalpenerimaan').html('0');
    $('#potongan').val('');
    $('#grandtotal').html('0');
  };

</script>
