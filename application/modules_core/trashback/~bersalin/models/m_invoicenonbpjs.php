<?php
class m_invoicenonbpjs extends CI_Model{
	public function get_visit_id($invoice){
		$sql = "SELECT * FROM tagihan WHERE no_invoice = '$invoice'";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}

	public function get_data_pasien($id){
		$sql = "SELECT * FROM visit v, pasien p WHERE v.rm_id = p.rm_id AND v.visit_id = '$id'";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}

	public function get_master_dept(){
    	$sql = "SELECT * FROM master_dept WHERE jenis = 'RAWAT INAP' AND nama_dept <> 'KAMAR OPERASI'";
    	$query = $this->db->query($sql);
    	$result = $query->result_array();
    	return $result;
    }

    public function get_dataakomodasi($sub_visit){
    	$sql = "SELECT makan_id, v.unit_tujuan as dept_id, vm.paket_makan as paket_id, g.harga_total as tarif FROM visit_permintaan_makan vm, visit_ri v, gizi_paket_makan g WHERE vm.sub_visit = v.ri_id AND g.id = vm.paket_makan AND vm.sub_visit = '$sub_visit' AND vm.makan_id NOT IN (SELECT makan_id FROM tagihan_akomodasi) AND vm.status = 'Sudah Dikirim' ORDER BY vm.makan_id ASC";
    	$query = $this->db->query($sql);
    	$result = $query->result_array();
    	return $result;	
    }

    public function insert_tagihanakomodasi($value=''){
		$query = $this->db->insert('tagihan_akomodasi',$value);
    	if ($query) {
    		return true;
    	}else{
    		return false;
    	}
    }

    public function get_tagihanakomodasi($invoice){
    	$sql = "SELECT *, t.id as tmakan_id FROM tagihan ta, tagihan_akomodasi t, master_dept m, gizi_paket_makan g WHERE t.dept_id = m.dept_id AND t.paket_id = g.id AND ta.no_invoice = t.no_invoice AND ta.no_invoice = '$invoice'";
    	$query = $this->db->query($sql);
    	$result = $query->result_array();
    	return $result;	
    }

    public function get_paket_makan(){
    	$sql = "SELECT * FROM gizi_paket_makan";
    	$query = $this->db->query($sql);
    	$result = $query->result_array();
    	return $result;		
    }

    public function get_datatagihan($id){
    	$sql = "SELECT * FROM tagihan_akomodasi WHERE id='$id' LIMIT 1";
    	$query = $this->db->query($sql);
    	$result = $query->row_array();
    	return $result;
    }

    public function hapus_permintaan($id){
    	$result = $this->db->delete('visit_permintaan_makan',array('makan_id'=>$id));
        return $result;
    }

    public function hapus_tagihanakomodasi($id){
    	$result = $this->db->delete('tagihan_akomodasi',array('id'=>$id));
        return $result;
    }

    public function get_datakamar($ri_id){
    	$sql = "SELECT v.inap_id, v.waktu_masuk as tgl_masuk, v.waktu_keluar as tgl_keluar, v.kamar_id, v.bed_id, m.tarif_kamar as tarif FROM visit_inap_kamar v, master_kamar m WHERE v.kamar_id = m.kamar_id AND v.ri_id = '$ri_id' AND v.inap_id NOT IN (SELECT inap_id FROM tagihan_kamar)";
    	$query = $this->db->query($sql);
    	$result = $query->result_array();
    	return $result;
    }

    public function get_preparekamarbpjs($kamar_id){
        $sql = "SELECT nama_kamar FROM master_kamar WHERE kamar_id = '$kamar_id' LIMIT 1";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['nama_kamar'];
    }

    public function get_kamarbpjs($nama, $kelas){
        $sql = "SELECT tarif_kamar FROM master_kamar WHERE nama_kamar LIKE '$nama%' AND kelas_kamar = 'Kelas $kelas' LIMIT 1";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['tarif_kamar'];
    }

    public function insert_tagihankamar($value=''){
		$query = $this->db->insert('tagihan_kamar',$value);
    	if ($query) {
    		return true;
    	}else{
    		return false;
    	}
    }

    public function get_tagihankamar($invoice){
    	$sql = "SELECT *, t.id as tkamar_id FROM tagihan_kamar t, master_kamar m WHERE t.kamar_id = m.kamar_id AND t.no_invoice = '$invoice'";
    	$query = $this->db->query($sql);
    	$result = $query->result_array();
    	return $result;	
    }

    public function get_preparemakan($id){
        $sql = "SELECT nama_paket FROM gizi_paket_makan WHERE id = '$id' LIMIT 1";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['nama_paket'];
    }

    public function get_makanbpjs($nama, $kelas){
        $sql = "SELECT harga_total FROM gizi_paket_makan WHERE nama_paket LIKE '$nama%' AND kelas = 'Kelas $kelas' LIMIT 1";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['harga_total'];
    }

    public function update_tagihankamar($id,$insert)
    {
        $this->db->where('id', $id);
        $update = $this->db->update('tagihan_kamar', $insert);
        if($update)
            return true;
        else
            return false;
    }

    public function update_tagihanmakan($id,$insert)
    {
        $this->db->where('id', $id);
        $update = $this->db->update('tagihan_akomodasi', $insert);
        if($update)
            return true;
        else
            return false;
    }
}
?>