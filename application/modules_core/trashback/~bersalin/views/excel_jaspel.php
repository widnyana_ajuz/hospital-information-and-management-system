<?php

$sekarang = str_replace('-', "_", date('m-Y'));
$title = 'Laporan_Jaspel'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>LAPORAN JASA PELAYANAN</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong><?php echo strtoupper($nama_dept) ?> RS BAHAYANGKARA PALANGKARAYA</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Laporan per </strong> 
	<?php echo $awal; ?> <strong>sampai</strong> <?php echo $akhir; ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th width="3%" style="text-align:center">No.</th>
					<th style="text-align:center" colspan="2">Tanggal</th>
					<th style="text-align:center" colspan="2">Tindakan</th>
					<th style="text-align:center" colspan="2">Cara Bayar</th>
					<th style="text-align:center" colspan="2">Paramedis</th>
					<th style="text-align:center" colspan="2">Paramedis Lain</th>
					<th style="text-align:center" colspan="2">Jasa Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					if (isset($jaspel) && !empty($jaspel)) {
						$i = 0;
						foreach ($jaspel as $value) {
							$tgl = DateTime::createFromFormat('Y-m-d', $value['tanggal']);
							echo '<tr>
									<td style="text-align:center">'.(++$i).'</td>
									<td style="text-align:center" colspan="2">'.$tgl->format('d F Y').'</td>
									<td style="text-align:center" colspan="2">'.$value['nama_tindakan'].'</td>
									<td style="text-align:center" colspan="2">'.$value['cara_bayar'].'</td>
									<td style="text-align:center" colspan="2">'.$value['nama_petugas'].'</td>
									<td style="text-align:center" colspan="2">'.$value['paramedis_lain'].'</td>
									<td style="text-align:center" colspan="2">'.$value['jp'].'</td>
									
								</tr>';
						}
					}
				?>
			</tbody>
		</table>
	</div>

</body>
</html>
