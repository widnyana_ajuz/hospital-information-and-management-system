<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>bersalin/homebersalin">PASIEN BERSALIN</a>
		<input type="hidden" class="my_dept_id" value="<?php echo $dept_id ?>">
	</li>
</div>
<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	   	<li class="active"><a href="#list" data-toggle="tab">List Pasien Bersalin</a></li>
	    <li><a href="#datakamar" data-toggle="tab">Data Kamar</a></li>
	    <li><a href="#farmasi" data-toggle="tab">Farmasi</a></li>
	    <li><a href="#logistik" data-toggle="tab">Logistik</a></li>
	    <li><a href="#laporan" data-toggle="tab">Laporan</a></li>
	    <li><a href="#master" data-toggle="tab">Master</a></li>
	    <li><a href="#tagihan" data-toggle="tab">Tagihan</a></li>
	    <li><a href="#dipulangkan" data-toggle="tab">Pasien Dipulangkan</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">

	<div class="modal fade" id="pindahkan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form id="submitpindahkan" method="POST" class="form-horizontal" role="form">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pindah Pasien</h3>
        			</div>	
        			<div class="modal-body">
        				<div class="form-group">
							<label class="control-label col-md-3" >Tanggal Periksa </label>
							<div class="col-md-3">	
								<input date-date-format="dd/mm/yyyy H:i:s" value="<?php echo date("d/m/Y H:i:s");?>" type="text" class="form-control" name="date" id="inputdate" placeholder="Date Now" disabled/>
							</div>				
						</div>	
						
						<div class="form-group">
							<label class="control-label col-md-3">No. Rekam Medis</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="modal_no_rm" name="noRm" placeholder="No Rekam Medis" disabled>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Nama Pasien</label>
							<div class="col-md-7">
								<input type="text" id="modal_nama" class="form-control" name="nama" placeholder="Nama Pasien" disabled>
							</div>
						</div>
													
						<div class="form-group">
							<label class="control-label col-md-3">Cara Bayar</label>
							<div class="col-md-5">
								<select class="form-control select" name="carabayar" id="carabayar">
									<option value="" selected>--Pilih Cara Bayar--</option>
									<option value="Umum">Umum</option>
									<option value="BPJS" id="op-bpjs">BPJS</option>
									<option value="Jamkesmas" >Jamkesmas</option>
									<option value="Asuransi" id="op-asuransi">Asuransi</option>
									<option value="Kontrak" id="op-kontrak">Kontrak</option>
									<option value="Gratis" >Gratis</option>
									<option value="Lain-laun">Lain-lain</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group" id="asuransi">
							<label class="control-label col-md-3">Nama Asuransi</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="namaAsuransi" name="namaAsuransi" placeholder="Nama Asuransi">
							</div>
						</div>
								
						<div class="form-group" id="kontrak">
							<label class="control-label col-md-3">Nama Perusahaan</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="namaPerusahaan" name="namaPerusahaan" placeholder="Nama Perusahaan">
							</div>
						</div>

						<div class="form-group" id="kelas">
							<label class="control-label col-md-3">Kelas Pelayanan </label>
							<div class="col-md-5">
								<select class="form-control select" name="kelasBpjs" id="kelas_pelayanan">
									<option value="" selected>--Pilih Kelas--</option>
									<option value="III">III</option>
									<option value="II">II</option>
									<option value="I"  >I</option>
									<option value="Utama" >Utama</option>
									<option value="VIP">VIP</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group" id="noAsuransi">
							<label class="control-label col-md-3">Nomor Asuransi</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="nomorAsuransi" name="nomorAsuransi" placeholder="Nomor Asuransi">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Cara Masuk</label>
							<div class="col-md-5">
								<select class="form-control select" name="caramasuk" id="caramasuk">
									<option value="" selected>--Pilih Cara Masuk--</option>
									<option value="Datang sendiri">Datang sendiri</option>
									<option value="Puskesmas"  >Puskesmas</option>
									<option value="Rujuk RS lain" >Rujuk RS lain</option>
									<option value="Instansi" >Instansi</option>
									<option value="Kasus Polisi" >Kasus Polisi</option>
									<option value="Rujukan Dokter" >Rujukan Dokter</option>
									<option value="Lain-laun">Lain-lain</option>
								</select>												
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Detail Cara Masuk</label>
							<div class="col-md-7">
								<textarea class="form-control" name="detailMasuk" id="detailmasuk" placeholder="Detail cara masuk .."></textarea> 
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Departemen Tujuan</label>
							<div class="col-md-6">
								<select class="form-control select" id="deptTujuan">
									<option value="" selected>--Pilih Departement--</option>
									<?php foreach( $departemen as $dep ) { ?>
										<option value="<?php echo $dep['dept_id']; ?>" >
											<?php echo $dep['nama_dept']; ?>
										</option>
										<?php } ?>
								</select>												
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Pilih Kamar & Kelas Kamar</label>
							<div class="col-md-4">
								<input type="hidden" id="kamar_id" name="kamar_id">
								<input type="hidden" id="bed_id" name="bed_id">
								<input type="text" class="form-control" id="textkamar" placeholder="Search Kamar" data-toggle="modal" data-target="#pilkamar" readonly="" style="cursor:pointer">
							</div>
						</div>				

						<!-- <div class="form-group">
							<label class="control-label col-md-3">Adminitrasi</label>
							<div class="col-md-5">
								<select class="form-control select" name="caramasuk" id="adminitrasi">
									<option value="" selected>Pilih Adminitrasi</option>
									<option value="1">Pasien Lama</option>
									<option value="0">Pasien Baru</option>
									<option value="NULL" >Pasien Lanjutan</option>
								</select>												
							</div>
						</div> -->
      				</div>
      				<div class="modal-footer">
      					<input type="hidden" id="visit_id_pindah">
      					<input type="hidden" id="ri_id_pindah">
      					<input type="hidden" id="bed_id_lama">
 			       		<button type="submit" class="btn btn-success">Simpan</button>
			      	</div>
        		</div>
        	</div>
        </form>
   	</div>

    <div class="modal fade" id="pilkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-300px">
    	<div class="modal-dialog">
    		<div class="modal-content" style="width:900px">
    			<div class="modal-header">
    				<button type="button" class="close" id="close-kamar" data-dismiss="modal" aria-hidden="true">X</button>
    				<h3 class="modal-title" id="myModalLabel">Pilih Kamar</h3>
    			</div>	
    			<div class="modal-body">

    				<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchPengirim">
							<thead>
								<tr class="success">
									<td>Kamar</td>
									<td>Kelas</td>
									<td>Jumlah Bed</td>
									<td>Terpakai</td>
									<td width="10%" style="text-align:center;">Pilih</td>
								</tr>
							</thead>
							<tbody id="tbody_kamar">
								
							</tbody>
						</table>												
					</div>
        			
  				</div>
  				<br>
  				<div class="modal-footer">
		       		<button type="button" id="modal-kamar" data-dismiss="modal" class="btn btn-warning">Kembali</button>	
		       	</div>
    		</div>
    	</div>        	
    </div>

		<div class="tab-pane active" id="list">
	       	<form method="POST" id="search_submit">
		       	<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">		
						<input type="text" id="search_bersalin" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-danger">Cari</button>
				</div>	
			</form>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">PASIEN BERSALIN</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabelutamapasienunit">
							<thead>
								<tr class="info">
									<th style="text-align:center;width:20px;">No.</th>
									<th> Nomor Rekam Medis </th>
									<th> Nama Lengkap </th>
									<th> Jenis Kelamin </th>
									<th> Tanggal Lahir </th>
									<th> Alamat Tinggal </th>
									<th> Unit Pengirim </th>
									<th style="width:30px"> Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 0;
									foreach ($pasien_bersalin as $data) {
										$i++;
										$tgl = strtotime($data['tanggal_lahir']);
										$hasil = date('d F Y', $tgl); 

										echo ' 
											<tr>
												<td>'.$i.'</td>
												<td>'.$data['rm_id'].'</td>
												<td>'.$data['nama'].'</td>
												<td>'.$data['jenis_kelamin'].'</td>
												<td style="text-align:center">'.$hasil.'</td>									
												<td>'.$data['alamat_skr'].'</td>
												<td>Admisi</td>
												<td style="text-align:center">
													<input type="hidden" class="ri_id" value="'.$data['ri_id'].'">
													<input type="hidden" class="b_id" value="'.$data['bed_id'].'">
													<input type="hidden" class="v_id" value="'.$data['visit_id'].'">
													<a href="#" data-toggle="modal" class="pindahpasien" data-target="#pindahkan"><i class="fa fa-external-link" data-toggle="tooltip" data-placement="top" title="Pindah"></i></a>
													<a href="bersalindetail/daftar/'.$data['ri_id'].'/'.$data['visit_id'].'"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a>
												</td>										
											</tr>
										';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>  
	    </div>

        <div class="tab-pane" id="datakamar">       
        	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
				<table class="table table-striped table-bordered table-hover tableDT" id="tabelkamarunit" style="font-size:99.5%">
					<thead>
						<tr class="info">
							<th width="20">No.</th>
							<th>Kamar</th>
							<th>Kelas</th>
							<th>Jumlah Bed</th>
							<th>Terpakai</th>
							<th>Tersedia</th>
							<th width="80" style="text-align:center;">Pilih</th>
						</tr>
					</thead>
					<tbody>
						<?php  
							if (isset($all_kamar_unit) && !empty($all_kamar_unit)) {
								$i = 0;
								foreach ($all_kamar_unit as $value) {
									echo '<tr>
										<td align="center">'.(++$i).'</td>
										<td>'.$value['nama_kamar'].'</td>
										<td>'.$value['kelas_kamar'].'</td>
										<td align="right">'.$value['jumlah'].'</td>
										<td align="right">'.$value['terpakai'].'</td>
										<td align="right">'.(intval($value['jumlah'] - intval($value['terpakai']))).'</td>
										<td style="text-align:center;">
											<a href="#detkamar" class="viewdetailkamar" data-toggle="modal">
											<input type="hidden" class="kamar_id_detail" value="'.$value['kamar_id'].'">
											<i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View"></i></a>
										</td>
									</tr>';	
								}
							}
						?>
					</tbody>
				</table>												
			</div>

			<div class="modal fade" id="detkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-400px">
				<div class="modal-dialog">
					<div class="modal-content" style="width:1000px;">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title titlekamar" id="myModalLabel"></h3>
				   			</div>
							<div class="modal-body">

								<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover table-responsive" id="tabeldetailkamar">
										<thead>
											<tr class="info">
												<th width="20">No.</th>
												<th> Nomor Bed</th>
												<th> Status</th>
												<th> Pasien</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td style="text-align:center" colspan="4">Tidak ada data bed</td>
											</tr>
										</tbody>
									</table>
								</div>
			       			</div>
			        		<br>
			        		<div class="modal-footer">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						    </div>
					</div>
				</div>
			</div>
        </div>

        <div class="tab-pane" id="farmasi">

        	<div class="dropdown" id="btnBawahInventori" >
	            <div id="titleInformasi">Inventori</div>
	            <div id="btnBawahInventori" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="infoInventori">
				<div class="form-group">
	            	<form class="form-horizontal informasi" role="form" method="post" id="submitfilterfarmasiunit">
		            	<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
						<div class="col-md-2" style="width:200px">
							<select class="form-control select" name="filterInv" id="filterInv">
								<option value="" selected>Pilih</option>
								<option value="jenis">Jenis Obat</option>
								<option value="merek">Merek</option>
								<option value="nama">Nama Obat</option>							
							</select>	
						</div>
						<div class="col-md-2" style="margin-left:-15px; width:200px;" >
							<input type="text" class="form-control" id="filterby" name="valfilter" placeholder="Value"/>
						</div>
						<div class="col-md-1" >
							<button type="submit" id="filterfarmasiunit" class="btn btn-warning">FILTER</button> 
						</div>
					</form>
					<div class="col-md-1" >
						<button class="btn btn-danger" id="expired">EXPIRED</button> 
					</div>
					<div class="col-md-1" >
						<button class="btn btn-warning" id="expired3">EX 3 BULAN</button>
					</div>
					<div class="col-md-1" style="margin-left: 20px;">
						<button class="btn btn-warning" id="expired6">EX 6 BULAN</button>
					</div>
				</div>
				<br><br>
				<div class="form-group" >
					<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabelinventoriunit">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th> Nama Obat </th>
									<th> No Batch </th>
									<th> Harga Jual </th>
									<th> Merek </th>
									<th> Stok</th>
									<th> Satuan </th>
									<th width="200"> Tanggal Kadaluarsa </th>
									<th width="100"> Action </th>								
								</tr>
							</thead>
							<tbody id="tbodyinventoriunit">
								<?php  
									if (isset($obatunit)) {
										$i = 1;
										foreach ($obatunit as $value) {
											$tgl = DateTime::createFromFormat('Y-m-d', $value['tgl_kadaluarsa']);
											echo '<tr>'.
												'<td align="center">'.($i++).'</td>'.
												'<td>'.$value['nama'].'</td>'.
												'<td align="right">'.$value['no_batch'].'</td>'.
												'<td align="right">'.$value['harga_jual'].'</td>'.
												'<td>'.$value['nama_merk'].'</td>'.
												'<td align="right">'.$value['total_stok'].'</td>'.
												'<td>'.$value['satuan'].'</td>'.								
												'<td align="center">'.$tgl->format('d F Y').'</td>'.
												'<td align="center"><a href="#" class="inoutobat" data-toggle="modal" data-target="#inout"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'.
												'<a href="#edInvenBer" data-toggle="modal" class="printobat"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>'.
												'<input type="hidden" class="barangmerk_id" value="'.$value['merk_id'].'">'.
												'<input type="hidden" class="barangjenis_obat_id" value="'.$value['jenis_obat_id'].'">'.
												'<input type="hidden" class="barangsatuan_id" value="'.$value['satuan_id'].'">'.
												'<input type="hidden" class="barangobat_dept_id" value="'.$value['obat_dept_id'].'">'.
											'</td></tr>';
										}
									}
								?>
							</tbody>
						</table>
					</div>
					<button class="btn btn-info" id="excel_obat" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
				</div>
				<br><br>
	        </div>
			<div class="modal fade" id="inout" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal informasi" role="form" method="post" id="submitinoutunit">
					<div class="modal-dialog">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<div class="form-group">
		        					<label class="control-label col-md-3" >Tanggal 
									</label>
									<div class="col-md-4" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" id="tglInOut" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" >In / Out 
									</label>
									<div class="col-md-4">
						         		<select class="form-control select" name="iober" id="iober">
											<option value="IN" selected>IN</option>
											<option value="OUT">OUT</option>					
										</select>
									</div>	
								</div>
								<div class="form-group">
		        					<label class="control-label col-md-3" >Jumlah </label>
									<div class="col-md-4" >
					         			<input type="text" class="form-control" id="jmlInOutBer" name="jmlInOutBer" placeholder="Jumlah">
									</div>
								</div>
								<div class="form-group">
		        					<label class="control-label col-md-3" >Sisa Stok </label>
									<div class="col-md-4" >
					         			<input type="text" class="form-control" id="sisaInOutBer" name="sisaInOutBer" placeholder="Sisa Stok" readonly="">
									</div>
								</div>
								<div class="form-group">
		        					<label class="control-label col-md-3" >Keterangan </label>
									<div class="col-md-6" >
										<textarea class="form-control" id="keteranganIO" placeholder="Keterangan"></textarea>
									</div>
								</div>
		        			</div>
		        			<div class="modal-footer">
		        				<input type="hidden" id="inout_obat_dept_id">
		        				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			       		<button type="submit" class="btn btn-success">Simpan</button>
					      	</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal fade" id="edInvenBer" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
	        			<form class="form-horizontal" role="form">
			            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
								<thead>
									<tr class="info" >
										<th  style="text-align:center"> Waktu </th>
										<th  style="text-align:left"> IN / OUT </th>
										<th  style="text-align:left"> Jumlah </th>
										<th  style="text-align:left"> Stok Akhir </th>
									</tr>
								</thead>
								<tbody id="tbodydetailobatinventori">
									<tr>
										<td colspan="4" style="text-align:center">Tidak ada Detail</td>
									</tr>
								</tbody>
							</table>
						</form>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>

			<div class="dropdown" id="btnBawahMintaObat">
	            <div id="titleInformasi">Permintaan Farmasi</div>
	            <div id="btnBawahMintaObat" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoMintaObat">
            	<form class="form-horizontal" role="form" method="post" id="permintaanfarmasibersalin">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="noPermFarmBers" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanfarmasi" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="ketObatFarBers" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>
					<a href="#modalMintaFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<!-- <th width="20"> No. </th> -->
										<th> Nama Obat </th>
										<th>Tanggal Kadaluarsa</th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Stok Gudang </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputMintaFar" class="addKosong">
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:1100px">
							<span class="customSpan">
								<button class="btn btn-warning" type="button" id="batalpermintaanfarmasi">RESET</button>
								<button class="btn btn-success" type="submit">SIMPAN</button> 
							</span>
						</div>
					</div>	
				</form>
			</div>	    
			<br>
			<div class="modal fade" id="modalMintaFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<div class="form-group">
		        				<form method="post" class="form-horizontal" role="form" id="formobatfarmasibersalin">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncifarmasibersalin" placeholder="Nama Obat"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
								</form>
								<div style="margin-right:10px;margin-left:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 10px 0px 15px">
									<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa" style="font-size:99%;">
										<thead>
											<tr class="info">
												<th>Nama Obat</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Stok Gudang</th>
												<th>Tgl Kadaluarsa</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodyobatpermintaanfarmasi">
											<tr>
												<td colspan="6" style="text-align:center">Cari data Obat</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
	           	
	       	<div class="dropdown" id="btnBawahRetDepartemen">
	            <div id="titleInformasi">Retur Farmasi</div>
	            <div id="btnBawahRetFarmasi" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
           	<div id="infoRetDepartemen">
            	<form class="form-horizontal" role="form" method="post" id="formsubmitreturbersalin">
            		<div class="informasi">
            			<br>
            			<div class="form-group">
            				<div class="col-md-2">
            					<label class="control-label">Nomor Retur</label>
            				</div>
            				<div class="col-md-3">
            					<input type="text" class="form-control" name="noRetFarBers" id="noRetFarBers" placeholder="Nomor Retur"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
            					<label class="control-label">Tanggal Retur</label>
            				</div>
            				<div class="col-md-2">
            					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white;" class="form-control" id="waktureturbersalin" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
            			</div>
            			<div class="form-group">
							<div class="col-md-2">
            					<label class="control-label">Keterangan</label>
            				</div>
            				<div class="col-md-3">
								<textarea class="form-control" id="ketObatRetFarBers" name="ketObatRetFarBers"></textarea>	
							</div>
            			</div>
            		</div>

            		<a href="#modalRetFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left : 40px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>
					
					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabRetur">
								<thead>
									<tr class="info" >
										<th > Nama Obat </th>
										<th > Tanggal Kadaluarsa</th>
										<th > Satuan </th>
										<th > Merek </th>
										<th > Stok Unit </th>
										<th > Jumlah Retur </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputRetFar" class="addKosong">
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:1100px">
							<span class="customSpan">
								<button class="btn btn-warning" type="button" id="batalreturfarmasi">RESET</button>
								<button class="btn btn-success" type="submit">SIMPAN</button>
							</span>
						</div>
						<br>
					</div>
				</form>
			</div>
			<div class="modal fade" id="modalRetFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<div class="form-group">
		        				<form method="post" role="form" class="form-horizontal" id="formsearchobatretur">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncireturbersalin" placeholder="Nama Obat"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>
								</form>
								<div style="margin-left:10px; margin-right:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 10px 0px 15px">
									<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
										<thead>
											<tr class="info">
												<th>Nama Obat</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Stok Unit</th>
												<th>Tgl Kadaluarsa</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodyreturbersalin">
											<tr>
												<td style="text-align:center" colspan="6">Cari data Obat</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>	
			<br>
	    </div>

        <div class="tab-pane" id="logistik">
        	<div class="modal fade" id="modalbarang" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:800px">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
	        			</div>
	        			<div class="modal-body">

		        			<div class="form-group">
		        				<form method="post" class="form-horizontal" role="form" id="formmintabarang">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncimintabarang" placeholder="Nama barang"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
								</form>
								<div style="margin-right:10px;margin-left:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%">
										<thead>
											<tr class="info">
												<th>Nama Barang</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Tahun Pengadaan</th>
												<th>Stok Gudang</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodybarangpermintaan">
											<tr>
												<td colspan="6" style="text-align:center">Cari data Barang</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
	       	<div class="dropdown" id="btnBawahInventoriBarang">
	            <div id="titleInformasi">Inventori</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoInventoriBarang">
				<div class="form-group" >
					<div class="portlet-body" style="margin: 30px 10px 20px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tblinventorigudangunit">
							<thead>
								<tr class="info" >
									<th width="20">No.</th>
									<th > Nama Barang </th>
									<th > Merek </th>
									<th > Harga </th>
									<th > Stok </th>
									<th > Satuan </th>
									<th > Tahun Pengadaan</th>
									<th > Sumber Dana</th>
									<th width="100"> Action </th>
								</tr>
							</thead>
							<tbody id="tbodyinventoribarang">
								<?php 
									if (isset($inventoribarang)) {
										if (!empty($inventoribarang)) {
											$i = 1;
											foreach ($inventoribarang as $value) {
												echo '<tr>
														<td>'.($i++).'</td>
														<td>'.$value['nama'].'</td>
														<td>'.$value['nama_merk'].'</td>
														<td>'.$value['harga'].'</td>
														<td>'.$value['stok'].'</td>
														<td>'.$value['satuan'].'</td>
														<td>'.$value['tahun_pengadaan'].'</td>
														<td>'.$value['sumber_dana'].'</td>
														<td style="text-align:center">
															<input type="hidden" class="barang_detail_inout" value="'.$value['barang_detail_id'].'">
															<a href="#inoutbar" data-toggle="modal" class="edBarang" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
															<a href="#edInvenBerBar" data-toggle="modal" class="detailinvenbarang"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
														</td>
													</tr>';
											}
										}
									}
								?>
									
							</tbody>
						</table>
					</div>
					<form method="post" action="<?php echo base_url() ?>bersalin/homebersalin/excel_barang_unit">
						<button class="btn btn-info" type="submit" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
						<input type="hidden" class="my_dept_id" name="my_dept_id" value="<?php echo $dept_id ?>">
					</form>
					<br>
	        	</div>
	        </div>
			<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<form class="form-horizontal" role="form" style="margin-left:30px;" id="forminoutbarang">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<div class="form-group">
			        				<label class="control-label col-md-3" >Tanggal</label>
									<div class="col-md-6" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white;" id="tanggalinout" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
									</div>
								</div>
										
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" >In / Out </label>
									<div class="col-md-6">
						         		<select class="form-control select" name="io" id="io">
											<option value="IN" selected>IN</option>
											<option value="OUT">OUT</option>					
										</select>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Jumlah in/out</label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="jmlInOut" name="jmlInOut" placeholder="Jumlah">
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Sisa Stok </label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="sisaInOut" name="sisaInOut" placeholder="Sisa Stok" readonly>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Keterangan </label>
									<div class="col-md-6" >
										<textarea class="form-control" id="keteranganIObar" placeholder="Keterangan"></textarea>
									</div>
								</div>										
		        			</div>
		        			<div class="modal-footer">
		        				<input type="hidden" id="id_barang_inoutprocess">
		 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			       		<button type="submit" class="btn btn-success">Simpan</button>
					      	</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<form class="form-horizontal" role="form">
				            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th> Waktu </th>
											<th> IN / OUT </th>
											<th> Jumlah </th>
											<th> Keterangan </th>
										</tr>
									</thead>
									<tbody id="tbodydetailbrginventori">
										<tr>
											<td colspan="4" style="text-align:center">Tidak ada detail in-out</td>
										</tr>
											
									</tbody>
								</table>
							</form>
							
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
			<br>

			<div class="dropdown" id="btnBawahPermintaanBarang" style="margin-left:10px;width:98.5%">
	            <div id="titleInformasi">Permintaan Logistik</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoPermintaanBarang">
            	<form class="form-horizontal" role="form" method="post" id="permintaanbarangunit">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="nomorpermintaanbarang" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white;" id="tglpermintaanbarang" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="keteranganpermintaanbarang" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>
					<a href="#modalbarang" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Barang</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<th> Nama Barang </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Tahun Pengadaan </th>
										<th> Stok Gudang </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputmintabarang">
									<?php echo '<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>'; ?>
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:1100px">
							<span class="customSpan">
								<button class="btn btn-warning" type="reset" id="batalpermintaanfarmasi">RESET</button>
								<button class="btn btn-success" type="submit">SIMPAN</button>
							</span>
						</div>
						<br>
					</div>	
				</form>
			</div>	    
			<br>
	    </div>
	    
        <div class="tab-pane" id="laporan" style="margin-left:40px"> 
	        <div style="width:100%" id="sensusharian">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Harian</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
        			method="post" action="<?php echo base_url() ?>bersalin/homebersalin/sensus_harian">
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="input-group col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" class="form-control isian" name="start" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right" style="margin-right:30px">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>  
	        <br>
        	<div style="width:100%" id="regisharian">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Register Harian</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
        			method="post" action="<?php echo base_url() ?>bersalin/homebersalin/register_harian">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="input-group col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" name="start" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right" style="margin-right:30px">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>  
	        <br>
			<div style="width:100%" id="sensusbulanan">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Bulanan</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
        			method="post" action="<?php echo base_url() ?>bersalin/homebersalin/get_sensus_bulanan_ri">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="input-group col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;background-color:white" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-2 pull-right" style="margin-right:30px">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>
	        <br>
	        <div style="width:100%" id="kegiatankebidanan">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Kegiatan Kebidanan</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
        			method="post" action="<?php echo base_url() ?>bersalin/homebersalin/get_sensus_bulanan_ri">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="input-group col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" name="start" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-2 pull-right" style="margin-right:30px">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>
	        
	        <br>
        </div>

        <div class="tab-pane" id="master">  
        	<div class="dropdown" id="">
	            <div id="titleInformasi">Jasa Pelayanan</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <form class="form-horizontal" method="post" action="<?php echo base_url() ?>bersalin/homebersalin/print_jaspel">
	            <div id="">
			            <div class="informasi">
				            <div class="form-group">
								<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Periode :</label>
								<div class="col-md-3" style="margin-left:-15px">
									<div class="input-daterange input-group" id="datepicker">
									    <input type="text" style="cursor:pointer;background-color:white;" class="form-control" name="start" data-date-format="dd/mm/yyyy" id="mulai_jaspel" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
									    <span class="input-group-addon">to</span>
									    <input type="text" style="cursor:pointer;background-color:white;" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" id="akhir_jaspel" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2"> <i class="glyphicon glyphicon-filter"></i>&nbsp;Cara Bayar</label>
								<div class="input-group col-md-2">
									<select class="form-control select" name="cara_bayar" id="carabayar_jaspel">
										<option value="" selected>-- SEMUA --</option>
										<option value="Umum">Umum</option>
										<option value="BPJS">BPJS</option>
										<option value="Jamkesmas" >Jamkesmas</option>
										<option value="Asuransi">Asuransi</option>
										<option value="Kontrak">Kontrak</option>
										<option value="Gratis">Gratis</option>
										<option value="Lain-lain">Lain-lain</option>					
									</select>
								</div>	
							</div>

							<div class="form-group">
								<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Paramedis</label>
								<div class="input-group col-md-2">
									<input type="text" class="form-control" id="nama_petugas_jaspel" autocomplete="off" spellcheck="false" placeholder="cari petugas">
									<input type="hidden" name="id_petugas_jaspel" id="id_petugas_jaspel">
								</div>
							</div>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:1190px">
							<span class="customSpan">
								<button type="button" id="btn_filter_jaspel" class="btn btn-warning">FILTER</button> 
							</span>
						</div>
						<br>
					
					<hr class="garis">
			    	<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover tableDTUtama" id="tabelJpPoliinap">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Tanggal</th>
									<th>Tindakan</th>
									<th>Cara Bayar</th>
									<th>Paramedis</th>
									<th>Paramedis Lain</th>
									<th>Jasa Pelayanan</th>
								</tr>
							</thead>
							<tbody id="tbody_resep">
								<?php  
									if (isset($jaspel) && !empty($jaspel)) {
										$i = 0;
										foreach ($jaspel as $value) {
											$tgl = DateTime::createFromFormat('Y-m-d', $value['tanggal']);
											echo '<tr>
													<td style="text-align:center">'.(++$i).'</td>
													<td style="text-align:center">'.$tgl->format('d F Y').'</td>
													<td>'.$value['nama_tindakan'].'</td>
													<td>'.$value['cara_bayar'].'</td>
													<td>'.$value['nama_petugas'].'</td>
													<td style="text-align:right">'.$value['paramedis_lain'].'</td>
													<td style="text-align:right">'.$value['jp'].'</td>													
												</tr>';
										}
									}
								?>
							</tbody>
						</table>
					</div>
					<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button> 
					<br><br>
				</div>
			</form>
        </div>

        <div class="tab-pane" id="tagihan" style="padding-bottom:50px;"> 
        	<form class="form-horizontal" method="POST" id="submitTagihanSearch">
		       	<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px">		
						<input type="text" id="search_tagihan" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-danger">Cari</button>&nbsp;&nbsp;&nbsp;
			        <a onclick="setStatus('19')" data-toggle="modal" class="btn btn-warning"> Tambah Invoice Baru</a>
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">PASIEN BERSALIN</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_tagihan">
						<thead>
							<tr class="info">
								<th style="text-align:center;width:20px;">No.</th>
								<th>Unit</th>
								<th>Nomor Invoice</th>
								<th>Nomor Visit</th>
								<th>Nomor Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Cara Bayar</th>
								<th style="text-align:center;width:25px;">Action</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>			
			</div> 
			</div>
			<br>
			<br>
			<br>
			<br>
        </div>
        
        <div class="tab-pane" id="dipulangkan">
        	<div class="dropdown" id="">
		 	  	<div id="titleInformasi" >List Pasien Yang Belum Pulang </div>
		    </div>

		    <div class="portlet-body" style="margin: 10px 10px 0px 10px">
				<table class="table table-striped table-bordered table-hover tableDT" id="listygblmpulang">
					<thead>
						<tr class="info">
							<th width="20">No.</th>
							<th>Kamar</th>
							<th>Bed</th>
							<th>Nomor Visit</th>
							<th>#Rekam Medis</th>
							<th>Nama Pasien</th>
							<th>Alamat</th>
							<th width="50">Action</th>
						</tr>
					</thead>
					<tbody id="tbody_resep">
						<?php  
							$i = 0;
							foreach ($belum_pulang as $key) {
								echo '<tr>
										<td width="20">'.(++$i).'</td>
										<td>'.$key['nama_kamar'].'</td>
										<td>'.$key['nama_bed'].'</td>
										<td style="text-align:right">'.$key['visit_id'].'</td>
										<td style="text-align:right">'.$key['rm_id'].'</td>
										<td>'.$key['nama'].'</td>
										<td>'.$key['alamat_skr'].'</td>
										<td style="text-align:center">
											<input type="hidden" class="v_id_pulang" value="'.$key['visit_id'].'">
											<input type="hidden" class="bed_id_pulang" value="'.$key['bed_id'].'">
											<input type="hidden" class="ri_id_pulang" value="'.$key['ri_id'].'">
											<a href="#" class="pulangkan_pasien">
											<i class="glyphicon glyphicon-log-out" data-toggle="tooltip" data-placement="top" title="Pulangkan"></i></a>
										</td>
									</tr>';
							}
						?>
					</tbody>
				</table>
			</div>
        </div>
    </div>

</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#infoPermintaanBarang").hide();
		$("#btnBawahPermintaanBarang").click(function(){
			$("#infoPermintaanBarang").slideToggle();
		});

		$('#search_submit').submit(function(e){
			var data = {};
			data['search'] = $('#search_bersalin').val();
			var admisi = 'Admisi';

			e.preventDefault();
			$.ajax({
				type:'POST',
				data:data,
				url:'<?php echo base_url(); ?>bersalin/homebersalin/search_pasien',
				success:function(data){
					console.log(data);
					var t = $('#tabelutamapasienunit').DataTable();

					t.clear().draw();

					for(var i=0; i<data.length; i++){
						var rm = data[i]['rm_id'],
							nama = data[i]['nama'],
							jk = data[i]['jenis_kelamin'],
							tgl = data[i]['tanggal_lahir'],
							alamat = data[i]['alamat_skr'];
						var last = '<center><a href="#" class="pindahpasien" data-toggle="modal" data-target="#pindahkan"><i class="fa fa-external-link" data-toggle="tooltip" data-placement="top" title="Pindah"></i></a>'+
									'<a href="<?php echo base_url() ?>bersalin/bersalindetail/daftar/'+data[i]['ri_id']+'/'+data[i]['visit_id']+'" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a></center>'+
									'<input type="hidden" class="ri_id" value="'+data[i]['ri_id']+'"><input type="hidden" class="v_id" value="'+data[i]['visit_id']+'">'+
									'<input type="hidden" class="b_id" value="'+data[i]['bed_id']+'">';
						t.row.add([
							i+1,
							rm,
							nama,
							jk,
							format_date(tgl),
							alamat,
							admisi,
							last,
							i
						]).draw();
					}
					t.on( 'order.dt search.dt', function () {
				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				            cell.innerHTML = i+1;
				        } );
				    } ).draw();
					$('[data-toggle="tooltip"]').tooltip();
				},
				error:function (data) {
					console.log(data);
				}
			});
		});

	});
</script>