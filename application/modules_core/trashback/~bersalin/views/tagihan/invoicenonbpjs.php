<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>bersalin/homebersalin">Nama Pasien Bersalin</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>bersalin/tagihannonbpjs">Invoice</a>
	</li>
</div>

<input type="hidden" id="visit_id" value="<?php echo $visit_id; ?>"/>
<input type="hidden" id="sub_visit" value="<?php echo $sub_visit; ?>"/>
<input type="hidden" id="no_invoice" value="<?php echo $no_invoice; ?>"/>
<div class="backregis">
	<div id="my-tab-content" class="tab-content">
			
		<div class="informasi">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="font-size:16px;">
						<label class="control-label1 col-md-4 nama">Nomor Invoice</label>
						<div class="col-md-4 nama">: <?php echo $no_invoice; ?> </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Visit ID</label>
						<div class="col-md-5">:	<?php echo $visit_id ?> </div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Kelas Perawatan</label>
						<div class="col-md-5">: Kelas <?php echo $invoice['kelas_perawatan'] ?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Tanggal Invoice</label>
						<div class="col-md-5">: <?php 
							$tgl = strtotime($invoice['tanggal_invoice']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;
						?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Tanggal Kunjungan</label>
						<div class="col-md-5">: <?php
							$tgl = strtotime($pasien['tanggal_visit']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;
						?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nomor Rekam Medis</label>
						<div class="col-md-5">: <?php echo $pasien['rm_id']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Cara Bayar</label>
						<div class="col-md-5">: <?php echo $invoice['cara_bayar']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Pasien</label>
						<div class="col-md-5">: <?php echo $pasien['nama']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Asuransi</label>
						<div class="col-md-5">: <?php echo $invoice['nama_asuransi']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Alamat</label>
						<div class="col-md-5">: <?php echo $pasien['alamat_skr']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Perusahaan</label>
						<div class="col-md-5">: <?php echo $invoice['nama_perusahaan']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Jenis Kunjungan</label>
						<div class="col-md-5">: <?php echo $pasien['tipe_kunjungan']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nomor Ansuransi </label>
						<div class="col-md-5">: <?php echo $invoice['no_asuransi']; ?> </div>
					</div>
				</div>
			</div>
		</div>

		<hr class="garis">

		<form class="form-horizontal" role="form" id="submitInvoice">
			<div id="tagihadmisi">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Admisi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Admisi Tertagih</th>
									<th>Waktu </th>
									<th>Tarif</th>
									
								</tr>
							</thead>
							<tbody id="tbody_resep">
								<tr>
									<td colspan="4" align="center">Tidak Terdapat Tagihan Admisi</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>
			
			<div id="tagihankamar">
				<div id="titleInformasi" style="margin-bottom:-40px; margin-top:30px">
				<p style="text-align:center;margin-top:-30px;">Tagihan Kamar</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihankamar">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Kamar Tertagih</th>
									<th>Waktu Masuk </th>
									<th>Waktu Keluar</th>
									<th>Lama</th>
									<th>Tarif</th>
									<th>Tarif Lain</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttkamar">
								<!-- <tr>
									<td align="center">1</td>
									<td>007</td>
									<td style="text-align:center;">12 Desember 2012 - 12:12</td>
									<td style="text-align:center;">12 Desember 2014 - 13:13</td>
									<td>2 tahun</td>
									<td style="text-align:right;">1000</td>
									<td>
										<input type="checkbox" class="check" style="float:left; margin-right:10px; margin-top:10px;" class="checktarif">
										<input type="number" class="form-control input-sm kamar_totallain" style="float:left; width:100px; margin-right:-20px;" name="onfakturakomodasi" readonly="true">
									</td>
									<td style="text-align:right;">299</td>
									<td style="text-align:right;">1000</td>
									<td style="text-align:center">
										<a href="#">
										<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
									</td>
								</tr> -->
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihanakomodasi">
				<div id="titleInformasi" style="margin-bottom:-40px; margin-top:30px">
				<p style="text-align:center;margin-top:-30px;">Tagihan Makan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
							<table class="table table-striped table-bordered table-hover" id="tbtagihanakomodasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Akomodasi Tertagih</th>
									<th>Unit</th>									
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttakomodasi">
								<!-- <tr>
									<td align="center">1</td>
									<td>123</td>
									<td>asd</td>
									<td style="text-align:right;">123</td>
									<td style="text-align:right;">123</td>
									<td style="text-align:right;"><input type="text" class="form-control input-sm" style="width:80px" name="onfakturakomodasi"></td>
									<td style="text-align:right;">123</td>
									<td style="text-align:center">
										<a href="#">
										<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
									</td>
								</tr> -->
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihantindakanperawatan">
				<div id="titleInformasi" style="margin-bottom:-40px;"><a href="#modalttperawatan" data-toggle="modal" style="text-align:left;margin-left:-10px;font-size:12pt;color:white"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan Tindakan Perawatan">&nbsp;Tambah Tagihan Tindakan Perawatan</i></a>
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Perawatan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihanperawatan">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Perawatan Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									<th width="50">Delete</th>
								</tr>
							</thead>
							<tbody id="tbody_ttperawatan">
								
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtindakanpenunjang" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Penunjang</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanpenunjang">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Penunjang Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									
								</tr>
							</thead>
							<tbody id="tbody_ttpenunjang">
								<tr>
									<td colspan="7" align="center">Tidak Terdapat Tagihan Tindakan Penunjang</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtagihantindakanoperasi" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Operasi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
					<table class="table table-striped table-bordered table-hover" id="tbtagihanoperasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Operasi Tertagih</th>
									<th>Lingkup Operasi</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									
								</tr>
							</thead>
							<tbody id="tbody_ttoperasi">
								<tr>
									<td colspan="7" align="center">Tidak Terdapat Tagihan Tindakan Operasi</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>


			<div style="margin-right:40px;">
				<div class="form-group">
					<div class="col-md-2 pull-right">
						<input type="hidden" id="realtotaltagihan">
						<label class="control-label pull-right" id="totaltagihan" style="font-size:1.8em;margin-top:-10px;">0</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Total Tagihan (Rp.) : 
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="deposit" style="font-size:1.8em;margin-top:-10px;">0</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Deposit (Rp.) : 
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="kekurangan" style="font-size:1.8em;margin-top:-10px;">0</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Kekurangan (Rp.) : 
					</div>
				</div>
			</div>

			<div class="pull-right" style="margin-right:40px;">
				<br>
				<button class="btn btn-info" id="trigger">CETAK</button> 
				<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
				<button type="submit" class="btn btn-success">SIMPAN</button> 
			</div>
			<br><br>
		</form>

		<div class="modal fade" id="modalttkamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Kamar</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   										
			        			<div class="form-group">
									<label class="control-label col-md-4">Kamar Tertagih</label>
									<div class="col-md-5">
										<input type="text"  class="typeahead form-control" autocomplete="off" spellcheck="false" id="kamartertagih" name="kamartertagih" placeholder="Kamar Tertagih"  > 
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Waktu Masuk</label>
									<div class="col-md-5">	
										<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" class="form-control" readonly data-date-format="dd/mm/yyyy - hh:ii" data-provide="datetimepicker" placeholder="<?php echo date("d/m/Y - H:i");?>">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Waktu Keluar</label>
									<div class="col-md-5">	
										<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" class="form-control" readonly data-date-format="dd/mm/yyyy - hh:ii" data-provide="datetimepicker" placeholder="<?php echo date("d/m/Y - H:i");?>">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Lama</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="lama" name="lama" placeholder="Lama" readonly> 
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="tarifttkamar" name="tarifttkamar" placeholder="Tarif" > 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="onfakturrrkamar" name="onfakturrrkamar" placeholder="On Faktur" >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Total</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="total" name="total" placeholder="Total" readonly >
									</div>
			        			</div>

								<div class="form-group">
									<label class="control-label col-md-4">Paramedis</label>
									<div class="col-md-5">	
										<input type="text" class="typeahead form-control" name="paramedis" id="kaparamedis" placeholder="Search Paramedis" autocomplete="off" spellcheck="false">		
									</div>
			        			</div>
			        			
		        			</div>
	       				</div>
		        		<br>
		        		<div class="modal-footer">
		        			<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
		 			     	<button type="submit" class="btn btn-success">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

		<div class="modal fade" id="modalttakomodasi" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakanMakan">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Makan</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="makan_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Akomodasi Tertagih</label>
									<div class="col-md-5">
										<input type="hidden" id="makan_id">
										<textarea class="form-control" id="makan_paket" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Akomodasi Tertagih" required></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Unit</label>
									<div class="col-md-5">	
										<input type="hidden" id="makan_unitid">
										<input type="text" class="form-control" id="makan_unit" name="unitakomodasi" placeholder="Unit"> 
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="makan_tarif" name="tarifakomodasi" placeholder="Tarif" readonly> 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="makan_onfaktur" name="onfakturakomodasi" placeholder="On Faktur" >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Total</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="makan_total" name="totalakomodasi" placeholder="Total" readonly >
									</div>
			        			</div>
			        			
		        			</div>
	       				</div>
		        		<br>
		        		<div class="modal-footer">
		        			<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
		 			     	<button type="submit" class="btn btn-success">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

		<div class="modal fade" id="modalttperawatan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakanRawat">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Tindakan Perawatan</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="tin_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
									</div>
			        			</div>
			        			<div class="form-group">
								<label class="control-label col-md-4">Unit</label>
									<div class="col-md-5">	
										<input type="hidden" id="idUnit">
										<input type="text" class="form-control" id="unitTindakan" autocomplete="off" spellcheck="false"  name="unit" placeholder="Unit" required >
									</div>
								</div>			
			        			<div class="form-group">
									<label class="control-label col-md-4">Tindakan</label>
									<div class="col-md-6">
										<input type="hidden" id="idtindakan_klinik">
										<input type="text" class="form-control" id="namatindakan" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Search Tindakan" required>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"></label>
									<div class="col-md-6">
										<textarea class="form-control" id="namatindakanarea" readonly placeholder="Tindakan"></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Kelas Pelayanan</label>
									<div class="col-md-5">	
										<input type="hidden" id="idtindakdetail">
										<select class="form-control" name="kelas_tindakan" id="kelas_klinik" required>
											<option value="">Pilih Kelas</option>
											<option value="Kelas VIP">VIP</option>
											<option value="Kelas Utama">Utama</option>
											<option value="Kelas I">Kelas I</option>
											<option value="Kelas II">Kelas II</option>
											<option value="Kelas III">Kelas III</option>
										</select>
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="hidden" id="js_klinik">
										<input type="hidden" id="jp_klinik">
										<input type="hidden" id="bakhp_klinik">
										<input type="text" class="form-control" id="tarif" name="tarif" placeholder="Tarif" readonly > 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="number" class="form-control" id="onfaktur" name="onfaktur" placeholder="On Faktur" required >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Jumlah</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" readonly>
									</div>
			        			</div>

								<div class="form-group">
									<label class="control-label col-md-4">Paramedis</label>
									<div class="col-md-5">	
										<input type="hidden" id="paramedis_id">
										<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Paramedis Lain</label>
									<div class="col-md-6">	
										<textarea class="form-control" id="paramedis_lain" placeholder="Paramedis Lain"></textarea>
									</div>
			        			</div>
		        			</div>
	       				</div>
		        		<br><br>
		        		<div class="modal-footer">
		        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
		 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
		 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>

		<br><br><br>	
	</div>
</div>

<script type="text/javascript">
	$(window).ready(function(){
		var nomor = {};
		var jumlahtable = 0;
		var total = 0;
		var deposit = 0;
		var kekurangan = 0;

		nomor['no_invoice'] = $('#no_invoice').val();
		nomor['sub_visit'] = $('#sub_visit').val();
		$.ajax({
			type:'POST',
			data:nomor,
			url:'<?php echo base_url();?>rawatjalan/invoicenonbpjs/create_tagihan',
			success:function(data){

				jumlahtable = data.length;

				if(data.length!=0){
					var no = 0;

					for(var i = 0 ; i<data.length; i++){
						no++;
						$('#tbody_ttperawatan').append(
							'<tr>'+
								'<td>'+no+'</td>'+
								'<td>'+data[i]['nama_tindakan']+'</td>'+
								'<td>'+data[i]['nama_dept']+'</td>'+
								'<td style="text-align:center;">'+data[i]['waktu_tindakan']+'</td>'+
								'<td style="text-align:right;">'+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:right;">'+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td style="text-align:center">'+
									'<input type="hidden" class="total_tagihanrawat" value="'+data[i]['jumlah']+'">'+
									'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
									'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
								'</td>'+
							'</tr>'
						);

						total += Number(data[i]['jumlah']);
						kekurangan += Number(data[i]['jumlah']);
					}
				}else{
					$('#tbody_ttperawatan').append(
						'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
					);
				}

				sumTotal();

			}
		});

		var jumlahmakan = 0;
		$.ajax({
			type:'POST',
			data:nomor,
			url:'<?php echo base_url();?>bersalin/invoicenonbpjs/create_tagihanakomodasi',
			success:function(data){
				
				jumlahmakan = data.length;

				if(data.length!=0){
					var no = 0;

					for(var i = 0 ; i<data.length; i++){
						no++;
						$('#tbody_ttakomodasi').append(
							'<tr>'+
								'<td>'+no+'</td>'+
								'<td>'+data[i]['nama_paket']+'</td>'+
								'<td>'+data[i]['nama_dept']+'</td>'+
								'<td style="text-align:right;">'+
									'<input type="hidden" class="getid_makan" value="'+data[i]['tmakan_id']+'">'+
									'<input type="hidden" class="tarif_makan" value="'+data[i]['tarif']+'">'+
									'<input type="hidden" class="total_tagihanmakan" value="'+data[i]['jumlah']+'">'+ //realtotal_makan
									''+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td><input type="number" class="form-control input-sm faktur_makan" style="width:80px" name="onfakturakomodasi" value="'+data[i]['on_faktur']+'"></td>'+
								'<td style="text-align:right;">'+
									'<input type="hidden" class="total_makan" value="'+data[i]['jumlah']+'">'+
									''+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'</tr>'
						);

						sumTotal();
					}
				}else{
					$('#tbody_ttakomodasi').append(
						'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Makan</td></tr>'
					);
				}
			},error:function(data){
				console.log(data);
			}
		});

		var jumlahkamar = 0;
		$.ajax({
			type:'POST',
			data:nomor,
			url:'<?php echo base_url();?>bersalin/invoicenonbpjs/create_tagihankamar',
			success:function(data){
				console.log(data);

				jumlahkamar = data.length;

				if(data.length!=0){
					var no = 0;

					for(var i = 0 ; i<data.length; i++){
						var tarifdb = Number(data[i]['tarif'])*Number(data[i]['hari']);
						var tarif = 0;
						var cek = "";
						var ro = "readonly";
						var selected = 0;

						if(data[i]['tarif_lain']!="0"){
							tarif = Number(data[i]['tarif_lain']);
							cek = "checked";
							ro = "";
							selected = Number(data[i]['tarif_lain']);
						}else{
							tarif = tarifdb;
						}
						var totalkamar = tarif+Number(data[i]['on_faktur']);

						no++;
						$('#tbody_ttkamar').append(
							'<tr>'+
								'<td align="center">'+(i+1)+'</td>'+
								'<td>'+data[i]['nama_kamar']+'</td>'+
								'<td style="text-align:center;">'+data[i]['tgl_masuk']+'</td>'+
								'<td style="text-align:center;">'+data[i]['tgl_keluar']+'</td>'+
								'<td>'+data[i]['waktu']+'</td>'+
								'<td style="text-align:right;">'+(Number(data[i]['tarif'])*Number(data[i]['hari'])).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
								'<td>'+
									'<input type="hidden" class="getid_kamar" value="'+data[i]['tkamar_id']+'">'+
									'<input type="hidden" class="kamar_tarifdb" value="'+Number(data[i]['tarif'])*Number(data[i]['hari'])+'">'+
									'<input type="hidden" class="kamar_realtotal" value="'+tarif+'">'+
									'<input type="hidden" class="total_tagihankamar" value="'+tarif+'">'+
									'<input type="checkbox" class="check checktarif" '+cek+' style="float:left; margin-right:10px; margin-top:10px;">'+
									'<input type="number" class="form-control input-sm kamar_totallain" style="float:left; width:100px; margin-right:-20px;" value="'+selected+'" '+ro+'>'+
								'</td>'+
								'<td><input type="number" class="form-control input-sm faktur_kamar" style="width:80px" name="onfakturakomodasi" value="'+data[i]['on_faktur']+'"></td>'+
								'<td style="text-align:right;">'+
									''+totalkamar.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
							'</tr>'
						);

					}

					sumTotal();
				}else{
					$('#tbody_ttkamar').append(
						'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Makan</td></tr>'
					);
				}

			},error:function(data){
				console.log(data);
			}
		});

		$('#tbody_ttakomodasi').on('keyup', 'tr td .faktur_makan', function(){
			var total = $(this).closest('tr').find('.tarif_makan').val();
			var faktur = $(this).val();
			var all = Number(total)+Number(faktur);

			$(this).closest('tr').find('.total_tagihanmakan').val(all);
			$(this).closest('tr').find('td').eq(5).text(all.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

			var totalsemua = $('#realtotaltagihan').val();
			var totalall = 0;
			var allfakur = 0;

			sumTotal();
		});		

		$('#submitTindakanRawat').submit(function(event){
			event.preventDefault();
			var item = {};

			item['waktu'] = $('#tin_date').val();
			item['tindakan_id'] = $('#idtindakdetail').val();
			item['on_faktur'] = $('#onfaktur').val();
			item['paramedis'] = $('#paramedis_id').val();
			item['paramedis_lain'] = $('#paramedis_lain').val();
			item['tarif'] = $('#tarif').val();
			item['jumlah'] = $('#jumlah').val();
			item['js'] = $('#js_klinik').val();
			item['jp'] = $('#jp_klinik').val();
			item['bakhp'] = $('#bakhp_klinik').val();
			item['dept_id'] = $('#idUnit').val();
			item['visit_id']=$('#visit_id').val();
			item['sub_visit']=$('#sub_visit').val();
			item['no_invoice']=$('#no_invoice').val();

			var nama_tindakan = $('#namatindakan').val();
			var nama_dept = $('#unitTindakan').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>rawatjalan/invoicenonbpjs/save_tindakan',
				success:function(data){
					var no = jumlahtable;
					no++;

					console.log(item);

					if(jumlahtable==0)
						$('#tbody_ttperawatan').empty();

					$('#tbody_ttperawatan').append(
						'<tr>'+
							'<td>'+no+'</td>'+
							'<td>'+nama_tindakan+'</td>'+
							'<td>'+nama_dept+'</td>'+
							'<td style="text-align:center;">'+data[0]['waktu']+'</td>'+
							'<td style="text-align:right;">'+(data[0]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
							'<td style="text-align:right;">'+(data[0]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
							'<td style="text-align:right;">'+(data[0]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
							'<td style="text-align:center">'+
								'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[0]['id']+'">'+
								'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
							'</td>'+
						'</tr>'
					);

					$(':input','#submitTindakanRawat')
					  .not(':button, :submit, :reset')
					  .val('');
					$('#tin_date').val("<?php echo date('d/m/Y H:i') ?>");

					$('#modalttperawatan').modal('hide');

					total += Number(data[0]['jumlah']);
					kekurangan = total-deposit;
					$('#realtotaltagihan').val(total);
					$('#totaltagihan').text(total);
					$('#deposit').text(deposit);
					$('#kekurangan').text(kekurangan);

					myALert('Data Berhasil Ditambahkan');
					console.log(data);
				},error:function(data){
					alert('error');
					console.log(data);
				}
			});
		});


		$(document).on('click','.hapusTindakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#visit_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>rawatjalan/invoicenonbpjs/hapus_tindakan/"+id,
				success:function(data){
					console.log(data);

					$('#tbody_ttperawatan').empty();

					total = 0;
					kekurangan = 0;
					deposit = 0;

					if(data.length!=0){
						var no = 0;

						for(var i = 0 ; i<data.length; i++){
							no++;
							$('#tbody_ttperawatan').append(
								'<tr>'+
									'<td>'+no+'</td>'+
									'<td>'+data[i]['nama_tindakan']+'</td>'+
									'<td>'+data[i]['nama_dept']+'</td>'+
									'<td style="text-align:center;">'+data[i]['waktu']+'</td>'+
									'<td style="text-align:right;">'+(data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
									'<td style="text-align:right;">'+(data[i]['on_faktur'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
									'<td style="text-align:right;">'+(data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))+'</td>'+
									'<td style="text-align:center">'+
										'<a style="cursor:pointer" class="hapusTindakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
										'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
									'</td>'+
								'</tr>'
							);

							total += Number(data[i]['jumlah']);
							kekurangan += Number(data[i]['jumlah']);
						}
					}else{
						$('#tbody_ttperawatan').append(
							'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Tindakan Perawatan</td></tr>'
						);
					}

					jumlahtable= data.length;

					kekurangan -= deposit;
					$('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
					$('#deposit').text(deposit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
					$('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

				},
				error:function(data){
					console.log(data);
				}	
			});
		});

		var autodata = [];
		var iddata = [];
		$('#namatindakan').focus(function(){
			var $input = $('#namatindakan');
			
			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>kamaroperasi/invoicenonbpjs/get_master_tindakan',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_tindakan']);
							iddata.push(data[i]['tindakan_id']);
						}
					}
				});
			}

			$input.typeahead({source:autodata, 
	        autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = autodata.indexOf(current);

			    $('#idtindakan_klinik').val(iddata[index]);			
			    $('#namatindakanarea').val(autodata[index]);	

			    $('#kelas_klinik').prop('disabled', false);    

			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

		});
		
		$('#kelas_klinik').prop('disabled', true);

		$('#kelas_klinik').change(function(){
			var item = {};
			item['kelas'] = $(this).val();
			item['nama'] = $('#idtindakan_klinik').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>kamaroperasi/invoicenonbpjs/get_tariftindakan',
				success:function(data){
					var tarif = 0;
					var lingkup = $('#lingkup').val();
					var persen = 0;

					$('#idtindakdetail').val(data['detail_id']);

					tarif = (Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))+((Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))*persen);
					
					$('#js_klinik').val(data['js']);
					$('#jp_klinik').val(data['jp']);
					$('#bakhp_klinik').val(data['bakhp']);
					$('#tarif').val(tarif);
					$('#jumlah').val(tarif);
				}
			});
		});

		$('#onfaktur').keyup(function(){
			var onfaktur = $('#onfaktur').val();
			var tarif = $('#tarif').val();
			var jumlah = Number(onfaktur)+Number(tarif);
			$('#jumlah').val(jumlah);
		});

		$('#onfaktur').change(function(){
			var tarif = $('#tarif').val();
			var onfaktur = $(this).val();
			var jumlah = Number(tarif)+Number(onfaktur);
			
			$('#jumlah').val(jumlah);
		});

		$('#unitTindakan').focus(function(){
			var $input = $('#unitTindakan');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>bersalin/invoicenonbpjs/get_master_dept',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_dept']);
						iddata.push(data[i]['dept_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#idUnit').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
		
		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#paramedis_id').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		var paket = [];
		var idpaket = [];
		var hargapaket = [];
		$('#makan_paket').focus(function(){
			var $input = $('#makan_paket');
			
			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>bersalin/invoicenonbpjs/get_paket_makan',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							paket.push(data[i]['nama_paket']);
							idpaket.push(data[i]['id']);
							hargapaket.push(data[i]['harga_total']);
						}
					}
				});
			}

			$input.typeahead({source:paket, 
	        autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = paket.indexOf(current);

			    $('#makan_id').val(idpaket[index]);	
			    $('#makan_tarif').val(hargapaket[index]);	

			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

		});

		$('#makan_unit').focus(function(){
			var $input = $('#makan_unit');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>bersalin/invoicenonbpjs/get_master_dept',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_dept']);
						iddata.push(data[i]['dept_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#idUnit').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
		
		$('#makan_onfaktur').keyup(function(){
			var onfaktur = $(this).val();
			var tarif = $('#makan_tarif').val();

			$('#makan_total').val(Number(tarif)+Number(onfaktur));
		});

		$('#makan_onfaktur').change(function(){
			var onfaktur = $(this).val();
			var tarif = $('#makan_tarif').val();

			$('#makan_total').val(Number(tarif)+Number(onfaktur));
		});

		$('#makan_tarif').change(function(){
			var tarif = $(this).val();
			var onfaktur = $('#makan_onfaktur').val();

			$('#makan_total').val(Number(tarif)+Number(onfaktur));
		});

		$(document).on('click','.hapusTindakanMakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#visit_id').val();

			$.ajax({
				type:'POST',
				url:'<?php echo base_url() ?>bersalin/invoicenonbpjs/hapus_tagihanmakan/'+id,
				success:function(data){

					jumlahmakan = data.length;

					$('#tbody_ttakomodasi').empty();

					if(data.length!=0){
						var no = 0;

						for(var i = 0 ; i<data.length; i++){
							no++;
							$('#tbody_ttakomodasi').append(
								'<tr>'+
									'<td>'+no+'</td>'+
									'<td>'+data[i]['nama_paket']+'</td>'+
									'<td>'+data[i]['nama_dept']+'</td>'+
									'<td style="text-align:right;">'+
										'<input type="hidden" class="tarif_makan" value="'+data[i]['tarif']+'">'+
										'<input type="hidden" class="realtotal_makan" value="'+data[i]['jumlah']+'">'+
										''+data[i]['tarif'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
									'<td><input type="number" class="form-control input-sm faktur_makan" style="width:80px" name="onfakturakomodasi" value="'+data[i]['on_faktur']+'"></td>'+
									'<td style="text-align:right;">'+
										'<input type="hidden" class="total_makan" value="'+data[i]['jumlah']+'">'+
										''+data[i]['jumlah'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'</td>'+
									'<td style="text-align:center">'+
										'<a style="cursor:pointer" class="hapusTindakanMakan"><input type="hidden" class="getid" value="'+data[i]['id']+'">'+
										'<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
									'</td>'+
								'</tr>'
							);

							total += Number(data[i]['jumlah']);
							kekurangan += Number(data[i]['jumlah']);
							$('#realtotaltagihan').val(total)
							$('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
							$('#deposit').text(deposit);
							$('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
						}
					}else{
						$('#tbody_ttakomodasi').append(
							'<tr><td colspan="8" style="text-align:center;">Tidak Terdapat Tagihan Makan</td></tr>'
						);
					}
				},error:function(data){
					console.log(data);
				}
			});
		});
		
		$('#tbody_ttkamar').on('change','tr td .checktarif', function(){
			var bol = this.checked;
			var lain = Number($(this).closest('tr').find('.kamar_totallain').val());
			var tarifdb = Number($(this).closest('tr').find('.kamar_tarifdb').val());
			var kfaktur = Number($(this).closest('tr').find('.faktur_kamar').val());

			if(bol){
				$(this).closest('tr').find('.kamar_totallain').attr('readonly',false);
				$(this).closest('tr').find('.kamar_realtotal').val(lain);
				$(this).closest('tr').find('.total_tagihankamar').val(lain+kfaktur);
				$(this).closest('tr').find('td').eq(8).text((lain+kfaktur).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			}else{
				$(this).closest('tr').find('.kamar_totallain').attr('readonly',true);
				$(this).closest('tr').find('.kamar_totallain').val('0');

				$(this).closest('tr').find('.kamar_realtotal').val(tarifdb);
				$(this).closest('tr').find('.total_tagihankamar').val(tarifdb+kfaktur);
				$(this).closest('tr').find('td').eq(8).text((tarifdb+kfaktur).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			}

			sumTotal()
		});

		$('#tbody_ttkamar').on('keyup','tr td .kamar_totallain', function(){
			var hargalain = Number($(this).val());
			var kfaktur = Number($(this).closest('tr').find('.faktur_kamar').val());

			$(this).closest('tr').find('.kamar_realtotal').val(hargalain);
			$(this).closest('tr').find('.total_tagihankamar').val(hargalain+kfaktur);
			$(this).closest('tr').find('td').eq(8).text((hargalain+kfaktur).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

			sumTotal()
		});

		$('#tbody_ttkamar').on('keyup','tr td .faktur_kamar', function(){
			var kfaktur = $(this).val();
			var real = $(this).closest('tr').find('.kamar_realtotal').val();
			var tfaktur = Number(real)+Number(kfaktur);

			if(kfaktur == "")
				kfaktur = 0;

			$(this).closest('tr').find('td').eq(8).text((Number(tfaktur)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			$(this).closest('tr').find('.total_tagihankamar').val(tfaktur);

			sumTotal();
		});


		$('#submitInvoice').submit(function(event){
			event.preventDefault();

			$('#tbody_ttkamar tr').each(function(){
				var item = {};
				item[1] = {};
				var bol = $(this).find('.checktarif').checked;

				item[1]['id'] = $(this).find('.getid_kamar').val();
				item[1]['on_faktur'] = $(this).find('.faktur_kamar').val();
				item[1]['tarif_lain'] = 0;

				if(bol)
					item[1]['tarif_lain'] = $(this).find('.kamar_totallain').val();

				$.ajax({
					type:'POST',
					data:item,
					url:'<?php echo base_url() ?>bersalin/invoicebpjs/update_tagihankamar',
					success:function(data){
						console.log(data);
					},error:function(data){
						console.log(data);
					}
				});

			});

			$('#tbody_ttakomodasi tr').each(function(){
				var item = {};
				item[1] = {};
				item[1]['id'] = $(this).find('.getid_makan').val();
				item[1]['on_faktur'] = $(this).find('.faktur_makan').val();

				$.ajax({
					type:'POST',
					data:item,
					url:'<?php echo base_url() ?>bersalin/invoicebpjs/update_tagihanmakan',
					success:function(data){
						console.log(data);
					},error:function(data){
						console.log(data);
					}
				});
			});

			// myALert("Data Berhasil Ditambahkan");
			window.location.href = "<?php echo base_url() ?>bersalin/homebersalin";
		});

	});

	function sumTotal(){
		var total = 0;
		var deposit = 0;
		var kekurangan = 0;
		var totalrawat = 0;
		var totalkamar = 0;
		var totalmakan = 0;

		$('.total_tagihanrawat').each(function(){
			totalrawat += Number($(this).val());
		});

		$('.total_tagihankamar').each(function(){
			totalkamar += Number($(this).val());
			console.log($(this).val());
		});

		$('.total_tagihanmakan').each(function(){
			totalmakan += Number($(this).val());
		});

		// console.log(totalmakan+" "+totalkamar+" "+totalrawat);

		total = totalkamar+totalmakan+totalrawat;
		kekurangan = total-deposit;

		$('#totaltagihan').text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
		$('#deposit').text(deposit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
		$('#kekurangan').text(kekurangan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
	}
</script>
