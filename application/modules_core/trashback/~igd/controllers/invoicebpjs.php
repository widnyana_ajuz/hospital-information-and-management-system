<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Invoicebpjs extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_invoicebpjs");
		$data['page_title'] = "Invoice BPJS";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		redirect('rawatjalan/homerawatjalan');
	}

	public function invoice($no_invoice){
		$data['content'] = 'tagihan/invoicebpjs';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$invoice = $this->m_invoicebpjs->get_visit_id($no_invoice);
		$visit_id = $invoice['visit_id'];
		$sub_visit = $invoice['sub_visit'];
		$data['visit_id'] = $visit_id;
		$data['sub_visit'] = $invoice['sub_visit'];
		$data['no_invoice'] = $no_invoice;
		$data['invoice'] = $invoice;
		$data['dept_id'] = $this->m_invoicebpjs->get_deptid("IGD");

		$pasien = $this->m_invoicebpjs->get_data_pasien($visit_id);
		$data['pasien'] = $pasien;
		
		$this->load->view('base/operator/template', $data);	
	}

	public function create_tagihan(){
		$no_invoice = $_POST['no_invoice'];
		$sub_visit = $_POST['sub_visit'];
		$kelas = $_POST['kelas'];

		$tindakan = $this->m_invoicebpjs->get_tindakan($sub_visit, $kelas);

		foreach ($tindakan as $value) {
			$getbpjs = $this->m_invoicebpjs->get_tarifbpjs($value['care_id'], $kelas);
			$value['tarif_bpjs'] = $getbpjs['tarif_bpjs'];
			$value['selisih'] = intval($value['tarif']) - intval($value['tarif_bpjs']);
			if(intval($value['selisih']) < 0)
				$value['selisih'] = 0;
			$value['total'] = $value['selisih'] + $value['on_faktur'];
			$value['no_invoice'] = $no_invoice;			

			$insert = $this->m_invoicebpjs->insert_tagihan($value);
			//$result[] = $value;
		}		

		$result = $this->m_invoicebpjs->get_tagihanperawatan($no_invoice);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function save_tindakan(){
		$kelas = $_POST['kelas'];

		$visit_id = $_POST['visit_id'];
		$care['visit_id'] = $visit_id;
		$care['sub_visit'] = $_POST['sub_visit'];
		$care['waktu_tindakan'] = $this->date_db($_POST['waktu']);
		$care['tindakan_id'] = $_POST['tindakan_id'];
		$care['on_faktur'] = $_POST['on_faktur'];
		$care['paramedis'] = $_POST['paramedis'];
		$care['js'] = $_POST['js'];
		$care['jp'] = $_POST['jp'];
		$care['bakhp'] = $_POST['bakhp'];
		$care['tarif'] = $_POST['tarif'];
		$care['jumlah'] = $_POST['jumlah'];
		$care['dept_id'] = $_POST['dept_id'];

		$id = $this->m_invoicebpjs->get_last_visit_care($visit_id);
		if($id){
			$vid = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vid) == "1") {
				$vid = '000'. $vid;
			}else if(strlen($vid) == "2"){
				$vid = '00' . $vid;
			}else if (strlen($vid) == "3") {
				$vid = '0' . $vid;
			}
			$care['care_id'] = "CA".$visit_id."".($vid);
		}else{
			$care['care_id'] = "CA".$visit_id."0001";
		}

		//subasdmit visit_care
		$in_care = $this->m_invoicebpjs->save_tindakan($care);

		$tagihan['care_id'] = $care['care_id'];
		$tagihan['no_invoice'] = $_POST['no_invoice'];
		$tagihan['tindakan_id'] = $_POST['tindakan_id'];
		$tagihan['dept_id'] = $_POST['dept_id'];
		$tagihan['waktu'] = $this->date_db($_POST['waktu']);
		$tagihan['js'] = $_POST['js'];
		$tagihan['jp'] = $_POST['jp'];
		$tagihan['bakhp'] = $_POST['bakhp'];
		$tagihan['tarif'] = $_POST['tarif'];
		$tagihan['jumlah'] = $_POST['jumlah'];
		$tagihan['on_faktur'] = $_POST['on_faktur'];
		$tagihan['paramedis'] = $_POST['paramedis'];
		$tagihan['petugas_input'] = $_POST['paramedis'];

		//perhitungan bpjs

		$tagihan['tarif_bpjs'] = $_POST['tarif_bpjs'];
		$tagihan['selisih'] = $_POST['selisih'];

		if(intval($tagihan['selisih']) < 0)
			$tagihan['selisih'] = 0;
		$tagihan['total'] = $tagihan['selisih'] + $tagihan['on_faktur'];

		//submit tindakan
		$in_tagihan = $this->m_invoicebpjs->save_tagihan($tagihan);

		$result = $this->m_invoicebpjs->get_inserted_tagihan($care['care_id']);

		header('Content-type: application/json');
		echo json_encode($result);
	}

}
