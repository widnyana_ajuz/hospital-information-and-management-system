<?php

class m_homeigd extends CI_Model {

// function __construct() {
//         // Call the Model constructor
//         parent::__construct();
//     }
    public function search_pasien($search){
        $sql = "SELECT * FROM pasien p, visit v, visit_igd r WHERE p.rm_id = v.rm_id AND v.visit_id = r.visit_id AND v.status_visit = 'REGISTRASI' AND r.waktu_keluar is NULL AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '%$search%')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_antrian_pasien(){
        $sql = "SELECT * FROM pasien p, visit v, visit_igd r WHERE p.rm_id = v.rm_id AND v.visit_id = r.visit_id AND v.status_visit = 'REGISTRASI' AND r.waktu_keluar is NULL ORDER BY r.waktu_masuk ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

	public function get_pasien($rm_id)
    {
    	$sql = "SELECT * FROM pasien where rm_id = ?";
    	//$this->db->where($rm_id);
    	$query = $this->db->query($sql,$rm_id);
    	if ($query) {
    		return $query->row_array();
    	}else{
    		return false;
    	}
    }

    public function search_tagihan($search){
        $sql = "SELECT *, t.cara_bayar as carapembayaran FROM tagihan t, pasien p, visit v, visit_rj vr, master_dept m 
                WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.sub_visit = vr.rj_id AND vr.unit_tujuan = m.dept_id
                AND vr.visit_id = v.visit_id AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '%$search%') AND m.jenis = 'IGD'
                ";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_dept_id($select){
        $sql = "SELECT dept_id FROM master_dept WHERE nama_dept = '$select' LIMIT 1";
        $query = $this->db->query($sql);
        if ($query) {
            return $query->row_array();
        }else{
            return false;
        }        
    }
}

?>