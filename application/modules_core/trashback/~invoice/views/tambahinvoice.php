<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>invoice/tambahinvoice">INVOICE</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>invoice/tambahinvoice">TAMBAH INVOICE</a>
	</li>
</div>

<input type="hidden" id="status_in">
<input type="hidden" id="from_url">

<div class="backregis" style="margin-top:20px;">
	<div id="my-tab-content" class="tab-content">
		<div class="dropdown">
            <div id="titleInformasi">Form Tambah Invoice</div>
       	</div>
       	<br>
       	
     	<form class="form-horizontal" role="form" id="submit_invoice">
    		<div class="informasi">
       			<table width="100%">
       				<tr>
       					<td width="50%">
       						<div class="form-group">
								<label class="control-label col-md-5"> Nomor Invoice</label>
								<div class="input-group col-md-4">
									<input type="text" class="form-control" name="in_nomor" readonly id="in_nomor" placeholder="Nomor Invoice">
								</div>
							</div>	

							<div class="form-group">
								<label class="control-label col-md-5">Visit ID</label>
								<div class="input-group col-md-4">
									<input type="hidden" id="in_subvisit">
									<input type="text" id="in_visitid" class="form-control" style="cursor:pointer;background-color:white" required id="visitid" placeholder="Search Visit ID" data-toggle="modal" data-target="#searchvisit">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-5">Tanggal</label>
								<div class="input-group col-md-4" >
									<div class="input-icon">
										<i class="fa fa-calendar"></i>
										<input type="text" id="in_date" style="cursor:pointer; background-color:white" class="form-control" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-5"> Nomor Rekam Medis</label>
								<div class="input-group col-md-4">
									<input type="text" class="form-control" id="in_rmid" readonly name="norm" readonly placeholder="Nomor RM">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-5"> Nama Pasien</label>
								<div class="input-group col-md-4">
									<input type="text" class="form-control" id="in_namapasien" readonly name="nmpasien" placeholder="Nama Pasien">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-5">Alamat </label>
								<div class="input-group col-md-5">
									<input type="text" class="form-control" id="in_alamat" readonly name="alamat" placeholder="Alamat">
								</div>
							</div>
       					</td>

       					<td width="50%" valign="top">
		       				<div style="">		
		       					<div class="form-group">
									<label class="control-label col-md-5">Jenis Kunjungan </label>
									<div class="input-group col-md-3">
										<input type="text" class="form-control" id="in_jeniskunjungan" readonly name="jeniskunjungan" placeholder="Jenis Kunjungan">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-5">Tanggal Kunjungan</label>
									<div class="input-group col-md-3" >
										<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" id="in_datekunjungan" style="cursor:pointer;background-color:white" class="form-control" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="Tanggal Kunjungan">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-5"> Kelas Perawatan</label>
									<div class="input-group col-md-3">
										<select class="form-control select" name="kelas_pelayanan" id="in_kelasperawatan" required>
											<option value="" selected>Pilih Kelas</option>
											<option value="III">III</option>
											<option value="II">II</option>
											<option value="I"  >I</option>
											<option value="Utama" >Utama</option>
											<option value="VIP">VIP</option>
										</select>
									</div>
								</div>
	       						
	       						<div class="form-group">
									<label class="control-label col-md-5">Cara Bayar</label>
									<div class="input-group col-md-3">
										<select class="form-control select" name="carabayar" id="carabayar" required>
											<option value="" selected>Pilih Cara Bayar</option>
											<option value="Umum">Umum</option>
											<option value="BPJS" id="op-bpjs">BPJS</option>
											<option value="Jamkesmas" >Jamkesmas</option>
											<option value="Asuransi" id="op-asuransi">Asuransi</option>
											<option value="Kontrak" id="op-kontrak">Kontrak</option>
											<option value="Gratis" >Gratis</option>
											<option value="Lain-laun">Lain-lain</option>
										</select>												
									</div>
								</div>
								
								<div class="form-group" id="asuransi">
									<label class="control-label col-md-5">Nama Asuransi</label>
									<div class="input-group col-md-4">
										<input type="text" class="form-control" id="namaAsuransi" name="namaAsuransi" placeholder="Nama Asuransi">
									</div>
								</div>
										
								<div class="form-group" id="kontrak">
									<label class="control-label col-md-5">Nama Perusahaan</label>
									<div class="input-group col-md-4">
										<input type="text" class="form-control" id="namaPerusahaan" name="namaPerusahaan" placeholder="Nama Perusahaan">
									</div>
								</div>

								<div class="form-group" id="kelas">
									<label class="control-label col-md-5">Kelas BPJS </label>
									<div class="input-group col-md-2">
										<select class="form-control select" name="kelas_pelayanan" id="kelas_pelayanan">
											<option value="" selected>Pilih Kelas</option>
											<option value="III">III</option>
											<option value="II">II</option>
											<option value="I"  >I</option>
											<option value="Utama" >Utama</option>
											<option value="VIP">VIP</option>
										</select>												
									</div>
								</div>
								
								<div class="form-group" id="noasuransi">
									<label class="control-label col-md-5">Nomor Asuransi</label>
									<div class="input-group col-md-4">
										<input type="text" class="form-control" name="nomorAsuransi" id="nomorAsuransi" placeholder="Nomor Asuransi">
									</div>
								</div>

								<div class="form-group" id="basebayar">
									<label class="control-label col-md-5">Base Bayar</label>
									<div class="col-md-7">
										<div class="radio-list">
											<label>
												<input type="radio" name="in_base" id="klama" value="Kelas BPJS"/><span style="margin-left:10px;">BPJS</span> 
											</label>
											<label style="margin-left:50px;">
												<input type="radio"  name="in_base" id="kbaru" value="Kelas Perawatan"/><span style="margin-left:10px">Perawatan</span>
											</label>
										</div>
									</div>
								</div>
							</div>
       					</td>
       				</tr>
       			</table>
       		</div>
   			<br>
			<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
			<div style="margin-left:80%">
				<span style=" padding:0px 10px 0px 10px;">
					<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
					<button type="submit" class="btn btn-success">TAMBAH</button>
					<!-- <a href="<?php //echo base_url() ?>rawatjalan/invoicenonbpjs" class="btn btn-success" id="ivnonbpjs">Tambah Tagihan</a>
  			 		<a href="<?php //echo base_url() ?>rawatjalan/invoicebpjs" class="btn btn-success" id="ivbpjs">Tambah Tagihan</a> -->
				</span>
			</div>
			<br>

   			<br>
   			<!-- <div class="pull-right" style="margin-right:20px">
       			<button type="reset" class="btn btn-warning" data-dismiss="modal">Reset</button>&nbsp;&nbsp;&nbsp;
  			 	<a href="<?php //echo base_url() ?>rawatjalan/invoicenonbpjs" class="btn btn-success" id="ivnonbpjs">Tambah Tagihan</a>
  			 	<a href="<?php //echo base_url() ?>rawatjalan/invoicebpjs" class="btn btn-success" id="ivbpjs">Tambah Tagihan</a>
  			 </div> -->
       	</form>

       	<br><br><br>
      	<div class="modal fade" id="searchvisit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" style="width:800px;">
				<div class="modal-content">
					<div class="modal-header">
	    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	    				<h3 class="modal-title" id="myModalLabel">Cari Visit ID</h3>
	    			</div>
	    			<div class="modal-body">
						<div class="form-group">
						<form method="POST" id="submit_visit">
							<div class="col-md-5">
								<input type="text" class="form-control" name="keyword" id="katakuncivisit" placeholder="Masukan Visit ID / Nama Pasien">
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div>	
						</form>
						</div>	
						<br>
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 5px 0px 5px">
							<table class="table table-striped table-bordered table-hover" id="tabelsearchvisitid">
								<thead>
									<tr class="info">
										<th>Visit ID</th>
										<th>Nama Pasien</th>
										<th>Departemen</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_visit">
									<tr>
										<td colspan="4" style="text-align:center">Cari Data Visit</td>
									</tr>
								</tbody>
							</table>												
						</div>
	    			</div>
	    			<div class="modal-footer">
				       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>
 	
	</div>
</div>
<script type="text/javascript">
	var temp_data = {};
	$(document).ready(function(){
		$('#basebayar').hide();

		var status = localStorage.getItem('department');
		var url = localStorage.getItem('url');
		$('#status_in').val(status);
		$('#from_url').val(url);

		$('#submit_visit').submit(function(e){
			e.preventDefault();
			var item = {};
			item['search'] = $('#katakuncivisit').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>invoice/tambahinvoice/get_visit_id',
				success:function(data){
					$('#tbody_visit').empty();
					console.log(data);
					temp_data = data;

					if(data.length!=0)
						for(var i = 0; i < data.length; i++){
							$('#tbody_visit').append(
								'<tr>'+
									'<td>'+data[i]['visit_id']+'</td>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['nama_dept']+'</td>'+
									'<td style="text-align:center; cursor:pointer;"><a onClick="getVisit('+i+')"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>'+
								'</tr>'
							);
					}else{
						$('#tbody_visit').append(
							'<tr>'+
								'<td colspan="3" style="text-align:center">Data Tidak Ditemukan</td>'+
							'</tr>'
						);
					}
				}
			});
		});

		$('#carabayar').change(function(){
			var e = $(this).val();
			if(e == "BPJS"){
				$('#basebayar').show();
			}else{
				$('#basebayar').hide();
			}
		});

		$('#submit_invoice').submit(function(e){
			e.preventDefault();
			var is_bpjs = false;
			var item = {};
			item[1] = {};

			item[1]['no_invoice'] = $('#in_nomor').val();
			item[1]['cara_bayar'] = $('#carabayar').val();
			item[1]['nama_asuransi'] = $('#namaAsuransi').val();
			item[1]['no_asuransi'] = $('#nomorAsuransi').val();
			item[1]['nama_perusahaan'] = $('#namaPerusahaan').val();
			item[1]['visit_id'] = $('#in_visitid').val();
			item[1]['sub_visit'] = $('#in_subvisit').val();
			item[1]['kelas_perawatan'] = $('#in_kelasperawatan').val();
			item[1]['tanggal_invoice'] = $('#in_date').val();
			//item[1]['total_tagihan'] = $('#').val();
			//item[1]['deposit'] = $('#').val();
			//item[1]['kekurangan'] = $('#').val();
			if($('#carabayar').val() == "BPJS"){
				item[1]['kelas_bpjs'] = $('#kelas_pelayanan').val();
				item[1]['kelas_pelayanan'] = $('#kelas_pelayanan').val();
				item[1]['base_bayar'] = $('input[name="in_base"]:checked').val();
				is_bpjs = true;
			}

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>invoice/tambahinvoice/save_invoice',
				success:function(data){
					alert('Data Berhasil Ditambahkan');
					var link = $('#from_url').val();

					if(is_bpjs)
						window.location.href = link+'invoicebpjs/invoice/'+data['no_invoice'];
					else
						window.location.href = link+'invoicenonbpjs/invoice/'+data['no_invoice'];

					console.log(data);
				},
				error:function(data){
					alert('Error');
					console.log(data);
				}
			});
		});
	});

	function getVisit(index){
		var visit = temp_data[index]['visit_id'],
			sub_visit = temp_data[index]['sub_visit'],
			rm_id = temp_data[index]['rm_id'],
			nama = temp_data[index]['nama'],
			alamat = temp_data[index]['alamat_skr'],
			tgl_kunjung = changeDate(temp_data[index]['tanggal_visit']),
			jenis_kunjung = temp_data[index]['jenis'],
			dept_id = temp_data[index]['unit_tujuan'];

		$('#in_visitid').val(visit);
		$('#in_subvisit').val(sub_visit);
		$('#in_namapasien').val(nama);
		$('#in_rmid').val(rm_id);
		$('#in_alamat').val(alamat);
		$('#in_datekunjungan').val(tgl_kunjung);
		$('#in_jeniskunjungan').val(jenis_kunjung);

		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>invoice/tambahinvoice/get_new_invoice/'+visit+'/'+dept_id,
			success:function(data){
				console.log(data);
				$('#in_nomor').val(data['invoice']);
			}, error:function(data){
				console.log(data+'sit');
			}
		});

		$('#tbody_visit').empty();
		$('#tbody_visit').append(
			'<tr>'+
				'<td colspan="4" style="text-align:center">Cari Data Pasien</td>'+
			'</tr>'
		);
		$('#searchvisit').modal('hide');
	}

	function changeDate(insert){
		var justdate = insert.split(" ");
		var remove = justdate[0].split("-");
		var bulan;
		
		var tgl = remove[2]+"/"+remove[1]+"/"+remove[0];

		return tgl;
	}
</script>
