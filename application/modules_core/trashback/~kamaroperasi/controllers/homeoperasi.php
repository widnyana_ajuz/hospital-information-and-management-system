<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );


class Homeoperasi extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_homeoperasi");
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$data['content'] = 'daftar/list';
		// load template
		$data['content'] = 'home';
		$data['javascript'] = 'javascript/j_home';
		$data['listorder'] = $this->m_homeoperasi->get_listorder();
		$data['listoperasi'] = $this->m_homeoperasi->get_listoperasi();
		$data['listriwayat'] = $this->m_homeoperasi->get_listriwayat();

		$this->load->view('base/operator/template', $data);
	}

	public function get_listorder(){
		$result = $this->m_homeoperasi->get_listorder();

		header('Content-type: application/json');
		echo json_encode($result);		
	}

	public function search_operasi(){
		$key = $_POST['search'];

		$result = $this->m_homeoperasi->search_operasi($key);

		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function search_listriwayat(){
		$key = $_POST['search'];

		$result = $this->m_homeoperasi->search_listriwayat($key);

		header('Content-type: application/json');
		echo json_encode($result);	
	}

	public function get_dnama(){
		$d1 = $_POST['d2'];
		$d2 = $_POST['d3'];
		$d3 = $_POST['d4'];
		$d4 = $_POST['d5'];

		if($d1!=""){
			$nama1 = $this->m_homeoperasi->get_dnama($d1);
			$result['nama1'] = $nama1['diagnosis_nama'];
		}else{
			$result['nama1'] = "";
		}

		if($d2!=""){
			$nama2 = $this->m_homeoperasi->get_dnama($d2);
			$result['nama2'] = $nama2['diagnosis_nama'];
		}else{
			$result['nama2'] = "";
		}

		if($d3!=""){
			$nama3 = $this->m_homeoperasi->get_dnama($d3);
			$result['nama3'] = $nama3['diagnosis_nama'];
		}else{
			$result['nama3'] = "";
		}

		if($d4!=""){
			$nama4 = $this->m_homeoperasi->get_dnama($d4);
			$result['nama4'] = $nama4['diagnosis_nama'];
		}else{
			$result['nama4'] = "";
		}

		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function hapus_order($id){
        $input = $this->m_homeoperasi->hapus_order_operasi($id);

        $key = $_POST['search'];
        $result = $this->m_homeoperasi->search_operasi($key);

        header('Content-Type:application/json');
		echo(json_encode($result));
    }

    public function submit_operasi($visit_id){
		foreach ($_POST as $value) {
			$insert = $value;
		}

		$tgl = $this->date_db($insert['waktu_rencana']);
		$insert['waktu_rencana'] = $tgl;

		$id = $this->m_homeoperasi->get_last_order_operasi($visit_id);

		if($id){
			$vir = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vir) == "1") {
				$vir = '0'. $vir;
			}
			$insert['rencana_id'] = "OR".$visit_id."".($vir);
		}else{
			$insert['rencana_id'] = "OR".$visit_id."01";
		}

		$in = $this->m_homeoperasi->submit_operasi($insert);

		$update['status'] = 'RENCANA';
		$up = $this->m_homeoperasi->update_status($insert['order_operasi_id'], $update);

		if($in && $up)
			return true;
		return false;
	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}

	public function fdate_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y',$date);
		$newDateString = $dateTime->format('Y-m-d');
		return $newDateString;
	}

	public function save_resep(){
		foreach ($_POST as $value) {
			$insert = $value;
		}

		$visit_id = $insert['visit_id'];
		$id = $this->m_homeoperasi->get_last_visit_resep($insert['visit_id']);
		if($id){
			$vir = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vir) == "1") {
				$vir = '000'. $vir;
			}else if(strlen($vir) == "2"){
				$vir = '00' . $vir;
			}else if (strlen($vir) == "3") {
				$vir = '0' . $vir;
			}
			$insert['resep_id'] = "RE".$visit_id."".($vir);
		}else{
			$insert['resep_id'] = "RE".$visit_id."0001";
		}

		$tgl = $this->fdate_db($insert['tanggal']);
		$insert['tanggal'] = $tgl;
		$hasil = $this->m_homeoperasi->save_visit_resep($insert);
		
		header('Content-Type:application/json');
		echo(json_encode($insert));
	}

	public function get_resep($sub){
		$result = $this->m_homeoperasi->get_resep($sub);

		header('Content-Type: application/json');
		echo(json_encode($result));
	}

	public function search_listoperasi(){
		$key = $_POST['search'];
		$result = $this->m_homeoperasi->search_listoperasi($key);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function search_tagihan(){
		$search = $_POST['search'];

		$result = $this->m_homeoperasi->search_tagihan($search);

		header('Content-Type: application/json');
		echo json_encode($result);
	}
}
