<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );
class Operasidaftarkan extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_operasidaftarkan");
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$this->session->set_userdata($data);
		$data['content'] = 'daftarkan';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}

	public function process($rencana_id, $order_id){
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$this->session->set_userdata($data);
		
		$data['content'] = 'daftarkan';
		$data['javascript'] = 'javascript/j_daftarkan';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$v = $this->m_operasidaftarkan->get_dataoperasi($order_id);
		$visit_id = $v['visit_id'];
		$sub_visit = $v['sub_visit'];
		$data['dataoperasi'] = $v;
		
		$this->load->view('base/operator/template', $data);
	}

	public function search_diagnosa(){
		$search = $_POST['search'];

		$result = $this->m_operasidaftarkan->search_diagnosa($search);

		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function searchDokter(){
		$search = $_POST['search'];

		$result = $this->m_operasidaftarkan->searchDokter($search);

		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function save_operasidetail(){
		foreach ($_POST as $value) {
			$insert = $value;
		}

		$tglmulai = $this->date_db($insert['waktu_mulai']);
		$insert['waktu_mulai'] = $tglmulai;

		$tglselesai = $this->date_db($insert['waktu_selesai']);
		$insert['waktu_selesai'] = $tglselesai;

		$in = $this->m_operasidaftarkan->save_operasidetail($insert);

		$update['status'] = "SELESAI";
		$tamp = $this->m_operasidaftarkan->get_order_operasi($insert['rencana_id']);
		$id = $tamp['order_operasi_id'];

		$update_status = $this->m_operasidaftarkan->update_status($id,$update);

		if($in)
			return true;
		return false;
	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y - H:i',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}
}
