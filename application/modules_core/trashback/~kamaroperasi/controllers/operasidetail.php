<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );
class Operasidetail extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_operasidaftarkan");
	}

	public function index($page = 0)
	{
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();
		$this->session->set_userdata($data);
		redirect('laboratorium/homelab');
	}

	public function detail($operasi_id, $order_id){
		$data['content'] = 'detailoperasi';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$v = $this->m_operasidaftarkan->get_dataoperasi($order_id);
		$visit_id = $v['visit_id'];
		$sub_visit = $v['sub_visit'];
		$data['dataoperasi'] = $v;

		$data['detailoperasi'] = $this->m_operasidaftarkan->get_detailoperasi($operasi_id);

		$this->load->view('base/operator/template', $data);
	}

}
