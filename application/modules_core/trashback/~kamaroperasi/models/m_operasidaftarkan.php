<?php
	class m_operasidaftarkan extends CI_Model {
		public function get_dataoperasi($order){
			$sql = "SELECT * FROM operasi_rencana r, order_operasi o, visit v, pasien p, master_diagnosa m, petugas t WHERE o.order_operasi_id = r.order_operasi_id AND v.visit_id = o.visit_id AND p.rm_id = v.rm_id  AND m.diagnosis_id = r.diagnosa_utama AND t.petugas_id = o.pengirim AND o.order_operasi_id = '$order'";
			$query = $this->db->query($sql);
			$result = $query->row_array();
			return $result;
		}

		public function search_diagnosa($search){
			$sql = "SELECT * FROM master_diagnosa WHERE diagnosis_nama LIKE '%$search%' OR diagnosis_id LIKE '%$search%'";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;	
		}

		public function searchDokter($search){
			$sql = "SELECT * FROM petugas p, master_jabatan m WHERE p.jabatan_id = m.jabatan_id AND m.nama_jabatan = 'Dokter' AND p.nama_petugas LIKE '%$search%'";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;		
		}

		public function save_operasidetail($value){
			$query = $this->db->insert('operasi_laporan',$value);
	        if ($query) {
	            return true;
	        }else{
	            return false;
	        }
		}

		public function get_order_operasi($rencana_id){
			$sql = "SELECT order_operasi_id FROM operasi_rencana WHERE rencana_id = '$rencana_id'";
			$query = $this->db->query($sql);
			$result = $query->row_array();
			return $result;		
		}

		public function update_status($id, $data){
    		$this->db->where('order_operasi_id', $id);
	        $update = $this->db->update('order_operasi', $data);

	        if($update)
	            return true;
	        else
	            return false;
    	}

  //   	public function get_detailoperasi($operasi_id){
		// 	$sql = "SELECT v.tanggal_visit,v.visit_id, r.waktu_rencana, sv.sub_visit, v.rm_id, p.nama, pt.nama_petugas, md.nama_dept, o.order_operasi_id, ov.diagnosa1, d.diagnosis_nama, ov.over_id, ov.diagnosa1, ov.diagnosa2, ov.diagnosa3, ov.diagnosa4, ov.diagnosa5, md.jenis, ol.waktu_mulai, ol.waktu_selesai, r.rencana_id, ol.operasi_id FROM master_diagnosa d, visit v, operasi_rencana r , operasi_laporan ol,(SELECT rj_id as sub_visit FROM visit_rj UNION SELECT ri_id as sub_visit FROM visit_ri UNION SELECT igd_id as sub_visit FROM visit_igd) sv, pasien p, petugas pt, master_dept md, order_operasi o, (SELECT max(id) as over_id, visit_id, rj_id as sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_klinik GROUP BY visit_id  UNION ALL SELECT max(id) as over_id, visit_id, sub_visit, diagnosa1, diagnosa2, diagnosa3, diagnosa4, diagnosa5 FROM overview_igd  GROUP BY visit_id) ov WHERE o.visit_id = v.visit_id AND v.rm_id = p.rm_id AND o.dept_id = md.dept_id AND ol.dokter_bedah = pt.petugas_id AND r.rencana_id = ol.rencana_id AND o.sub_visit = sv.sub_visit AND o.sub_visit = ov.sub_visit AND ol.diagnosa_post = d.diagnosis_id AND r.order_operasi_id = o.order_operasi_id AND o.status='SELESAI' AND ol.operasi_id = '$operasi_id'";
		// 	$query = $this->db->query($sql);
		// 	$result = $query->row_array();
		// 	return $result;
		// }

		public function get_detailoperasi($operasi_id){
			$query = $this->db->query("SELECT o.*, pb.nama_petugas as nama_bedah, pa.nama_petugas as nama_anestesi, pn.nama_petugas as nama_anak, m1.diagnosis_nama as diag_pra, m2.diagnosis_nama as diag_post FROM 
				operasi_laporan o LEFT JOIN petugas pb ON o.dokter_bedah = pb.petugas_id
				LEFT JOIN petugas pa ON o.dokter_anestesi = pa.petugas_id 
				LEFT JOIN petugas pn ON o.dokter_anak = pn.petugas_id 
				LEFT JOIN master_diagnosa m1 ON o.diagnosa_pra = m1.diagnosis_id
				LEFT JOIN master_diagnosa m2 ON o.diagnosa_pra = m2.diagnosis_id
				WHERE operasi_id = '$operasi_id'
				");
			$result = $query->row_array();
			return $result;
		}
	}
?>