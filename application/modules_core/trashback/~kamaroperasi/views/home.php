<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kamaroperasi/homeoperasi">KAMAR OPERASI</a>
	</li>
</div>

<div class="navigation" style="margin-left: 10px" >
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	    <li class="active"><a href="#list" data-toggle="tab">List Pasien</a></li>
	    <li><a href="#rencanaOperasi" data-toggle="tab">List Rencana Operasi</a></li>
	    <li><a href="#riwayatOperasi" data-toggle="tab">Riwayat Operasi</a></li>
	    <li><a href="#farmasi" data-toggle="tab">Farmasi </a></li>
	    <li><a href="#logistik" data-toggle="tab">Logistik</a></li>
	    <li><a href="#laporan" data-toggle="tab">Laporan</a></li>
	    <li><a href="#master" data-toggle="tab">Master</a></li>
	    <li><a href="#tagihan" data-toggle="tab">Tagihan</a></li>
	</ul> 

	<div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="list">
            <form method="POST" id="search_order">
		       	<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px">		
						<input type="text" id="input_order" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">LIST PASIEN</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

			<div class="portlet box red">
				<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_order">
						<thead>
							<tr class="info" >
								<th style="text-align:center;width:20px;"> No.</th>
								<th>Tanggal Daftar</th>
								<th>#Rekam Medis</th>
								<th>Nama</th>
								<th>Diagnosa</th>
								<th>Dokter Pengirim</th>
								<th>Unit Asal</th>
								<th style="width:80px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$i=0;
								foreach ($listorder as $data) {
									$tgl = strtotime($data['waktu_mulai']);
									$hasil = date('d F Y H:s', $tgl);
									echo '
										<tr>
											<td>'.($i+1).'</td>
											<td style="text-align:center" width="150">'.$hasil.'</td>
											<td>'.$data['rm_id'].'</td>
											<td>'.$data['nama'].'</td>
											<td>'.$data['diagnosis_nama'].'</td>	
											<td>'.$data['nama_petugas'].'</td>									
											<td>'.$data['nama_dept'].'</td>
											<td style="text-align:center">
												<a href="#tambahPeri" data-toggle="modal" onClick="getDetailOrder(&quot;'.$data['order_operasi_id'].'&quot;, &quot;'.$i.'&quot;)"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Jadwalkan Operasi"></i></a>
												<a style="cursor:pointer" class="hapusorder"><input type="hidden" class="getid" value="'.$data['order_operasi_id'].'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
											</td>										
										</tr>
									';
									$i++;
								}
							?>

							<!-- <tr>
								<td>1.</td>
								<td style="text-align:center" width="150">12 Desember 2012</td>
								<td style="text-align:right">12112</td>
								<td>Khrisna</td>
								<td>Diagnosa</td>	
								<td>Bejo</td>									
								<td>UNit Bersalin</td>
								<td style="text-align:center">
									<a href="#tambahPeri" data-toggle="modal" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Jadwalkan Operasi"></i></a>	
									<a href="#"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>	
								</td>										
							</tr> -->
						</tbody>
					</table>
				</div>
			</div>
	    </div>
	    <br>
	    <br>
	    <br>
	    </div>
	
    
    	<div class="tab-pane" id="rencanaOperasi">
  			<form method="POST" class="form-horizontal" id="search_list">
		       <div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px">		
						<input type="text" class="form-control" id="input_list" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">LIST RENCANA OPERASI</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_list">
						<thead>
							<tr class="info" >
								<th style="text-align:center;width:20px;"> No.</th>
								<th>#Rekam Medis</th>
								<th>Nama</th>
								<th>Waktu Rencana Operasi</th>
								<th>Diagnosa</th>
								<th>Dokter Pengirim</th>
								<th>Status</th>
								<th>Resep</th>
								<th width="90">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$i = 0;
								foreach ($listoperasi as $data) {
									$i++;
									$tgl = strtotime($data['waktu_rencana']);
									$hasil = date('d F Y H:s', $tgl); 

									echo '
										<tr>
											<td>'.$i.'</td>
											<td>'.$data['rm_id'].'</td>
											<td>'.$data['nama'].'</td>										
											<td style="text-align:center;">'.$hasil.'</td>
											<td>'.$data['diagnosis_nama'].'</td>
											<td>'.$data['nama_petugas'].'</td>										
											<td>Belum Selesai</td>
											<td width="130" style="text-align:center"><a href="#tambahresep" onclick="getResep(&quot;'.$data['visit_id'].'&quot;,&quot;'.$data['sub_visit'].'&quot;)" data-toggle="modal" ><i data-toggle="tooltip" data-placement="top" title="Beri Resep">Tambah Resep</i></a>
												</td>
											<td style="text-align:center">
												<a href="'.base_url().'kamaroperasi/operasidaftarkan/process/'.$data['rencana_id'].'/'.$data['order_operasi_id'].'"><i class="glyphicon glyphicon-list-alt" data-toggle="tooltip" data-placement="top" title="Details Status Operasi"></i></a>
												<a href="#detailStatusOperasi" data-toggle="modal" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
											</td>										
										</tr>
									';
								}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
			</div>
			<br>
			<br>
			<br>
			<br>

			<div class="modal fade" id="tambahresep" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        	<div class="modal-dialog" style="width:1100px;">
	        		<div class="modal-content">
		        		<form class="form-horizontal" role="form" id="submit_resep">	
		        			<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">Form Resep Anastesi</h3>
		        			</div>	
		        			<input type="hidden" id="res_sub_visit">
							<input type="hidden" id="res_visit_id">
		        			<div class="modal-body" style="margin-left:40px;">
		        				<div class="form-group">
									<label class="control-label col-md-3 col-lg-3">Tanggal Resep</label>
									<div class="col-md-5" >
										<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" id="res_date" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-lg-3">Dokter Peresep</label>
									<div class="col-md-6">
										<input type="hidden" id="res_idokter">
										<input type="text" class="form-control" id="res_ndokter" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
									</div>
								</div>
									
								<div class="form-group">
									<label class="control-label col-md-3 col-lg-3">Resep Dokter</label>
									<div class="col-md-6">
										<textarea id="res_resep" class="form-control" name="dokterresep"></textarea>
									</div>
								</div>

		        			</div>
		      				<div class="modal-footer" style="margin-right:40px;margin-left:40px;margin-bottom:20px;">
		 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>	 			       		
		 			       		<button type="success" class="btn btn-success">Simpan</button>
					      	</div>
					    </form>
	        		
	        		<!-- <input type="hidden" id="jml_resep" value="<?php //echo count($visit_resep); ?>"> -->
	        		<center>
	        			<input type="hidden" id="jml_resep" value="0">
						<table class="table table-striped table-bordered table-hover tableDTUtama" id="tableResep" style="width:90%;">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Dokter</th>
									<th>Tanggal</th>
									<th>Deskripsi Resep</th>
									<th>Status Bayar</th>
									<th>Status Ambil</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody id="tbody_resep">								
							</tbody>
						</table>
					</center>
					</div>
	        	</div>        	
	        </div>

			<div class="modal fade" id="detailStatusOperasi" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        	<div class="modal-dialog">
	        		<div class="modal-content">
	        			<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Daftar Operasi Pasien</h3>
	        			</div>	
	        			<div class="modal-body" style="margin-left:40px;">
		        			<form class="form-horizontal" role="form">	
		        				<div class="form-group">
									<label class="control-label col-md-3">No. RM</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="noRM" placeholder="13123" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Nama</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="nama" placeholder="Nama Pasien" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3" >Waktu Operasi </label>
									<div class="col-md-3">	
										<input type="text" class="form-control isian" name="timestart" placeholder=" mulai" disabled/> 
									</div>			
									<label class="control-label col-md-1"> s/d</label>				
									<div class="col-md-3">
										<input type="text" class="form-control isian" name="timeend" placeholder=" selesai" disabled/>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Dokter Operator</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="dp" placeholder="Dr. Jems" data-toggle="modal" data-target="#searchDokter" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Dokter Pelaksana</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="dp" placeholder="Dr. Jems" data-toggle="modal" data-target="#searchDokter" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Dokter Anastesi</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="da" placeholder="Dr. Putu" data-toggle="modal" data-target="#searchDokter" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Dokter Anak</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="da" placeholder="Dr. Putu" data-toggle="modal" data-target="#searchDokter" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Asisten</label>
									<div class="col-md-7">
										<textarea class="form-control isian" name="namaPasien" placeholder="List Nama Asisten" disabled></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Istrumen</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="ins" placeholder="Instrumen" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Sirkuler</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="sir" placeholder="Sirkuler" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3"> Anastesi</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="an" placeholder="Anestesi" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Jenis Anastesi</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="da" placeholder="Jenis Anastesi" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Keadaan Akhir</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="ka" placeholder="Hidup" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Status Operasi</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="so" placeholder="blm selesai" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Tempat Operasi</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="so" placeholder="blm selesai" disabled>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Onfaktur</label>
									<div class="col-md-7">
										<input type="text" class="form-control isian" name="on" placeholder="onfaktur" disabled>
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-3">Keterangan</label>
									<div class="col-md-7">
										<textarea class="form-control isian" name="ket" placeholder="Keterangan" disabled></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3"> </label>
									<div class="col-md-5">
										<button type="button" class="btn btn-success" id="simpanbtn" data-dismiss="modal">Simpan</button>
										<button type="button" class="btn btn-danger" id="batalbtn">Batal</button>
									</div>
								</div>
							</form>
	        			</div>
	      				<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" id="editbtn">Edit</button>	 			       		
	 			       		<button type="button" class="btn btn-danger" id="clsbtn" data-dismiss="modal">Keluar</button>
				      	</div>
	        		</div>
	        	</div>        	
	        </div>
		</div>

		<div class="tab-pane" id="riwayatOperasi">
  			<form method="POST" id="search_riwayat">
		       	<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left: -400px">		
						<input type="text" class="form-control" id="input_riwayat" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>		
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">RIWAYAT OPERASI</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tableRiwayat">
						<thead>
							<tr class="info" >
								<th style="text-align:center;width:20px;"> No.</th>
								<th>#Rekam Medis</th>
								<th>Nama</th>
								<th>Tanggal Operasi</th>
								<th>Waktu Mulai</th>
								<th>Waktu Selesai</th>
								<th>Diagnosa</th>
								<th>Dokter Pelaksana</th>
								<th width="90">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$i = 0;
								foreach ($listriwayat as $data) {
									$tgl = strtotime($data['waktu_mulai']);
									$tanggal = date('d F Y', $tgl); 
									$waktu_mulai = date('H:i', $tgl); 

									$tgls = strtotime($data['waktu_selesai']);
									$waktu_selesai = date('H:i', $tgls); 

									echo'
									<tr>
										<td>'.++$i.'</td>
										<td>'.$data['rm_id'].'</td>
										<td>'.$data['nama'].'</td>										
										<td style="text-align:center;">'.$tanggal.'</td>
										<td style="text-align:center;">'.$waktu_mulai.'</td>
										<td style="text-align:center;">'.$waktu_selesai.'</td>
										<td>'.$data['diagnosis_nama'].'</td>
										<td>'.$data['nama_petugas'].'</td>										
										<td style="text-align:center">
											<a href="'.base_url().'kamaroperasi/operasidetail/detail/'.$data['operasi_id'].'/'.$data['order_operasi_id'].'"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Detail"></i></a>
										</td>										
									</tr>';		
								}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
			</div>
			<br>
			<br>
			<br>
			<br>
		</div>

       <div class="tab-pane" id="farmasi">
        	<div class="dropdown" id="btnBawahInventori" >
	            <div id="titleInformasi">Inventori</div>
	            <div id="btnBawahInventori" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="infoInventori" class="tutupBiru">
				<form class="form-horizontal informasi" role="form">
	            	<div class="form-group">
		            	<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
						<div class="col-md-2" style="width:200px">
							<select class="form-control select" name="filterInv" id="filterInv">
								<option value="Jenis Obat" selected>Jenis Obat</option>
								<option value="Merek">Merek</option>
								<option value="Nama Obat">Nama Obat</option>							
							</select>	
						</div>
						<div class="col-md-2" style="margin-left:-15px; width:200px;" >
									<input type="text" class="form-control" id="filterby" name="valfilter" placeholder="Value"/>
						</div>

								<div class="col-md-1" >
									<button class="btn btn-danger">EXPIRED</button> 
								</div>
								<div class="col-md-1" >
									<button class="btn btn-warning">EX 3 BULAN</button>
								</div>
								<div class="col-md-1" style="margin-left: 20px;">
									<button class="btn btn-warning">EX 6 BULAN</button>
								</div>
					</div>
				</form>
				<div class="form-group" >
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th> Nama Obat </th>
									<th> No Batch </th>
									<th> Harga Jual </th>
									<th> Merek </th>
									<th> Stok</th>
									<th> Satuan </th>
									<th width="200"> Tanggal Kadaluarsa </th>
									<th width="100"> Action </th>								
								</tr>
							</thead>
							<tbody>
								<tr>
									<td align="center">1</td>
									<td>A</td>
									<td style="text-align:right">12</td>
									<td style="text-align:right">120120</td>
									<td>a</td>									
									<td style="text-align:right">2</td>
									<td style="text-align:right">12</td>
									<td style="text-align:center">12 Mei 1201</td>
									<td style="text-align:center"><a href="#inout" data-toggle="modal" class="edObat" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
											<a href="#edInvenBer" data-toggle="modal" class="edObat"><i class="glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
										</td>										
								</tr>
								<tr>
									<td>2</td>
									<td>A</td>
									<td style="text-align:right">12</td>
									<td style="text-align:right">120120</td>
									<td>a</td>									
									<td style="text-align:right">2</td>
									<td style="text-align:right">12</td>
									<td style="text-align:center">12 Mei 1201</td>
									<td style="text-align:center"><a href="#inout" data-toggle="modal" class="edObat" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
											<a href="#edInvenBer" data-toggle="modal" class="edObat"><i class="glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
										</td>													
								</tr>
							</tbody>
						</table>
					</div>
					<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button> 
					
				</div>
				<br><br>
	        </div>
			<div class="modal fade" id="inout" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
	        			</div>
	        			<div class="modal-body">
	        			<form class="form-horizontal informasi" role="form">
	
		        			<div class="form-group">
		        					<label class="control-label col-md-3" >Tanggal 
									</label>
									<div class="col-md-4" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
										</div>
									</div>
									
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" >In / Out 
								</label>
								<div class="col-md-4">
					         		<select class="form-control select" name="iober" id="iober">
										<option value="IN" selected>IN</option>
										<option value="OUT">OUT</option>					
									</select>
								</div>	
							</div>

							<div class="form-group">
		        					<label class="control-label col-md-3" >Jumlah 
									</label>
									<div class="col-md-4" >
					         		<input type="text" class="form-control" name="jmlInOutBer" placeholder="Jumlah">
									</div>
									
							</div>
							<div class="form-group">
		        					<label class="control-label col-md-3" >Sisa Stok 
									</label>
									<div class="col-md-4" >
					         		<input type="text" class="form-control" name="sisaInOutBer" placeholder="Sisa Stok">
									</div>
									
							</div>
							<div class="form-group">
		        					<label class="control-label col-md-3" >Keterangan 
									</label>
									<div class="col-md-6" >
										<textarea class="form-control" placeholder="Keterangan"></textarea>
									</div>
		
							</div>
							</form>
							
	        			</div>
	        			<div class="modal-footer">
	        				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
	 			       		<button type="button" class="btn btn-success" data-dismiss="modal">Simpan</button>
				      	</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="edInvenBer" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
		        			</div>
		        			<div class="modal-body">
		        			<form class="form-horizontal" role="form">
				            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th  style="text-align:left" width="10%"> Waktu </th>
											<th  style="text-align:left"> IN / OUT </th>
											<th  style="text-align:left"> Jumlah </th>
											<th  style="text-align:left"> Stok Akhir </th>
											<th  style="text-align:left"> Jenis </th>
											<th  style="text-align:left">  Keterangan </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
											
									</tbody>
								</table>

			        			
								</form>
								
		        			</div>
		        			<div class="modal-footer">
		 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					      	</div>
						</div>
					</div>
			</div>

			<div class="dropdown" id="btnBawahMintaObat">
	            <div id="titleInformasi">Permintaan Farmasi</div>
	            <div id="btnBawahMintaObat" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoMintaObat">
            	<form class="form-horizontal" role="form">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="noPermFarmBers" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control" data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
								</div>
							</div>
	        			</div>

	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="ketObatFarBers" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>
					<a href="#modalMintaFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<th width="20"> No. </th>
										<th> Nama Obat </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Stok Unit </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputMintaFar" class="addKosong">
									<!-- <tr>
										<td colspan="6" style="text-align:center" id="dataKosong">DATA KOSONG</td>												
									</tr> -->
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning">RESET</button> &nbsp;
								<button class="btn btn-success">SIMPAN</button> 
							</span>
						</div>
						<br>
					</div>	
					<div class="modal fade" id="modalMintaFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
			        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
			        			</div>
			        			<div class="modal-body">

				        			<div class="form-group">
										<div class="form-group">	
											<div class="col-md-5" style="margin-left:20px;">
												<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama Obat"/>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-info">Cari</button>
											</div>
											<br><br>	
										</div>		
										<div style="margin-right:10px;margin-left:10px;"><hr></div>
										<div class="portlet-body" style="margin: 0px 20px 0px 15px">
											<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa">
												<thead>
													<tr class="info">
														<th>Nama Obat</th>
														<th>Satuan</th>
														<th>Merek</th>
														<th>Stok Gudang</th>
														<th>Tgl Kadaluarsa</th>
														<th width="10%">Pilih</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>100</td>
														<td>Paramex</td>
														<td style="text-align:center"><a href="#" class ="addNewMintaFar"><i class="glyphicon glyphicon-check"></i></a></td>
													</tr>
													<tr>
														<td>Panadol</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>100</td>
														<td>Paramex</td>
														<td style="text-align:center"><a href="#" class ="addNewMintaFar"><i class="glyphicon glyphicon-check"></i></a></td>
													</tr>
												</tbody>
											</table>												
										</div>
									</div>
			        			</div>
			        			<div class="modal-footer">
			 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						      	</div>
							</div>
						</div>
					</div>

				</form>
			</div>	    
			<br>
	           	
	       	<div class="dropdown" id="btnBawahRetDepartemen">
	            <div id="titleInformasi">Retur Farmasi</div>
	            <div id="btnBawahRetFarmasi" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
           	<div id="infoRetDepartemen">
            	<form class="form-horizontal" role="form">
            		<div class="informasi">
            			<br>
            			<div class="form-group">
            				<div class="col-md-2">
            					<label class="control-label">Nomor Retur</label>
            				</div>
            				<div class="col-md-3">
            					<input type="text" class="form-control" name="noRetFarBers" id="noRetFarBers" placeholder="Nomor Retur"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
            					<label class="control-label">Tanggal Permintaan</label>
            				</div>
            				<div class="col-md-2">
            					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control" data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
								</div>
							</div>
            			</div>

            			<div class="form-group">
            				
							<div class="col-md-2">
            					<label class="control-label">Keterangan</label>
            				</div>
            				<div class="col-md-3">
								<textarea class="form-control" id="ketObatRetFarBers" name="ketObatRetFarBers"></textarea>	
							</div>
            			</div>
            		</div>

            		<a href="#modalRetFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left : 40px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>
					
					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabRetur">
								<thead>
									<tr class="info" >
										<th width="20"> No. </th>
										<th > Nama Obat </th>
										<th > Satuan </th>
										<th > Merek </th>
										<th > Stok Unit </th>
										<th > Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								
								<tbody  id="addinputRetFar" class="addKosong">
										<!-- <tr>
											<td colspan="6" style="text-align:center" id="dataKosong">DATA KOSONG</td>												
										</tr> -->
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning">RESET</button> &nbsp;
								<button class="btn btn-success">SIMPAN</button> 
							</span>
						</div>
						<br>
					</div>
					
					<div class="modal fade" id="modalRetFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
			        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
			        			</div>
			        			<div class="modal-body">

				        			<div class="form-group">
										<div class="form-group">	
											<div class="col-md-5" style="margin-left:20px;">
												<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama petugas"/>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-info">Cari</button>
											</div>
											<br><br>	
										</div>		
										<div style="margin-left:10px; margin-right:10px;"><hr></div>
										<div class="portlet-body" style="margin: 0px 20px 0px 15px">
											<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa">
												<thead>
													<tr class="info">
														<td>Nama Obat</td>
														<td>Satuan</td>
														<td>Merek</td>
														<td>Tgl Kadaluarsa</td>
														<td width="10%">Pilih</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td style="text-align:center"><a href="#" class ="addNewRetFar"><i class="glyphicon glyphicon-check"></i></a></td>
													</tr>
													<tr>
														<td>Panadol</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td style="text-align:center"><a href="#" class ="addNewRetFar"><i class="glyphicon glyphicon-check"></i></a></td>
													</tr>

												</tbody>
											</table>												
										</div>
									</div>
			        			</div>
			        			<div class="modal-footer">
			 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						      	</div>
							</div>
						</div>
					</div>
				</form>
			</div>	
			<br>
	    </div>

        <div class="tab-pane" id="logistik">
	       	<div class="dropdown" id="btnBawahInventoriBarang">
	            <div id="titleInformasi">Inventori</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoInventoriBarang" class="tutupBiru">
				
				<div class="form-group" >
					<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th> Nama Barang </th>
									<th> Group </th>
									<th> Merek </th>
									<th> Harga </th>
									<th> Stok</th>
									<th> Satuan </th>
									<th width="120"> Action </th>								
								</tr>
							</thead>
							<tbody>
								<tr>
									<td align="center">1</td>
									<td>A</td>
									<td>a</td>
									<td>a</td>									
									<td style="text-align:right">200</td>
									<td style="text-align:right">12</td>
									<td>A</td>
									<td style="text-align:center"><a href="#inoutbar" data-toggle="modal" class="edObat" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
											<a href="#edInvenBerBar" data-toggle="modal" class="edObat"><i class="glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
										</td>										
								</tr>
								<tr>
									<td align="center">2</td>
									<td>A</td>
									<td>a</td>
									<td>a</td>									
									<td style="text-align:right">200</td>
									<td style="text-align:right">12</td>
									<td>A</td>
									<td style="text-align:center"><a href="#inoutbar" data-toggle="modal" class="edObat" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
											<a href="#edInvenBerBar" data-toggle="modal" class="edObat"><i class="glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
										</td>											
								</tr>
							</tbody>
						</table>
					</div>
					<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button> 
					
	        	</div>
	        </div>
			<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<form class="form-horizontal informasi" role="form">
	    	
				        			<div class="form-group">
				        					<label class="control-label col-md-3" >Tanggal 
											</label>
											<div class="col-md-4" >
							         			<div class="input-icon">
													<i class="fa fa-calendar"></i>
													<input type="text" style="cursor:pointer;background-color:white" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
												</div>
							         		</div>
											
									</div>
									<div class="form-group">
											<label class="control-label col-md-3" >In / Out 
											</label>
											<div class="col-md-4">
							         		<select class="form-control select" name="ioberbar" id="ioberbar">
													<option value="IN" selected>IN</option>
													<option value="OUT">OUT</option>					
											</select>
											</div>

									</div>
									<div class="form-group">
				        					<label class="control-label col-md-3" >Jumlah 
											</label>
											<div class="col-md-4" >
							         		<input type="text" class="form-control" name="jmlInOutBerBar" placeholder="Jumlah">
											</div>
											
									</div>
									<div class="form-group">
				        					<label class="control-label col-md-3" >Sisa Stok 
											</label>
											<div class="col-md-4" >
							         		<input type="text" class="form-control" name="sisaInOutBerBar" placeholder="Sisa Stok">
											</div>
											
									</div>
									<div class="form-group">
				        					<label class="control-label col-md-3" >Keterangan 
											</label>
											<div class="col-md-6" >
												<textarea class="form-control" placeholder="Keterangan"></textarea>
											</div>
				
									</div>
								</form>
									
			        		</div>
		        			<div class="modal-footer">
		        				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					      		<button type="button" class="btn btn-success" data-dismiss="modal">Simpan</button>
					      	</div>
						</div>
					</div>
			</div>
			<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
		        			</div>
		        			<div class="modal-body">
		        			<form class="form-horizontal" role="form">
				            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th  style="text-align:left" width="10%"> Waktu </th>
											<th  style="text-align:left"> IN / OUT </th>
											<th  style="text-align:left"> Jumlah </th>
											<th  style="text-align:left"> Stok Akhir </th>
											<th  style="text-align:left"> Jenis </th>
											<th  style="text-align:left">  Keterangan </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td align="center"></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
											
									</tbody>
								</table>

			        			
								</form>
								
		        			</div>
		        			<div class="modal-footer">
		 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					      	</div>
						</div>
					</div>
			</div>
			<br>

			<div class="dropdown" id="btnBawahPermintaanBarang" style="margin-left:10px;width:98.5%">
	            <div id="titleInformasi">Permintaan Logistik</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoPermintaanBarang" class="tutupBiru">
            	<form class="form-horizontal" role="form">
            		<br>
            		<div class="informasi">
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermLogBers" id="noPermFarmBers" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control" data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
								</div>
							</div>
	        			</div>

	        			<div class="form-group">
							<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<textarea class="form-control" id="ketObatLogBers" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
        			</div>
        			
					<a href="#modalMintaLogBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left : 40px; font-size:11pt;">&nbsp;Tambah Barang</i></a>
					<div class="clearfix"></div>
					
					<div class="portlet box red">
						<div class="portlet-body" style="margin: 15px 40px 0px 40px">
						
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<th width="20"> No. </th>
										<th> Nama Barang </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Stok Unit </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								
								<tbody  id="addinputMintaLog" class="addKosong">
										<!-- <tr>
											<td colspan="6" style="text-align:center" id="dataKosong">DATA KOSONG</td>												
										</tr> -->
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning">RESET</button> &nbsp;
								<button class="btn btn-success">SIMPAN</button> 
							</span>
						</div>
						<br>
					</div>
				</form>
				
				<div class="modal fade" id="modalMintaLogBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
		        			</div>
		        			<div class="modal-body">

			        			<div class="form-group">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:10px;">
											<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama petugas"/>
										</div>
										<div class="col-md-2">
											<button type="button" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
									<div style="margin-left:10px; margin-right:10px;"><hr></div>
									<div class="portlet-body" style="margin: 0px 20px 0px 15px">
										<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa">
											<thead>
												<tr class="info">
													<td>Nama Barang</td>
													<td>Satuan</td>
													<td>Merek</td>
													<td>Tgl Kadaluarsa</td>
													<td width="10%">Pilih</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Paramex</td>
													<td>Paramex</td>
													<td>Paramex</td>
													<td>Paramex</td>
													<td style="text-align:center"><a href="#" class ="addNewLog"><i class="glyphicon glyphicon-check"></i></a></td>
												</tr>
												<tr>
													<td>Panadol</td>
													<td>Paramex</td>
													<td>Paramex</td>
													<td>Paramex</td>
													<td style="text-align:center"><a href="#" class ="addNewLog"><i class="glyphicon glyphicon-check"></i></a></td>
												</tr>

											</tbody>
										</table>												
									</div>
								</div>
		        			</div>
		        			<div class="modal-footer">
		 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					      	</div>
						</div>
					</div>
				</div>
			</div>	    
			<br>
	    </div>

        <div class="tab-pane" id="laporan"> 
        	<div class="informasi" style="margin-left:35px;">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Kamar Operasi</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form">
	        		

	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
						<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
		        		<div class="col-md-3" style="margin-left:-20px;">
							<input type="text" class="form-control" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" id="dokter">
						</div>

						<div class="col-md-3">
							<div class="input-daterange input-group" id="datepicker">
							    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="start"  data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
							    <span class="input-group-addon">to</span>
							    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right" style="margin-right:30px" >
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>  

			  
	        <br> 
        </div>
        
        <div class="tab-pane" id="master">  
       		<div class="dropdown" id="">
	            <div id="titleInformasi">Jasa Pelayanan Kamar Operasi</div>
	        </div>
            <br>
            <div class="informasi form-horizontal">

				<div class="form-group">
					<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Periode :</label>
					<div class="col-md-3" style="margin-left:-15px">
						<div class="input-daterange input-group" id="datepicker">
						    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="start" data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly placeholder="<?php echo date("d/m/Y");?>" />
						    <span class="input-group-addon">to</span>
						    <input type="text" style="cursor:pointer;background-color:white" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>" />
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2"> <i class="glyphicon glyphicon-filter"></i>&nbsp;Cara Bayar</label>
					<div class="input-group col-md-2">
						<select class="form-control select" name="carabayar" id="carabayar">
							<option value="" selected>Pilih</option>
							<option value="BPJS">BPJS</option>	
							<option value="Asuransi">Asuransi</option>		
							<option value="Gratis">Gratis</option>	
							<option value="Tunjangan">Tunjangan</option>					
						</select>
					</div>	
				</div>

		    	<div class="form-group">
					<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp; Nama Paramedis </label>
					<div class="input-group col-md-2">
						<input type="text" class="form-control" readonly style="background-color:white;cursor:pointer" placeholder="Search Paramedis" data-toggle="modal" data-target="#searchParamedis" id="paramedis">
					</div>

					<div class="pull-right" style="margin-right:20px">
	        			<div class="col-md-3">
	        				<button class="btn btn-warning">FILTER</button>
	        			</div>
        			</div>
				</div>
			</div>
			<hr class="garis">
		    <div class="portlet-body" style="margin: 10px 10px 0px 10px;">
				<table class="table table-striped table-bordered table-hover tableDTUtamaScroll" id="">
					<thead>
						<tr class="info">
							<th width="20">No.</th>
							<th>Tanggal</th>
							<th>Cara Bayar</th>
							<th>Tindakan</th>
							<th>Nama Pasien</th>
							<th>JP</th>
							<th>Dokter Operator</th>
							<th width="100">Jasa Dokter Operator</th>
							<th>Dokter Anastesi</th>
							<th width="100">Jasa Dokter Anastesi</th>
							<th>Dokter Anak</th>
							<th width="100">Jasa Dokter Anak</th>
							<th width="100">Jasa Asisten</th>
							<th>JS</th>
							<th>BAKHP</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody_resep">
						<tr>
							<td style="text-align:center">1</td>
							<td style="text-align:center">12 Mei 1201</td>
							<td>Cara Byar</td>
							<td>Operasi</td>
							<td>Klaudius Jemly Naban Abadi </td>
							<td style="text-align:right">1000</td>
							<td>Putu Widyana Santika</td>
							<td><input type="text" name="jasadokteroperator" style="text-align:right" id="jasadokteroperator" class="input-sm form-control jasa" value="100"></td>
							<td>I Made Arya Beta Widyatmika</td>
							<td><input type="text" name="jasadokteranastesi" style="text-align:right" id="jasadokteranastesi" class="input-sm form-control jasa" value="100"></td>
							<td>Siapa aja alah</td>
							<td><input type="text" name="jasadokteranak" style="text-align:right" id="jasadokteranak" class="input-sm form-control jasa" value="100"></td>
							<td><input type="text" name="jasaasisten" style="text-align:right" id="jasaasisten" class="input-sm form-control jasa" value="100"></td>
							<td style="text-align:right">2000</td>
							<td style="text-align:right">12000</td>
							<td style="text-align:center">
								<a href="#" id="btneditko"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
								<a href="#" id="btnsaveko"><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Simpan"></i></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
        </div>

        <div class="tab-pane" id="tagihan" style="padding-bottom:50px;">   
			<form class="form-horizontal" method="POST" id="submitTagihanSearch">
		       	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px">		
						<input type="text" id="search_tagihan" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        <a onclick="setStatus('20')" class="btn btn-warning"> Tambah Invoice Baru</a>
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">TAGIHAN</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_tagihan">
						<thead>
							<tr class="info">
								<th style="text-align:center;width:20px;">No.</th>
								<th>Unit</th>
								<th>Nomor Invoice</th>
								<th>Nomor Visit</th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Cara Bayar</th>
								<th style="text-align:center;width:25px;">Action</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>			
			</div>  
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>    
	    </div>


        
    </div>

    <!-- search dokter modal -->
						<div class="modal fade" id="searchDokter" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
				        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        				<h3 class="modal-title" id="myModalLabel">Pilih Dokter</h3>
				        			</div>
				        			<div class="modal-body">
										<div class="form-group">	
											<div class="col-md-5">
												<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama dokter"/>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-info">Cari</button>
											</div>	
										</div>	
										<br>	
										<div style="margin-left:5px; margin-right:5px;"><hr></div>
										<div class="portlet-body" style="margin: 0px 10px 0px 10px">
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr class="info">
														<td>Nama Dokter</td>
														<td width="10%">Pilih</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Jems</td>
														<td style="text-align:center"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></td>
													</tr>
													<tr>
														<td>Putu</td>
														<td style="text-align:center"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></td>
													</tr>
												</tbody>
											</table>												
										</div>
				        			</div>
				        			<div class="modal-footer">
				 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
							      	</div>
								</div>
							</div>
						</div>
						<!-- end modal -->

						<!-- Modal tambah diagnosa -->
						<div class="modal fade" id="searchDiagnosa" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-right:15px;">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
				        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        				<h3 class="modal-title" id="myModalLabel">Pilih Diagnosa</h3>
				        			</div>
				        			<div class="modal-body">
										<div class="form-group">	
											<div class="col-md-5">
												<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Kata kunci"/>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-info">Cari</button>
											</div>	
										</div>	
										<br>	
										<div style="margin-left:5px; margin-right:5px;"><hr></div>
										<div class="portlet-body" style="margin: 0px 10px 0px 10px">
											<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
												<thead>
													<tr class="info">
														<td width="30%;">Kode Diagnosa</td>
														<td>Keterangan</td>
														<td width="10%">Pilih</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>99999</td>
														<td>Diagnosa Lain-lain</td>
														<td style="text-align:center"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></td>
													</tr>
												</tbody>
											</table>												
										</div>
				        			</div>
				        			<div class="modal-footer">
				 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
							      	</div>
								</div>
							</div>
						</div>
						<!-- end modal tambah diagnosa-->

			<div class="modal fade" id="tambahPeri" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        	<div class="modal-dialog">
	        		<div class="modal-content">
	        			<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Daftar Operasi Pasien</h3>
	        			</div>	
	        			<div class="modal-body" style="margin-left:40px;">
	        				<form class="form-horizontal" role="form" id="submit_operasi">
		        				<div class="form-group">
									<label class="control-label col-md-3">Waktu Operasi</label>
									<div class="col-md-5" >
										<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" id="order_date" style="cursor:pointer;" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Nama Pasien</label>
									<div class="col-md-6">
										<input type="hidden" id="order_operasi_id">
										<input type="hidden" id="order_overview_id">
										<input type="hidden" id="order_visit_id">
										<input type="text" class="form-control" name="nama" id="order_nama" value="Khrisna" readonly>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Diagnosa Utama</label>
									<div class="col-md-2">
										<input type="text" class="form-control" placeholder="Kode" id="order_kd1" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" id="order_nd1" readonly>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Diagnosa Sekuder</label>
									<div class="col-md-2">
										<input type="text" class="form-control" placeholder="Kode"  id="order_kd2" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" id="order_nd2" readonly>
									</div>
									<label class="control-label">1</label>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3"></label>
									<div class="col-md-2">
										<input type="text" class="form-control" placeholder="Kode"  id="order_kd3" readonly >
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" id="order_nd3" readonly>
									</div>
									<label class="control-label">2</label>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3"></label>
									<div class="col-md-2">
										<input type="text" class="form-control" placeholder="Kode"  id="order_kd4" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" id="order_nd4" readonly>
									</div>
									<label class="control-label">3</label>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3"></label>
									<div class="col-md-2">
										<input type="text" class="form-control" placeholder="Kode"  id="order_kd5" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Diagnosa" id="order_nd5" readonly>
									</div>
									<label class="control-label">4</label>
								</div>

								<!-- <div class="form-group">
									<label class="control-label col-md-3">Dokter Pelaksana</label>
									<div class="col-md-5">
										<input type="text" class="form-control isian" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter">
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-3">Dokter Anastesi</label>
									<div class="col-md-5">
										<input type="text" class="form-control isian" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Jenis Anestesi</label>
									<div class="col-md-7">
										<input type="text" class="form-control" name="jenisAnestesi" placeholder="Jenis Anestesi">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Asisten</label>
									<div class="col-md-7">
										<textarea class="form-control" name="namaPasien" placeholder="Daftar Nama Pasien"></textarea>	
									</div>
								</div> -->

								<div class="form-group">
									<label class="control-label col-md-3">Tempat Operasi</label>
									<div class="col-md-7">
										<input type="text" class="form-control" name="tempatOperasi" id="order_tempat" placeholder="Tempat Operasi">
									</div>
								</div>
	        			</div>
	      				<div class="modal-footer">
		      				<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
	 			       		<button type="submit" class="btn btn-success">Tambah</button>
				      	</div>

				      	</form>
	        		</div>
	        	</div>        	
	        </div>
</div>

<script type="text/javascript">
	function setStatus(departmen){
		var u = "<?php echo base_url() ?>kamaroperasi/";
		localStorage.setItem('department', departmen);
		localStorage.setItem('url', u);
		window.location.href="<?php echo base_url() ?>invoice/tambahinvoice";
	}
</script>