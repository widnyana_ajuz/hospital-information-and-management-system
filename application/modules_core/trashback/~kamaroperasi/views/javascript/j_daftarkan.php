<?php 

?>
<script type="text/javascript">
	$(window).ready(function(){
		//----------- prepare diagnosa -------------//
		var diagnosa;
		$('#kode_pra').click(function(){
			diagnosa = 1;
		});
		$('#nama_pra').click(function(){
			diagnosa = 1;
		});
		$('#kode_post').click(function(){
			diagnosa = 2;
		});
		$('#nama_post').click(function(){
			diagnosa = 2;
		});

		$('#submit_diagnosa').submit(function(event){
			event.preventDefault();
			var item = {};
			item['search'] = $('#diag_katakunci').val();

			$.ajax({
				type:"POST",
				data:item,
				url:'<?php echo base_url(); ?>kamaroperasi/operasidaftarkan/search_diagnosa',
				success:function(data){
					$('#tbody_diagnosa').empty();

					if(data.length!=0){
						for(var i = 0; i<data.length; i++){
							$('#tbody_diagnosa').append(
								'<tr>'+
									'<td>'+data[i]['diagnosis_id']+'</td>'+
									'<td style="word-wrap: break-word;white-space: pre-wrap; ">'+data[i]['diagnosis_nama']+'</td>'+
									'<td style="text-align:center; cursor:pointer;"><a style="cursor:pointer" onclick="getDiagnosa(&quot;'+data[i]['diagnosis_id']+'&quot;,&quot;'+data[i]['diagnosis_nama']+'&quot;,&quot;'+diagnosa+'&quot;)"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_diagnosa').append('<tr><td colspan="3"><center>Data Diagnosa Tidak Ditemukan</center></td></tr>');
					}
				}
			});
		});

		//------------ prepare dokter -------------//
		var dokter;
		$('#lap_ndokterbedah').click(function(){
			dokter = 1;
		});

		$('#lap_ndokteranak').click(function(){
			dokter = 2;
		});

		$('#lap_ndokteranastesi').click(function(){
			dokter = 3;
		});

		$('#submit_dokter').submit(function(event){
			event.preventDefault();
			var item = {};
			item['search'] = $('#dokter_katakunci').val();

			$.ajax({
				type:"POST",
				data:item,
				url:"<?php echo base_url();?>kamaroperasi/operasidaftarkan/searchDokter",
				success:function(data){
					$('#tbody_dokter').empty();

					if(data.length!=0){
						for(var i = 0; i<data.length; i++){
							$('#tbody_dokter').append(
								'<tr>'+
									'<td>'+data[i]['nama_petugas']+'</td>'+
									'<td style="text-align:center; cursor:pointer;"><a style="cursor:pointer" onclick="getDokter(&quot;'+data[i]['petugas_id']+'&quot;,&quot;'+data[i]['nama_petugas']+'&quot;,&quot;'+dokter+'&quot;)"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_dokter').append('<tr><td colspan="3"><center>Data Dokter Tidak Ditemukan</center></td></tr>');
					}
				}
			});
		});

		//-------- submit detail operasi ----------//
		$('#submit_laporan').submit(function(event){
			event.preventDefault();

			var asisten = "";
			$('.nama-asisten').each(function(){
				asisten += $(this).val()+",";
			});

			var item = {};
			item[1] = {};

			var id = $('#rencana_id').val();
			var operasi_id = "OK"+id.substr(2, 12);

			item[1]['rencana_id'] = $('#rencana_id').val();
			item[1]['operasi_id'] = operasi_id;
			item[1]['ruangan'] = $('#lap_ruangan').val();
			item[1]['spesialisasi_kamar_operasi'] = $('#lap_specialisasi').val();
			item[1]['dokter_bedah'] = $('#lap_idokterbedah').val();
			item[1]['dokter_anak'] = $('#lap_idokteranak').val();
			item[1]['dokter_anestesi'] = $('#lap_idokteranastesi').val();
			item[1]['asisten'] = asisten.slice(0,-2);
			item[1]['instrumen'] = $('#lap_instrument').val();
			item[1]['onlop'] = $('#lap_onlop').val();
			item[1]['diagnosa_pra'] = $('#kode_pra').val();
			item[1]['diagnosa_post'] = $('#kode_post').val();
			item[1]['waktu_mulai'] = $('#lap_datestart').val();
			item[1]['waktu_selesai'] = $('#lap_dateend').val();
			item[1]['jenis_operasi'] = $('#lap_jenis').val();
			item[1]['lingkup_operasi'] = $('#lap_lingkup').val();
			item[1]['pa_jaringan'] = $('#lap_pajaringan').val();
			item[1]['jenis_jaringan_kirim'] = $('#lap_jenisjaringan').val();
			item[1]['uraian_tindakan'] = $('#lap_uraian').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>kamaroperasi/operasidaftarkan/save_operasidetail',
				success:function(data){
					console.log(data);
					myAlert("Data Berhasil Ditambahkan");

					var delay=2000; //1 seconds

					setTimeout(function(){
					  window.location.href="<?php echo base_url()?>kamaroperasi/homeoperasi#rencanaOperasi";
					}, delay); 
				},error:function(data){
					console.log(data);
				}
			});
			// OKyymmddxxxxzz
		});
		
	});

	function getDiagnosa(id, nama, diag){
		if(diag==1){
			$('#kode_pra').val(id);
			$('#nama_pra').val(nama);
		}else if(diag==2){
			$('#kode_post').val(id);
			$('#nama_post').val(nama);
		}

		$('#tbody_diagnosa').empty();
		$('#tbody_diagnosa').append('<tr><td colspan="3"><center>Cari Data Diagnosa</center></td></tr>');
		$('#diag_katakunci').val('');
		$('#searchDiagnosa').modal('hide');
	}

	function getDokter(id, nama, dokter){
		if(dokter==1){
			$('#lap_idokterbedah').val(id);
			$('#lap_ndokterbedah').val(nama);
		}else if(dokter==2){
			$('#lap_idokteranak').val(id);
			$('#lap_ndokteranak').val(nama);
		}else if(dokter==3){
			$('#lap_idokteranastesi').val(id);
			$('#lap_ndokteranastesi').val(nama);
		}

		$('#tbody_dokter').empty();
		$('#tbody_dokter').append('<tr><td colspan="3"><center>Cari Data Dokter</center></td></tr>');
		$('#dokter_katakunci').val('');
		$('#searchDokter').modal('hide');
	}

	function myAlert(text) {
		// simulate loading (for demo purposes only)
		// setTimeout( function() {
		var m = '<p>'+text+'</p>';
		// create the notification
		var notification = new NotificationFx({
			message : m,
			layout : 'growl',
			effect : 'genie',
			type : 'notice', // notice, warning or error
		});

		// show the notification
		notification.show();

		// }, 1200 );
		// disable the button (for demo purposes only)
	}
</script>