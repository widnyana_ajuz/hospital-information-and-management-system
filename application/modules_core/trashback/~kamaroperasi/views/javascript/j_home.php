<?php
?>

<script type="text/javascript">
	var searchData = {};

	$(window).ready(function(){
		$.ajax({
			type:"POST",
			url:"<?php echo base_url() ?>kamaroperasi/homeoperasi/get_listorder",
			success:function(data){
				searchData = data;
				console.log(searchData);
			}
		});

		$('#search_order').submit(function(event){
			event.preventDefault();
			var item = {};
			item['search'] = $('#input_order').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url();?>kamaroperasi/homeoperasi/search_operasi',
				success:function(data){
					var t = $('#table_order').DataTable();
					var no=0;
					var tanggal, stgl;
					searchData = data;
					t.clear().draw();

					for(var i=0; i<data.length; i++){
						var action = '<a href="#tambahPeri" data-toggle="modal" onClick="getDetailOrder(&quot;'+data[i]['order_operasi_id']+'&quot;, &quot;'+i+'&quot;)"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Jadwalkan Operasi"></i></a>';
						action +='<a style="cursor:pointer" class="hapusorder"><input type="hidden" class="getid" value="'+data[i]['order_operasi_id']+'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>';

						no++;
						stgl = data[i]['tanggal_visit'].split(" ");
						tanggal = changeDate(stgl[0]);
						
						t.row.add([
							no,
							tanggal,
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['diagnosis_nama'],
							data[i]['nama_petugas'],
							data[i]['nama_dept'],
							action,
							no
						]).draw();
					}
				}
			});
		});
		
		$(document).on('click','.hapusorder',function(){
			var id = $(this).children('.getid').val();
			var item = {};
			item['search'] = $('#input_order').val();
			
			$.ajax({
				type:"POST",
				data:item,
				url:"<?php echo base_url()?>kamaroperasi/homeoperasi/hapus_order/"+id,
				success:function(data){
					console.log(data);
					var t = $('#table_order').DataTable();
					var no=0;
					var tanggal, stgl;
					searchData = data;
					t.clear().draw();

					for(var i=0; i<data.length; i++){
						var action = '<a href="#tambahPeri" data-toggle="modal" onClick="getDetailOrder(&quot;'+data[i]['order_operasi_id']+'&quot;, &quot;'+i+'&quot;)"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Jadwalkan Operasi"></i></a>';
						action +='<a style="cursor:pointer" class="hapusorder"><input type="hidden" class="getid" value="'+data[i]['order_operasi_id']+'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>';

						no++;
						stgl = data[i]['tanggal_visit'].split(" ");
						tanggal = changeDate(stgl[0]);
						
						t.row.add([
							no,
							tanggal,
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['diagnosis_nama'],
							data[i]['nama_petugas'],
							data[i]['nama_dept'],
							action,
							no
						]).draw();
					}
				},
				error:function(data){
					console.log(data);
				}	
			});
		});

		$('#submit_operasi').submit(function(event){
			event.preventDefault();
			var item = {};
			item[1] = {};
			var v_id = $('#order_visit_id').val();

			item[1]['order_operasi_id'] = $('#order_operasi_id').val();
			item[1]['overview_id'] = $('#order_overview_id').val();
			item[1]['diagnosa_utama'] = $('#order_kd1').val();
			item[1]['waktu_rencana'] = $('#order_date').val();
			item[1]['tempat_operasi'] = $('#order_tempat').val();

			$.ajax({
				type:"POST",
				data:item,
				url:"<?php echo base_url(); ?>kamaroperasi/homeoperasi/submit_operasi/"+v_id,
				success:function(data){
					console.log(data);
					var t = $('#table_order').DataTable();
					t.clear().draw();

					$('#order_tempat').val('');
					$('#tambahPeri').modal('hide');
					myAlert('Data Berhasil Ditambahkan');
				},error:function(data){
					console.log();
				}
			});
		});

		//------------------ list order rencana here ----------------------//
		$('#search_list').submit(function(event){
			event.preventDefault();
			var item  = {};
			item['search'] = $('#input_list').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>kamaroperasi/homeoperasi/search_listoperasi',
				success:function(data){
					console.log(data);
					var t = $('#table_list').DataTable();
					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						var resep= '<center><a href="#tambahresep" onclick="getResep(&quot;'+data[i]['visit_id']+'&quot;,&quot;'+data[i]['sub_visit']+'&quot;)" data-toggle="modal" ><i data-toggle="tooltip" data-placement="top" title="Beri Resep">Tambah Resep</i></a></center>';
						var action = '<center><a href="<?php echo base_url(); ?>kamaroperasi/operasidaftarkan/process/'+data[i]['rencana_id']+'/'+data[i]['order_operasi_id']+'"><i class="glyphicon glyphicon-list-alt" data-toggle="tooltip" data-placement="top" title="Details Status Operasi"></i></a>';
							action += '<a href="#detailStatusOperasi" data-toggle="modal" ><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></center>';

						t.row.add([
							(i+1),
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['waktu_rencana'],
							data[i]['diagnosis_nama'],
							data[i]['nama_petugas'],
							'Belum Selesai',
							resep,
							action,
							i
						]).draw();
					}
				},error:function(data) {
					console.log(data);
				}
			});
		});

		$('#submit_resep').submit(function(event){
			event.preventDefault();
			var item = {};
			item[1] = {};

			item[1]['visit_id'] = $('#res_visit_id').val();
			item[1]['sub_visit'] = $('#res_sub_visit').val();
			item[1]['tanggal'] = $('#res_date').val();
			item[1]['dokter'] = $('#res_idokter').val();
			item[1]['resep'] = $('#res_resep').val();

			var dokter = $('#res_ndokter').val();
			
			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>kamaroperasi/homeoperasi/save_resep',
				success:function(data){
					console.log(data);
					var t = $('#tableResep').DataTable();
					var no = $('#jml_resep').val();

					var action = '<a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value=""><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>';

					t.row.add([
						++no,
						dokter,
						changeDate(data['tanggal']),
						data['resep'],
						"-",
						"-",
						action,
						no
					]).draw();

					myAlert('Data Berhasil Ditambahkan');
				},error:function(data){
					console.log(data);
				}
			});
		});

		$('#res_ndokter').focus(function(){
			var $input = $('#res_ndokter');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#res_idokter').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});

		//--------------- list riwayat ----------------//
		$('#search_riwayat').submit(function(event){
			event.preventDefault();
			var item = {};
			item['search'] = $('#input_riwayat').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>kamaroperasi/homeoperasi/search_listriwayat',
				success:function(data){
					console.log(data);
					var t = $('#tableRiwayat').DataTable();
					var action;

					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						action = '<center><a href="<?php echo base_url() ?>kamaroperasi/operasidetail/detail/'+data[i]['operasi_id']+'/'+data[i]['order_operasi_id']+'"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Detail"></i></a></center>';
						t.row.add([
							(i+1),
							data[i]['rm_id'],
							data[i]['nama'],
							getJustDate(data[i]['waktu_mulai']),
							getJustTime(data[i]['waktu_mulai']),
							getJustTime(data[i]['waktu_selesai']),
							data[i]['diagnosis_nama'],
							data[i]['nama_petugas'],
							action,
							i
						]).draw();
					}
				},error:function(data){
					console.log(data);
				}
			});
		});

		$('#submitTagihanSearch').submit(function(event){
			event.preventDefault();

			var item = {};
			item['search'] = $('#search_tagihan').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo  base_url(); ?>kamaroperasi/homeoperasi/search_tagihan',
				success:function(data){
					console.log(data);
					var t = $('#table_tagihan').DataTable();
					var no = 0;
					var action;
					t.clear().draw();

					for(var i=0; i<data.length; i++){
						no++;
						if(data[i]['carapembayaran'] == "BPJS"){
							action = '<a href="<?php echo base_url() ?>kamaroperasi/invoicebpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}else{
							action = '<a href="<?php echo base_url() ?>kamaroperasi/invoicenonbpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}

						t.row.add([
							no,
							data[i]['nama_dept'],
							data[i]['no_invoice'],
							data[i]['visit_id'],
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['alamat_skr'],
							data[i]['carapembayaran'],
							action,
							i
						]).draw();
					}

				}
			});
		});


	});

	function getDetailOrder(order_id, index){
		console.log(searchData[index]);
		var waktu = changeDateTime(searchData[index]['waktu_mulai']);

		$('#order_nama').val(searchData[index]['nama']);
		$('#order_operasi_id').val(order_id);
		$('#order_overview_id').val(searchData[index]['over_id']);
		$('#order_visit_id').val(searchData[index]['visit_id']);
		$('#order_date').val(waktu);
		$('#order_kd1').val(searchData[index]['diagnosa1']);
		$('#order_nd1').val(searchData[index]['diagnosis_nama']);
		$('#order_kd2').val(searchData[index]['diagnosa2']);
		$('#order_kd3').val(searchData[index]['diagnosa3']);
		$('#order_kd4').val(searchData[index]['diagnosa4']);
		$('#order_kd5').val(searchData[index]['diagnosa5']);

		var item = {};
		item['d2'] = searchData[index]['diagnosa2'];
		item['d3'] = searchData[index]['diagnosa3'];
		item['d4'] = searchData[index]['diagnosa4'];
		item['d5'] = searchData[index]['diagnosa5'];

		$.ajax({
			type:"POST",
			data:item,
			url:"<?php echo base_url(); ?>kamaroperasi/homeoperasi/get_dnama",
			success:function(data){
				console.log(data);
				$('#order_nd2').val(data['nama1']);
				$('#order_nd3').val(data['nama2']);
				$('#order_nd4').val(data['nama3']);
				$('#order_nd5').val(data['nama4']);
			}
		});
	}

	function getResep(visit_id, sub){
		$('#res_visit_id').val(visit_id);
		$('#res_sub_visit').val(sub);

		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>kamaroperasi/homeoperasi/get_resep/'+sub,
			success:function(data){
				console.log(data);
				var t = $('#tableResep').DataTable();
				var action;
				var no = 0;
				t.clear().draw();
				$('#jml_resep').val(data.length);

				for(var i=0; i<data.length; i++){
					if(data[i]['status_bayar']!="SUDAH" || data[i]['status_ambil']!="SUDAH"){
						action = '<center><a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value=""><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a></center>';
					}else{
						action = "";
					}

					t.row.add([
						++no,
						data[i]['nama_petugas'],
						changeDate(data[i]['tanggal']),
						data[i]['resep'],
						data[i]['status_bayar'],
						data[i]['status_ambil'],
						action,
						no
					]).draw();
				}
			},error:function(data){
				console.log(data);
			}
		});
	}

	function changeDate(tgl_lahir){
		var remove = tgl_lahir.split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0];

		return tgl;
	}

	function changeDateTime(tgl_lahir){
		var getdate = tgl_lahir.split(" ");
		var remove = getdate[0].split("-");
		var times = getdate[1].split(":");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+"/"+remove[1]+"/"+remove[0]+" "+times[0]+":"+times[1];

		return tgl;
	}

	function getJustDate(tgl){
		var jdate = tgl.split(" ");
		var remove = jdate[0].split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var jtgl = remove[2]+" "+bulan+" "+remove[0];

		return jtgl;
	}

	function getJustTime(tgl){
		var jdate = tgl.split(" ");
		var remove = jdate[1].split(":");
		
		var times = remove[0]+":"+remove[1];

		return times;
	}

	function myAlert(text) {
		// simulate loading (for demo purposes only)
		// setTimeout( function() {
		var m = '<p>'+text+'</p>';
		// create the notification
		var notification = new NotificationFx({
			message : m,
			layout : 'growl',
			effect : 'genie',
			type : 'notice', // notice, warning or error
		});

		// show the notification
		notification.show();

		// }, 1200 );
		// disable the button (for demo purposes only)
	}

	//----------------- assests ----------------//	
	// var table = $('#tabelpermintaangudang').DataTable();
	// 				table.row(this_minta.parents('tr') ).remove().draw();
	// 				table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	// 		            cell.innerHTML = i+1;
	// 		        } );

	// 	t.on( 'order.dt search.dt', function () {
	// 					        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	// 					            cell.innerHTML = i+1;
	// 					        } );
	// 					    } ).draw();

	function setStatus(departmen){
		var u = "<?php echo base_url() ?>kamaroperasi/";
		localStorage.setItem('department', departmen);
		localStorage.setItem('url', u);
		window.location.href="<?php echo base_url() ?>invoice/tambahinvoice";
	}
</script>