<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homekasirtindakan extends Operator_base {
	function __construct(){

		parent:: __construct();
		$this->load->model("m_homekasirtindakan");
		$data['page_title'] = "Kasir";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load template
		$data['content'] = 'home';
		$this->check_auth('R');
		$data['user'] = $this->user;
		$data['menu_view'] = $this->menu();

		$data['listkasir'] = $this->m_homekasirtindakan->get_listkasir();

		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$this->load->view('base/operator/template', $data);
	}

	public function search_tagihan(){
		$search = $_POST['search'];

		$result = $this->m_homekasirtindakan->search_tagihan($search);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

}
