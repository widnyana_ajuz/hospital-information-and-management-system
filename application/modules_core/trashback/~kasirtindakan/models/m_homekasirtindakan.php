<?php
class m_homekasirtindakan extends CI_Model {
	public function search_tagihan($search){
    	$sql = "SELECT *, t.cara_bayar as carapembayaran FROM tagihan t, pasien p, visit v, visit_rj vr, master_dept m 
    			WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.sub_visit = vr.rj_id AND vr.unit_tujuan = m.dept_id
    			AND vr.visit_id = v.visit_id AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '$search')
    			";

    	$query = $this->db->query($sql);
    	return $query->result_array();
    }

    public function get_listkasir(){
    	$sql = "SELECT v.visit_id, p.rm_id, p.nama, p.alamat_skr, t.cara_bayar as carapembayaran FROM tagihan t, pasien p, visit v, master_dept m 
    			WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id GROUP BY t.visit_id
    			";

    	$query = $this->db->query($sql);
    	return $query->result_array();
    }
}
?>
