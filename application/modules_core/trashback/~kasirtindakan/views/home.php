<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kasirtindakan/homekasirtindakan">KASIR</a>
	</li>
</div>

<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	   	<li class="active"><a href="#list" data-toggle="tab">List Invoice</a></li>
	    <li><a href="#riwayat" data-toggle="tab">Riwayat Pembayaran</a></li>
	    <li><a href="#deposit" data-toggle="tab">Deposit</a></li>
	  <!--   <li><a href="#export" data-toggle="tab">Export INA-CBG's</a></li> -->
	</ul>

	<div id="my-tab-content" class="tab-content">

		<div class="tab-pane active" id="list">
			<form method="POST" id="search_submit">
		       	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">		
						<input type="text" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        <a href="<?php echo base_url() ?>kasirtindakan/tambahinvoice" data-toggle="modal" class="btn btn-warning"> Tambah Invoice Baru</a>
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">LIST INVOICE</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama">
						<thead>
							<tr class="info">
								<th style="text-align:center;width:20px;">No.</th>
								<th>Unit</th>
								<th>Nomor Invoice</th>
								<th>Nomor Visit</th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Cara Bayar</th>
								<th style="text-align:center;width:25px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td align="center">1</td>
								<td>Bersalin</td>
								<td align="right">1212121</td>
								<td align="right">32323</td>	
								<td align="right">123123</td>									
								<td>Selena</td>
								<td>Rumahnya</td>
								<td>Ansuransi</td>
								<td style="text-align:center">
									<a href="<?php echo base_url() ?>kasirtindakan/invoicenonbpjs" ><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>
								</td>										
							</tr>
							<tr>
								<td align="center">1</td>
								<td>Bersalin</td>
								<td align="right">1212121</td>
								<td align="right">32323</td>	
								<td align="right">123123</td>								
								<td>Jems</td>
								<td>Rumahnya</td>
								<td>BPJS</td>
								<td style="text-align:center">
									<a href="<?php echo base_url() ?>kasirtindakan/invoicebpjs" ><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Proses"></i></a>
								</td>										
							</tr>
						</tbody>
					</table>
				</div>			
			</div>
			</div>
			<br>
			<br>
			<br>
			<br> 
	    </div>
    	
	    <div class="tab-pane" id="riwayat">
	    	<form method="POST" id="search_submit">
		       	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">		
						<input type="text" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">RIWAYAT INVOICE</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama">
						<thead>
							<tr class="info">
								<th style="text-align:center;width:20px;">No.</th>
								<th>Unit</th>
								<th>Nomor Invoice</th>
								<th>Nomor Visit</th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Cara Bayar</th>
								<th style="text-align:center;width:25px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Bersalin</td>
								<td>1212121</td>
								<td>32323</td>	
								<td>123123</td>									
								<td>Selena</td>
								<td>Rumahnya</td>
								<td>Ansuransi</td>
								<td style="text-align:center">
									<a href="<?php echo base_url() ?>kasirtindakan/viewinvoicenonbpjs" ><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Details"></i></a>
								</td>										
							</tr>
							<tr>
								<td>2</td>
								<td>Bersalin</td>
								<td>1212121</td>
								<td>32323</td>	
								<td>123123</td>									
								<td>Jems</td>
								<td>Rumahnya</td>
								<td>BPJS</td>
								<td style="text-align:center">
									<a href="<?php echo base_url() ?>kasirtindakan/viewinvoicebpjs" ><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Details"></i></a>
								</td>										
							</tr>
						</tbody>
					</table>
				</div>			
			</div> 
			</div>
			<br>
			<br>
			<br>
			<br>
	    </div>

	    <div class="tab-pane" id="deposit">
	    	<form method="POST" id="search_submit">
		       	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">		
						<input type="text" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        <a href="<?php echo base_url() ?>kasirtindakan/tambahdeposit" data-toggle="modal" class="btn btn-warning"> Tambah Deposit Baru</a>
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">DEPOSIT</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:10px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -95px 0px">
					<table class="table table-striped table-bordered table-hover tableDTUtama" id="">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Unit</th>
								<th>#Rekam Medis</th>
								<th>Nomor Visit</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Tanggal Masuk</th>
								<th>Diagnosis</th>
								<th>Total Deposit</th>
								<th width="80">Action</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<tr>
								<td align="center">1</td>
								<td>Bersalin</td>
								<td align="right">123</td>
								<td align="right">321</td>
								<td>Joe Boy</td>
								<td>Rumahnya </td>
								<td style="text-align:center">12 Mei 2012</td>
								<td>SDA - qweqweqwe</td>
								<td style="text-align:right">2000</td>
								<td style="text-align:center">
									<a href="<?php echo base_url();?>kasirtindakan/listdeposit">
										<i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Kelola deposit"></i>
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>
	    </div>

	    <!-- <div class="tab-pane" id="export">

	    	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
				<table class="table table-striped table-bordered table-hover tableDT" id="tableinacbgbpjs">
					<thead>
						<tr class="info">
							<th width="20">No.</th>
							<th>#Rekam Medis</th>
							<th>Nama Pasien</th>
							<th>Unit</th>
							<th>Nomor BPJS</th>
							<th>Kelas BPJS</th>
							<th>Kelas Perawatan</th>
							<th width="80">Action</th>
						</tr>
					</thead>
					<tbody id="tbody_resep">
						<tr>
							<td width="20" align="center">No.</td>
							<td align="right">123</td>
							<td>Bejo</td>
							<td>RJ</td>
							<td align="right">123123</td>
							<td>12</td>
							<td>Kelas 1</td>
							<td style="text-align:center">
								<a href="#viewinacbgs" data-toggle="modal">
									<i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View"></i>
								</a>
								<a href="#">
									<i class="glyphicon glyphicon-export" data-toggle="tooltip" data-placement="top" title="Export .txt"></i>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="modal fade" id="viewinacbgs" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="">
					<div class="modal-dialog" style="width:900px;">
						<div class="modal-content">
							<form class="form-horizontal" role="form">
								<div class="modal-header">
					   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					   				<h3 class="modal-title" id="myModalLabel">Edit Input INA-CBG's</h3>
					   			</div>
								<div class="modal-body">
									<table border="0" width="100%" class="tbladdinacbg">
										<tr>
											<td width="20">1.</td>
											<td width="30%">Kode Rumah Sakit</td>
											<td colspan="3"> <input readonly type="text" class="form-control input-sm" name="koders" placeholder="Kode Rumah Sakit" style="width:190px;"> </td>
										</tr>
										<tr>
											<td width="20">2.</td>
											<td width="30%">Kelas Rumah Sakit</td>
											<td colspan="3"> <div class="input-group col-md-3">
													<select class="form-control input-sm" readonly name="kelasrs" id="kelasrs">
														<option value="" selected>Pilih</option>
														<option value="A">A</option>
														<option value="B">B</option>
														<option value="C">C</option>
														<option value="D">D</option>							
													</select>
												</div>	
											</td>
										</tr>
										<tr>
											<td width="20">3.</td>
											<td width="30%">Nomor Rekam Medis</td>
											<td colspan="3"> <input type="text" class="form-control input-sm" name="nomorrm" placeholder="Nomor RM" style="width:190px;" readonly> </td>
										</tr>
										<tr>
											<td width="20">4.</td>
											<td width="30%">Kelas Perawatan</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="kelasperawatan" placeholder="III/II/I/Utama/VIP" style="width:190px;" readonly>  </td>
										</tr>
										<tr>
											<td width="20">5.</td>
											<td width="30%">Biaya Perawatan</td>
											<td colspan="3"> 
												<div class="input-group col-md-3">
													<span class="input-group-addon" id="basic-addon1">Rp.</span>
													<input type="text" class="form-control input-sm" name="biayaperawatan" readonly>
												</div>
											</td>
										</tr>
										<tr>
											<td width="20">6.</td>
											<td width="30%">Jenis Perawatan</td>
											<td colspan="3"> <input type="text" class="form-control input-sm" name="jenisperawatan" style="width:190px;" readonly> </td>
										</tr>
										<tr>
											<td width="20">7.</td>
											<td width="30%">Tanggal Masuk</td>
											<td colspan="3"> <input type="text" class="form-control input-sm" name="tanggalmasuk" style="width:190px;" readonly> </td>
										</tr>
										<tr>
											<td width="20">8.</td>
											<td width="30%">Tanggal Keluar</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="tanggalkeluar" style="width:190px;" readonly> </td>
										</tr>
										<tr>
											<td width="20">9.</td>
											<td width="30%">Lama Rawat</td>
											<td colspan="3">
												<div class="input-group col-md-3">
													<input type="text" class="form-control input-sm" name="lamarawat" readonly>
													<span class="input-group-addon" id="basic-addon1" style="width:70px;">hari</span>
												</div>
											</td>
										</tr>
										<tr>
											<td width="20">10.</td>
											<td width="30%">Tanggal Lahir</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="tgllahir" style="width:190px;" readonly> </td>
										</tr>
										<tr>
											<td width="20">11.</td>
											<td width="30%">Umur Tahun</td>
											<td colspan="3">
												<div class="input-group col-md-3">
													<input type="text" class="form-control input-sm" name="umurtahun" readonly>
													<span class="input-group-addon" id="basic-addon1" style="width:70px;">tahun</span>
												</div>
											</td>
										</tr>
										<tr>
											<td width="20">12.</td>
											<td width="30%">Umur Hari</td>
											<td colspan="3"> 
												<div class="input-group col-md-3">
													<input type="text" class="form-control input-sm" name="umurhari" readonly>
													<span class="input-group-addon" id="basic-addon1" style="width:70px;">hari</span>
												</div>
											</td>
										</tr>
										<tr>
											<td width="20">13.</td>
											<td width="30%">Jenis Kelamin </td>
											<td colspan="3"> <input type="text" class="form-control input-sm" name="jk" style="width:190px;" readonly> </td>
										</tr>
										<tr>
											<td width="20">14.</td>
											<td width="30%">Cara Pulang</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="carapulang" style="width:190px;" readonly>  </td>
										</tr>
										<tr>
											<td width="20">15.</td>
											<td width="30%">Berat Lahir</td>
											<td colspan="3"> <div class="input-group col-md-3">
													<input type="text" class="form-control input-sm" name="beratlahir" readonly>
													<span class="input-group-addon" id="basic-addon1" style="width:70px;">gram</span>
												</div>
											</td>
										</tr>
										<tr>
											<td width="20">16.</td>
											<td width="30%">Diagnosa Utama</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="diagnosautama" style="width:190px;" readonly></td>
										</tr>
										<tr>
											<td width="20">17.</td>
											<td width="30%">Diagnosa Sekunder</td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 1" data-toggle="modal" data-target="#searchDiagnosa" name="dns1" style="width:190px;"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 11" data-toggle="modal" data-target="#searchDiagnosa" name="dns11" style="width:190px;"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 21" data-toggle="modal" data-target="#searchDiagnosa" name="dns21" style="width:190px;"></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 2" data-toggle="modal" data-target="#searchDiagnosa" name="dns2" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 12" data-toggle="modal" data-target="#searchDiagnosa" name="dns12" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 22" data-toggle="modal" data-target="#searchDiagnosa" name="dns22" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 3" data-toggle="modal" data-target="#searchDiagnosa" name="dns3" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 13" data-toggle="modal" data-target="#searchDiagnosa" name="dns13" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 23" data-toggle="modal" data-target="#searchDiagnosa" name="dns23" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 4" data-toggle="modal" data-target="#searchDiagnosa" name="dns4" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 14" data-toggle="modal" data-target="#searchDiagnosa" name="dns14" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 24" data-toggle="modal" data-target="#searchDiagnosa" name="dns24" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 5" data-toggle="modal" data-target="#searchDiagnosa" name="dns1" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 15" data-toggle="modal" data-target="#searchDiagnosa" name="dns15" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 25" data-toggle="modal" data-target="#searchDiagnosa" name="dns25" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 6" data-toggle="modal" data-target="#searchDiagnosa" name="dns6" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 16" data-toggle="modal" data-target="#searchDiagnosa" name="dns16" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 26" data-toggle="modal" data-target="#searchDiagnosa" name="dns26" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 7" data-toggle="modal" data-target="#searchDiagnosa" name="dns7" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 17" data-toggle="modal" data-target="#searchDiagnosa" name="dns17" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 27" data-toggle="modal" data-target="#searchDiagnosa" name="dns27" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 8" data-toggle="modal" data-target="#searchDiagnosa" name="dns8" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 18" data-toggle="modal" data-target="#searchDiagnosa" name="dns18" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 28" data-toggle="modal" data-target="#searchDiagnosa" name="dns28" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 9" data-toggle="modal" data-target="#searchDiagnosa" name="dns9" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 19" data-toggle="modal" data-target="#searchDiagnosa" name="dns19" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 29" data-toggle="modal" data-target="#searchDiagnosa" name="dns29" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 10" data-toggle="modal" data-target="#searchDiagnosa" name="dns10" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Diagnosa Sekunder 20" data-toggle="modal" data-target="#searchDiagnosa" name="dns20" style="width:190px;" ></td>
											<td></td>
										</tr>
										<tr>
											<td colspan="5">&nbsp;</td>
										</tr>
										<tr>
											<td width="20">18.</td>
											<td width="30%">Prosedur/Tindakan ICD-9-CM</td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 1" data-toggle="modal" data-target="#searchICD9" name="dns1" style="width:190px;"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 11" data-toggle="modal" data-target="#searchICD9" name="dns11" style="width:190px;"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 21" data-toggle="modal" data-target="#searchICD9" name="dns21" style="width:190px;"></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 2" data-toggle="modal" data-target="#searchICD9" name="dns2" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly readonly placeholder="Prosedur/tindakan 12" data-toggle="modal" data-target="#searchICD9" name="dns12" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 22" data-toggle="modal" data-target="#searchICD9" name="dns22" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 3" data-toggle="modal" data-target="#searchICD9" name="dns3" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 13" data-toggle="modal" data-target="#searchICD9" name="dns13" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 23" data-toggle="modal" data-target="#searchICD9" name="dns23" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 4" data-toggle="modal" data-target="#searchICD9" name="dns4" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 14" data-toggle="modal" data-target="#searchICD9" name="dns14" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 24" data-toggle="modal" data-target="#searchICD9" name="dns24" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 5" data-toggle="modal" data-target="#searchICD9" name="dns1" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 15" data-toggle="modal" data-target="#searchICD9" name="dns15" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 25" data-toggle="modal" data-target="#searchICD9" name="dns25" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 6" data-toggle="modal" data-target="#searchICD9" name="dns6" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 16" data-toggle="modal" data-target="#searchICD9" name="dns16" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 26" data-toggle="modal" data-target="#searchICD9" name="dns26" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 7" data-toggle="modal" data-target="#searchICD9" name="dns7" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 17" data-toggle="modal" data-target="#searchICD9" name="dns17" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 27" data-toggle="modal" data-target="#searchICD9" name="dns27" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 8" data-toggle="modal" data-target="#searchICD9" name="dns8" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 18" data-toggle="modal" data-target="#searchICD9" name="dns18" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 28" data-toggle="modal" data-target="#searchICD9" name="dns28" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 9" data-toggle="modal" data-target="#searchICD9" name="dns9" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 19" data-toggle="modal" data-target="#searchICD9" name="dns19" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 29" data-toggle="modal" data-target="#searchICD9" name="dns29" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%"></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 10" data-toggle="modal" data-target="#searchICD9" name="dns10" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 20" data-toggle="modal" data-target="#searchICD9" name="dns20" style="width:190px;" ></td>
											<td><input type="text" class="form-control input-sm" readonly placeholder="Prosedur/tindakan 30" data-toggle="modal" data-target="#searchICD9" name="dns30" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20">19.</td>
											<td width="30%">Record ID/No. Urut dalam file tersebut</td>
											<td colspan="3"><input type="text"  readonly class="form-control input-sm" name="urutfile" style="width:190px;"></td>
										</tr>
										<tr>
											<td width="20">20.</td>
											<td width="30%">Kode CBG</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="kodecbg" style="width:190px;" readonly></td>
										</tr>
										<tr>
											<td width="20">21.</td>
											<td width="30%">Tarif CBG</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="tarifcbg" style="width:190px;" readonly></td>
										</tr>
										<tr>
											<td width="20">22.</td>
											<td width="30%">Deskripsi CBG</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="deskripsicbg" style="width:190px;" readonly></td>
										</tr>
										<tr>
											<td width="20">23.</td>
											<td width="30%">ALOS</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="alos" plasceholder="0" style="width:130px;" readonly></td>
										</tr>
										<tr>
											<td width="20">24.</td>
											<td width="30%">Nama Pasien</td>
											<td colspan="3"><input type="text" class="form-control input-sm" name="namapasien" style="width:190px;" readonly></td>
										</tr>
										<tr>
											<td width="20">25.</td>
											<td width="30%">Dokter Penanggung Jawab</td>
											<td colspan="3"><input type="text"  readonly class="form-control input-sm" name="dokterpj" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" style="width:190px;" ></td>
										</tr>
										<tr>
											<td width="20">26.</td>
											<td width="30%">Nomor SKP</td>
											<td colspan="3"><input type="text" readonly class="form-control input-sm" name="noskp" style="width:190px;"></td>
										</tr>
										<tr>
											<td width="20"></td>
											<td width="30%">Nomor Kartu Peserta</td>
											<td colspan="3"><input type="text" readonly class="form-control input-sm" name="nokartupeserta" style="width:190px;"></td>
										</tr>
										<tr>
											<td width="20">27.</td>
											<td width="30%">Surat Rujukan</td>
											<td colspan="3">
												<div class="input-group col-md-3">
													<select class="form-control input-sm" readonly name="suratrujukan" id="suratrujukan">
														<option value="" selected>Pilih</option>
														<option value="ada">Ada</option>
														<option value="tanpa surat rujukan">Tanpa Surat Rujukan</option>
																				
													</select>
												</div>		
											</td>
										</tr>
										<tr>
											<td width="20">28.</td>
											<td width="30%">BHP (jika ada)</td>
											<td colspan="3"><input type="text" readonly class="form-control input-sm" name="bhp" style="width:190px;"></td>
										</tr>
										<tr>
											<td width="20">29.</td>
											<td width="30%">Harga BHP</td>
											<td colspan="3">
												<div class="input-group col-md-3">
													<span class="input-group-addon" id="basic-addon1">Rp.</span>
													<input type="text" class="form-control input-sm" readonly name="hargabhp">
												</div>
											</td>
										</tr>
										<tr>
											<td width="20">30.</td>
											<td width="30%">Severiti level 3</td>
											<td colspan="3">
												<div class="input-group col-md-3">
													<select class="form-control input-sm" readonly name="severitilv3" id="severitilv3">
														<option value="" selected>Pilih</option>
														<option value="ada">Ada</option>
														<option value="tidak ada">Tidak Ada</option>
																				
													</select>
												</div>	
											</td>
										</tr>
										<tr>
											<td width="20">31.</td>
											<td width="30%">Tipe Tarif sesuai Rumah Sakit</td>
											<td colspan="3"><input type="text" readonly class="form-control input-sm" name="tarifsesuairs" style="width:190px;"></td>
										</tr>

									</table>
			       				</div>
				        		<br>
				        		<div class="modal-footer">
				        			<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
				 			     	<button type="submit" class="btn btn-success" id="">Simpan</button>
							    </div>
							</form>
						</div>
					</div>
				</form>
			</div> 


	    <br>
	    </div> -->

	</div>

</div>

