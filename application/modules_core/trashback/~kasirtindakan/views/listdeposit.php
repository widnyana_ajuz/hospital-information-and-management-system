
<div class="title">
	PENGELOLAAN DEPOSIT RAWAT INAP
</div>
<div class="bar">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kasirtindakan/homekasirtindakan">Kasir</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kasirtindakan/listdeposit">Kelola Deposit</a>
	</li>
</div>

<div class="backregis">
	<div id="my-tab-content" class="tab-content">

		<div class="informasi">		
			<form class="form-horizontal" role="form">
	            <table width="95%" >
	            	<tr>
	            		<td width="50%">
	            			<fieldset class="fsStyle">
								<legend>
					                Informasi Pasien
								</legend>
								<br>
								<div class="form-group">
									<label class="control-label1 col-md-4">Nomor Rekam Medis</label>
									<div class="col-md-5 nama">
										: &nbsp;&nbsp;121212
									</div>
								</div>
								<div class="form-group">
									<label class="control-label1 col-md-4">Nama Pasien</label>
									<div class="col-md-5 nama">
										: &nbsp;&nbsp;Joe 
									</div>
								</div>
								<div class="form-group">
									<label class="control-label1 col-md-4">Alamat</label>
									<div class="col-md-5 nama">
										: &nbsp;&nbsp;Rumahnya
									</div>
								</div>
								<div class="form-group">
									<label class="control-label1 col-md-4">Kamar Rawat Inap</label>
									<div class="col-md-5 nama">
										: &nbsp;&nbsp;Kamar Mayat
									</div>
								</div>
								<br>
							</fieldset>
	            		</td>
	            		<td width="50%">
	            			<fieldset class="fsStyle">
								<legend>
					                Riwayat Deposit
								</legend>

								<a href="#modaldeposit" data-toggle="modal" style="margin-left:5px;"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Deposit">&nbsp;Tambah Deposit</i></a>
				
						    	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover tableDTUtama" id="">
										<thead>
											<tr class="info">
												<th width="20">No.</th>
												<th>Tanggal</th>
												<th>Jumlah</th>
												<th>Petugas</th>
												<th width="70">Delete</th>
											</tr>
										</thead>
										<tbody id="tbody_resep">
											<tr>
												<td width="20" align="center">No.</td>
												<td style="text-align:center">12 Mei 2012</td>
												<td style="text-align:right">1212</td>
												<td>Jems</td>
												<td style="text-align:center">
													<a href="#">
													<i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
												</td>
												
											</tr>
										</tbody>
									</table>
								</div>

								<div class="modal fade" id="modaldeposit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<form class="form-form-horizontal informasi" role="form">
											<div class="modal-content">
												<div class="modal-header">
								    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
								    				<h3 class="modal-title" id="myModalLabel">Tambah Deposit</h3>
								    			</div>
								    			<div class="modal-body">
													<div class="informasi">
														<div class="form-group">
															<label class="control-label col-md-4">Tanggal Deposit</label>
															<div class="input-group col-md-3" >
																<div class="input-icon">
																	<i class="fa fa-calendar"></i>
																	<input type="text" style="cursor:pointer;background-color:white" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-4"> Jumlah Deposit </label>
															<div class="input-group col-md-4">
																<input type="text" class="form-control" name="jumlahdepo" placeholder="Jumlah Deposit">
															</div>
														</div>
													</div>
								    			</div>
								    			<div class="modal-footer">
											       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
											       		<button type="button" class="btn btn-success" data-dismiss="modal">Deposit</button>
										      	</div>
											</div>
										</form>
									</div>
								</div>
							</fieldset>	
	            		</td>
	            	</tr>
	            </table>				
			</form>
	
		<br>
	</div>
</div>