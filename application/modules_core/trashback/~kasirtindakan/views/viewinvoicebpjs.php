<div class="title">
	KASIR - INVOICE PASIEN BPJS
</div>
<div class="bar">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kasirtindakan/homekasirtindakan">Kasir</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>kasirtindakan/viewinvoicebpjs"> Details Invoice - Nama Pasien</a>
	</li>
</div>

<div class="backregis">
	<div id="my-tab-content" class="tab-content">
		<div id="my-tab-content" class="tab-content">
			<div class="informasi">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4 nama">Nomor Invoice</label>
							<div class="col-md-3 nama">: 0001 </div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Jenis Kunjungan</label>
							<div class="col-md-5">: Biasa</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Visit ID</label>
							<div class="col-md-5">:	20202 </div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Kelas Perawatan</label>
							<div class="col-md-5">: Kelas 1</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Tanggal Bayar</label>
							<div class="col-md-5">: 12 Mei 2012</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Tanggal Kunjungan</label>
							<div class="col-md-5">: 20 Mei 2012</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Nomor Rekam Medis</label>
							<div class="col-md-5">: 123123</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Cara Bayar</label>
							<div class="col-md-5">: BPJS</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Nama Pasien</label>
							<div class="col-md-5">: Bejoe</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Nomor BPJS</label>
							<div class="col-md-5">: 123123</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Alamat</label>
							<div class="col-md-5">: Rumahnya</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label1 col-md-4">Kelas Pelayanan</label>
							<div class="col-md-5">: III</div>
						</div>
					</div>
				</div>

			</div>

			<hr class="garis">

			<form class="form-horizontal" role="form">
				<div id="tagihadmisi">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Admisi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Admisi Tertagih</th>
										<th>Waktu </th>
										<th>Tarif</th>
										
									</tr>
								</thead>
								<tbody id="tbody_resep">
									<tr>
										<td width="20">No.</td>
										<td>Coeg</td>
										<td style="text-align:center">12 Mei 1201</td>
										<td style="text-align:right">1000011</td>
										
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tagihankamar" style="margin-top:30px">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Kamar</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihankamar">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Kamar Tertagih</th>
										<th>Masuk dan Keluar</th>
										<th>Lama</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th>On Faktur</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody id="tbody_ttkamarbpjs">
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tagihanakomodasi" style="margin-top:30px">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Makan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihanakomodasi">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Akomodasi Tertagih</th>
										<th>Unit</th>
										<th>Jumlah</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th width="100">On Faktur</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody id="tbody_ttakomodasibpjs">
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tagihantindakanperawatan" style="margin-top:30px">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Perawatan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihanperawatan">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Perawatan Tertagih</th>
										<th>Unit</th>
										<th>Waktu</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th>On Faktur</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody id="tbody_ttperawatanbpjs">
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tambahtindakanpenunjang" style="margin-top:30px">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Penunjang</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihanpenunjang">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Penunjang Tertagih</th>
										<th>Unit</th>
										<th>Waktu</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th width="100">On Faktur</th>
										<th>Total</th>
										
									</tr>
								</thead>
								<tbody id="tbody_ttpenunjangbpjs">
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div id="tambahtagihantindakanoperasi" style="margin-top:30px">
					<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Operasi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
				
						<table class="table table-striped table-bordered table-hover" id="tbtagihanoperasi">
								<thead>
									<tr class="info">
										<th width="20">No.</th>
										<th>Operasi Tertagih</th>
										<th>Lingkup Operasi</th>
										<th>Waktu</th>
										<th>Tarif</th>
										<th>Tarif BPJS</th>
										<th>Selisih</th>
										<th>On Faktur</th>
										<th>Total</th>
										
									</tr>
								</thead>
								<tbody id="tbody_ttoperasibpjs">
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										<td style="text-align:right;"></td>
										
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><br>

				<div style="margin-right:40px;">
					<div class="form-group">
						<div class="col-md-2 pull-right">
							<label class="control-label pull-right" style="font-size:1.8em;margin-top:-10px;">1.000.000</label>
						</div>
						<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
							Total Tagihan (Rp.) : 
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-2 pull-right">
							<label class="control-label pull-right" style="font-size:1.8em;margin-top:-10px;">1.000.000</label>
						</div>
						<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
							Deposit (Rp.) : 
						</div>
					</div>


					<div class="form-group">
						<div class="col-md-2 pull-right">
							<label class="control-label pull-right" style="font-size:1.8em;margin-top:-10px;">1.000.000</label>
						</div>
						<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
							Kekurangan (Rp.) : 
						</div>
					</div>
					
					<br>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="submit" class="btn btn-info">CETAK</button>			 
						</span>
					</div>
					<br>
				</div>

			</form>

			<br><br>
		</div>
	</div>
</div>
