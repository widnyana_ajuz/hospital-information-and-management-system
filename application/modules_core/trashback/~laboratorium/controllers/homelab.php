<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Homelab extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_homelab");
	}

	public function index($page = 0)
	{
		// load template
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		$data['page_title'] = 'LABORATORIUM';
		$this->session->set_userdata($data);
		$data['content'] = 'home';
		$data['javascript'] = 'javascript/j_home';
		$data['listaps'] = $this->m_homelab->get_listaps();
		$data['listrujuk'] = $this->m_homelab->get_listrujuk();

		$dept_id = $this->m_homelab->get_dept_id('LABORATORIUM');
		$data['dept_id'] = $dept_id['dept_id'];
		$data['jenisperiksa'] = $this->m_homelab->get_periksa($dept_id['dept_id']);
		$tindakan = $this->m_homelab->get_alltindakan($dept_id['dept_id']);
		$data['listperiksa'] = $this->m_homelab->get_listperiksa();

		$i = 0;
		if(!empty($tindakan)){
			$tamp = $tindakan[0]['kategori'];
			foreach ($tindakan as $value) {
				if($tamp==$value['kategori']){
					$value['nomor'] = $i;
					$i++;
				}else{
					$i = 0;
					$value['nomor'] = $i;
				}			

				$tamp = $value['kategori'];
				$result[] = $value;
			}

			$data['tindakan'] = $result;
		}

		$this->load->view('base/operator/template', $data);
	}

	public function search_listaps(){
		$search = $_POST['search'];
		$result = $this->m_homelab->search_listaps($search);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function search_listrujuk(){
		$search = $_POST['search'];
		$result = $this->m_homelab->search_listrujuk($search);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function submit_pemeriksaan(){
		//get data until id
		$insert['penunjang_id'] = $_POST['penunjang_id'];
		$kelas = $_POST['kelas_pelayanan'];
		$nama = $_POST['nama_tindakan'];
		$tindakan = $this->m_homelab->get_tindakpenunjang($nama, $kelas);
		$insert['tindakan_penunjang_id'] = $tindakan['tindakan_penunjang_id'];
		$insert['js'] = $tindakan['js'];
		$insert['jp'] = $tindakan['jp'];
		$insert['bakhp'] = $tindakan['bakhp'];
		$insert['penunjang_detail_id'] = $this->m_homelab->get_penunjang_detail_id($insert['penunjang_id']);
		
		//submit penunjang detail
		$ins = $this->m_homelab->save_tindakan($insert);

		//edit visit penunjang status
		$update['status'] = 'PROSES';
		$upd = $this->m_homelab->update_status($insert['penunjang_id'], $update);

		header("Content-type:application/json");
		echo json_encode($insert);
	}

	public function get_detailperiksa($penunjang_id){
		$result = $this->m_homelab->get_detailperiksa($penunjang_id);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function get_paramedis(){
		$result = $this->m_homelab->get_paramedis();

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function get_allperiksa($id){
		$result = $this->m_homelab->get_allperiksa($id);

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function update_hasilperiksa(){
		$up_detail['penunjang_detail_id'] = $_POST['penunjang_detail_id'];
		$up_detail['hasil'] = $_POST['hasil'];
		$up_detail['nilai_normal'] = $_POST['nilai_normal'];
		$up_detail['keterangan'] = $_POST['keterangan'];
		$up_detail['pemeriksa'] = $_POST['pemeriksa'];

		$update_detail = $this->m_homelab->update_detail($up_detail['penunjang_detail_id'], $up_detail);

		$update['status'] = $_POST['status'];
		$update['penunjang_id'] = $_POST['penunjang_id'];

		$update_status = $this->m_homelab->update_status($update['penunjang_id'], $update);		
	}

	public function get_listperiksa(){
		$result = $this->m_homelab->get_listperiksa();

		header("Content-type:application/json");
		echo json_encode($result);
	}

	public function search_tagihan(){
		$search = $_POST['search'];

		$result = $this->m_homelab->search_tagihan($search);

		header('Content-Type: application/json');
		echo json_encode($result);
	}
}
