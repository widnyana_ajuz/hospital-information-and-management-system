<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Invoicebpjs extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_invoicebpjs");
		$data['page_title'] = "Invoice BPJS";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		redirect('laboratorium/homelab');
	}

	public function invoice($no_invoice){
		$data['content'] = 'tagihan/invoicebpjs';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$invoice = $this->m_invoicebpjs->get_visit_id($no_invoice);
		$visit_id = $invoice['visit_id'];
		$sub_visit = $invoice['sub_visit'];
		$data['visit_id'] = $visit_id;
		$data['sub_visit'] = $invoice['sub_visit'];
		$data['no_invoice'] = $no_invoice;
		$data['invoice'] = $invoice;
		$data['dept_id'] = $this->m_invoicebpjs->get_deptid('LABORATORIUM');

		$pasien = $this->m_invoicebpjs->get_data_pasien($visit_id);
		$data['pasien'] = $pasien;

		$temp = $this->m_invoicebpjs->get_tagihantunjang($sub_visit);
		$data['tagihantunjang'] = $temp;

		$kelas = $invoice['kelas_pelayanan'];
		$i = 0;

		foreach ($temp as $value) {
			$nama = $value['nama_tindakan'];
			$bpjs = $this->m_invoicebpjs->get_tindakanbpjs($nama , $kelas);
			$value['tarif_bpjs'] = intval($bpjs['js'])+intval($bpjs['jp'])+intval($bpjs['bakhp']);
			$value['tarif'] = intval($value['js'])+intval($value['jp'])+intval($value['bakhp']);
			$value['selisih'] = $value['tarif']-$value['tarif_bpjs'];

			$insert[$i++] = $value;
		}

		$data['tagihantunjang'] = $insert;
		// print_r($data['tagihantunjang']);
		// print_r($insert);
		// die;

		$this->load->view('base/operator/template', $data);	
	}
}
