<?php

class m_invoicebpjs extends CI_Model {
	public function get_visit_id($invoice){
		$sql = "SELECT * FROM tagihan WHERE no_invoice = '$invoice'";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}

	public function get_data_pasien($id){
		$sql = "SELECT * FROM visit v, pasien p WHERE v.rm_id = p.rm_id AND v.visit_id = '$id'";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}

	public function get_tindakan($id, $kelas){
		$sql = "SELECT care_id, tindakan_id, dept_id, waktu_tindakan as waktu, on_faktur, paramedis, js, jp, bakhp, tarif, jumlah FROM visit_care WHERE sub_visit = '$id' AND care_id NOT IN (SELECT care_id FROM tagihan_perawatan) ORDER BY care_id ASC";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}	

    public function get_tagihantunjang($id){
        $sql = "SELECT v.*, vp.*, m.*, mp.nama_tindakan, mp.tindakan_penunjang_id FROM visit_penunjang v, visit_penunjang_detail vp, master_tindakan_penunjang mp,  master_dept m WHERE v.penunjang_id = '$id' AND v.penunjang_id = vp.penunjang_id AND mp.tindakan_penunjang_id = vp.tindakan_penunjang_id AND m.dept_id = v.dept_tujuan";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_deptid($nama){
        $query = $this->db->query("SELECT * FROM master_dept WHERE nama_dept = '$nama' LIMIT 1");
        $result = $query->row_array();
        return $result['dept_id'];   
    }

    public function get_tindakanbpjs($nama, $kelas){
        $query = $this->db->query("SELECT * FROM master_tindakan_penunjang WHERE nama_tindakan = '$nama' AND jenis_tarif = '$kelas' LIMIT 1");
        $result = $query->row_array();
        return $result;      
    }

    // SELECT v.care_id, v.tindakan_id, v.dept_id, v.waktu_tindakan as waktu, v.on_faktur, v.paramedis, v.js, v.jp, v.bakhp, v.tarif, v.jumlah, t.tarif_bpjs FROM visit_care v, (SELECT (js+jp+bakhp) as tarif_bpjs FROM master_tindakan WHERE nama_tindakan = (SELECT m.nama_tindakan FROM visit_care v, master_tindakan m WHERE v.sub_visit = '101508130001' AND m.tindakan_id = v.tindakan_id) AND (kelas = "III" OR kelas = "ALL")) t WHERE v.sub_visit = '101508130001' AND v.care_id NOT IN (SELECT care_id FROM tagihan_perawatan) ORDER BY v.care_id ASC

    // (SELECT (js+jp+bakhp) as tarif_bpjs FROM master_tindakan WHERE nama_tindakan = (SELECT m.nama_tindakan FROM visit_care v, master_tindakan m WHERE v.sub_visit = '101508130001' AND m.tindakan_id = v.tindakan_id) AND (kelas = "III" OR kelas = "ALL")) t

    //SELECT care_id, tindakan_id, dept_id, waktu_tindakan as waktu, on_faktur, paramedis, js, jp, bakhp, tarif, jumlah, (SELECT (js+jp+bakhp) as tarif_bpjs FROM master_tindakan WHERE nama_tindakan = (SELECT m.nama_tindakan FROM visit_care v, master_tindakan m WHERE v.sub_visit = '101508130001' AND m.tindakan_id = v.tindakan_id) AND (kelas = "III" OR kelas = "ALL")) as tarif_bpjs FROM visit_care WHERE sub_visit = '101508130001' AND care_id NOT IN (SELECT care_id FROM tagihan_perawatan) ORDER BY care_id ASC//
    
    //SELECT care_id, tindakan_id, dept_id, waktu_tindakan as waktu, on_faktur, paramedis, js, jp, bakhp, tarif, jumlah, (SELECT (js+jp+bakhp) as tarif_bpjs FROM master_tindakan WHERE nama_tindakan = (SELECT m.nama_tindakan FROM visit_care v, master_tindakan m WHERE v.sub_visit = '101508130001' AND m.tindakan_id = v.tindakan_id) AND (kelas = "III" OR kelas = "ALL")) as tarif_bpjs FROM visit_care WHERE sub_visit = '101508130001' AND care_id NOT IN (SELECT care_id FROM tagihan_perawatan) ORDER BY care_id ASC//

    //(SELECT (js+jp+bakhp) as tarif_bpjs FROM master_tindakan WHERE nama_tindakan = (SELECT m.nama_tindakan FROM visit_care v, master_tindakan m WHERE v.sub_visit = '$id' AND m.tindakan_id = v.tindakan_id) AND (kelas = '$kelas' OR kelas = 'ALL'))

    public function get_tarifbpjs($care_id, $kelas){
        $sql = "SELECT (js+jp+bakhp) as tarif_bpjs FROM master_tindakan WHERE nama_tindakan = (SELECT m.nama_tindakan FROM visit_care v, master_tindakan m WHERE m.tindakan_id = v.tindakan_id AND v.care_id = '$care_id') AND (kelas = '$kelas' OR kelas = 'ALL')";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

	public function insert_tagihan($value=''){
		$query = $this->db->insert('tagihan_perawatan',$value);
    	if ($query) {
    		return true;
    	}else{
    		return false;
    	}
    }

    public function get_tagihanperawatan($invoice){
    	$sql = "SELECT * FROM tagihan_perawatan t, master_tindakan mt, master_dept md 
    			WHERE t.tindakan_id = mt.tindakan_id AND md.dept_id = t.dept_id AND t.no_invoice = '$invoice'";
    	$query = $this->db->query($sql);
    	$result = $query->result_array();
    	return $result;
    }

    public function update_status($id, $data){
    	$this->db->where('care_id', $id);
    	$query = $this->db->update('visit_care', $data);
    	if ($query) {
    		return true;
    	}else{
    		return false;
    	}
    }

    public function get_datatagihan($id){
    	$sql = "SELECT * FROM tagihan_perawatan WHERE id='$id' LIMIT 1";
    	$query = $this->db->query($sql);
    	$result = $query->row_array();
    	return $result;
    }

    public function hapus_tindakan($id){
    	$result = $this->db->delete('visit_care',array('care_id'=>$id));
        return $result;
    }

    public function hapus_tagihan($id){
    	$result = $this->db->delete('tagihan_perawatan', array('id'=>$id));
    	return $result;
    }

    public function get_master_dept(){
    	$sql = "SELECT * FROM master_dept WHERE jenis = 'POLIKLINIK'";
    	$query = $this->db->query($sql);
    	$result = $query->result_array();
    	return $result;
    }

    public function get_last_visit_care($visit_id)
    {
        $sql = "SELECT max(care_id) as value from visit_care WHERE visit_id = '$visit_id'";
        $query = $this->db->query($sql);
        if ($query) {
            return $query->row_array();
        }else{
            return false;
        }
    }

    public function save_tindakan($value='')
    {
        $query = $this->db->insert('visit_care',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

    public function save_tagihan($value='')
    {
        $query = $this->db->insert('tagihan_perawatan',$value);
        if ($query) {
            return true;
        }else{
            return false;
        }
    }

    public function get_inserted_tagihan($value){
    	$sql = "SELECT * FROM tagihan_perawatan t, master_dept d, master_tindakan mt WHERE t.dept_id = d.dept_id AND t.tindakan_id = mt.tindakan_id AND care_id = '$value'";
    	$query = $this->db->query($sql);
    	$result = $query->row_array();
    	return $result;
    }
}
?>