<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>laboratorium/homelab">Poliklinik Laboratorium</a>
	</li>
</div>

<input type="hidden" id="dept_id" value="<?php echo $dept_id; ?>">
<div class="navigation" style="margin-left: 10px" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	    <li class="active"><a href="#list" data-toggle="tab">List Pasien APS</a></li>
	    <li><a href="#permintaan" data-toggle="tab">List Pasien Rujukan</a></li>
	    <li><a href="#hasil" data-toggle="tab">Hasil Pemeriksaan Lab</a></li>
	    <li><a href="#tagihan" data-toggle="tab">Tagihan</a></li>
	   	<li><a href="#farmasi" data-toggle="tab">Farmasi</a></li>
	    <li><a href="#logistik" data-toggle="tab">Logistik</a></li>
	    <li><a href="#laporan" data-toggle="tab">Laporan</a></li>
	    <li><a href="#master" data-toggle="tab">Master</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="list">
	        <form class="form-horizontal" method="POST" id="search_aps">
	           	<div class="search">
					<label class="control-label col-md-3">Nama Pasien / Rekam Medis <span class="required" style="color : red">* </span>
					</label>
					<div class="col-md-4" style="width:420px;">		
						<input type="text" id="input_aps" class="form-control" placeholder="Masukkan Nama atau Nomor Rekam Medis Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>	
			</form>
			<hr class="garis">

			<div class="portlet box red">
				<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<input type="hidden" id="jmlaps" value="<?php echo count($listaps); ?>">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tableAPS">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal</th>
								<th>#Rekam Medis</th>
								<th> Nama Pasien</th>
								<th>Jenis Kelamin</th>
								<th>Jenis Pemeriksaan</th>
								<th width="80">Action</th>
							</tr>
						</thead>	
						<tbody>
							<?php
								$i = 0;

								foreach($listaps as $data){
									$tgl = strtotime($data['waktu']);
									$hasil = date('d F Y', $tgl); 
									echo '
										<tr>
											<td align="center">'.++$i.'</td>
											<td style="text-align:center">'.$hasil.'</td>
											<td>'.$data['rm_id'].'</td>
											<td>'.$data['nama'].'</td>										
											<td>'.$data['jenis_kelamin'].'</td>
											<td>'.$data['jenis_periksa'].'</td>
											<td style="text-align:center"><a href="#tambahPeri" onclick="pemeriksaan(&quot;'.$data['penunjang_id'].'&quot;)" class="tambahlab" data-toggle="modal" ><i class="glyphicon glyphicon-check"></i></a></td>										
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="tambahHasil" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-600px">
			<div class="modal-dialog">
        		<div class="modal-content" style="width:1200px">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Tambah Hasil Pemeriksaan</h3>
        			</div>	
        	    
				    <div class="dropdown">
			            <div id="titleInformasi">Identitas Pasien</div>
			            <div class="btnBawah" id="btnBawahhasil"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			        </div>
			        <form class="form-horizontal" role="form">
			        	<br>
				        <div class="informasi" id="infohasillab">
				        	<div class="form-group">
								<label class="control-label col-md-3">No. Rekam Medis :</label>
								<label type="text" class="control-label col-md-3"  name="noRm" id="phasil_rm" placeholder="No Rekam Medis" >00000000</label>
								<label class="control-label col-md-2">Jenis Kelamin :</label>
								<label type="text"  class="control-label col-md-2" name="jk" d="phasil_kelamin" placeholder="Jenis Kelamin" >Pria </label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Nama Pasien :</label>
								<label type="text"  class="control-label col-md-3" name="nama" id="phasil_nama" placeholder="Nama Pasien" >Putu </label>
								<label class="control-label col-md-2">Tanggal Lahir :</label>
								<label type="text"  class="control-label col-md-2" name="ttl" id="phasil_tlahir" placeholder="Tanggal Lahir" >30 Mei 1977</label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Alamat :</label>
								<label type="text" class="control-label col-md-3" name="alamat" id="phasil_alamat" placeholder="Alamat" >Bali</label>
								<label class="control-label col-md-2">Umur :</label>
								<label type="text"  class="control-label col-md-2" name="umur" id="phasil_umur" placeholder="Umur" >50 tahun</label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Pengirim :</label>
								<label type="text" class="control-label col-md-2" name="pengirim" id="phasil_pengirim" placeholder="Pengirim" >Jems</label>
								
							</div>
							<br>						
			            </div>
			        </form>

			        <div class="dropdown">
			            <div id="titleInformasi">Hasil Periksa</div>
			            <div class="btnBawah" id="btnBawahPeriksa"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			        </div>
			        <form class="form-horizontal" role="form" id="submit_periksa">
			        	<div class="informasi" id="infohasilperiksa" >
							<div class="form-group">
								<label class="control-label col-md-3">Penunjang</label>
								<div class="col-md-2">
									<input type="hidden" id="phasil_penunjang_id">
									<input type="text" class="form-control" id="phasil_penunjang" name="penunjang" placeholder="Penunjang" readonly>
								</div>
							</div>
				
							<div class="form-group">
								<label class="control-label col-md-3">Pemeriksa</label>
								<div class="col-md-3">	
									<input type="hidden" id="paramedis_id">
									<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" required>
								</div>
		        			</div>

							<div class="form-group">
								<label class="control-label col-md-3">Status</label>
								<div class="col-md-3">
									<select class="form-control select" name="pilihpemeriksa" id="phasil_status" required>
										<option value="" selected>Pilih Status</option>
										<option value="Selesai Sebagian">Selesai Sebagian</option>
										<option value="Selesai">Selesai</option>
									</select>
								</div>
							</div>
						</div>	
						<hr class="garis" style="margin-left:30px; margin-right:30px;">										
						<div class="tabelinformasi">	
							<div class="portlet-body" style="margin: 20px 30px 10px 10px">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="info">
											<th width="10px">No.</th>
											<th>Jenis Pemeriksaan</th>
											<th>Hasil</th>
											<th>Nilai Normal</th>
											<th>Keterangan</th>
										</tr>
									</thead>
									<tbody id="tbody_periksa">
										
									</tbody>
								</table>
							</div>
						</div>

						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:980px">
							<span style="background-color:white	; padding:0px 10px 0px 10px;">
								<button type="reset"  data-dismiss="modal" class="btn btn-danger">Batal</button> &nbsp;
								<button  type="submit" class="btn btn-success">Simpan</button> 
							</span>
						</div>
						<br>	
				    </form>
			   	</div>
			</div>
		</div>

		<div class="modal fade" id="view" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-600px">
			<div class="modal-dialog">
        		<div class="modal-content" style="width:1200px">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Lihat Hasil Pemeriksaan</h3>
        			</div>	
        	    
				    <div class="dropdown">
			            <div id="titleInformasi">Identitas Pasien</div>
			            <div class="btnBawah" id="btnBawahhasil"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			        </div>
			        <form class="form-horizontal" role="form">
			        	<br>
			        	  <div class="informasi" id="infohasillab">
				        	<div class="form-group">
								<label class="control-label col-md-3">No. Rekam Medis :</label>
								<label type="text" class="control-label col-md-3"  name="noRm" placeholder="No Rekam Medis" id="lhasil_rm"></label>
								<label class="control-label col-md-2">Jenis Kelamin :</label>
								<label type="text"  class="control-label col-md-2" name="jk" placeholder="Jenis Kelamin" id="lhasil_jenis" ></label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Nama Pasien :</label>
								<label type="text"  class="control-label col-md-3" name="nama" id="lhasil_nama" placeholder="Nama Pasien" > </label>
								<label class="control-label col-md-2">Tanggal Lahir :</label>
								<label type="text"  class="control-label col-md-2" name="ttl" id="lhasil_tlahir" placeholder="Tanggal Lahir" >30 Mei 1977</label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Alamat :</label>
								<label type="text" class="control-label col-md-3" name="alamat" id="lhasil_alamat" placeholder="Alamat" >Bali</label>
								<label class="control-label col-md-2">Umur :</label>
								<label type="text"  class="control-label col-md-2" name="umur" id="lhasil_umur" placeholder="Umur" >50 tahun</label>
								
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Pengirim :</label>
								<label type="text" class="control-label col-md-2" name="pengirim" id="lhasil_pengirim" placeholder="Pengirim" >Jems</label>
								
							</div>
							<br>						
			            </div>
				        
			        </form>

			        <div class="dropdown">
			            <div id="titleInformasi">Hasil Periksa</div>
			            <div class="btnBawah" id="btnBawahPeriksa"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
			        </div>
			        <form class="form-horizontal" role="form">
			        	<div class="informasi" id="infohasilperiksa" >
			        		
							<div class="form-group">
								<label class="control-label col-md-3">Penunjang</label>
								<div class="col-md-2">
									<input type="text" class="form-control" name="penunjang" id="lhasil_penunjang" placeholder="Penunjang" readonly>
									
								</div>
							</div>
				
							<div class="form-group">
								<label class="control-label col-md-3">Pemeriksa</label>
								<div class="col-md-3">
									<input type="text" class="form-control" readonly name="pilihpemeriksa" id="lhasil_pemeriksa">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Status</label>
								<div class="col-md-3">
									<input type="text" class="form-control" readonly name="pilihpemeriksa" id="lhasil_status">
								</div>
							</div>
						</div>	
						<hr class="garis" style="margin-left:30px; margin-right:30px;">										
						<div class="tabelinformasi">	
							<div class="portlet-body" style="margin: 20px 30px 10px 10px">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="info">
											<th width="10px">No.</th>
											<th>Jenis Pemeriksaan</th>
											<th>Hasil</th>
											<th>Nilai Normal</th>
											<th>Keterangan</th>
										</tr>
									</thead>
									<tbody id="tbody_lihathasil">
										
									</tbody>
								</table>
							</div>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span style="background-color:white; padding:0px 10px 0px 10px;">
								<button type="reset" class="btn btn-warning" data-dismiss="modal">Kembali</button>
								
								<button type="reset" class="btn btn-info" data-dismiss="modal">Cetak</button>
							</span>
						</div>
						<br><br>	
				    </form>
			   	</div>
			</div>
		</div>

		<div class="modal fade" id="tambahPeri" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:-600px">
        	<div class="modal-dialog">
        		<div class="modal-content" style="width:1200px">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Tambah Jenis Pemeriksaan</h3>
        			</div>	
        			<div class="modal-body">
        			<form class="form-horizontal" method="POST" id="submit_pemeriksaan">
        				<div class="form-group" style="padding-left:10px;">
        					<input type="hidden" id="penunjang_id">
							<label class="control-label col-md-2">Kelas Pemeriksaan</label>
							<div class="col-md-3">
								<select class="form-control select" name="pilihpemeriksa" id="kelas_periksa" required>
									<option value="" selected>Pilih Kelas</option>
									<option value="III">III</option>
									<option value="II">II</option>
									<option value="I">I</option>
									<option value="Utama">Utama</option>
									<option value="VIP">VIP</option>
								</select>
							</div>
						</div>

        				<div class="navigation" style="margin-left: 10px" >
						 	<ul id="tabs" class="nav tablab nav-tabs" data-tabs="tabs">
						 		<?php
						 			foreach ($jenisperiksa as $key => $data) {
						 				if($key==0)
						 					echo'
						 						<li class="active"><a href="#'.$data['kategori'].'" data-toggle="tab">'.$data['kategori'].'</a></li>			
						 					';	
						 				else
							 				echo'
							 					<li><a href="#'.$data['kategori'].'" data-toggle="tab">'.$data['kategori'].'</a></li>			
							 				';
						 			}
						 		?>
							</ul>
							
							<div id="my-tab-content" class="tab-content" >
							<?php
								foreach ($jenisperiksa as $data) {
									echo '
										
								        <div class="tab-pane" id="'.$data['kategori'].'" >
								        	<table class="table table-striped table-bordered table-hover">
										';
										foreach ($tindakan as $key => $value) {
											if(intval($value['nomor'])==0)
												echo '<tr>';

											if(intval($value['nomor'])%4==0 && intval($value['nomor'])!=0)
												echo '</tr><tr>';

											if($value['kategori']==$data['kategori']){
												echo'
												<td>
													<label class="checkbox-inline"><input type="checkbox" name="jenisperiksa" value="'.$value['nama_tindakan'].'">'.$value['nama_tindakan'].'</label>
												</td>';
											}
										}

									echo'
											</tr>
											</table>
									    </div>
									';
								}
							?>
							</div>
        				</div>

        			</div>	
      				<div class="modal-footer">
 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
 			       		<button type="submit" class="btn btn-success">Tambah</button>
			      	</div>

			      	</form>
        		</div>        	
        	</div>
        </div>

        <div class="tab-pane" id="hasil">
        	<form class="form-horizontal">
	           	<div class="search">
					<label class="control-label col-md-3">Nama Pasien / Rekam Medis <span class="required" style="color : red">* </span>
					</label>
					<div class="col-md-4" style="width:420px;">		
						<input type="text" class="form-control" placeholder="Masukkan Nama atau Nomor Rekam Medis Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>	
			</form>
			<hr class="garis">
			
			<div class="portlet-body" style="margin: 10px 10px 0px 10px">
				<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tableHasil">
					<thead>
						<tr class="info">
							<th width="20">No.</th>
							<th>Tanggal</th>
							<th>#Rekam Medis</th>
							<th>Nama</th>
							<th>Pengirim</th>
							<th>Unit Pengirim</th>
							<th>Status</th>
							<th width="100">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no = 0;
							foreach ($listperiksa as $data) {
								$tgl = strtotime($data['waktu']);
								$date = date('d F Y', $tgl);
								echo '
									<tr>
										<td align="center">'.++$no.'</td>
										<td align="center">'.$date.'</td>
										<td>'.$data['rm_id'].'</td>
										<td>'.$data['nama'].'</td>';

								if(is_null($data['nama_petugas'])){
									echo '<td>'.$data['pengirim'].'</td>';
								}else{
									echo '<td>'.$data['nama_petugas'].'</td>';
								}

								echo'<td>'.$data['nama_dept'].'</td>';
										
								if($data['status']=="PROSES") echo'<td>Belum</td>';
								else echo'<td>'.$data['status'].'</td>';

								echo'<td style="text-align:center">';
								if($data['status']=="Selesai"){
									echo '<a href="#view" data-toggle="modal" onclick="LihatHasil(&quot;'.$data['penunjang_id'].'&quot;)"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Lihat Detail"></i></a>';
								}else{
									echo '
										<a href="#tambahHasil" data-toggle="modal" onclick="Periksa(&quot;'.$data['penunjang_id'].'&quot;)"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Pemeriksaan"></i></a>
											<a href="#view" data-toggle="modal" onclick="LihatHasil(&quot;'.$data['penunjang_id'].'&quot;)"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Lihat Detail"></i></a>
									';
								}
								echo '</td>										
									</tr>
								';
							}
						?>
						
					</tbody>
				</table>
			</div>
		</div>

		<div class="tab-pane" id="permintaan"> 
			<form class="form-horizontal" id="search_rujuk">
				<div class="search">
					<label class="control-label col-md-3">Nama Pasien / Rekam Medis <span class="required" style="color : red">* </span>
					</label>
					<div class="col-md-4" style="width:420px;">		
						<input type="text" class="form-control" id="input_searchRujuk" placeholder="Masukkan Nama atau Nomor Rekam Medis Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>	
			</form>
			<hr class="garis">
            <div>
            	<div class="portlet-body" style="margin: 10px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tableRujuk">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Unit</th>
								<th>Tanggal </th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Dokter Pengirim</th>
								<th>Jenis Pemeriksaan</th>
								<th width="80">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 0;
								foreach ($listrujuk as $data) {
									$tgl = strtotime($data['waktu']);
									$hasil = date('d F Y', $tgl);
									echo '
										<tr>
											<td align="center">'.++$no.'</td>
											<td>'.$data['nama_dept'].'</td>
											<td style="text-align:center">'.$hasil.'</td>
											<td style="rext-align:right">'.$data['rm_id'].'</td>
											<td>'.$data['nama'].'</td>
											<td>'.$data['nama_petugas'].'</td>
											<td>'.$data['jenis_periksa'].'</td>
											<td style="text-align:center; cursor:pointer;">
												<a href="#tambahPeri" onclick="pemeriksaan(&quot;'.$data['penunjang_id'].'&quot;)" class="tambahlab" data-toggle="modal"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Tambah JenisPemeriksaan"></i></a>
											</td>
										</tr>
									';
								}
							?>
						</tbody>
					</table>												
				</div>
            </div>
		</div>
        
        <div class="tab-pane" id="farmasi">

        	<div class="dropdown" id="bwinvent" >
	            <div id="titleInformasi">Inventori</div>
	            <div id="btnBawahInventori" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="ibwinvent" class="tutupBiru">
				<form class="form-horizontal informasi" role="form">
	            	<div class="form-group">
		            	<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
						<div class="col-md-2" style="width:200px">
							<select class="form-control select" name="filterInv" id="filterInv">
								<option value="Jenis Obat" selected>Jenis Obat</option>
								<option value="Merek">Merek</option>
								<option value="Nama Obat">Nama Obat</option>							
							</select>	
						</div>
						<div class="col-md-2" style="margin-left:-15px; width:200px;" >
									<input type="text" class="form-control" id="filterby" name="valfilter" placeholder="Jenis Obat"/>
						</div>

							<div class="col-md-1" >
								<button class="btn btn-danger">EXPIRED</button> 
							</div>
							<div class="col-md-1" >
								<button class="btn btn-warning">EX 3 BULAN</button>
							</div>
							<div class="col-md-1" style="margin-left: 20px;">
								<button class="btn btn-warning">EX 6 BULAN</button>
							</div>
					</div>
				</form>
				<div class="form-group" >
					<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th> Nama Obat </th>
									<th> No Batch </th>
									<th> Harga Jual </th>
									<th> Merek </th>
									<th> Stok</th>
									<th> Satuan </th>
									<th width="200"> Tanggal Kadaluarsa </th>
									<th width="100"> Action </th>								
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align:center">1</td>
									<td>A</td>
									<td style="text-align:right">12</td>
									<td style="text-align:right">120120</td>
									<td>a</td>									
									<td style="text-align:right">2</td>
									<td style="text-align:right">12</td>
									<td style="text-align:center">12 Mei 1201</td>
									<td style="text-align:center"><a href="#inout" data-toggle="modal" class="edObat" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
											<a href="#edInvenBer" data-toggle="modal" class="edObat"><i class="glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
										</td>										
								</tr>
								<tr>
									<td style="text-align:center">2</td>
									<td>A</td>
									<td style="text-align:right">12</td>
									<td style="text-align:right">120120</td>
									<td>a</td>									
									<td style="text-align:right">2</td>
									<td style="text-align:right">12</td>
									<td style="text-align:center">12 Mei 1201</td>
									<td style="text-align:center"><a href="#inout" data-toggle="modal" class="edObat" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
											<a href="#edInvenBer" data-toggle="modal" class="edObat"><i class="glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
										</td>													
								</tr>
							</tbody>
						</table>
					</div>
					<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button> 
					
				</div>
			</div>
			<div class="modal fade" id="inout" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
	        			</div>
	        			<div class="modal-body">
	        			<form class="form-horizontal informasi" role="form">
	
		        			<div class="form-group">
		        					<label class="control-label col-md-3" >Tanggal 
									</label>
									<div class="col-md-4" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
										</div>
									</div>
									
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" >In / Out 
								</label>
								<div class="col-md-4">
					         		<select class="form-control select" name="iober" id="iober">
										<option value="IN" selected>IN</option>
										<option value="OUT">OUT</option>					
									</select>
								</div>	
							</div>

							<div class="form-group">
		        					<label class="control-label col-md-3" >Jumlah 
									</label>
									<div class="col-md-4" >
					         		<input type="text" class="form-control" name="jmlInOutBer" placeholder="Jumlah">
									</div>
									
							</div>
							<div class="form-group">
		        					<label class="control-label col-md-3" >Sisa Stok 
									</label>
									<div class="col-md-4" >
					         		<input type="text" class="form-control" name="sisaInOutBer" placeholder="Sisa Stok">
									</div>
									
							</div>
							<div class="form-group">
		        					<label class="control-label col-md-3" >Keterangan 
									</label>
									<div class="col-md-6" >
										<textarea class="form-control" placeholder="Keterangan"></textarea>
									</div>
		
							</div>
							</form>
							
	        			</div>
	        			<div class="modal-footer">
	        				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
	 			       		<button type="button" class="btn btn-success" data-dismiss="modal">Simpan</button>
				      	</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="edInvenBer" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
		        			</div>
		        			<div class="modal-body">
		        			<form class="form-horizontal" role="form">
				            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th  style="text-align:left" width="10%"> Waktu </th>
											<th  style="text-align:left"> IN / OUT </th>
											<th  style="text-align:left"> Jumlah </th>
											<th  style="text-align:left"> Stok Akhir </th>
											<th  style="text-align:left"> Jenis </th>
											<th  style="text-align:left">  Keterangan </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
											
									</tbody>
								</table>

			        			
								</form>
								
		        			</div>
		        			<div class="modal-footer">
		 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					      	</div>
						</div>
					</div>
			</div>

			<div class="dropdown" id="bwpermintaanfarmasi">
	            <div id="titleInformasi">Permintaan Farmasi</div>
	            <div id="btnBawahMintaObat" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="ibwpermintaanfarmasi" class="tutupBiru">
            	<form class="form-horizontal" role="form">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="noPermFarmBers" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control" data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
								</div>
							</div>
	        			</div>

	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="ketObatFarBers" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>

					<a href="#modalMintaFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left:10px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive">
								<thead>
									<tr class="info" >
										<th width="20"> No. </th>
										<th> Nama Obat </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Stok Unit </th>
										<th width="200"> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody id="addinputMintaFar">
									
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span style="padding:0px 10px 0px 10px;">
								<button class="btn btn-warning">RESET</button> &nbsp;
								<button class="btn btn-success">SIMPAN</button> 
							</span>
						</div>
						<br>
					</div>	
					<div class="modal fade" id="modalMintaFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
			        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
			        			</div>
			        			<div class="modal-body">

				        			<div class="form-group">
										<div class="form-group">	
											<div class="col-md-5" style="margin-left:20px;">
												<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama Obat"/>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-info">Cari</button>
											</div>
											<br><br>	
										</div>		
										<div style="margin-right:10px;margin-left:10px;"><hr></div>
										<div class="portlet-body" style="margin: 0px 20px 0px 15px">
											<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
												<thead>
													<tr class="info">
														<th>Nama Obat</th>
														<th>Satuan</th>
														<th>Merek</th>
														<th>Stok Gudang</th>
														<th>Tgl Kadaluarsa</th>
														<th width="10%">Pilih</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>100</td>
														<td>Paramex</td>
														<td style="text-align:center"><a href="#" class="addNewMintaFar"><i class="glyphicon glyphicon-check"></i></a></td>
													</tr>
													<tr>
														<td>Panadol</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>100</td>
														<td>Paramex</td>
														<td style="text-align:center"><a href="#" class="addNewMintaFar"><i class="glyphicon glyphicon-check"></i></a></td>
													</tr>
												</tbody>
											</table>												
										</div>
									</div>
			        			</div>
			        			<div class="modal-footer">
			 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						      	</div>
							</div>
						</div>
					</div>

				</form>
			</div>	    
			<br>   	

	       	<div class="dropdown" id="bwreturfarmasi">
	            <div id="titleInformasi">Retur Farmasi</div>
	            <div id="btnBawahRetFarmasi" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
           	<div id="ibwreturfarmasi" class="tutupBiru">
            	<form class="form-horizontal" role="form">
            		<div class="informasi">
            			<br>
            			<div class="form-group">
            				<div class="col-md-2">
            					<label class="control-label">Nomor Retur</label>
            				</div>
            				<div class="col-md-3">
            					<input type="text" class="form-control" name="noRetFarBers" id="noRetFarBers" placeholder="Nomor Retur"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
            					<label class="control-label">Tanggal Permintaan</label>
            				</div>
            				<div class="col-md-2">
            					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control" data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
								</div>
							</div>
            			</div>

            			<div class="form-group">
            				
							<div class="col-md-2">
            					<label class="control-label">Keterangan</label>
            				</div>
            				<div class="col-md-3">
								<textarea class="form-control" id="ketObatRetFarBers" name="ketObatRetFarBers"></textarea>	
							</div>
            			</div>
            		</div>

            		<a href="#modalRetFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left : 10px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>
					
					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabRetur">
								<thead>
									<tr class="info" >
										<th width="20"> No. </th>
										<th > Nama Obat </th>
										<th > Satuan </th>
										<th > Merek </th>
										<th > Stok Unit </th>
										<th width="200"> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								
								<tbody id="addinputRetFar">
										
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span style="padding:0px 10px 0px 10px;">
								<button class="btn btn-warning">RESET</button> &nbsp;
								<button class="btn btn-success">SIMPAN</button> 
							</span>
						</div>
						<br>
					</div>
					
					<div class="modal fade" id="modalRetFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
			        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
			        			</div>
			        			<div class="modal-body">

				        			<div class="form-group">
										<div class="form-group">	
											<div class="col-md-5" style="margin-left:20px;">
												<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama petugas"/>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-info">Cari</button>
											</div>
											<br><br>	
										</div>		
										<div style="margin-left:10px; margin-right:10px;"><hr></div>
										<div class="portlet-body" style="margin: 0px 20px 0px 15px">
											<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
												<thead>
													<tr class="info">
														<th>Nama Obat</th>
														<th>Satuan</th>
														<th>Merek</th>
														<th>Tgl Kadaluarsa</th>
														<th width="10%">Pilih</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td style="text-align:center"><a href="#" class ="addNewRetFar"><i class="glyphicon glyphicon-check"></i></a></td>
													</tr>
													<tr>
														<td>Panadol</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td>Paramex</td>
														<td style="text-align:center"><a href="#" class ="addNewRetFar"><i class="glyphicon glyphicon-check"></i></a></td>
													</tr>

												</tbody>
											</table>												
										</div>
									</div>
			        			</div>
			        			<div class="modal-footer">
			 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						      	</div>
							</div>
						</div>
					</div>
				</form>
			</div>	
			<br>
	    </div>

        <div class="tab-pane" id="logistik">
	       	<div class="dropdown" id="bwinlogistik">
	            <div id="titleInformasi">Inventori</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="ibwinlogistik" class="tutupBiru">
				<div class="form-group" >
					<div class="portlet-body" style="margin: 0px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th> Nama Barang </th>
									<th> Group </th>
									<th> Merek </th>
									<th> Harga </th>
									<th> Stok</th>
									<th> Satuan </th>
									<th width="120"> Action </th>								
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align:center">1</td>
									<td>A</td>
									<td>a</td>
									<td>a</td>									
									<td style="text-align:right">200</td>
									<td style="text-align:right">12</td>
									<td>A</td>
									<td style="text-align:center"><a href="#inoutbar" data-toggle="modal" class="edObat" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
											<a href="#edInvenBerBar" data-toggle="modal" class="edObat"><i class="glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
										</td>										
								</tr>
								<tr>
									<td style="text-align:center">2</td>
									<td>A</td>
									<td>a</td>
									<td>a</td>									
									<td style="text-align:right">200</td>
									<td style="text-align:right">12</td>
									<td>A</td>
									<td style="text-align:center"><a href="#inoutbar" data-toggle="modal" class="edObat" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
											<a href="#edInvenBerBar" data-toggle="modal" class="edObat"><i class="glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
										</td>											
								</tr>
							</tbody>
						</table>
					</div>
					<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button> 
					
	        	</div>
	        	<br>
	        </div>

			<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
	        			</div>
	        			<div class="modal-body">
		        			<form class="form-horizontal informasi" role="form">
    	
			        			<div class="form-group">
			        					<label class="control-label col-md-3" >Tanggal 
										</label>
										<div class="col-md-4" >
						         			<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input type="text" style="cursor:pointer;background-color:white" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
											</div>
						         		</div>
										
								</div>
								<div class="form-group">
										<label class="control-label col-md-3" >In / Out 
										</label>
										<div class="col-md-4">
						         		<select class="form-control select" name="ioberbar" id="ioberbar">
												<option value="IN" selected>IN</option>
												<option value="OUT">OUT</option>					
										</select>
										</div>

								</div>
								<div class="form-group">
			        					<label class="control-label col-md-3" >Jumlah 
										</label>
										<div class="col-md-4" >
						         		<input type="text" class="form-control" name="jmlInOutBerBar" placeholder="Jumlah">
										</div>
										
								</div>
								<div class="form-group">
			        					<label class="control-label col-md-3" >Sisa Stok 
										</label>
										<div class="col-md-4" >
						         		<input type="text" class="form-control" name="sisaInOutBerBar" placeholder="Sisa Stok">
										</div>
										
								</div>
								<div class="form-group">
			        					<label class="control-label col-md-3" >Keterangan 
										</label>
										<div class="col-md-6" >
											<textarea class="form-control" placeholder="Keterangan"></textarea>
										</div>
			
								</div>
							</form>
								
		        		</div>
	        			<div class="modal-footer">
	        				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
				      		<button type="button" class="btn btn-success" data-dismiss="modal">Simpan</button>
				      	</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
	        			<form class="form-horizontal" role="form">
			            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
								<thead>
									<tr class="info" >
										<th  style="text-align:left" width="10%"> Waktu </th>
										<th  style="text-align:left"> IN / OUT </th>
										<th  style="text-align:left"> Jumlah </th>
										<th  style="text-align:left"> Stok Akhir </th>
										<th  style="text-align:left"> Jenis </th>
										<th  style="text-align:left">  Keterangan </th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
										
								</tbody>
							</table>

		        			
							</form>
							
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
			
			<div class="dropdown" id="bwpermintaanlogistik" style="margin-left:10px;width:98.5%">
	            <div id="titleInformasi">Permintaan Logistik</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="ibwpermintaanlogistik" class="tutupBiru">
            	<form class="form-horizontal" role="form">
            		<br>
            		<div class="informasi">
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermLogBers" id="noPermFarmBers" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control" data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
								</div>
							</div>
	        			</div>

	        			<div class="form-group">
							<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<textarea class="form-control" id="ketObatLogBers" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
        			</div>
        			
					<a href="#modalMintaLogBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left :10px; font-size:11pt;">&nbsp;Tambah Barang</i></a>
					<div class="clearfix"></div>
					
					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 10px 10px">
						
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<th width="20"> No. </th>
										<th> Nama Barang </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Stok Unit </th>
										<th width="200"> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								
								<tbody id="addinputMintaLog">
										<!-- <tr>
											<td colspan="6" style="text-align:center" id="dataKosong">DATA KOSONG</td>												
										</tr> -->
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span style="padding:0px 10px 0px 10px;">
								<button class="btn btn-warning">RESET</button> &nbsp;
								<button class="btn btn-success">SIMPAN</button> 
							</span>
						</div>
						<br>
					</div>
				</form>
				
				<div class="modal fade" id="modalMintaLogBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
		        			</div>
		        			<div class="modal-body">

			        			<div class="form-group">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:10px;">
											<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama petugas"/>
										</div>
										<div class="col-md-2">
											<button type="button" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
									<div style="margin-left:10px; margin-right:10px;"><hr></div>
									<div class="portlet-body" style="margin: 0px 20px 0px 15px">
										<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
											<thead>
												<tr class="info">
													<th>Nama Barang</th>
													<th>Satuan</th>
													<th>Merek</th>
													<th>Tgl Kadaluarsa</th>
													<th width="10%">Pilih</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Paramex</td>
													<td>Paramex</td>
													<td>Paramex</td>
													<td>Paramex</td>
													<td style="text-align:center"><a href="#" class="addNewLog"><i class="glyphicon glyphicon-check"></i></a></td>
												</tr>
												<tr>
													<td>Panadol</td>
													<td>Paramex</td>
													<td>Paramex</td>
													<td>Paramex</td>
													<td style="text-align:center"><a href="#" class="addNewLog"><i class="glyphicon glyphicon-check"></i></a></td>
												</tr>

											</tbody>
										</table>												
									</div>
								</div>
		        			</div>
		        			<div class="modal-footer">
		 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					      	</div>
						</div>
					</div>
				</div>
				<br>
			</div>	
			<br>    
	    </div>

        <div class="tab-pane" id="laporan" style="margin-left:40px">   
        	<div id="lab">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Laboratorium</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;padding:0px 30px 0px 30px;" role="form">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>  
	        <br>
			<div id="radiolog">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Radiologi</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;padding:0px 30px 0px 30px;" role="form">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>
	        <br>
	        <div id="fisio">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Laporan Fisioterapi</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;padding:0px 30px 0px 30px;" role="form">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white;" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>  
	        <br>      
        </div>
        
        <div class="tab-pane" id="master"> 
        	<div class="dropdown" id="tambahpen" style="margin-left:10px;width:98.5%" id="btnBawahRiwTerimaObat">
	            <div id="titleInformasi">Tambah Tarif Penunjang</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
            <br>

            <div id="itambahpen" class="tutupBiru">
	            <form class="form-horizontal" role="form">
		        	<div class="informasi">
		            	<div class="form-group">
		            		<label class="control-label col-md-2">Nama Tarif</label>
		            		<div class="col-md-3">
		            			<input type="text" class="form-control" id="namaTarifPjg" name="namaTarifPjg" placeholder="Nama Tarif">
		            		</div>
		            	</div>
		            	
		            	<div class="form-group">
		            		<label class="control-label col-md-2">Kategori</label>
		            		<div class="col-md-3">
								<select class="form-control select" name="kategoriLab" id="kategoriLab">
									<option selected>Pilih</option>
									<option value="Hematologi">Hematologi</option>
									<option value="Kimia Klinik">Kimia Klinik</option>
									<option value="Urine">Urine</option>
									<option value="Feses">Feses</option>					
									<option value="Imuno-Serologi">Imuno-Serologi</option>					
									<option value="Mikrobiologi">Mikrobiologi</option>
									<option value="Lain-lain">Lain-lain</option>
								</select>	
							</div>
		            	</div>

		            	<div class="form-group">
		            		<label class="control-label col-md-2">Group</label>
		            		<div class="col-md-3">
		            			<input type="text" class="form-control" placeholder="ICD-9CM" data-toggle="modal" data-target="#searchICDIX" id="group">
							</div>
		            	</div>

		            	<div class="form-group">
		            		<label class="control-label col-md-2">Nilai Normal</label>
		            		<div class="col-md-2">
		            			<textarea type="text" class="form-control" placeholder="Nilai Normal" id="nilaiNormal"></textarea>
							</div>
		            	</div>
		            </div>
					<br>
	            	<div class="form-group">
	            		<div class="portlet-body" style="margin: 0px 25px 0px 25px">
						
							<table class="table table-striped table-bordered table-hover table-responsive">
								<thead>
									<tr class="info" >
										<th  style="text-align:center"> Jenis Tarif </th>
										<th  style="text-align:center"> BAKHP </th>
										<th  style="text-align:center"> JS </th>
										<th  style="text-align:center"> JP </th>
										<th  style="text-align:center"> Total </th>
										<th  style="text-align:center"> Clear </th>
									</tr>
								</thead>
								<tbody>
									
									
									<tr>
										<td>Kelas VIP</td>
										<td><input type="text" class="form-control cekrow1" id="BAKHPPjgLabVIP" name="BAKHPPjgLabVIP" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow1" id="JSPjgLabVIP" name="JSPjgLabVIP" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow1" id="JPPjgLabVIP" name="JPPjgLabVIP" placeholder="0"></td>									
										<td><input type="text" class="form-control cekrow1" id="TotalPjgLabVIP" name="TotalPjgLabVIP" placeholder="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow1"><i class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
									</tr>
									
									<tr>
										<td>Kelas Utama</td>
										<td><input type="text" class="form-control cekrow2" id="BAKHPPjgLabUtama" name="BAKHPPjgLabUtama" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow2" id="JSPjgLabUtama" name="JSPjgLabUtama" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow2" id="JPPjgLabUtama" name="JPPjgLabUtama" placeholder="0"></td>									
										<td><input type="text" class="form-control cekrow2" id="TotalPjgLabUtama" name="TotalPjgLabUtama" placeholder="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow2"><i class="glyphicon glyphicon-repeat" data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
									</tr>

									<tr>
										<td> Kelas I</td>
										<td><input type="text" class="form-control cekrow3"  id="BAKHPPjgLabKlsI" name="BAKHPPjgLabKlsI" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow3" id="JSPjgLabKlsI" name="JSPjgLabKlsI" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow3" id="JPPjgLabKlsI" name="JPPjgLabKlsI" placeholder="0"></td>									
										<td><input type="text" class="form-control cekrow3" id="TotalPjgLabKlsI" name="TotalPjgLabKlsI" placeholder="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow3"><i class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
									</tr>
									<tr>
										<td>Kelas II</td>
										<td><input type="text" class="form-control cekrow4" id="BAKHPPjgLabKlsII" name="BAKHPPjgLabKlsII" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow4" id="JSPjgLabKlsII" name="JSPjgLabKlsII" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow4" id="JPPjgLabKlsII" name="JPPjgLabKlsII" placeholder="0"></td>									
										<td><input type="text" class="form-control cekrow4" id="TotalPjgLabKlsII" name="TotalPjgLabKlsII" placeholder="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow4"><i class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
									</tr>
									<tr>
										<td>Kelas III</td>
										<td><input type="text" class="form-control cekrow5" id="BAKHPPjgLabKlsIII" name="BAKHPPjgLabKlsIII" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow5" id="JSPjgLabKlsIII" name="JSPjgLabKlsIII" placeholder="0"></td>
										<td><input type="text" class="form-control cekrow5" id="JPPjgLabKlsIII" name="JPPjgLabKlsIII" placeholder="0"></td>									
										<td><input type="text" class="form-control cekrow5" id="TotalPjgLabKlsIII" name="TotalPjgLabKlsIII" placeholder="0" readonly></td>
										<td style="text-align:center"><a href="#" class="clearRow5"><i class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="reset row"></i></a></td>						
										
									</tr>
								</tbody>
							</table>
						</div>
	            	</div>
	            	
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button class="btn btn-success" id="trigger">SIMPAN</button> 
						</span>
					</div>
					<br><br>

	            </form>
	        </div>

	        <div class="modal fade" id="searchICDIX" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Search ICD-9CM</h3>
				   			</div>
							<div class="modal-body">
								<div class="form-group">
									<div class="form-group">	
										<div class="col-md-3" style="margin-left:35px;">
											<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama icd"/>
										</div>
										<div class="col-md-2">
											<button type="button" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
									<div style="margin-left:20px; margin-right:20px;"><hr></div>
									<div class="portlet-body" style="margin: 0px 10px 0px 10px">
										<table class="table table-striped table-bordered table-hover tabelinformasi" style="width:90%;">
											<thead>
												<tr class="info">
													<td>Nama ICD-9CM</td>
													<td width="10%">Pilih</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Jems</td>
													<td style="text-align:center"><a href="#" class ="addNewObatNon"><i class="glyphicon glyphicon-check"></i></a></td>
												</tr>

											</tbody>
										</table>												
									</div>
								</div>
		       				</div>
			        		<br>
			        		<div class="modal-footer">
			        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			 			     	
						    </div>
						</div>
					</div>
				</form>
			</div>

			<div class="dropdown" id="jaspel">
	            <div id="titleInformasi">Jasa Pelayanan</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="ijaspel" class="tutupBiru">
	            <div class="form-horizontal">
		            <div class="informasi">
			            <div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Periode </label>
							<div class="col-md-3" style="margin-left:-15px">
								<div class="input-daterange input-group" id="datepicker">
								    <input type="text" style="cursor:pointer;background-color:white;" class="form-control" name="start" data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly placeholder="<?php echo date("d/m/Y");?>" />
								    <span class="input-group-addon">to</span>
								    <input type="text" style="cursor:pointer;background-color:white;" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" placeholder="<?php echo date("d/m/Y");?>" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2"> <i class="glyphicon glyphicon-filter"></i>&nbsp;Cara Bayar</label>
							<div class="input-group col-md-2">
								<select class="form-control select" name="carabayar" id="carabayar">
									<option value="" selected>Pilih</option>
									<option value="BPJS">BPJS</option>	
									<option value="Ansuransi">Ansuransi</option>		
									<option value="Gratis">Gratis</option>	
									<option value="Tunjangan">Tunjangan</option>					
								</select>
							</div>	
						</div>

						<div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Paramedis</label>
							<div class="input-group col-md-2">
								<input type="text" class="form-control" style="cursor:pointer;background-color:white;" readonly id="nmpegawai" placeholder="Search Pegawai" data-toggle="modal" data-target="#searchPegawai">
							</div>

						</div>
					</div>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button class="btn btn-warning">FILTER</button> 
						</span>
					</div>
					<br>

				</div>

				<div class="modal fade" id="searchPegawai" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
			    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			    				<h3 class="modal-title" id="myModalLabel">Pilih Pegawai</h3>
			    			</div>
			    			<div class="modal-body">
								<div class="form-group">	
									<div class="col-md-5">
										<input type="text" class="form-control" name="kwpegawai" id="kwpegawai" placeholder="Nama Pegawai"/>
									</div>
									<div class="col-md-2">
										<button type="button" class="btn btn-info">Cari</button>
									</div>	
									<div class="col-md-2">
										<button type="button" class="btn btn-success">Tampilkan Semua Pegawai</button>
									</div>	
								</div>	
								<br>	
								<div style="margin-left:5px; margin-right:5px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 10px 0px 10px">
									<table class="table table-striped table-bordered table-hover" id="tabelpegawai">
										<thead>
											<tr class="info">
												<th>Nama Pegawai</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Jems</td>
												<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
											</tr>
											<tr>
												<td>Putu</td>
												<td style="text-align:center; cursor:pointer;"><a href="#"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>
											</tr>
										</tbody>
									</table>												
								</div>
			    			</div>
			    			<div class="modal-footer">
						       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					      	</div>
						</div>
					</div>
				</div>

		    	<div class="portlet-body" style="margin: 10px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover tableDTUtama" id="tabelJpPoliinap">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal</th>
								<th>Tindakan</th>
								<th>Cara Bayar</th>
								<th>Paramedis</th>
								<th>Jumlah Tindakan</th>
								<th>Jasa Pelayanan</th>
								<th width="100">Fee Pelayanan</th>
								<th>Total Fee Pelayanan</th>
								<th width="80">Action</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<tr>
								<td style="text-align:center">1</td>
								<td style="text-align:center">12 Mei 1201</td>
								<td>Tindakan</td>
								<td>Cara Bayar</td>
								<td>Paramedis</td>
								<td style="text-align:right">1000</td>
								<td style="text-align:right">213</td>
								<td><input type="text" class="input-sm form-control jp" value="200" style="text-align:right" readonly></td>
								<td style="text-align:right">123</td>
								<td style="text-align:center">
									<a href="#" id="btneditjp"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
									<a href="#" id="btnsavejp"><i class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Simpan"></i></a>
									
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" style="font-size:2em;">1.000.000</label>
					</div>
					<div class="col-md-4 pull-right" style="font-size:1.5em; margin-top:5px; text-align:right;">
						Total Jasa Pelayanan (Rp.) : 
					</div>
				</div>
				<br><br>
			</div>
	    </div>
   		
   		<div class="tab-pane" id="tagihan" style="padding-bottom:50px;"> 
        	<form method="POST" class="form-horizontal" id="submitTagihanSearch">
		       	<div class="search">
					<label class="control-label col-md-3" style="margin-top:5px;">
						<i class="fa fa-search">&nbsp;&nbsp;</i>Nama Pasien / Rekam Medis <span class="required" style="color : red">* </span>
					</label>
					<div class="col-md-4" style="width:420px;">		
						<input type="text" id="search_tagihan" class="form-control" placeholder="Masukkan Nama atau Nomor Rekam Medis Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        <a onclick="setStatus('26');" data-toggle="modal" class="btn btn-warning"> Tambah Invoice Baru</a>
				</div>	
			</form>
			
			<hr class="garis">

			<div class="portlet box red">
				<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_tagihan">
						<thead>
							<tr class="info">
								<th style="text-align:center;width:20px;">No.</th>
								<th>Unit</th>
								<th>Nomor Invoice</th>
								<th>Nomor Visit</th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Cara Bayar</th>
								<th style="text-align:center;width:25px;">Action</th>
							</tr>
						</thead>
						<tbody>

							<!-- <tr>
								<td>1</td>
								<td>Bersalin</td>
								<td>1212121</td>
								<td>32323</td>	
								<td>123123</td>									
								<td>Selena</td>
								<td>Rumahnya</td>
								<td>Ansuransi</td>
								<td style="text-align:center">
									<a href="<?php echo base_url() ?>laboratorium/invoicenonbpjs" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>
								</td>										
							</tr>
							<tr>
								<td>2</td>
								<td>Bersalin</td>
								<td>1212121</td>
								<td>32323</td>	
								<td>123123</td>									
								<td>Jems</td>
								<td>Rumahnya</td>
								<td>BPJS</td>
								<td style="text-align:center">
									<a href="<?php echo base_url() ?>laboratorium/invoicebpjs" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>
								</td>										
							</tr> -->
						</tbody>
					</table>
				</div>			
			</div> 

        </div>

    </div>

</div>
				
<script type="text/javascript">
	$(document).ready( function(){
		$('.addNewMintaFar').on('click',function(){
			tambahPermintaanFarmasi('#addinputMintaFar','.addNewMintaFar');
		});

		$('.addNewRetFar').on('click',function(){
			tambahReturFarmasi('#addinputRetFar','.addNewRetFar');
		});

		$('.addNewLog').on('click',function(){
			tambahPermintaanLogistik('#addinputMintaLog','.addNewLog');
		});

		$("#tambahpen").click(function(){
			$("#itambahpen").slideToggle();
		});

		$("#jaspel").click(function(){
			$("#ijaspel").slideToggle();
		});

		$("#bwinvent").click(function(){
			$("#ibwinvent").slideToggle();
		});

		$("#bwpermintaanfarmasi").click(function(){
			$("#ibwpermintaanfarmasi").slideToggle();
		});

		$("#bwreturfarmasi").click(function(){
			$("#ibwreturfarmasi").slideToggle();
		});

		$("#bwinlogistik").click(function(){
			$("#ibwinlogistik").slideToggle();
		});

		$("#bwpermintaanlogistik").click(function(){
			$("#ibwpermintaanlogistik").slideToggle();
		});


		$(".clearRow1").click(function(e){
			e.preventDefault();
			$(".cekrow1").val('');
		});

		$(".clearRow2").click(function(e){
			e.preventDefault();
			$(".cekrow2").val('');
		});
		$(".clearRow3").click(function(e){
			e.preventDefault();
			$(".cekrow3").val('');
		});
		$(".clearRow4").click(function(e){
			e.preventDefault();
			$(".cekrow4").val('');
		});
		$(".clearRow5").click(function(e){
			e.preventDefault();
			$(".cekrow5").val('');
		});

		$('.tambahlab').click(function(){
			$('.tablab a[href="#Hematologi"]').tab('show');
			$('#Hematologi').addClass('active');
		});

		//-------------- search APS -----------------------------//
		$('#search_aps').submit(function(event){
			event.preventDefault();
			var item = {};
			item['search'] = $('#input_aps').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>laboratorium/homelab/search_listaps',
				success:function(data){
					var t = $('#tableAPS').DataTable();
					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						var action = '<td style="text-align:center"><a href="#tambahPeri" class="tambahlab" data-toggle="modal" ><i class="glyphicon glyphicon-check"></i></a></td>';

						t.row.add([
							(i+1),
							changeDateTime(data[i]['waktu']),
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['jenis_kelamin'],
							data[i]['jenis_periksa'],
							action,
							i
						]).draw();
					}
				},error:function(data){
					
				}
			});

		});


		$('#search_rujuk').submit(function(event){
			event.preventDefault();
			var item = {};
			item['search'] = $('#input_searchRujuk').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>laboratorium/homelab/search_listrujuk',
				success:function(data){
					var t = $('#tableRujuk').DataTable();
					t.clear().draw();
					console.log(data);
					console.log('data');

					for(var i = 0; i<data.length; i++){
						var action = '<td style="text-align:center"><a href="#tambahPeri" class="tambahlab" data-toggle="modal" ><i class="glyphicon glyphicon-check"></i></a></td>';

						t.row.add([
							(i+1),
							data[i]['nama_dept'],
							changeDateTime(data[i]['waktu']),
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['nama_petugas'],
							data[i]['jenis_periksa'],
							action,
							i
						]).draw();
					}
				},error:function(data){
					
				}
			});

		});

		//------------ submit jenis pemeriksaan -----------------//
		var pasien;
		var selected;
		$('#tableRujuk').on('click','tr', function (e) {
			pasien = 'rujuk';
			selected = $(this);
			e.preventDefault();
		});

		$('#tableAPS').on('click','tr', function (e) {
			pasien = 'aps';
			selected = $(this);
			e.preventDefault();
		});

		$('#submit_pemeriksaan').submit(function(event){
			event.preventDefault();
			var item = [];

			$("input:checkbox[name='jenisperiksa']:checked").each(function(){
			    item.push($(this).val());
			});

			for(var i = 0; i < item.length; i++){
				var value = {};
				value['nama_tindakan'] = item[i];
				value['penunjang_id'] = $('#penunjang_id').val();
				value['kelas_pelayanan'] = $('#kelas_periksa').val();

				$.ajax({
					type:'POST',
					data:value,
					url:'<?php echo base_url() ?>laboratorium/homelab/submit_pemeriksaan',
					success:function(data){
						console.log(data);
					}, error:function(data){
						console.log(data);
					}
				});
			}

		});

		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			var autodata = [];
			var iddata = [];

			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>laboratorium/homelab/get_paramedis',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_petugas']);
							iddata.push(data[i]['petugas_id']);
						}
						console.log(autodata);
					}
				});
			}

			$input.typeahead({source:autodata, autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = autodata.indexOf(current);

			    $('#paramedis_id').val(iddata[index]);
			    
			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});
		});

		$('#submit_periksa').submit(function(event){
			event.preventDefault();

			$('#tbody_periksa tr').each(function(){
				var item = {};
				var hasil = $(this).children('td').children('.hasil').val();
				var nilai = $(this).children('td').children('.nilai').val();
				var keterangan = $(this).children('td').children('.keterangan').val();
				var id = $(this).children('td').children('.penunjang_detail_id').val();

				item['hasil'] = hasil;
				item['nilai_normal'] = nilai;
				item['keterangan'] = keterangan;
				item['penunjang_detail_id'] = id;
				item['status'] = $('#phasil_status').val();
				item['pemeriksa'] = $('#paramedis_id').val();
				item['penunjang_id'] = $('#phasil_penunjang_id').val();

				$.ajax({
					type:"POST",
					data:item,
					url:'<?php echo base_url()?>laboratorium/homelab/update_hasilperiksa',
					success:function(data){
						
					},error:function(data){
						console.log(data);
						alert('gagal');
					}
				});
			}).promise().done(get_listrujuk());

		});	

		$('#submitTagihanSearch').submit(function(event){
			event.preventDefault();

			var item = {};
			item['search'] = $('#search_tagihan').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo  base_url(); ?>laboratorium/homelab/search_tagihan',
				success:function(data){
					console.log(data);
					var t = $('#table_tagihan').DataTable();
					var no = 0;
					var action;
					t.clear().draw();

					for(var i=0; i<data.length; i++){
						no++;
						if(data[i]['carapembayaran'] == "BPJS"){
							action = '<a href="<?php echo base_url() ?>laboratorium/invoicebpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}else{
							action = '<a href="<?php echo base_url() ?>laboratorium/invoicenonbpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}

						t.row.add([
							no,
							data[i]['nama_dept'],
							data[i]['no_invoice'],
							data[i]['visit_id'],
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['alamat_skr'],
							data[i]['carapembayaran'],
							action,
							i
						]).draw();
					}

				}
			});
		});
	});

	function get_listrujuk(){
		$.ajax({
			type:'POST',
			url:'<?php echo base_url() ?>laboratorium/homelab/get_listperiksa',
			success:function(data){
				var t = $('#tableHasil').DataTable();

				t.clear().draw();

				for(var i = 0; i<data.length; i++){
					var pengirim, status;
					var action = '<a href="#tambahHasil" data-toggle="modal" onclick="Periksa(&quot;'+data[i]['penunjang_id']+'&quot;)"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah Pemeriksaan"></i></a>'+
									'<a href="#view" data-toggle="modal" ><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Lihat Detail"></i></a>';

					if(!data[i]['nama_petugas'])
						pengirim = data[i]['pengirim'];
					else
						pengirim = data[i]['nama_petugas'];

					if(data[i]['status'] == "PROSES")
						status = "Belum";
					else
						status = data[i]['status'];

					t.row.add([
						(i+1),
						changeDateTime(data[i]['waktu']),
						data[i]['rm_id'],
						data[i]['nama'],
						pengirim,
						data[i]['nama_dept'],
						status,
						action,
						i
					]).draw();
				}

				$('#tambahHasil').modal('hide');

				console.log(data);
			}
		});
	}

	function changeDateTime(tgl_lahir){
		var getdate = tgl_lahir.split(" ");
		var remove = getdate[0].split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0];

		return tgl;
	}

	function changeDate(tgl_lahir){
		var remove = tgl_lahir.split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0];

		return tgl;
	}

	function pemeriksaan(penunjang){
		$('#penunjang_id').val(penunjang);
	}

	function Periksa(penunjang){
		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>laboratorium/homelab/get_detailperiksa/'+penunjang,
			success:function(data){
				$('#phasil_rm').text(data['rm_id']);
				$('#phasil_penunjang_id').val(data['penunjang_id']);
				$('#phasil_nama').text(data['nama'])
				$('#phasil_alamat').text(data['alamat_skr']);
				$('#phasil_kelamin').text(data['jenis_kelamin']);
				$('#phasil_tlahir').text(changeDate(data['tanggal_lahir']));

				if(!data['nama_petugas']){
					$('#phasil_pengirim').text(data['pengirim']);
				}else{
					$('#phasil_pengirim').text(data['nama_petugas']);
				}

				var text = data['tanggal_lahir'];
				var from = text.split("-");
				var born = new Date(from[0], from[1] - 1, from[2]);

				$('#phasil_umur').text(getAge(born));

				$('#phasil_penunjang').val(data['nama_dept']);
			}
		});

		$.ajax({
			type:'POST',
			url:'<?php echo base_url() ?>laboratorium/homelab/get_allperiksa/'+penunjang,
			success:function(data){
				$('#tbody_periksa').empty();

				for(var i=0; i<data.length; i++){
					if(!data[i]['pemeriksa']){
						$('#paramedis_id').val(data[i]['pemeriksa']);
						$('#paramedis').val(data[i]['nama_petugas']);
					}

					$('#tbody_periksa').append(
						'<tr>'+
							'<td style="text-align:center">'+(i+1)+'</td>'+
							'<td>'+data[i]['nama_tindakan']+'</td>'+
							'<td>'+
								'<input type="hidden" class="penunjang_detail_id" value="'+data[i]['penunjang_detail_id']+'">'+
								'<input type="text" class="form-control hasil" name="inputPeriksa" value="'+data[i]['hasilp']+'" placeholder="Hasil">'+
							'</td>'+
							'<td>'+
								'<input type="text" class="form-control nilai" name="inputNormal" value="'+data[i]['nilaip']+'" placeholder="Nilai Normal">'+
							'</td>'+
							'<td>'+
								'<input type="text" class="form-control keterangan" name="inputKeterangan" value="'+data[i]['keteranganp']+'" placeholder="Keterangan">'+
							'</td>'+
						'</tr>'
					);
				}
			}
		});
	}

	function LihatHasil(penunjang){

		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>laboratorium/homelab/get_detailperiksa/'+penunjang,
			success:function(data){
				console.log(data);
				$('#lhasil_rm').text(data['rm_id']);
				$('#lhasil_nama').text(data['nama'])
				$('#lhasil_alamat').text(data['alamat_skr']);
				$('#lhasil_jenis').text(data['jenis_kelamin']);
				$('#lhasil_tlahir').text(changeDate(data['tanggal_lahir']));

				if(!data['nama_petugas']){
					$('#lhasil_pengirim').text(data['pengirim']);
				}else{
					$('#lhasil_pengirim').text(data['nama_petugas']);
				}

				var text = data['tanggal_lahir'];
				var from = text.split("-");
				var born = new Date(from[0], from[1] - 1, from[2]);

				$('#lhasil_umur').text(getAge(born));

				$('#lhasil_penunjang').val(data['nama_dept']);
			}
		});

		$.ajax({
			type:'POST',
			url:'<?php echo base_url() ?>laboratorium/homelab/get_allperiksa/'+penunjang,
			success:function(data){
				$('#tbody_lihathasil').empty();

				for(var i=0; i<data.length; i++){
					$('#lhasil_pemeriksa').val(data[i]['nama_petugas']);
					$('#lhasil_status').val(data[i]['status_p']);

					$('#tbody_lihathasil').append(
						'<tr>'+
							'<td style="text-align:center">'+(i+1)+'</td>'+
							'<td>'+data[i]['nama_tindakan']+'</td>'+
							'<td style="text-align:right">'+data[i]['hasilp']+'</td>'+
							'<td style="text-align:right">'+data[i]['nilaip']+'</td>'+
							'<td>'+data[i]['keteranganp']+'</td>'+
						'</tr>'
					);
				}
			}
		})
	}

	function getAge(date) {
		  var now = new Date();
		  var today = new Date(now.getYear(),now.getMonth(),now.getDate());

		  var yearNow = now.getYear();
		  var monthNow = now.getMonth();
		  var dateNow = now.getDate();

		  var dob = date;

		  var yearDob = dob.getYear();
		  var monthDob = dob.getMonth();
		  var dateDob = dob.getDate();
		  var age = {};
		  var ageString = "";
		  var yearString = "";
		  var monthString = "";
		  var dayString = "";


		  yearAge = yearNow - yearDob;

		  if (monthNow >= monthDob)
		    var monthAge = monthNow - monthDob;
		  else {
		    yearAge--;
		    var monthAge = 12 + monthNow -monthDob;
		  }

		  if (dateNow >= dateDob)
		    var dateAge = dateNow - dateDob;
		  else {
		    monthAge--;
		    var dateAge = 31 + dateNow - dateDob;

		    if (monthAge < 0) {
		      monthAge = 11;
		      yearAge--;
		    }
		  }

		  age = {
		      years: yearAge,
		      months: monthAge,
		      days: dateAge
		      };

			  if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
			    ageString = age.years +" Tahun  ";
			  else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
			    ageString =  age.days + " Hari.";
			  else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
			    ageString = age.years + " Tahun.";
			  else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
			    ageString = age.years+" Tahun ";
			  else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
			    ageString = age.months + " Bulan ";
			  else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
			    ageString = age.years + " Tahun ";
			  else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
			    ageString = age.months + " Bulan.";
			  else ageString = "Belum lahir";

			  return ageString;
	}

	function setStatus(departmen){
		var u = "<?php echo base_url() ?>laboratorium/";
		localStorage.setItem('department', departmen);
		localStorage.setItem('url', u);
		window.location.href="<?php echo base_url() ?>invoice/tambahinvoice";
	}
	
</script>							