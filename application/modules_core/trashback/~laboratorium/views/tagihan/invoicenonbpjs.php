<div class="title">
	LABORATORIUM - INVOICE PASIEN NON BPJS
</div>
<div class="bar">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>laboratorium/homelab">Laboratorium</a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>laboratorium/invoicenonbpjs">Invoice - Nama Pasien</a>
	</li>
</div>

<input type="hidden" id="visit_id" value="<?php echo $visit_id; ?>"/>
<input type="hidden" id="sub_visit" value="<?php echo $sub_visit; ?>"/>
<input type="hidden" id="no_invoice" value="<?php echo $no_invoice; ?>"/>
<input type="hidden" id="dept_id" value="<?php echo $dept_id; ?>"/>
<div class="backregis">
	<div id="my-tab-content" class="tab-content">
			
		<div class="informasi">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="font-size:16px;">
						<label class="control-label1 col-md-4 nama">Nomor Invoice</label>
						<div class="col-md-4 nama">: <?php echo $no_invoice; ?> </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Visit ID</label>
						<div class="col-md-5">:	<?php echo $visit_id ?> </div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Kelas Perawatan</label>
						<div class="col-md-5">: Kelas <?php echo $invoice['kelas_perawatan'] ?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Tanggal Invoice</label>
						<div class="col-md-5">: <?php 
							$tgl = strtotime($invoice['tanggal_invoice']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;
						?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Tanggal Kunjungan</label>
						<div class="col-md-5">: <?php
							$tgl = strtotime($pasien['tanggal_visit']);
							$hasil = date('d F Y', $tgl); 
							echo $hasil;
						?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nomor Rekam Medis</label>
						<div class="col-md-5">: <?php echo $pasien['rm_id']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Cara Bayar</label>
						<div class="col-md-5">: <?php echo $invoice['cara_bayar']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Pasien</label>
						<div class="col-md-5">: <?php echo $pasien['nama']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Asuransi</label>
						<div class="col-md-5">: <?php echo $invoice['nama_asuransi']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Alamat</label>
						<div class="col-md-5">: <?php echo $pasien['alamat_skr']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nama Perusahaan</label>
						<div class="col-md-5">: <?php echo $invoice['nama_perusahaan']; ?> </div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Jenis Kunjungan</label>
						<div class="col-md-5">: <?php echo $pasien['tipe_kunjungan']; ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label1 col-md-4">Nomor Ansuransi </label>
						<div class="col-md-5">: <?php echo $invoice['no_asuransi']; ?> </div>
					</div>
				</div>
			</div>
		</div>

		<hr class="garis">

		<form class="form-horizontal" role="form" id="submitTagihan">
			<div id="tagihadmisi">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Admisi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Admisi Tertagih</th>
									<th>Waktu </th>
									<th>Tarif</th>
									
								</tr>
							</thead>
							<tbody id="tbody_resep">
								<tr>
									<td colspan="4" align="center">Tidak Terdapat Tagihan Admisi</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>
			
			<div id="tagihankamar" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Kamar</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihankamar">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Kamar Tertagih</th>
									<th>Waktu Masuk </th>
									<th>Waktu Keluar</th>
									<th>Lama</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									
								</tr>
							</thead>
							<tbody id="tbody_ttkamar">
								<tr>
									<td colspan="8" align="center">Tidak Terdapat Tagihan Kamar</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihanakomodasi" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Makan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanakomodasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Akomodasi Tertagih</th>
									<th>Unit</th>
									<th>Jumlah</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									
								</tr>
							</thead>
							<tbody id="tbody_ttakomodasi">
								<tr>
									<td colspan="7" align="center">Tidak Terdapat Tagihan Makan</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tagihantindakanperawatan" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Perawatan</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanperawatan">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Perawatan Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttperawatan">
								<tr>
									<td colspan="7" align="center">Tidak Terdapat Tagihan Tindakan Perawatan</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtindakanpenunjang" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Penunjang</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanpenunjang">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Penunjang Tertagih</th>
									<th>Unit</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="tbody_ttpenunjang">
								<?php
									$no = 0;
									if(!empty($tagihantunjang)){
										foreach ($tagihantunjang as $data) {
											$tgl = strtotime(substr($data['waktu'], 0, 10));
											$hasil = date('d F Y', $tgl); 

											echo '
												<tr>
													<td>'.++$no.'</td>
													<td>'.$data['nama_tindakan'].'
														<input type="hidden" class="tpenunjang_id" value="'.$data['tindakan_penunjang_id'].'">
														<input type="hidden" class="vpenunjang_id" value="'.$data['penunjang_detail_id'].'">
													</td>
													<td>'.$data['nama_dept'].'</td>
													<td>'.$hasil.'</td>
													<td style="text-align:right;">'.number_format((intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])),0,'','.').'</td>
													<td style="text-align:right;">
														<input type="hidden" class="inputtarif" value="'.(intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])).'">
														<input type="number" class="form-control input-sm inputfraktur" style="width:80px" name="onfakturpenunjang" value="'.$data['on_faktur'].'">
														<input type="hidden" class="inputtotal">
													</td>
													<td style="text-align:right;" class="t_total">'.number_format((intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])+intval($data['on_faktur'])),0,'','.').'</td>
												</tr>
											';
										}
									}else{
										echo '
											<tr>
												<td colspan="7" align="center">Tidak Terdapat Tagihan Tindakan Penunjang</td>
											</tr>
										';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div id="tambahtagihantindakanoperasi" style="margin-top:30px">
				<div id="titleInformasi" style="margin-bottom:-40px;">
				<p style="text-align:center;margin-top:-30px;">Tagihan Tindakan Operasi</p></div>
				<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:20px;" role="form">

					<div class="clearfix"></div>
					
					<div class="portlet-body" style="margin: -20px 0px -20px 0px">
						<table class="table table-striped table-bordered table-hover" id="tbtagihanoperasi">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th>Operasi Tertagih</th>
									<th>Lingkup Operasi</th>
									<th>Waktu</th>
									<th>Tarif</th>
									<th width="100">On Faktur</th>
									<th>Total</th>
									
								</tr>
							</thead>
							<tbody id="tbody_ttoperasi">
								<tr>
									<td colspan="7" align="center">Tidak Terdapat Tagihan Tindakan Operasi</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><br>

			<div style="margin-right:40px;">
				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="totaltagihan" style="font-size:1.8em;margin-top:-10px;">
						<?php
							$total = 0;
							$totalreal = 0;
							if(!empty($tagihantunjang)){
								foreach ($tagihantunjang as $data) {
									$total += (intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])+intval($data['on_faktur']));
									$totalreal += (intval($data['js'])+intval($data['jp'])+intval($data['bakhp']));
								}
								echo number_format($total,0,'','.');
							}else echo $total;

							echo '</label>';
							echo '<input type="hidden" id="inputalltotal" value="'.$totalreal.'">';
							echo '<input type="hidden" id="fixtotal" ">';
						?>
					</div>
					<div class="col-md-4 pull-right" style="width:170px; margin-top:5px; text-align:right;">
						Total Tagihan (Rp.) : 
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="deposit" style="font-size:1.8em;margin-top:-10px;">
							0
						</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Deposit (Rp.) : 
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-2 pull-right">
						<label class="control-label pull-right" id="kekurangan" style="font-size:1.8em;margin-top:-10px;">
						<?php
							$total = 0;
							if(!empty($tagihantunjang)){
								foreach ($tagihantunjang as $data) {
									$total += (intval($data['js'])+intval($data['jp'])+intval($data['bakhp'])+intval($data['on_faktur']));
								}
								echo number_format($total,0,'','.');
							}else echo $total;
						?>
						</label>
					</div>
					<div class="col-md-4 pull-right" style="width:150px; margin-top:5px; text-align:right;">
						Kekurangan (Rp.) : 
					</div>
				</div>
			</div>

			<br>
			<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
			<div style="margin-left:80%">
				<span style="padding:0px 10px 0px 10px;">
					<button class="btn btn-info" >CETAK</button> 
					<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
					<button type="submit" class="btn btn-success">SIMPAN</button> 
				</span>
			</div>
			<br>
		</form>

		<div class="modal fade" id="modalttpenunjang" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Tindakan Penunjang</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   										
			        			<div class="form-group">
									<label class="control-label col-md-4">Penunjang Tertagih</label>
									<div class="col-md-5">
										<input type="text"  class="typeahead form-control" autocomplete="off" spellcheck="false" id="penunjangtertagih" name="penunjangtertagih" placeholder="Penunjang Tertagih"  > 
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Unit</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="penunjangunit" name="penunjangunit" placeholder="Unint Penunjang"  > 
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Waktu</label>
									<div class="col-md-5">	
										<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" class="form-control" readonly data-date-format="dd/mm/yyyy - hh:ii" data-provide="datetimepicker" placeholder="<?php echo date("d/m/Y - H:i");?>">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="tarifttpenunjang" name="tarifttpenunjang" placeholder="Tarif" > 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="onfakturrrpenunjang" name="onfakturrrpenunjang" placeholder="On Faktur" >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Total</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="total" name="total" placeholder="Total" readonly >
									</div>
			        			</div>

								<div class="form-group">
									<label class="control-label col-md-4">Paramedis</label>
									<div class="col-md-5">	
										<input type="text" class="typeahead form-control" name="paramedis" id="paramedis" placeholder="Search Paramedis" autocomplete="off" spellcheck="false">		
									</div>
			        			</div>
			        			
		        			</div>
	       				</div>
		        		<br>
		        		<div class="modal-footer">
		        			<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			     	<button type="submit" class="btn btn-success">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>


		<br><br><br>	
	</div>
</div>

<script type="text/javascript">
	$(window).ready(function(){

		$('.inputfraktur').change(function(){
			var element = $(this).closest('tr').find('td .inputtarif').val();
			var inputtotal = $(this).closest('tr').find('td .inputtotal');
			var ttotal = $(this).closest('tr').find('td').eq(6);
			var faktur = $(this).val();
			var alltotal = $('#inputalltotal').val();
			var all = 0;
			var total = Number(element)+Number(faktur);

			inputtotal.val(total);
			ttotal.text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

			$('.inputfraktur').each(function(){
				all+=Number($(this).val());
			});
			
			all += Number(alltotal);

			$('#totaltagihan').text(all.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			$('#kekurangan').text(all.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			$('#fixtotal').val(all);
		});

		$('.inputfraktur').keyup(function(){
			var element = $(this).closest('tr').find('td .inputtarif').val();
			var inputtotal = $(this).closest('tr').find('td .inputtotal');
			var ttotal = $(this).closest('tr').find('td').eq(6);
			var faktur = $(this).val();
			var alltotal = $('#inputalltotal').val();
			var all = 0;
			var total = Number(element)+Number(faktur);

			inputtotal.val(total);
			ttotal.text(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

			$('.inputfraktur').each(function(){
				all+=Number($(this).val());
			});
			
			all += Number(alltotal);

			$('#totaltagihan').text(all.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			$('#kekurangan').text(all.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			$('#fixtotal').val(all);
		});

		$('#submitTagihan').submit(function(event){
			event.preventDefault();
			
			$('#tbody_ttpenunjang tr').each(function(){
				var item = {};
				item[1] = {};
				var vpenunjang = $(this).children('td').children('.vpenunjang_id');
				var mpenunjang = $(this).children('td').children('.tpenunjang_id');
				var tarif = $(this).children('td').children('.inputtarif');
				var faktur = $(this).children('td').children('.inputfraktur');

				item[1]['tindakan_penunjang_id'] = mpenunjang.val();
				item[1]['penunjang_detail_id'] = vpenunjang.val();
				item[1]['no_invoice'] = $('#no_invoice').val();
				item[1]['dept_id'] = $('#dept_id').val();
				item[1]['tarif'] = tarif.val();
				item[1]['tarif_bpjs'] = 0;
				item[1]['selisih'] = 0;
				item[1]['on_faktur'] = faktur.val();

				$.ajax({
					type:'POST',
					data:item,
					url:'<?php echo base_url(); ?>laboratorium/invoicenonbpjs/save_tagihan',
					success:function(data){
						console.log(data);
						window.location.href="<?php echo base_url(); ?>laboratorium/homelab";
					},error:function(data){
						console.log(data);
					}
				});

			});

			myAlert('Data Berhasil Disimpan');

		});
	});
</script>