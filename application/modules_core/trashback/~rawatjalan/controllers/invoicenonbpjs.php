<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Invoicenonbpjs extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_invoicenonbpjs");
		$data['page_title'] = "Invoice Non BPJS";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load template
		//$data['content'] = 'tagihan/invoicenonbpjs';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		//$this->load->view('base/operator/template', $data);
		redirect('rawatjalan/homerawatjalan');
	}

	public function invoice($no_invoice){
		$data['content'] = 'tagihan/invoicenonbpjs';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$invoice = $this->m_invoicenonbpjs->get_visit_id($no_invoice);
		$visit_id = $invoice['visit_id'];
		$sub_visit = $invoice['sub_visit'];
		$data['visit_id'] = $visit_id;
		$data['sub_visit'] = $invoice['sub_visit'];
		$data['no_invoice'] = $no_invoice;
		$data['invoice'] = $invoice;

		$pasien = $this->m_invoicenonbpjs->get_data_pasien($visit_id);
		$data['pasien'] = $pasien;
		
		$this->load->view('base/operator/template', $data);	
	}

	public function create_tagihan(){
		$no_invoice = $_POST['no_invoice'];
		$sub_visit = $_POST['sub_visit'];

		$tindakan = $this->m_invoicenonbpjs->get_tindakan($sub_visit);

		foreach ($tindakan as $value) {
			$value['no_invoice'] = $no_invoice;
			$value['total'] = $value['jumlah'];
			
			$insert = $this->m_invoicenonbpjs->insert_tagihan($value);
		}

		$result = $this->m_invoicenonbpjs->get_tagihanperawatan($no_invoice);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function hapus_tindakan($id){
		$temp = $this->m_invoicenonbpjs->get_datatagihan($id);
		$no_invoice = $temp['no_invoice'];
		$care_id = $temp['care_id'];

		$input = $this->m_invoicenonbpjs->hapus_tindakan($care_id);
		$input = $this->m_invoicenonbpjs->hapus_tagihan($id);

		$result = $this->m_invoicenonbpjs->get_tagihanperawatan($no_invoice);
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_master_dept(){
		$result = $this->m_invoicenonbpjs->get_master_dept();

		header('Content-Type: application/json');
		echo json_encode($result);
	}
	
	public function save_tindakan(){
		$visit_id = $_POST['visit_id'];
		$care['visit_id'] = $visit_id;
		$care['sub_visit'] = $_POST['sub_visit'];
		$care['waktu_tindakan'] = $this->date_db($_POST['waktu']);
		$care['tindakan_id'] = $_POST['tindakan_id'];
		$care['on_faktur'] = $_POST['on_faktur'];
		$care['paramedis'] = $_POST['paramedis'];
		$care['js'] = $_POST['js'];
		$care['jp'] = $_POST['jp'];
		$care['bakhp'] = $_POST['bakhp'];
		$care['tarif'] = $_POST['tarif'];
		$care['jumlah'] = $_POST['jumlah'];
		$care['dept_id'] = $_POST['dept_id'];
		$care['paramedis_lain'] = $_POST['paramedis_lain'];

		$id = $this->m_invoicenonbpjs->get_last_visit_care($visit_id);
		if($id){
			$vid = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vid) == "1") {
				$vid = '000'. $vid;
			}else if(strlen($vid) == "2"){
				$vid = '00' . $vid;
			}else if (strlen($vid) == "3") {
				$vid = '0' . $vid;
			}
			$care['care_id'] = "CA".$visit_id."".($vid);
		}else{
			$care['care_id'] = "CA".$visit_id."0001";
		}

		$tagihan['care_id'] = $care['care_id'];
		$tagihan['no_invoice'] = $_POST['no_invoice'];
		$tagihan['tindakan_id'] = $_POST['tindakan_id'];
		$tagihan['dept_id'] = $_POST['dept_id'];
		$tagihan['waktu'] = $this->date_db($_POST['waktu']);
		$tagihan['js'] = $_POST['js'];
		$tagihan['jp'] = $_POST['jp'];
		$tagihan['bakhp'] = $_POST['bakhp'];
		$tagihan['tarif'] = $_POST['tarif'];
		$tagihan['jumlah'] = $_POST['jumlah'];
		$tagihan['on_faktur'] = $_POST['on_faktur'];
		$tagihan['paramedis'] = $_POST['paramedis'];
		$tagihan['petugas_input'] = $_POST['paramedis'];

		//submit visit_care
		$in_care = $this->m_invoicenonbpjs->save_tindakan($care);

		//submit tindakan
		$in_tagihan = $this->m_invoicenonbpjs->save_tagihan($tagihan);

		$result = $this->m_invoicenonbpjs->get_inserted_tagihan($care['care_id']);

		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function date_db($date){
		$dateTime = DateTime::createFromFormat('d/m/Y H:i',$date);
		$newDateString = $dateTime->format('Y-m-d H:i:s');
		return $newDateString;
	}
}
