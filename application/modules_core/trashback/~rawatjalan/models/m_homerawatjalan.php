<?php
class m_homerawatjalan extends CI_Model{
	public function get_search_pasien($query){
		$sql = "SELECT * FROM pasien p, visit v, visit_rj vr, master_dept d 
					WHERE p.rm_id = v.rm_id AND v.visit_id = vr.visit_id 
					AND v.dept_id = d.dept_id AND v.dept_id = 10
					AND vr.waktu_keluar IS NULL AND v.status_visit = 'REGISTRASI'
					AND (p.nama LIKE '%$query%' 
					OR p.rm_id LIKE '%$query%')"; //10 adalah dept_id poli umum
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function get_filter_pasien($query, $start, $end){
		$sql = "SELECT * FROM pasien p, visit v, visit_rj vr, master_dept d 
					WHERE p.rm_id = v.rm_id AND v.visit_id = vr.visit_id 
					AND v.dept_id = d.dept_id AND v.dept_id = 10
					AND vr.waktu_keluar IS NULL 
					AND vr.waktu_masuk BETWEEN '$start' AND '$end'
					AND p.nama LIKE '%$query%' 
					OR p.rm_id LIKE '%$query%'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function daftar_pasien(){
		$sql = "SELECT * FROM pasien p, visit v, visit_rj vr, master_dept d 
			WHERE p.rm_id = v.rm_id AND v.visit_id = vr.visit_id 
			AND v.dept_id = d.dept_id AND vr.unit_tujuan = 10
			AND vr.waktu_keluar IS NULL AND v.status_visit = 'REGISTRASI'
			ORDER BY vr.waktu_masuk ASC"; //10 adalah dept_id poli umum

			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
	}

	public function get_dept_id($select){
        $sql = "SELECT dept_id FROM master_dept WHERE nama_dept = '$select' LIMIT 1";
        $query = $this->db->query($sql);
        if ($query) {
            return $query->row_array();
        }else{
            return false;
        }        
    }

    public function search_tagihan($search){
    	$sql = "SELECT *, t.cara_bayar as carapembayaran FROM tagihan t, pasien p, visit v, visit_rj vr, master_dept m 
    			WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.sub_visit = vr.rj_id AND vr.unit_tujuan = m.dept_id
    			AND vr.visit_id = v.visit_id AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '$search') AND m.jenis = 'POLIKLINIK'
    			";

    	$query = $this->db->query($sql);
    	return $query->result_array();
    }

    public function get_obat_unit($dept_id)
    {
        $sql = "SELECT *  FROM obat o left join obat_detail od on o.obat_id = od.obat_id 
                left join obat_dept ot on od.obat_detail_id = ot.obat_detail_id left join 
                (select * from obat_dept_stok order by obat_dept_stok_id desc) ods on ot.obat_dept_id = ods.obat_dept_id 
                left join obat_merk om on om.merk_id = o.merk_id left join jenis_obat jo on jo.jenis_obat_id = o.jenis_obat_id
                left join master_penyedia mp on mp.penyedia_id = o.penyedia_id
                left join obat_satuan os on os.satuan_id = o.satuan_id
                where ot.dept_id = '$dept_id' and o.is_hidden = '0' group by ods.obat_dept_id";
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function filter_farmasi_expired($filterby,$now,$dept_id)
	{
		$end = $filterby;
		$sql = "SELECT *  FROM obat o left join obat_detail od on o.obat_id = od.obat_id 
				left join obat_dept ot on od.obat_detail_id = ot.obat_detail_id left join 
				(select * from obat_dept_stok order by obat_dept_stok_id desc) ods on ot.obat_dept_id = ods.obat_dept_id 
				left join obat_merk om on om.merk_id = o.merk_id left join jenis_obat jo on jo.jenis_obat_id = o.jenis_obat_id
				left join master_penyedia mp on mp.penyedia_id = o.penyedia_id
				left join obat_satuan os on os.satuan_id = o.satuan_id
				where ot.dept_id = '$dept_id' and o.is_hidden = '0'
				AND TIMESTAMPDIFF(MONTH, '$now', od.tgl_kadaluarsa) +
					  DATEDIFF(
					    od.tgl_kadaluarsa,
					    '$now' + INTERVAL
					      TIMESTAMPDIFF(MONTH, '$now', od.tgl_kadaluarsa)
					    MONTH
					  ) /
					  DATEDIFF(
					    '$now' + INTERVAL
					      TIMESTAMPDIFF(MONTH, '$now', od.tgl_kadaluarsa) + 1
					    MONTH,
					    '$now' + INTERVAL
					      TIMESTAMPDIFF(MONTH, '$now', od.tgl_kadaluarsa)
					    MONTH
					  ) <= '$end'
				AND TIMESTAMPDIFF(MONTH, '$now', od.tgl_kadaluarsa) +
					  DATEDIFF(
					    od.tgl_kadaluarsa,
					    '$now' + INTERVAL
					      TIMESTAMPDIFF(MONTH, '$now', od.tgl_kadaluarsa)
					    MONTH
					  ) /
					  DATEDIFF(
					    '$now' + INTERVAL
					      TIMESTAMPDIFF(MONTH, '$now', od.tgl_kadaluarsa) + 1
					    MONTH,
					    '$now' + INTERVAL
					      TIMESTAMPDIFF(MONTH, '$now', od.tgl_kadaluarsa)
					    MONTH
					  ) >  ('$end' - 3)
				group by ods.obat_dept_id";
		$hasil = $this->db->query($sql);
		if ($hasil) {
			return $hasil->result_array();
		}else{
			return false;
		}
	}

	public function filter_farmasi($filterby,$filterval,$dept_id)
	{
		$sql = "SELECT *  FROM obat o left join obat_detail od on o.obat_id = od.obat_id 
				left join obat_dept ot on od.obat_detail_id = ot.obat_detail_id left join 
				(select * from obat_dept_stok order by obat_dept_stok_id desc) ods on ot.obat_dept_id = ods.obat_dept_id 
				left join obat_merk om on om.merk_id = o.merk_id left join jenis_obat jo on jo.jenis_obat_id = o.jenis_obat_id
				left join master_penyedia mp on mp.penyedia_id = o.penyedia_id
				left join obat_satuan os on os.satuan_id = o.satuan_id
				where ot.dept_id = '$dept_id' and o.is_hidden = '0'";
		if ($filterby == 'jenis') {
			$sql .= " AND jo.jenis_obat LIKE '%$filterval%' ";
		}else if ($filterby == 'nama') {
			$sql .= " AND o.nama LIKE '%$filterval%' ";
		}else if ($filterby == 'merek') {
			$sql .= " AND om.nama_merk LIKE '%$filterval%' ";
		}
		$sql .= "group by ods.obat_dept_id";
		$result = $this->db->query($sql);
		if($result)
			return $result->result_array();
		else
			return false;
	}

	public function insert_permintaan($insert)
	{
		$result = $this->db->insert('obat_permintaan', $insert);
		if ($result) {
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	public function insert_detail_permintaan($ins)
	{
		$result = $this->db->insert('obat_permintaan_detail', $ins);
		if ($result) {
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	public function submit_retur_dept($insert)
	{
		$result = $this->db->insert('obat_retur_dept', $insert);
		if ($result) {
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	public function insert_detail_returdept($ins)
	{
		$result = $this->db->insert('obat_retur_dept_detail', $ins);
		if ($result) {
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	public function insert_permintaanbarang($insert)
	{
		$result = $this->db->insert('barang_permintaan', $insert);
		if ($result) {
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	public function insert_detail_permintaanbarang($insert)
	{
		$result = $this->db->insert('barang_permintaan_detail', $insert);
		if ($result) {
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	public function get_dept_nama($dept)
	{
		$sql = "SELECT nama_dept from master_dept where dept_id = '$dept'";
		return $this->db->query($sql)->row_array();
	}

	public function get_sensus_harian_poli($dept_id='', $start, $end)
	{
		$sql = "SELECT p.rm_id, p.nama, p.alamat_skr, p.jenis_kelamin,a.cara_bayar, 
				IF((a.is_kasus_baru=1 or a.is_kasus_baru=NULL),'Lama', 'Baru') as 'jenis_kasus', x.nama_dept as 'asal_rujukan',
				z.nama_dept as 'tujuan_rujuk', b.diagnosa1, p.tanggal_lahir
				FROM visit_rj a inner join overview_klinik b on a.rj_id = b.rj_id 
				left join visit v on v.visit_id = a.visit_id 
				left join pasien p on p.rm_id = v.rm_id left join master_dept x on x.dept_id = a.unit_asal 
				left join master_dept z on z.dept_id = a.unit_rujukan 
				where substr(a.rj_id,1,2) = '$dept_id' and (a.waktu_masuk >= date('$start') and a.waktu_masuk <= date('$end'))";
		return $this->db->query($sql)->result_array();
	}	

	public function get_jasapelayanan($dept_id){
		$sql = "SELECT v.waktu_tindakan, mt.nama_tindakan ,v.care_id, vr.cara_bayar, p.nama_petugas, v.paramedis_lain, v.jp FROM visit_care v, visit_rj vr, petugas p, master_tindakan mt, master_tindakan_detail mtd WHERE v.paramedis = p.petugas_id AND v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = vr.rj_id AND v.dept_id = $dept_id";
		return $this->db->query($sql)->result_array();	
	}

	public function search_jasalayanan($dept, $mulai, $sampai, $carabayar, $paramedis){
		if($carabayar == ''){
			$sql = "SELECT v.waktu_tindakan, mt.nama_tindakan ,v.care_id, vr.cara_bayar, p.nama_petugas, v.paramedis_lain, v.jp FROM visit_care v, visit_rj vr, petugas p, master_tindakan mt, master_tindakan_detail mtd WHERE v.paramedis = p.petugas_id AND v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = vr.rj_id AND v.dept_id = '$dept'  AND v.paramedis = '$paramedis' AND v.waktu_tindakan BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59'";
		}else if($paramedis == ''){
			$sql = "SELECT v.waktu_tindakan, mt.nama_tindakan ,v.care_id, vr.cara_bayar, p.nama_petugas, v.paramedis_lain, v.jp FROM visit_care v, visit_rj vr, petugas p, master_tindakan mt, master_tindakan_detail mtd WHERE v.paramedis = p.petugas_id AND v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = vr.rj_id AND v.dept_id = '$dept' AND vr.cara_bayar = '$carabayar' AND v.waktu_tindakan BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59'";
		}else{
			$sql = "SELECT v.waktu_tindakan, mt.nama_tindakan ,v.care_id, vr.cara_bayar, p.nama_petugas, v.paramedis_lain, v.jp FROM visit_care v, visit_rj vr, petugas p, master_tindakan mt, master_tindakan_detail mtd WHERE v.paramedis = p.petugas_id AND v.tindakan_id = mtd.detail_id AND mtd.tindakan_id = mt.tindakan_id AND v.sub_visit = vr.rj_id AND v.dept_id = '$dept' AND v.waktu_tindakan BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59'";
		}
		return $this->db->query($sql)->result_array();			
	}
}
?>