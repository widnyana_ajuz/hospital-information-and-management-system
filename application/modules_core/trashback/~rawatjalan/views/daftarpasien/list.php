<br>
<div class="title" id="rowfix" style="position:fixed; z-index:10; width:98.5%">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>rawatjalan/homerawatjalan">Poliklinik Umum</a>
		<i class="fa fa-angle-right"></i>
		<a class="nama" href="<?php echo base_url() ?>rawatjalan/daftarpasien">
			<?php echo $pasien['alias']; ?>. 
			<?php echo $pasien['nama']; ?> / 
			<?php  
				$datetime1 = new DateTime();
				$datetime2 = new DateTime($pasien['tanggal_lahir']);
				$interval = $datetime1->diff($datetime2);
												
				if($interval->y > 0)
					echo $interval->y ." tahun ";
			?> / 
			<?php echo $pasien['jenis_kelamin']; ?>
		</a>&nbsp;&nbsp;
	</li>
</div>


<input type="hidden" id="rj_id" value ="<?php echo $visit_rj['rj_id']; ?>">
<input type="hidden" id="v_id" value="<?php echo $visit_id; ?>"/>
<input type="hidden" id="dept_id" value="<?php echo $dept_id; ?>">

<div class="navigation" style="margin-left: 10px; margin-top:110px;" >
 	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
    	<li class="active"><a href="#identitas" data-toggle="tab">Identitas Pasien</a></li>
  		<li ><a href="#rm" data-toggle="tab">Overview Klinik</a></li>
    	<li><a href="#resep" data-toggle="tab">Pemberian Resep</a></li>
    	<li><a href="#penunjang" data-toggle="tab">Pemeriksaan Penunjang</a></li>
    	<li><a href="#orderkamar" data-toggle="tab">Order Kamar Operasi</a></li>
    	<li><a href="#konsul" data-toggle="tab">Order Konsultasi Gizi</a></li>
    	<li><a href="#riwayat" data-toggle="tab">Riwayat Kunjungan</a></li>
    	<li><a href="#resume" data-toggle="tab">Resume Pulang</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">

		<div class="tab-pane active" id="identitas">
    		<div class="dropdown">
        		<div id="titleInformasi">Identitas Pasien</div>
 			</div>
          	
            <div class="informasi" id="info1">
	            <form class="form-horizontal" role="form">
	            	
	           		<div class="form-group">
						<label class="control-label col-md-3" >Jenis Identitas Pasien</label>
						<div class="col-md-1">
							<input  id="jnsIdentitas" name="jenis_identitas" value="<?php echo $pasien['jenis_id']; ?>"style="border:0px;background-color:transparent;font-weight:bold" disabled />
						</div>					
						
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3" >Nomor Identitas Pasien</label>
						<div class="col-md-3">
							<input  id="NomorID" name="nomor_identitas" value="<?php echo $pasien['no_id']; ?>" style="border:0px;background-color:transparent;font-weight:bold" disabled />
						</div>
					</div>	
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					<br>

					<div class="form-group">
						<label class="control-label col-md-3">No RM</label>
						<div class="col-md-4">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="rm_id" name="rm_id" value="<?php echo $pasien['rm_id']; ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					

					<div class="form-group">
						<label class="control-label col-md-3">Nama Lengkap </label>
						<div class="col-md-4">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="NamaLengkap" name="NamaLengkap" value="<?php echo $pasien['nama']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Alias </label>
						<div class="col-md-1">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="alias" name="alias" value="<?php echo $pasien['alias']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Jenis Kelamin</label>
						<div class="col-md-1">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="jk" name="jk" value="<?php echo $pasien['jenis_kelamin']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Golongan Darah </label>
						<div class="col-md-1">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="goldarah" name="goldarah" value="<?php echo $pasien['gol_darah']; ?>" disabled />												
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Agama </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="agama" name="agama" value="<?php echo $pasien['agama']; ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					
					<div class="form-group">
						<label class="control-label col-md-3">Tempat Lahir </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="newTempatLahir" name="tempat_lahir" value="<?php echo $pasien['tempat_lahir']; ?>" disabled/>
						</div>
																		
					</div>			
					<div class="form-group">
						<label class="control-label col-md-3">Tanggal Lahir </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" class="input-medium date-picker" maxlength="12" type="text" value="<?php
								$tgl = strtotime($pasien['tanggal_lahir']);
								$hasil = date('d F Y', $tgl); 
								echo $hasil; ?>" 
								data-date-format="dd/mm/yyyy" id="TanggalLahir" placeholder="Tanggal Lahir" disabled />
						</div>												
					</div>			

					<div class="form-group">
						<label class="control-label col-md-3">Umur</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="umur" name="umur" 
							value="<?php  
								$datetime1 = new DateTime();
								$datetime2 = new DateTime($pasien['tanggal_lahir']);
								$interval = $datetime1->diff($datetime2);
																
								if($interval->y > 0)
									echo $interval->y ." tahun ";
								if($interval->m > 0)
									echo $interval->m." bulan ";
								if($interval->d > 0)
									echo $interval->d ." hari";
							?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					
					<div class="form-group">
						<label class="control-label col-md-3">Status Kawin</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="status kawin" name="statuskawin" value="<?php echo $pasien['status_perkawinan']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Pendidikan Terakhir</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="pendidikan" name="pendidikan" value="<?php echo $pasien['pendidikan']; ?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Pekerjaan </label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="Pekerjaan" name="pekerjaan" value="<?php echo $pasien['pekerjaan']; ?>" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Nomor Telepon</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="nomorPasien" name="nomor_pasien" value="<?php echo $pasien['no_telp']; ?>" disabled />
						</div>						
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
					
					<div class="form-group">
						<label class="control-label col-md-3">Alamat </label>
						<div class="col-md-5">
							<input style="border:0px;background-color:transparent;width:900px;font-weight:bold" id="Alamat" name="alamat" value="<?php echo $pasien['alamat_skr']; ?>" disabled />
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Alamat KTP</label>
						<div class="col-md-5">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="AlamatKTP" name="alamatKTP" value="<?php echo $pasien['alamat_ktp']; ?>" disabled />
						</div>						
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Wilayah </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="provinsi" name="provinsi" value="<?php echo $pasien['nama_prov']; ?>" disabled />
						</div>											
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kabupaten </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="kabupaten" name="kabupaten" value="<?php echo $pasien['nama_kab']; ?>" disabled />
						</div>												
						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kecamatan </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="kecamatan" name="kecamatan" value="<?php echo $pasien['nama_kec']; ?>" disabled />
						</div>
						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kelurahan </label>									
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="kelurahan" name="kelurahan" value="<?php echo $pasien['kel_id_skr']; ?>" disabled />
						</div>
					</div>
					<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:50px;">
						
					<div class="form-group">
						<label class="control-label col-md-3">Cara Pembayaran</label>
						<div class="col-md-2">
							<input style="border:0px;background-color:transparent;font-weight:bold" id="cara_bayar" name="cara_bayar" value="<?php echo $pasien['cara_bayar']; ?>" disabled />
						</div>						
					</div>
				</form>
			</div>
			<br><br>
		</div>

		<div class="tab-pane" id="rm"> 
		
			<div class="dropdown"  id="btnBawahTambahCare">
		        <div id="titleInformasi">Konsultasi Dokter
		        </div>
		        <div class="btnBawah floatright" style="margin-top:-25px;">
		           	<i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i>
		        </div>
		    </div>
	        <div class="informasi" id="tbhCare">
	 			<form class="form-horizontal" role="form" method="POST" id="submitOver">
			       	<br>
	 				<div class="form-group">
						<label class="control-label col-md-3">Waktu</label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="inputdate" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>" required>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Anamnesa</label>
						<div class="col-md-4">
							<textarea class="form-control isian" id="anamnesaOver" name="anamnesa" placeholder="Anamnesa" required></textarea>
						</div>
					</div>

					<fieldset class="fsStyle">
						<legend>
			                Tanda Vital
						</legend>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Tekanan Darah</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="tekanandarahOver" name="takanandarah" placeholder="Tekanan Darah" required>
							</div>
							<label class="control-label col-md-2">mmHg</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Temperatur</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="tempOver" name="temperatur" placeholder="Temperatur" required>
							</div>
							<label class="control-label col-md-2">&deg;C</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Nadi</label>
							<div class="col-md-2">
								<input type="number" class="form-control" id="nadiOver" name="nadi" placeholder="Nadi" required>
							</div>
							<label class="control-label col-md-2">x/menit</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Pernapasan</label>
							<div class="col-md-2">
								<input type="number" class="form-control" id="pernapasanOver" name="pernapasan" placeholder="Pernapasan" required>
							</div>
							<label class="control-label col-md-2">x/menit</label>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Berat Badan</label>
							<div class="col-md-2">
								<input type="number" class="form-control" id="beratOver" name="berat" placeholder="Berat Badan" required>
							</div>
							<label class="control-label col-md-2">Kg</label>
						</div>
			  		</fieldset>

			  		<fieldset class="fsStyle">
						<legend>
			                Diagnosa & Terapi
						</legend>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Dokter Pemeriksa</label>
							<div class="col-md-3">
								<input type="hidden" id="id_dokterOver">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control" id="nama_dOver" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" readonly required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Diagnosa Utama</label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d1" id="k_utama_over" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly required>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d1" id="dnama1" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Diagnosa Sekunder</label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d2" id="k_sek_over" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d2" id="dnama2" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;"></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d3" id="k_sek_over2" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d3" id="dnama3" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;"></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d4" id="k_sek_over3" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d4" id="dnama4" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;"></label>
							<div class="col-md-1">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d5" id="k_sek_over4" placeholder="Kode" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
							<div class="col-md-2">
								<input type="text" style="cursor:pointer;background-color:white" class="form-control isian d5" id="dnama5" placeholder="Keterangan" data-toggle="modal" data-target="#searchDiagnosa" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Detail Diagnosa</label>
							<div class="col-md-4">
								<textarea class="form-control" id="detail_over" name="detailDiagnosa" placeholder="Detail Diagnosa" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Terapi</label>
							<div class="col-md-4">
								<textarea class="form-control" id="terapi_over" name="terapi" placeholder="Terapi" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" style="width:310px;">Alergi</label>
							<div class="col-md-3">
								<input type="text" class="form-control isian" id="alergi_over" name="alergi" placeholder="Alergi">
							</div>
						</div>
			  		</fieldset>

					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>
			</div>
			<br>
			<hr class="garis" style="border: solid 1px #50BFF9; border-radius: 5px; margin-left:0px; margin-right:20px;margin-left:20px;">
					
				<div class="portlet-body" style="margin: 20px 10px 0px 10px">
					<table id="tableOverview" class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tableOverview">
						<thead>
							<tr class="info" style="text_align: center;">
								<th width="20">No.</th>
								<th>Unit</th>
								<th>Anamnesa</th>
								<th>Dokter Pemeriksa</th>
								<th width="20">Action</th>
							</tr>
						</thead>
						<tbody id="tbody_overview">
							<?php
								echo "<input type='hidden' id='jml_over' value='".count($overview_history)."' ";
								$i = 0;
								if(!empty($overview_history)){
									foreach ($overview_history as $over) {
										$i++;
										echo'<tr>';
											echo'<td>'.$i.'</td>';
											echo'<td>Poli Umum</td>';
											echo'<td>'.$over['anamnesa'].'</td>';
											echo'<td>'.$over['nama_petugas'].'</td>';
											echo'<td style="text-align:center;"><a href="#riwayat_over" data-toggle="modal" onClick="detailOver(&quot;'.$over['id'].'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>';
										echo'</tr>';
									}
								}
							?>
							
						</tbody>
					</table>												
				</div>

			<br>
		 	
		 	<div class="dropdown" id="btnBawahCare">
		 	  	<div id="titleInformasi" >Uraian Tindakan Klinik</div>
		        <div id="btnBawahCare" class="btnBawah floatright"  style="margin-top:-25px;"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>
	        <br>

		<div class="modal fade" id="tambahTindakan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="submitTindakan">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
			   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			   				<h3 class="modal-title" id="myModalLabel">Tambah Tagihan Tindakan Perawatan</h3>
			   			</div>
						<div class="modal-body">
							<div class="informasi">
				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="tin_date" style="cursor:pointer;" class="form-control"  readonly data-provide="datetimepicker" data-date-format="dd/mm/yyyy hh:ii" value="<?php echo date("d/m/Y H:i");?>"/>
									</div>
			        			</div>
			        			<div class="form-group">
									<label class="control-label col-md-4">Tindakan</label>
									<div class="col-md-6">
										<input type="hidden" id="idtindakan_klinik">
										<input type="text" class="form-control" id="namatindakan" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Search Tindakan" required>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"></label>
									<div class="col-md-6">
										<textarea class="form-control" id="namatindakanarea" readonly placeholder="Tindakan"></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Kelas Pelayanan</label>
									<div class="col-md-5">	
										<input type="hidden" id="idtindakdetail">
										<select class="form-control" name="kelas_tindakan" id="kelas_klinik" required>
											<option value="">Pilih Kelas</option>
											<option value="Kelas VIP">VIP</option>
											<option value="Kelas Utama">Utama</option>
											<option value="Kelas I">Kelas I</option>
											<option value="Kelas II">Kelas II</option>
											<option value="Kelas III">Kelas III</option>
										</select>
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Tarif</label>
									<div class="col-md-5">	
										<input type="hidden" id="js_klinik">
										<input type="hidden" id="jp_klinik">
										<input type="hidden" id="bakhp_klinik">
										<input type="text" class="form-control" id="tarif" name="tarif" placeholder="Tarif" readonly > 
									</div>
			        			</div>
			        			
			        			<div class="form-group">
									<label class="control-label col-md-4">On Faktur</label>
									<div class="col-md-5">	
										<input type="number" class="form-control" id="onfaktur" name="onfaktur" placeholder="On Faktur" required >
									</div>
			        			</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Jumlah</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" readonly>
									</div>
			        			</div>

								<div class="form-group">
									<label class="control-label col-md-4">Paramedis</label>
									<div class="col-md-5">	
										<input type="hidden" id="paramedis_id">
										<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
									</div>
			        			</div>
			        			<div class="form-group">
									<label class="control-label col-md-4">Paramedis Lain</label>
									<div class="col-md-6">	
										<textarea class="form-control" id="paramedis_lain" placeholder="Paramedis Lain"></textarea>
									</div>
			        			</div>
			        			
		        			</div>
	       				</div>
		        		<br><br>
		        		<div class="modal-footer">
		        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
		 			     	<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			     	<button type="submit" class="btn btn-success" id="saveTindakan">Simpan</button>
					    </div>
					</div>
				</div>
			</form>
		</div>


			<div class="tabelinformasi" id="tabelcare">
				<form class="form-horizontal" role="form" method="POST" style="margin-left:20px;margin-right:20px;">
					<div class="form-group">
						<a href="#tambahTindakan" data-toggle="modal"  style="margin-left:15px;font-size:11pt;"	><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Tambah">&nbsp;Tambah Pengadaan</i></a>
						<div class="clearfix"></div>        
					</div>
				    <div class="form-group">
				        <div class="portlet-body" style="margin: 0px 10px 0px 10px">
				        	<?php echo '<input type="hidden" id="jmlh_table" value="'.count($visit_care).'" >'; ?>
				            <table class="table table-striped table-bordered table-hover tableDTUtama" id="tableCare">
								<thead>
									<tr class="info">
										<th style="width:10px;">No.</th>
										<th>Waktu</th>
										<th>Tindakan</th>
										<th>Jasa Sarana</th>
										<th>Jasa Pelayanan</th>
										<th>BAKHP</th>
										<th>On faktur</th>
										<th>Paramedis</th>
										<th>Jumlah</th>
										<th style="width:20px;">Action</th>
									</tr>
								</thead>
								<tbody id="tbody_tindakan">
									<?php  
										if (!empty($visit_care)) {
											$i = 0;
											foreach($visit_care as $value){
												$i++;
												echo "<tr>";
													echo "<td>".$i."</td>";										
													echo "<td>".$value['waktu_tindakan']."</td>";									
													echo "<td>".$value['nama_tindakan']."</td>";												
													echo "<td>".$value['js']."</td>";										
													echo "<td>".$value['jp']."</td>";
													echo "<td>".$value['bakhp']."</td>";										
													echo "<td>".$value['on_faktur']."</td>";
													echo "<td>".$value['nama_petugas']."</td>";										
													echo "<td>".$value['jumlah']."</td>";
													echo "<td style='text-align:center'><a style='cursor:pointer;' class='hapusTindakan'><input type='hidden' class='getid' value='".$value['care_id']."''><i class='glyphicon glyphicon-trash'></i></a></td>";
												echo "</tr>";
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>

	        </div>
        <div class="tab-pane" id="resep">
	 		<div class="dropdown" id="btnBawahTambahResep">
    		    <div id="titleInformasi">Tambah Resep</div>
        		<div id="btnBawahTambahResep" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
        	</div>
            <br>
        	<div class="informasi" id="tambahResep">
        		<form class="form-horizontal" role="form" method="POST" id="submitresep">
					<div class="form-group">
						<label class="control-label col-md-3">Dokter</label>
						<div class="col-md-3">
							<input type="hidden" id="resepdokter">
							<input type="text" class="form-control" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" id="namadokter" required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Tanggal</label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="tgl_resep" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Deskripsi Resep</label>
						<div class="col-md-5">
							<textarea class="form-control" name="deskripsiResep" placeholder="Deskripsi Resep" id="deskripsiResep" required></textarea>							
						</div>
					</div>

					<!-- <div class="form-group">
						<label class="control-label col-md-3">Status Bayar</label>
						<div class="col-md-3">
							<input type="text" class="form-control" name="statusBayar" placeholder="Belum Lunas" readonly>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Status Ambil</label>
						<div class="col-md-3">
							<input type="text" class="form-control" name="statusBayar" placeholder="Belum Ambil" readonly>
						</div>
					</div> -->
					
					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>	
			</div>

	 		<div class="dropdown" id="btnBawahTabelResep">
		        <div id="titleInformasi">Tabel Resep</div>
		        <div id="btnBawahTabelResep" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
            <br>

        	<div id="tblResep">
	        	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover" id="tabelresepbersalin">
						<thead>
						<tr class="info">
							<th>Dokter</th>
							<th>Tanggal</th>
							<th>Deskripsi Resep</th>
							<th>Status Bayar</th>
							<th>Status Ambil</th>
							<th>Delete</th>
						</tr>
						</thead>
						<tbody id="tbody_resep">
						<?php  
							if (!empty($visit_resep)) {
								foreach ($visit_resep as $value) {
									echo '<tr>';
									echo '<td>'.$value['nama_petugas'].'</td>';										
									echo '<td>'.$value['tanggal'].'</td>';										
									echo '<td>'.$value['resep'].'</td>';										
									echo '<td>'.$value['status_bayar'].'</td>';										
									echo '<td>'.$value['status_ambil'].'</td>';										
									echo '<td style="text-align:center">';
									echo '<a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value="'.$value['resep_id'].'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>';
									echo '</td>';
									//echo '<td class="resep_id" style="display:none" >'.$value['resep_id'].'</td>';											
									echo '</tr>';
								}
							}else{
								echo '<tr>';
								echo '<td colspan="6"><center>Data Kosong</center><input type="hidden" id="checktable" value="null"/></td>';
								echo '</tr>';
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
        </div>

        <div class="tab-pane" id="penunjang">
	        <div class="dropdown" id="btnBawahPenunjang">
		        <div id="titleInformasi">Pemeriksaan Penunjang</div>
		        <div class="btnBawah" id="btnBawahPenunjang"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
		    </div>
		    <br>

            <div class="informasi" id="infoPenunjang">
	            <form class="form-horizontal" id="submit_penunjang">
	          		<div class="form-group">
						<label class="control-label col-md-3">Tanggal</label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="tun_date" class="form-control isian calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
							</div>
						</div>
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3" >Tujuan Penunjang</label>
						<div class="col-md-2">			
							<select class="form-control select" name="depTujuan" id="tun_tujuan">
								<option value="" selected>Pilih Unit Penunjang</option>
								<?php
									foreach ($penunjang as $data) {
										echo '<option value="'.$data['dept_id'].'">'.$data['nama_dept'].'</option>';
									}
								?>
							</select>		
						</div>							
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" >Pengirim</label>
						<div class="col-md-3">
							<input type="hidden" id="tun_iddokter">
							<input type="text" class="form-control" id="tun_namadokter" placeholder="Search Pengirim" data-toggle="modal" data-target="#searchDokter">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3" >Jenis Pemeriksaan</label>
						<div class="col-md-5">
							<textarea class="form-control" rows="5" id="tun_jenis" placeholder="Jenis Penunjang"></textarea>
						</div>
					</div>
					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>		
				<br>
	        </div>

	        <div class="dropdown" id="btnBawahTabelRiwayat">
		        <div id="titleInformasi">Tabel Riwayat Pemeriksaan</div>
		        <div id="btnBawahTabelRiwayat" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
            <br>

        	<div class="tabelinformasi" id="tblRiwayat">
	        	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
	        		<?php echo '<input type="hidden" id="tun_jumlah" value="'.count($visit_penunjang).'">'?>
					<table class="table table-striped table-bordered tableDTUtama table-hover" id="table_penunjang">
						<thead>
							<tr class="info">
								<th width="3%"> No. </th>
								<th> Tanggal Tindakan</th>
								<th> Departemen Penunjang</th>
								<th> Unit Rujukan</th>
								<th> Status </th>
								<th style="width:20px;"> Details</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 0;
								foreach ($visit_penunjang as $data) {
									$tgl = strtotime($data['waktu']);
									$hasil = date('d F Y', $tgl); 
									
									$no++;
									echo '
										<tr>
											<td>'.$no.'</td>
											<td align="center">'.$hasil.'</td>								
											<td>'.$data['unit_tujuan'].'</td>										
											<td>'.$data['unit_asal'].'</td>										
											<td>'.$data['status'].'</td>											
											<td style="text-align:center">
												<a href="#viewRiwayat" data-toggle="modal" data-placement="top"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Details"></i></a>
											</td>												
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="modal fade" id="viewRiwayat" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-dialog" style="width:1300px;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        	<h3 class="modal-title" id="myModalLabel">Hasil Pemeriksaan</h3>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-3 nama goright">Order ID:</label>
										<div class="col-md-9 nama">	0001 </div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-3 goright">Tanggal Tindakan:</label>
										<div class="col-md-8">
											12 Desember 2012
										</div>
									</div>
								</div>
								
								
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-4 goright">Departemen Penunjang:</label>
										<div class="col-md-8"> Labolatorium	</div>
									</div>
								</div>
								
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-3 goright">Keterangan Order:</label>
										<div class="col-md-8">terserah</div>
									</div>
								</div>								
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label1 col-md-4 goright">Status Hasil:</label>
										<div class="col-md-8">SELESAI</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<hr/>
							<table class="table table-striped table-bordered table-hover" id="tabelHasilPenunjang">
								<thead>
									<tr class="info">
										<th>
											 Pemeriksaan
										</th>
										<th>
											 Jenis
										</th>
										<th>
											 Hasil Pemeriksaan
										</th>							
										<th>
											 Waktu Publikasi
										</th>							
										<th>
											 Keterangan/Rujukan
										</th>							
										<th>
											 Pemohon Periksa
										</th>							
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											 Pemeriksaan
										</td>
										<td>
											 Jenis
										</td>
										<td>
											 Hasil Pemeriksaan
										</td>							
										<td>
											 Waktu Publikasi
										</td>							
										<td>
											 Keterangan/Rujukan
										</td>							
										<td>
											 Pemohon Periksa
										</td>							
									</tr>
								</tbody>
							</table>
						</div>
						<div class="modal-footer">
				 			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
        </div>

        <div class="tab-pane" id="orderkamar">
        	<!-- dropdown order kamar operasi -->
        	<div class="dropdown" id="btnBawahOrder">
	            <div id="titleInformasi">Order Kamar Operasi</div>
	            <div class="btnBawah" id="btnBawahOrder"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>

	        <div class="informasi" id="infoKamar">
	            <form class="form-horizontal" method="POST" id="submit_order_operasi">
	          		<div class="form-group">
						<label class="control-label col-md-3">Waktu Pelaksanaan</label>
						<div class="col-md-3" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="wktpelaksanaan" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
							</div>
						</div>
					</div>	

					<div class="form-group">
						<label class="control-label col-md-3">Dokter</label>
						<div class="col-md-3">
							<input type="hidden" id="iddokter_o">
							<input type="text" class="form-control" id="namadokter_o" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter">
						</div>
					</div>

					<!-- <div class="form-group">
						<label class="control-label col-md-3" >Tujuan Order</label>
						<div class="col-md-3">			
							<select class="form-control select" name="order" id="order">
								<option value="Kamar Operasi" selected>Kamar Operasi</option>
								<option value="Laboratorium">Laboratorium</option>
								<option value="Radiologi"  >Radiologi</option>
								<option value="Fisioterapi" >Fisioterapi</option>
							</select>		
						</div>							
					</div> -->

					<div class="form-group">
						<label class="control-label col-md-3" >Jenis Operasi</label>
						<div class="col-md-3">			
							<!-- <input type="text" class="form-control" placeholder="Jenis Operasi" id="jnsOperasi"> -->
							<select class="form-control select" name="jnsOperasi" id="jnsOperasi">
								<option value="" selected>Pilih Jenis Operasi</option>
								<option value="Kecil">Kecil</option>
								<option value="Sedang">Sedang</option>
								<option value="Besar">Besar</option>
								<option value="Khusus">Khusus</option>
							</select>
				 		</div>
					</div>					
							
					<div class="form-group">
						<label class="control-label col-md-3" >Detail Operasi</label>
						<div class="col-md-5">			
							<textarea class="form-control" rows="5" id="alasanoperasi" placeholder="Detail Operasi"></textarea>
				 		</div>
					</div>

					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>
				<br>
			</div>	<!-- End Dropdown -->

			<div class="dropdown" id="btnTableKamarOperasi">
	            <div id="titleInformasi">Table Daftar Kamar Operasi</div>
	            <div class="btnBawah" id="btnTableKamarOperasi"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	           	<br>

	        <div class="tabelinformasi" id="tabelKamar">
	           	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
	           		<?php echo "<input type='hidden' id='jml_data' value='".count($order_operasi)."'>"; ?>
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_order" >
						<thead>
							<tr class="info">
								<th>No. </th>
								<th>Tanggal Tindakan</th>
								<th>Dokter</th>
								<th>Tujuan Order</th>
								<th>Detail Operasi</th>
								<th> </th>
							</tr>
						</thead>
						<tbody>
						<?php
							$i = 1;
							if(!empty($order_operasi)){
								foreach ($order_operasi as $value) {
									echo"
										<tr>
											<td>".$i."</td>
											<td>".$value['waktu_mulai']."</td>
											<td>".$value['nama_petugas']."</td>										
											<td>Kamar Operasi</td>
											<td>".$value['alasan']."</td>
											<td style='text-align:center'>
												<i class='glyphicon glyphicon-trash hapusorder' data-toggle='tooltip' data-placement='top' style='cursor:pointer;' title='Hapus'><input type='hidden' class='getid' value='".$value['order_operasi_id']."'></i>
											</td>										
										</tr>
									";
									//'<i class="glyphicon glyphicon-trash" onclick="hapus_order(&quot;'+data['order_operasi_id']+'&quot;)" data-toggle="tooltip" data-placement="top" title="Hapus" style="cursor:pointer;"></i>'
									$i++;
								}
							}
						?>
						</tbody>
					</table>
				</div>	<br><br>
			</div>	<!-- End Dropdown -->
        </div>

        <div class="tab-pane" id="konsul">
        	<div class="dropdown" id="btnBawahOrderKonsul">
	            <div id="titleInformasi">Order Konsultasi Gizi</div>
	            <div class="btnBawah" id="btnBawahOrderKonsul"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>
	       
	        <div class="informasi" id="infoKonsul">
		      	<form class="form-horizontal" role="form" id="submit_gizi">
		          		<div class="form-group">
							<label class="control-label col-md-3">Tanggal Konsultasi</label>
							<div class="col-md-2" >
								<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" id="tanggal_gizi" style="cursor:pointer;" id="tgl_gizi" class="form-control isian calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
								</div>
							</div>
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3" >Konsultan Gizi</label>
							<div class="col-md-3">
								<input type="hidden"s id="k_gizi_id">
								<input type="text" class="form-control" id="k_gizi_nama" placeholder="Search Konsultan" data-toggle="modal" data-target="#searchDokter">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Kajian Gizi</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="kajianGizi" placeholder="Kajian Gizi"></textarea>
								
						 	</div>				
						</div>

						<div class="form-group">
							<label class="control-label col-md-3" >Anamnesa Diet</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="anamnesaDiet" placeholder="Anamnesa Diet"></textarea>
								
						 	</div>		
						</div>

						<!-- <div class="form-group">
							<label class="control-label col-md-3">Data Lab Pasien</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="dataLabPasien" placeholder="Data Lab Pasien">
							</div>
						</div> -->

						<div class="form-group">
							<label class="control-label col-md-3">Kajian Diet</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="kajianDiet"  placeholder="Kajian Diet"></textarea>
								
						 	</div>		
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Detail Menu Sehari-hari</label>
							<div class="col-md-5">			
								<textarea class="form-control" rows="2" id="DetailMenu"  placeholder="Detail Menu"></textarea>
								
						 	</div>		
						</div>

						<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>
			</div>	

			<div class="dropdown" id="btnTabelKonsultasi">
	            <div id="titleInformasi">Table Daftar Konsultasi Gizi</div>
	           	<div class="btnBawah" id="btnTabelKonsultasi"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
	        </div>
	        <br>

	        <div class="tabelinformasi" id="tabelKonsultasi" >
	           	<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" >
								<thead>
									<tr class="info">
										<th>Tanggal Konsultasi </th>
										<th>Konsultan</th>
										<th>Kajian Gizi</th>
										<th>Anamnesa Diet</th>
										<th>Kajian Diet</th>
										<th>Detail Menu Sehari-hari</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="tbody_gizi">
								
								<?php
									echo"<input type='hidden' id='jml_gizi' value='".count($gizi)."'>";

									if(!empty($gizi)){
										foreach ($gizi as $data) {
											echo'
												<tr>
													<td>'.$data['tanggal'].'</td>
													<td>'.$data['konsultan'].'</td>
													<td>'.$data['kajian_gizi'].'</td>										
													<td>'.$data['anamnesa_diet'].'</td>
													<td>'.$data['kajian_diet'].'</td>
													<td>'.$data['detail_menu'].'</td>
													<td style="text-align:center">
														<a style="cursor:pointer;" class="hapusgizi"><input type="hidden" class="getid" value="'.$data['gizi_id'].'"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
														<a href="#print"><i class="glyphicon glyphicon-print"  data-toggle="tooltip" data-placement="top" title="Print"></i></a>
													</td>										
												</tr>
											';
											$i++;
										}
									}else{
										echo '<tr>';
										echo '<td colspan="8"><center>Data Kosong</center><input type="hidden" id="checktable" value="null"/></td>';
										echo '</tr>';
									}
								?>

								</tbody>
							</table>
				</div>	<br><br>
			</div>	
        </div>   

        <div class="tab-pane" id="riwayat">
         	<div class="dropdown" id="rwp1">
            	<div id="titleInformasi">Riwayat Klinik</div>
            	<div class="btnBawah" id="btnBawahRiwayat"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div class="portlet-body" id="tblrwp1" style="margin: 20px 10px 0px 10px">
            	
				<table class="table table-striped table-bordered table-hover table-responsive tableDT">
					<thead>
						<tr class="info" style="text_align: center;">
							<th width="20">No.</th>
							<th>Tanggal</th>
							<th>Unit</th>
							<th>Anamnesa</th>
							<th>Dokter Pemeriksa</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody id="tbody_r_klinik">	
						<?php
							$i = 0;
							foreach ($riwayat_klinik as $data) {
								$i++;
								$tgl = strtotime($data['waktu']);
								$hasil = date('d F Y H:i:s', $tgl);

								echo '
									<tr>
										<td>'.$i.'</td>
										<td style="text-align:center">'.$hasil.'</td>
										<td>'.$data['nama_dept'].'</td>
										<td>'.$data['anamnesa'].'</td>
										<td>'.$data['nama_petugas'].'</td>
										<td style="text-align:center;"><a href="#riwayat_over" data-toggle="modal" onClick="detailOver(&quot;'.$data['id'].'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>
									</tr>
								';
							}
						?>					
					</tbody>
				</table>												
			</div>
			<br>
            <div class="dropdown" id="rwp2">
            	<div id="titleInformasi">Riwayat IGD</div>
            	<div class="btnBawah" id="btnBawahRiwayat"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div class="portlet-body" id="tblrwp2" style="margin: 20px 10px 0px 10px">
            	
				<table class="table table-striped table-bordered table-hover table-responsive tableDT">
					<thead>
						<tr class="info" style="text_align: center;">
							<th width="20">No.</th>
							<th>Tanggal</th>
							<th>Anamnesa</th>
							<th>Dokter Jaga</th>
							<th>Perawat Jaga</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no = 0;
							foreach ($riwayat_igd as $data) {
								$no++;
								$tgl = strtotime($data['waktu']);
								$hasil = date('d F Y H:i:s', $tgl); 
								echo '
									<tr>
										<td>'.$no.'</td>
										<td style="text-align:center">'.$hasil.'</td>
										<td>'.$data['anamnesa'].'</td>
										<td>'.$data['rdokter'].	'</td>
										<td>'.$data['rperawat'].'</td>
										<td style="text-align:center;"><a href="#riwigd" data-toggle="modal" onClick="detailOverIGD(&quot;'.$data['id'].'&quot;)><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>
									</tr>
								';
							}
						?>
					</tbody>
				</table>												
			</div>
			<div class="modal fade" id="riwigd" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="riwkondok1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Penanganan IGD</h3>
				   			</div>
							<div class="modal-body" style="padding-left:80px;">

				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="imod_date" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
									</div>
			        			</div>	
			        			<div class="form-group">
									<label class="control-label col-md-4">Anamnesa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="imod_anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
									</div>
								</div>

								<fieldset class="fsStyle">
									<legend>
						                Tanda Vital
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Tekanan Darah</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_tekanandarah" name="takanandarah" placeholder="Tekanan Darah" readonly>
										</div>
										<label class="control-label col-md-2">mmHg</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Temperatur</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_temperatur" name="temperatur" placeholder="Temperatur" readonly>
										</div>
										<label class="control-label col-md-2">&deg;C</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Nadi</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_nadi" name="nadi" placeholder="Nadi" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Pernapasan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Berat Badan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="imod_berat" name="berat" placeholder="Berat Badan" readonly>
										</div>
										<label class="control-label col-md-2">Kg</label>
									</div>
						  		</fieldset>

						  		<fieldset class="fsStyle">
									<legend>
						                Pemeriksaan Fisik
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4">Kepala & Leher</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_kepala" name="kepala" readonly placeholder="Hasil Pemeriksaan" ></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Thorax & ABD</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_thorax" name="thorax" readonly placeholder="Hasil Pemeriksaan" ></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Extremitas</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_ex" name="ex" readonly placeholder="Hasil Pemeriksaan" ></textarea>
										</div>
									</div>
								</fieldset>

						  		<fieldset class="fsStyle">
									<legend>
						                Diagnosa & Terapi
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Dokter Pemeriksa</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="imod_dokter" placeholder="Search Dokter" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Diagnosa Utama</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_utama" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_utama" placeholder=" Diagnosa" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Diagnosa Sekunder</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_sek1" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_sek1" placeholder=" Diagnosa" readonly>
										</div>
										<label class="control-label">1</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4"></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_sek2" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_sek2" placeholder=" Diagnosa" readonly>
										</div>
										<label class="control-label">2</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4"></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_sek3" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_sek3" placeholder=" Diagnosa" readonly>
										</div>
										<label class="control-label">3</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4"></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="imod_kode_sek4" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="imod_nama_sek4" placeholder=" Diagnosa" readonly>
										</div>
										<label class="control-label">4</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Detail Diagnosa</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_detailDiagnosa" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Terapi</label>
										<div class="col-md-7">
											<textarea class="form-control" id="imod_terapi" name="terapi" placeholder="Terapi" readonly></textarea>
										</div>
									</div>
									<!-- <div class="form-group">
										<label class="control-label col-md-4" >Alergi</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="alergi" name="alergi" placeholder="Alergi" readonly>
										</div>
									</div> -->
						  		</fieldset>
				        	</div>
			        		
			        		<div class="modal-footer">
			        			<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						    </div>
						</div>
					</div>
				</form>
			</div>
	        <br>
	
			<div class="dropdown" id="rwp3">
            	<div id="titleInformasi">Riwayat Perawatan</div>
            	<div class="btnBawah" id="btnBawahRiwayat"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div class="portlet-body" id="tblrwp3" style="margin: 20px 10px 0px 10px">
            	
            	<table class="table table-striped table-bordered table-hover table-responsive tableDT">
					<thead>
						<tr class="info" style="text_align: center;">
							<th style="width:10px;">No.</th>
							<th>Tanggal</th>
							<th>Waktu</th>
							<th>Dokter Visit</th>
							<th>Diagnosa Utama</th>
							<th>Diagnosa Sekunder</th>
							<th>Perkembangan Penyakit</th>
							<th style="width:20px;">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td style="text-align:center">12 Desember 2012</td>
							<td>12:12</td>
							<td>Jems</td>
							<td>Bebas</td>
							<td>Bebas</td>
							<td>Bebas</td>
							<td style="text-align:center;"><a href="#riwperawatan" data-toggle="modal"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>
						</tr>
					</tbody>
				</table>												
			</div>
			<div class="modal fade" id="riwperawatan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="riwkondok">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Perawatan</h3>
				   			</div>
							<div class="modal-body" style="padding-left:80px;">

				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
									</div>
			        			</div>	

			        			<div class="form-group">
									<label class="control-label col-md-4">Dokter Visit</label>
									<div class="col-md-5">
										<input type="text" class="form-control" id="dokterv" name="dokterv" placeholder="Dokter" readonly></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4">Petugas</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="petugas" name="petugas" placeholder="Petugas" readonly></textarea>
									</div>
								</div>

			        			<div class="form-group">
									<label class="control-label col-md-4">Anamnesa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Utama</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="kode_utama" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" >Diagnosa Sekunder</label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="rawatkode_sek1" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="label-cntrol">1</label>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="rawatkode_se2" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="label-cntrol">2</label>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="rawatkode_sek3" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="label-cntrol">3</label>
								</div>

								<div class="form-group">
									<label class="control-label col-md-4" ></label>
									<div class="col-md-3">
										<input type="text" class="form-control" id="rawatkode_sek4" placeholder="Kode" readonly>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Keterangan" readonly>
									</div>
									<label class="label-cntrol">4</label>
								</div>


								<div class="form-group">
									<label class="control-label col-md-4">Perbangan Penyakit</label>
									<div class="col-md-7">
										<textarea class="form-control" id="perkembangan" name="perkembangan" placeholder="Perkembangan Penyakit" readonly></textarea>
									</div>
								</div>

								<fieldset class="fsStyle">
									<legend>
						                Tanda Vital
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Tekanan Darah</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="tekanandarah" name="takanandarah" placeholder="Tekanan Darah" readonly>
										</div>
										<label class="control-label col-md-2">mmHg</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Temperatur</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="temperatur" name="temperatur" placeholder="Temperatur" readonly>
										</div>
										<label class="control-label col-md-2">&deg;C</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Nadi</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="nadi" name="nadi" placeholder="Nadi" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Pernapasan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Berat Badan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="berat" name="berat" placeholder="Berat Badan" readonly>
										</div>
										<label class="control-label col-md-2">Kg</label>
									</div>
						  		</fieldset>

				        	</div>
			        		
			        		<div class="modal-footer">
			        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						    </div>
						</div>
					</div>
				</form>
			</div>
			<br>
        </div>
        <div class="tab-pane" id="resume">
        	<div class="dropdown">
	            <div id="titleInformasi">Resume Pulang Pasien <span style="color:red; margin-left:30px;font-style:italic;">WAJIB DIISI!</span> </div>
	            <!-- <div class="btnBawah" id="btnBawahResumePulang"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div>  -->
            </div>

            <div class="informasi" id="infoResumePulang">
            	<form class="form-horizontal" role="form" id="submit_resume">
            		<div class="form-group">
						<label class="control-label col-md-3">Waktu Keluar <span class="required">* </span></label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="waktuPulang" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
							</div>
						</div>
					</div>	
	
					<div class="form-group">
						<label class="control-label col-md-3" >Alasan Keluar<span class="required">* </span>
						</label>
						<div class="col-md-3">			
							<select class="form-control select" name="alasanKeluarPasien" id="alasanKeluarPasien">
								<option value="" selected>Pilih</option>									
								<option value="Pasien Dipulangkan">Pasien Dipulangkan</option>									
								<option value="Pasien Dipindahkan">Pasien Pindah Poli Lain</option>
								<option value="Atas Permintaan Sendiri">Atas Permintaan Sendiri</option>
								<option value="Rujuk Rumah Sakit Lain"  >Rujuk ke Rumah Sakit Lain</option>
								<option value="Pasien Meninggal" >Pasien Meninggal</option>
								<option value="Rujuk Rawat Inap" >Rujuk Rawat Inap</option>
								<option value="Rujuk IGD" >Rujuk IGD</option>
							</select>
						</div>							
					</div>	
					
					<div class="form-group" id="alasanPlg">
						<label class="control-label col-md-3" >Alasan Pulang
						</label>
						<div class="col-md-4">			
							<textarea class="form-control" rows="3" id="alasanPulang"></textarea>
					 	</div>
					</div>

					<div class="form-group" id="alasanPindah">
						<label class="control-label col-md-3" >Alasan Pindah
						</label>
						<div class="col-md-4">			
							<textarea class="form-control" rows="3" id="alasanPindah"></textarea>
					 	</div>
					</div>

					<div class="form-group" id="dokter_resume">
						<label class="control-label col-md-3">Dokter</label>
						<div class="col-md-3">
							<input type="hidden" id="iddokter_res">
							<input type="text" class="form-control" id="namadokter_res" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter">
						</div>
					</div>

					<div class="form-group" id="poli_res">
							<label class="control-label col-md-3">Poliklinik</label>
							<div class="col-md-5">
								<select class="form-control select" name="poli" id="poli_resume">
									<option value="" selected>Pilih Poliklinik</option>
									<?php foreach( $poliklinik as $poli ) { ?>
										<option value="<?php echo $poli['dept_id']; ?>" >
											<?php echo $poli['nama_dept']; ?>
										</option>
									<?php } ?>
								</select>												
							</div>
						</div>

					
					<div class="form-group" id="isiRujuk">
						<label class="control-label col-md-3" >Isian Rumah Sakit Rujukan<span class="required">* </span>
						</label>
						<div class="col-md-4">		
							<input type="hidden" id="id_rsrujuk">
							<input type="text" class="form-control" id="isianRSRujuk" name="isianRSRujuk" placeholder="Rumah Sakit Rujukan">
					 	</div>
					</div>				

					<div class="form-group" id="semuaPoli">
						<label class="control-label col-md-3" >Semua Poli<span class="required">* </span>
						</label>
						<div class="col-md-4">			
							<select class="form-control select" name="allPoli" id="allPoli">
								<option value="Poliklinik Umum" selected>Umum</option>
								<option value="Poliklinik Gigi">Gigi</option>
								<option value="Poliklinik Bedah"  >Bedah</option>
							</select>		
						</div>							
					</div>

					<div class="form-group" id="formRekDokter">
						<label class="control-label col-md-3" >Rekomendasi Dokter<span class="required">* </span>
						</label>
						<div class="col-md-4">			
							<select class="form-control select" name="rekDokter" id="rekDokter">
								<option value="Arya" selected>Arya</option>
								<option value="Breki">Breki</option>
								<option value="Jems">Jems</option>
							</select>		
						</div>							
					</div>
					
					<div class="form-group" id="formRekUnit">
						<label class="control-label col-md-3" >Rekomendasi Unit<span class="required">* </span>
						</label>
						<div class="col-md-4">			
							<select class="form-control select" name="rekUnit" id="rekUnit">
								<option value="Unit A" selected>Unit A</option>
								<option value="Unit B">Unit B</option>
								<option value="Unit C">Unit C</option>
							</select>		
					 	</div>
					</div>

					<div class="form-group" id="detPasienMeninggal">
						<label class="control-label col-md-3" >Detail Pasien Meninggal<span class="required">* </span>
						</label>
						<div class="col-md-4">			
							<select class="form-control select" name="detPasDie" id="detPasDie">
								<option value="" selected>Pilih Detail Pasien Meninggal</option>
								<option value="Sebelum Dirawat">Meninggal sebelum dirawat</option>
								<option value="Sesudah Dirawat > 48 Jam">Meninggal sesudah dirawat > 48 jam</option>
								<option value="Sesudah Dirawat < 48 Jam">Meninggal sesudah dirawat < 48 jam</option>
							</select>
					 	</div>							
					</div>

					<div class="form-group" id="pasienMeninggal">
						<label class="control-label col-md-3">Waktu Pasien Meninggal</label>
						<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" style="cursor:pointer;background-color:white" id="waktuMeninggal" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy hh:ii" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
							</div>
						</div>
					</div>
					
					<div class="form-group" id="ketMati">
						<label class="control-label col-md-3"> Keterangan Kematian
						</label>
						<div class="col-md-4">			
						<textarea class="form-control" rows="3" id="keteranganMati"></textarea>
					 	</div>
					</div>

					<div class="form-group">
						<div class="form-inline">
							<label class="control-label col-md-3">Jenis Kasus <span class="required">* </span></label>
							<div class="col-md-4">
								<div class="radio-list">
									<label>
										<input type="radio"  name="jkasus" id="klama" value="1"/><span style="margin-left:15px;font-size:10pt;font-width:normal">Kasus Lama </span> 
									</label>
									<label style="margin-left:40px;">
										<input type="radio"  name="jkasus" id="kbaru" value="0"/><span style="margin-left:15px;font-size:10pt;font-width:normal">Kasus Baru </span>
									</label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group" style="margin-top:10px">
						<div class="form-inline">
							<label class="control-label col-md-3">Jenis Kunjungan <span class="required">* </span></label>
							<div class="col-md-7">
								<div class="radio-list">
									<label>
										<input type="radio"  name="jnsKunjungan" id="knjunganLama" value="1" data-title="Lama"/><span style="margin-left:10px">Kunjungan Lama</span> 
									</label>
									<label style="margin-left: 18px">
										<input type="radio"  name="jnsKunjungan" id="knjunganBaru" value="0" data-title="Baru"/><span style="margin-left:10px">Kunjungan Baru </span>
									</label>
								</div>
							</div>
						</div>
					</div>


					<br>
					<hr style="margin-bottom:-17px; margin-left:-45px; margin-right:22px">
					<div style="margin-left:80%">
						<span style="padding:0px 10px 0px 10px;">
							<button type="reset" class="btn btn-warning">RESET</button> &nbsp;
							<button type="submit" class="btn btn-success">SIMPAN</button> 
						</span>
					</div>
				</form>	
				<br>
            </div>
            <br>
        </div>

		<div class="modal fade" id="searchDiagnosa" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Diagnosa</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">
							<form method="POST" id="search_diagnosa">	
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="katakunci_diag" placeholder="Kata kunci"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div>	
							</form>
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" style="table-layout:fixed" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th width="25%;">Kode Diagnosa</th>
										<th>Keterangan</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_diagnosa">
									<tr><td colspan="3" style="text-align:center;">Cari Data Diagnosa</td></tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

		
		<div class="modal fade" id="searchPerawat" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Perawat</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">	
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="inputPerawat" placeholder="Nama perawat"/>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-info">Cari</button>
							</div>	
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<td>Nama Perawat</td>
										<td width="10%">Pilih</td>
									</tr>
								</thead>
								<tbody id="tbody_petugas">
									<?php
										if(!empty($dokter)){
											foreach ($dokter as $d) {
												echo "<tr>";
													echo "<td>".$d['nama_petugas']."</td>";
													echo "<td style='text-align:center'><i class='glyphicon glyphicon-check' style='cursor:pointer;' onclick='getDokter(&quot;".$d['petugas_id']."&quot; , &quot;".$d['nama_petugas']."&quot;)'></i></td>";
												echo"</tr>";
											}
										}else{
											echo "<tr>";
												echo "<td colspan='2'>Data Kosong</td>";
											echo"</tr>";
										}
									?>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>


		<div class="modal fade" id="searchDokter" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        				<h3 class="modal-title" id="myModalLabel">Pilih Dokter</h3>
        			</div>
        			<div class="modal-body">
						<div class="form-group">
						<form method="POST" id="search_dokter">
							<div class="col-md-5">
								<input type="text" class="form-control" name="katakunci" id="inputDokter" placeholder="Nama dokter"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-info">Cari</button>
							</div>	
						</form>
						</div>	
						<br>	
						<div style="margin-left:5px; margin-right:5px;"><hr></div>
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover" id="tabelSearchDiagnosa">
								<thead>
									<tr class="info">
										<th>Nama Dokter</th>
										<th width="10%">Pilih</th>
									</tr>
								</thead>
								<tbody id="tbody_dokter">
									<?php
										
									?>
									<tr>
										<td colspan='2' style="text-align:center;">Cari Data Dokter</td>
									</tr>
								</tbody>
							</table>												
						</div>
        			</div>
        			<div class="modal-footer">
 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
			      	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="riwayat_over" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" role="form" method="POST" id="riwkondok">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Detail Riwayat Klinik</h3>
				   			</div>
							<div class="modal-body" style="padding-left:80px;">

				   				<div class="form-group">
									<label class="control-label col-md-4">Waktu Tindakan</label>
									<div class="col-md-5">	
										<input type="text" id="detail_waktu" class="form-control" readonly placeholder="<?php echo date("d/m/Y H:i:s");?>"/>
									</div>
			        			</div>	
			        			<div class="form-group">
									<label class="control-label col-md-4">Anamnesa</label>
									<div class="col-md-7">
										<textarea class="form-control" id="detail_anamnesa" name="anamnesa" placeholder="Anamnesa" readonly></textarea>
									</div>
								</div>

								<fieldset class="fsStyle">
									<legend>
						                Tanda Vital
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Tekanan Darah</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_tekanan" name="takanandarah" placeholder="Tekanan Darah" readonly>
										</div>
										<label class="control-label col-md-2">mmHg</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Temperatur</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_temperatur" name="temperatur" placeholder="Temperatur" readonly>
										</div>
										<label class="control-label col-md-2">&deg;C</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Nadi</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_nadi" name="nadi" placeholder="Nadi" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Pernapasan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_pernapasan" name="pernapasan" placeholder="Pernapasan" readonly>
										</div>
										<label class="control-label col-md-2">x/menit</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Berat Badan</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="detail_berat" name="berat" placeholder="Berat Badan" readonly>
										</div>
										<label class="control-label col-md-2">Kg</label>
									</div>
						  		</fieldset>

						  		<fieldset class="fsStyle">
									<legend>
						                Diagnosa & Terapi
									</legend>
									<div class="form-group">
										<label class="control-label col-md-4" >Dokter Pemeriksa</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="detail_dokter" placeholder="Search Dokter" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Diagnosa</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="detail_kutama" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="detail_dutama" placeholder="Keterangan" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Diagnosa Sekunder</label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_sek1" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="nama_sek1" placeholder="Diagnosa" readonly>
										</div>
										<label class="control-label">1</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" ></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_sek2" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="nama_sek2" placeholder="Diagnosa" readonly>
										</div>
										<label class="control-label">2</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" ></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_sek3" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="nama_sek3" placeholder="Diagnosa" readonly>
										</div>
										<label  class="control-label">3</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" ></label>
										<div class="col-md-3">
											<input type="text" class="form-control" id="kode_sek4" placeholder="Kode" readonly>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" id="nama_sek4" placeholder="Diagnosa" readonly>
										</div>
										<label class="control-label">4</label>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Detail Diagnosa</label>
										<div class="col-md-7">
											<textarea class="form-control" id="detail_detail" name="detailDiagnosa" placeholder="Detail Diagnosa" readonly></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Terapi</label>
										<div class="col-md-7">
											<textarea class="form-control" id="detail_terapi" name="terapi" placeholder="Terapi" readonly></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4" >Alergi</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="detail_alergi" name="alergi" placeholder="Alergi" readonly>
										</div>
									</div>
						  		</fieldset>
				        	</div>
			        		
			        		<div class="modal-footer">
			        			<input type="hidden" id="visit_id" value="<?php echo $this->session->userdata('visit_id'); ?>">
			 			     	<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
						    </div>
						</div>
					</div>
				</form>
			</div>
	        <br>
		</div>


    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//$('#v_id').val(localStorage.getItem('visit_id'));
		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
				success:function(data){
					var autodata = [];
					var iddata = [];

					for(var i = 0; i<data.length; i++){
						autodata.push(data[i]['nama_petugas']);
						iddata.push(data[i]['petugas_id']);
					}
					console.log(autodata);

					$input.typeahead({source:autodata, 
			            autoSelect: true}); 

					$input.change(function() {
					    var current = $input.typeahead("getActive");
					    var index = autodata.indexOf(current);

					    $('#paramedis_id').val(iddata[index]);
					    
					    if (current) {
					        // Some item from your model is active!
					        if (current.name == $input.val()) {
					            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
					        } else {
					            // This means it is only a partial match, you can either add a new item 
					            // or take the active if you don't want new items
					        }
					    } else {
					        // Nothing is active so it is a new value (or maybe empty value)
					    }
					});
				}
			});
		});
		//----------------- overview tab -----------------//
		$('#submitresep').submit(function (e) {
			e.preventDefault();
			var item = {};
		    var number = 1;
		    item[number] = {};

			item[number]['dokter'] = $('#resepdokter').val();
			item[number]['sub_visit'] = $('#rj_id').val();
			item[number]['visit_id'] = <?php echo $visit_id; ?>;
			item[number]['resep'] = $('#deskripsiResep').val();
			item[number]['tanggal'] = $('#tgl_resep').val();

			$.ajax({
				type: "POST",
				data : item,
				url: "<?php echo base_url()?>rawatjalan/daftarpasien/save_visit_resep",
				success: function (data) {
					console.log(data);

					if($('#checktable').val()=='null'){
						$('#tbody_resep').empty() ;
						$('#checktable').val('done')
					}

					$('#resepdokter').val('');
					$('#resep').val('');
						jQuery('#tabelresepbersalin tbody:first').append(
							"<tr>"+
							"<td>"+data['nama_petugas']+"</td>"+
							"<td>"+data['tanggal']+"</td>"+
							"<td>"+data['resep']+"</td>"+
							"<td>"+data['status_bayar']+"</td>"+
							"<td>"+data['status_ambil']+"</td>"+
							'<td style="text-align:center"><a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value="'+data['resep_id']+'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a></td>'+
							'<td class="resep_id" style="display:none" >'+data['resep_id']+'</td>'+
							"</tr>"
						);

					$('#namadokter').val('');
					$('#deskripsiResep').val('');

				},
				error: function (data) {
					alert('gagal');
					
				}
			});
		});
	
		var tindakan_temp = [];

		var autodata = [];
		var iddata = [];
		$('#namatindakan').focus(function(){
			var $input = $('#namatindakan');
			
			if(autodata.length == 0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>kamaroperasi/invoicenonbpjs/get_master_tindakan',
					success:function(data){

						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_tindakan']);
							iddata.push(data[i]['tindakan_id']);
						}
					}
				});
			}

			$input.typeahead({source:autodata, 
	        autoSelect: true}); 

			$input.change(function() {
			    var current = $input.typeahead("getActive");
			    var index = autodata.indexOf(current);

			    $('#idtindakan_klinik').val(iddata[index]);			
			    $('#namatindakanarea').val(autodata[index]);	

			    $('#kelas_klinik').prop('disabled', false);    

			    if (current) {
			        // Some item from your model is active!
			        if (current.name == $input.val()) {
			            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
			        } else {
			            // This means it is only a partial match, you can either add a new item 
			            // or take the active if you don't want new items
			        }
			    } else {
			        // Nothing is active so it is a new value (or maybe empty value)
			    }
			});

		});
		
		$('#kelas_klinik').prop('disabled', true);

		$('#kelas_klinik').change(function(){
			var item = {};
			item['kelas'] = $(this).val();
			item['nama'] = $('#idtindakan_klinik').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url() ?>kamaroperasi/invoicenonbpjs/get_tariftindakan',
				success:function(data){
					var tarif = 0;
					var lingkup = $('#lingkup').val();
					var persen = 0;

					$('#idtindakdetail').val(data['detail_id']);

					tarif = (Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))+((Number(data['js'])+Number(data['jp'])+Number(data['bakhp']))*persen);
					
					$('#js_klinik').val(data['js']);
					$('#jp_klinik').val(data['jp']);
					$('#bakhp_klinik').val(data['bakhp']);
					$('#tarif').val(tarif);
					$('#jumlah').val(tarif);
				}
			});
		});

		$('#onfaktur').keyup(function(){
			var onfaktur = $('#onfaktur').val();
			if(onfaktur == "")
				onfaktur = 0;
			var tarif = $('#tarif').val();
			var jumlah = parseInt(onfaktur)+parseInt(tarif);
			$('#jumlah').val(jumlah);
		});

		$('#submitTindakan').submit(function (e) {
			e.preventDefault();
			var item = {};
		    var number = 1;
		    item[number] = {};

		    item[number]['waktu_tindakan'] = $('#tin_date').val();
			item[number]['tindakan_id'] = $('#idtindakan_klinik').val();
			item[number]['visit_id'] = $('#v_id').val();//localStorage.getItem('visit_id');//$('#visit_id').val();	
			item[number]['on_faktur'] = $('#onfaktur').val();
			item[number]['paramedis'] = $('#paramedis_id').val();
			item[number]['tarif'] = $('#tarif').val();
			item[number]['jumlah'] = $('#jumlah').val();
			item[number]['sub_visit'] = $('#rj_id').val();
			item[number]['js'] = $('#js_klinik').val();
			item[number]['jp'] = $('#jp_klinik').val();
			item[number]['bakhp'] = $('#bakhp_klinik').val();
			item[number]['dept_id'] = $('#dept_id').val();
			item[number]['paramedis_lain'] = $('#paramedis_lain').val();

			//item[number]['kat_id'] = $('#kategori').find('option:selected').val();

			// console.log(item);
			// return false;
			$.ajax({
				type: "POST",
				data : item,
				url: "<?php echo base_url()?>rawatjalan/daftarpasien/save_tindakan",
				success: function (data) {
					console.log(data);

					//$('#namaTindakan').find('option:selected').val();
					//$('#visit_id').val('');
					$('#kelas_klinik option[value=""]').attr('selected','selected');
					$('#namatindakan').val('');
					$('#tarif').val('');
					$('#onfaktur').val('');
					$('#paramedis').val('');
					$('#tarif').val('');
					$('#jumlah').val('');
					$('#kelas_klinik').prop('disabled', true);

					//$('#kategori').find('option:selected').val();
					var no = $('#jmlh_table').val();
					if(no==0){
						$('#tbody_tindakan').empty();
					}
					no++;

					var t = $('#tableCare').DataTable();

					var action = "<center><a class='hapusTindakan' style='cursor:pointer;'><input type='hidden' class='getid' value='"+data['care_id']+"''><i class='glyphicon glyphicon-trash'></i></a></center>";

					t.row.add([
						no,
						data['waktu_tindakan'],
						data['nama_tindakan'],
						data['js'],
						data['jp'],
						data['bakhp'],
						data['on_faktur'],
						data['nama_petugas'],
						data['jumlah'],
						action,
						no
					]).draw();

					$('#jmlh_table').val(no);
					// $('#tbody_tindakan').append(
					// 	"<tr>"+
					// 	"<td>"+no+"</td>"+
					// 	"<td>"+data['waktu_tindakan']+"</td>"+
					// 	"<td>"+data['nama_tindakan']+"</td>"+
					// 	"<td>"+data['js']+"</td>"+
					// 	"<td>"+data['jp']+"</td>"+
					// 	"<td>"+data['bakhp']+"</td>"+
					// 	"<td>"+data['on_faktur']+"</td>"+
					// 	"<td>"+data['nama_petugas']+"</td>"+
					// 	"<td>"+data['jumlah']+"</td>"+
					// 	"<td style='text-align:center'><a class='hapusTindakan' style='cursor:pointer;'><input type='hidden' class='getid' value='"+data['care_id']+"''><i class='glyphicon glyphicon-trash'></i></a></td>"+
					// 	"</tr>"
					// );

				},
				error: function (data) {
					console.log(data);
					alert('gagal');
					
				}
			});
			$('#tambahTindakan').modal('hide');
		});

		$(document).on('click','.hapusTindakan',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#v_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>rawatjalan/daftarpasien/hapus_tindakan/"+id+"/"+v_id,
				success:function(data){
					console.log(data);

					$('#tbody_tindakan').empty();

					var t = $('#tableCare').DataTable();

					$('#jmlh_table').val(data.length);

					t.clear().draw();

					if(data.length>0){
						for(var i = 0; i<data.length; i++){

							var action = "<center><a class='hapusTindakan' style='cursor:pointer;'><input type='hidden' class='getid' value='"+data[i]['care_id']+"''><i class='glyphicon glyphicon-trash'></i></a></center>";

							t.row.add([
								(i+1),
								data[i]['waktu_tindakan'],
								data[i]['nama_tindakan'],
								data[i]['js'],
								data[i]['jp'],
								data[i]['bakhp'],
								data[i]['on_faktur'],
								data[i]['nama_petugas'],
								data[i]['jumlah'],
								action,
								i
							]).draw();
						}
					}
				},
				error:function(data){
					console.log(data);
				}	
			});
		});
	
		$("#submitOver").submit(function(e){
			e.preventDefault();
			// alert('ok');
			// return false;
			var item = {};
		    var number = 1;
		    item[number] = {};
			item[number]['waktu'] = $('#inputdate').val();
			item[number]['rj_id'] = $('#rj_id').val();
			item[number]['visit_id'] = $('#v_id').val();
			item[number]['tekanan_darah'] = $("#tekanandarahOver").val();
			item[number]['anamnesa'] = $('#anamnesaOver').val();
			item[number]['temperatur'] = $('#tempOver').val();
			item[number]['nadi'] = $('#nadiOver').val();
			item[number]['pernapasan'] = $('#pernapasanOver').val();
			item[number]['berat_badan'] = $('#beratOver').val();
			item[number]['dokter'] = $('#id_dokterOver').val();
			item[number]['diagnosa1'] = $('#k_utama_over').val();
			item[number]['diagnosa2'] = $('#k_sek_over').val();
			item[number]['diagnosa3'] = $('#k_sek_over2').val();
			item[number]['diagnosa4'] = $('#k_sek_over3').val();
			item[number]['diagnosa5'] = $('#k_sek_over4').val();
			item[number]['detail_diagnosa'] = $('#detail_over').val();
			item[number]['terapi'] = $('#terapi_over').val();
			item[number]['alergi'] = $('#alergi_over').val();

			console.log(item);
			save_overview(item);

			return false;
		});
		
		var diagnosa = 0;
		$('.d1').click(function(){
			diagnosa = 1;
		});

		$('.d2').click(function(){
			diagnosa = 2;
		});

		$('.d3').click(function(){
			diagnosa = 3;
		});

		$('.d4').click(function(){
			diagnosa = 4;
		});

		$('.d5').click(function(){
			diagnosa = 5;		
		});

		$('#search_diagnosa').submit(function(e){
			e.preventDefault();
			var key = $('#katakunci_diag').val();

			$.ajax({
				type:'POST',
				url:'<?php echo base_url() ?>rawatjalan/daftarpasien/search_diagnosa/'+key,
				success:function(data){
					$('#tbody_diagnosa').empty();

					if(data.length>0){
						for(var i = 0; i<data.length;i++){
							$('#tbody_diagnosa').append(
								'<tr>'+
									'<td>'+data[i]['diagnosis_id']+'</td>'+
									'<td style="word-wrap: break-word;white-space: pre-wrap; ">'+data[i]['diagnosis_nama']+'</td>'+
									'<td style="text-align:center; cursor:pointer;"><a onclick="get_diagnosa(&quot;'+data[i]['diagnosis_id']+'&quot;, &quot;'+data[i]['diagnosis_nama']+'&quot;, &quot;'+diagnosa+'&quot;)"><i class="glyphicon glyphicon-check" data-toggle="tooltip" data-placement="top" title="Pilih"></i></a></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_diagnosa').append(
							'<tr><td colspan="3" style="text-align:center;">Data Diagnosa Tidak Ditemukan</td></tr>'
						);
					}
				}, error:function(data){
					console.log(data);
					alert('gagal');
				}
			});
		});
		//----------------- end overview tab -----------------//

		//----------------- Dokter Search pemberian resep ------------------//
		var d_click = 0;

		$('#nama_dOver').click(function(){
			d_click = 1;
		});

		$('#namadokter').click(function(){
			d_click = 2;
		});

		$('#k_gizi_nama').click(function(){
			d_click = 3;
		});

		$('#namadokter_res').click(function(){
			d_click = 4;
		});

		$('#tun_namadokter').click(function(){
			d_click = 5;
		});		

		$('#namadokter_o').click(function(){
			d_click = 10;
		});		

		$('#search_dokter').submit(function(event){
			var d_item = {};
			d_item['dokter'] = $('#inputDokter').val();
			event.preventDefault();

			$.ajax({
				type:'POST',
				data:d_item,
				url:"<?php echo base_url()?>rawatjalan/daftarpasien/search_dokter",
				success:function(data){
					console.log(data);
					$('#tbody_dokter').empty();
 					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							var nama = data[i]['nama_petugas'],
								id = data[i]['petugas_id'];

							$("#tbody_dokter").append(
								'<tr>'+
									'<td>'+nama+'</td>'+
									'<td style="text-align:center"><i class="glyphicon glyphicon-check" style="cursor:pointer;" onclick="getDokter(&quot;'+id+'&quot; , &quot;'+nama+'&quot;, &quot;'+d_click+'&quot;)"></i></td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_dokter').empty();
						$('#tbody_dokter').append(
							'<tr>'+
					 			'<td colspan="2"><center>Data Paket Tidak Ditemukan</center></td>'+
					 		'</tr>'
						);
					}
				},
				error:function(data){

				}
			});
		});

		$(document).on('click','.hapusresep',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#v_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>rawatjalan/daftarpasien/hapus_resep/"+id+"/"+v_id,
				success:function(data){
					console.log(data);

					$('#tbody_resep').empty();

					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							$('#tbody_resep').append(
								'<tr>'+
									'<td>'+data[i]['nama_petugas']+'</td>'+								
									'<td>'+data[i]['tanggal']+'</td>'+									
									'<td>'+data[i]['resep']+'</td>'+									
									'<td>-</td>'+					
									'<td>-</td>'+										
									'<td style="text-align:center">'+
									'<a style="cursor:pointer;" class="hapusresep"><input type="hidden" class="getid" value="'+data[i]['resep_id']+'"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
									'</td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_resep').append(
							'<tr><td colspan="6" style="text-align:center">Data Kosong</td></tr>'
						);
					}
				},
				error:function(data){
					console.log(data);
				}	
			});
		});

		//---------------- end pemberian resep ---------------//

		//---------------- order kamar operasi ---------------//
		var item_order = {};
		item_order[1] = {};
		$('#submit_order_operasi').submit(function(e){
			e.preventDefault();

			item_order[1]['visit_id'] = <?php echo $visit_id; ?>;
			item_order[1]['sub_visit'] = $('#rj_id').val();
			item_order[1]['dept_id'] = "POLI UMUM";
			item_order[1]['dept_tujuan'] = "KAMAR OPERASI"; 
			item_order[1]['pengirim'] = $('#iddokter_o').val();
			item_order[1]['alasan'] = $('#alasanoperasi').val();
			item_order[1]['jenis_operasi'] = $('#jnsOperasi').val();			
			item_order[1]['waktu_mulai'] = $('#wktpelaksanaan').val();

			$.ajax({
				type: "POST",
				data: item_order,
				url: "<?php echo base_url()?>rawatjalan/daftarpasien/save_order_operasi",
				success: function(data){
					console.log(data);
					var i = $('#jml_data').val();
					var t = $("#table_order").DataTable();	
					var action = '<center><i class="glyphicon glyphicon-trash hapusorder" data-toggle="tooltip" data-placement="top" title="Hapus" style="cursor:pointer;"><input type="hidden" class="getid" value="'+data['order_operasi_id']+'"></i></center>';					
					var tgl = changeDateTime(data['waktu_mulai']);

					i++;

					t.row.add([
						i,
						tgl,
						data['nama_petugas'],
						"Kamar Operasi",
						data['alasan'],
						action,
						i
					]).draw();

					$('#jml_data').val(i);
					$('#wktpelaksanaan').val('<?php echo date("d/m/Y H:i");?>');
					$('#alasanoperasi').val('');
					$('#jnsOperasi').val('');
					$("#namadokter_o").val('');
					myAlert('Data Berhasil Ditambahkan');
				},
				error: function(data){
					console.log(data);
					alert('gagal');
				}

			});
		});

		$(document).on('click','.hapusorder',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var sub_visit = $('#rj_id').val();
			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>rawatjalan/daftarpasien/hapus_order/"+id+"/"+sub_visit,
				success:function(data){
					var t = $("#table_order").DataTable();	
					var no=0;
					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						var action = '<center><i class="glyphicon glyphicon-trash hapusorder" data-toggle="tooltip" data-placement="top" title="Hapus" style="cursor:pointer;"><input type="hidden" class="getid" value="'+data[i]['order_operasi_id']+'"></i></center>';					
						var tgl = changeDateTime(data[i]['waktu_mulai']);
						no++;
						t.row.add([
							no,
							tgl,
							data[i]['nama_petugas'],
							"Kamar Operasi",
							data[i]['alasan'],
							action,
							i
						]).draw();
					}
					
					$('#jml_data').val(data.length);
				},
				error:function(data){
					console.log(data);
				}	
			});
		});

		//-------------- end order kamar operasi -------------//

		$("#namadokter").click(function(){
			$('#inputDokter').val("");
			$('#tbody_dokter').empty();
			$('#tbody_dokter').append(
				'<tr>'+
					'<td colspan="2" style="text-align:center;">Cari Data Dokter</td>'+
				'</tr>'
			);
		});

		$("#namadokter_o").click(function(){
			$('#inputDokter').val("");
			$('#tbody_dokter').empty();
			$('#tbody_dokter').append(
				'<tr>'+
					'<td colspan="2" style="text-align:center;">Cari Data Dokter</td>'+
				'</tr>'
			);
		});

		//-------------- Order Konsultasi Gizi -----------------//
		var g_item = {};
		g_item[1] = {};
		$('#submit_gizi').submit(function(e){
			e.preventDefault();
			g_item[1]['tanggal'] = $('#tanggal_gizi').val();
			g_item[1]['visit_id'] = $('#v_id').val();
			g_item[1]['sub_visit'] = $('#rj_id').val();
			g_item[1]['konsultan'] = $('#k_gizi_id').val();
			g_item[1]['kajian_gizi'] = $('#kajianGizi').val();
			g_item[1]['anamnesa_diet'] = $('#anamnesaDiet').val();
			//g_item[1]['data_lab'] = $('').val();
			g_item[1]['kajian_diet'] = $('#kajianDiet').val();
			g_item[1]['detail_menu'] =  $('#DetailMenu').val();

			$.ajax({
				type:'POST',
				data:g_item,
				url:'<?php echo base_url(); ?>rawatjalan/daftarpasien/save_gizi',
				success:function(data){
					console.log(data);
					var jml = parseInt($('#jml_gizi').val());
					alert("data Berhasil Ditambahkan ");	
					
					if(jml == 0){
						$('#tbody_gizi').empty();						
					}

					$('#tbody_gizi').append(
						'<tr>'+
							'<td>'+data['tanggal']+'</td>'+
							'<td>'+data['konsultan']+'</td>'+
							'<td>'+data['kajian_gizi']+'</td>'+										
							'<td>'+data['anamnesa_diet']+'</td>'+
							'<td>'+data['kajian_diet']+'</td>'+
							'<td>'+data['detail_menu']+'</td>'+
							'<td style="text-align:center">'+
								'<a style="cursor:pointer;" class="hapusgizi"><input type="hidden" class="getid" value="'+data['gizi_id']+'"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
								'<a href="#print"><i class="glyphicon glyphicon-print"  data-toggle="tooltip" data-placement="top" title="Print"></i></a>'+
							'</td>'+
						'</tr>'
					);

					$('#jml_gizi').val(jml++);

					$('#tanggal_gizi').val("");
					$('#k_gizi_id').val("");
					$('#k_gizi_nama').val("");
					$('#kajianGizi').val("");
					$('#anamnesaDiet').val("");
					$('#kajianDiet').val("");
					$('#DetailMenu').val("");
				},error:function(data){
					console.log(data);
				}
			});

		});
		
		$(document).on('click','.hapusgizi',function(){
			var id = $(this).children('.getid').val();
			var tr = $(this).parent().parent();
			var v_id = $('#v_id').val();

			$.ajax({
				type:"POST",
				url:"<?php echo base_url()?>rawatjalan/daftarpasien/hapus_gizi/"+id+"/"+v_id,
				success:function(data){
					console.log(data);

					$('#tbody_gizi').empty();

					if(data.length>0){
						for(var i = 0; i<data.length; i++){
							$('#tbody_gizi').append(
								'<tr>'+
									'<td>'+data[i]['tanggal']+'</td>'+
									'<td>'+data[i]['konsultan']+'</td>'+
									'<td>'+data[i]['kajian_gizi']+'</td>'+										
									'<td>'+data[i]['anamnesa_diet']+'</td>'+
									'<td>'+data[i]['kajian_diet']+'</td>'+
									'<td>'+data[i]['detail_menu']+'</td>'+
									'<td style="text-align:center">'+
										'<a style="cursor:pointer;" class="hapusgizi"><input type="hidden" class="getid" value="'+data[i]['gizi_id']+'"><i class="glyphicon glyphicon-trash"  data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>'+
										'<a href="#print"><i class="glyphicon glyphicon-print"  data-toggle="tooltip" data-placement="top" title="Print"></i></a>'+
									'</td>'+
								'</tr>'
							);
						}
					}else{
						$('#tbody_gizi').append(
							'<tr><td colspan="7" style="text-align:center">Data Kosong</td></tr>'
						);
					}
				},
				error:function(data){
					console.log(data);
				}	
			});
		});
		//--------------------- Resume Pulang -------------------//
		//submit resume pulang
		$('#dokter_resume').hide();
		$('#poli_res').hide();

		$('#submit_resume').submit(function(e){

			e.preventDefault();
			//get data
			var res_item = {};
			res_item[1] = {};
			var alasan = $('#alasanKeluarPasien').val();

			res_item[1]['rj_id'] = $('#rj_id').val();
			res_item[1]['visit_id'] = $('#v_id').val();
			res_item[1]['waktu_keluar'] = $('#waktuPulang').val();
			res_item[1]['alasan_keluar'] = alasan;

			if(alasan == 'Pasien Dipindahkan'){
				res_item[1]['detail_alasan_keluar'] = $('#alasanPindah').val();
				res_item[1]['unit_rujukan'] = $('#poli_resume').val();
				res_item[1]['dokter_rujuk'] = $('#iddokter_res').val();
			}
			else if(alasan == 'Pasien Dipulangkan'){
				res_item[1]['detail_alasan_keluar'] = $('#alasanPulang').val();
			}
			else if(alasan == 'Atas Permintaan Sendiri'){
				res_item[1]['detail_alasan_keluar'] = $('#alasanPulang').val();
			}
			else if(alasan == 'Rujuk Rumah Sakit Lain'){
				res_item[1]['rs_id'] = $('#id_rsrujuk').val();
			}
			else if(alasan == 'Rujuk IGD'){
				res_item[1]['unit_rujukan'] = "9";
				res_item[1]['dokter_rujuk'] = $('#iddokter_res').val();
			}
			else  if(alasan == 'Pasien Meninggal'){
				res_item[1]['detail_alasan_keluar'] = $('#detPasDie').val();
				res_item[1]['waktu_kematian'] = changeDateDB($('#waktuMeninggal').val());
				res_item[1]['keterangan'] = $('#keteranganMati').val();
			}else{
				res_item[1]['detail_alasan_keluar'] = "NULL";
			}

			res_item[1]['is_kasus_baru'] = $("input[name=jkasus]:checked").val();
			res_item[1]['is_kunjungan_baru'] = $("input[name=jnsKunjungan]:checked").val();

			console.log(res_item);
			
			//ajax save
			$.ajax({
				type:'POST',
				data:res_item,
				url:'<?php echo base_url(); ?>rawatjalan/daftarpasien/save_resume',
				success:function(data){
					myAlert('Data Berhasil Ditambahkan');
					window.location.href ="<?php echo base_url(); ?>rawatjalan/homerawatjalan";
					console.log(data);
					e.preventDefault();
				},error:function(data){
					myAlert('error');
					console.log(data);
				}
			});

		});

		$('#alasanKeluarPasien').change(function(){
			var data = $(this).val();

			if(data == "Pasien Dipindahkan"){
				$('#dokter_resume').show();
				$('#poli_res').show();
			}else if(data == "Rujuk IGD"){
				$('#dokter_resume').show();
			}else{
				$('#dokter_resume').hide();
				$('#poli_res').hide();
			}
		});

		//---------- riwayat kunjungan ------------//

		//------------ pemeriksaan penunjang here --------------//

		$('#submit_penunjang').submit(function(event){
			event.preventDefault();
			var item = {};
			item[1] = {};

			item[1]['visit_id'] = $('#v_id').val();
			item[1]['sub_visit'] = $('#rj_id').val();
			item[1]['dept_tujuan'] = $('#tun_tujuan').val();
			item[1]['pengirim'] = $('#tun_iddokter').val();
			item[1]['jenis_periksa'] = $('#tun_jenis').val();
			item[1]['waktu'] = $('#tun_date').val();
			item[1]['status'] = "BELUM";

			var pengirim = $('#tun_namadokter').val();
			var unit_asal = "POLI UMUM";
			var unit_tujuan = $('#tun_tujuan option:selected').html();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>rawatjalan/daftarpasien/save_penunjang',
				success:function(data){
					var t = $('#table_penunjang').DataTable();
					var no = $('#tun_jumlah').val();
					var detail = '<center><input type="hidden" class="idpenunjang" value="'+data['penunjang_id']+'"><a class="detailpenunjang" href="#viewRiwayat" data-toggle="modal" data-placement="top"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View Details"></i></a></center>';
					var tanggal = changeDate(data['waktu']);

					t.row.add([
						++no,
						tanggal,
						unit_tujuan,
						unit_asal,
						data['status'],
						detail,
						data['waktu']
					]).draw();

					$('#tun_jumlah').val(no);
					myAlert('Data Berhasil Ditambahkan');
					console.log(data);

				},error:function(data){
					console.log(data);
					myAlert('Data Gagal Ditambahkan');
				}
			});
		});

		//------------ end pemeriksaan penunjang here ----------//


	});

		function save_overview (item) {
			/*alert(item[1]['jenis']);
			return false;*/
			var petugas = $('#nama_dOver').val();

			$.ajax({
				type: "POST",
				data : item,
				url: "<?php echo base_url()?>rawatjalan/daftarpasien/save_overview",
				success: function (data) {				
					$('#inputdate').val("");
					$('#anamnesaOver').val("");
					$('#tempOver').val("");
					$('#nadiOver').val("");
					$('#pernapasanOver').val("");
					$('#beratOver').val("");
					$('#id_dokterOver').val("");
					$('#nama_dOver').val("");
					$('#k_utama_over').val("");
					$('#k_sek_over').val("");
					$('#k_sek_over2').val("");
					$('#k_sek_over3').val("");
					$('#k_sek_over4').val("");
					$('#terapi_over').val("");
					$('#alergi_over').val("");
					$('#detail_over').val("");
					$("#tekanandarahOver").val("");

					console.log(data);

					var jml = $('#jml_over').val();
					var no = parseInt(jml)+1;

					var t = $('#tableOverview').DataTable();
					var action = '<td style="text-align:center;"><a href="#riwayat_over" data-toggle="modal" onClick="detailOver(&quot;'+data['id']+'&quot;)"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Lihat detail"></i></a></td>';

					t.row.add([
						no,
						'Poli Umum',
						data['anamnesa'],
						petugas,
						action,
						jml
					]).draw();

					$('#jml_over').val(no);
					myAlert("Data Berhasil Ditambahkan");
				},
				error: function (data) {
					console.log(data);
					alert(data);
				}
			});

			return false;
		}

	function getDokter(id, nama, dokter){
		if(dokter == 1){
			$('#id_dokterOver').val(id);
			$('#nama_dOver').val(nama);
		}
		else if(dokter == 2){
			$("#resepdokter").val(id);
			$("#namadokter").val(nama);
		}
		else if(dokter == 3){
			$("#k_gizi_id").val(id);
			$("#k_gizi_nama").val(nama);	
		}else if(dokter == 4){
			$('#iddokter_res').val(id);
			$('#namadokter_res').val(nama);
		}else if(dokter==5){
			$('#tun_iddokter').val(id);
			$('#tun_namadokter').val(nama);
		}
		else{
			$("#namadokter_o").val(nama);
			$("#iddokter_o").val(id);
		}

		$("#searchDokter").modal('hide');
		$("#inputDokter").val("");
		$('#tbody_dokter').empty();
		$('#tbody_dokter').append('<tr><td colspan="3" style="text-align:center;">Cari Data Dokter</td></tr>');
	}

	function get_diagnosa(id, nama, diagnosa){
		if(diagnosa==1){
			$('#k_utama_over').val(id);
			$('#dnama1').val(nama);
		}else if(diagnosa==2){
			$('#k_sek_over').val(id);
			$('#dnama2').val(nama);
		}else if(diagnosa==3){
			$('#k_sek_over2').val(id);
			$('#dnama3').val(nama);
		}else if(diagnosa==4){
			$('#k_sek_over3').val(id);
			$('#dnama4').val(nama);
		}else if(diagnosa==5){
			$('#k_sek_over4').val(id);
			$('#dnama5').val(nama);
		}

		$('#tbody_diagnosa').empty();
		$('#tbody_diagnosa').append('<tr><td colspan="3" style="text-align:center;">Cari Data Diagnosa</td></tr>');
		$('#katakunci_diag').val("");
		$('#searchDiagnosa').modal('hide');
	}

	function detailOver(id){
		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>rawatjalan/daftarpasien/get_detail_over/'+id,
			success:function(data){
				console.log(data);
				$('#detail_waktu').val(data['waktu']);
				$('#detail_anamnesa').val(data['anamnesa']);
				$('#detail_tekanan').val(data['tekanan_darah']);
				$('#detail_temperatur').val(data['temperatur']);
				$('#detail_nadi').val(data['nadi']);
				$('#detail_pernapasan').val(data['pernapasan']);
				$('#detail_berat').val(data['berat_badan']);
				$('#detail_dokter').val(data['nama_petugas']);
				$('#detail_kutama').val(data['diagnosa1']);
				$('#detail_dutama').val(data['diagnosis_nama']);

				$('#kode_sek1').val(data['diagnosa2']);
				$('#nama_sek1').val("");
				$('#kode_sek2').val(data['diagnosa3']);
				$('#nama_sek2').val("");
				$('#kode_sek3').val(data['diagnosa4']);
				$('#nama_sek3').val("");
				$('#kode_sek4').val(data['diagnosa5']);
				$('#nama_sek4').val("");

				$('#detail_detail').val(data['detail_diagnosa']);
				$('#detail_terapi').val(data['terapi']);
				$('#detail_alergi').val(data['alergi']);

				dsek1 = data['diagnosa2'];
				dsek2 = data['diagnosa3'];
				dsek3 = data['diagnosa4'];
				dsek4 = data['diagnosa5'];

				if(dsek1 != null)
					get_diag_name(dsek1, 1);
				if(dsek2 != null)
					get_diag_name(dsek2, 2);
				if(dsek3 != null)
					get_diag_name(dsek3, 3);
				if(dsek4 != null)
					get_diag_name(dsek4, 4);
			}
		});
	}

	function get_diag_name(kode, i){
		$.ajax({
			type:'POST',
			url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_diag_name/'+kode,
			success:function(data){
				console.log(data);
					if(i==1)
						$('#nama_sek1').val(data['diagnosis_nama']);
					if(i==2)
						$('#nama_sek2').val(data['diagnosis_nama']);
					if(i==3)
						$('#nama_sek3').val(data['diagnosis_nama']);
					if(i==4)
						$('#nama_sek4').val(data['diagnosis_nama']);
			}
		});
	}

	function myAlert(text) {
		// simulate loading (for demo purposes only)
		// setTimeout( function() {
		var m = '<p>'+text+'</p>';
		// create the notification
		var notification = new NotificationFx({
			message : m,
			layout : 'growl',
			effect : 'genie',
			type : 'notice', // notice, warning or error
		});

		// show the notification
		notification.show();

		// }, 1200 );
		// disable the button (for demo purposes only)
	}


	function changeDate(tgl_lahir){
		var remove = tgl_lahir.split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0];

		return tgl;
	}

	function changeDateTime(tgl_lahir){
		var getdate = tgl_lahir.split(" ");
		var remove = getdate[0].split("-");
		var bulan;
		switch(remove[1]){
			case "01": bulan="January";break;
			case "02": bulan="February";break;
			case "03": bulan="March";break;
			case "04": bulan="April";break;
			case "05": bulan="May";break;
			case "06": bulan="June";break;
			case "07": bulan="Juli";break;
			case "08": bulan="August";break;
			case "09": bulan="September";break;
			case "10": bulan="October";break;
			case "11": bulan="November";break;
			case "12": bulan="December";break;
		}
		var tgl = remove[2]+" "+bulan+" "+remove[0]+" "+getdate[1];

		return tgl;
	}

	function changeDateDB(tgl_lahir){
		var getdate = tgl_lahir.split(" ");
		var remove = getdate[0].split("/");
		
		var tgl = remove[2]+"-"+remove[1]+"-"+remove[0]+" "+getdate[1]+":00";

		return tgl;
	}

	function detailOverIGD(id){
		var dsek1,
			dsek2,
			dsek3,
			dsek4;
		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>igd/igddetail/get_detail_over_igd/'+id,
			success:function(data){
				console.log(data);
				$('#imod_date').val(data['waktu']);
				$('#imod_anamnesa').val(data['anamnesa']);
				$('#imod_tekanandarah').val(data['tekanan_darah']);
				$('#imod_temperatur').val(data['temperatur']);
				$('#imod_nadi').val(data['nadi']);
				$('#imod_pernapasan').val(data['pernapasan']);
				$('#imod_berat').val(data['berat_badan']);
				$('#imod_kepala').val(data['kepala_leher']);
				$('#imod_thorax').val(data['thorax_abd']);
				$('#imod_ex').val(data['extrimitas']);
				$('#imod_dokter').val(data['nama_petugas']);
				$('#imod_kode_utama').val(data['diagnosa1']);
				$('#imod_kode_sek1').val(data['diagnosa2']);
				$('#imod_kode_sek2').val(data['diagnosa3']);
				$('#imod_kode_sek3').val(data['diagnosa4']);
				$('#imod_kode_sek4').val(data['diagnosa5']);
				$('#imod_nama_utama').val(data['diagnosis_nama']);
				$('#imod_detailDiagnosa').val(data['detail_diagnosa']);
				$('#imod_terapi').val(data['terapi']);
				$('#imod_nama_sek1').val("");
				$('#imod_nama_sek2').val("");
				$('#imod_nama_sek3').val("");
				$('#imod_nama_sek4').val("");

				dsek1 = data['diagnosa2'];
				dsek2 = data['diagnosa3'];
				dsek3 = data['diagnosa4'];
				dsek4 = data['diagnosa5'];

				if(dsek1 != null)
					get_diag_name(dsek1, 11);
				if(dsek2 != null)
					get_diag_name(dsek2, 12);
				if(dsek3 != null)
					get_diag_name(dsek3, 13);
				if(dsek4 != null)
					get_diag_name(dsek4, 14);
					
			}
		});
	}
</script>