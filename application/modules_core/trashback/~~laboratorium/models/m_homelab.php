<?php
	class m_homelab extends CI_Model {

		public function get_listaps(){
			$sql = "SELECT * FROM visit v, pasien p, visit_penunjang vp WHERE v.rm_id = p.rm_id AND vp.visit_id = v.visit_id AND v.status_visit = 'PENUNJANG' AND vp.status = 'BELUM' AND vp.dept_asal = (SELECT dept_id FROM master_dept WHERE nama_dept LIKE 'ADMISI' LIMIT 1)";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function search_listaps($keyword){
			$sql = "SELECT * FROM visit v, pasien p, visit_penunjang vp WHERE v.rm_id = p.rm_id AND vp.visit_id = v.visit_id AND v.status_visit = 'PENUNJANG' AND vp.status = 'BELUM' AND (p.nama LIKE '%$keyword%' OR p.rm_id LIKE '%$keyword%') AND vp.dept_asal = (SELECT dept_id FROM master_dept WHERE nama_dept LIKE 'ADMISI' LIMIT 1)";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function get_periksa($dept_id){
			$sql = "SELECT kategori FROM master_tindakan_penunjang WHERE dept_id = '$dept_id' GROUP BY kategori";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;	
		}

		public function get_dept_id($value){
			$query = $this->db->query("SELECT dept_id FROM master_dept WHERE nama_dept LIKE '$value'");
			$result = $query->row_array();
			return $result;	
		}

		public function get_listrujuk(){
			$sql = "SELECT d.nama_dept, vp.waktu, p.rm_id, p.nama, pt.nama_petugas, vp.jenis_periksa, vp.penunjang_id FROM master_dept d, visit_penunjang vp, visit v, pasien p, petugas pt WHERE d.dept_id = vp.dept_asal AND vp.visit_id = v.visit_id AND v.rm_id = p.rm_id AND vp.pengirim = pt.petugas_id AND vp.status = 'BELUM' AND vp.dept_asal <> (SELECT dept_id FROM master_dept WHERE nama_dept LIKE 'ADMISI') ORDER BY vp.waktu ASC";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;	
		}

		public function search_listrujuk($keyword){
			$sql = "SELECT d.nama_dept, vp.waktu, p.rm_id, p.nama, pt.nama_petugas, vp.jenis_periksa, vp.penunjang_id FROM master_dept d, visit_penunjang vp, visit v, pasien p, petugas pt WHERE d.dept_id = vp.dept_asal AND vp.visit_id = v.visit_id AND v.rm_id = p.rm_id AND vp.pengirim = pt.petugas_id AND vp.status = 'BELUM' AND vp.dept_asal <> (SELECT dept_id FROM master_dept WHERE nama_dept LIKE 'ADMISI') AND (p.nama LIKE '%$keyword%' OR p.rm_id LIKE '%$keyword%') ORDER BY vp.waktu ASC";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;	
		}

		public function get_alltindakan(){
			$sql = "SELECT kategori, nama_tindakan FROM master_tindakan_penunjang GROUP BY nama_tindakan ORDER BY kategori";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;	
		}

		public function get_tindakpenunjang($nama, $kelas){
			$sql = "SELECT tindakan_penunjang_id, js, jp, bakhp FROM master_tindakan_penunjang WHERE nama_tindakan LIKE '$nama' AND jenis_tarif = '$kelas' ";
			$query = $this->db->query($sql);
			$result = $query->row_array();
			return $result;
		}

		public function get_penunjang_detail_id($penunjang_id){
			$sql = "SELECT max(penunjang_detail_id) as penunjang_detail_id FROM visit_penunjang_detail WHERE penunjang_id = '$penunjang_id'";
			$query = $this->db->query($sql);

			$id = $query->row_array();
			if ($id['penunjang_detail_id']!=NULL) {
	            $id = intval(substr($id['penunjang_detail_id'], -3)) + 1;

	            if (strlen($id) == '1') {
	                $id = '00' . $id;
	            } elseif (strlen($id) == '2') {
	                $id = '0' . $id;
	            } else {
	                $id = $id;
	            }
	            return $penunjang_id . $id;
	        } else {
	            return $penunjang_id . '001';
	        }
		}

		public function save_tindakan($data){
			$insert = $this->db->insert('visit_penunjang_detail', $data);
			if ($insert) {
				return true;
			} else {
				return false;
			}
		}

		public function update_status($id, $data){
			$this->db->where('penunjang_id', $id);
			$update = $this->db->update('visit_penunjang', $data);

			if($update)
				return true;
			else
				return false;	
		}

		public function get_listperiksa(){
			$sql = "SELECT p.rm_id, v.visit_id, vp.penunjang_id, vp.waktu, p.nama, d.nama_dept, vp.pengirim, pt.nama_petugas, vp.status FROM pasien p LEFT JOIN visit v ON p.rm_id = v.rm_id
					LEFT JOIN visit_penunjang vp ON vp.visit_id = v.visit_id
					LEFT JOIN petugas pt ON vp.pengirim = pt.petugas_id
					LEFT JOIN master_dept d ON vp.dept_asal = d.dept_id
					WHERE vp.status = 'PROSES'UNION ALL 
					SELECT p.rm_id, v.visit_id, vp.penunjang_id, vp.waktu, p.nama, d.nama_dept, vp.pengirim, pt.nama_petugas, vp.status FROM pasien p LEFT JOIN visit v ON p.rm_id = v.rm_id
					LEFT JOIN visit_penunjang vp ON vp.visit_id = v.visit_id
					LEFT JOIN petugas pt ON vp.pengirim = pt.petugas_id
					LEFT JOIN master_dept d ON vp.dept_asal = d.dept_id
					WHERE vp.status = 'Selesai Sebagian' UNION ALL
					SELECT p.rm_id, v.visit_id, vp.penunjang_id, vp.waktu, p.nama, d.nama_dept, vp.pengirim, pt.nama_petugas, vp.status FROM pasien p LEFT JOIN visit v ON p.rm_id = v.rm_id
					LEFT JOIN visit_penunjang vp ON vp.visit_id = v.visit_id
					LEFT JOIN petugas pt ON vp.pengirim = pt.petugas_id
					LEFT JOIN master_dept d ON vp.dept_asal = d.dept_id
					WHERE vp.status = 'Selesai'";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function get_detailperiksa($id){
			$sql = "SELECT p.rm_id, p.jenis_kelamin, p.tanggal_lahir, p.alamat_skr, v.visit_id, vp.penunjang_id, vp.waktu, p.nama, d.nama_dept, vp.pengirim, pt.nama_petugas FROM pasien p LEFT JOIN visit v ON p.rm_id = v.rm_id
					LEFT JOIN visit_penunjang vp ON vp.visit_id = v.visit_id
					LEFT JOIN petugas pt ON vp.pengirim = pt.petugas_id
					LEFT JOIN master_dept d ON vp.dept_tujuan = d.dept_id
					WHERE vp.penunjang_id = '$id'";
			$query = $this->db->query($sql);
			$result = $query->row_array();
			return $result;
		}

		public function get_paramedis(){
			$sql = "SELECT * FROM petugas p, master_jabatan m WHERE p.jabatan_id = m.jabatan_id AND m.jenis = 'MEDIS'";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;	
		}

		public function get_allperiksa($id){
			$sql = "SELECT *, IFNULL(vp.hasil,'') 'hasilp', IFNULL(vp.nilai_normal,'') 'nilaip' , IFNULL(vp.keterangan,'') 'keteranganp', v.status as status_p FROM visit_penunjang v, master_tindakan_penunjang m, visit_penunjang_detail vp LEFT JOIN petugas pt ON pt.petugas_id = vp.pemeriksa WHERE vp.penunjang_id = v.penunjang_id AND m.tindakan_penunjang_id = vp.tindakan_penunjang_id AND v.penunjang_id = '$id'";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function update_detail($id, $data){
			$this->db->where('penunjang_detail_id', $id);
			$update = $this->db->update('visit_penunjang_detail', $data);

			if($update)
				return true;
			else
				return false;	
		}

		public function search_tagihan($search){
	    	// $sql = "SELECT *, t.cara_bayar as carapembayaran FROM tagihan t, pasien p, visit v, visit_penunjang vr, master_dept m 
	    	// 		WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND t.sub_visit = vr.penunjang_id AND vr.dept_tujuan = m.dept_id
	    	// 		AND vr.visit_id = v.visit_id AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '$search') AND m.jenis = 'PENUNJANG'
	    	// 		";
	    	$sql = "SELECT *, t.cara_bayar as carapembayaran FROM tagihan t, pasien p, visit v, master_dept m, (SELECT substr(sub_visit, 1, 2) as departemen, no_invoice FROM tagihan) sb
	    			WHERE t.visit_id = v.visit_id AND p.rm_id = v.rm_id AND sb.departemen = m.dept_id
	    			AND sb.no_invoice = t.no_invoice AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '%$search%')";
	    	$query = $this->db->query($sql);
	    	return $query->result_array();
    	}

    	public function search_hasil($search){
    		$sql = "SELECT p.rm_id, v.visit_id, vp.penunjang_id, vp.waktu, p.nama, d.nama_dept, vp.pengirim, pt.nama_petugas, vp.status FROM pasien p LEFT JOIN visit v ON p.rm_id = v.rm_id
					LEFT JOIN visit_penunjang vp ON vp.visit_id = v.visit_id
					LEFT JOIN petugas pt ON vp.pengirim = pt.petugas_id
					LEFT JOIN master_dept d ON vp.dept_asal = d.dept_id
					WHERE vp.status = 'PROSES' OR vp.status = 'Selesai Sebagian' OR vp.status = 'Selesai' AND (p.nama LIKE '%$search%' OR p.rm_id LIKE '%$search%')";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
    	}

    	public function get_jasapelayanan($dept_id){
			$sql = "SELECT v.waktu as tanggal, mt.nama_tindakan, t.cara_bayar, p.nama_petugas, vpd.jp FROM visit_penunjang v, visit_penunjang_detail vpd, petugas p, master_tindakan_penunjang mt, tagihan t, tagihan_penunjang tp WHERE vpd.pemeriksa = p.petugas_id AND v.penunjang_id = vpd.penunjang_id AND vpd.tindakan_penunjang_id = mt.tindakan_penunjang_id AND tp.penunjang_detail_id = vpd.penunjang_detail_id AND tp.no_invoice = t.no_invoice";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}

		public function search_jasapelayanan($dept, $mulai, $sampai, $carabayar, $paramedis){
		if($carabayar != "" && $paramedis != ""){
			$sql = "SELECT v.waktu as tanggal, mt.nama_tindakan, t.cara_bayar, p.nama_petugas, vpd.jp FROM visit_penunjang v, visit_penunjang_detail vpd, petugas p, master_tindakan_penunjang mt, tagihan t, tagihan_penunjang tp WHERE vpd.pemeriksa = p.petugas_id AND v.penunjang_id = vpd.penunjang_id AND vpd.tindakan_penunjang_id = mt.tindakan_penunjang_id AND tp.penunjang_detail_id = vpd.penunjang_detail_id AND tp.no_invoice = t.no_invoice AND v.dept_tujuan = $dept  AND vpd.pemeriksa = '$paramedis' AND t.cara_bayar = '$carabayar' AND v.waktu BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59'";
		}
		else if($carabayar == '' && $paramedis != ''){
			$sql = "SELECT v.waktu as tanggal, mt.nama_tindakan, t.cara_bayar, p.nama_petugas, vpd.jp FROM visit_penunjang v, visit_penunjang_detail vpd, petugas p, master_tindakan_penunjang mt, tagihan t, tagihan_penunjang tp WHERE vpd.pemeriksa = p.petugas_id AND v.penunjang_id = vpd.penunjang_id AND vpd.tindakan_penunjang_id = mt.tindakan_penunjang_id AND tp.penunjang_detail_id = vpd.penunjang_detail_id AND tp.no_invoice = t.no_invoice AND v.dept_tujuan = $dept  AND vpd.pemeriksa = '$paramedis' AND v.waktu BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59'";
		}else if($paramedis == '' && $carabayar != ''){
			$sql = "SELECT v.waktu as tanggal, mt.nama_tindakan, t.cara_bayar, p.nama_petugas, vpd.jp FROM visit_penunjang v, visit_penunjang_detail vpd, petugas p, master_tindakan_penunjang mt, tagihan t, tagihan_penunjang tp WHERE vpd.pemeriksa = p.petugas_id AND v.penunjang_id = vpd.penunjang_id AND vpd.tindakan_penunjang_id = mt.tindakan_penunjang_id AND tp.penunjang_detail_id = vpd.penunjang_detail_id AND tp.no_invoice = t.no_invoice AND v.dept_tujuan = $dept AND t.cara_bayar = '$carabayar' AND v.waktu BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59'";
		}else{
			$sql = "SELECT v.waktu as tanggal, mt.nama_tindakan, t.cara_bayar, p.nama_petugas, vpd.jp FROM visit_penunjang v, visit_penunjang_detail vpd, petugas p, master_tindakan_penunjang mt, tagihan t, tagihan_penunjang tp WHERE vpd.pemeriksa = p.petugas_id AND v.penunjang_id = vpd.penunjang_id AND vpd.tindakan_penunjang_id = mt.tindakan_penunjang_id AND tp.penunjang_detail_id = vpd.penunjang_detail_id AND tp.no_invoice = t.no_invoice AND v.dept_tujuan = $dept AND v.waktu BETWEEN '$mulai 00:00:00' AND '$sampai 23:59:59'";
		}

		// return $sql;
		return $this->db->query($sql)->result_array();			
	}

	public function get_kodetindakan($kode){
		$sql = "SELECT max(kode_tindakan) as kode FROM master_tindakan_penunjang WHERE substr(kode_tindakan,1,3) = '$kode'";
		$id = $this->db->query($sql)->row_array();
		if ($id['kode']!=NULL) {
            $id = intval(substr($id['kode'], -2)) + 1;

            if (strlen($id) == '1') {
                $id = '0' . $id;
            } else {
                $id = $id;
            }
            return $kode .'-'. $id;
        } else {
            return $kode .'-'. '01';
        }
	}

	public function save_tindakan_penunjang($data){
		$insert = $this->db->insert('master_tindakan_penunjang', $data);
		if ($insert) {
			return true;
		} else {
			return false;
		}
	}
}
?>	
