<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once( APPPATH . 'modules_core/base/controllers/application_base.php' );
require_once( APPPATH . 'modules_core/base/controllers/operator_base.php' );

class Invoicebpjs extends Operator_base {
	function __construct(){
		parent:: __construct();
		$this->load->model("m_invoicenonbpjs");
		$this->load->model("m_invoicebpjs");
		$data['page_title'] = "Invoice Non BPJS";
		$this->session->set_userdata($data);
	}

	public function index($page = 0)
	{
		// load template
		//$data['content'] = 'tagihan/invoicenonbpjs';
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		//$this->load->view('base/operator/template', $data);
		redirect('bersalin/homebersalin');
	}

	public function invoice($no_invoice){
		$data['content'] = 'tagihan/invoicebpjs';
		$this->check_auth('R');
		$data['menu_view'] = $this->menu();
		$data['user'] = $this->user;
		
		// $data['javascript'] = 'master/diagnosis/javascript/j_list';
		$invoice = $this->m_invoicenonbpjs->get_visit_id($no_invoice);
		$visit_id = $invoice['visit_id'];
		$sub_visit = $invoice['sub_visit'];
		$data['visit_id'] = $visit_id;
		$data['sub_visit'] = $invoice['sub_visit'];
		$data['no_invoice'] = $no_invoice;
		$data['invoice'] = $invoice;

		$pasien = $this->m_invoicenonbpjs->get_data_pasien($visit_id);
		$data['pasien'] = $pasien;
		$data['tagihanadmisi'] = $this->m_invoicenonbpjs->get_tagihanadmisi($no_invoice);
		$temp = $this->m_invoicenonbpjs->get_tagihantunjang($sub_visit);
		$data['tagihantunjang'] = $temp;

		$kelas = $invoice['kelas_pelayanan'];
		$i = 0;
		$insert = [];
		foreach ($temp as $value) {
			$nama = $value['nama_tindakan'];
			$bpjs = $this->m_invoicenonbpjs->get_tindakanbpjs($nama , $kelas);
			$value['tarif_bpjs'] = intval($bpjs['js'])+intval($bpjs['jp'])+intval($bpjs['bakhp']);
			$value['tarif'] = intval($value['js'])+intval($value['jp'])+intval($value['bakhp']);
			$value['selisih'] = $value['tarif']-$value['tarif_bpjs'];

			$insert[$i++] = $value;
		}

		$data['tagihantunjang'] = $insert;
		$data['tindakanop'] = $this->m_invoicenonbpjs->get_tindakanoperasi($no_invoice);
		$data['deposit'] = $this->m_invoicenonbpjs->get_deposit($visit_id)['deposit'];
		
		$this->load->view('base/operator/template', $data);	
	}

	public function create_tagihankamar(){
		$no_invoice = $_POST['no_invoice'];
		$sub_visit = $_POST['sub_visit'];
		$kelas = $_POST['kelas_pelayanan'];

		$tindakan = $this->m_invoicenonbpjs->get_datakamar($sub_visit);

		foreach ($tindakan as $value) {
			$insert = $value;
			$kamar_id = $value['kamar_id'];	
			$insert['no_invoice'] = $no_invoice;	

			$prepare = $this->m_invoicenonbpjs->get_preparekamarbpjs($kamar_id);
			$exp = explode(" ", $prepare);

			$realkamar = "";
			for ($i=0; $i < count($exp)-1; $i++) { 
				$realkamar .= $exp[$i]." ";
			}

			$insert['tarif_bpjs'] = $this->m_invoicenonbpjs->get_kamarbpjs($realkamar,$kelas);
			$insert['selisih'] = Intval($value['tarif'])-Intval($insert['tarif_bpjs']);

			if($insert['selisih']<0)
				$insert['selisih'] = 0;			

			$in = $this->m_invoicenonbpjs->insert_tagihankamar($insert);

		}

		$result = $this->m_invoicenonbpjs->get_tagihankamar($no_invoice);

		$final = [];
		$i = 0;
		foreach ($result as $data) {

			$startTimeStamp = strtotime($data['tgl_masuk']);
			$endTimeStamp = strtotime($data['tgl_keluar']);

			$timeDiff = abs($endTimeStamp - $startTimeStamp);

			$numberDays = $timeDiff/86400;  // 86400 seconds in one day
			$mod = abs(($timeDiff % 86400)/3600);

			// and you might want to convert to integer
			$numberDays = intval($numberDays);
			$mod = intval($mod);
			
			$data['waktu'] = $numberDays." Hari ".$mod." Jam";
			$data['hari'] = $numberDays;

			$final[$i] = $data;
			$i++;
		}

		header('Content-Type: application/json');
		echo json_encode($final);
	}

	public function create_tagihanakomodasi(){
		$no_invoice = $_POST['no_invoice'];
		$sub_visit = $_POST['sub_visit'];
		$kelas = $_POST['kelas_pelayanan'];

		$tindakan = $this->m_invoicenonbpjs->get_dataakomodasi($sub_visit);

		$insert = [];
		$no = 0;
		foreach ($tindakan as $value) {
			$value['no_invoice'] = $no_invoice;
			$value['jumlah'] = $value['tarif'];

			$prepare = $this->m_invoicenonbpjs->get_preparemakan($value['makan_id']);
			$exp = explode(" ", $prepare);

			$realmakan = "";
			for ($i=0; $i < count($exp)-1; $i++) { 
				$realmakan .= $exp[$i]." ";
			}

			$value['tarif_bpjs'] = $this->m_invoicenonbpjs->get_makanbpjs($realmakan,$kelas);
			$value['selisih'] = Intval($value['tarif'])-Intval($value['tarif_bpjs']);

			if($value['selisih']<0)
				$value['selisih'] = 0;
			
			$insert = $this->m_invoicenonbpjs->insert_tagihanakomodasi($value);
			// $insert[$no] = $prepare;
			// $no++;
		}

		$result = $this->m_invoicenonbpjs->get_tagihanakomodasi($no_invoice);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function update_tagihankamar(){
		foreach ($_POST as $data) {
			$insert = $data;
		}

		$update = $this->m_invoicenonbpjs->update_tagihankamar($insert['id'], $insert);

		return "Data Berhasil Disimpan";
	}

	public function update_tagihanmakan(){
		foreach ($_POST as $data) {
			$insert = $data;
		}

		$update = $this->m_invoicenonbpjs->update_tagihanmakan($insert['id'], $insert);

		return "Data Berhasil Disimpan";
	}

	public function create_tagihan(){
		$no_invoice = $_POST['no_invoice'];
		$sub_visit = $_POST['sub_visit'];
		$kelas = $_POST['kelas'];

		$tindakan = $this->m_invoicebpjs->get_tindakan($sub_visit, $kelas);

		foreach ($tindakan as $value) {
			$getbpjs = $this->m_invoicebpjs->get_tarifbpjs($value['care_id'], $kelas);
			$value['tarif_bpjs'] = $getbpjs['tarif_bpjs'];
			$value['selisih'] = intval($value['tarif']) - intval($value['tarif_bpjs']);
			if(intval($value['selisih']) < 0)
				$value['selisih'] = 0;
			$value['total'] = $value['selisih'] + $value['on_faktur'];
			$value['no_invoice'] = $no_invoice;			

			$insert = $this->m_invoicebpjs->insert_tagihan($value);
			//$result[] = $value;
		}		

		$result = $this->m_invoicebpjs->get_tagihanperawatan($no_invoice);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function save_tindakan(){
		$kelas = $_POST['kelas'];

		$visit_id = $_POST['visit_id'];
		$care['visit_id'] = $visit_id;
		$care['sub_visit'] = $_POST['sub_visit'];
		$care['waktu_tindakan'] = $this->date_db($_POST['waktu']);
		$care['tindakan_id'] = $_POST['tindakan_id'];
		$care['on_faktur'] = $_POST['on_faktur'];
		$care['paramedis'] = $_POST['paramedis'];
		$care['js'] = $_POST['js'];
		$care['jp'] = $_POST['jp'];
		$care['bakhp'] = $_POST['bakhp'];
		$care['tarif'] = $_POST['tarif'];
		$care['jumlah'] = $_POST['jumlah'];
		$care['dept_id'] = $_POST['dept_id'];

		$id = $this->m_invoicebpjs->get_last_visit_care($visit_id);
		if($id){
			$vid = intval(substr($id['value'], strlen($visit_id) + 2)) + 1;
			if (strlen($vid) == "1") {
				$vid = '000'. $vid;
			}else if(strlen($vid) == "2"){
				$vid = '00' . $vid;
			}else if (strlen($vid) == "3") {
				$vid = '0' . $vid;
			}
			$care['care_id'] = "CA".$visit_id."".($vid);
		}else{
			$care['care_id'] = "CA".$visit_id."0001";
		}

		//subasdmit visit_care
		$in_care = $this->m_invoicebpjs->save_tindakan($care);

		$tagihan['care_id'] = $care['care_id'];
		$tagihan['no_invoice'] = $_POST['no_invoice'];
		$tagihan['tindakan_id'] = $_POST['tindakan_id'];
		$tagihan['dept_id'] = $_POST['dept_id'];
		$tagihan['waktu'] = $this->date_db($_POST['waktu']);
		$tagihan['js'] = $_POST['js'];
		$tagihan['jp'] = $_POST['jp'];
		$tagihan['bakhp'] = $_POST['bakhp'];
		$tagihan['tarif'] = $_POST['tarif'];
		$tagihan['jumlah'] = $_POST['jumlah'];
		$tagihan['on_faktur'] = $_POST['on_faktur'];
		$tagihan['paramedis'] = $_POST['paramedis'];
		$tagihan['petugas_input'] = $_POST['paramedis'];

		//perhitungan bpjs

		$tagihan['tarif_bpjs'] = $_POST['tarif_bpjs'];
		$tagihan['selisih'] = $_POST['selisih'];

		if(intval($tagihan['selisih']) < 0)
			$tagihan['selisih'] = 0;
		$tagihan['total'] = $tagihan['selisih'] + $tagihan['on_faktur'];

		//submit tindakan
		$in_tagihan = $this->m_invoicebpjs->save_tagihan($tagihan);

		$result = $this->m_invoicebpjs->get_inserted_tagihan($care['care_id']);

		header('Content-type: application/json');
		echo json_encode($result);
	}
}
