<br>
<div class="title">
	<li style="list-style: none">
		<a href="<?php echo base_url() ?>dashboard/operator"><i class="fa fa-home"></i></a>
		<i class="fa fa-angle-right"></i>
		<a href="<?php echo base_url() ?>rawatjalan/homerawatjalan">POLIKLINIK UMUM</a>
		<i class="fa fa-angle-right"></i>
		<a href="#" id="dasbod" style="width:400px;background:transparent;border: 0px;">List Pasien Poliklinik</a>
	</li>
</div>

<input type="hidden" id="dept_id" value="<?php echo $dept_id; ?>">

<div class="navigation" style="margin-left: 10px" >
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
		<li class="active"><a href="#list"  class="cl" data-toggle="tab">List Pasien Poliklinik</a></li>
		<li><a href="#tagihan" data-toggle="tab">Tagihan</a></li>
 		<li><a href="#farmasi" data-toggle="tab">Farmasi</a></li>
		<li><a href="#logistik" data-toggle="tab">Logistik</a></li>
	    <li><a href="#laporan" data-toggle="tab">Laporan</a></li>
	    <li><a href="#master" data-toggle="tab">Master</a></li>
	</ul>

	<div id="my-tab-content" class="tab-content">
	   	<div class="tab-pane active" id="list">
		   	<form method="POST" id="search_submit">
		       		<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px;">		
						<input type="text" class="form-control" id="searchkey" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>
				</div>	
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">PASIEN POLIKLINIK</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_search">
						<thead>
							<tr class="info">
								<th> No.</th>
								<th> #Rekam Medis </th>
								<th> Nama Lengkap </th>
								<th> Jenis Kelamin </th>
								<th> Tanggal Lahir </th>
								<th> Alamat Tinggal </th>
								<th> Identitas </th>
								<th> Periksa</th>
							</tr>
						</thead>
						<tbody id="t_body">
							<?php
								$i = 0;
								foreach ($antrian as $data) {
									$i++;
									$tgl = strtotime($data['tanggal_lahir']);
									$hasil = date('d F Y', $tgl); 

									echo '<tr>';
										echo '<td align="center">'.$i.'</td>';
							 			echo'<td>'.$data['rm_id'].'</td>';
							 			echo'<td>'.$data['nama'].'</td>';
							 			echo'<td>'.$data['jenis_kelamin'].'</td>';
							 			echo'<td align="center">'.$hasil.'</td>';
							 			echo'<td>'.$data['alamat_skr'].'</td>';
							 			echo'<td>'.$data['jenis_id'].'</td>';

							 			echo'<td style="text-align:center">';
							 				echo'<a href="'.base_url().'rawatjalan/daftarpasien/periksa/'.$data['rj_id'].'/'.$data['visit_id'].'"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a>';
										echo'</td>';
							 		echo'</tr>';
								}
							?>
						</tbody>
					</table>
				</div>
			</div>  
			</div>
			<br>
			<br>
			<br>
			<br>  
	    </div>

	    <div class="tab-pane" id="tagihan" style="padding-bottom:50px;">   
			<form class="form-horizontal" method="POST" id="submitTagihanSearch">
		       		<div class="search">
					<label class="control-label col-md-3">
						<i class="fa fa-search" style="margin-left: -130px">&nbsp;&nbsp;</i>
					</label>
					<div class="col-md-4" style="margin-left:-400px">		
						<input type="text" id="search_tagihan" class="form-control" placeholder="Masukkan Nama atau Nomor RM Pasien" autofocus>
			        </div>
			        <button type="submit" class="btn btn-info">Cari</button>&nbsp;&nbsp;&nbsp;
			        <a onclick="setStatus('10')" class="btn btn-warning"> Tambah Invoice Baru</a>
				</div>		
			</form>
			<br>
			<hr class="garis">

			<div id="titleInformasi" style="margin-bottom:-40px;">
			<p style="text-align:center;margin-top:-30px; margin-left: -50px;">TAGIHAN</p></div>
			<div style="border: solid 3px #50BFF9;border-top-width:30px;margin:0px 10px 0px 10px;padding:0px;padding-top:0px;" role="form">

				<div class="portlet box red">
					<div class="portlet-body" style="margin: -11px 0px -85px 0px">
					<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="table_tagihan">
						<thead>
							<tr class="info">
								<th style="text-align:center;width:20px;">No.</th>
								<th>Unit</th>
								<th>Nomor Invoice</th>
								<th>Nomor Visit</th>
								<th>#Rekam Medis</th>
								<th>Nama Pasien</th>
								<th>Alamat</th>
								<th>Cara Bayar</th>
								<th style="text-align:center;width:25px;">Action</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>			
			</div>  
			</div>
			<br>
			<br> 
	    </div>

	    <div class="tab-pane" id="farmasi">

        	<div class="dropdown" id="btnBawahInventori" >
	            <div id="titleInformasi">Inventori</div>
	            <div id="btnBawahInventori" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <div id="infoInventori">
				<div class="form-group">
	            	<form class="form-horizontal informasi" role="form" method="post" id="submitfilterfarmasiunit">
		            	<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
						<div class="col-md-2" style="width:200px">
							<select class="form-control select" name="filterInv" id="filterInv">
								<option value="" selected>Pilih</option>
								<option value="jenis">Jenis Obat</option>
								<option value="merek">Merek</option>
								<option value="nama">Nama Obat</option>							
							</select>	
						</div>
						<div class="col-md-2" style="margin-left:-15px; width:200px;" >
							<input type="text" class="form-control" id="filterby" name="valfilter" placeholder="Value"/>
						</div>
						<div class="col-md-1" >
							<button type="submit" class="btn btn-warning">FILTER</button> 
						</div>
					</form>
					<div class="col-md-1" >
						<button class="btn btn-danger" id="expired">EXPIRED</button> 
					</div>
					<div class="col-md-1" >
						<button class="btn btn-warning" id="expired3">EX 3 BULAN</button>
					</div>
					<div class="col-md-1" style="margin-left: 20px;">
						<button class="btn btn-warning" id="expired6">EX 6 BULAN</button>
					</div>
				</div>
				<br><br>
				<div class="form-group" >
					<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDTUtama" id="tabelinventoriunit">
							<thead>
								<tr class="info">
									<th width="20">No.</th>
									<th> Nama Obat </th>
									<th> No Batch </th>
									<th> Harga Jual </th>
									<th> Merek </th>
									<th> Stok</th>
									<th> Satuan </th>
									<th width="200"> Tanggal Kadaluarsa </th>
									<th width="100"> Action </th>								
								</tr>
							</thead>
							<tbody id="tbodyinventoriunit">
								<?php  
									if (isset($obatunit)) {
										$i = 1;
										foreach ($obatunit as $value) {
											$tgl = DateTime::createFromFormat('Y-m-d', $value['tgl_kadaluarsa']);
											echo '<tr>'.
												'<td>'.($i++).'</td>'.
												'<td>'.$value['nama'].'</td>'.
												'<td>'.$value['no_batch'].'</td>'.
												'<td>'.$value['harga_jual'].'</td>'.
												'<td>'.$value['nama_merk'].'</td>'.
												'<td>'.$value['total_stok'].'</td>'.
												'<td>'.$value['satuan'].'</td>'.								
												'<td>'.$tgl->format('d F Y').'</td>'.
												'<td><a href="#" class="inoutobat" data-toggle="modal" data-target="#inout"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'.
												'<a href="#edInvenBer" data-toggle="modal" class="printobat"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>'.
												'<input type="hidden" class="barangmerk_id" value="'.$value['merk_id'].'">'.
												'<input type="hidden" class="barangjenis_obat_id" value="'.$value['jenis_obat_id'].'">'.
												'<input type="hidden" class="barangsatuan_id" value="'.$value['satuan_id'].'">'.
												'<input type="hidden" class="barangobat_dept_id" value="'.$value['obat_dept_id'].'">'.
											'</td></tr>';
										}
									}
								?>
							</tbody>
						</table>
					</div>
					<button class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
				</div>
				<br><br>
	        </div>
			<div class="modal fade" id="inout" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal informasi" role="form" method="post" id="submitinoutunit">
					<div class="modal-dialog">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<div class="form-group">
		        					<label class="control-label col-md-3" >Tanggal 
									</label>
									<div class="col-md-4" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" id="tglInOut" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" >In / Out 
									</label>
									<div class="col-md-4">
						         		<select class="form-control select" name="iober" id="iober">
											<option value="IN" selected>IN</option>
											<option value="OUT">OUT</option>					
										</select>
									</div>	
								</div>
								<div class="form-group">
		        					<label class="control-label col-md-3" >Jumlah </label>
									<div class="col-md-4" >
					         			<input type="text" class="form-control" id="jmlInOutBer" name="jmlInOutBer" placeholder="Jumlah">
									</div>
								</div>
								<div class="form-group">
		        					<label class="control-label col-md-3" >Sisa Stok </label>
									<div class="col-md-4" >
					         			<input type="text" class="form-control" id="sisaInOutBer" name="sisaInOutBer" placeholder="Sisa Stok" readonly="">
									</div>
								</div>
								<div class="form-group">
		        					<label class="control-label col-md-3" >Keterangan </label>
									<div class="col-md-6" >
										<textarea class="form-control" id="keteranganIO" placeholder="Keterangan"></textarea>
									</div>
								</div>
		        			</div>
		        			<div class="modal-footer">
		        				<input type="hidden" id="inout_obat_dept_id">
		        				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			       		<button type="submit" class="btn btn-success">Simpan</button>
					      	</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal fade" id="edInvenBer" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
	        			<form class="form-horizontal" role="form">
			            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
								<thead>
									<tr class="info" >
										<th  style="text-align:center"> Waktu </th>
										<th  style="text-align:left"> IN / OUT </th>
										<th  style="text-align:left"> Jumlah </th>
										<th  style="text-align:left"> Stok Akhir </th>
									</tr>
								</thead>
								<tbody id="tbodydetailobatinventori">
									<tr>
										<td colspan="4" style="text-align:center">Tidak ada Detail</td>
									</tr>
								</tbody>
							</table>
						</form>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>

			<div class="dropdown" id="btnBawahMintaObat">
	            <div id="titleInformasi">Permintaan Farmasi</div>
	            <div id="btnBawahMintaObat" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoMintaObat">
            	<form class="form-horizontal" role="form" method="post" id="permintaanfarmasibersalin">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="noPermFarmBers" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanfarmasi" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="ketObatFarBers" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>
					<a href="#modalMintaFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<!-- <th width="20"> No. </th> -->
										<th> Nama Obat </th>
										<th>Tanggal Kadaluarsa</th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Stok Gudang </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputMintaFar" class="addKosong">
								</tbody>
							</table>
						</div>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning" type="button" id="batalpermintaanfarmasi">RESET</button>
								<button class="btn btn-success" type="submit">SIMPAN</button> 
							</span>
						</div>
					</div>	
				</form>
			</div>	    
			<br>
			<div class="modal fade" id="modalMintaFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<div class="form-group">
		        				<form method="post" class="form-horizontal" role="form" id="formobatfarmasi">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncifarmasi" placeholder="Nama Obat"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
								</form>
								<div style="margin-right:10px;margin-left:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%;">
										<thead>
											<tr class="info">
												<th>Nama Obat</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Stok Gudang</th>
												<th>Tgl Kadaluarsa</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodyobatpermintaanfarmasi">
											<tr>
												<td colspan="6" style="text-align:center">Cari data Obat</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
	           	
	       	<div class="dropdown" id="btnBawahRetDepartemen">
	            <div id="titleInformasi">Retur Farmasi</div>
	            <div id="btnBawahRetFarmasi" class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
           	<div id="infoRetDepartemen">
            	<form class="form-horizontal" role="form" method="post" id="formsubmitretur">
            		<div class="informasi">
            			<br>
            			<div class="form-group">
            				<div class="col-md-2">
            					<label class="control-label">Nomor Retur</label>
            				</div>
            				<div class="col-md-3">
            					<input type="text" class="form-control" name="noRetFarBers" id="noRetFarBers" placeholder="Nomor Retur"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
            					<label class="control-label">Tanggal Retur</label>
            				</div>
            				<div class="col-md-2">
            					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" class="form-control" id="waktureturbersalin" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
            			</div>
            			<div class="form-group">
							<div class="col-md-2">
            					<label class="control-label">Keterangan</label>
            				</div>
            				<div class="col-md-3">
								<textarea class="form-control" id="ketObatRetFarBers" name="ketObatRetFarBers"></textarea>	
							</div>
            			</div>
            		</div>

            		<a href="#modalRetFarBers" data-toggle="modal"><i class="fa fa-plus" style="margin-left : 40px;font-size:11pt;">&nbsp;Tambah Obat</i></a>
					<div class="clearfix"></div>
					
					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
						
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabRetur">
								<thead>
									<tr class="info" >
										<th > Nama Obat </th>
										<th > Tanggal Kadaluarsa</th>
										<th > Satuan </th>
										<th > Merek </th>
										<th > Stok Unit </th>
										<th > Jumlah Retur </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputRetFar" class="addKosong">
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning" type="button" id="batalreturfarmasi">RESET</button>
								<button class="btn btn-success" type="submit">SIMPAN</button>
							</span>
						</div>
						<br>
					</div>
				</form>
			</div>
			<div class="modal fade" id="modalRetFarBers" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Obat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<div class="form-group">
		        				<form method="post" role="form" class="form-horizontal" id="formsearchobatretur">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncireturbersalin" placeholder="Nama Obat"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>
								</form>
								<div style="margin-left:10px; margin-right:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa">
										<thead>
											<tr class="info">
												<td>Nama Obat</td>
												<td>Satuan</td>
												<td>Merek</td>
												<td>Stok Unit</td>
												<td>Tgl Kadaluarsa</td>
												<td width="10%">Pilih</td>
											</tr>
										</thead>
										<tbody id="tbodyreturbersalin">
											<tr>
												<td style="text-align:center" colspan="6">Cari data Obat</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>	
			<br>
	    </div>

        <div class="tab-pane" id="logistik">
        	<div class="modal fade" id="modalbarang" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:800px">
					<div class="modal-content">
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Pilih Barang</h3>
	        			</div>
	        			<div class="modal-body">

		        			<div class="form-group">
		        				<form method="post" class="form-horizontal" role="form" id="formmintabarang">
									<div class="form-group">	
										<div class="col-md-5" style="margin-left:20px;">
											<input type="text" class="form-control" name="katakunci" id="katakuncimintabarang" placeholder="Nama barang"/>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-info">Cari</button>
										</div>
										<br><br>	
									</div>		
								</form>
								<div style="margin-right:10px;margin-left:10px;"><hr></div>
								<div class="portlet-body" style="margin: 0px 20px 0px 15px">
									<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchDiagnosa" style="font-size:99%">
										<thead>
											<tr class="info">
												<th>Nama Barang</th>
												<th>Satuan</th>
												<th>Merek</th>
												<th>Tahun Pengadaan</th>
												<th>Stok Gudang</th>
												<th width="10%">Pilih</th>
											</tr>
										</thead>
										<tbody id="tbodybarangpermintaan">
											<tr>
												<td colspan="6" style="text-align:center">Cari data Barang</td>
											</tr>
										</tbody>
									</table>												
								</div>
							</div>
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
	       	<div class="dropdown" id="btnBawahInventoriBarang">
	            <div id="titleInformasi">Inventori</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoInventoriBarang">
				<div class="form-group" >
					<div class="portlet-body" style="margin: 30px 10px 20px 10px">
						<table class="table table-striped table-bordered table-hover table-responsive tableDT" id="tblinventorigudangunit">
							<thead>
								<tr class="info" >
									<th width="20">No.</th>
									<th > Nama Barang </th>
									<th > Merek </th>
									<th > Harga </th>
									<th > Stok </th>
									<th > Satuan </th>
									<th > Tahun Pengadaan</th>
									<th > Sumber Dana</th>
									<th width="100"> Action </th>
								</tr>
							</thead>
							<tbody id="tbodyinventoribarang">
								<?php 
									if (isset($inventoribarang)) {
										if (!empty($inventoribarang)) {
											$i = 1;
											foreach ($inventoribarang as $value) {
												echo '<tr>
														<td>'.($i++).'</td>
														<td>'.$value['nama'].'</td>
														<td>'.$value['nama_merk'].'</td>
														<td>'.$value['harga'].'</td>
														<td>'.$value['stok'].'</td>
														<td>'.$value['satuan'].'</td>
														<td>'.$value['tahun_pengadaan'].'</td>
														<td>'.$value['sumber_dana'].'</td>
														<td style="text-align:center">
															<input type="hidden" class="barang_detail_inout" value="'.$value['barang_detail_id'].'">
															<a href="#inoutbar" data-toggle="modal" class="edBarang" id="edMasObat"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="IN-OUT"></i></a>
															<a href="#edInvenBerBar" data-toggle="modal" class="detailinvenbarang"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>							
														</td>
													</tr>';
											}
										}
									}
								?>
									
							</tbody>
						</table>
					</div>
					<form method="post" action="<?php echo base_url() ?>rawatjalan/homerawatjalan/excel_barang_unit">
						<button class="btn btn-info" type="submit" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button>
						<input type="hidden" class="my_dept_id" name="my_dept_id" value="<?php echo $dept_id ?>">
					</form>
					<br>
	        	</div>
	        </div>
			<div class="modal fade" id="inoutbar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<form class="form-horizontal" role="form" style="margin-left:30px;" id="forminoutbarang">
						<div class="modal-content" >
							<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        				<h3 class="modal-title" id="myModalLabel">IN OUT</h3>
		        			</div>
		        			<div class="modal-body">
			        			<div class="form-group">
			        				<label class="control-label col-md-3" >Tanggal </label>
									<div class="col-md-6" >
						         		<div class="input-icon">
											<i class="fa fa-calendar"></i>
											<input type="text" style="cursor:pointer;background-color:white" id="tanggalinout" data-date-autoclose="true" class="form-control calder" readonly data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
									</div>
								</div>
										
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" >In / Out </label>
									<div class="col-md-6">
						         		<select class="form-control select" name="io" id="io">
											<option value="IN" selected>IN</option>
											<option value="OUT">OUT</option>					
										</select>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Jumlah in/out</label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="jmlInOut" name="jmlInOut" placeholder="Jumlah">
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Sisa Stok </label>
									<div class="col-md-6" >
						         		<input type="text" class="form-control" id="sisaInOut" name="sisaInOut" placeholder="Sisa Stok" readonly>
									</div>
								</div>
								<div class="form-group">
			        				<label class="control-label col-md-3" >Keterangan </label>
									<div class="col-md-6" >
										<textarea class="form-control" id="keteranganIObarang" placeholder="Keterangan"></textarea>
									</div>
								</div>										
		        			</div>
		        			<div class="modal-footer">
		        				<input type="hidden" id="id_barang_inoutprocess">
		 			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
		 			       		<button type="submit" class="btn btn-success">Simpan</button>
					      	</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal fade" id="edInvenBerBar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" >
						<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
	        				<h3 class="modal-title" id="myModalLabel">Riwayat</h3>
	        			</div>
	        			<div class="modal-body">
		        			<form class="form-horizontal" role="form">
				            	<table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th> Waktu </th>
											<th> IN / OUT </th>
											<th> Jumlah </th>
											<th> Keterangan </th>
										</tr>
									</thead>
									<tbody id="tbodydetailbrginventori">
										<tr>
											<td colspan="4" style="text-align:center">Tidak ada detail in-out</td>
										</tr>
											
									</tbody>
								</table>
							</form>
							
	        			</div>
	        			<div class="modal-footer">
	 			       		<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
				      	</div>
					</div>
				</div>
			</div>
			<br>

			<div class="dropdown" id="btnBawahPermintaanBarang" style="margin-left:10px;width:98.5%">
	            <div id="titleInformasi">Permintaan Logistik</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <div id="infoPermintaanBarang">
            	<form class="form-horizontal" role="form" method="post" id="permintaanbarangunit">
	            	<div class="informasi">
	            		<br>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Nomor Permintaan</label>
	        				</div>
	        				<div class="col-md-3">
	        					<input type="text" class="form-control" name="noPermFarmBers" id="nomorpermintaanbarang" placeholder="Nomor Permintaan"/>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
	        					<label class="control-label">Tanggal Permintaan</label>
	        				</div>
	        				<div class="col-md-2">
	        					<div class="input-icon">
									<i class="fa fa-calendar"></i>
									<input type="text" style="cursor:pointer;background-color:white" id="tglpermintaanbarang" class="form-control" data-date-format="dd/mm/yyyy H:i" data-provide="datetimepicker" value="<?php echo date("d/m/Y H:i");?>">
								</div>
							</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-md-2">
	        					<label class="control-label">Keterangan</label>
	        				</div>
	        				<div class="col-md-3">	
								<textarea class="form-control" id="keteranganpermintaanbarang" name="ketObatFarBers"></textarea>	
							</div>
	        			</div>
	        		</div>
					<a href="#modalbarang" data-toggle="modal"><i class="fa fa-plus" style="margin-left:40px;font-size:11pt;">&nbsp;Tambah Barang</i></a>
					<div class="clearfix"></div>

					<div class="portlet box red">
						<div class="portlet-body" style="margin: 10px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover table-responsive" id="tabApo">
								<thead>
									<tr class="info" >
										<th> Nama Barang </th>
										<th> Satuan </th>
										<th> Merek </th>
										<th> Tahun Pengadaan </th>
										<th> Stok Gudang </th>
										<th> Jumlah Diminta </th>
										<th width="80"> Action </th>			
									</tr>
								</thead>
								<tbody  id="addinputmintabarang">
									<?php echo '<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>'; ?>
								</tbody>
							</table>
						</div>
						<br>
						<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
						<div style="margin-left:80%">
							<span class="customSpan">
								<button class="btn btn-warning" type="reset" id="batalpermintaanfarmasi">RESET</button>
								<button class="btn btn-success" type="submit">SIMPAN</button>
							</span>
						</div>
						<br>
					</div>	
				</form>
			</div>	    
			<br>
	    </div>
	    
	    <div class="tab-pane" id="laporan" style="margin-left:40px"> 
	    	<div id="sensusharian" style="width:100%">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Harian</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" 
	        		role="form" method="post" action="<?php echo base_url() ?>rawatjalan/homerawatjalan/get_sensus_harian_poli">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="col-md-2" >
							<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="text" name="hari_date" style="cursor:pointer;background-color:white;" class="form-control isian" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 pull-right" style="margin-right:30px">
								<button type="submit" class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>  
	        <br>

			<div id="sensusbulanan">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Bulanan</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_rekap_bulanan_poli">
        		
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="col-md-2">
	        				<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="hidden" name="dept_sensus" value="<?php echo $dept_id ?>">
								<input type="text" data-date-format="mm/yyyy" style="cursor:pointer;background-color:white;" class="form-control" name="start" id="monthonly" data-date-min-view-mode="1" data-provide="datepicker" readonly value="<?php echo date("m/Y");?>" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-2 pull-right"  style="margin-right:30px">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>
	        <br>

	        <div id="sensustahunan">
	        	<div id="titleInformasi" style="margin-bottom:-30px;">Sensus Tahunan</div>
        		<form class="form-horizontal laporan" style="border: solid 3px #50BFF9;border-top-width:30px;margin-right:40px;" role="form"
	        		method="post" action="<?php echo base_url() ?>rekammedis/homelaporanrekammedis/get_rekap_tahunan_poli">
	        		<div class="form-group" style="margin-top:20px;margin-left:10px;">
				
	        			<label class="control-label col-md-2" style="width:120px"><i class="glyphicon glyphicon-filter"></i>&nbsp;Filter by
						</label>
	        			<div class="col-md-2">
	        				<div class="input-icon">
								<i class="fa fa-calendar"></i>
								<input type="hidden" name="dept_sensus" value="<?php echo $dept_id ?>">
								<input type="text" data-date-format="yyyy" style="cursor:pointer;background-color:white;" class="form-control" name="start" id="yearly" data-date-min-view-mode="2" data-provide="datepicker" readonly value="<?php echo date("Y");?>" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-2 pull-right" style="margin-right:30px">
								<button class="btn btn-info ">Simpan ke Excel(.xls)</button> 
							</div>
						</div>
					</div>
	        	</form>
	        </div>  

			<br>  
	    </div>
	    
	    <div class="tab-pane" id="master">   
	    	<div class="dropdown" id="">
	            <div id="titleInformasi">Jasa Pelayanan</div>
	            <div class="btnBawah"><i class="glyphicon glyphicon-chevron-down" style="margin-right: 5px"></i></div> 
            </div>
            <br>
            <form method="POST" id="submitJasaPelayanan" action="<?php echo base_url() ?>rawatjalan/homerawatjalan/print_jaspel">
	            <div class="form-horizontal">
		            <div class="informasi">
			            <div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Periode </label>
							<div class="col-md-3" style="margin-left:-15px">
								<div class="input-daterange input-group" id="datepicker">
								    <input type="text" id="mulai_date" style="cursor:pointer;background-color:white;" class="form-control" name="start" data-date-format="dd/mm/yyyy" data-provide="datepicker" readonly value="<?php echo date("d/m/Y");?>" />
								    <div class="input-group-addon">to</div>
								    <input type="text" id="sampai_date" style="cursor:pointer;background-color:white;" class="form-control" name="end" readonly data-date-format="dd/mm/yyyy" data-provide="datepicker" value="<?php echo date("d/m/Y");?>" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2"> <i class="glyphicon glyphicon-filter"></i>&nbsp;Cara Bayar</label>
							<div class="input-group col-md-2">
								<select class="form-control select" name="cara_bayar" id="carabayar">
									<option value="" selected>Pilih Cara Bayar</option>
									<option value="Umum">Umum</option>
									<option value="BPJS" id="op-bpjs">BPJS</option>
									<option value="Jamkesmas" >Jamkesmas</option>
									<option value="Asuransi" id="op-asuransi">Asuransi</option>
									<option value="Kontrak" id="op-kontrak">Kontrak</option>
									<option value="Gratis" >Gratis</option>
									<option value="Lain">Lain-lain</option>
								</select>
							</div>	
						</div>

						<div class="form-group">
							<label class="control-label col-md-2"><i class="glyphicon glyphicon-filter"></i>&nbsp;Paramedis</label>
							<div class="input-group col-md-2">
								<input type="hidden" id="paramedis_id" name="id_petugas_jaspel" value="">
								<input type="text" class="form-control" id="paramedis" autocomplete="off" spellcheck="false"  name="paramedis" placeholder="Paramedis" >
							</div>

						</div>

					</div>
					<hr style="margin-bottom:-17px; margin-left:10px; margin-right:10px">
					<div style="margin-left:80%">
						<span class="customSpan">
								<button type="button" id="btn_filter_jaspel" class="btn btn-warning">FILTER</button> 
							</span>
					</div>
					<br>

				</div>

		    	<div class="portlet-body" style="margin: 10px 10px 0px 10px">
					<table class="table table-striped table-bordered table-hover tableDTUtama" id="tabelJpPoliinap">
						<thead>
							<tr class="info">
								<th width="20">No.</th>
								<th>Tanggal</th>
								<th>Tindakan</th>
								<th>Cara Bayar</th>
								<th>Paramedis</th>
								<th>Paramedis Lain</th>
								<th>Jasa Pelayanan</th>
							</tr>
						</thead>
						<tbody id="tbody_resep">
							<?php
								$no = 0;
								foreach ($jasalayan as $data) {
									echo '
										<tr>
											<td style="text-align:center">'.++$no.'</td>
											<td style="text-align:center">'.$data['waktu_tindakan'].'</td>
											<td>'.$data['nama_tindakan'].'</td>
											<td>'.$data['cara_bayar'].'</td>
											<td>'.$data['nama_petugas'].'</td>
											<td>'.$data['paramedis_lain'].'</td>
											<td style="text-align:right">'.$data['jp'].'</td>
										</tr>
									';
								}
							?>
						</tbody>
					</table>
				</div>
				<button type="submit" class="btn btn-info" style="margin:-100px 0px 0px 10px;">Simpan ke Excel(.xls)</button> 
				<br><br>
			</form>
				
				<br><br>
			</div>    
	    </div>
	    
	    <div class="modal fade" id="petugas" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		        		<h3 class="modal-title" id="myModalLabel">Pilih Petugas</h3>
		        	</div>
		        	<div class="modal-body">
			        	<div class="form-group">
							<div class="form-group">	
								<div class="col-md-5">
									<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama petugas"/>
								</div>
								<div class="col-md-2">
									<button type="button" class="btn btn-info">Cari</button><br>
								</div>	
							</div>		
							<br>	
							<div style="margin-left:5px; margin-right:5px;"><br><hr></div>
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover" id="tabelSearchPetugas">
									<thead>
										<tr class="info">
											<td>Nama Petugas</td>
											<td width="10%">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Jems</td>
											<td style="text-align:center"><i class="glyphicon glyphicon-check"></i></td>
										</tr>
										<tr>
											<td>Putu</td>
											<td style="text-align:center"><i class="glyphicon glyphicon-check"></i></td>
										</tr>
									</tbody>
								</table>												
							</div>
						</div>
		        	</div>
		        	<div class="modal-footer">
		 				<button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="editinacbgs" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form class="form-horizontal" role="form" method="POST" id="">
				<div class="modal-dialog" style="width:930px;">
					<div class="modal-content">
						<form class="form-horizontal" role="form">
							<div class="modal-header">
				   				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				   				<h3 class="modal-title" id="myModalLabel">Edit Input INA-CBG's</h3>
				   			</div>
							<div class="modal-body">
								<table border="0" width="100%" class="tbladdinacbg">
									<tr>
										<td width="20">1.</td>
										<td width="30%">Kode Rumah Sakit</td>
										<td colspan="3"> <input type="text" class="form-control input-sm" name="koders" placeholder="Kode Rumah Sakit" style="width:190px;"> </td>
									</tr>
									<tr>
										<td width="20">2.</td>
										<td width="30%">Kelas Rumah Sakit</td>
										<td colspan="3"> <div class="input-group col-md-3">
												<select class="form-control input-sm" name="kelasrs" id="kelasrs">
													<option value="" selected>Pilih</option>
													<option value="A">A</option>
													<option value="B">B</option>
													<option value="C">C</option>
													<option value="D">D</option>							
												</select>
											</div>	
										</td>
									</tr>
									<tr>
										<td width="20">3.</td>
										<td width="30%">Nomor Rekam Medis</td>
										<td colspan="3"> <input type="text" class="form-control input-sm" name="nomorrm" placeholder="Nomor RM" style="width:190px;" readonly> </td>
									</tr>
									<tr>
										<td width="20">4.</td>
										<td width="30%">Kelas Perawatan</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="kelasperawatan" placeholder="III/II/I/Utama/VIP" style="width:190px;" readonly>  </td>
									</tr>
									<tr>
										<td width="20">5.</td>
										<td width="30%">Biaya Perawatan</td>
										<td colspan="3"> 
											<div class="input-group col-md-3">
												<span class="input-group-addon" id="basic-addon1">Rp.</span>
												<input type="text" class="form-control input-sm" name="biayaperawatan" readonly>
											</div>
										</td>
									</tr>
									<tr>
										<td width="20">6.</td>
										<td width="30%">Jenis Perawatan</td>
										<td colspan="3"> <input type="text" class="form-control input-sm" name="jenisperawatan" style="width:190px;" readonly> </td>
									</tr>
									<tr>
										<td width="20">7.</td>
										<td width="30%">Tanggal Masuk</td>
										<td colspan="3"> <input type="text" class="form-control input-sm" name="tanggalmasuk" style="width:190px;" readonly> </td>
									</tr>
									<tr>
										<td width="20">8.</td>
										<td width="30%">Tanggal Keluar</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="tanggalkeluar" style="width:190px;" readonly> </td>
									</tr>
									<tr>
										<td width="20">9.</td>
										<td width="30%">Lama Rawat</td>
										<td colspan="3">
											<div class="input-group col-md-3">
												<input type="text" class="form-control input-sm" name="lamarawat" readonly>
												<span class="input-group-addon" id="basic-addon1" style="width:70px;">hari</span>
											</div>
										</td>
									</tr>
									<tr>
										<td width="20">10.</td>
										<td width="30%">Tanggal Lahir</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="tgllahir" style="width:190px;" readonly> </td>
									</tr>
									<tr>
										<td width="20">11.</td>
										<td width="30%">Umur Tahun</td>
										<td colspan="3">
											<div class="input-group col-md-3">
												<input type="text" class="form-control input-sm" name="umurtahun" readonly>
												<span class="input-group-addon" id="basic-addon1" style="width:70px;">tahun</span>
											</div>
										</td>
									</tr>
									<tr>
										<td width="20">12.</td>
										<td width="30%">Umur Hari</td>
										<td colspan="3"> 
											<div class="input-group col-md-3">
												<input type="text" class="form-control input-sm" name="umurhari" readonly>
												<span class="input-group-addon" id="basic-addon1" style="width:70px;">hari</span>
											</div>
										</td>
									</tr>
									<tr>
										<td width="20">13.</td>
										<td width="30%">Jenis Kelamin </td>
										<td colspan="3"> <input type="text" class="form-control input-sm" name="jk" style="width:190px;" readonly> </td>
									</tr>
									<tr>
										<td width="20">14.</td>
										<td width="30%">Cara Pulang</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="carapulang" style="width:190px;" readonly>  </td>
									</tr>
									<tr>
										<td width="20">15.</td>
										<td width="30%">Berat Lahir</td>
										<td colspan="3"> <div class="input-group col-md-3">
												<input type="text" class="form-control input-sm" name="beratlahir">
												<span class="input-group-addon" id="basic-addon1" style="width:70px;">gram</span>
											</div>
										</td>
									</tr>
									<tr>
										<td width="20">16.</td>
										<td width="30%">Diagnosa Utama</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="diagnosautama" style="width:190px;" readonly></td>
									</tr>
									<tr>
										<td width="20">17.</td>
										<td width="30%">Diagnosa Sekunder</td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 1" data-toggle="modal" data-target="#searchDiagnosa" name="dns1" style="width:190px;"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 11" data-toggle="modal" data-target="#searchDiagnosa" name="dns11" style="width:190px;"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 21" data-toggle="modal" data-target="#searchDiagnosa" name="dns21" style="width:190px;"></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 2" data-toggle="modal" data-target="#searchDiagnosa" name="dns2" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 12" data-toggle="modal" data-target="#searchDiagnosa" name="dns12" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 22" data-toggle="modal" data-target="#searchDiagnosa" name="dns22" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 3" data-toggle="modal" data-target="#searchDiagnosa" name="dns3" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 13" data-toggle="modal" data-target="#searchDiagnosa" name="dns13" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 23" data-toggle="modal" data-target="#searchDiagnosa" name="dns23" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 4" data-toggle="modal" data-target="#searchDiagnosa" name="dns4" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 14" data-toggle="modal" data-target="#searchDiagnosa" name="dns14" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 24" data-toggle="modal" data-target="#searchDiagnosa" name="dns24" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 5" data-toggle="modal" data-target="#searchDiagnosa" name="dns1" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 15" data-toggle="modal" data-target="#searchDiagnosa" name="dns15" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 25" data-toggle="modal" data-target="#searchDiagnosa" name="dns25" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 6" data-toggle="modal" data-target="#searchDiagnosa" name="dns6" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 16" data-toggle="modal" data-target="#searchDiagnosa" name="dns16" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 26" data-toggle="modal" data-target="#searchDiagnosa" name="dns26" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 7" data-toggle="modal" data-target="#searchDiagnosa" name="dns7" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 17" data-toggle="modal" data-target="#searchDiagnosa" name="dns17" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 27" data-toggle="modal" data-target="#searchDiagnosa" name="dns27" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 8" data-toggle="modal" data-target="#searchDiagnosa" name="dns8" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 18" data-toggle="modal" data-target="#searchDiagnosa" name="dns18" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 28" data-toggle="modal" data-target="#searchDiagnosa" name="dns28" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 9" data-toggle="modal" data-target="#searchDiagnosa" name="dns9" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 19" data-toggle="modal" data-target="#searchDiagnosa" name="dns19" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 29" data-toggle="modal" data-target="#searchDiagnosa" name="dns29" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 10" data-toggle="modal" data-target="#searchDiagnosa" name="dns10" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Diagnosa Sekunder 20" data-toggle="modal" data-target="#searchDiagnosa" name="dns20" style="width:190px;" ></td>
										<td></td>
									</tr>
									<tr>
										<td colspan="5">&nbsp;</td>
									</tr>
									<tr>
										<td width="20">18.</td>
										<td width="30%">Prosedur/Tindakan ICD-9-CM</td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 1" data-toggle="modal" data-target="#searchICD9" name="dns1" style="width:190px;"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 11" data-toggle="modal" data-target="#searchICD9" name="dns11" style="width:190px;"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 21" data-toggle="modal" data-target="#searchICD9" name="dns21" style="width:190px;"></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 2" data-toggle="modal" data-target="#searchICD9" name="dns2" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 12" data-toggle="modal" data-target="#searchICD9" name="dns12" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 22" data-toggle="modal" data-target="#searchICD9" name="dns22" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 3" data-toggle="modal" data-target="#searchICD9" name="dns3" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 13" data-toggle="modal" data-target="#searchICD9" name="dns13" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 23" data-toggle="modal" data-target="#searchICD9" name="dns23" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 4" data-toggle="modal" data-target="#searchICD9" name="dns4" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 14" data-toggle="modal" data-target="#searchICD9" name="dns14" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 24" data-toggle="modal" data-target="#searchICD9" name="dns24" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 5" data-toggle="modal" data-target="#searchICD9" name="dns1" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 15" data-toggle="modal" data-target="#searchICD9" name="dns15" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 25" data-toggle="modal" data-target="#searchICD9" name="dns25" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 6" data-toggle="modal" data-target="#searchICD9" name="dns6" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 16" data-toggle="modal" data-target="#searchICD9" name="dns16" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 26" data-toggle="modal" data-target="#searchICD9" name="dns26" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 7" data-toggle="modal" data-target="#searchICD9" name="dns7" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 17" data-toggle="modal" data-target="#searchICD9" name="dns17" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 27" data-toggle="modal" data-target="#searchICD9" name="dns27" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 8" data-toggle="modal" data-target="#searchICD9" name="dns8" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 18" data-toggle="modal" data-target="#searchICD9" name="dns18" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 28" data-toggle="modal" data-target="#searchICD9" name="dns28" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 9" data-toggle="modal" data-target="#searchICD9" name="dns9" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 19" data-toggle="modal" data-target="#searchICD9" name="dns19" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 29" data-toggle="modal" data-target="#searchICD9" name="dns29" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%"></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 10" data-toggle="modal" data-target="#searchICD9" name="dns10" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 20" data-toggle="modal" data-target="#searchICD9" name="dns20" style="width:190px;" ></td>
										<td><input type="text" class="form-control input-sm" placeholder="Prosedur/tindakan 30" data-toggle="modal" data-target="#searchICD9" name="dns30" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20">19.</td>
										<td width="30%">Record ID/No. Urut dalam file tersebut</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="urutfile" style="width:190px;"></td>
									</tr>
									<tr>
										<td width="20">20.</td>
										<td width="30%">Kode CBG</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="kodecbg" style="width:190px;" readonly></td>
									</tr>
									<tr>
										<td width="20">21.</td>
										<td width="30%">Tarif CBG</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="tarifcbg" style="width:190px;" readonly></td>
									</tr>
									<tr>
										<td width="20">22.</td>
										<td width="30%">Deskripsi CBG</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="deskripsicbg" style="width:190px;" readonly></td>
									</tr>
									<tr>
										<td width="20">23.</td>
										<td width="30%">ALOS</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="alos" value="0" style="width:130px;" readonly></td>
									</tr>
									<tr>
										<td width="20">24.</td>
										<td width="30%">Nama Pasien</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="namapasien" style="width:190px;" readonly></td>
									</tr>
									<tr>
										<td width="20">25.</td>
										<td width="30%">Dokter Penanggung Jawab</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="dokterpj" placeholder="Search Dokter" data-toggle="modal" data-target="#searchDokter" style="width:190px;" ></td>
									</tr>
									<tr>
										<td width="20">26.</td>
										<td width="30%">Nomor SKP</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="noskp" style="width:190px;"></td>
									</tr>
									<tr>
										<td width="20"></td>
										<td width="30%">Nomor Kartu Peserta</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="nokartupeserta" style="width:190px;"></td>
									</tr>
									<tr>
										<td width="20">27.</td>
										<td width="30%">Surat Rujukan</td>
										<td colspan="3">
											<div class="input-group col-md-3">
												<select class="form-control input-sm" name="suratrujukan" id="suratrujukan">
													<option value="" selected>Pilih</option>
													<option value="ada">Ada</option>
													<option value="tanpa surat rujukan">Tanpa Surat Rujukan</option>
																			
												</select>
											</div>		
										</td>
									</tr>
									<tr>
										<td width="20">28.</td>
										<td width="30%">BHP (jika ada)</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="bhp" style="width:190px;"></td>
									</tr>
									<tr>
										<td width="20">29.</td>
										<td width="30%">Harga BHP</td>
										<td colspan="3">
											<div class="input-group col-md-3">
												<span class="input-group-addon" id="basic-addon1">Rp.</span>
												<input type="text" class="form-control input-sm" name="hargabhp">
											</div>
										</td>
									</tr>
									<tr>
										<td width="20">30.</td>
										<td width="30%">Severiti level 3</td>
										<td colspan="3">
											<div class="input-group col-md-3">
												<select class="form-control input-sm" name="severitilv3" id="severitilv3">
													<option value="" selected>Pilih</option>
													<option value="ada">Ada</option>
													<option value="tidak ada">Tidak Ada</option>
																			
												</select>
											</div>	
										</td>
									</tr>
									<tr>
										<td width="20">31.</td>
										<td width="30%">Tipe Tarif sesuai Rumah Sakit</td>
										<td colspan="3"><input type="text" class="form-control input-sm" name="tarifsesuairs" style="width:190px;"></td>
									</tr>

								</table>
		       				</div>
			        		<br>
			        		<div class="modal-footer">
			        			<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
			 			     	<button type="submit" class="btn btn-success" id="">Simpan</button>
						    </div>
						</form>
					</div>
				</div>
			</form>
		</div> 

	</div>

</div>

<script type="text/javascript">

	$(document).ready(function(){
		var lastSearch = null;
		if(lastSearch==null){
			$("#tgl_start").attr('disabled', true);
			$("#tgl_end").attr('disabled', true);
		}

		$('#searchkey').keyup(function(){
			var check = $(this).val();

			if(check == ""){
				$.ajax({
					type:'POST',
					url :'<?php echo base_url()?>rawatjalan/homerawatjalan/get_antrian',
					success:function(data){
						// $("#t_body").html(hasil);

						console.log(data);
						var t = $('#table_search').DataTable();
						
						t.clear().draw();
						
						if(data.length>0){
							
							for(var i = 0; i<data.length;i++){
								var rm_id = data[i]['rj_id'],
									name = data[i]['nama'],									
									jk = data[i]['jenis_kelamin'],
									tgl_lahir = data[i]['tanggal_lahir'],
									alamat = data[i]['alamat_skr'],
									id = data[i]['jenis_id'],
									visit_id = data[i]['visit_id'];;

								var remove = tgl_lahir.split("-");
								var bulan;
								switch(remove[1]){
									case "01": bulan="Januari";break;
									case "02": bulan="Februari";break;
									case "03": bulan="Maret";break;
									case "04": bulan="April";break;
									case "05": bulan="Mei";break;
									case "06": bulan="Juni";break;
									case "07": bulan="Juli";break;
									case "08": bulan="Agustus";break;
									case "09": bulan="September";break;
									case "10": bulan="Oktober";break;
									case "11": bulan="November";break;
									case "12": bulan="Desember";break;
								}
								var tgl = remove[2]+" "+bulan+" "+remove[0];

								// $('#t_body').append(
								// 	'<tr>'+
								// 		'<td>'+(i+1)+'</td>'+
							 // 			'<td>'+rm_id+'</td>'+
							 // 			'<td>'+name+'</td>'+
							 // 			'<td>'+jk+'</td>'+
							 // 			'<td>'+tgl+'</td>'+
							 // 			'<td>'+alamat+'</td>'+
							 // 			'<td>'+id+'</td>'+

							 // 			'<td style="text-align:center">'+
							 // 				'<a href="<?php echo base_url() ?>rawatjalan/daftarpasien/periksa/'+rm_id+'/'+visit_id+'"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a>'+
								// 		'</td>'+
							 // 		'</tr>'
								// 	);

								var action = '<a href="<?php echo base_url() ?>rawatjalan/daftarpasien/periksa/'+rm_id+'/'+visit_id+'" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a>';
								
								t.row.add([
									(i+1),
									rm_id,
									name,
									jk,
									tgl,
									alamat,
									id,
									action,
									i
								]).draw();
							}
						}

					},
					error:function (data){
						$('#t_body').empty();

						$('#t_body').append(
							'<tr>'+
					 			'<td colspan="7"><center>Error</center></td>'+
					 		'</tr>'
						);
					}

				});
			}
		});

		$("#search_submit").submit(function(event){
			var search = $("input:first").val();
			lastSearch = search;
			var data = {};
			data['search'] = search;
			$("#tgl_start").attr('disabled', false);
			$("#tgl_end").attr('disabled', false);	

			if(search!=""){
				$.ajax({
					type:'POST',
					data:data,
					url :'<?php echo base_url()?>rawatjalan/homerawatjalan/search_pasien',
					success:function(data){
						// $("#t_body").html(hasil);

						console.log(data);
						var t = $('#table_search').DataTable();
						
						t.clear().draw();

						if(data.length>0){

							for(var i = 0; i<data.length;i++){
								var rm_id = data[i]['rj_id'],
									name = data[i]['nama'],									
									jk = data[i]['jenis_kelamin'],
									tgl_lahir = data[i]['tanggal_lahir'],
									alamat = data[i]['alamat_skr'],
									id = data[i]['jenis_id'],
									visit_id = data[i]['visit_id'];;

								var remove = tgl_lahir.split("-");
								var bulan;
								switch(remove[1]){
									case "01": bulan="Januari";break;
									case "02": bulan="Februari";break;
									case "03": bulan="Maret";break;
									case "04": bulan="April";break;
									case "05": bulan="Mei";break;
									case "06": bulan="Juni";break;
									case "07": bulan="Juli";break;
									case "08": bulan="Agustus";break;
									case "09": bulan="September";break;
									case "10": bulan="Oktober";break;
									case "11": bulan="November";break;
									case "12": bulan="Desember";break;
								}
								var tgl = remove[2]+" "+bulan+" "+remove[0];

								// $('#t_body').append(
								// 	'<tr>'+
								// 		'<td>'+(i+1)+'</td>'+
							 // 			'<td>'+rm_id+'</td>'+
							 // 			'<td>'+name+'</td>'+
							 // 			'<td>'+jk+'</td>'+
							 // 			'<td>'+tgl+'</td>'+
							 // 			'<td>'+alamat+'</td>'+
							 // 			'<td>'+id+'</td>'+

							 // 			'<td style="text-align:center">'+
							 // 				'<a href="<?php echo base_url() ?>rawatjalan/daftarpasien/periksa/'+rm_id+'/'+visit_id+'" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a>'+
								// 		'</td>'+
							 // 		'</tr>'
								// 	);
								var action = '<center><a href="<?php echo base_url() ?>rawatjalan/daftarpasien/periksa/'+rm_id+'/'+visit_id+'" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a></center>';

								t.row.add([
									'<center>'+(i+1)+'</center>',
									rm_id,
									name,
									jk,
									'<center>'+tgl+'</center>',
									alamat,
									id,
									action,
									i
								]).draw();
							}
						}

					},
					error:function (data){
						$('#t_body').empty();

						$('#t_body').append(
							'<tr>'+
					 			'<td colspan="7"><center>Error</center></td>'+
					 		'</tr>'
						);
					}

				});
			}

			event.preventDefault();
		});	

		var item_start = {};
		$("#tgl_start").change(function(event){
			item_start['search'] = lastSearch;
			item_start['start'] = $('#tgl_start').val();
			item_start['end'] = $('#tgl_end').val();

			console.log(item_start);
			$.ajax({
				type:'POST',
				data:item_start,
				url :'<?php echo base_url()?>icu/homeicu/filter_pasien/',
				success:function(data){
					// $("#t_body").html(hasil);

					console.log(data);
					
					if(data.length>0){
						$('#t_body').empty();
						for(var i = 0; i<data.length;i++){
							var rm_id = data[i]['rj_id'],
								name = data[i]['nama'],									
								jk = data[i]['jenis_kelamin'],
								tgl_lahir = data[i]['tanggal_lahir'],
								alamat = data[i]['alamat_skr'],
								id = data[i]['jenis_id'],
								visit_id = data[i]['visit_id'];;

							var remove = tgl_lahir.split("-");
							var bulan;
							switch(remove[1]){
								case "01": bulan="Januari";break;
								case "02": bulan="Februari";break;
								case "03": bulan="Maret";break;
								case "04": bulan="April";break;
								case "05": bulan="Mei";break;
								case "06": bulan="Juni";break;
								case "07": bulan="Juli";break;
								case "08": bulan="Agustus";break;
								case "09": bulan="September";break;
								case "10": bulan="Oktober";break;
								case "11": bulan="November";break;
								case "12": bulan="Desember";break;
							}
							var tgl = remove[2]+" "+bulan+" "+remove[0];

							$('#t_body').append(
								'<tr>'+
						 			'<td>'+rm_id+'</td>'+
						 			'<td>'+name+'</td>'+
						 			'<td>'+jk+'</td>'+
						 			'<td>'+tgl+'</td>'+
						 			'<td>'+alamat+'</td>'+
						 			'<td>'+id+'</td>'+

						 			'<td style="text-align:center">'+
						 				'<a href="<?php echo base_url() ?>rawatjalan/daftarpasien/periksa/'+rm_id+'/'+visit_id+'" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a>'+
									'</td>'+
						 		'</tr>'
								);
						}
					}else{
						$('#t_body').empty();

						$('#t_body').append(
								'<tr>'+
						 			'<td colspan="7"><center>Data Pasien Tidak Ditemukan</center></td>'+
						 		'</tr>'
							);
					}

				},
				error:function (data){
					$('#t_body').empty();

					$('#t_body').append(
						'<tr>'+
				 			'<td colspan="7"><center>Error</center></td>'+
				 		'</tr>'
					);
				}

			});

			event.preventDefault();
		});

		$("#tgl_end").change(function(event){
			item_start['search'] = lastSearch;
			item_start['start'] = $('#tgl_start').val();
			item_start['end'] = $('#tgl_end').val();

			console.log(item_start);
			$.ajax({
				type:'POST',
				data:item_start,
				url :'<?php echo base_url()?>icu/homeicu/filter_pasien/',
				success:function(data){
					// $("#t_body").html(hasil);

					console.log(data);
					
					if(data.length>0){
						$('#t_body').empty();
						for(var i = 0; i<data.length;i++){
							var rm_id = data[i]['rj_id'],
								name = data[i]['nama'],									
								jk = data[i]['jenis_kelamin'],
								tgl_lahir = data[i]['tanggal_lahir'],
								alamat = data[i]['alamat_skr'],
								id = data[i]['jenis_id'],
								visit_id = data[i]['visit_id'];

							var remove = tgl_lahir.split("-");
							var bulan;
							switch(remove[1]){
								case "01": bulan="Januari";break;
								case "02": bulan="Februari";break;
								case "03": bulan="Maret";break;
								case "04": bulan="April";break;
								case "05": bulan="Mei";break;
								case "06": bulan="Juni";break;
								case "07": bulan="Juli";break;
								case "08": bulan="Agustus";break;
								case "09": bulan="September";break;
								case "10": bulan="Oktober";break;
								case "11": bulan="November";break;
								case "12": bulan="Desember";break;
							}
							var tgl = remove[2]+" "+bulan+" "+remove[0];

							$('#t_body').append(
								'<tr>'+
						 			'<td>'+visit_id	+'</td>'+
						 			'<td>'+name+'</td>'+
						 			'<td>'+jk+'</td>'+
						 			'<td>'+tgl+'</td>'+
						 			'<td>'+alamat+'</td>'+
						 			'<td>'+id+'</td>'+

						 			'<td style="text-align:center">'+
						 				'<a href="<?php echo base_url() ?>rawatjalan/daftarpasien/periksa/'+rm_id+'/'+visit_id+'"><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a>'+
									'</td>'+
						 		'</tr>'
								);
						}
					}else{
						$('#t_body').empty();

						$('#t_body').append(
								'<tr>'+
						 			'<td colspan="7"><center>Data Pasien Tidak Ditemukan</center></td>'+
						 		'</tr>'
							);
					}

				},
				error:function (data){
					$('#t_body').empty();

					$('#t_body').append(
						'<tr>'+
				 			'<td colspan="7"><center>Error</center></td>'+
				 		'</tr>'
					);
				}
			});
			event.preventDefault();
		});

		$('#submitTagihanSearch').submit(function(event){
			event.preventDefault();

			var item = {};
			item['search'] = $('#search_tagihan').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo  base_url(); ?>rawatjalan/homerawatjalan/search_tagihan',
				success:function(data){
					console.log(data);
					var t = $('#table_tagihan').DataTable();
					var no = 0;
					var action;
					t.clear().draw();

					for(var i=0; i<data.length; i++){
						no++;
						if(data[i]['carapembayaran'] == "BPJS"){
							action = '<a href="<?php echo base_url() ?>rawatjalan/invoicebpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}else{
							action = '<a href="<?php echo base_url() ?>rawatjalan/invoicenonbpjs/invoice/'+data[i]['no_invoice']+'" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="Tambah Tagihan"></i></a>';
						}

						t.row.add([
							no,
							data[i]['nama_dept'],
							data[i]['no_invoice'],
							data[i]['visit_id'],
							data[i]['rm_id'],
							data[i]['nama'],
							data[i]['alamat_skr'],
							data[i]['carapembayaran'],
							action,
							i
						]).draw();
					}

				}
			});
		});


		//------------------------ farmasi here ------------------------//
		$('#submitfilterfarmasiunit').submit(function (e) {
			e.preventDefault();
			var filter = {};
			filter['filterby'] = $('#filterInv').find('option:selected').val();
			filter['valfilter'] = $('#filterby').val();
			submit_filter(filter);
		})

		$('#expired').on('click', function (e) {
			e.preventDefault();
			var filter = {};
			filter['expired'] = '0';
			submit_filter(filter)
		})

		$('#expired3').on('click', function (e) {
			e.preventDefault();
			var filter = {};
			filter['expired'] = '3';
			submit_filter(filter)
		})

		$('#expired6').on('click', function (e) {
			e.preventDefault();
			var filter = {};
			filter['expired'] = '6';
			submit_filter(filter);
		})

		$('#formobatfarmasi').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['katakunci'] = $('#katakuncifarmasi').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>bersalin/homebersalin/get_obat_gudang',
				success: function (data) {
					console.log(data);
					$('#tbodyobatpermintaanfarmasi').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodyobatpermintaanfarmasi').append(
								'<tr>'+
									'<td style="display:none">'+data[i]['obat_detail_id']+'</td>'+
									'<td style="display:none">'+data[i]['tgl_kadaluarsa']+'</td>'+
									'<td style="display:none">'+data[i]['obat_id']+'</td>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['total_stok']+'</td>'+
									'<td>'+format_date(data[i]['tgl_kadaluarsa'])+'</td>'+
									'<td style="text-align:center"><a href="#" class="addNewMintaFar"><i class="glyphicon glyphicon-check"></i></a></td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodyobatpermintaanfarmasi').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tbodyobatpermintaanfarmasi').on('click','tr td a.addNewMintaFar', function (e) {
			e.preventDefault();

			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputMintaFar').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputMintaFar').append(
				'<tr><td style="display:none">'+cols[0]+'</td>'+
				'<td style="display:none">'+cols[2]+'</td>'+
				'<td>'+cols[3]+'</td>'+
				'<td>'+format_date(cols[1])+'</td>'+
				'<td>'+cols[4]+'</td>'+
				'<td>'+cols[5]+'</td>'+
				'<td>'+cols[6]+'</td>'+
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>'
			)
		})

		$('#permintaanfarmasibersalin').submit(function (e) {
			e.preventDefault();
			$('#addinputMintaFar').find('tr td.dataKosong').closest('tr').remove();
			var item = {};
			item['no_permintaan'] = $('#noPermFarmBers').val();
			item['tanggal_request'] = $('#tglpermintaanfarmasi').val();
			item['keterangan_request'] = $('#ketObatFarBers').val();

			//jlh = 9, obat_id = 1, obat_detail_id = 0
			var data = [];
		    $('#addinputMintaFar').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				$('#addinputMintaFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;
		    var a = confirm('yakin disimpan ?');
		    if (a == true) {
			    $.ajax({
					type: "POST",
					data: item,
					url: '<?php echo base_url()?>rawatjalan/homerawatjalan/submit_permintaan_bersalin',
					success: function (data) {
						console.log(data);
						if (data['error'] == 'n'){
							$('#addinputMintaFar').empty();
							$('#noPermFarmBers').val('');
							$('#ketObatFarBers').val('');
							$('#addinputMintaFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
						}
						myAlert(data['message']);
					},
					error: function (data) {
						console.log(data);
					}
				})
			};
		})

		$('#batalpermintaanfarmasi').on('click',function (e) {
			e.preventDefault();
			$('#addinputMintaFar').empty();
			$('#addinputMintaFar').append('<tr><td colspan="6" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
		})

		$('#formsubmitretur').submit(function (e) {
			e.preventDefault();
			$('#addinputRetFar').find('tr td.dataKosong').closest('tr').remove();
			var item = {};
			item['no_returdept'] = $('#noRetFarBers').val();
			item['waktu'] = $('#waktureturbersalin').val();
			item['keterangan'] = $('#ketObatRetFarBers').val();

			//jlh = 8, obat_id = 1, obat_detail_id = 0
			var data = [];
		    $('#addinputRetFar').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				 $('#addinputRetFar').append('<tr><td colspan="7" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;
		    var a = confirm('yakin diproses ?');
		    if (a == true) {
			    $.ajax({
					type: "POST",
					data: item,
					url: '<?php echo base_url()?>rawatjalan/homerawatjalan/submit_retur_bersalin',
					success: function (data) {
						console.log(data);
						if (data['error'] == 'n'){
							$('#addinputRetFar').empty();
							$('#noRetFarBers').val('');
							$('#ketObatRetFarBers').val('');
						}
						myAlert(data['message']);
					},
					error: function (data) {
						console.log(data);
					}
				})
			};
		})
		
		$('#formsearchobatretur').submit(function (e) {
			e.preventDefault();
			var item ={};
			item['katakunci'] = $('#katakuncireturbersalin').val();

			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>rawatjalan/homerawatjalan/get_obat_retur',
				success: function (data) {
					//console.log(data);
					$('#tbodyreturbersalin').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodyreturbersalin').append(
								'<tr>'+
									'<td style="display:none">'+data[i]['obat_detail_id']+'</td>'+
									'<td style="display:none">'+data[i]['tgl_kadaluarsa']+'</td>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['total_stok']+'</td>'+
									'<td>'+format_date(data[i]['tgl_kadaluarsa'])+'</td>'+
									'<td style="text-align:center"><a href="#" class="addNewReturFar"><i class="glyphicon glyphicon-check"></i></a></td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodyreturbersalin').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tbodyreturbersalin').on('click', 'tr td a.addNewReturFar', function (e) {
			e.preventDefault();
			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputRetFar').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputRetFar').append(
				'<tr><td style="display:none">'+cols[0]+'</td>'+//obat detail id
				'<td>'+cols[2]+'</td>'+  //nama
				'<td>'+format_date(cols[1])+'</td>'+ //tanggal kadaluarsa
				'<td>'+cols[3]+'</td>'+ //satuan
				'<td>'+cols[4]+'</td>'+ //merk
				'<td>'+cols[5]+'</td>'+ //stok unit
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah retur
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td></tr>'
			)
		})

		$('#permintaanbarangunit').submit(function (e) {
			e.preventDefault();
			var item = {};
			item['no_permintaanbarang'] = $('#nomorpermintaanbarang').val();
			item['tanggal_request'] = $('#tglpermintaanbarang').val();
			item['keterangan_request'] = $('#keteranganpermintaanbarang').val();

			var data = [];
			$('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
		    $('#addinputmintabarang').find('tr').each(function (rowIndex, r) {
		        var cols = [];
		        $(this).find('td').each(function (colIndex, c) {
		            cols.push(c.textContent);
		        });
		        $(this).find('td input[type=number]').each(function (colIndex, c) {
		            cols.push(c.value);
		        });
		        data.push(cols);
		    });
			if(data.length == 0){
				$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
				myAlert('detail tidak ada, isi data dengan benar');
				return false;
			}

		    item['data'] = data;

		    $.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>rawatjalan/homerawatjalan/submit_permintaan_barangunit',
				success: function (data) {
					console.log(data);
					if (data['error'] == 'n'){
						$('#addinputmintabarang').empty();
						$('#addinputmintabarang').append('<tr><td colspan="8" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
						$('#nomorpermintaanbarang').val('');
						$('#keteranganpermintaanbarang').val('');
					}
					myAlert(data['message']);
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#formmintabarang').submit(function (e) {
			e.preventDefault();
			var item ={};
			item['katakunci'] = $('#katakuncimintabarang').val();
			$.ajax({
				type: "POST",
				data: item,
				url: '<?php echo base_url()?>bersalin/homebersalin/get_barang_gudang',
				success: function (data) {
					console.log(data);//return false;
					$('#tbodybarangpermintaan').empty();
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#tbodybarangpermintaan').append(
								'<tr>'+
									'<td>'+data[i]['nama']+'</td>'+
									'<td>'+data[i]['satuan']+'</td>'+
									'<td>'+data[i]['nama_merk']+'</td>'+
									'<td>'+data[i]['tahun_pengadaan']+'</td>'+
									'<td>'+data[i]['stok_gudang']+'</td>'+
									'<td style="text-align:center"><a href="#" class="addnewpermintaanbarang"><i class="glyphicon glyphicon-check"></i></a></td>'+
									'<td style="display:none">'+data[i]['barang_stok_id']+'</td>'+
									'<td style="display:none">'+data[i]['barang_id']+'</td>'+
								'</tr>'
							)
						};
					}else{
						$('#tbodybarangpermintaan').append('<tr><td style="text-align:center" colspan="6">Data tidak ditemukan</td></tr>');
					} 
				},
				error: function (data) {
					console.log(data);
				}
			})
		})

		$('#tbodybarangpermintaan').on('click', 'tr td a.addnewpermintaanbarang',function (e) {
			e.preventDefault();
			var cols = [];
	        $(this).closest('tr').find('td').each(function (colIndex, c) {
	            cols.push(c.textContent);
	        });

	        $('#addinputmintabarang').find('tr td.dataKosong').closest('tr').remove();
			$('#addinputmintabarang').append(
				'<tr><td>'+cols[0]+'</td>'+//nama
				'<td>'+cols[1]+'</td>'+  //satuan
				'<td>'+cols[2]+'</td>'+ //merk
				'<td>'+cols[3]+'</td>'+ //tahun pengadaan
				'<td>'+cols[4]+'</td>'+ //stok gudang
				'<td><input type="number" class="form-control" style="width:90px" placeholder="0"></td>'+ //jumlah minta
				'<td style="text-align:center"><a href="#" class="removeRow"><i class="glyphicon glyphicon-remove"></i></a></td>'+
				'<td style="display:none">'+cols[6]+'</td>'+ //barang_stok_id
				'<td style="display:none">'+cols[7]+'</td></tr>' //barang_id
			)
		})

		//-------------- master here ---------------//
		var autodata = [];
		var iddata = [];
		$('#paramedis').focus(function(){
			var $input = $('#paramedis');
			
			if(autodata.length==0){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>rawatjalan/daftarpasien/get_dokter',
					success:function(data){
						console.log(data);
						for(var i = 0; i<data.length; i++){
							autodata.push(data[i]['nama_petugas']);
							iddata.push(data[i]['petugas_id']);
						}
						console.log(autodata);

						$input.typeahead({source:autodata, 
				            autoSelect: true}); 

						$input.change(function() {
						    var current = $input.typeahead("getActive");
						    var index = autodata.indexOf(current);

						    $('#paramedis_id').val(iddata[index]);
						    
						    if (current) {
						        // Some item from your model is active!
						        if (current.name == $input.val()) {
						            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
						        } else {
						            // This means it is only a partial match, you can either add a new item 
						            // or take the active if you don't want new items
						        }
						    } else {
						        // Nothing is active so it is a new value (or maybe empty value)
						    }
						});
					}
				});
			}
		});

		$('#batalreturfarmasi').on('click',function (e) {
			e.preventDefault();
			$('#addinputRetFar').empty();
			$('#addinputRetFar').append('<tr><td colspan="6" style="text-align:center" class="dataKosong">DATA KOSONG</td></tr>');
		})

		var this_io_obat;
		$('#tbodyinventoriunit').on('click','tr td a.inoutobat', function (e) {
			e.preventDefault();
			this_io_obat = $(this);
			var obat_dept_id = $(this).closest('tr').find('td .barangobat_dept_id').val();
			var jlh = $(this).closest('tr').find('td').eq(5).text();

			$('#inout_obat_dept_id').val(obat_dept_id);
			$('#sisaInOutBer').val(jlh);

			$('#jmlInOutBer').on('change', function (e) {
				e.preventDefault();

				var is_in = $('#iober').find('option:selected').val();
				var jmlInOut = $('#jmlInOutBer').val();
				var sisa = jlh;//$('#sisaInOut').val();
				var hasil ="";
				if (is_in == 'IN') {
					hasil = Number(jmlInOut) + Number(sisa);
				}else{			
					hasil = Number(sisa) - Number(jmlInOut);
				}

				if (jmlInOut == '') {
					hasil = Number(sisa);
				}
				$('#sisaInOutBer').val(hasil);			
			})

		})

		$('#submitinoutunit').submit(function (e) {
			e.preventDefault();

			var item = {};
			item['obat_dept_id'] = $('#inout_obat_dept_id').val();
			item['jumlah'] = $('#jmlInOutBer').val();
			item['sisa'] = $('#sisaInOutBer').val();
			item['is_out'] = $('#iober').find('option:selected').val();
			item['tanggal'] = $('#tglInOut').val();
		    item['keterangan'] = $('#keteranganIO').val();

		    if (item['jumlah'] != "") {
			    $.ajax({
			    	type: "POST",
			    	data: item,
			    	url: "<?php echo base_url()?>bersalin/homebersalin/input_in_out",
			    	success: function (data) {

			    		if (data == "true") {
			    			myAlert('data berhasil disimpan');
			    			$('#keteranganIO').val('');
			    			$('#jmlInOutBer').val('');
			    			this_io_obat.closest('tr').find('td').eq(5).text(item['sisa']);
			    			$('#inout').modal('hide');	
			    		} else{
			    			myAlert('gagal, terdapat kesalahan');
			    		};
			    		
			    	},
			    	error: function (data) {
			    		myAlert('gagal');
			    	}
			    })
			} else{
				myAlert('isi data dengan benar');
				$('#jmlInOutBer').focus();
			};		
		})

		$("#tbodyinventoriunit").on('click', 'tr td a.printobat', function (e) {
			var obat_dept_id = $(this).closest('tr').find('td .barangobat_dept_id').val();

			 $.ajax({
		    	type: "POST",
		    	url: "<?php echo base_url()?>farmasi/homegudangobat/get_detail_obat_bydeptid/" + obat_dept_id, //benar
		    	success: function (data) {
		    		console.log(data);
		    		$('#tbodydetailobatinventori').empty();
		    		for(var i = 0; i < data.length ; i++){
		    			var a = "";
		    			var jlh = "";
		    			if(data[i]['masuk'] == 0) {a = "OUT"} else a = "IN";
		    			if(data[i]['masuk'] == 0)  {jlh = data[i]['keluar']} else jlh = data[i]['masuk'];
		    			$('#tbodydetailobatinventori').append(
							'<tr>'+
								'<td style="text-align:center">'+format_date(data[i]['tanggal'])+'</td>'+
								'<td>'+a+'</td>'+
								'<td>'+jlh+'</td>'+
								'<td>'+data[i]['total_stok']+'</td>'+
							'</tr>'
		    			)
		    		}
		    	},
		    	error: function (data) {
		    		myAlert('gagal');
		    	}
		    })
		})

		//--------------------barang here--------------------------//
		var this_io;
		$('#tbodyinventoribarang').on('click', 'tr td a.edBarang', function (e) {
			e.preventDefault();
			$('#id_barang_inoutprocess').val($(this).closest('tr').find('td .barang_detail_inout').val());
			var jlh = $(this).closest('tr').find('td').eq(4).text();
			this_io = $(this);
			$('#sisaInOut').val(jlh);

			$('#jmlInOut').on('change', function (e) {
				e.preventDefault();

				var is_in = $('#io').find('option:selected').val();
				var jmlInOut = $('#jmlInOut').val();
				var sisa = jlh;//$('#sisaInOut').val();
				var hasil ="";
				if (is_in == 'IN') {
					hasil = Number(jmlInOut) + Number(sisa);
				}else{			
					hasil = Number(sisa) - Number(jmlInOut);
				}

				if (jmlInOut == '') {
					hasil = Number(sisa);
				}
				$('#sisaInOut').val(hasil);			
			})

			$('#io').on('change', function () {
				var jumlah = Number($('#jmlInOut').val());
				var sisa = Number(jlh);//Number($('#sisaInOut').val());

				var isout = $('#io').find('option:selected').val();
				if (isout === 'IN') {
					$('#sisaInOut').val(jumlah + sisa);
				} else{
					$('#sisaInOut').val(sisa - jumlah);
				};
			})
		})
		
		$('#forminoutbarang').submit(function (e) {
			e.preventDefault();

			var item = {};
			item['barang_detail_id'] = $('#id_barang_inoutprocess').val();
			item['jumlah'] = $('#jmlInOut').val();
			item['sisa'] = $('#sisaInOut').val();
			item['is_out'] = $('#io').find('option:selected').val();
		    item['tanggal'] = $('#tanggalinout').val();
		    item['keterangan'] = $('#keteranganIObarang').val();
		    
		    if (item['jumlah'] != "") {
			    $.ajax({
			    	type: "POST",
			    	data: item,
			    	url: "<?php echo base_url()?>rawatjalan/homerawatjalan/input_in_outbarang",
			    	success: function (data) {
			    		if (data == "true") {
			    			myAlert('data berhasil disimpan');
			    			$('#keteranganIO').val('');
			    			$('#jmlInOut').val('');
			    			this_io.closest('tr').find('td').eq(4).text(item['sisa']);
			    			$('#inoutbar').modal('hide');	
			    		} else{
			    			myAlert('gagal, terdapat kesalahan');
			    		};
			    	},
			    	error: function (data) {
			    		myAlert('gagal');
			    	}
			    })
			} else{
				myAlert('isi data dengan benar');
				$('#jmlInOut').focus();
			};			
		})

		$("#tbodyinventoribarang").on('click', 'tr td a.detailinvenbarang', function (e) {
			var id = $(this).closest('tr').find('td .barang_detail_inout').val();

			 $.ajax({
		    	type: "POST",
		    	url: "<?php echo base_url()?>rawatjalan/homerawatjalan/get_detail_inventori/" + id,
		    	success: function (data) {
		    		console.log(data);
		    		$('#tbodydetailbrginventori').empty();
		    		for(var i = 0; i < data.length ; i++){
		    			$('#tbodydetailbrginventori').append(
							'<tr>'+
								'<td>'+format_date(data[i]['tanggal'])+'</td>'+
								'<td>'+data[i]['is_out']+'</td>'+
								'<td>'+data[i]['jumlah']+'</td>'+
								'<td>'+data[i]['keterangan']+'</td>'+
							'</tr>'
		    			)
		    		}
		    	},
		    	error: function (data) {
		    		myAlert('gagal');
		    	}
		    })
		})

		$('#btn_filter_jaspel').click(function(event){
			event.preventDefault();
			var item = {};
			item['mulai'] = $('#mulai_date').val();
			item['sampai'] = $('#sampai_date').val();
			item['carabayar'] = $('#carabayar').val();
			item['paramedis'] = "";

			if($('#paramedis').val() != "")
				item['paramedis'] = $('#paramedis_id').val();

			$.ajax({
				type:'POST',
				data:item,
				url:'<?php echo base_url(); ?>rawatjalan/homerawatjalan/search_jasapelayanan',
				success:function(data){
					
					var t = $('#tabelJpPoliinap').DataTable();

					t.clear().draw();

					for(var i = 0; i<data.length; i++){
						t.row.add([
							(i+1),
							data[i]['waktu_tindakan'],
							data[i]['nama_tindakan'],
							data[i]['cara_bayar'],
							data[i]['nama_petugas'],
							data[i]['paramedis_lain'],
							data[i]['jp'],
							i
						]).draw();
					}

					console.log(data);
				},error:function(data){
					alert('error');
					console.log(data);

				}
			});
		});

	});

	function submit_filter (filter) {
		$.ajax({
			type: "POST",
			data: filter,
			url: "<?php echo base_url()?>rawatjalan/homerawatjalan/submit_filter_farmasi",
			success:function (data) {
				console.log(data);
				$('#tbodyinventoriunit').empty();
				var t = $('#tabelinventoriunit').DataTable();

				t.clear().draw();
				for (var i =  0; i < data.length; i++) {
					var last = '<a href="#" class="inoutobat" data-toggle="modal" data-target="#inout"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>'+
								'<a href="#edInvenBer" data-toggle="modal" class="printobat"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Riwayat"></i></a>'+
								'<input type="hidden" class="barangmerk_id" value="'+data[i]['merk_id']+'">'+
								'<input type="hidden" class="barangjenis_obat_id" value="'+data[i]['jenis_obat_id']+'">'+
								'<input type="hidden" class="barangsatuan_id" value="'+data[i]['satuan_id']+'">'+
								'<input type="hidden" class="barangobat_dept_id" value="'+data[i]['obat_dept_id']+'">';
					var tgl_kadaluarsa = format_date(data[i]['tgl_kadaluarsa']);
					t.row.add([
						(Number(i+1)),
						data[i]['nama'],
						data[i]['no_batch'],
						data[i]['harga_jual'],
						data[i]['nama_merk'],
						data[i]['total_stok'],
						data[i]['satuan'],								
						tgl_kadaluarsa,
						last
					]).draw();
				}

				t.on( 'order.dt search.dt', function () {
			        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			            cell.innerHTML = i+1;
			        } );
			    } ).draw();

				$('[data-toggle="tooltip"]').tooltip();
					
			},
			error:function (data) {
				console.log(data);
			}
		})
	}

	function setStatus(departmen){
		var u = "<?php echo base_url() ?>rawatjalan/";
		localStorage.setItem('department', departmen);
		localStorage.setItem('url', u);
		window.location.href="<?php echo base_url() ?>invoice/tambahinvoice";
	}

	//'<a href="<?php echo base_url() ?>rawatjalan/daftarpasien" onclick="visit(&quot;'+rm_id+'&quot; , &quot;'+visit_id+'&quot;)" ><i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Pemeriksaan"></i></a>'+
</script>


<script type="text/javascript">
	$(document).ready( function(){
		$('.addNewMintaFar').on('click',function(){
			tambahPermintaanFarmasi('#addinputMintaFar','.addNewMintaFar');
		});

		$('.addNewRetFar').on('click',function(){
			tambahReturFarmasi('#addinputRetFar','.addNewRetFar');
		});

		$('.addNewLog').on('click',function(){
			tambahPermintaanLogistik('#addinputMintaLog','.addNewLog');
		});

		$("#bwinvent").click(function(){
			$("#ibwinvent").slideToggle();
		});

		$("#bwpermintaanfarmasi").click(function(){
			$("#ibwpermintaanfarmasi").slideToggle();
		});

		$("#bwreturfarmasi").click(function(){
			$("#ibwreturfarmasi").slideToggle();
		});

		$("#bwinlogistik").click(function(){
			$("#ibwinlogistik").slideToggle();
		});

		$("#bwpermintaanlogistik").click(function(){
			$("#ibwpermintaanlogistik").slideToggle();
		});

	});

</script>