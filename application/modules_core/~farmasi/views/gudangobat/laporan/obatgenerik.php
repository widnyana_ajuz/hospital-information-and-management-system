<?php

$sekarang = str_replace('-', "_", date('d-m-Y'));
$title = 'Laporan_obatgenerik_'.$nama_dept.'_'.$sekarang;

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kartu Stok</title>

   <style>
    	.grup-pertanyaan {
			text-align: center;
			border: solid 1px #000;
		}
		table td {
			border-bottom: solid 0.5px #000;
			font-size: 12pt;
			vertical-align:middle;
			line-height:40px;
			width: 300px;
		}
		.keterangan_pertanyaan {
			font-size: 8pt;
		}
		table .nama_matkul{
			text-transform:capitalize;
		}
		table {
			width: 100%;
		}
		table .header {
			background-color: yellow;			
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;
			font-weight: bold;
			vertical-align:middle;
			line-height:40px;
		}
		table .body {
			border-top: solid 0.5px #000;			
			border-left: solid 0.5px #000;			
			border-right: solid 0.5px #000;			
			border-bottom: solid 0.2px #000;			
		}
		.center {
			text-align:center;
		}
		.right {
			text-align:right;			
		}
		.italic {
			font-style:italic;
		}
    </style>

</head>

<body>
	<table>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong>LAPORAN PENULISAN RESEP GENERIK</strong></td>
		</tr>
		<tr>
			<td colspan="20" style="text-align:center;border-bottom:none;"><strong> RS DATU SANGGUL RANTAU</strong></td>
		</tr>
	</table>

	<br/>
	<strong>Daftar obat dari tanggal </strong> <?php echo $awal ?>
	<strong> sampai tanggal</strong> <?php echo date('d F Y') ?>
	<br/>
	<br/>

	<div class="hasil_kelas">
		<table class="table" id="hasil-evaluasi-dosen" border="1">
			<thead>
				<tr class="header">
					<th style="text-align:center;width:300px">Nama Obat</th>
					<th style="text-align:center;width:300px">Nomor Batch</th>
					<th style="text-align:center;width:300px" colspan="2">Tanggal kadaluarsa</th>
					<th style="text-align:center;width:300px">Jumlah</th>
					
				</tr>
			</thead>
			<tbody>
				<?php  
					foreach ($result as $value) {
						$dateTime = DateTime::createFromFormat('Y-m-d',$value['tgl_kadaluarsa']);
						$newDateString = $dateTime->format('d F Y');
						echo '<tr class="body">'.
									'<td style="text-align:center;width:300px">'.$value['nama'].'</td>'.
									'<td style="text-align:center;width:300px">'.$value['no_batch'].'</td>'.
									'<td style="text-align:center;width:300px" colspan="2">'.$newDateString.'</td>'.
									'<td style="text-align:center;width:300px">'.$value['jumlah'].'</td>'.					
								'</tr>';
					}
				?>	
			</tbody>
		</table>
	</div>
</body>
</html>